<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */


set_time_limit(0);
define('WP_MEMORY_LIMIT', '256M');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'shplbllt_iklvb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1gzztkpuhhydzixmbwxv8xjgokkaftgj7trrwinern7wmexabxiyzgkzjpv9ljja');
define('SECURE_AUTH_KEY',  'j7s9gyasdcgdj8tzt1b4lczmkyjmt0lhruhgxhzai14xygrthfvncv027ro6gzgp');
define('LOGGED_IN_KEY',    'seatw8cfgex10eg5fq2y0t2adifvjoikcxju1qubtd9uy3xjks2ju5mqcjhza3lx');
define('NONCE_KEY',        'pd3s5vyuxjvldccdbyx9bs7u33dnsikx6utwsi8uftf3otieg4c1g9yqpfdk3ryl');
define('AUTH_SALT',        'eazv8jx4wpbgb9tvr5jb5xvudln1oaeoooquuxuan6euelhe3qaeyidqggi9yqpd');
define('SECURE_AUTH_SALT', 'uua9cuvkiyn9ywndpg5olfvgo3oryhyydgfrqsnf3fmmwbtx0q0g2ahhn518ukr6');
define('LOGGED_IN_SALT',   '2bh8tvh88p2m3dp4lift8zrykwuczmztulqvcdtcikvfgqp3e2baockfazwegqju');
define('NONCE_SALT',       'basl6dczwp5b5flclg9gsivau4xkeyahk2l8lnglfkui1e9hfosblqo2yqitditl');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpgd_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define( 'WP_MEMORY_LIMIT', '128M' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');



// disable WP cron => moved to siteground cPanel
define('DISABLE_WP_CRON', true);

# Disables all core updates. Added by SiteGround Autoupdate:
define( 'WP_AUTO_UPDATE_CORE', false );
