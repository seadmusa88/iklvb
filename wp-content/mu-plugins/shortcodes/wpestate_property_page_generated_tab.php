<?php

function estate_property_page_generated_tab($propid,$description,$property_address,$property_details,$amenities_features,$map,$virtual_tour,$walkscore,$floor_plans,$page_views,$yelp_details){
    $walkscore_api      =   esc_html ( get_option('wp_estate_walkscore_api','') );
    $show_graph_prop_page   = esc_html( get_option('wp_estate_show_graph_prop_page', '') );
    $random             =   rand(0,99999);
	
	$embed_virtual_tour = get_post_meta($propid, 'embed_virtual_tour', true);
	
    $return             =   '
			<div role="tabpanel" class="bhs-wrapper" id="tab_prpg"> 
			<div class="bhs-wrapper">
				<!-- div class="scroller scroller-left"><i class="glyphicon glyphicon-chevron-left"></i></div -->
				<!-- div class="scroller scroller-right"><i class="glyphicon glyphicon-chevron-right"></i></div -->
				<div class="more-items"></div>
				<ul class="nav nav-tabs list" role="tablist">
	';
    $active_class       =   " active ";
    $active_class_tab   =   " active ";
    
    if($description!=''){
        $return.='<li role="presentation" class="'.$active_class.'" >
        <a href="#description'.$random.'" aria-controls="description'.$random.'" role="tab" data-toggle="tab">
        '.$description.'
        </a>
        </li>';
        $active_class= '';
    }
    
    if($property_address!=''){
        $return.='<li role="presentation" class="'.$active_class.'">
        <a href="#address'.$random.'" aria-controls="address'.$random.'" role="tab" data-toggle="tab">
        '.$property_address.'
        </a>
        </li>';
        $active_class= '';
    }
    
    if($property_details!=''){
        $return.='<li role="presentation" class="'.$active_class.'">
        <a href="#details'.$random.'" aria-controls="details'.$random.'" role="tab" data-toggle="tab">
        '.$property_details.'
        </a>
        </li>';
        $active_class= '';
    }
    
    if($amenities_features!=''){
        $return.='<li role="presentation" class="'.$active_class.'">
        <a href="#features'.$random.'" aria-controls="features'.$random.'" role="tab" data-toggle="tab">
        '.$amenities_features.'
        </a>
        </li>';
        $active_class= '';
    }
    
    if($map!=''){
        $return.='<li role="presentation" class="shtabmap '.$active_class.'">
        <a href="#propmap'.$random.'" aria-controls="propmap'.$random.'" role="tab" data-toggle="tab">
        '.$map.'
        </a>
        </li>';
        $active_class= '';
    }
    
   
    //if($virtual_tour!=''){
    if($embed_virtual_tour != ''){
        $active_class= '';
        $return.='<li role="presentation" class="'.$active_class.'">
        <a href="#virtual_tour'.$random.'" aria-controls="virtual_tour'.$random.'" role="tab" data-toggle="tab">
        '.$virtual_tour.'
        </a>
        </li>';
        $active_class= '';
    }
   
    
    if($walkscore!=''){
        $active_class= '';
        $return.='<li role="presentation" class="'.$active_class.'">
        <a href="#walkscore'.$random.'" aria-controls="walkscore'.$random.'" role="tab" data-toggle="tab">
        '.$walkscore.'
        </a>
        </li>';
        $active_class= '';
    }
    
	/*
    if($floor_plans!=''){
        $return.='<li role="presentation" class=" '.$active_class.'" >
        <a href="#floor'.$random.'" aria-controls="floor'.$random.'" role="tab" data-toggle="tab">
        '.$floor_plans.'
        </a>
        </li>';
        $active_class= '';
    }
    */
	
    if($page_views!=''){
        $return.='<li role="presentation" class="tabs_stats '.$active_class.'" data-listingid="'. intval($propid).'">
        <a href="#stats'.$random.'" aria-controls="stats'.$random.'" role="tab" data-toggle="tab">
        '.$page_views.'
        </a>
        </li>';
        $active_class= '';
    }
    
    
    
    if($yelp_details!=''){
        $return.='<li role="presentation" class="tabs_stats '.$active_class.'" data-listingid="'. intval($propid).'">
        <a href="#yelp'.$random.'" aria-controls="yelp'.$random.'" role="tab" data-toggle="tab">
        '.$yelp_details.'
        </a>
        </li>';
        $active_class= '';
    }
    
    
    
    
    
    
    $return .=' </ul></div>';
   
    ///////////////////////////////////////////////////////////////////////////
    
    $return .='<div class="tab-content">';
    if($description!=''){
        $return.='<div role="tabpanel" class="tab-pane '.$active_class_tab.'" id="description'.$random.'">';
        $return.=   estate_listing_content($propid);
        //$return.=    get_template_part ('/templates/download_pdf');
        $return.='</div>';
        $active_class_tab ='';
    }
    
    if($property_address!=''){
        $return.='<div role="tabpanel" class="tab-pane '.$active_class_tab.'" id="address'.$random.'">
        '.estate_listing_address($propid).'
        </div>';
        $active_class_tab ='';
    }
    
    if($property_details!=''){
        $return.='<div role="tabpanel" class="tab-pane '.$active_class_tab.'" id="details'.$random.'">
        '.estate_listing_details($propid).'  
        </div>';
        $active_class_tab ='';     
    }
    
    if($amenities_features!=''){
        $return.='<div role="tabpanel" class="tab-pane '.$active_class_tab.'" id="features'.$random.'">
        '.estate_listing_features($propid).'
        </div>';
        $active_class_tab ='';
    }
    
    if($map!=''){
        $return.='<div role="propmap" class="tab-pane   '.$active_class_tab.'" id="propmap'.$random.'">'
        .do_shortcode('[property_page_map propertyid="'.$propid.'" istab="1"][/property_page_map]').
        '</div>';
         
        $active_class_tab ='';
    }
    
    
    // if($virtual_tour!=''){
    if($embed_virtual_tour != ''){
        $return.='<div role="tabpanel" class="tab-pane '.$active_class_tab.'" id="virtual_tour'.$random.'">';
        if($virtual_tour!=''){
            ob_start();
            wpestate_virtual_tour_details($propid);
            $temp=  ob_get_contents();
            ob_end_clean();
            $return.=$temp;
        }
        $return.='</div>';
        $active_class_tab ='';
    }
    
    if($walkscore!=''){
        $return.='<div role="tabpanel" class="tab-pane '.$active_class_tab.'" id="walkscore'.$random.'">';
        if($walkscore_api!=''){
            ob_start();
            wpestate_walkscore_details($propid);
            $temp=  ob_get_contents();
            ob_end_clean();
            $return.=$temp;
        }
        $return.='</div>';
        $active_class_tab ='';
    }
    
	/*
    if($floor_plans!=''){
        $return.='<div role="tabpanel" class="tab-pane '.$active_class_tab.'" id="floor'.$random.'">';
        ob_start();
        estate_floor_plan($propid);
        $temp=  ob_get_contents();
        ob_end_clean();
                
        $return.=$temp.'</div>';
        $active_class_tab ='';
    }
    */
	
    if($page_views!=''){
        $return.='  <div role="tabpanel" class="tab-pane '.$active_class_tab.'" id="stats'.$random.'">
             <div class="panel-body">
                <canvas id="myChart"></canvas>
             </div>
        </div>';
        $active_class_tab ='';
    }


    
    
    
    if($yelp_details!=''){
      
        $return.='  <div role="tabpanel" class="tab-pane '.$active_class_tab.'" id="yelp'.$random.'">';
            $yelp_client_id         =   get_option('wp_estate_yelp_client_id','');
            $yelp_client_secret     =   get_option('wp_estate_yelp_client_secret','');
            $yelp_client_api_key_2018  =   get_option('wp_estate_yelp_client_api_key_2018','');
            if($yelp_client_api_key_2018!=='' && $yelp_client_id!==''  ){ 
                ob_start();
                wpestate_yelp_details($propid);
                $temp=  ob_get_contents();
                ob_end_clean();
                $return.=$temp;

            }
        $return.='</div>';
        $active_class_tab ='';
    }
    
    $return.=' </div></div>';
    
 
    
    return $return;
}