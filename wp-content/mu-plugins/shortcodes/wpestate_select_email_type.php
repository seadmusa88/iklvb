<?php

function wpestate_select_email_type($user_email,$type,$arguments)
{
	$value          =   get_option('wp_estate_'.$type,'');
	$value_subject  =   get_option('wp_estate_subject_'.$type,'');  

	if (function_exists('icl_translate') ){
		$value          =  icl_translate('wpresidence-core','wp_estate_email_'.$value, $value ) ;
		$value_subject  =  icl_translate('wpresidence-core','wp_estate_email_subject_'.$value_subject, $value_subject ) ;
	}

	$message = wpautop($value);  // 2019-12-07 Lorenzo M.
	
	wpestate_emails_filter_replace($user_email,$message,$value_subject,$arguments);
}