<?php

function wpestate_estate_property_design_intext_details($attributes,$content=''){
    
    
    global $post;
    global $propid;
    $return_string='';
    $detail='';
    $detail =$content;
    $css = '';

    extract(shortcode_atts(array(
    'css' => ''
    ), $attributes));
    
    $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ),'', $attributes );
    

    $feature_list       =   esc_html( get_option('wp_estate_feature_list') );
    $feature_list_array =   explode( ',',$feature_list);
    $features_details   =   array();
    
    foreach($feature_list_array as $key => $value){
        $post_var_name      =   str_replace(' ','_', trim($value) );
        $input_name         =   wpestate_limit45(sanitize_title( $post_var_name ));
        $input_name         =   sanitize_key($input_name);
        $features_details[$value] =      $input_name;
    }
  
    
    
    
    preg_match_all("/\{[^\}]*\}/", $detail, $matches);
    //var_dump($matches[0]);   
    
    $return_string =    '<div class="wpestate_estate_property_design_intext_details '.$css_class.'"> '; 

        
    foreach($matches[0] as $key=>$value){
        // $element =   substr($value,  1);
        $element    =    substr($value, 1);
        $element    =    substr($element, 0, -1);
        //$return_string.=  $value.'/'.$element.'*';
        
        if(in_array ($element,$features_details) ){

            $get_value= intval(get_post_meta($propid,$element,true));
            if($get_value==1){
                $replace='yes';
            }else{
                $replace='no';
            }
        }else{
            if($element =='prop_id'){
                $replace=$propid;
            }else if($element =='prop_url'){
                $replace=get_permalink($propid);
            }else if($element =='count_favor'){
                $replace=get_permalink($propid);
            }else if($element =='count_visitor'){
                $replace=get_permalink($propid);
            }else if($element =='favorite_action'){
                $current_user               = wp_get_current_user();
                $userID                     =   $current_user->ID;
                $user_option                =   'favorites'.$userID;
                $curent_fav                 =   get_option($user_option);
                $favorite_class             =   'icon-fav-off'; 
                $favorite_text              =   esc_html__('add to favorites','wpresidence-core');
                if($curent_fav){
                    if ( in_array ($propid,$curent_fav) ){
                        $favorite_class =   'icon-fav-on';     
                        $favorite_text  =   esc_html__('favorite','wpresidence-core');
                    } 
                }
                //$replace= '<span class="icon-fav icon-fav-on-remove" data-postid="'.$propid.'"> '.$favorite_text.'</span>';

                //$replace='<span id="add_favorites" class="'.esc_html($favorite_class).'" data-postid="'.$propid.'">'.$favorite_text.'</span>';
                
                $replace='<span id="add_favorites" class="icon-fav custom_fav '.esc_html($favorite_class).'" data-original-title="'.$favorite_text.'" data-postid="'.$propid.'"></span>';
            }else if($element =='property_status'){
                $replace= stripslashes ( get_post_meta($propid,$element,true));
                if($replace=='normal'){
                    $replace='';
                }
            }else if($element =='property_pdf'){
                  $replace=wpestate_property_sh_download_pdf($propid);
            }else if($element =='page_views'){
                $replace=intval( get_post_meta($propid, 'wpestate_total_views', true) ).' '.esc_html__('views','wpredidence');
            }    
            else if($element=='title'){
               $replace=get_the_title($propid);
            }else if($element =='property_price'){
                $currency                   =   esc_html( get_option('wp_estate_currency_symbol', '') );
                $where_currency             =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
                $replace =  wpestate_show_price_without_label_after($propid,$currency,$where_currency,1);  // 2108-09-28 Lorenzo M.
			}else if($element =='property_label') {  
				// 2108-09-28 Lorenzo M. >>>
				$replace = stripslashes ( get_post_meta($propid,$element,true));
				if(strlen($replace)) $replace = ' / '.$replace;
				// <<<
            }else if($element =='description'){
                $replace= estate_listing_content($propid);
            }else if($element =='property_category' || $element =='property_action_category' || $element =='property_city' || $element =='property_area' || $element =='property_county_state' ){
                $replace=  '#'.get_the_term_list($propid, $element, '', ', ', '') ;
            }else if($element =='link_action'){
                $replace=  '<a href="#schedule_meeting"><span class="fa fa-calendar-o"></span></a>' ;
            }else if($element =='date_publish'){
                $replace = get_the_date('d F Y');
            }else if($element =='user_property'){     	
                $replace = '<a href="'.get_permalink( $post ).'">'.get_the_title($post).'</a>';                                
            }else if($element =='listing_actions'){
                $replace='<div class="listing_actions"><div class="share_unit">';                
                $replace.='<a href="http://www.facebook.com/sharer.php?u='.get_the_permalink($propid).'&amp;t='.urlencode(get_the_title($propid)).'" target="_blank"><i class="fa fa-facebook"></i></a>';
                $replace.='<a href="http://twitter.com/home?status='.urlencode(get_the_title($propid).' '.get_the_permalink($propid)).'" target="_blank"><i class="fa fa-twitter"></i></a>';
                $replace.='<a href="whatsapp://send?text='.urlencode(get_the_title($propid).' '.get_permalink($propid)).'" data-action="share/whatsapp/share"><i class="fa fa-whatsapp"></i></a>';
                if (has_post_thumbnail($propid)){
                    $pinterest = wp_get_attachment_image_src(get_post_thumbnail_id($propid),'property_full_map');
                }
                $replace.='<a href="http://pinterest.com/pin/create/button/?url='.get_the_permalink($propid).'&amp;media='.esc_url($pinterest[0]).'&amp;description='.urlencode(get_the_title($propid)).'" target="_blank"> <i class="fa fa-pinterest"></i> </a>';
                $replace.='</div><span class="share_list" data-original-title="Condividere"></span>';
                $current_user               = wp_get_current_user();
                $userID                     =   $current_user->ID;
                $user_option                =   'favorites'.$userID;
                $curent_fav                 =   get_option($user_option);
                $favorite_class             =   'icon-fav-off';
                $favorite_text              =   esc_html__('add to favorites','wpresidence-core');
                if($curent_fav){
                    if ( in_array ($propid,$curent_fav) ){
                        $favorite_class =   'icon-fav-on';
                        $favorite_text  =   esc_html__('favorite','wpresidence-core');
                    }
                }
                $replace.='<span class="link_unit"><a href="#schedule_meeting"><span class="fa fa-calendar-o"></span></a></span>' ;
                $replace.='<span class="icon-fav '.esc_html($favorite_class).'" data-original-title="'.$favorite_text.'" data-postid="'.$propid.'"></span>';
                $replace.='<span class="link_unit fa fa-print" id="print_page" data-propid="'.$propid.'"></span>';
                $replace.='</div>';
            }else if($element =='property_visits'){
                $args = array(
                    'post_type' => 'woovouchercodes',
                    'post_status' => 'publish',
                    'meta_query' => array(
                        array(
                            'key'     => '_wpw_pcv_assign_prop',
                            'value'   => $propid,
                            'compare' => '=',
                        ),
                        array(
                            'key'     => '_wpw_pcv_status',
                            'value'   => array('assigned','confirmed','performed'),
                            'compare' => 'IN',
                        ),
                    ),
                );
                
                $result = new WP_Query( $args );
                $counter= $result->post_count;
                $replace = '<span class="property_visits">'.$counter.' '.esc_html__('people visited this property','wpredidence').'</span>';     
        }else if($element =='property_visitors'){
            $args = array(
                'post_type' => 'woovouchercodes',
                'post_status' => 'publish',
                'meta_query' => array(
                    array(
                        'key'     => '_wpw_pcv_assign_prop',
                        'value'   => $propid,
                        'compare' => '=',
                    ),
                    array(
                        'key'     => '_wpw_pcv_status',
                        'value'   => array('assigned','confirmed','performed'),
                        'compare' => 'IN',
                    ),
                ),
            );
            
            $result = new WP_Query( $args );
            $counter= $result->post_count;
            $replace = '<span class="property_visitors">'.$counter.' '.esc_html__('visitors','wpredidence').'</span>';
        }else{
                $meta_value = get_post_meta($propid,$element,true);
                $replace = apply_filters( 'wpml_translate_single_string', $meta_value, 'wpresidence-core', 'wp_estate_property_custom_s_'.$meta_value );
           
            
                $replace= get_post_meta($propid,$element,true);
            }
            
        }
      

        $detail=str_replace($value,$replace,$detail);
    }
    
    

    $return_string .=  do_shortcode( $detail ).'</div>'; 
    wp_reset_query();
    wp_reset_postdata();
    return $return_string;
}