<?php

// if( !function_exists('wpestate_estate_property_design_agent_contact') ):
function wpestate_estate_property_design_agent_contact($attributes,$content = null)
{
    // global $post;
    global $propid;
	
		
	

		
	// init
	$url_find_more = '/buyers-renters/';						// migliorare
	$url_tickets = '/my-visits/';
	$min_date = date('Y\, m\, d', strtotime('+'.WPW_PCV_TIME_NEXT_MOVE_OWNER_FIRST.' seconds'));	// data minima del calendario
	$max_date = '';																					// data massima del calendario
    $css_class = '';	
	$prop_action_type = '';
	$user_have_open_tickets = false;
    $user_have_open_tickets_info = '';
	
	// oggi
	$now = date('Y-m-d H:i:00');

	// init object 
	$ticket_obj = new Wpw_Pcv_Public();
	
	// valuta impostata in woocommerce
	$currency = get_woocommerce_currency_symbol();
	
	// formato data e ora impostati in wordpress
	$date_format = get_option('date_format');
	$time_format = get_option('time_format');
	$date_format_short = $date_format;
	
	// formato data per jQuery datepicker
	switch($date_format)
	{
		case 'F j, Y'	: 	$date_format_datepiker = 'MM d, yy';	$date_format_short = 'M j, Y'; break;
		case 'm/d/Y'	: 	$date_format_datepiker = 'mm/dd/yy';	break;
		case 'd/m/Y'	: 	$date_format_datepiker = 'dd/mm/yy';	break;		
		case 'Y-m-d'	: 	
		default			:	$date_format_datepiker = 'yy-mm-dd';	break;
	}
	
	
    // carico / ricavo il valore delle categoria / action della property
	$prop_action_category = get_the_terms($propid,'property_action_category');
	if(isset($prop_action_category[0]))
	{
		switch($prop_action_category[0]->slug)
		{
			case 'for-sale':	$prop_action_type = 'for_sale';
								break;

			case 'for-rent':	$prop_action_type = 'for_rent';
								break;

			case 'evento':	
			case 'event':		$prop_action_type = 'event';
								break;
		}
	}
	
	// property status
	$property_status = get_post_meta($propid, 'property_status', true);
	$property_status_message = '';
	
	

	
	// user login image >>>
	if(is_user_logged_in())
	{
		// dati dell'utente loggato
		$current_user = wp_get_current_user(); 

		// immagine del profilo dell'utente loggato
		$preview_img_html = iklvb_get_user_image_cover($current_user->ID);
		
		// controllo se l'utente ha dei biglietti in corso per questa property
		// (cioè in status 'assigned' o 'confirmed')
		$assign_prop_user_result = $ticket_obj->wpw_pcv_voucher_assign_prop_user($propid,$current_user->ID);
		$user_have_open_tickets = ($assign_prop_user_result->post_count > 0 ? true : false);
	}
	else
	{
		$preview_img_html = iklvb_get_user_image_cover();		
	}
    // <<<
	
	
	
	$ticket_edit_link_html = '
		<div class="col-sm-12">
			<p class="text-info">'.sprintf(__("Go to the <a href=\"%s\">ticket management page</a>.",'iklvb-wpresidence'),$url_tickets).'</p>
		</div>
	';

	

	
	
	// se si tratta di tour (for_rent o for_sale) >>>
	if($prop_action_type != 'event') 
	{
		// valore per visualizzare i campi input anche per i tour
		$prop_event_availability = 1;
		
		// prezzo della proprietà
		$prop_price	= get_post_meta($propid,'property_price',true);

		// ottengo info in merito al biglietto più adatto da acquistare per questa property
		$ticket_link = get_tour_ticket_link($prop_action_type,$prop_price);
			
		
		// biglietto già presente per questa proprietà / utente
		if($user_have_open_tickets)
		{
			$form_title = '<span class="text-success"><i class="fa fa-lg fa-check-circle"></i> '.__('You have a current request for this property','iklvb-wpresidence').'</span>';
			
			$open_ticket_status = get_post_meta($assign_prop_user_result->post->ID,'_wpw_pcv_status',true);
			$open_ticket_schedule_time = get_post_meta($assign_prop_user_result->post->ID,'_wpw_pcv_schedule_time',true);
			
			$open_ticket_schedule_day = date_i18n($date_format_short,strtotime($open_ticket_schedule_time));
			$open_ticket_schedule_hour = date_i18n($time_format,strtotime($open_ticket_schedule_time));
			
			$form_schedule_fields = '';
			
			$user_have_open_tickets_info = '
				<div class="row row-fields">
					<div class="col-sm-3 col-xs-6">
						<label>'.__('Schedule Day','iklvb-wpresidence').'</label>
						<p class="field-date-hour">'.strtoupper($open_ticket_schedule_day).'</p>
					</div>
					<div class="col-sm-3 col-xs-6">
						<label>'.__('Schedule Time','iklvb-wpresidence').'</label>
						<p class="field-date-hour">'.$open_ticket_schedule_hour.'</p>
					</div>
					<div class="col-sm-3 col-xs-6">
						<label>'.__('Ticket type','iklvb-wpresidence').'</label>
						<p class="field-date-hour">'.strtoupper(__(str_replace('_',' ',$prop_action_type),'iklvb-wpresidence')).'</p>
					</div>
					<div class="col-sm-3 col-xs-6">
						<label>'.__('Status','iklvb-wpresidence').'</label>
						<p class="field-date-hour">
							<a href="'.$ticket_link['shop_permalink'].'" target="_blank">
								'.ucfirst(__($open_ticket_status,'iklvb-wpresidence')).'
							</a>
						</p>
					</div>
					'.$ticket_edit_link_html.'
				</div>
			';			
		}
		else
		{
			$form_title = __('Book your tour in this private property','iklvb-wpresidence');
		
			// preparo i valori per la select delle ore
			$schedule_hour_option = '<option value="">--</option>';
			for ($i = 0; $i <= 23; $i++)
			{
				for ($j = 0; $j <= 45; $j+=30) // Lorenzo M. // cambiare 30 in 15 per intervalli inferiori
				{
					$show_j = $j;
					if($j == 0) $show_j = '00';

					$val = str_pad($i, 2, '0', STR_PAD_LEFT).':'.$show_j;
					$schedule_hour_option .= '<option value="'.$val.'">'.date_i18n($time_format,strtotime($val)).'</option>';
				}
			}

			$form_schedule_fields = '
				<div class="row row-fields">
					<div class="col-sm-3 col-xs-6">
						<label>'.__('Select a Date','iklvb-wpresidence').'</label>
						<input name="schedule_day" id="schedule_day_custom" type="input" readonly
							placeholder="'. __('Day','wpestate').'" 
							class="form-control datepicker readonly-to-default"
							min="'.$min_date.'"
							max="'.$max_date.'"
							date-format="'.$date_format_datepiker.'"
							aria-required="true">
					</div>
					<div class="col-sm-3 col-xs-6">
						<label>'.__('Select a Time','iklvb-wpresidence').'</label>
						<select name="schedule_hour" id="schedule_hour" class="form-control">
							'.$schedule_hour_option.'
						</select>
					</div>
					<div class="col-sm-6 col-xs-12">
						<label>'.__('Ticket type','iklvb-wpresidence').'</label>
						<p class="field-date-hour">
							<a href="'.$ticket_link['shop_permalink'].'" target="_blank">
								'.strtoupper(__(str_replace('_',' ',$prop_action_type),'iklvb-wpresidence')).'<br>
								<span>('.__('up to','iklvb-wpresidence').' '.number_format($ticket_link['price_range_max'],0,',','.').' '.$currency.')</span>
							</a>
						</p>
					</div>
				</div>
			';	
		}		
	}
	// <<<
	
	// se si tratta di events >>>
	else	
	{
		// titolo
		if($user_have_open_tickets)
		{
			$form_title = '<span class="text-success"><i class="fa fa-lg fa-check-circle"></i> '.__('You have already signed up for this event','iklvb-wpresidence').'</span>';
		}
		else
		{
			$form_title = __('Participate as a guest at the event in this private property','iklvb-wpredidence');
			$ticket_edit_link_html = '';
		}
		
		$prop_event_schedule_day	= get_post_meta($propid, 'event_schedule_day',	true);
		$prop_event_schedule_hour	= get_post_meta($propid, 'event_schedule_hour',	true);
		$prop_event_ticket_type		= get_post_meta($propid, 'event_ticket_type',	true);	
		$prop_event_guest_limit		= get_post_meta($propid, 'event_guest_limit',	true);	
		
		$guest_count = $ticket_obj->wpw_pcv_voucher_count_by_property($propid);
		$prop_event_availability = $prop_event_guest_limit - $guest_count;	
		
		// ottengo info in merito al biglietto più adatto da acquistare per questa property
		$ticket_link = get_event_ticket_link($prop_event_ticket_type);
		
		$form_schedule_fields = '
			<div class="row row-fields">
				<div class="col-sm-3 col-xs-6">
					<label>'.__('Event Date','iklvb-wpresidence').'</label>
					<p class="field-date-hour">'.strtoupper(date_i18n($date_format_short,strtotime($prop_event_schedule_day))).'</p>
					<input type="hidden" name="schedule_day" id="schedule_day_hidden" value="'.$prop_event_schedule_day.'" readonly>
				</div>
				<div class="col-sm-3 col-xs-6">
					<label>'.__('Event Time','iklvb-wpresidence').'</label>
					<p class="field-date-hour">'.date_i18n($time_format,strtotime($prop_event_schedule_hour)).'</p>
					<input type="hidden" name="schedule_hour" id="schedule_hour_hidden" value="'.$prop_event_schedule_hour.'" readonly>
				</div>
				<div class="col-sm-3 col-xs-6">
					<label>'.__('Ticket type','iklvb-wpresidence').'</label>
					<p class="field-date-hour">
						<a href="'.$ticket_link['shop_permalink'].'" target="_blank">
							'.strtoupper($prop_event_ticket_type).'
						</a>				
					</p>				
				</div>
				<div class="col-sm-3 col-xs-6">
					<label>'.__('Available places','iklvb-wpresidence').'</label>
					<p class="field-date-hour">'.($prop_event_availability > 0 ? $prop_event_availability : '<span class="text-danger">SOLD OUT</span>').'</p>				
				</div>
				'.$ticket_edit_link_html.'
			</div>
		';		
	}
	// <<<
	

	$disabled_submit = '';
	$message_not_logged_in = '';

	if(!is_user_logged_in())		
	{
		$disabled_submit = 'disabled';
		$message_not_logged_in = '
			<p class="text-warning">
				<i class="fa fa-warning fa-lg"></i> '.__('You need to be logged in to check ticket number','iklvb-wpredidence').'
			</p>
		';
	}
	
	
	// messaggi in base allo status della property >>>
	switch($property_status) 
	{
		case 'on tour now!':
		case 'in negotiation now':
			$property_status_message = __('The owner is evaluating an offer, therefore it is not possible to visit the property at the moment.','iklvb-wpredidence');
			break;

		case 'closed':
			$property_status_message = __('It is not possible to visit the property.','iklvb-wpredidence');
			break;
	}
	
	if(empty($property_status_message))
	{
		if($prop_action_type == 'event' && $now > $prop_event_schedule_day.' '.$prop_event_schedule_hour)
		{
			$prop_event_schedule_text  = date_i18n($date_format, strtotime($prop_event_schedule_day)).', ';
			$prop_event_schedule_text .= date_i18n($time_format, strtotime($prop_event_schedule_hour));
			$property_status_message = __('The event in this property has already taken place.','iklvb-wpredidence').' ('.$prop_event_schedule_text.')';
		}
	}
	
	if(strlen($property_status_message))
	{
		$property_status_message = '
			<p class="text-warning">
				<i class="fa fa-warning fa-lg"></i> '.$property_status_message.'
			</p>
		';
		$form_schedule_fields = '';
	}
	// <<<

		
	$return_string = '
		<a id="schedule_meeting"><!-- anchor text --></a>
		<div class="wpestate_estate_property_design_agent '.$css_class.'">	
			<div class="agent_contanct_form">
				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-3">
						'.$preview_img_html.'
					</div>
                    <div class="col-md-9 col-sm-9 col-xs-9">
                        <div class="row row-confirm">
							<div class="col-md-12">                                    
                                <h4 id="show_contact_">'.$form_title.'</h4>
								'.$property_status_message.'							
							</div>
						</div>	
                    </div>
					<div class="col-md-9 col-sm-12 col-xs-12">													
							
						'.$form_schedule_fields.'							
						
						'.($prop_event_availability > 0 && $user_have_open_tickets == false && empty($property_status_message) ? '
						<div class="row row-fields">						
							<div class="col-md-12">
								<label>'. __('Insert a Ticket Number','iklvb-wpresidence').'</label>
								<div class="autocomplete" style="width:300px;">
								<input type="text" name="ticket_number"  
									class="form-control" id="ticket_number" 
									placeholder="'. __('Your Ticket Number','wpwpcv').'" 
									aria-required="true">
									</div>
							</div>							
							<div class="col-md-12">
								<div class="alert-box">
									<div id="image-loader"><img src="'.get_template_directory_uri().'-child/img/loading_icon.gif"></div>
									<div class="alert-error" id="message_error_default">'.__('<strong>ERROR</strong>: Some error occur while submitting request... try again...','iklvb-wpresidence').'</div>
									<div class="alert-error" id="message_error_json"></div>
								</div>									
							</div> 
						</div>
						
						<div class="row row-fields" style="margin-top:30px">
							<div class="col-md-4">
								<input type="submit" class="wpresidence_button submit_property_booking btn" '.$disabled_submit.'
									id="agent_submit" value="'. __('Book a tour','iklvb-wpresidence').'">

								<input name="prop_id" type="hidden"  
									id="agent_property_id" value="'.intval($propid).'">
									
								<input type="hidden" name="contact_ajax_nonce" 
									id="agent_property_ajax_nonce"  value="'. wp_create_nonce( 'ajax-property-contact' ).'">
							</div>
							<div class="col-md-8">
								'.$message_not_logged_in.'
							</div>
						</div>
						
						<div class="row row-fields" style="margin-top:30px">
							<div class="col-md-12">
								<p>'.sprintf(__("You don't have a Ticket? <a href=\"%s\" target=\"_blank\">Click here</a> It's easy!",'iklvb-wpresidence'),$ticket_link['shop_permalink']).'<br>
									<span class="text-info">
										'.__("Deliver your ticket directly at the entrance to the private property. The author of the ad will have to phisically receive your ticket to be able to collect it. iKLVB is the 100% secure system.",'iklvb-wpresidence').'
										'.sprintf(__("<a href=\"%s\" target=\"_blank\">Find out more</a>",'iklvb-wpresidence'),$url_find_more).'
									</span>
								</p>
							</div>
						</div>
						' : '').'
						
						'.($prop_event_availability <= 0 && $user_have_open_tickets == false && empty($property_status_message) ? '
						<div class="row">
							<div class="col-md-12">
								<p><span class="text-info">'.__('Sorry but there are no more places available for this event...','iklvb-wpresidence').'</span></p>
							</div>
						</div>
						' : '').'
							
						'.$user_have_open_tickets_info.'
							
						<div class="row row-confirm">
							<div class="col-md-12">
								<div class="alert-box">
									<div id="image-confirm"><img src="'.get_template_directory_uri().'-child/img/confirm_icon.png"></div>
									<div class="alert-ok" id="message_ok_json"></div>
								</div>
							</div>
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
	';
	?>
<?php
	
    return $return_string;
}
// endif;