<?php

function wpestate_register_form_function($attributes, $content = null) {
 
     $register_nonce=wp_nonce_field( 'register_ajax_nonce', 'security-register',true,false );
     $return_string='
          <div class="login_form shortcode-login">
               <div class="loginalert" id="register_message_area" ></div>
               
                <div class="loginrow">
                    <input type="text" name="user_firstname_register" id="user_firstname_register" class="form-control" placeholder="'.esc_html__('First Name','wpresidence-core').'" size="20" />
                    <input type="text" name="user_secondname_register" id="user_secondname_register" class="form-control" placeholder="'.esc_html__('Last Name','wpresidence-core').'" size="20" />
                </div>
                <div class="loginrow">
                    <input type="text" name="user_email_register" id="user_email_register" class="form-control" placeholder="'.esc_html__('Email','wpresidence-core').'" size="20" />
                </div>';
                
                $enable_user_pass_status= esc_html ( get_option('wp_estate_enable_user_pass','') );
                if($enable_user_pass_status == 'yes'){
                    $return_string.= '
                    <div class="loginrow">
                        <input type="password" name="user_password" id="user_password" class="form-control" placeholder="'.esc_html__('Password','wpresidence-core').'"/>
                    </div>
                    <div class="loginrow">
                        <input type="password" name="user_password_retype" id="user_password_retype" class="form-control" placeholder="'.esc_html__('Retype Password','wpresidence-core').'"  />
                    </div>
                    ';
                }
                if(1==1){
                $user_types = array(
                    esc_html__('Select User Type','wpresidence-core'),
                    esc_html__('User','wpresidence-core'),
                    esc_html__('Single Agent','wpresidence-core'),
                    esc_html__('Agency','wpresidence-core'),
                    esc_html__('Developer','wpresidence-core'),
                ); 
                
                
                $permited_roles             = get_option('wp_estate_visible_user_role',true);
                $visible_user_role_dropdown = get_option('wp_estate_visible_user_role_dropdown',true);
                    
                    if($visible_user_role_dropdown=='yes'){
                        $return_string.='<select id="new_user_type" name="new_user_type" class="form-control" >';
                        $return_string.= '<option value="0">'.esc_html__('Select User Type','wpresidence-core').'</option>';
                        foreach($user_types as $key=>$name){
                            if(in_array($name, $permited_roles)){
                                $return_string.= '<option value="'.$key.'">'.$name.'</option>';
                            }
                        }
                        $return_string.= '</select>';
                }
                }

                $return_string.='        
                <input type="checkbox" name="terms" id="user_terms_register_sh">
                <label id="user_terms_register_sh_label" for="user_terms_register_sh">'.esc_html__('I agree with ','wpresidence-core').'<a href="'.wpestate_get_template_link('terms_conditions.php').'" target="_blank" id="user_terms_register_topbar_link">'.esc_html__('terms & conditions','wpresidence-core').'</a> </label>';
               
                if(get_option('wp_estate_use_captcha','')=='yes'){
                    $return_string.= '<div id="shortcode_register_menu"  style="float: left;margin-top: 10px;transform:scale(0.75);-webkit-transform:scale(0.75);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>';
                 }
           
                
                if($enable_user_pass_status != 'yes'){
                    $return_string.='<p id="reg_passmail">'.esc_html__('A password will be e-mailed to you','wpresidence-core').'</p>';
                }
                
                $return_string.= '   
                <input type="hidden" id="security-register" name="security-register" value="'.estate_create_onetime_nonce( 'register_ajax_nonce_sh' ).'">
           
                <p class="submit">
                    <button id="wp-submit-register"  class="wpresidence_button">'.esc_html__('Register','wpresidence-core').'</button>
                </p>
                
        </div>
                     
    ';
     return  $return_string;
}
