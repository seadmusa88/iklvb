<?php

// if( !function_exists('wpestate_login_form_function') ):
  
function wpestate_login_form_function($attributes, $content = null) {
     // get user dashboard link
        global $wpdb;
        $redirect='';
        $mess='';
        $allowed_html   =   array();
        
        $attributes = shortcode_atts( 
              array(
                  'register_label'                  => '',
                  'register_url'                =>  '',
                 
              ), $attributes) ;  

  
    $post_id=get_the_ID();
    $login_nonce=wp_nonce_field( 'login_ajax_nonce', 'security-login',true,false );
    $security_nonce=wp_nonce_field( 'forgot_ajax_nonce', 'security-forgot',true,false );
    $return_string='<div class="login_form shortcode-login" id="login-div">
         <div class="loginalert" id="login_message_area" >'.$mess.'</div>
        
                <div class="loginrow">
                    <input type="text" class="form-control" name="log" id="login_user" placeholder="'.esc_html__('Email','wpresidence-core').'" size="20" />
                </div>
                <div class="loginrow">
                    <input type="password" class="form-control" name="pwd" id="login_pwd"  placeholder="'.esc_html__('Password','wpresidence-core').'" size="20" />
                </div>
                <input type="hidden" name="loginpop" id="loginpop" value="0">
              
                <input type="hidden" id="security-login" name="security-login" value="'. estate_create_onetime_nonce( 'login_ajax_nonce' ).'">
       
                   
                <button id="wp-login-but" class="wpresidence_button">'.esc_html__('Login','wpresidence-core').'</button>
                <div class="login-links shortlog">
    
				<p class="forgetmenot"><label for="rememberme"><input name="rememberme" type="checkbox" id="rememberme" value="forever"  /> Remember Me</label></p>
						
				';
          
                if(isset($attributes['register_label']) && $attributes['register_label']!=''){
                     $return_string.='<a href="'.$attributes['register_url'].'">'.$attributes['register_label'].'</a> | ';
                }         
                $return_string.='<a href="#" id="forgot_pass">'.esc_html__('Forgot Password?','wpresidence-core').'</a>
                </div>';
                $facebook_status    =   esc_html( get_option('wp_estate_facebook_login','') );
                $google_status      =   esc_html( get_option('wp_estate_google_login','') );
                $yahoo_status       =   esc_html( get_option('wp_estate_yahoo_login','') );
               
                
                if($facebook_status=='yes'){
                    $return_string.='<div id="facebooklogin" data-social="facebook">'.esc_html__('Login with Facebook','wpresidence-core').'</div>';
                }
                if($google_status=='yes'){
                    $return_string.='<div id="googlelogin" data-social="google">'.esc_html__('Login with Google','wpresidence-core').'</div>';
                }
                if($yahoo_status=='yes'){
                    $return_string.='<div id="yahoologin" data-social="yahoo">'.esc_html__('Login with Yahoo','wpresidence-core').'</div>';
                }
                   
         $return_string.='                 
         </div>
         <div class="login_form  shortcode-login" id="forgot-pass-div-sh">
            <div class="loginalert" id="forgot_pass_area"></div>
            <div class="loginrow">
                    <input type="text" class="form-control" name="forgot_email" id="forgot_email" placeholder="'.esc_html__('Enter Your Email Address','wpresidence-core').'" size="20" />
            </div>
            '. $security_nonce.'  
            <input type="hidden" id="postid" value="'.$post_id.'">    
            <button class="wpresidence_button" id="wp-forgot-but" name="forgot" >'.esc_html__('Reset Password','wpresidence-core').'</button>
            <div class="login-links shortlog">
            <a href="#" id="return_login">'.esc_html__('Return to Login','wpresidence-core').'</a>
            </div>
         </div>
        
            ';
    return  $return_string;
}
// endif; // end   wpestate_login_form_function 