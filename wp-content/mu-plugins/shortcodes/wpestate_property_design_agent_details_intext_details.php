<?php

// if( !function_exists('wpestate_estate_property_design_agent_details_intext_details') ):
function wpestate_estate_property_design_agent_details_intext_details($attributes,$content = null){
    global $post;
    global $propid;
    $return_string  = '';
    $maxwidth       = '';
    $margin         = '';
    $image_no       = '';
    $css_class      = '';
    $detail			= '';
    $detail			= $content;
    
    extract(shortcode_atts(array(
            'css'              =>   '',
    ), $attributes));
    $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ),'', $attributes );
    
    $agent_id       = intval( get_post_meta($propid, 'property_agent', true) );
    $author_id      = wpsestate_get_author($propid);

	// 2018-08-06 // Lorenzo M. 
	if(empty($agent_id)) $agent_id = $author_id;
	
    $agent_single_details = array(
        'Name'          =>  'name',
        'Description'   =>  'description',
        'Main Image'    =>  'image',
        'Page Link'     =>  'page_link',
        'Agent Skype'   =>  'agent_skype',
        'Agent Phone'   =>  'agent_phone',
        'Agent Mobile'  =>  'agent_mobile',
        'Agent email'   =>  'agent_email',
        'Agent position'                =>  'agent_position',
        'Agent Facebook'                =>  'agent_facebook',
        'Agent Twitter'                 =>  'agent_twitter',
        'Agent Linkedin'                => 'agent_linkedin',
        'Agent Pinterest'               => 'agent_pinterest',
        'Agent Instagram'               => 'agent_instagram',
        'Agent Website'                 => 'agent_website',
        'Agent Category'                => 'property_category_agent', 
        'Agent action category'         => 'property_action_category_agent', 
        'Agent city category'           => 'property_city_agent',
        'Agent Area category'           => 'property_area_agent',
        'Agent County/State category'   => 'property_county_state_agent'
    );
    preg_match_all("/\{[^\}]*\}/", $detail, $matches);
    $return_string =    '<div class="wpestate_estate_property_design_agent_details_intext_details '.$css_class.'"> '; 

        
    foreach($matches[0] as $key=>$value)
	{
		$replace = '';
        $element = substr($value, 1);
        $element = substr($element, 0, -1);
     
        if($element == 'name')
		{
            $replace = get_the_title($agent_id);
			
			// 2018-08-30 Lorenzo M.
			if(empty($replace))
			{
				$first_name = get_the_author_meta( 'first_name' , $author_id );
				$last_name = get_the_author_meta( 'last_name' , $author_id );
				$replace = trim($first_name.' '.$last_name);				
			}
			// <<<
	    }
		else if($element == 'description')
		{
            //$replace=get_the_content($agent_id);
            ob_start();
            $page_object = get_post( $agent_id );
            echo $page_object->post_content;
            $replace=  ob_get_contents();
            ob_end_clean();
        }
		else if($element == 'image')
		{
            $preview            = wp_get_attachment_image_src(get_post_thumbnail_id($agent_id), 'property_listings');
            $preview_img        = $preview[0];
            if($preview_img == '')
			{
               $preview_img  = WPESTATE_PLUGIN_DIR_URL.'img/default_user.png';
            }
            $replace = '<img src="'.$preview_img.'" alt="'.get_the_title($agent_id).'">';
        }
		
		// 2018-08-06 Lorenzo M. >>>
		else if($element == 'image_cover')
		{
            $replace = iklvb_get_user_image_cover($author_id);
        }
		// <<<
		
		else if($element == 'page_link')
		{
			$replace = get_the_permalink($agent_id);
            if(empty($replace)) $replace = '#';
        }
		else if($element == 'property_category_agent' || $element == 'property_action_category_agent' || $element == 'property_city_agent' || $element == 'property_area_agent' || $element == 'property_county_state_agent')
		{
            $replace = get_the_term_list($agent_id, $element, '', ', ', '') ;
        }
		
		// 2018-08-06 // Lorenzo M. >>>
		else if($element == 'property_creation_date')
		{
			$replace = get_the_date('d F Y');
		}
		// <<<
				
		else
		{
            $replace = esc_html( get_post_meta($agent_id, $element, true) );
        }
            
     
        $detail = str_replace($value,$replace,$detail);
    }
    
    

    $return_string .= do_shortcode( $detail ).'</div>'; 
    return $return_string;
    
    
    
    
}
// endif;