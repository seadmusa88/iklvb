<?php

add_action( 'wp_ajax_wpestate_ajax_property_booking','wpestate_ajax_property_booking');

function wpestate_ajax_property_booking()
{
	// error_reporting(-1);
	// ini_set('display_errors', 'On');


	// controllo fake submit
	if(!wp_verify_nonce($_POST['nonce'],'ajax-property-contact')) exit("No naughty business please");

	// check for POST vars
	$property_id	= intval($_POST['property_id']);
	$schedule_day	= strval($_POST['schedule_day']);
	$schedule_hour	= strval($_POST['schedule_hour']);
	$ticket_number	= strval($_POST['ticket_number']);

	// init
	$response = array(
		'type' 		=> 'error',
		'message' 	=> '',
		'focus'		=> ''
	);

	
	// utente loggato che sta eseguendo la richiesta
	$current_user	= wp_get_current_user();   
	$user_id		= $current_user->ID;
	
	
	
	
	
	// carico / ricavo il valore delle categoria / action della property
	$prop_action_type = '';
	$prop_action_category = get_the_terms($property_id,'property_action_category');
	if(isset($prop_action_category[0]))
	{
		switch($prop_action_category[0]->slug)
		{
			case 'for-sale':	$prop_action_type = 'for_sale';
								break;

			case 'for-rent':	$prop_action_type = 'for_rent';
								break;

			case 'evento':		
			case 'event':		$prop_action_type = 'event';
								break;
		}
	}	
	
	// ottengo id dell'utente proprietario (user)
	// e controllo: se non è stato indicato l'utente allora considero l'autore del post
	// (non dovrebbe essere mai necessario)
	$property_user_id = get_post_meta($property_id, 'property_user', true);
	if(empty($property_user_id)) $property_user_id = get_post_meta($property_id, 'original_author',	true);
	
	// controllo per impedire la prenotazione da parte del proprietario dell'immobile stesso
	// non dovrebbe mai uscire
	if($property_user_id == $user_id)
	{
		$response['message'] = __('<strong>ERROR</strong>: Action not allowed, you are the owner of this property.','iklvb-wpresidence');
		wp_send_json($response);
		exit;
	}
	
	
	// controllo se sono presenti altri biglietti in corso per questa property / utente
	$ticket_obj = new Wpw_Pcv_Public();
	$assign_prop_user_result = $ticket_obj->wpw_pcv_voucher_assign_prop_user($property_id,$user_id);

	if($assign_prop_user_result->post_count > 0)
	{
		$response['message'] = __('<strong>ERROR</strong>: You already have a ticket in progress for this property, please reload the page and check.','iklvb-wpresidence');
	}

	// controllo campi compilati
	elseif($prop_action_type != 'event' && empty($schedule_day))
	{
		$response['message'] = __('<strong>ERROR</strong>: You need select a date.','iklvb-wpresidence');
		// $response['focus'] = 'schedule_day';	// niente focus... altrimenti si apre subito il calendario e crea confusione... non si vede il messaggio di errore
	}

	elseif($prop_action_type != 'event' && empty($schedule_hour))
	{
		$response['message'] = __('<strong>ERROR</strong>: You need select a time.','iklvb-wpresidence');
		$response['focus'] = 'schedule_hour';
	}

	elseif(empty($ticket_number))
	{
		$response['message'] = __('<strong>ERROR</strong>: You need insert a ticket number.','iklvb-wpresidence');
		$response['focus'] = 'ticket_number';
	}

	if(strlen($response['message']))
	{
		wp_send_json($response);
		exit;
	}


	// controllo la validità del biglietto 
	$_POST['voucode'] = $ticket_number;
	$vou_voucher = new WOO_Vou_Voucher();
	$vou_voucher_results = $vou_voucher->woo_vou_check_voucher_code();

	if(isset($vou_voucher_results['error']) && strlen($vou_voucher_results['error']))
	{
		$response['message'] = __('<strong>ERROR</strong>: ','iklvb-wpresidence').$vou_voucher_results['error'];
	}

	elseif(isset($vou_voucher_results['used']) && strlen($vou_voucher_results['used']))
	{
		$response['message'] = __('<strong>ERROR</strong>: Ticket number has already been used.','iklvb-wpresidence');
	}

	elseif(strlen($vou_voucher_results['success']) && $vou_voucher_results['expire'] == true)
	{
		$response['message'] = __('<strong>ERROR</strong>: Ticket number was expired.','iklvb-wpresidence');
	}

	if(strlen($response['message']))
	{
		wp_send_json($response);
		exit;
	}
	

	// verifico la richiesta di assegnazione del biglietto alla property
	if(strlen($vou_voucher_results['success']))
	{
		// preparo alcuni valori da passare alla classe che controlla il biglietto
		$_POST['user_id'] = $user_id;
		$_POST['prop_id'] = $property_id;
		$_POST['operation'] = 'add_property';
		
		// controllo che il biglietto non sia stato usato per altri annunci
		$pcv_public = new Wpw_Pcv_Public();
		$pcv_public_results = $pcv_public->wpw_pcv_add_ticket_property();

		if($pcv_public_results['type'] == 'error')
		{
			$response['message'] = $pcv_public_results['message'];
		}
	}

	if(strlen($response['message']))
	{
		wp_send_json($response);
		exit;
	}

	// prex($pcv_public_results);
	
	
	
	

	// in caso di risposta affermativa procedo con l'invio del messaggio di avviso al proprietario dell'immobile >>>
	if(strlen($vou_voucher_results['success']) && $pcv_public_results['type'] == 'success')
	{
		// destinatario del messaggio
		// email del proprietario dell'annuncio
		$receiver_email = iklvb_get_email_owner_from_property_id($property_id);
		
		// preparo le parti del messaggio		
		$args = array(
			'visitor_id'	=> $user_id,
			'owner_id'		=> $property_user_id,
			'property_id'	=> $property_id,
			'schedule_time'	=> $_POST['schedule_day'].' '.$_POST['schedule_hour']
		);
		
		// messaggio a seconda del tipo di biglietto
		$email_message_slug = ($prop_action_type == 'event' ? 'event-new-request-visitor-to-owner' : 'tour-new-request-visitor-to-owner');
		$iklvb_email = iklvb_get_email_parts($email_message_slug,$args);

		// contenuto email in alto a destra (destinatario)
		global $email_header_right;
		$email_header_right = iklvb_get_user_agent_email_to_box($property_user_id);		
		
		// intestazione
		$headers = array(
			'Content-Type: text/html; charset=UTF-8',
			'From:iKLVB <notification@iklvb.com>'
		);

		// invio della email
		wp_mail($receiver_email, $iklvb_email['subject'], $iklvb_email['message'], $headers);
		

		// messaggio di conferma
		$response['type'] = 'success';
		$response['message']  = $pcv_public_results['message'].'<br>';
		$response['message'] .= __('Your request has been notified to the advertiser, you will receive an e-mail reply as soon as possible.','iklvb-wpresidence');
		
		wp_send_json($response);
		exit;
	}
	// <<<
}
