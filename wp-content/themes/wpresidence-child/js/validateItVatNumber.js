/**
 * source:
 * https://gist.github.com/heartear/2d4ce3db7cbeebfa31d8
 * 
 * @param {type} value
 * @returns {Boolean}
 * 
 */


validateVatNumber = function(value) 
{
	// 2018-08-29 Lorenzo M. 
	// al cliente NON serve questo controllo al momento
	return true;
	
	
	
	var VAT_NUMBER_LENGTH = 11;

	var validVat = false;

	if(value && value.length == VAT_NUMBER_LENGTH) {

		var x = 0;
		var y = 0;

		for(var position = 0; position < VAT_NUMBER_LENGTH; ++position) {
			if(((position + 1) % 2) > 0) {
				x += parseInt(value.charAt(position));
			}
			else {
				var double = parseInt(value.charAt(position)) * 2;

				if(double > 9) {
					double -= 9;
				}

				y += double;
			}
		}

		if((x + y) % 10 == 0) {
			validVat = true;
		}
	}

	return validVat;
};
