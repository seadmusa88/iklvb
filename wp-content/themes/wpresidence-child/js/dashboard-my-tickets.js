/**
 * 2018-08-23 Lorenzo M.
 * 
 * funzioni custom per la gestione di biglietti nella pagina my-tickets
 * 
 */


jQuery(document).ready(function ($) 
{
	"use strict";

	// init
	init_datepicker();
	
	// observer 
	$('.row_dasboard-prop-listing').on('click','.tickets-listing .tickets-toggle-wrapper',tickets_details_toggle);
	$('.row_dasboard-prop-listing').on('click','.tickets-listing .row-actions .wpresidence_button',tickets_action_button);
	
	
	
	// controllo se nella pagina ci sono dei campi input da gestire con jQuery.datepicker
	function init_datepicker()
	{
		$('.schedule_day_custom').each( function() 
		{
			var date_format = $(this).attr('date-format');
			if(date_format === '') date_format = 'yy-mm-dd';

			$( this ).datepicker( 
			{
				dateFormat: date_format,
				minDate: new Date( $( this ).attr( 'min' ) ),
				maxDate: new Date( $( this ).attr( 'max' ) )
			}).datepicker('widget').wrap('<div class="ll-skin-melon"/>');
		});
	}	
	
	
	
	
	// gestione pulsanti "action" per singolo biglietto >>>
	function tickets_action_button(e)
	{
		// leggo i valori dalla pagina
		var id = $(this).attr('data-id');
		var action = $(this).attr('data-action');
		var row = $('#row-' + id);
		
		// se è stato cliccato su download allora non serve eseguire JS
		if(action === 'download') return;
		
		// impedisco il clik sul link (tag <a>)
		e.stopImmediatePropagation();
		e.preventDefault();
		
		// nascondo eventuali messaggi visualizzati in precedenza
		row.find('.alert-ok').hide();
		row.find('.alert-error').hide();
		
		
		// conferma della data 
		if(action === 'confirm')
		{
			// visualizzo il loader per far capire che la pagina sta lavorando
			row.find('.image-loader').show();
			
			// chiamata ajax
			$.ajax(
			{
				type	: 'POST',
				dataType: 'json',
				url		: WPWPCV.ajaxurl,
				data: {
					action			:   'wpestate_ajax_property_booking_confirm',
					action_from		:	'visitor',
					ticket_id		:	id,
					nonce			:   row.find('.ajax_ticket_nonce').val()
				},
				success: function(response) 
				{ 				
					if(response.type === 'success') 
					{
						update_ticket_listing_row(id,response.message);
					}
					else if(response.type === 'error')
					{
						row.find('.message_error_json').html(response.message).show();   
						row.find('.image-loader').hide();
					}
				},
				error: function() 
				{
					row.find('.message_error_default').show();				
					row.find('.image-loader').hide();
				}
			});
		}
		
		
		// visualizzo i campi per procedere alla modifica della data
		if(action === 'change-toggle')
		{
			row.find('.row-buttons').toggle();
			row.find('.row-change-data-fields').toggle();
		}
		
		
		// invio richiesta cambio data
		if(action === 'change')
		{
			// visualizzo il loader per far capire che la pagina sta lavorando
			row.find('.image-loader').show();
			
			// chiamata ajax
			$.ajax(
			{
				type	: 'POST',
				dataType: 'json',
				url		: WPWPCV.ajaxurl,
				data: {
					action			:   'wpestate_ajax_property_booking_change',
					action_from		:	'visitor',
					schedule_day	:   row.find('.schedule_day_custom').val(),
					schedule_hour	:   row.find('.schedule_hour').val(),
					ticket_id		:	id,
					nonce			:   row.find('.ajax_ticket_nonce').val()
				},
				success: function(response) 
				{ 				
					if(response.type === 'success') 
					{
						update_ticket_listing_row(id,response.message);
					}
					else if(response.type === 'error')
					{
						row.find('.message_error_json').html(response.message).show();   
						row.find('.image-loader').hide();
					}
				},
				error: function() 
				{
					row.find('.message_error_default').show();				
					row.find('.image-loader').hide();
				}
			});			
		}	
		
		
		// visualizzo i campi per procedere alla cancellazione della data
		if(action === 'cancel-toggle')
		{
			row.find('.row-buttons').toggle();
			row.find('.row-confirm-cancel').toggle();
		}
		
		
		// cancello la data, e scollego il biglietto dalla proprietà
		if(action === 'cancel')
		{
			// visualizzo il loader per far capire che la pagina sta lavorando
			row.find('.image-loader').show();
			
			// chiamata ajax
			$.ajax(
			{
				type	: 'POST',
				dataType: 'json',
				url		: WPWPCV.ajaxurl,
				data: {
					action			:   'wpestate_ajax_property_booking_cancel',
					action_from		:	'visitor',
					ticket_id		:	id,
					nonce			:   row.find('.ajax_ticket_nonce').val()
				},
				success: function(response) 
				{ 				
					if(response.type === 'success') 
					{
						update_ticket_listing_row(id,response.message);
					}
					else if(response.type === 'error')
					{
						row.find('.message_error_json').html(response.message).show();   
						row.find('.image-loader').hide();
					}
				},
				error: function() 
				{
					row.find('.message_error_default').show();				
					row.find('.image-loader').hide();
				}
			});	
		}	
    }

	
	
	
	function update_ticket_listing_row(id,message)
	{
		var row = $('#row-' + id);
		
		$.ajax(
		{
			type	: 'POST',
			dataType: 'html',
			url		: WPWPCV.ajaxurl,
            data: {
                action			:   'wpestate_ajax_update_ticket_listing_single',
                ticket_id		:	id,
                my_page			:	'tickets'
            },
            success: function(response) 
			{ 				
				row.replaceWith(response);
				
				var new_row = $('#row-' + id);
				new_row.find('.message_ok_json').html(message).show();
				
				// debugger;
				
				init_datepicker();
		    },
            error: function() 
			{
				row.find('.message_error_default').show();				
				row.find('.image-loader').hide();
            }	
		});
	}
		
	
	
	/**
	 * toggle della riga con le info aggiuntive del biglietto
	 * 
	 * @returns {undefined}
	 */
	function tickets_details_toggle()
	{
		var row = $('#row-' + $(this).attr('data-id'));
		row.find('.tickets-details').toggle();
	}

});