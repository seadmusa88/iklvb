/**
 * js pr la gestione dei campi aggiuntivi in property category
 * 
 * tipo biglietto
 * data evento
 * numero invitati massimo
 * 
 */

jQuery(document).ready(function ($) 
{
	"use strict";
	
	// init
	init_datepicker();
	
	// observer 
	$('.page-template').on('change','#prop_action_category_submit',event_fields_toggle);
	
	
	
	
	// controllo se nella pagina ci sono dei campi input da gestire con jQuery.datepicker
	function init_datepicker()
	{
		$('.schedule_day_custom').each( function() 
		{
			var date_format = $(this).attr('date-format');
			if(date_format === '') date_format = 'yy-mm-dd';

			$( this ).datepicker( 
			{
				dateFormat: date_format,
				minDate: new Date( $( this ).attr( 'min' ) ),
				maxDate: new Date( $( this ).attr( 'max' ) )
			}).datepicker('widget').wrap('<div class="ll-skin-melon"/>');
		});
	}
	
	
	
	function event_fields_toggle()
	{
		var val = $(this).find('option:selected').text().toLowerCase();
		
		// debugger;
		
		if(val === 'event' || val === 'evento')
		{
			$('.event_fields').show();
			$('.property_label_wrapper').hide();
		}
		else if(val === 'for rent' || val === 'in affitto')
		{
			$('.event_fields').hide();
			$('.property_label_wrapper').show();			
		}
		else
		{
			$('.event_fields').hide();			
			$('.property_label_wrapper').hide();
		}
	}
});