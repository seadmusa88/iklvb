
// front property submit page-template-front_property_submit
jQuery(document).ready(function ($)
{
	
    jQuery('#loginpop').val('3');
	
	$('.page-template-front_property_submit #submit_property').click(function(event)
	{
		event.preventDefault();

		if (parseInt(ajaxcalls_vars.userid, 10) === 0 ) {
			jQuery('.login-links').hide();
			jQuery("#modal_login_wrapper").show(); 
		}else{
			$('#front_submit_form').submit();
		}
	});

        
        
        
	$('#front_end_submit_register').click(function()
	{
		"use strict";
		var post_id, securitypass, ajaxurl;
		securitypass    =  jQuery('#security-pass').val();
		ajaxurl         =  ajaxcalls_vars.admin_url + 'admin-ajax.php';

		if (parseInt(ajaxcalls_vars.userid, 10) === 0 ) 
		{
			if (!Modernizr.mq('only all and (max-width: 768px)')) {
				jQuery('#modal_login_wrapper').show(); 
				jQuery('#loginpop').val('2');
			}else{
				jQuery('.mobile-trigger-user').trigger('click');
			}
		} 
		
		jQuery('#loginpop').val('2');
	});
        
        
	// inner navigation processing
	/*
	$('.inner_navigation').click(function(e)
	{
		e.preventDefault();
		var current_step = parseInt( $('.page-template-front_property_submit #current_step').val() );	
		$('.page-template-front_property_submit .step_'+current_step).hide();
		
		var id = parseInt( $(this).attr('data-id') );
		
		$('.page-template-front_property_submit .step_'+id).fadeIn();
		$('.page-template-front_property_submit .inner_navigation').removeClass('active');
		$(this).addClass('active');
		$('.page-template-front_property_submit #current_step').val( id );
		
		if( id < 7 )
		{
			$('#front_submit_prev_step').show();
			$('#front_submit_next_step').show();
			google.maps.event.trigger(map, 'resize');
			$('.page-template-front_property_submit #submit_property').hide();
		}
		
		if( id === 5 )
		{          
			$('#front_submit_next_step').hide();
			$('.page-template-front_property_submit #submit_property').show();
//                        if (parseInt(ajaxcalls_vars.userid, 10) === 0 ) {
//                            jQuery("#modal_login_wrapper").show(); 
//                        }
		}
		
		if( id === 1 )
		{
        	$('#front_submit_prev_step').hide();
			$('#front_submit_next_step').show();
			$('.page-template-front_property_submit #submit_property').hide();
		}
	});
	*/
   
	// process next step action
	$('#front_submit_next_step').click(function()
	{
		var flag_error = false;
		var current_step = parseInt( $('.page-template-front_property_submit #current_step').val() );	
               
		if( current_step === 1 )
		{
			if($('#title').val() === '')
			{	
				$('#title').addClass('required');
				flag_error = true;
			}
			
			if($('#property_price').val() === '' || isPositiveInteger($('#property_price').val()) === false)
			{	
				$('#property_price').addClass('required');
				flag_error = true;
			}
		}
		
		if( current_step === 2 )
		{
			if($('.uploaded_images').length  === 0)
			{
				$('.full_form_image').addClass('required');
				flag_error = true;
			}
			else
			{
				$('.full_form_image').removeClass('required');
			}
		}	
		
		if( current_step === 3 )
		{
			if($('#property_address').val() === '')
			{	
				$('#property_address').addClass('required');
				flag_error = true;
			}
		}
		
		if( current_step === 4 )
		{
			if($('#property_size').val() === '' || isPositiveInteger($('#property_size').val()) === false)
			{	
				$('#property_size').addClass('required');
				flag_error = true;
			}
			
			if($('#property_rooms').val() === '' || isPositiveInteger($('#property_rooms').val()) === false)
			{	
				$('#property_rooms').addClass('required');
				flag_error = true;
			}
			
			if($('#property_bedrooms').val() === '' || isPositiveInteger($('#property_bedrooms').val()) === false)
			{	
				$('#property_bedrooms').addClass('required');
				flag_error = true;
			}
			
			if($('#property_bathrooms').val() === '' || isPositiveInteger($('#property_bathrooms').val()) === false)
			{	
				$('#property_bathrooms').addClass('required');
				flag_error = true;
			}
		}
		
			
		if( flag_error === false )
		{
			if( current_step < 5 )
			{
				$('.page-template-front_property_submit .step_'+current_step).hide();
				current_step++;

				// innner navigaton
				$('.page-template-front_property_submit .inner_navigation').removeClass('active');
				$('.page-template-front_property_submit .navigation_'+current_step).addClass('active');

				$('.page-template-front_property_submit #current_step').val( current_step );
				$('.page-template-front_property_submit .step_'+current_step).show();
				$('#front_submit_prev_step').show();
				google.maps.event.trigger(map, 'resize');
				$('.page-template-front_property_submit #submit_property').hide();
			}

			if( current_step === 5 )
			{
				$('#front_submit_next_step').hide();
				$('.page-template-front_property_submit #submit_property').show();
				//  if (parseInt(ajaxcalls_vars.userid, 10) === 0 ) {
				//		jQuery("#modal_login_wrapper").show(); 
				//	}
			}
		}
	});
	
	// process prev step action
	$('#front_submit_prev_step').click(function()
	{
		var current_step = parseInt( $('.page-template-front_property_submit #current_step').val() );
         
		if( current_step <= 5 )
		{
			$('.page-template-front_property_submit .step_'+current_step).hide();
			current_step--;
			
			// innner navigaton
			$('.page-template-front_property_submit .inner_navigation').removeClass('active');
			$('.page-template-front_property_submit .navigation_'+current_step).addClass('active');
			$('.page-template-front_property_submit #current_step').val( current_step );
			$('.page-template-front_property_submit .step_'+current_step).show();
			$('#front_submit_next_step').show();
			google.maps.event.trigger(map, 'resize');
            $('.page-template-front_property_submit #submit_property').hide();
		}
		
		if( current_step === 1 )
		{
			$('#front_submit_prev_step').hide();
			$('#front_submit_next_step').show();
			$('.page-template-front_property_submit #submit_property').hide();
		}
	});
	
	// login link / register link swap fn
	$('#register_link').click(function(e)
	{
		e.preventDefault();
		$('.page-template-front_property_submit #register_link').hide();
		$('.page-template-front_property_submit #login_link').show();
		
		$('.page-template-front_property_submit .register_row').show();
		$('.page-template-front_property_submit .login_row').hide();
		$('#submit_type').val('register');
	});
	
	$('#login_link').click(function(e)
	{
		e.preventDefault();
		$('.page-template-front_property_submit #register_link').show();
		$('.page-template-front_property_submit #login_link').hide();
		
		$('.page-template-front_property_submit .register_row').hide();
		$('.page-template-front_property_submit .login_row').show();
		$('#submit_type').val('login');
	});
	
	
	
	
	
	$('form').on('keypress', 'input.required', remove_required_class);
	$('form').on('change', 'select.required', remove_required_class);
	$('form').on('change', 'textarea.required', remove_required_class);
	
	function remove_required_class()
	{
		$(this).removeClass('required');
	}
	
	/**
	 * source:
	 * https://stackoverflow.com/questions/13219926/check-for-positive-integers-in-javascript-performance-and-security
	 * 
	 * @param {type} s
	 * @returns {Boolean}	 * 
	 */
	function isPositiveInteger(s)
	{
		var i = +s; // convert to a number
		if (i <= 0) return false; // make sure it's positive
		if (i != ~~i) return false; // make sure there's no decimal part
		return true;
	}
	
});

function validateEmail(email) 
{ 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


// front property submit page-template-front_property_submit END
