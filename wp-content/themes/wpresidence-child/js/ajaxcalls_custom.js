/**
 * 2018-07-16 Lorenzo M.
 * 
 * funzioni custom per la gestione dei campi personalizzati degli utenti
 * 
 */


jQuery(document).ready(function ($) 
{
	"use strict";
	
    $('#update_profile_custom').click(function () 
	{
        var firstname       =  $('#firstname').val();
        var secondname      =  $('#secondname').val();
        var useremail       =  $('#useremail').val();
        var userphone       =  $('#userphone').val();
        var usermobile      =  $('#usermobile').val();
        var userskype       =  $('#userskype').val();
        var usertitle       =  $('#usertitle').val();
        var description     =  $('#about_me').val();
        var userfacebook    =  $('#userfacebook').val();
        var usertwitter     =  $('#usertwitter').val();
        var userlinkedin    =  $('#userlinkedin').val();
        var userpinterest   =  $('#userpinterest').val();
        var userinstagram   =  $('#userinstagram').val();
        var userurl         =  $('#website').val();
        // var agent_member    =  $('#agent_member').val();
        var agent_member    =  '';
		
		
		var user_billing_first_name  =  $('#user_billing_first_name').val();
		var user_billing_last_name   =  $('#user_billing_last_name').val();
        var user_billing_address_1   =  $('#user_billing_address_1').val();
        var user_billing_city		 =  $('#user_billing_city').val();
        var user_billing_postcode    =  $('#user_billing_postcode').val();
        var user_billing_country	 =  $('#user_billing_country').val();

		
        var user_billing_state		 =  $('#user_billing_state').val();
        var user_billing_fiscal_code =  $('#user_billing_fiscal_code').val();
		
		
        var agent_category_submit	 =  $('#agent_category_submit').val();
        var agent_action_submit		 =  $('#agent_action_submit').val();
        var agent_city				 =  $('#agent_city').val();
        var agent_county			 =  $('#agent_county').val();
        var agent_area				 =  $('#agent_area').val();    
        
        var ajaxurl					 =  ajaxcalls_vars.admin_url + 'admin-ajax.php';
        var securityprofile			 =  $('#security-profile').val();
        var upload_picture			 =  $('#upload_picture').val();
        var profile_image_url		 =  $('#profile-image').attr('data-profileurl');
        var profile_image_url_small  =  $('#profile-image').attr('data-smallprofileurl');
       
        // customparameters
  		var agent_custom_label = [];
  		$('.agent_custom_label').each(function()
		{
  			agent_custom_label.push( $(this).val() );
  		});
  	 
  		var agent_custom_value = [];
  		$('.agent_custom_value').each(function()
		{
  			agent_custom_value.push( $(this).val() );
  		});
       
	   
	   
	   
	   
	    // controllo errori
		var errors_count = 0;
		
		// nascondo eventuali messaggi visualizzati in precedenza
		$('.alert-ok').hide();
		$('.alert-error').hide();
		$('#image-loader').hide();
		
		// vado ad inizio pagina
		window.scrollTo(0,0);

		// controllo campi vuoti
		if(		firstname === ''
			||	secondname === ''
			||	useremail === ''
			||	userphone === ''
			||	user_billing_first_name === ''
			||	user_billing_last_name === ''
			||	user_billing_address_1 === ''
			||	user_billing_city === ''
			||	user_billing_postcode === ''
			||	user_billing_country === ''
			||	user_billing_state === ''
			||	user_billing_fiscal_code === '' )
		{
			errors_count++;
			$('#profile_message #profile_empty_fields').show();
		}
		
		// controllo campi particolari 
		if(validateEmail(useremail) === false)
		{
			errors_count++;
			$('#profile_message #profile_invalid_user_email').show();
		}
		
		// controllo campi particolari 
		if(validateTaxCode(user_billing_fiscal_code) === false)
		{
			errors_count++;
			$('#profile_message #profile_invalid_fiscal_code').show();
		}
		
		// visualizzo eventuali errori e fermo lo script
		if(errors_count > 0) return; 
		
		// visualizzo il loader per far capire che la pagina sta salvando
		$('#image-loader').show();
		
		
		
		
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                'action'            :   'wpestate_ajax_update_profile',
                'firstname'         :   firstname,
                'secondname'        :   secondname,
                'useremail'         :   useremail,
                'userphone'         :   userphone,
                'usermobile'        :   usermobile,
                'userskype'         :   userskype,
                'usertitle'         :   usertitle,
                'description'       :   description,
                'upload_picture'    :   upload_picture,
                'security-profile'  :   securityprofile,
                'profile_image_url' :   profile_image_url,
                'profile_image_url_small':profile_image_url_small,
                'userfacebook'      :   userfacebook,
                'usertwitter'       :   usertwitter,
                'userlinkedin'      :   userlinkedin,
                'userpinterest'     :   userpinterest,
                'userinstagram'     :   userinstagram,
                'userurl'           :   userurl,
				
				'user_billing_first_name'   :   user_billing_first_name,
				'user_billing_last_name'    :   user_billing_last_name,
                'user_billing_address_1'    :   user_billing_address_1,
                'user_billing_city'			:   user_billing_city,
                'user_billing_postcode'		:   user_billing_postcode,
                'user_billing_country'		:   user_billing_country,
                'user_billing_state'		:   user_billing_state,
                'user_billing_fiscal_code'  :   user_billing_fiscal_code,
				
                'agent_category_submit'     :   agent_category_submit,
                'agent_action_submit'       :   agent_action_submit,
                'agent_city'                :   agent_city,
                'agent_county'              :   agent_county,
                'agent_area'                :   agent_area,
                'agent_member'              :   agent_member,
        
                'agent_custom_label'		: agent_custom_label,
				'agent_custom_value'		: agent_custom_value
        
            },
            success: function (data) 
			{            
                $('#profile_message #profile_update_ok').show();
				$('#image-loader').hide();
            },
            error: function (errorThrown) 
			{
                $('#profile_message #profile_update_error').show();
				$('#image-loader').hide();
            }
        });
    });







    
    $('#update_profile_agent_custom').click(function () 
	{
        var firstname       =  $('#firstname').val();
        var secondname      =  $('#secondname').val();
        var useremail       =  $('#useremail').val();
        var userphone       =  $('#userphone').val();
		var usermobile      =  $('#usermobile').val();
        var userskype       =  $('#userskype').val();
        var usertitle       =  $('#usertitle').val();
        var description     =  $('#about_me').val();
        var userfacebook    =  $('#userfacebook').val();
        var usertwitter     =  $('#usertwitter').val();
        var userlinkedin    =  $('#userlinkedin').val();
        var userpinterest   =  $('#userpinterest').val();
        var userinstagram   =  $('#userinstagram').val();
        var userurl         =  $('#website').val();
        // var agent_member    =  $('#agent_member').val();
        var agent_member    =  '';
		
		
		var user_billing_company	 =  $('#user_billing_company').val();
		var user_billing_address_1   =  $('#user_billing_address_1').val();
        var user_billing_city		 =  $('#user_billing_city').val();
        var user_billing_postcode    =  $('#user_billing_postcode').val();
        var user_billing_country	 =  $('#user_billing_country').val();
		var user_billing_state		 =  $('#user_billing_state').val();
		var user_billing_fiscal_code =  $('#user_billing_fiscal_code').val();
        var user_billing_vat		 =  $('#user_billing_vat').val();
		
		
        var agent_category_submit	 =  $('#agent_category_submit').val();
        var agent_action_submit		 =  $('#agent_action_submit').val();
        var agent_city				 =  $('#agent_city').val();
        var agent_county			 =  $('#agent_county').val();
        var agent_area				 =  $('#agent_area').val();    
        
        var ajaxurl					 =  ajaxcalls_vars.admin_url + 'admin-ajax.php';
        var securityprofile			 =  $('#security-profile').val();
        var upload_picture			 =  $('#upload_picture').val();
        var profile_image_url		 =  $('#profile-image').attr('data-profileurl');
        var profile_image_url_small  =  $('#profile-image').attr('data-smallprofileurl');
       
        // customparameters
  		var agent_custom_label = [];
  		$('.agent_custom_label').each(function()
		{
  			agent_custom_label.push( $(this).val() );
  		});
  	 
  		var agent_custom_value = [];
  		$('.agent_custom_value').each(function()
		{
  			agent_custom_value.push( $(this).val() );
  		});
       
	   
	 
	   
	   
	    // controllo errori
		var errors_count = 0;
		
		// nascondo eventuali messaggi visualizzati in precedenza
		$('.alert-ok').hide();
		$('.alert-error').hide();
		$('#image-loader').hide();
		
		// vado ad inizio pagina
		window.scrollTo(0,0);

		// controllo campi vuoti
		if(		firstname === ''
			||	secondname === ''
			||	useremail === ''
			||	userphone === ''
			||	user_billing_company === ''
			||	user_billing_address_1 === ''
			||	user_billing_city === ''
			||	user_billing_postcode === ''
			||	user_billing_country === ''
			||	user_billing_state === ''
			||	user_billing_fiscal_code === '' )
		{
			errors_count++;
			$('#profile_message #profile_empty_fields').show();
		}
		
		// controllo campi particolari 
		if(validateEmail(useremail) === false)
		{
			errors_count++;
			$('#profile_message #profile_invalid_user_email').show();
		}
		
		// controllo campi particolari 
		if(validateTaxCode(user_billing_fiscal_code) === false)
		{
			errors_count++;
			$('#profile_message #profile_invalid_fiscal_code').show();
		}
		
		// controllo campi particolari 
		if(user_billing_vat !== '' && validateVatNumber(user_billing_vat) === false)
		{
			errors_count++;
			$('#profile_message #profile_invalid_vat').show();
		}		
		
		// visualizzo eventuali errori e fermo lo script
		if(errors_count > 0) return; 
		
		// visualizzo il loader per far capire che la pagina sta salvando
		$('#image-loader').show();
		
		
		
		
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                'action'            :   'wpestate_ajax_update_profile_agent',
                'firstname'         :   firstname,
                'secondname'        :   secondname,
                'useremail'         :   useremail,
                'userphone'         :   userphone,
				'usermobile'        :   usermobile,
                'userskype'         :   userskype,
                'usertitle'         :   usertitle,
                'description'       :   description,
                'upload_picture'    :   upload_picture,
                'security-profile'  :   securityprofile,
                'profile_image_url' :   profile_image_url,
                'profile_image_url_small':profile_image_url_small,
                'userfacebook'      :   userfacebook,
                'usertwitter'       :   usertwitter,
                'userlinkedin'      :   userlinkedin,
                'userpinterest'     :   userpinterest,
                'userinstagram'     :   userinstagram,
                'userurl'           :   userurl,
				
				'user_billing_company'		:   user_billing_company,
			    'user_billing_address_1'    :   user_billing_address_1,
                'user_billing_city'			:   user_billing_city,
                'user_billing_postcode'		:   user_billing_postcode,
                'user_billing_country'		:   user_billing_country,
                'user_billing_state'		:   user_billing_state,
				'user_billing_fiscal_code'  :   user_billing_fiscal_code,
                'user_billing_vat'			:   user_billing_vat,
				
                'agent_category_submit'     :   agent_category_submit,
                'agent_action_submit'       :   agent_action_submit,
                'agent_city'                :   agent_city,
                'agent_county'              :   agent_county,
                'agent_area'                :   agent_area,
                'agent_member'              :   agent_member,
        
                'agent_custom_label'		: agent_custom_label,
				'agent_custom_value'		: agent_custom_value
        
            },
            success: function (data) 
			{            
                $('#profile_message #profile_update_ok').show();
				$('#image-loader').hide();
            },
            error: function (errorThrown) 
			{
                $('#profile_message #profile_update_error').show();
				$('#image-loader').hide();
            }
        });
    });







    
    $('#update_profile_agency_custom').click(function () 
	{
        var firstname       =  $('#firstname').val();
        var secondname      =  $('#secondname').val();
		
		var agency_name     =  $('#agency_title').val();
        var useremail       =  $('#useremail').val();
        var userphone       =  $('#userphone').val();
        var usermobile      =  $('#usermobile').val();
        var userskype       =  $('#userskype').val();
  
        var description     =  $('#about_me').val();
        var userfacebook    =  $('#userfacebook').val();
        var usertwitter     =  $('#usertwitter').val();
        var userlinkedin    =  $('#userlinkedin').val();
        var userpinterest   =  $('#userpinterest').val();
        var userinstagram   =  $('#userinstagram').val();      
        
        var agency_languages=  $('#agency_languages').val();
        var agency_website  =  $('#agency_website').val();
        // var agency_taxes    =  $('#agency_taxes').val();
        var agency_taxes    =  '';
        var agency_license  =  $('#agency_license').val();
     
 		var user_billing_company	=  $('#user_billing_company').val();
		var user_billing_address_1  =  $('#user_billing_address_1').val();
        var user_billing_city		=  $('#user_billing_city').val();
        var user_billing_postcode   =  $('#user_billing_postcode').val();
        var user_billing_country	=  $('#user_billing_country').val();
		var user_billing_state		=  $('#user_billing_state').val();
        var user_billing_vat		=  $('#user_billing_vat').val();
        
        var agency_category_submit  =  $('#agency_category_submit').val();
        var agency_action_submit    =  $('#agency_action_submit').val();
        var agency_city				=  $('#agency_city').val();
        var agency_county			=  $('#agency_county').val();
        var agency_area				=  $('#agency_area').val();
        var agency_address			=  $('#agency_address').val();
        var agency_lat				=  $('#agency_lat').val();
        var agency_long				=  $('#agency_long').val();
        var agency_opening_hours	=  $('#agency_opening_hours ').val();
        var securityprofile         =  $('#security-profile').val();
        var upload_picture          =  $('#upload_picture').val();
        var profile_image_url       =  $('#profile-image').attr('data-profileurl');
        var profile_image_url_small =  $('#profile-image').attr('data-smallprofileurl');
      
        var ajaxurl         =  ajaxcalls_vars.admin_url + 'admin-ajax.php';





      	// controllo errori
		var errors_count = 0;
		
		// nascondo eventuali messaggi visualizzati in precedenza
		$('.alert-ok').hide();
		$('.alert-error').hide();
		$('#image-loader').hide();
		
		// vado ad inizio pagina
		window.scrollTo(0,0);

		// controllo campi vuoti
		if(		firstname === ''
			||	secondname === ''
			||	useremail === ''
			||	userphone === ''
			||	agency_name === ''
			||	user_billing_company === ''
			||	user_billing_address_1 === ''
			||	user_billing_city === ''
			||	user_billing_postcode === ''
			||	user_billing_country === ''
			||	user_billing_state === ''
			||	user_billing_vat === '' )
		{
			errors_count++;
			$('#profile_message #profile_empty_fields').show();
		}
		
		// controllo campi particolari 
		if(validateEmail(useremail) === false)
		{
			errors_count++;
			$('#profile_message #profile_invalid_user_email').show();
		}
		
		// controllo campi particolari 
		if(validateVatNumber(user_billing_vat) === false)
		{
			errors_count++;
			$('#profile_message #profile_invalid_vat').show();
		}
		
		// visualizzo eventuali errori e fermo lo script
		if(errors_count > 0) return; 
		
		// visualizzo il loader per far capire che la pagina sta salvando
		$('#image-loader').show();
		
		
		
		
      
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                'action'            :   'wpestate_ajax_update_profile_agency',
				
                'firstname'			:   firstname,
                'secondname'		:   secondname,
				
                'agency_name'       :   agency_name,
                'useremail'         :   useremail,
                'userphone'         :   userphone,
                'usermobile'        :   usermobile,
                'userskype'         :   userskype,
                // 'usertitle'         :   usertitle,
                'description'       :   description,
                'upload_picture'    :   upload_picture,
                'security-profile'  :   securityprofile,
                'profile_image_url' :   profile_image_url,
                'profile_image_url_small':profile_image_url_small,
                'userfacebook'      :   userfacebook,
                'usertwitter'       :   usertwitter,
                'userlinkedin'      :   userlinkedin,
                'userpinterest'     :   userpinterest,
                'userinstagram'     :   userinstagram,
                // 'userurl'           :   userurl,
				
				'user_billing_company'		:   user_billing_company,
			    'user_billing_address_1'    :   user_billing_address_1,
                'user_billing_city'			:   user_billing_city,
                'user_billing_postcode'		:   user_billing_postcode,
                'user_billing_country'		:   user_billing_country,
                'user_billing_state'		:   user_billing_state,
                'user_billing_vat'			:   user_billing_vat,
				
                'agency_languages'			:   agency_languages,
                'agency_website'			:   agency_website,
                'agency_taxes'				:   agency_taxes,
                'agency_license'			:   agency_license,
                'agency_category_submit'	:	agency_category_submit,
                'agency_action_submit'		:	agency_action_submit,
                'agency_city'				:   agency_city,
                'agency_county'				:   agency_county,
                'agency_area'				:   agency_area,
                'agency_address'			:   agency_address,
                'agency_lat'				:   agency_lat,
                'agency_opening_hours'		:	agency_opening_hours,
                'agency_long'				:   agency_long
            },
            success: function (data) 
			{
                $('#profile_message #profile_update_ok').show();
                $('#image-loader').hide();
            },
            error: function (errorThrown) 
			{
                $('#profile_message #profile_update_error').show();
				$('#image-loader').hide();
            }
        });
    });
    
    
	
	
	
	
	
	
	
    
	//    update developer profile 
    $('#update_profile_developer_custom').click(function () 
	{
        var firstname       =  $('#firstname').val();
        var secondname      =  $('#secondname').val();
		
		var developer_name  =  $('#developer_title').val();
        var useremail       =  $('#useremail').val();
        var userphone       =  $('#userphone').val();
        var usermobile      =  $('#usermobile').val();
        var userskype       =  $('#userskype').val();
  
        var description     =  $('#about_me').val();
        var userfacebook    =  $('#userfacebook').val();
        var usertwitter     =  $('#usertwitter').val();
        var userlinkedin    =  $('#userlinkedin').val();
        var userpinterest   =  $('#userpinterest').val();
        var userinstagram   =  $('#userinstagram').val();
      
        
        var developer_languages =  $('#developer_languages').val();
        var developer_website   =  $('#developer_website').val();
        // var developer_taxes     =  $('#developer_taxes').val();
        var developer_taxes     =  '';
        var developer_license   =  $('#developer_license').val();
     
        
        var developer_category_submit  =   $('#developer_category_submit').val();
        var developer_action_submit    =   $('#developer_action_submit').val();
        var developer_city             =   $('#developer_city').val();
        var developer_county           =   $('#developer_county').val();
        var developer_area             =   $('#developer_area').val();
        var developer_address          =   $('#developer_address').val();
        var developer_lat              =   $('#developer_lat').val();
        var developer_long             =   $('#developer_long').val();
        var securityprofile             = $('#security-profile').val();
        var upload_picture              = $('#upload_picture').val();
        var profile_image_url           = $('#profile-image').attr('data-profileurl');
        var profile_image_url_small     = $('#profile-image').attr('data-smallprofileurl');
		
		var user_billing_company	=  $('#user_billing_company').val();
		var user_billing_address_1  =  $('#user_billing_address_1').val();
        var user_billing_city		=  $('#user_billing_city').val();
        var user_billing_postcode   =  $('#user_billing_postcode').val();
        var user_billing_country	=  $('#user_billing_country').val();
		var user_billing_state		=  $('#user_billing_state').val();
        var user_billing_vat		=  $('#user_billing_vat').val();
      
        var ajaxurl =  ajaxcalls_vars.admin_url + 'admin-ajax.php';
        
		
		
		
		
	    // controllo errori
		var errors_count = 0;
		
		// nascondo eventuali messaggi visualizzati in precedenza
		$('.alert-ok').hide();
		$('.alert-error').hide();
		$('#image-loader').hide();
		
		// vado ad inizio pagina
		window.scrollTo(0,0);

		// controllo campi vuoti
		if(		firstname === ''
			||	secondname === ''
			||	useremail === ''
			||	userphone === ''
			||	developer_name === ''
			||	user_billing_company === ''
			||	user_billing_address_1 === ''
			||	user_billing_city === ''
			||	user_billing_postcode === ''
			||	user_billing_country === ''
			||	user_billing_state === ''
			||	user_billing_vat === '' )
		{
			errors_count++;
			$('#profile_message #profile_empty_fields').show();
		}
		
		// controllo campi particolari 
		if(validateEmail(useremail) === false)
		{
			errors_count++;
			$('#profile_message #profile_invalid_user_email').show();
		}
		
		// controllo campi particolari 
		if(validateVatNumber(user_billing_vat) === false)
		{
			errors_count++;
			$('#profile_message #profile_invalid_vat').show();
		}
		
		// visualizzo eventuali errori e fermo lo script
		if(errors_count > 0) return; 
		
		// visualizzo il loader per far capire che la pagina sta salvando
		$('#image-loader').show();
		
		
		
		
        
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                'action'            :   'wpestate_ajax_update_profile_developer',
				
				'firstname'			:   firstname,
                'secondname'		:   secondname,
				
                'developer_name'    :   developer_name,
                'useremail'         :   useremail,
                'userphone'         :   userphone,
                'usermobile'        :   usermobile,
                'userskype'         :   userskype,
                // 'usertitle'      :   usertitle,
                'description'       :   description,
                'upload_picture'    :   upload_picture,
                'security-profile'  :   securityprofile,
                'profile_image_url' :   profile_image_url,
                'profile_image_url_small':profile_image_url_small,
                'userfacebook'      :   userfacebook,
                'usertwitter'       :   usertwitter,
                'userlinkedin'      :   userlinkedin,
                'userpinterest'     :   userpinterest,
                'userinstagram'     :   userinstagram,
                // 'userurl'        :   userurl,
				
				'user_billing_company'		:   user_billing_company,
			    'user_billing_address_1'    :   user_billing_address_1,
                'user_billing_city'			:   user_billing_city,
                'user_billing_postcode'		:   user_billing_postcode,
                'user_billing_country'		:   user_billing_country,
                'user_billing_state'		:   user_billing_state,
                'user_billing_vat'			:   user_billing_vat,
				
                'developer_languages'  :   developer_languages,
                'developer_website'   :   developer_website,
                'developer_taxes'      :   developer_taxes,
                'developer_license'    :   developer_license,
                'developer_category_submit': developer_category_submit,
                'developer_action_submit'  : developer_action_submit,
                'developer_city'       :   developer_city,
                'developer_county'     :   developer_county,
                'developer_area'       :   developer_area,
                'developer_address'    :   developer_address,
                'developer_lat'        :   developer_lat,
                'developer_long'       :   developer_long
            },
            success: function (data) {
                $('#profile_message #profile_update_ok').show();
                $('#image-loader').hide();
            },
            error: function (errorThrown) 
			{
                $('#profile_message #profile_update_error').show();
				$('#image-loader').hide();				
            }
        });
    });

});