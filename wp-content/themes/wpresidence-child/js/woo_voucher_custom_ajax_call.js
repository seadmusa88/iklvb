

// 
jQuery(document).ready(function ($) 
{
	$('button.ticket-checked-to-payed').on('click', function(event)
	{
		event.preventDefault();
		
		var ticket_id;
		ticket_id = $(this).attr('data-ticket-id');
				
		$.ajax(
		{
            type: 'POST',
            url: ajaxurl,
            data: {
                action      : 'wpestate_ajax_update_ticket_payed',
				ticket_id	: ticket_id
			},
            success: function () 
			{            
                $('#ticket-id-'+ticket_id).replaceWith('OK');
            },
            error: function () 
			{
                $('#ticket-id-'+ticket_id).replaceWith('Error...');
            }		
		});		
	});
});


