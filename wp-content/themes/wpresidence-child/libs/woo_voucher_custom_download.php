<?php

/**
 * Lorenzo M. 2018-08-27
 * 
 * personalizzazione della function woo_vou_my_pdf_vouchers_download_link()
 * per ottenere un array con coucher-code => download_url
 * 
 */
function woo_vou_my_pdf_vouchers_download_link_array($downloads = array()) 
{
	global $woo_vou_model;
	// global $woo_vou_voucher;
	
    // get prefix
    $prefix = WOO_VOU_META_PREFIX;
	$vou_download_dashboard = get_option('vou_download_dashboard');

	// If user is logged in AND download enable for customer dashboard
    if ( is_user_logged_in() && ( $vou_download_dashboard == 'yes') ) 
	{
        //Get user ID
        $user_id = get_current_user_id();

        //Get User Order Arguments
        $args = array(
            'numberposts' => -1,
            'meta_key' => '_customer_user',
            'meta_value' => $user_id,
            'post_type' => WOO_VOU_MAIN_SHOP_POST_TYPE,
            'post_status' => array('wc-completed'),
            'meta_query' => array(
                array(
                    'key' => $prefix . 'meta_order_details',
                    'compare' => 'EXISTS',
                )
            )
        );

        //user orders
        $user_orders = get_posts($args);

		//If orders are not empty
        if (!empty($user_orders)) 
		{
            foreach ($user_orders as $user_order) 
			{
                //Get order ID
                $order_id = isset($user_order->ID) ? $user_order->ID : '';

				//Order it not empty
                if (!empty($order_id)) 
				{
                    global $vou_order;

                    //Set global order ID
                    $vou_order = $order_id;

                    //Get cart details
                    $cart_details = wc_get_order($order_id);
                    $order_items = $cart_details->get_items();
                    $order_date = $woo_vou_model->woo_vou_get_order_date_from_order($cart_details); // Get order date
                    $order_date = date_i18n('F j, Y', strtotime($order_date));

					// Check cart details are not empty
                    if (!empty($order_items)) 
					{
                        foreach ($order_items as $item_id => $product_data) 
						{

                            //Get product from Item ( It is required otherwise multipdf voucher link not work and global $woo_vou_item_id will not work )
                            $_product = apply_filters('woocommerce_order_item_product', $cart_details->get_product_from_item($product_data), $product_data);

							//If product deleted
                            if (!$_product) 
							{
                                $download_file_data = array();
                            }
							else
							{
                                $download_file_data = $woo_vou_model->woo_vou_get_item_downloads_from_order($cart_details, $product_data);
                            }

                            //Get voucher codes
                            $codes = wc_get_order_item_meta($item_id, $prefix.'codes');

							//If download exist and code is not empty
                            if (!empty($download_file_data) && !empty($codes)) 
							{
								$vou_codes 			= $codes; // Get voucher codes from meta
						        $vou_codes_array	= explode(',', $vou_codes); // Explode voucher code in case it is having multiple voucher codes
								
                                foreach($download_file_data as $key => $download_file) 
								{									
									//check download key is voucher key or not
                                    $check_key = strpos($key, 'woo_vou_pdf_');

                                    if(!empty($download_file) && $check_key !== false) 
									{
								    	// get voucher number
										$voucher_key = str_replace('woo_vou_pdf_', '', $key) - 1;

										// if empty voucher number
										if(empty($voucher_key)) $voucher_key = 0;

										// ottengo il voucher code corrispondente al download
										$vou_code = trim($vou_codes_array[$voucher_key]);

										// se voucher code è vuoto si tratta di vecchi biglietti di prova generati senza serial number
										if(empty($vou_code)) continue;

										// Lorenzo M. 2018-08-31
										// aggiungo un parametro query_string necessario per il download...
										$download_file['download_url'] .= '&woo_vou_screen=download';
										
										// append voucher download to downloads array
										$downloads[$vou_code] = $download_file;
									}	
								}
							}
						}
					}
				}

				//reset global order ID
				$vou_order = 0;
			}
		}
	}

    return $downloads;
}