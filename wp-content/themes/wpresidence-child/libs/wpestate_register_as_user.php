<?php

function  wpestate_register_as_user($user_name,$user_id,$new_user_type=0,$first_name='',$last_name='')
{
	$post_type  =   '';
	$app_type   =   '';
	if($new_user_type==1){
		$post_type = 'estate_user'; 
		// $app_type   =esc_html__('Private user','wpresidence'); // = 'publish'
	}else if($new_user_type==2){
		$post_type = 'estate_agent'; 
		$app_type   =esc_html__('Agent','wpresidence');
	}else if($new_user_type==3){
		$post_type = 'estate_agency'; 
		$app_type   =esc_html__('Agency','wpresidence');
	}else if($new_user_type==4){
		$post_type = 'estate_developer';
		$app_type   =esc_html__('Developer','wpresidence');
	}
	$admin_submission_user_role = get_option('wp_estate_admin_submission_user_role',true);



	$post_approve = 'publish';
	if(in_array($app_type, $admin_submission_user_role)){
		$post_approve = 'pending';  
	}


	if($post_type!=''){
		$post = array(
		  'post_title'      => $user_name,
		  'post_status'     => $post_approve, 
		  'post_type'       => $post_type ,
		);
		$post_id =  wp_insert_post($post );  
		update_post_meta($post_id, 'user_meda_id', $user_id);
		update_user_meta($user_id, 'user_agent_id', $post_id);
	}



	$user_email             =   get_the_author_meta( 'user_email' , $user_id );

	if($post_type!=''){
		update_post_meta($post_id, 'agent_email',   $user_email);
	}

	if($first_name!=''){
		update_user_meta( $user_id, 'first_name' , $first_name) ; 
	}
	if($last_name!=''){
		update_user_meta( $user_id, 'last_name' , $last_name) ; 
	}
 }
