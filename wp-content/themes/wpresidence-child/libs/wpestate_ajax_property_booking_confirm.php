<?php

add_action( 'wp_ajax_wpestate_ajax_property_booking_confirm','wpestate_ajax_property_booking_confirm');





function wpestate_ajax_property_booking_confirm()
{
	// check for POST vars
	$ticket_id		= intval($_POST['ticket_id']);
	$action_from	= strval($_POST['action_from']);
	
	// controlli
	if(!is_numeric($ticket_id)) $ticket_id = '';
	if($action_from != 'owner' && $action_from != 'visitor') $action_from = '';
	
	// fermo lo script se necessario
	if(empty($ticket_id) || empty($action_from)) exit('Mandatory fields missing...');
	
	// controllo fake submit
	if(!wp_verify_nonce($_POST['nonce'],'ajax-ticket-'.$ticket_id)) exit('No naughty business please');

	// init
	$response = array(
		'type' 		=> 'error',
		'message' 	=> '',
		'focus'		=> ''
	);

	// oggi
	$now = date('Y-m-d H:i:00');
	
	// utente loggato che sta eseguendo la richiesta
	$current_user = wp_get_current_user();   
	$user_id	= $current_user->ID;
	
	// valori dal biglietto
	$ticket_status				= get_post_meta($ticket_id,'_wpw_pcv_status',true);
	$ticket_number				= get_post_meta($ticket_id,'_woo_vou_purchased_codes',true);
	$ticket_customer_user_id	= get_post_meta($ticket_id,'_wpw_pcv_customer_user_id',true);
	$ticket_assign_prop			= get_post_meta($ticket_id,'_wpw_pcv_assign_prop',true);
	$ticket_assign_prop_user_id = get_post_meta($ticket_id,'_wpw_pcv_assign_prop_user_id',true);
	$ticket_schedule_time		= get_post_meta($ticket_id,'_wpw_pcv_schedule_time', true);
	$ticket_action_type			= get_post_meta($ticket_id,'_wpw_pcv_action_type', true);
	
	
	// controllo la validità del richiedente dell'azione da eseguire
	if($action_from == 'owner' && $user_id != $ticket_assign_prop_user_id) exit('Field mismatch values...');
	if($action_from == 'visitor' && $user_id != $ticket_customer_user_id) exit('Field mismatch values...');
	if($ticket_status != 'assigned') exit('Ticket wrong status...');
	
	

	// controllo la  validità del biglietto 
	$_POST['voucode'] = $ticket_number;
	$vou_voucher = new WOO_Vou_Voucher();
	$vou_voucher_results = $vou_voucher->woo_vou_check_voucher_code();
	
	if(strlen($vou_voucher_results['error']))
	{
		$response['message'] = __('<strong>ERROR</strong>: ','iklvb-wpresidence').$vou_voucher_results['error'];
	}
	elseif(strlen($vou_voucher_results['used']))
	{
		$response['message'] = __('<strong>ERROR</strong>: Ticket number has already been used.','iklvb-wpresidence');
	}

	elseif(strlen($vou_voucher_results['success']) && $vou_voucher_results['expire'] == true)
	{
		$response['message'] = __('<strong>ERROR</strong>: Ticket number was expired.','iklvb-wpresidence');
	}

	if(strlen($response['message']))
	{
		wp_send_json($response);
		exit;
	}


	


	
	// aggiorno le informazioni del biglietto
	update_post_meta($ticket_id, '_wpw_pcv_status', 'confirmed');
	update_post_meta($ticket_id, '_wpw_pcv_next_move', '');
	update_post_meta($ticket_id, '_wpw_pcv_time_update', $now);
	
	
	// preparo le parti del messaggio
	$args = array(
		'visitor_id'	=> $ticket_customer_user_id,
		'owner_id'		=> $ticket_assign_prop_user_id,
		'property_id'	=> $ticket_assign_prop,
		'schedule_time'	=> $ticket_schedule_time
	);
	
	

	// invio messaggio di avviso al proprietario dell'immobile >>>
	if($action_from == 'visitor')
	{		
		// destinatario del messaggio
		// email del proprietario dell'annuncio
		$receiver_email = iklvb_get_email_owner_from_property_id($ticket_assign_prop);

		// messaggio a seconda del tipo di biglietto
		$iklvb_email = iklvb_get_email_parts('tour-confirm-visitor-to-owner',$args);

		// contenuto email in alto a destra (destinatario)
		global $email_header_right;
		$email_header_right = iklvb_get_user_agent_email_to_box($ticket_assign_prop_user_id);		
		
		// intestazione
		$headers = array(
			'Content-Type: text/html; charset=UTF-8',
			'From:iKLVB <notification@iklvb.com>'
		);

		// invio della email
		wp_mail($receiver_email, $iklvb_email['subject'], $iklvb_email['message'], $headers);

		
		// messaggio di conferma
		$response['type'] = 'success';
		$response['message'] = __('Your confirmation has been notified to the advertiser.','iklvb-wpresidence');
	}
	// <<<

	
	// invio messaggio di avviso al visitatore >>>
	if($action_from == 'owner')
	{		
		// destinatario del messaggio
		// email del visitatore
		$receiver_email = iklvb_get_email_visitor_from_user_agent_id($ticket_customer_user_id);

		// messaggio a seconda del tipo di biglietto
		$iklvb_email = iklvb_get_email_parts('tour-confirm-owner-to-visitor',$args);

		// contenuto email in alto a destra (destinatario)
		global $email_header_right;
		$email_header_right = iklvb_get_user_agent_email_to_box($ticket_customer_user_id);		
		
		// intestazione
		$headers = array(
			'Content-Type: text/html; charset=UTF-8',
			'From:iKLVB <notification@iklvb.com>'
		);

		// invio della email
		wp_mail($receiver_email, $iklvb_email['subject'], $iklvb_email['message'], $headers);
		
		
		// messaggio di conferma
		$response['type'] = 'success';
		$response['message'] = __('Your confirmation has been notified to the visitor.','iklvb-wpresidence');
	}
	// <<<

	

	// output
	wp_send_json($response);
	exit;
}
