<?php

add_action( 'wp_ajax_wpestate_ajax_update_ticket_cash_in','wpestate_ajax_update_ticket_cash_in');





function wpestate_ajax_update_ticket_cash_in()
{
	// check for POST vars
	$ticket_id	= strval($_POST['ticket_id']);
	
	// controlli
	if(!is_numeric($ticket_id)) exit('ticket non valido');

	// elenco biglietti collegati alle proprietà dell'utente loggato
	$args = array(
		'post_type' => 'woovouchercodes',
		'p'			=> $ticket_id,
	);

	$tickets_results = new WP_Query($args);

	if($tickets_results->post_count > 0)
	{
		while($tickets_results->have_posts())
		{
			$tickets_results->the_post(); 
			get_template_part('templates/user_profile_ticket_cash_in'); 
		}
	}	

	// non voglio altro output
	exit;	
}