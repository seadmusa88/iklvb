<?php

if( !function_exists('estate_listing_address') ):
function estate_listing_address($post_id,$col=3){
    
    $property_address   = esc_html( get_post_meta($post_id, 'property_address', true) );
    $property_city      = get_the_term_list($post_id, 'property_city', '', ', ', '');
    $property_area      = get_the_term_list($post_id, 'property_area', '', ', ', '');
    $property_county    = get_the_term_list($post_id, 'property_county_state', '', ', ', '') ;
    $property_zip       = esc_html(get_post_meta($post_id, 'property_zip', true) );
    $property_country   = esc_html(get_post_meta($post_id, 'property_country', true) );
    $colmd=4;
    
	
	// user login + performed or confirmed_24 >>>
	if(is_user_logged_in())
	{
		// dati dell'utente loggato
		$current_user = wp_get_current_user(); 
	
		// init object 
		$ticket_obj = new Wpw_Pcv_Public();
	
		// controllo se l'utente ha dei biglietti 
		// - performed cioè incontro già avvenuto
		// - confirmed_24 cioè l'incontro è stato ocnfermato ed è previsto nelle prossime 24 ore (l'iincontro non può essere annullato)
		$flag_show_address = false;
		if($ticket_obj->wpw_pcv_voucher_is_performed_prop_user($post_id,$current_user->ID) || $ticket_obj->wpw_pcv_voucher_is_confirmed_24_prop_user($post_id,$current_user->ID)) $flag_show_address = true;	
	}
	// <<<
	
	
    switch ($col) {
        case 1:
            $colmd=12;
            break;
        case  2:
            $colmd=6;
            break;
        case  3:
            $colmd=4;
            break;
        case  4:
            $colmd=3;
            break;
    }
    
    $return_string='';
	
	$property_address_hidden = __('(hidden address: will be visible 24 hours before the confirmed date)','iklvb-wpresidence');
    
    if ($property_address != ''){
        $return_string.='<div class="listing_detail col-md-'.$colmd.'"><strong>'.esc_html__('Address','wpresidence').':</strong> ' . ($flag_show_address ? $property_address : $property_address_hidden). '</div>'; 
    }
    if ($property_city != ''){
        $return_string.= '<div class="listing_detail col-md-'.$colmd.'"><strong>'.esc_html__('City','wpresidence').':</strong> ' .$property_city. '</div>';  
    }  
    if ($property_area != ''){
        $return_string.= '<div class="listing_detail col-md-'.$colmd.'"><strong>'.esc_html__('Area','wpresidence').':</strong> ' .$property_area. '</div>';
    }    
    if ($property_county != ''){
        $return_string.= '<div class="listing_detail col-md-'.$colmd.'"><strong>'.esc_html__('State/County','wpresidence').':</strong> ' . $property_county . '</div>'; 
    }
    if ($property_zip != ''){
        $return_string.= '<div class="listing_detail col-md-'.$colmd.'"><strong>'.esc_html__('Zip','wpresidence').':</strong> ' . $property_zip . '</div>';
    }  
    if ($property_country != '') {
        $return_string.= '<div class="listing_detail col-md-'.$colmd.'"><strong>'.esc_html__('Country','wpresidence').':</strong> ' . $property_country . '</div>'; 
    } 
    $property_address   =   esc_html( get_post_meta($post_id, 'property_address', true) );
    $property_city      =   strip_tags (  get_the_term_list($post_id, 'property_city', '', ', ', '') );
    $url                =   urlencode($property_address.','.$property_city);
    $google_map_url     =   "http://maps.google.com/?q=".$url;

	if($flag_show_address)
	{
	    $return_string.= ' <a href="'.$google_map_url.'" target="_blank" class="acc_google_maps">'.esc_html__('Open In Google Maps','wpresidence').'</a>';
	}
	
    return  $return_string;
}
endif; // end   estate_listing_address  

