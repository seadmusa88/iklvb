<?php

add_action( 'wp_ajax_wpestate_ajax_update_ticket_payed','wpestate_ajax_update_ticket_payed');





function wpestate_ajax_update_ticket_payed()
{
	// check for POST vars
	$ticket_id	= strval($_POST['ticket_id']);
	
	// controlli
	if(!is_numeric($ticket_id)) exit('ticket non valido');

	// ottengo lo status attuale
	$ticket_status = get_post_meta($ticket_id,'_wpw_pcv_status',true);
	
	// controllo
	if($ticket_status == 'checked')
	{
		// imposto lo status a "payed"
		update_post_meta($ticket_id, '_wpw_pcv_status', 'payed');
	}	

	exit;	
}