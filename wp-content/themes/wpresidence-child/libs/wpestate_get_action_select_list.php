<?php



if( !function_exists('wpestate_get_action_select_list') ): 
    function wpestate_get_action_select_list($args){
        $categ_select_list  =   get_transient('wpestate_get_action_select_list');
        if($categ_select_list===false){
            $taxonomy           =   'property_action_category';
            $categories          =   get_terms($taxonomy,$args);

            $categ_select_list =   ' <li role="presentation" data-value="all">'. esc_html__('All Actions','wpresidence').'</li>';
            foreach ($categories as $categ) {
                $received = wpestate_hierarchical_category_childen($taxonomy, $categ->term_id,$args ); 
                $counter = $categ->count;
                if(isset($received['count'])){
                    $counter = $counter+$received['count'];
                }

                // $categ_select_list     .=   '<li role="presentation" data-value="'.$categ->slug.'">'. ucwords ( urldecode( $categ->name ) ).' ('.$counter.')'.'</li>';
				
				// 2018-12-06 Lorenzo M.
                $categ_select_list     .=   '<li role="presentation" data-value="'.$categ->slug.'">'. ucwords ( urldecode( $categ->name ) ).'</li>';
				/* --- */
				
                if(isset($received['html'])){
                    $categ_select_list     .=   $received['html'];  
                }

            }
        $transient_appendix='';
        if ( defined( 'ICL_LANGUAGE_CODE' ) ) {
            $transient_appendix.='_'. ICL_LANGUAGE_CODE;
        }
        set_transient('wpestate_get_action_select_list'.$transient_appendix,$categ_select_list,4*60*60);
        
        }
        return $categ_select_list;
    }
endif;