<?php

///////////////////////////////////////////////////////////////////////////////////////////
// login form  function
///////////////////////////////////////////////////////////////////////////////////////////

if( !function_exists('wpestate_login_form_function') ):
  
function wpestate_login_form_function($attributes, $content = null) {
     // get user dashboard link
        global $wpdb;
        $redirect='';
        $mess='';
        $allowed_html   =   array();
        
        $attributes = shortcode_atts( 
              array(
                  'register_label'                  => '',
                  'register_url'                =>  '',
                 
              ), $attributes) ;  

  
    $post_id=get_the_ID();
    $login_nonce=wp_nonce_field( 'login_ajax_nonce', 'security-login',true,false );
    $security_nonce=wp_nonce_field( 'forgot_ajax_nonce', 'security-forgot',true,false );
    $return_string = '
		<div class="login_form shortcode-login" id="login-div">
			<div class="loginalert" id="login_message_area" >'.$mess.'</div>
        
                <div class="loginrow">
                    <input type="text" class="form-control" name="log" id="login_user" placeholder="'.__('Username','wpestate').'" size="20" />
                </div>
                <div class="loginrow">
                    <input type="password" class="form-control" name="pwd" id="login_pwd"  placeholder="'.__('Password','wpestate').'" size="20" />
                </div>
                <input type="hidden" name="loginpop" id="loginpop" value="0">
              
                <input type="hidden" id="security-login" name="security-login" value="'. estate_create_onetime_nonce( 'login_ajax_nonce' ).'">
       
                   
                <button id="wp-login-but" class="wpresidence_button">'.__('Login','wpestate').'</button>
                <div class="login-links shortlog">
	';
    
	// 2018-07-20 Lorenzo M.
	$return_string .= '
				<label for="rememberme"><input name="rememberme" type="checkbox" id="rememberme" value="forever" /> Remember Me</label>
	';
					
          
                if(isset($attributes['register_label']) && $attributes['register_label']!=''){
                     $return_string.='<a href="'.$attributes['register_url'].'">'.$attributes['register_label'].'</a> | ';
                }         
                $return_string.='<a href="#" id="forgot_pass">'.__('Forgot Password?','wpestate').'</a>
                </div>';
                $facebook_status    =   esc_html( get_option('wp_estate_facebook_login','') );
                $google_status      =   esc_html( get_option('wp_estate_google_login','') );
                $yahoo_status       =   esc_html( get_option('wp_estate_yahoo_login','') );
               
                
                if($facebook_status=='yes'){
                    $return_string.='<div id="facebooklogin" data-social="facebook">'.__('Login with Facebook','wpestate').'</div>';
                }
                if($google_status=='yes'){
                    $return_string.='<div id="googlelogin" data-social="google">'.__('Login with Google','wpestate').'</div>';
                }
                if($yahoo_status=='yes'){
                    $return_string.='<div id="yahoologin" data-social="yahoo">'.__('Login with Yahoo','wpestate').'</div>';
                }
                   
         $return_string.='                 
         </div>
         <div class="login_form  shortcode-login" id="forgot-pass-div-sh">
            <div class="loginalert" id="forgot_pass_area"></div>
            <div class="loginrow">
                    <input type="text" class="form-control" name="forgot_email" id="forgot_email" placeholder="'.__('Enter Your Email Address','wpestate').'" size="20" />
            </div>
            '. $security_nonce.'  
            <input type="hidden" id="postid" value="'.$post_id.'">    
            <button class="wpresidence_button" id="wp-forgot-but" name="forgot" >'.__('Reset Password','wpestate').'</button>
            <div class="login-links shortlog">
            <a href="#" id="return_login">'.__('Return to Login','wpestate').'</a>
            </div>
         </div>
        
            ';
    return  $return_string;
}
endif; // end   wpestate_login_form_function 
