<?php

add_action( 'wp_ajax_wpestate_ajax_update_ticket_listing_single','wpestate_ajax_update_ticket_listing_single');





function wpestate_ajax_update_ticket_listing_single()
{
	// check for POST vars
	$ticket_id	= strval($_POST['ticket_id']);
	$my_page	= strval($_POST['my_page']);

	
	// controlli
	if(!is_numeric($ticket_id)) exit('ticket non valido');
	
	
	
	// load del template in base al tipo di richiesta
	if($my_page == 'tickets')
	{
		// elenco biglietti scaricabili a partire dagli ordini eseguiti dall'utente loggato
		// NB: viene utilizzato dal template di pagina grazie all'uso di global
		$download_url_list = woo_vou_my_pdf_vouchers_download_link_array();
		
		// elenco biglietti collegati alle proprietà dell'utente loggato
		$args = array(
			'post_type' => 'woovouchercodes',
			'p'			=> $ticket_id,
		);

		$tickets_results = new WP_Query($args);
		
		
		if($tickets_results->post_count > 0)
		{
			while($tickets_results->have_posts())
			{
				
				$tickets_results->the_post(); 
				get_template_part('templates/user_profile_ticket_listing_unit'); 
			}
		}	
		
		// non voglio altro output
		exit;
	}
	
	
	
	if($my_page == 'visitors')
	{
		// elenco biglietti collegati alle proprietà dell'utente loggato
		$args = array(
			'post_type' => 'woovouchercodes',
			'p'			=> $ticket_id,
		);

		$tickets_results = new WP_Query($args);
		
		if($tickets_results->post_count > 0)
		{
			while($tickets_results->have_posts())
			{
				$tickets_results->the_post(); 
				get_template_part('templates/user_profile_visitor_listing_unit'); 
			}
		}		
		
		// non voglio altro output
		exit;
	}
	
}