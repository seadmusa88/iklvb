<?php

/** 
 * Lorenzo M. // 2018-08-17
 * 
 * personalizzazioni per Woocommerce Pdf Voucher 
 * realizzate seguendo la loro guida:
 * https://wpweb.co.in/documents/woocommerce-pdf-vouchers/developer-doc/ 
 * 
 */


/**
 * quando un ordine viene indicato come completato 
 * copio alcuni campi dai prodotti ai voucher
 * e imposto lo status iniziale come "available"
 */
function title_filter($where, $query) 
{
	$where .= " AND post_title = '".$query->get('order_id')."'";
	return $where;
}

add_action( 'woocommerce_order_status_completed', 'wpv_custom_order_status_completed', 10, 100 );
function wpv_custom_order_status_completed($order_id)
{
	// ottengo i voucher collegati all'ordine di acquisto
	$args = array(
		'post_type'		=> 'woovouchercodes',
		'post_status'	=> 'publish',
		'posts_per_page'=> -1,
		'order_id'		=> $order_id
	);	
	
	add_filter('posts_where','title_filter',10,2);
	$query = new WP_Query($args);
	remove_filter('posts_where','title_filter',10,2);
	
	
	// controllo se ci sono risultati da elaborare
	if(isset($query->posts) && !empty($query->posts))
	{
		// carico l'id utente che ha acquistato il voucher
		$wc_order = new WC_Order($order_id);
		$customer_user_id = $wc_order->get_customer_id();		
		
		// ciclo per ogni voucher trovato
		foreach($query->posts as $post)
		{
			// ottengo i valori dal prodotto
			$action_type		= get_post_meta($post->post_parent, 'action_type',			true);
			$price_range_min	= get_post_meta($post->post_parent, 'price_range_min',		true);
			$price_range_max	= get_post_meta($post->post_parent, 'price_range_max',		true);
			$event_ticket_type	= get_post_meta($post->post_parent, 'event_ticket_type',	true);
			$pdf_template		= get_post_meta($post->post_parent, '_woo_vou_pdf_template',true);

			// inserisco i valori nel voucher
			update_post_meta($post->ID, '_wpw_pcv_action_type',			$action_type);
			update_post_meta($post->ID, '_wpw_pcv_price_range_min',		$price_range_min);
			update_post_meta($post->ID, '_wpw_pcv_price_range_max',		$price_range_max);
			update_post_meta($post->ID, '_wpw_pcv_event_ticket_type',	$event_ticket_type);
			update_post_meta($post->ID, '_wpw_pcv_pdf_template',		$pdf_template);

			// imposto il valore di status del voucher
			update_post_meta($post->ID, '_wpw_pcv_status', 'available');
			
			// data ultima modifica
			update_post_meta($post->ID, '_wpw_pcv_time_update',	date('Y-m-d H:i:00'));

			// altri campi 
			update_post_meta($post->ID, '_wpw_pcv_next_move', '');
			update_post_meta($post->ID, '_wpw_pcv_email_notify_sent', '0');			
			update_post_meta($post->ID, '_wpw_pcv_customer_user_id', $customer_user_id);			
		}
	}
}


// debug
// wpv_custom_order_status_completed(23736);







add_filter ( 'woo_vou_check_vou_voucherinfo_fields', 'wpv_custom_voucher_expires_info', 10, 5 );
function wpv_custom_voucher_expires_info ( $voucher_info, $order_id, $voucode ) 
{
	$voucher_info['status'] = 'Status';
	return $voucher_info;
}


add_filter ( 'woo_vou_check_voucher_column_value', 'wpv_custom_voucher_column_value', 10, 5 );
function wpv_custom_voucher_column_value ( $column_value, $col_key, $voucodeid, $item_id, $order_id ) 
{	
	if( $col_key == 'status' ) 
	{
		$prop_id = get_post_meta( $voucodeid, '_wpw_pcv_assign_prop', true);
		$column_value = (empty($prop_id) ? 'Available' : 'Assigned');
	}
		
	return $column_value;
}


add_filter ('woo_vou_custom_add_column','wpv_custom_purchased_column', 10, 5);
add_filter ('woo_vou_purchased_add_column','wpv_custom_purchased_column', 10, 5);
function wpv_custom_purchased_column($columns)
{
	$columns_new = array();
	foreach($columns as $k => $v)
	{
		$columns_new[$k] = $v;
		
		if($k == 'buyers_info') unset($columns_new['buyers_info']);		
		if($k == 'order_info') $columns_new['ticket_info'] = 'Ticket Information'; 		
	}
	$columns_new['status'] = 'Status';
	return $columns_new;
}


add_filter ('woo_vou_custom_column_value','wpv_custom_purchased_column_value', 10, 5);
add_filter ('woo_vou_purchased_column_value','wpv_custom_purchased_column_value', 10, 5);
function wpv_custom_purchased_column_value($column_value, $column_name = '', $item = '')
{
	if($column_name == 'status')
	{
		$status = get_post_meta($item['ID'], '_wpw_pcv_status', true);
		$column_value = get_ticket_status_html($status);
		
		if($status == 'checked')
		{
			$column_value .= '<br><button class="button button-primary button-large ticket-checked-to-payed" data-ticket-id="'.$item['ID'].'" id="ticket-id-'.$item['ID'].'">Set as PAYED</button>';
		}
	}
	
	return $column_value;
}




add_filter( 'woo_vou_admin_voucher_code_tabs', 'woo_vou_custom_admin_voucher_code_custom_tabs' );
function woo_vou_custom_admin_voucher_code_custom_tabs( $voucher_code_tabs ) 
{	
	// For remove purchased voucher codes tab
	if( !empty( $voucher_code_tabs['purchased'] ) ) $voucher_code_tabs['purchased']['name'] = 'Purchased (All Tickets)';
	
	// For remove expired voucher codes tab
	if( !empty( $voucher_code_tabs['used'] ) ) unset( $voucher_code_tabs['used'] );
	
	// For remove expired voucher codes tab
	if( !empty( $voucher_code_tabs['expired'] ) ) unset( $voucher_code_tabs['expired'] );
	
	// add new custom tabs
	$voucher_code_tabs['available_voucher'] =  array(
		'name' => get_ticket_status_html('available'),
		'path' => get_theme_file_path().'/woocommerce-pdf-vouchers/woo-vou-available-list.php'
	);
	$voucher_code_tabs['assigned_voucher'] =  array(
		'name' => get_ticket_status_html('assigned'),
		'path' => get_theme_file_path().'/woocommerce-pdf-vouchers/woo-vou-assigned-list.php'
	);
	$voucher_code_tabs['confirmed_voucher'] =  array(
		'name' => get_ticket_status_html('confirmed'),
		'path' => get_theme_file_path().'/woocommerce-pdf-vouchers/woo-vou-confirmed-list.php'
	);
	$voucher_code_tabs['performed_voucher'] =  array(
		'name' => get_ticket_status_html('performed'),
		'path' => get_theme_file_path().'/woocommerce-pdf-vouchers/woo-vou-performed-list.php'
	);
	$voucher_code_tabs['checked_voucher'] =  array(
		'name' => get_ticket_status_html('checked'),
		'path' => get_theme_file_path().'/woocommerce-pdf-vouchers/woo-vou-checked-list.php'
	);
	$voucher_code_tabs['payed_voucher'] =  array(
		'name' => get_ticket_status_html('payed'),
		'path' => get_theme_file_path().'/woocommerce-pdf-vouchers/woo-vou-payed-list.php'
	);
	$voucher_code_tabs['expired_voucher'] =  array(
		'name' => get_ticket_status_html('expired'),
		'path' => get_theme_file_path().'/woocommerce-pdf-vouchers/woo-vou-expired-list.php'
	);
		
	return $voucher_code_tabs;
}






add_filter ('woo_vou_custom_column_value','wpv_custom_column_ticket_info_value', 10, 5);
add_filter ('woo_vou_purchased_column_value','wpv_custom_column_ticket_info_value', 10, 5);
function wpv_custom_column_ticket_info_value($column_value, $column_name = '', $item = '')
{
	$column_value_add = '';
	
	if($column_name == 'product_info')
	{
		$expire_date = get_post_meta($item['ID'], '_woo_vou_exp_date', true);
		$email_notify_sent = get_post_meta($item['ID'], '_wpw_pcv_email_notify_sent', true);
		$time_update = get_post_meta($item['ID'], '_wpw_pcv_time_update', true);
		
		$column_value_add = '
			<tr>
				<td style="font-weight:bold;">Expire:</td>
				<td>'.$expire_date.'</td>
			</tr>
			'.($email_notify_sent == 1 ? '
			<tr>
				<td style="font-weight:bold;">Notify:</td>
				<td>'.$time_update.'</td>
			</tr>	
			' : '').'
		';
		
		$column_value = str_replace('</table>',$column_value_add,$column_value).'</table>';
	}	
	
	if($column_name == 'order_info')
	{
		$buyer_id	= get_post_meta($item['ID'], '_wpw_pcv_customer_user_id', true);
		$buyer_info = get_userdata($buyer_id);
		$buyer_name = $buyer_info->last_name.' '.$buyer_info->first_name;
		$buyer_link = get_edit_user_link($buyer_id);		
				
		$column_value_add = '
			<tr>
				<td style="font-weight:bold;">Buyer Name:</td>
				<td><a href="'.$buyer_link.'" target="_blank">'.$buyer_name.'</a></td>
			</tr>
		';
		
		$column_value = str_replace('</table>',$column_value_add,$column_value).'</table>';
	}
	
	if($column_name == 'ticket_info')
	{
		$prop_id = get_post_meta($item['ID'], '_wpw_pcv_assign_prop', true);
		$prop_title = get_the_title($prop_id);
		$prop_permalink = get_the_permalink($prop_id);
		
		$action_type		= get_post_meta($item['ID'], '_wpw_pcv_action_type', true);
		// $price_range_min	= get_post_meta($item['ID'], '_wpw_pcv_price_range_min', true);
		$price_range_max	= get_post_meta($item['ID'], '_wpw_pcv_price_range_max', true);
		$event_ticket_type	= get_post_meta($item['ID'], '_wpw_pcv_event_ticket_type', true);
		$schedule_time		= get_post_meta($item['ID'], '_wpw_pcv_schedule_time', true);
		
		$currency = get_woocommerce_currency_symbol();
		
		$column_value = '
			<table>
				<tr>
					<td width="30%" style="font-weight:bold;">Action:</td>
					<td width="70%">'.$action_type.'</td>
				</tr>
				<tr>
				'.($action_type == 'event' || $action_type == 'evento' ? '
					<td style="font-weight:bold;">Ticket:</td>
					<td>'.$event_ticket_type.'</td>					
					
				' : 
					/* <td>'.$price_range_min.' '.$currency.' / '.$price_range_max.' '.$currency.'</td> */ // 2018-10-23 Lorenzo M.
					'
					<td style="font-weight:bold;">Range:</td>
					<td>'.__('up to price of','iklvb-wpresidence').' '.$price_range_max.' '.$currency.'</td>
				').'
				</tr>
				'.(is_numeric($prop_id) ? '
				<tr>
					<td style="font-weight:bold;">Property:</td>
					<td><a href="'.$prop_permalink.'" target="_blank">'.$prop_title.'</a></td>
				</tr>
				<tr>
					<td style="font-weight:bold;">Schedule:</td>
					<td>'.$schedule_time.'</td>
				</tr>
				' : '').'
				<tr>
					<td style="font-weight:bold;">ID:</td>
					<td>'.$item['ID'].'</td>
				</tr>	
			</table>
		';
	}
	
	return $column_value;
}


/* --- */