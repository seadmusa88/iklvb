<?php


/*
 * 2018-10-23 Lorenzo M.
 * 
 * aggiungo al tema il supporto a woocommerce
 * 
 * source:
 * https://github.com/woocommerce/woocommerce/wiki/Declaring-WooCommerce-support-in-themes
 * 
 */
/*
add_action( 'after_setup_theme', 'iklvb_add_woocommerce_support' );

function iklvb_add_woocommerce_support() 
{
	add_theme_support( 'woocommerce', array(
		'thumbnail_image_width' => 150,
		'single_image_width'    => 300,

        'product_grid'          => array(
            'default_rows'    => 8,
            'min_rows'        => 8,
            'max_rows'        => 8,
            'default_columns' => 1,
            'min_columns'     => 1,
            'max_columns'     => 1,
        ),
	) );
}
*/


/**
 * 2018-10-22 Lorenzo M.
 * 
 * Remove related products output
 * 
 * source:
 * https://docs.woocommerce.com/document/remove-related-posts-output/
 * 
 */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );



/**
 * 2018-10-22 Lorenzo M.
 * 
 * source:
 * https://businessbloomer.com/woocommerce-remove-sidebar-single-product-page/
 * 
 * @snippet       Remove Sidebar @ Single Product Page
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=19572
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 3.2.6
 */

add_action( 'wp', 'bbloomer_remove_sidebar_product_pages' );
 
function bbloomer_remove_sidebar_product_pages() 
{
	if ( is_product() ) 
	{
		remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
	}
}



// remove
remove_action('woocommerce_before_single_product_summary','woocommerce_show_product_sale_flash');
remove_action('woocommerce_before_single_product_summary','woocommerce_show_product_images');
remove_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt', 20);
remove_action('woocommerce_single_product_summary','woocommerce_template_single_meta', 40);

// add
add_action('woocommerce_single_product_summary','iklvb_woocommerce_template_single_title_type', 2);
add_action('woocommerce_single_product_summary','woocommerce_show_product_images',7);
add_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt', 45);
add_action('woocommerce_single_product_summary','woocommerce_template_single_meta', 60);


/**
 * 2018-10-23 Lorenzo M.
 * 
 * aggiungo il tipo di biglietto nella parte alta della pagina
 * 
 */
function iklvb_woocommerce_template_single_title_type()
{
	global $post;
	
	$action_type = get_post_meta($post->post_parent, 'action_type', true);
	
	if($action_type == 'event' || $action_type == 'evento') 
	{
		echo '<h1>'.__('Ticket Event','iklvb-wpresidence').'</h1>'; // testo non presente nel file .po
	}
	else
	{
		echo '<h1>'.__('Ticket Tour','iklvb-wpresidence').'</h1>'; // testo non presente nel file .po
	}
}