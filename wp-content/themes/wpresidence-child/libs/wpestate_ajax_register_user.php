<?php

function wpestate_ajax_register_user()
{
        $type       =   intval( $_POST['type'] );
        $capthca    =   sanitize_text_field ( $_POST['capthca'] );
        
        if( get_option('wp_estate_use_captcha','')=='yes'){
            if(!isset($_POST['capthca']) || $_POST['capthca']==''){
                exit( esc_html__('wrong captcha','wpresidence') );
            }

            $secret    = get_option('wp_estate_recaptha_secretkey','');
            global $wp_filesystem;
            if (empty($wp_filesystem)) {
                require_once (ABSPATH . '/wp-admin/includes/file.php');
                WP_Filesystem();
            }
            $response = $wp_filesystem->get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);

            $response=json_decode(json_decode);
            if ($response['success'] = false) {
                exit('out pls captcha');
            }
        }
        
      
        $allowed_html               =   array();
		
        $user_firstname             =   trim( sanitize_text_field(wp_kses( $_POST['user_firstname_register'] ,$allowed_html) ) );		// 2018-12-17 Lorenzo M.
        $user_secondname            =   trim( sanitize_text_field(wp_kses( $_POST['user_secondname_register'] ,$allowed_html) ) );		// 2018-12-17 Lorenzo M.
		
        $user_email                 =   trim( sanitize_text_field(wp_kses( $_POST['user_email_register'] ,$allowed_html) ) );
        $user_name                  =   trim( sanitize_text_field(wp_kses( $_POST['user_login_register'] ,$allowed_html) ) );
        $enable_user_pass_status    =   esc_html ( get_option('wp_estate_enable_user_pass','') );
        
        $new_user_type              =   intval($_POST['new_user_type']);
      
        /*
        if (preg_match("/^[0-9A-Za-z_]+$/", $user_name) == 0) {
            print esc_html__('Invalid username (do not use special characters or spaces)!','wpresidence');
            die();
        }
        */
		
        if ($user_firstname == '' ){
            print esc_html__('First Name field is empty!','iklvb-wpresidence');
            exit();
        }
		
        if ($user_secondname == '' ){
            print esc_html__('Last Name field is empty!','iklvb-wpresidence');
            exit();
        }
        
        if(filter_var($user_email,FILTER_VALIDATE_EMAIL) === false) {
            print esc_html__('The email doesn\'t look right !','wpresidence');
            exit();
        }
        
        $domain = mb_substr(strrchr($user_email, "@"), 1);
        if( !checkdnsrr ($domain) ){
            print esc_html__('The email\'s domain doesn\'t look right.','wpresidence');
            exit();
        }
        
        
        $user_id     =   username_exists( $user_name );
        if ($user_id){
            print esc_html__('Username already exists.  Please choose a new one.','wpresidence');
            exit();
        }
        
        if($enable_user_pass_status=='yes' ){
            $user_pass              =   trim( sanitize_text_field(wp_kses( $_POST['user_pass'] ,$allowed_html) ) );
            $user_pass_retype       =   trim( sanitize_text_field(wp_kses( $_POST['user_pass_retype'] ,$allowed_html) ) );
        
            if ($user_pass=='' || $user_pass_retype=='' ){
                print esc_html__('One of the password field is empty!','wpresidence');
                exit();
            }
            
            if ($user_pass !== $user_pass_retype ){
                print esc_html__('Passwords do not match','wpresidence');
                exit();
            }
        }
         
 
         
        if ( !$user_id and email_exists($user_email) == false ) {
            if($enable_user_pass_status=='yes' ){
                $user_password = $user_pass; // no so random now!
            }else{
                $user_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
            }
            
            $user_id         = wp_create_user( $user_name, $user_password, $user_email );
         
            if ( is_wp_error($user_id) ){
        
            }else{
                if($enable_user_pass_status=='yes' ){
                    print esc_html__('Your account was created and you can login now!','wpresidence');
                }else{
                    print esc_html__('An email with the generated password was sent!','wpresidence');
                }
                  
                wpestate_update_profile($user_id);
                wpestate_wp_new_user_notification( $user_id, $user_password ) ;
                update_user_meta( $user_id, 'user_estate_role', $new_user_type) ;
             
				update_user_meta( $user_id, 'first_name', $user_firstname ) ;		// 2018-12-17 Lorenzo M.
				update_user_meta( $user_id, 'last_name',  $user_secondname) ;		// 2018-12-17 Lorenzo M.
		
				$user_name_full = $user_firstname.' '.$user_secondname;				// 2019-12-07 Lorenzo M.
				
                // if($new_user_type!==0 && $new_user_type!==1 ){		// 2019-12-07 Lorenzo M.
                if($new_user_type!==0 ){								// 2019-12-07 Lorenzo M.
                    wpestate_register_as_user($user_name_full,$user_id,$new_user_type);
                }
             }
             
        } else {
           print esc_html__('Email already exists.  Please choose a new one.','wpresidence');
        }
        die(); 
              
}