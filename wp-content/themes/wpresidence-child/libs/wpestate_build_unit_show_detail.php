<?php

if(!function_exists('wpestate_build_unit_show_detail')):
function wpestate_build_unit_show_detail($element,$propID,$property_unit_slider,$text,$icon){
    $element = strtolower($element);
    
    
    switch ($element) {
        case 'share':
            $link=get_permalink($propID);
            if ( has_post_thumbnail() ){
                $pinterest = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full_map');
            }
            $protocol = is_ssl() ? 'https' : 'http';
            print '
                <div class="share_unit">
                <a href="'.$protocol.'://www.facebook.com/sharer.php?u='.esc_url($link).'&amp;t='.urlencode(get_the_title($propID)).'" target="_blank" class="social_facebook"></a>
                <a href="'.$protocol.'://twitter.com/home?status='.urlencode(get_the_title($propID).' '.$link).'" class="social_tweet" target="_blank"></a>
                <a href="'.$protocol.'://plus.google.com/share?url='.esc_url($link).'" onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;" target="_blank" class="social_google"></a> 
                <a href="'.$protocol.'://pinterest.com/pin/create/button/?url='.esc_url($link).'&amp;media=';
                    if (isset( $pinterest[0])){ print esc_url($pinterest[0]); }
                    print '&amp;description='.urlencode(get_the_title($propID)).'" target="_blank" class="social_pinterest"></a>
                </div>';
            if($text==''){
                if($icon!=''){
                    if ( strpos($icon, 'fa-') !== false){
                        print '<span class="share_list text_share"  data-original-title="'.esc_html__('share','wpresidence').'" ><i class="fa '.$icon.'" aria-hidden="true"></i></span>';
                    }else{
                        print '<span class="share_list text_share"  data-original-title="'.esc_html__('share','wpresidence').'" ><img src="'.$icon.'" alt="featured_icon"></span>';
                    }
                }else{
                    print '<span class="share_list"  data-original-title="'.esc_html__('share','wpresidence').'" ></span>';
                }
                
            }else{
               print '<span class="share_list text_share"  data-original-title="'.esc_html__('share','wpresidence').'" >'.$text.'</span>';
            }       
               
        break;
        
        
        case 'link_to_page':
         
            $link=get_permalink($propID);        
            if($text==''){
                if ( strpos($icon, 'fa-') !== false){
                    print '<a href="'.$link.'"><i class="fa '.$icon.'" aria-hidden="true"></i></a>';
                }else{
                    print '<a href="'.$link.'"><img src="'.$icon.'" alt="details"></a>';
                }
            }else{
               print '<a href="'.$link.'">'.str_replace('_',' ',$text).'</a>';
               
            }       
               
        break;
       
        case 'favorite':
            $current_user   =   wp_get_current_user();
            $userID         =   $current_user->ID;
            $user_option    =   'favorites'.$userID;
            $favorite_class =   'icon-fav-off';
            $fav_mes        =   esc_html__('add to favorites','wpresidence');
            $user_option    =   'favorites'.$userID;
            $curent_fav     =   get_option($user_option);
            if($curent_fav){
                if ( in_array ($propID,$curent_fav) ){
                    $favorite_class =   'icon-fav-on';   
                    $fav_mes        =   esc_html__('remove from favorites','wpresidence');
                } 
            }
        print '<span class="icon-fav custom_fav '.esc_html($favorite_class).'" data-original-title="'.$fav_mes.'" data-postid="'.intval($propID).'"></span>';
        
        break;
        
               
        case 'compare':
         
          //    
            $compare   = wp_get_attachment_image_src(get_post_thumbnail_id(), 'slider_thumb');           
            if($text==''){
              
                if($icon!=''){
                    if ( strpos($icon, 'fa-') !== false){
                        print '<span class="compare-action text_compare" data-original-title="'.esc_html__('compare','wpresidence').'" data-pimage="';
                        if( isset($compare[0])){print esc_html($compare[0]);} 
                        print '" data-pid="'.intval($propID).'"><i class="fa '.$icon.'" aria-hidden="true"></i></span>';
                       
                    }else{
                        print '<span class="compare-action text_compare" data-original-title="'.esc_html__('compare','wpresidence').'" data-pimage="';
                        if( isset($compare[0])){print esc_html($compare[0]);} 
                        print '" data-pid="'.intval($propID).'"><img src="'.$icon.'" alt="featured_icon"></span>';
                    }
                }else{
                    print '<span class="compare-action" data-original-title="'.esc_html__('compare','wpresidence').'" data-pimage="';
                    if( isset($compare[0])){print esc_html($compare[0]);} 
                    print '" data-pid="'.intval($propID).'"></span>';
                }
                
            }else{
               print '<span class="compare-action text_compare" data-original-title="'.esc_html__('compare','wpresidence').'" data-pimage="';
               if( isset($compare[0])){print esc_html($compare[0]);} 
               print '" data-pid="'.intval($propID).'">'.$text.'</span>';
               
            }       
               
        break;
        
        
         case 'property_status':
            $prop_stat              =   esc_html( get_post_meta($propID, 'property_status', true) );
            if ($prop_stat != 'normal') {
                $ribbon_class = str_replace(' ', '-', $prop_stat);
                if (function_exists('icl_translate') ){
                    $prop_stat     =   icl_translate('wpestate','wp_estate_property_status'.$prop_stat, $prop_stat );
                }
                print stripslashes($prop_stat) ;
            }
        break;
        
        
    
            
        case 'icon':
            if ( strpos($icon, 'fa-') !== false){
                print '<i class="fa '.$icon.'" aria-hidden="true"></i>';
            }else{
                print '<img src="'.$icon.'" alt="featured_icon">';
            }
        break;
        
        
        
        case 'featured_icon':
            if(intval  ( get_post_meta($propID, 'prop_featured', true) )==1){
                
                if($text!=''){
                    print $text;
                }else{
                    if ( strpos($icon, 'fa-') !== false){
                        print '<i class="fa '.$icon.'" aria-hidden="true"></i>';
                    }else{
                        print '<img src="'.$icon.'" alt="featured_icon">';
                    }
                }
                
               
            }
        break;
        
        case 'text':
            if (function_exists('icl_translate') ){
                print stripslashes(str_replace('_',' ',$text));
            }else{
                $meta_value =stripslashes(str_replace('_',' ',$text));
                $meta_value = apply_filters( 'wpml_translate_single_string', $meta_value, 'wpestate', 'wp_estate_custom_unit_'.$meta_value );
                print $meta_value;
            }
        break;
        
        case 'image':
            wpestate_build_unit_show_detail_image($propID,$property_unit_slider);
        break;
    
        case 'description':
            print wpestate_strip_excerpt_by_char(get_the_excerpt(),115,$propID);
        break;
    
        case 'title':
            print '<h4><a href="'.get_permalink($propID).'">'.get_the_title($propID).'</a></h4>';
        break;
    
        case 'property_price':
            $currency                   =   esc_html( get_option('wp_estate_currency_symbol', '') );
            $where_currency             =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
            wpestate_show_price($propID,$currency,$where_currency);
        break;
    
        case 'property_category';
            print get_the_term_list($propID, 'property_category', '', ', ', '') ;
        break;
    
        case 'property_action_category';
            print get_the_term_list($propID, 'property_action_category', '', ', ', '') ;
        break;
        
        case 'property_city';
            print get_the_term_list($propID, 'property_city', '', ', ', '') ;
        break;
        
        case 'property_area';
            print get_the_term_list($propID, 'property_area', '', ', ', '') ;
        break;
        
        case 'property_county_state';
            print  get_the_term_list($propID, 'property_county_state', '', ', ', '') ;
        break;
        
        // 2018-09-12 Lorenzo M.
		// personalizzazione
		case 'property_agent';
			$agent_name = '';
			$agent_permalink = '';
			
            $agent_id   = intval( get_post_meta($propID, 'property_agent', true) );
			$author_id  = wpsestate_get_author($propID);
			
			if($agent_id != 0)
			{
				$agent_name = get_the_title($agent_id);
				$agent_permalink = get_permalink($agent_id);
			}
			
			if(empty($agent_name))
			{
				$first_name = get_the_author_meta( 'first_name' , $author_id );
				$last_name = get_the_author_meta( 'last_name' , $author_id );
				$agent_name = trim($first_name.' '.$last_name);				
			}
			
            if(strlen($agent_permalink)) 
			{
				print '<a href="'.$agent_permalink.'">'.$agent_name.'</a>';
			}
			else
			{
				print $agent_name;
			}
        break;
        
        // 2018-09-12 Lorenzo M.
		// personalizzazione
        case 'property_agent_picture';            
			/*
			// $agent_id   = intval( get_post_meta($propID, 'property_agent', true) );
			$preview    = wp_get_attachment_image_src(get_post_thumbnail_id($agent_id), 'agent_picture_thumb');
            $preview_img         = $preview[0];
            print '<a href="'.get_permalink($agent_id).'" class="property_unit_custom_agent_face" style="background-image:url('.$preview_img.')"></a>';
			 * 
			 */
			
			$author_id  = wpsestate_get_author($propID);
			print iklvb_get_user_image_cover($author_id,'property_unit_custom_agent_face');
        break;
        
        case 'custom_div';
            print '';
        break;
        case 'property_size';
            //print wpestate_sizes_no_format ( floatval ( get_post_meta($propID, 'property_size', true) ) );
            print wpestate_get_converted_measure( $propID, 'property_size' ); 
        break;
        default:
           
            if (function_exists('icl_translate') ){
                print  get_post_meta($propID, $element, true);
            }else{
                $meta_value = get_post_meta($propID, $element, true);;
                $meta_value = apply_filters( 'wpml_translate_single_string', $meta_value, 'wpestate', 'wp_estate_custom_unit_'.$meta_value );
                print $meta_value;
            }
    }
    
  
}
endif;
