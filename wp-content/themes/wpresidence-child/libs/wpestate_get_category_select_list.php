<?php



if( !function_exists('wpestate_get_category_select_list') ): 
    function wpestate_get_category_select_list($args){
        $categ_select_list  =   get_transient('wpestate_get_category_select_list');
        
        if($categ_select_list===false){

            $taxonomy           =   'property_category';
            $categories         =   get_terms($taxonomy,$args);

            $categ_select_list  =  '<li role="presentation" data-value="all">'. esc_html__('All Types','wpresidence').'</li>'; 

            foreach ($categories as $categ) {
                $counter = $categ->count;
                $received = wpestate_hierarchical_category_childen($taxonomy, $categ->term_id,$args ); 


                if(isset($received['count'])){
                    $counter = $counter+$received['count'];
                }

                // $categ_select_list     .=   '<li role="presentation" data-value="'.$categ->slug.'">'. ucwords ( urldecode( $categ->name ) ).' ('.$counter.')'.'</li>';
				
				// 2018-12-06 Lorenzo M.
                $categ_select_list     .=   '<li role="presentation" data-value="'.$categ->slug.'">'. ucwords ( urldecode( $categ->name ) ).'</li>';
				/* --- */				
				
                if(isset($received['html'])){
                    $categ_select_list     .=   $received['html'];  
                }

            }
            $transient_appendix='';
            if ( defined( 'ICL_LANGUAGE_CODE' ) ) {
                $transient_appendix.='_'. ICL_LANGUAGE_CODE;
            }
        set_transient('wpestate_get_category_select_list'.$transient_appendix,$categ_select_list,4*60*60);
        }
        return $categ_select_list;
    }
endif;




if( !function_exists('wpestate_hierarchical_category_childen') ): 
    function wpestate_hierarchical_category_childen($taxonomy, $cat,$args,$base=1,$level=1  ) {
        $level++;
        $args['parent']             =   $cat;
        $children                   =   get_terms($taxonomy,$args);
        $return_array=array();
        $total_main[$level]=0;
        $children_categ_select_list =   '';
        foreach ($children as $categ) {
            
            $area_addon =   '';
            $city_addon =   '';

            if($taxonomy=='property_city'){
				
				$term_meta      =   get_option( "taxonomy_$categ->term_id");
			
				$string_county  = '';
				if( isset( $term_meta['stateparent'] ) ){
					$string_county         =   wpestate_limit45 ( sanitize_title ( $term_meta['stateparent'] ) );  
				}
				$slug_county           =   sanitize_key($string_county);
				
				
                $string       =     wpestate_limit45 ( sanitize_title ( $categ->slug ) );              
                $slug         =     sanitize_key($string);
                $city_addon   =     '  data-parentcounty="' . $slug_county  . '" data-value2="'.$slug.'" ';
            }

            if($taxonomy=='property_area'){
                $term_meta    =   get_option( "taxonomy_$categ->term_id");
                $string       =   wpestate_limit45 ( sanitize_title ( $term_meta['cityparent'] ) );              
                $slug         =   sanitize_key($string);
                $area_addon   =   ' data-parentcity="' . $slug . '" ';

            }  
            
            $hold_base=  $base;
            $base_string='';
            $base++;
            $hold_base=  $base;
            
            if($level==2){
                $base_string='-';
            }else{
                $i=2;
                $base_string='';
                while( $i <= $level ){
                    $base_string.='-';
                    $i++;
                }
              
            }
    
            
            if($categ->parent!=0){
                $received =wpestate_hierarchical_category_childen( $taxonomy, $categ->term_id,$args,$base,$level ); 
            }
            
            
            $counter = $categ->count;
            if(isset($received['count'])){
                $counter = $counter+$received['count'];
            }
            
            // $children_categ_select_list     .=   '<li role="presentation" data-value="'.$categ->slug.'" '.$city_addon.' '.$area_addon.' > '.$base_string.' '. ucwords ( urldecode( $categ->name ) ).' ('.$counter.')'.'</li>';
			
			// 2018-12-06 Lorenzo M.
            $children_categ_select_list     .=   '<li role="presentation" data-value="'.$categ->slug.'" '.$city_addon.' '.$area_addon.' > '.$base_string.' '. ucwords ( urldecode( $categ->name ) ).'</li>';
           /* --- */
			
			
            if(isset($received['html'])){
                $children_categ_select_list     .=   $received['html'];  
            }
          
            $total_main[$level]=$total_main[$level]+$counter;
            
            $return_array['count']=$counter;
            $return_array['html']=$children_categ_select_list;
            
            
        }
      //  return $children_categ_select_list;
 
        $return_array['count']=$total_main[$level];
    
     
        return $return_array;
    }
endif;
