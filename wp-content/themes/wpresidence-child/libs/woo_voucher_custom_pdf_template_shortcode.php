<?php

/**
 * 2018-10-30	Lorenzo M.
 * 
 * pagina di riferimento della guida ufficiale del plugin
 * https://wpweb.co.in/documents/woocommerce-pdf-vouchers/shortcodes-support/
 * 
 */



/**
 * Adding Custom shortcode value in PDF voucher
 */

// Add filter to replace voucher template shortcodes in download pdf
add_filter('woo_vou_pdf_template_inner_html', 'woo_vou_pdf_template_replace_shortcodes', 10, 6 );

function woo_vou_pdf_template_replace_shortcodes( $voucher_template_html, $orderid, $item_key, $items, $voucodes, $productid ) 
{
	// formato data e ora impostati in wordpress
	$date_format = get_option('date_format');
	$time_format = get_option('time_format');
	
	// valuta impostata in woocommerce
	$currency = get_woocommerce_currency_symbol();
	
	// classe voucher
	$woo_vou = new WOO_Vou_Voucher();
	$woo_vou_id = $woo_vou->woo_vou_get_voucodeid_from_voucode($voucodes);
	
	// recupero dei valori per questa function
	$vou_expire_date = get_post_meta($woo_vou_id, '_woo_vou_exp_date', true);
	
	// date
	$expire_date_time = date_i18n($date_format, strtotime($vou_expire_date)).', '.date_i18n($time_format, strtotime($vou_expire_date));
	$cash_date = date_i18n($date_format, strtotime($vou_expire_date.' +'.CASHIN_DAY_RANGE.' days')).', '.date_i18n($time_format, strtotime($vou_expire_date.' +'.CASHIN_DAY_RANGE.' days'));
	
	
	// voglio id_prodotto nella lingua in cui si stampa e non la lingua al momento dell'acquisto
	$productid_locale = apply_filters( 'wpml_object_id', $productid, 'product', TRUE  );
	$product = wc_get_product( $productid_locale );
	
	// get values from product
	$product_price = $product->get_price().' '.$currency;
	$product_title = $product->get_title();
	$price_range_max = get_post_meta($productid_locale, 'price_range_max', true).' '.$currency;
	$action_type = get_post_meta($productid_locale, 'action_type', true);
	$action_type_text = __(str_replace('_',' ',$action_type), 'iklvb-wpresidence');
	$customer_user_id = get_post_meta($woo_vou_id, '_wpw_pcv_customer_user_id', true);
	$customer_name = iklvb_get_user_agent_name($customer_user_id);
	$event_ticket_type = get_post_meta($woo_vou_id, '_wpw_pcv_event_ticket_type', true);
	
	
	// Creating order object for order id
	// $woo_order = new WC_Order( $orderid );
	// $customer_name = iklvb_get_user_agent_name($woo_order->user_id);
		
	
	// replace shortcode on pdf - campi con valori
	$voucher_template_html = str_replace( '{product_title}', $product_title, $voucher_template_html );
	$voucher_template_html = str_replace( '{expire_date_time}', $expire_date_time, $voucher_template_html );
	$voucher_template_html = str_replace( '{product_price}', $product_price, $voucher_template_html );
	$voucher_template_html = str_replace( '{price_range_max}', $price_range_max, $voucher_template_html );
	$voucher_template_html = str_replace( '{action_type}', $action_type_text, $voucher_template_html );
	$voucher_template_html = str_replace( '{customer_name}', $customer_name, $voucher_template_html );
	$voucher_template_html = str_replace( '{cash_date}', $cash_date, $voucher_template_html );
	$voucher_template_html = str_replace( '{event_ticket_type}', $event_ticket_type, $voucher_template_html );

	// replace shortcode on pdf - label di solo testo
	$voucher_template_html = str_replace( '{label_ticket_expiry}',		__('Ticket expiry:','iklvb-wpresidence'), $voucher_template_html );
	$voucher_template_html = str_replace( '{label_valid}',				__('Valid:','iklvb-wpresidence'), $voucher_template_html );
	$voucher_template_html = str_replace( '{text_valid}',				__('worldwide','iklvb-wpresidence'), $voucher_template_html );
	$voucher_template_html = str_replace( '{label_up_to_the_price}',	__('Up to the price:','iklvb-wpresidence'), $voucher_template_html );
	$voucher_template_html = str_replace( '{label_category}',			__('Category:','iklvb-wpresidence'), $voucher_template_html );
	$voucher_template_html = str_replace( '{label_contract}',			__('Contract:','iklvb-wpresidence'), $voucher_template_html );	
	$voucher_template_html = str_replace( '{label_ticket_type}',		__('Ticket type:','iklvb-wpresidence'), $voucher_template_html );	
	$voucher_template_html = str_replace( '{label_price}',				__('Price:','iklvb-wpresidence'), $voucher_template_html );
	$voucher_template_html = str_replace( '{label_guest}',				__('Guest:','iklvb-wpresidence'), $voucher_template_html );
	$voucher_template_html = str_replace( '{label_cash_date}',			__('Cash out within:','iklvb-wpresidence'), $voucher_template_html );
	
  	return $voucher_template_html;
}