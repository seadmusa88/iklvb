<?php





/**
 * 2018-07-16 Lorenzo M.
 * 
 * versioni modificate per la gestione del save ajax dei dati utente personalizzati 
 * 
 */


////////////////////////////////////////////////////////////////////////////////
/// Ajax  upadte profile
////////////////////////////////////////////////////////////////////////////////
// for user 
add_action( 'wp_ajax_wpestate_ajax_update_profile', 'wpestate_ajax_update_profile' );  
if( !function_exists('wpestate_ajax_update_profile') )
{  
	function wpestate_ajax_update_profile()
	{
        $current_user           =   wp_get_current_user();
        $userID                 =   $current_user->ID;
        $user_login             =   $current_user->user_login;
        check_ajax_referer( 'profile_ajax_nonce', 'security-profile' );
        if ( !is_user_logged_in() ) {   
            exit('ko');
        }
        if($userID === 0 ){
            exit('out pls');
        }


        
        $allowed_html               =   array('</br>');
        $firstname                  =   sanitize_text_field ( wp_kses( $_POST['firstname'] ,$allowed_html) );
        $secondname                 =   sanitize_text_field ( wp_kses( $_POST['secondname'] ,$allowed_html) );
        $useremail                  =   sanitize_text_field ( wp_kses( $_POST['useremail'] ,$allowed_html) );
        $userphone                  =   sanitize_text_field ( wp_kses( $_POST['userphone'] ,$allowed_html) );
        $usermobile                 =   sanitize_text_field ( wp_kses( $_POST['usermobile'] ,$allowed_html) );
        $userskype                  =   sanitize_text_field ( wp_kses( $_POST['userskype'] ,$allowed_html) );
        $usertitle                  =   sanitize_text_field ( wp_kses( $_POST['usertitle'] ,$allowed_html) );
        $about_me                   =   wp_kses( $_POST['description'],$allowed_html );
        $profile_image_url_small    =   sanitize_text_field ( wp_kses($_POST['profile_image_url_small'],$allowed_html) );
        $profile_image_url          =   sanitize_text_field ( wp_kses($_POST['profile_image_url'],$allowed_html) );       
        $userfacebook               =   sanitize_text_field ( wp_kses( $_POST['userfacebook'],$allowed_html) );
        $usertwitter                =   sanitize_text_field ( wp_kses( $_POST['usertwitter'],$allowed_html) );
        $userlinkedin               =   sanitize_text_field ( wp_kses( $_POST['userlinkedin'],$allowed_html) );
        $userpinterest              =   sanitize_text_field ( wp_kses( $_POST['userpinterest'],$allowed_html ) );
        $userinstagram              =   sanitize_text_field ( wp_kses( $_POST['userinstagram'],$allowed_html ) );
        $userurl                    =   sanitize_text_field ( wp_kses( $_POST['userurl'],$allowed_html ) );
        $agent_category_submit      =   sanitize_text_field ( wp_kses( $_POST['agent_category_submit'],$allowed_html ) );
        $agent_action_submit        =   sanitize_text_field ( wp_kses( $_POST['agent_action_submit'],$allowed_html ) );
        $agent_city                 =   sanitize_text_field ( wp_kses( $_POST['agent_city'],$allowed_html ) );
        $agent_county               =   sanitize_text_field ( wp_kses( $_POST['agent_county'],$allowed_html ) );
        $agent_area                 =   sanitize_text_field ( wp_kses( $_POST['agent_area'],$allowed_html ) );     
        $agent_member               =   sanitize_text_field ( wp_kses( $_POST['agent_member'],$allowed_html ) );   
        
		$agent_custom_label         =   $_POST['agent_custom_label'];
		$agent_custom_value         =   $_POST['agent_custom_value'];
		
        $user_billing_first_name	=   sanitize_text_field ( wp_kses( $_POST['user_billing_first_name'], $allowed_html ) );   
        $user_billing_last_name		=   sanitize_text_field ( wp_kses( $_POST['user_billing_last_name'], $allowed_html ) );   
        $user_billing_address_1		=   sanitize_text_field ( wp_kses( $_POST['user_billing_address_1'], $allowed_html ) );   
        $user_billing_city			=   sanitize_text_field ( wp_kses( $_POST['user_billing_city'], $allowed_html ) );   
        $user_billing_postcode		=   sanitize_text_field ( wp_kses( $_POST['user_billing_postcode'], $allowed_html ) );   
        $user_billing_country		=   sanitize_text_field ( wp_kses( $_POST['user_billing_country'], $allowed_html ) );   
        $user_billing_state			=   sanitize_text_field ( wp_kses( $_POST['user_billing_state'], $allowed_html ) );   
        $user_billing_fiscal_code	=   sanitize_text_field ( wp_kses( $_POST['user_billing_fiscal_code'], $allowed_html ) );   
		
		// prex($_POST['user_billing_company']);
		
		// process fields data
		$agent_fields_array = array();
		for( $i=1; $i<count( $agent_custom_label  ); $i++ ){
			$agent_fields_array[] = array( 'label' => sanitize_text_field( $agent_custom_label[$i] ), 'value' => sanitize_text_field( $agent_custom_value[$i] ) );
		}
		
		
		
        update_user_meta( $userID, 'first_name', $firstname ) ;
        update_user_meta( $userID, 'last_name',  $secondname) ;
        update_user_meta( $userID, 'phone' , $userphone) ;
        update_user_meta( $userID, 'skype' , $userskype) ;
        update_user_meta( $userID, 'title', $usertitle) ;
        update_user_meta( $userID, 'custom_picture',$profile_image_url);
        update_user_meta( $userID, 'small_custom_picture',$profile_image_url_small);     
        update_user_meta( $userID, 'mobile' , $usermobile) ;
        
        
        
        update_user_meta( $userID, 'facebook' , $userfacebook) ;
        update_user_meta( $userID, 'twitter' , $usertwitter) ;
        update_user_meta( $userID, 'linkedin' , $userlinkedin) ;
        update_user_meta( $userID, 'pinterest' , $userpinterest) ;
        update_user_meta( $userID, 'instagram' , $userinstagram) ;
        update_user_meta( $userID, 'description' , $about_me) ;
        update_user_meta( $userID, 'website' , $userurl) ;
        

		
        update_user_meta( $userID, 'billing_first_name', $user_billing_first_name );
        update_user_meta( $userID, 'billing_last_name', $user_billing_last_name );
        update_user_meta( $userID, 'billing_address_1', $user_billing_address_1 );
        update_user_meta( $userID, 'billing_city', $user_billing_city );
        update_user_meta( $userID, 'billing_postcode', $user_billing_postcode );
        update_user_meta( $userID, 'billing_country', $user_billing_country );
        update_user_meta( $userID, 'billing_state', $user_billing_state );
        update_user_meta( $userID, 'billing_fiscal_code', strtoupper($user_billing_fiscal_code) );
        
        
        $agent_id=get_user_meta( $userID, 'user_agent_id',true);
     
	 
		update_post_meta( $agent_id, 'agent_custom_data' , $agent_fields_array) ;
	 
        wpestate_update_user_agent ($agent_member,$agent_category_submit,$agent_action_submit,$agent_city,$agent_county,$agent_area,$userurl,$agent_id, $firstname ,$secondname ,$useremail,$userphone,$userskype,$usertitle,$profile_image_url,$usermobile,$about_me,$profile_image_url_small,$userfacebook,$usertwitter,$userlinkedin,$userpinterest,$userinstagram);
       
        
        if( $current_user->user_email != $useremail ) {
            $user_id=email_exists( $useremail ) ;
            if ( $user_id){
                _e('The email was not saved because it is used by another user.</br>','wpestate');
            } else{
                $args = array(
                    'ID'         => $userID,
                    'user_email' => $useremail
                ); 
                wp_update_user( $args );
            } 
        }
        
        $arguments=array(
            'user_profile'      =>  $user_login,
        );

        wpestate_select_email_type(get_option('admin_email'),'agent_update_profile',$arguments);
        _e('Profile updated','wpestate');
        die(); 
   }
} // end   wpestate_ajax_update_profile 






////////////////////////////////////////////////////////////////////////////////
/// Ajax  upadte agent profile agent
////////////////////////////////////////////////////////////////////////////////
// for user 
add_action( 'wp_ajax_wpestate_ajax_update_profile_agent', 'wpestate_ajax_update_profile_agent' );  
if( !function_exists('wpestate_ajax_update_profile_agent') )
{  
	function wpestate_ajax_update_profile_agent()
	{
        $current_user           =   wp_get_current_user();
        $userID                 =   $current_user->ID;
        $user_login             =   $current_user->user_login;
        check_ajax_referer( 'profile_ajax_nonce', 'security-profile' );
        if ( !is_user_logged_in() ) {   
            exit('ko');
        }
        if($userID === 0 ){
            exit('out pls');
        }

        
        $allowed_html               =   array('</br>');
        $firstname                  =   sanitize_text_field ( wp_kses( $_POST['firstname'] ,$allowed_html) );
        $secondname                 =   sanitize_text_field ( wp_kses( $_POST['secondname'] ,$allowed_html) );
        $useremail                  =   sanitize_text_field ( wp_kses( $_POST['useremail'] ,$allowed_html) );
        $userphone                  =   sanitize_text_field ( wp_kses( $_POST['userphone'] ,$allowed_html) );
        $usermobile                 =   sanitize_text_field ( wp_kses( $_POST['usermobile'] ,$allowed_html) );
        $userskype                  =   sanitize_text_field ( wp_kses( $_POST['userskype'] ,$allowed_html) );
        $usertitle                  =   sanitize_text_field ( wp_kses( $_POST['usertitle'] ,$allowed_html) );
        $about_me                   =   wp_kses( $_POST['description'],$allowed_html );
        $profile_image_url_small    =   sanitize_text_field ( wp_kses($_POST['profile_image_url_small'],$allowed_html) );
        $profile_image_url          =   sanitize_text_field ( wp_kses($_POST['profile_image_url'],$allowed_html) );       
        $userfacebook               =   sanitize_text_field ( wp_kses( $_POST['userfacebook'],$allowed_html) );
        $usertwitter                =   sanitize_text_field ( wp_kses( $_POST['usertwitter'],$allowed_html) );
        $userlinkedin               =   sanitize_text_field ( wp_kses( $_POST['userlinkedin'],$allowed_html) );
        $userpinterest              =   sanitize_text_field ( wp_kses( $_POST['userpinterest'],$allowed_html ) );
        $userinstagram              =   sanitize_text_field ( wp_kses( $_POST['userinstagram'],$allowed_html ) );
        $userurl                    =   sanitize_text_field ( wp_kses( $_POST['userurl'],$allowed_html ) );
        $agent_category_submit      =   sanitize_text_field ( wp_kses( $_POST['agent_category_submit'],$allowed_html ) );
        $agent_action_submit        =   sanitize_text_field ( wp_kses( $_POST['agent_action_submit'],$allowed_html ) );
        $agent_city                 =   sanitize_text_field ( wp_kses( $_POST['agent_city'],$allowed_html ) );
        $agent_county               =   sanitize_text_field ( wp_kses( $_POST['agent_county'],$allowed_html ) );
        $agent_area                 =   sanitize_text_field ( wp_kses( $_POST['agent_area'],$allowed_html ) );     
        $agent_member               =   sanitize_text_field ( wp_kses( $_POST['agent_member'],$allowed_html ) );   
        
		$agent_custom_label         =   $_POST['agent_custom_label'];
		$agent_custom_value         =   $_POST['agent_custom_value'];
		
        $user_billing_company		=   sanitize_text_field ( wp_kses( $_POST['user_billing_company'], $allowed_html ) );   
        $user_billing_address_1		=   sanitize_text_field ( wp_kses( $_POST['user_billing_address_1'], $allowed_html ) );   
        $user_billing_city			=   sanitize_text_field ( wp_kses( $_POST['user_billing_city'], $allowed_html ) );   
        $user_billing_postcode		=   sanitize_text_field ( wp_kses( $_POST['user_billing_postcode'], $allowed_html ) );   
        $user_billing_country		=   sanitize_text_field ( wp_kses( $_POST['user_billing_country'], $allowed_html ) );   
        $user_billing_state			=   sanitize_text_field ( wp_kses( $_POST['user_billing_state'], $allowed_html ) );   
		$user_billing_fiscal_code	=   sanitize_text_field ( wp_kses( $_POST['user_billing_fiscal_code'], $allowed_html ) );   
        $user_billing_vat			=   sanitize_text_field ( wp_kses( $_POST['user_billing_vat'], $allowed_html ) );   
		
		// prex($_POST['user_billing_company']);
		
		// process fields data
		$agent_fields_array = array();
		for( $i=1; $i<count( $agent_custom_label  ); $i++ ){
			$agent_fields_array[] = array( 'label' => sanitize_text_field( $agent_custom_label[$i] ), 'value' => sanitize_text_field( $agent_custom_value[$i] ) );
		}
		
		
		
        update_user_meta( $userID, 'first_name', $firstname ) ;
        update_user_meta( $userID, 'last_name',  $secondname) ;
        update_user_meta( $userID, 'phone' , $userphone) ;
        update_user_meta( $userID, 'skype' , $userskype) ;
        update_user_meta( $userID, 'title', $usertitle) ;
        update_user_meta( $userID, 'custom_picture',$profile_image_url);
        update_user_meta( $userID, 'small_custom_picture',$profile_image_url_small);     
        update_user_meta( $userID, 'mobile' , $usermobile) ;
        
        
        
        update_user_meta( $userID, 'facebook' , $userfacebook) ;
        update_user_meta( $userID, 'twitter' , $usertwitter) ;
        update_user_meta( $userID, 'linkedin' , $userlinkedin) ;
        update_user_meta( $userID, 'pinterest' , $userpinterest) ;
        update_user_meta( $userID, 'instagram' , $userinstagram) ;
        update_user_meta( $userID, 'description' , $about_me) ;
        update_user_meta( $userID, 'website' , $userurl) ;
        

		
        update_user_meta( $userID, 'billing_company', $user_billing_company );
        update_user_meta( $userID, 'billing_address_1', $user_billing_address_1 );
        update_user_meta( $userID, 'billing_city', $user_billing_city );
        update_user_meta( $userID, 'billing_postcode', $user_billing_postcode );
        update_user_meta( $userID, 'billing_country', $user_billing_country );
        update_user_meta( $userID, 'billing_state', $user_billing_state );
		update_user_meta( $userID, 'billing_fiscal_code', strtoupper($user_billing_fiscal_code) );
        update_user_meta( $userID, 'billing_vat', $user_billing_vat );
        
        
        $agent_id=get_user_meta( $userID, 'user_agent_id',true);
     
	 
		update_post_meta( $agent_id, 'agent_custom_data' , $agent_fields_array) ;
	 
        wpestate_update_user_agent ($agent_member,$agent_category_submit,$agent_action_submit,$agent_city,$agent_county,$agent_area,$userurl,$agent_id, $firstname ,$secondname ,$useremail,$userphone,$userskype,$usertitle,$profile_image_url,$usermobile,$about_me,$profile_image_url_small,$userfacebook,$usertwitter,$userlinkedin,$userpinterest,$userinstagram);
       
        
        if( $current_user->user_email != $useremail ) {
            $user_id=email_exists( $useremail ) ;
            if ( $user_id){
                _e('The email was not saved because it is used by another user.</br>','wpestate');
            } else{
                $args = array(
                    'ID'         => $userID,
                    'user_email' => $useremail
                ); 
                wp_update_user( $args );
            } 
        }
        
        $arguments=array(
            'user_profile'      =>  $user_login,
        );

        wpestate_select_email_type(get_option('admin_email'),'agent_update_profile',$arguments);
        _e('Profile updated','wpestate');
        die(); 
   }
} // end   wpestate_ajax_update_profile agent






////////////////////////////////////////////////////////////////////////////////
/// Ajax  update profile agency
////////////////////////////////////////////////////////////////////////////////
add_action( 'wp_ajax_wpestate_ajax_update_profile_agency', 'wpestate_ajax_update_profile_agency' );  
if( !function_exists('wpestate_ajax_update_profile_agency') )
{
	function wpestate_ajax_update_profile_agency()
	{
        $current_user           =   wp_get_current_user();
        $userID                 =   $current_user->ID;
        $user_login             =   $current_user->user_login;
        check_ajax_referer( 'profile_ajax_nonce', 'security-profile' );
        if ( !is_user_logged_in() ) {   
            exit('ko');
        }
        if($userID === 0 ){
            exit('out pls');
        }
        $user_role = intval (get_user_meta( $current_user->ID, 'user_estate_role', true) );
        
        if($user_role!=3){
            exit('not the right role');
        }
        
		$agent_id=get_user_meta( $userID, 'user_agent_id',true);

		$allowed_html               =   array('</br>');
		
		$firstname                  =   sanitize_text_field ( wp_kses( $_POST['firstname'] ,$allowed_html) );
        $secondname                 =   sanitize_text_field ( wp_kses( $_POST['secondname'] ,$allowed_html) );
		
        $agency_name                =   sanitize_text_field ( wp_kses( $_POST['agency_name'] ,$allowed_html) );
        $useremail                  =   sanitize_text_field ( wp_kses( $_POST['useremail'] ,$allowed_html) );
        $userphone                  =   sanitize_text_field ( wp_kses( $_POST['userphone'] ,$allowed_html) );
        $usermobile                 =   sanitize_text_field ( wp_kses( $_POST['usermobile'] ,$allowed_html) );
        $userskype                  =   sanitize_text_field ( wp_kses( $_POST['userskype'] ,$allowed_html) );
        // $usertitle                  =   sanitize_text_field ( wp_kses( $_POST['usertitle'] ,$allowed_html) );
        $about_me                   =   wp_kses( $_POST['description'],$allowed_html );
        $profile_image_url_small    =   sanitize_text_field ( wp_kses($_POST['profile_image_url_small'],$allowed_html) );
        $profile_image_url          =   sanitize_text_field ( wp_kses($_POST['profile_image_url'],$allowed_html) );       
        $userfacebook               =   sanitize_text_field ( wp_kses( $_POST['userfacebook'],$allowed_html) );
        $usertwitter                =   sanitize_text_field ( wp_kses( $_POST['usertwitter'],$allowed_html) );
        $userlinkedin               =   sanitize_text_field ( wp_kses( $_POST['userlinkedin'],$allowed_html) );
        $userpinterest              =   sanitize_text_field ( wp_kses( $_POST['userpinterest'],$allowed_html ) );
        $userinstagram              =   sanitize_text_field ( wp_kses( $_POST['userinstagram'],$allowed_html ) );
        // $userurl                    =   sanitize_text_field ( wp_kses( $_POST['userurl'],$allowed_html ) );         
        $agency_languages           =   sanitize_text_field ( wp_kses( $_POST['agency_languages'],$allowed_html ) );
        $agency_website             =   sanitize_text_field ( wp_kses( $_POST['agency_website'],$allowed_html ) );
        $agency_taxes               =   sanitize_text_field ( wp_kses( $_POST['agency_taxes'],$allowed_html ) );     
        $agency_category_submit     =   sanitize_text_field ( wp_kses( $_POST['agency_category_submit'],$allowed_html ) );
        $agency_action_submit       =   sanitize_text_field ( wp_kses( $_POST['agency_action_submit'],$allowed_html ) );
        $agency_city                =   sanitize_text_field ( wp_kses( $_POST['agency_city'],$allowed_html ) );
        $agency_county              =   sanitize_text_field ( wp_kses( $_POST['agency_county'],$allowed_html ) );
        $agency_area                =   sanitize_text_field ( wp_kses( $_POST['agency_area'],$allowed_html ) );     
        $agency_address             =   sanitize_text_field ( wp_kses( $_POST['agency_address'],$allowed_html ) );
        $agency_lat                 =   sanitize_text_field ( wp_kses( $_POST['agency_lat'],$allowed_html ) );
        $agency_long                =   sanitize_text_field ( wp_kses( $_POST['agency_long'],$allowed_html ) );
        $agency_opening_hours       =   sanitize_text_field ( wp_kses( $_POST['agency_opening_hours'],$allowed_html ) );
        
	    $user_billing_company		=   sanitize_text_field ( wp_kses( $_POST['user_billing_company'], $allowed_html ) );   
        $user_billing_address_1		=   sanitize_text_field ( wp_kses( $_POST['user_billing_address_1'], $allowed_html ) );   
        $user_billing_city			=   sanitize_text_field ( wp_kses( $_POST['user_billing_city'], $allowed_html ) );   
        $user_billing_postcode		=   sanitize_text_field ( wp_kses( $_POST['user_billing_postcode'], $allowed_html ) );   
        $user_billing_country		=   sanitize_text_field ( wp_kses( $_POST['user_billing_country'], $allowed_html ) );   
        $user_billing_state			=   sanitize_text_field ( wp_kses( $_POST['user_billing_state'], $allowed_html ) );   
        $user_billing_vat			=   sanitize_text_field ( wp_kses( $_POST['user_billing_vat'], $allowed_html ) );   
		
		$agent_id=get_user_meta( $userID, 'user_agent_id',true);
       
       
        update_user_meta( $userID, 'custom_picture',$profile_image_url);
        update_user_meta( $userID, 'small_custom_picture',$profile_image_url_small);     
    
        
        
         if($firstname!=='' || $secondname!='' ){
            $post = array(
                    'ID'            => $agent_id,
                    'post_title'    => $agency_name,
                    'post_content'  => $about_me,
            );
            $post_id =  wp_update_post($post );  
        }
    
		
     
        update_post_meta($agent_id, 'agency_firstname',   $firstname);
        update_post_meta($agent_id, 'agency_lastname',   $secondname);
		
        update_post_meta($agent_id, 'agency_email',   $useremail);
        update_post_meta($agent_id, 'agency_phone',   $userphone);
        update_post_meta($agent_id, 'agency_mobile',  $usermobile);
        update_post_meta($agent_id, 'agency_skype',   $userskype);
        update_post_meta($agent_id, 'agency_opening_hours',   $agency_opening_hours);
        
        update_post_meta($agent_id, 'agency_facebook',   $userfacebook);
        update_post_meta($agent_id, 'agency_twitter',   $usertwitter);
        update_post_meta($agent_id, 'agency_linkedin',   $userlinkedin);
        update_post_meta($agent_id, 'agency_pinterest',   $userpinterest);
        update_post_meta($agent_id, 'agency_instagram',   $userinstagram);
        update_post_meta($agent_id, 'agency_languages',   $agency_languages);
        update_post_meta($agent_id, 'agency_website',   $agency_website);
        update_post_meta($agent_id, 'agency_taxes',   $agency_taxes);
        update_post_meta($agent_id, 'agency_address',   $agency_address);
        update_post_meta($agent_id, 'agency_lat',   $agency_lat);
        update_post_meta($agent_id, 'agency_long',   $agency_long);
     
		
        update_user_meta($userID, 'first_name', $firstname );
        update_user_meta($userID, 'last_name',  $secondname);
		
        update_user_meta($userID, 'billing_company', $user_billing_company );
        update_user_meta($userID, 'billing_address_1', $user_billing_address_1 );
        update_user_meta($userID, 'billing_city', $user_billing_city );
        update_user_meta($userID, 'billing_postcode', $user_billing_postcode );
        update_user_meta($userID, 'billing_country', $user_billing_country );
        update_user_meta($userID, 'billing_state', $user_billing_state );
        update_user_meta($userID, 'billing_vat', $user_billing_vat );

  
        $agency_category           =   get_term( $agency_category_submit, 'category_agency');     
        if(isset($agency_category->term_id)){
            $agency_category_submit  =   $agency_category->name;
        }else{
            $agency_category_submit=-1;
        }
        
        if( isset($agency_category_submit) && $agency_category_submit!='none' ){
            wp_set_object_terms($agent_id,$agency_category_submit,'category_agency'); 
        }  

        
        
        
        
        $agency_category           =   get_term( $agency_action_submit, 'action_category_agency');     
        if(isset($agency_category->term_id)){
            $agency_action_submit  =   $agency_category->name;
        }else{
            $agency_action_submit=-1;
        }
        
        if( isset($agency_action_submit) && $agency_action_submit!='none' ){
            wp_set_object_terms($agent_id,$agency_action_submit,'action_category_agency'); 
        }  

        
        if( isset($agency_city) && $agency_city!='none' ){
            wp_set_object_terms($agent_id,$agency_city,'city_agency'); 
        }  
        
        if( isset($agency_area) && $agency_area!='none' ){
            wp_set_object_terms($agent_id,$agency_area,'area_agency'); 
        }  
        
        if( isset($agency_county) && $agency_county!='none' ){
            wp_set_object_terms($agent_id,$agency_county,'county_state_agency'); 
        }  

     

     
        set_post_thumbnail( $agent_id, $profile_image_url_small );

        
        
        
        
        
        if( $current_user->user_email != $useremail ) {
            $user_id=email_exists( $useremail ) ;
            if ( $user_id){
                _e('The email was not saved because it is used by another user.</br>','wpestate');
            } else{
                $args = array(
                    'ID'         => $userID,
                    'user_email' => $useremail
                ); 
                wp_update_user( $args );
            } 
        }
        
        $arguments=array(
            'user_profile'      =>  $user_login,
        );

        wpestate_select_email_type(get_option('admin_email'),'agent_update_profile',$arguments);
        _e('Profile updated','wpestate');
        die(); 
   }
} // end   wpestate_ajax_update_profile agency






////////////////////////////////////////////////////////////////////////////////
/// Ajax  update profile developer
////////////////////////////////////////////////////////////////////////////////
add_action( 'wp_ajax_wpestate_ajax_update_profile_developer', 'wpestate_ajax_update_profile_developer' );  
if( !function_exists('wpestate_ajax_update_profile_developer') )
{   
   function wpestate_ajax_update_profile_developer()
	{
        $current_user           =   wp_get_current_user();
        $userID                 =   $current_user->ID;
        $user_login             =   $current_user->user_login;
        check_ajax_referer( 'profile_ajax_nonce', 'security-profile' );
        if ( !is_user_logged_in() ) {   
            exit('ko');
        }
        if($userID === 0 ){
            exit('out pls');
        }
        $user_role = intval (get_user_meta( $current_user->ID, 'user_estate_role', true) );
        
        if($user_role!=4){
            exit('not the right role');
        }
        
        $developer_id=get_user_meta( $userID, 'user_agent_id',true);

        
        $allowed_html               =   array('</br>');
		
		$firstname                  =   sanitize_text_field ( wp_kses( $_POST['firstname'] ,$allowed_html) );
        $secondname                 =   sanitize_text_field ( wp_kses( $_POST['secondname'] ,$allowed_html) );
		
        $developer_name             =   sanitize_text_field ( wp_kses( $_POST['developer_name'] ,$allowed_html) );
        $useremail                  =   sanitize_text_field ( wp_kses( $_POST['useremail'] ,$allowed_html) );
        $userphone                  =   sanitize_text_field ( wp_kses( $_POST['userphone'] ,$allowed_html) );
        $usermobile                 =   sanitize_text_field ( wp_kses( $_POST['usermobile'] ,$allowed_html) );
        $userskype                  =   sanitize_text_field ( wp_kses( $_POST['userskype'] ,$allowed_html) );
        // $usertitle                  =   sanitize_text_field ( wp_kses( $_POST['usertitle'] ,$allowed_html) );
        $about_me                   =   wp_kses( $_POST['description'],$allowed_html );
        $profile_image_url_small    =   sanitize_text_field ( wp_kses($_POST['profile_image_url_small'],$allowed_html) );
        $profile_image_url          =   sanitize_text_field ( wp_kses($_POST['profile_image_url'],$allowed_html) );       
        $userfacebook               =   sanitize_text_field ( wp_kses( $_POST['userfacebook'],$allowed_html) );
        $usertwitter                =   sanitize_text_field ( wp_kses( $_POST['usertwitter'],$allowed_html) );
        $userlinkedin               =   sanitize_text_field ( wp_kses( $_POST['userlinkedin'],$allowed_html) );
        $userpinterest              =   sanitize_text_field ( wp_kses( $_POST['userpinterest'],$allowed_html ) );
        $userinstagram              =   sanitize_text_field ( wp_kses( $_POST['userinstagram'],$allowed_html ) );
        // $userurl                 =   sanitize_text_field ( wp_kses( $_POST['userurl'],$allowed_html ) );         
        $developer_languages        =   sanitize_text_field ( wp_kses( $_POST['developer_languages'],$allowed_html ) );
        $developer_website          =   sanitize_text_field ( wp_kses( $_POST['developer_website'],$allowed_html ) );
        $developer_taxes            =   sanitize_text_field ( wp_kses( $_POST['developer_taxes'],$allowed_html ) );     
        $developer_category_submit  =   sanitize_text_field ( wp_kses( $_POST['developer_category_submit'],$allowed_html ) );
        $developer_action_submit    =   sanitize_text_field ( wp_kses( $_POST['developer_action_submit'],$allowed_html ) );
        $developer_city             =   sanitize_text_field ( wp_kses( $_POST['developer_city'],$allowed_html ) );
        $developer_county           =   sanitize_text_field ( wp_kses( $_POST['developer_county'],$allowed_html ) );
        $developer_area             =   sanitize_text_field ( wp_kses( $_POST['developer_area'],$allowed_html ) );     
        $developer_address          =   sanitize_text_field ( wp_kses( $_POST['developer_address'],$allowed_html ) );
        $developer_lat              =   sanitize_text_field ( wp_kses( $_POST['developer_lat'],$allowed_html ) );
        $developer_long             =   sanitize_text_field ( wp_kses( $_POST['developer_long'],$allowed_html ) );
       
        $user_billing_company		=   sanitize_text_field ( wp_kses( $_POST['user_billing_company'], $allowed_html ) );   
        $user_billing_address_1		=   sanitize_text_field ( wp_kses( $_POST['user_billing_address_1'], $allowed_html ) );   
        $user_billing_city			=   sanitize_text_field ( wp_kses( $_POST['user_billing_city'], $allowed_html ) );   
        $user_billing_postcode		=   sanitize_text_field ( wp_kses( $_POST['user_billing_postcode'], $allowed_html ) );   
        $user_billing_country		=   sanitize_text_field ( wp_kses( $_POST['user_billing_country'], $allowed_html ) );   
        $user_billing_state			=   sanitize_text_field ( wp_kses( $_POST['user_billing_state'], $allowed_html ) );   
        $user_billing_vat			=   sanitize_text_field ( wp_kses( $_POST['user_billing_vat'], $allowed_html ) );   
		
      
        $developer_id=get_user_meta($userID,'user_agent_id',true);
        
        update_user_meta( $userID, 'custom_picture',$profile_image_url);
        update_user_meta( $userID, 'small_custom_picture',$profile_image_url_small);     
    
        
        
         if($firstname!=='' || $secondname!='' ){
            $post = array(
                    'ID'            => $developer_id,
                    'post_title'    => $developer_name,
                    'post_content'  => $about_me,
            );
            $post_id =  wp_update_post($post );  
        }
    
		update_post_meta($developer_id, 'agency_firstname',   $firstname);
        update_post_meta($developer_id, 'agency_lastname',   $secondname);
            
        update_post_meta($developer_id, 'developer_email',   $useremail);
        update_post_meta($developer_id, 'developer_phone',   $userphone);
        update_post_meta($developer_id, 'developer_mobile',  $usermobile);
        update_post_meta($developer_id, 'developer_skype',   $userskype);

        update_post_meta($developer_id, 'developer_facebook',   $userfacebook);
        update_post_meta($developer_id, 'developer_twitter',   $usertwitter);
        update_post_meta($developer_id, 'developer_linkedin',   $userlinkedin);
        update_post_meta($developer_id, 'developer_pinterest',   $userpinterest);
        update_post_meta($developer_id, 'developer_instagram',   $userinstagram);
        update_post_meta($developer_id, 'developer_languages',   $developer_languages);
        update_post_meta($developer_id, 'developer_website',   $developer_website);
        update_post_meta($developer_id, 'developer_taxes',   $developer_taxes);
        update_post_meta($developer_id, 'developer_address',   $developer_address);
        update_post_meta($developer_id, 'developer_lat',   $developer_lat);
        update_post_meta($developer_id, 'developer_long',   $developer_long);
        
		update_user_meta($userID, 'first_name', $firstname );
        update_user_meta($userID, 'last_name',  $secondname);
		
        update_user_meta($userID, 'billing_company', $user_billing_company );
        update_user_meta($userID, 'billing_address_1', $user_billing_address_1 );
        update_user_meta($userID, 'billing_city', $user_billing_city );
        update_user_meta($userID, 'billing_postcode', $user_billing_postcode );
        update_user_meta($userID, 'billing_country', $user_billing_country );
        update_user_meta($userID, 'billing_state', $user_billing_state );
        update_user_meta($userID, 'billing_vat', $user_billing_vat );
 

  
        $developer_category           =   get_term( $developer_category_submit, 'property_category_developer');     
        if(isset($developer_category->term_id)){
            $developer_category_submit  =   $developer_category->name;
        }else{
            $developer_category_submit=-1;
        }
        
        if( isset($developer_category_submit) && $developer_category_submit!='none' ){
            wp_set_object_terms($developer_id,$developer_category_submit,'property_category_developer'); 
        }  

        
        
        
        
        $developer_category           =   get_term( $developer_action_submit, 'property_action_developer');     
        if(isset($developer_category->term_id)){
            $developer_action_submit  =   $developer_category->name;
        }else{
            $developer_action_submit=-1;
        }
        
        if( isset($developer_action_submit) && $developer_action_submit!='none' ){
            wp_set_object_terms($developer_id,$developer_action_submit,'property_action_developer'); 
        }  

        
        if( isset($developer_city) && $developer_city!='none' ){
            wp_set_object_terms($developer_id,$developer_city,'property_city_developer'); 
        }  
        
        if( isset($developer_area) && $developer_area!='none' ){
            wp_set_object_terms($developer_id,$developer_area,'property_area_developer'); 
        }  
        
        if( isset($developer_county) && $developer_county!='none' ){
            wp_set_object_terms($developer_id,$developer_county,'property_county_state_developer'); 
        }  

     

     
        set_post_thumbnail( $developer_id, $profile_image_url_small );

        
        
        
        
        
        if( $current_user->user_email != $useremail ) {
            $user_id=email_exists( $useremail ) ;
            if ( $user_id){
                _e('The email was not saved because it is used by another user.</br>','wpestate');
            } else{
                $args = array(
                    'ID'         => $userID,
                    'user_email' => $useremail
                ); 
                wp_update_user( $args );
            } 
        }
        
        $arguments=array(
            'user_profile'      =>  $user_login,
        );

        wpestate_select_email_type(get_option('admin_email'),'agent_update_profile',$arguments);
        _e('Profile updated','wpestate');
        die(); 
   }
} // end   wpestate_ajax_update_profile developer
