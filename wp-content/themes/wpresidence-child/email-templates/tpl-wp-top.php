<html>
	<head>
	</head>
	<body style="height: 100%;margin: 0;padding: 0;background-color: #ffffff;width: 100% !important;">

		<!-- MAIN WRAPPER -->
		<div id="main-wrapper" style="max-width: 640px;display: block;margin: 0px auto 0px auto;padding: 20px 0;background-color: #ffffff;">
			
			<!-- HEADER -->
			<div id="main-header">
				<table width="100%" style="width: 100%;margin: 0;padding: 0;">
					<tr>
						<td width="50%" style="margin: 0;padding: 0;">
							<?php if(isset($email_header_left)) echo $email_header_left; ?>
						</td>
						<td width="50%" style="margin: 0;padding: 0; text-align: right;">
							<?php if(isset($email_header_right)) echo $email_header_right; ?>
						</td>
					</tr>
				</table>
			</div>
			<!-- /HEADER -->

			<!-- BODY -->
			<div id="main-body" style="width: 100%;font-family: Arial, sans-serif;min-height: 200px;margin: 20px 0;">
				<div class="separator" style="height: 1px;background-color: #ebebeb; margin: 5px 0;"></div>
				
				<!-- start email content -->
				


