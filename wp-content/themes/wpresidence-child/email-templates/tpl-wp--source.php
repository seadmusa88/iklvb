<html>
	<head>
		<style>
		body {
			width:100% !important; 
			height:100%; 
			margin:0; 
			padding:0; 
			background-color:#FFF;
		}
		p {
			font-family:Arial, sans-serif; 
			font-weight:normal; 
			font-size:12pt; 
			line-height:1.4; 
			margin:0 0 10px 0; 
			padding:0;
		}
		a {
			color:#0084B4; 
			text-decoration:none;
		}
		table {
			width:100%; 
			margin:0; 
			padding:0;
		}
		td {
			margin:0; 
			padding:0;
		}
		#main-wrapper {
			max-width:640px; 
			display:block; 
			margin:0px auto 0px auto; 
			padding:20px 0px; 
			background-color:#FFFFFF;
		}
		#main-body {
			width:100%;
			font-family:Arial, sans-serif;
			min-height:200px;
			margin:20px 0;
		}
		#main-footer p {
			font-size:11pt;
			color:#909090;
		}
		.separator {
			height:1px;
			background-color:#ebebeb;
			margin:5px 0;
		}
		</style>
	</head>
	<body>

		<!-- MAIN WRAPPER -->
		<div id="main-wrapper">
			
			<!-- HEADER -->
			<div id="main-header">
				<table width="100%">
					<tr>
						<td width="50%">
							<?php if(isset($email_header_left)) echo $email_header_left; ?>
						</td>
						<td width="50%" style="text-align: right;">
							<?php if(isset($email_header_right)) echo $email_header_right; ?>
						</td>
					</tr>
				</table>
			</div>
			<!-- /HEADER -->

			<!-- BODY -->
			<div id="main-body">
				<div class="separator"></div>
				
				<!-- start email content -->
				<!-- end email content -->
				
			</div>
			<!-- /BODY -->

			<!-- FOOTER -->
			<div id="main-footer">
				<p>© 2018 iKLVB SRLS, 2 Piazza Toscana, Pieve Emanuele, MILANO 20090, ITALIA</p>
			</div>
			<!-- /FOOTER -->
		
		</div>
		<!-- /MAIN WRAPPER -->
		
	</body>
</html>
