<?php



// logo nelle email
$email_header_left = '<img src="'.get_stylesheet_directory_uri().'/img/iklvb-logo-email.png" class="width:100%;max-width:100px;margin:0;padding:0;">';

// messaggio in alto a destra
// opzionale: compare solo quando utilizzato
$email_header_right = '';




/**
 * 2018-09-09 Lorenzo M.
 * 
 * template per TUTTE le email inviate da Wordpress
 * 
 * source:
 * https://developer.wordpress.org/reference/hooks/wp_mail/
 * 
 */
add_filter('wp_mail','iklvb_add_email_template', 20, 1);
function iklvb_add_email_template($args)
{
	// variabili global usate nel template della email
	global $email_header_left;
	global $email_header_right;

	ob_start();
	include(WPR_CHILD_DIR.'/email-templates/tpl-wp-top.php');
    echo $args['message'];
	include(WPR_CHILD_DIR.'/email-templates/tpl-wp-bottom.php');
	$message = ob_get_contents();
    ob_end_clean();
	
	$args['message'] = $message;
	
    return $args;
  }



/**
 * 2018-09-08 Lorenzo M.
 * 
 * preparo titolo e testo per le email da inviare
 * 
 */
function iklvb_get_email_parts($_tpl_slug, $_args = array())
{
	// controlli
	if(!isset($_args['visitor_id'])) $_args['visitor_id'] = '';
	if(!isset($_args['owner_id'])) $_args['owner_id'] = '';
	if(!isset($_args['property_id'])) $_args['property_id'] = '';
	if(!isset($_args['schedule_time'])) $_args['schedule_time'] = '';
	
	// init
	$email = array(
		'subject' => '',
		'message' => '',
	);
	
	// init
	$parts = iklvb_init_email_parts();
	
	// formato data e ora impostati in wordpress
	$date_format = get_option('date_format');
	$time_format = get_option('time_format');

	// preparo le parti del messaggio	
	$parts['visitor_name']		= iklvb_get_user_agent_name($_args['visitor_id']);
	$parts['visitor_image']		= iklvb_get_user_image($_args['visitor_id'],'','width:100px;margin:10px 5px 5px 5px;');
	$parts['owner_name']		= iklvb_get_user_agent_name($_args['owner_id']);
	$parts['owner_image']		= iklvb_get_user_image($_args['owner_id'],'','width:100px;margin:10px 5px 5px 5px;');
	$parts['property_title']	= get_the_title($_args['property_id']);
	$parts['property_address']	= iklvb_get_property_address($_args['property_id']);
	$parts['property_image']	= iklvb_get_property_image($_args['property_id'],'','width:100px;margin:10px 5px 5px 5px;');
	$parts['property_url']		= get_the_permalink($_args['property_id']);
	$parts['schedule_day']		= date_i18n($date_format, strtotime($_args['schedule_time']));
	$parts['schedule_hour']		= date_i18n($time_format, strtotime($_args['schedule_time']));
	$parts['schedule_box']		= iklvb_get_schedule_box($_args['schedule_time']);	
	
	// carico il custom_post_type
	$tpl = get_page_by_path($_tpl_slug.'-'.ICL_LANGUAGE_CODE,'object','email-message');
	
	// se ho trovato il tpl
	if(is_object($tpl))
	{
		$email['subject'] = iklvb_replace($tpl->post_title,$parts);
		$email['message'] = iklvb_replace(apply_filters('the_content',$tpl->post_content),$parts);
	}
	
	// return
	return $email;
}



/**
 * 2018-09-09 Lorenzo M.
 * 
 * replace dei placeholder nel email message
 * 
 * @param type $_string
 * @param type $_replace
 * @return type * 
 */
function iklvb_replace($_string = '',$_replace = array())
{
	// init
	$string = $_string;
	
	// replace dei placeholder
	foreach($_replace as $k => $v) $string = str_replace('['.$k.']',$v,$string);
	
	// return
	return $string;
}



/**
 * 2018-09-09 Lorenzo M.
 * 
 * array con tutti i placeholder previsti
 * in modo da sostituirli tutti e non lasciare parti NON sostuite nel messaggio email
 * (eventualmente nelle email dove non sono previsti alcuni placeholdere l'output previsto NON compare)
 * 
 */
function iklvb_init_email_parts()
{
	$parts = array(
		'visitor_name'		=> '',
		'visitor_image'		=> '',
		'owner_name'		=> '',
		'owner_image'		=> '',
		'property_title'	=> '',
		'property_address'	=> '',
		'property_image'	=> '',
		'property_url'		=> '',
		'schedule_day'		=> '',
		'schedule_hour'		=> '',
		'schedule_box'		=> '',		
	);	
	
	return $parts;
}





/**
 * 2018-09-10 Lorenzo M.
 * 
 * preparo html con nome e immagine profilo di chi riceve l'email
 * quello che compare in alto a destra
 * 
 * @param type $_user_id
 * @return string
 * 
 */
function iklvb_get_user_agent_email_to_box($_user_id = '')
{
	$html = '
		<div style="">
			<span style="margin:10px 10px 0 0; display: inline-block; vertical-align: top; font-family: Arial; font-size:13px; font-weight:700; color:#111111;">'.iklvb_get_user_agent_name($_user_id).'</span> 
			'.iklvb_get_user_image($_user_id,'','width:35px;border-radius:45px;').'</p>
		</div>
	';	
	
	return $html;	
}






/**
 * 2018-09-10 Lorenzo M.
 * 
 * ottengo le'email del proprietario dell'immobile a partire dal valore di property_id
 * 
 * @param type $_property_id
 * @return type
 */
function iklvb_get_email_owner_from_property_id($_property_id)
{
	// destinatario del messaggio >>>
	// email del proprietario dell'annuncio
	$receiver_email = '';

	$agent_id = intval(get_post_meta($_property_id, 'property_agent', true) );

	if($agent_id != 0)
	{
		$agent_agency_dev_id    = intval(get_post_meta($agent_id,'user_meda_id',true));
		$receiver_email         = get_the_author_meta('user_email',$agent_agency_dev_id);

		if($receiver_email == '')
		{
			$receiver_email		= esc_html(get_post_meta($agent_id,'agent_email',true));
		}
	}
	else
	{
		$the_post       = get_post($_property_id); 
		$author_id      = $the_post->post_author;
		$receiver_email = get_the_author_meta('user_email',$author_id); 
	}
	// <<<

	// controllo
	// non dovrebbbe mai uscire
	if(empty($receiver_email)) exit('Receiver email not found...');	
	
	return $receiver_email;
}


/**
 * 2018-09-10 Lorenzo M.
 * 
 * ottengo l'email del visitatore dal suo agent_id o user_id
 * 
 * @param type $_user_agent_id
 * @return type
 */
function iklvb_get_email_visitor_from_user_agent_id($_user_agent_id)
{
	// destinatario del messaggio >>>
	// email del visitatore
	$receiver_email = '';

	$agent_id = get_user_meta($_user_agent_id,'user_agent_id',true);

	if($agent_id != 0)
	{
		$agent_agency_dev_id    = intval(get_post_meta($agent_id,'user_meda_id',true));
		$receiver_email         = get_the_author_meta('user_email',$agent_agency_dev_id);

		if($receiver_email == '')
		{
			$receiver_email		= esc_html(get_post_meta($agent_id,'agent_email',true));
		}
	}
	else
	{
		$receiver_email = get_the_author_meta('user_email',$_user_agent_id); 
	}
	// <<<	
	
	// controllo
	// non dovrebbbe mai uscire
	if(empty($receiver_email)) exit('Receiver email not found...');		
	
	return $receiver_email;
}