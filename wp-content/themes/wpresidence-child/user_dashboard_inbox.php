<?php
// Template Name: User Dashboard Inbox
// Wp Estate Pack
// 
// 2018-08-21
// Custom template by Lorenzo M. 
// template usato per la gestione dei tickets
// NB: non posso cambiare il nome del template perchè la mappatura è gestito nel tema padre... (vedi /libs/dashboard_widget.php)
// 

if(!function_exists('wpestate_residence_functionality'))
{
    esc_html_e('This page will not work without WpResidence Core Plugin, Please activate it from the plugins menu!','wpresidence');
    exit();
}

if ( !is_user_logged_in() ) 
{   
     wp_redirect(  home_url() );
	 exit;
} 

// pagina richiesta
$my_page = 'tickets';
if( basename( $_SERVER['REQUEST_URI'] ) == 'my-visitors' ) $my_page = 'visitors';
if( basename( $_SERVER['REQUEST_URI'] ) == 'my-earnings' ) $my_page = 'earnings';
// carico il js per la gestione di questa pagina
if($my_page == 'tickets')
{
	wp_enqueue_script("js-dashboard-my-tickets", get_template_directory_uri() ."-child/js/dashboard-my-tickets.js", array("jquery"), false, true);
}

if($my_page == 'visitors' || $my_page == 'earnings')
{
	wp_enqueue_script("js-dashboard-my-visitors", get_template_directory_uri() ."-child/js/dashboard-my-visitors.js", array("jquery"), false, true);
}
/* da separare se necessario
if($my_page == 'earnings')
{
    wp_enqueue_script("js-dashboard-my-earnings", get_template_directory_uri() ."-child/js/dashboard-my-earnings.js", array("jquery"), false, true);
}*/

$current_user       = wp_get_current_user();
$user_login         =   $current_user->user_login;
$dash_profile_link  = wpestate_get_template_link('user_dashboard_profile.php');


get_header();


$options		=   wpestate_page_details($post->ID);
$user_role		=   get_user_meta( $current_user->ID, 'user_estate_role', true) ;
$userID			=   $current_user->ID;

$first_name		=   get_the_author_meta( 'first_name' , $userID );
$last_name		=   get_the_author_meta( 'last_name' , $userID );

if(empty($first_name) && empty($last_name)) $first_name = $user_login;

?>

<!--  -->
<?php

$current_user               =   wp_get_current_user();
// $user_custom_picture        =   get_the_author_meta( 'small_custom_picture' , $current_user->ID  );
$user_small_picture_id      =   get_the_author_meta( 'small_custom_picture' , $current_user->ID  );

if( $user_small_picture_id == '' )
{
    $user_small_picture[0] = get_template_directory_uri().'/img/default-user_1.png';
}
else
{
    $user_small_picture = wp_get_attachment_image_src($user_small_picture_id,'agent_picture_thumb');    
}


// 
$no_results = '
	<div>
		<p>'.__('There are no results to display.','iklvb-wpresidence').'</p>
	</div>
';










?>
<!--  -->




<div class="row row_user_dashboard">

    <div class="col-md-3 user_menu_wrapper">
       <div class="dashboard_menu_user_image">
            <div class="menu_user_picture" style="background-image: url('<?php print $user_small_picture[0];  ?>');height: 120px;width: 120px;" ></div>
            <div class="dashboard_username">
                <?php /* esc_html_e('Welcome back, ','wpresidence');*/ print trim($first_name.' '.$last_name); ?>
            </div> 
        </div>
          <?php  get_template_part('templates/user_menu');  ?>
    </div>  
    
    <div class="col-md-9 dashboard-margin">
		
        <?php // get_template_part('templates/breadcrumbs'); ?>
        <?php // get_template_part('templates/user_memebership_profile');  ?>
        <?php get_template_part('templates/ajax_container'); ?>
		
		
        <?php while (have_posts()) : the_post(); ?>
            <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no' ) { ?>
                <h3 class="entry-title"><?php the_title(); ?></h3>
            <?php }?>
         
            <div class="single-content"><?php the_content();?></div><!-- single content-->

        <?php endwhile; // end of the loop. ?>
        
			
		<?php 
			
		
		?>
			
		<div class="row <?php echo 'my-'.$my_page.'-wrapper'; ?>">
			<div class="col-md-12 row_dasboard-prop-listing">

			<?php  
			
			
				// wpw_pvp_update_custom_post_meta(); // rimuovere
			
				
			
				/**
				 * elenco tickets
				 */
				if($my_page == 'tickets')
				{
					// prex(date('Y-m-d H:i:00'));

					// cron_wpw_pcv_voucher_expire_notify();

					// cron_wpw_pcv_voucher_status_update();
					// exit;
					
					// update_post_meta(27424,'_wpw_pcv_schedule_time','2018-11-12 16:00:00');
					// update_post_meta(27424,'_wpw_pcv_status','performed');
					
					// elenco biglietti scaricabili a partire dagli ordini eseguiti dall'utente loggato
					$download_url_list = woo_vou_my_pdf_vouchers_download_link_array();

					// elenco biglietti acquistati
					$args = array(
						'post_type'         => 'woovouchercodes',
						'post_status'       => 'publish',
						'paged'             => $paged,
						'posts_per_page'    => -1,
						'meta_key'			=> '_wpw_pcv_time_update',
						'orderby'			=> 'meta_value',
						'order'             => 'DESC',
						'meta_query' => array(
							array(
								'key'       => '_wpw_pcv_customer_user_id',
								'value'     => $userID,
								'compare'   => '='
							),
						),
					);

					$tickets_results = new WP_Query($args);

					if($tickets_results->post_count > 0)
					{
						while($tickets_results->have_posts())
						{
							$tickets_results->the_post(); 
							get_template_part('templates/user_profile_ticket_listing_unit'); 
						}

						wp_reset_query();
					}
					else
					{
						echo $no_results;
					}
				}
				
				
				
				/**
				 * elenco visitatori
				 */
				if($my_page == 'visitors')
				{	
					// elenco biglietti collegati alle proprietà dell'utente loggato
					$args = array(
						'post_type'         => 'woovouchercodes',
						'post_status'       => 'publish',
						'paged'             => $paged,
						'posts_per_page'    => -1,
						'meta_key'			=> '_wpw_pcv_schedule_time',
						'orderby'			=> 'meta_value',
						'order'             => 'DESC',
						'meta_query' => array(
							array(
								'key'       => '_wpw_pcv_assign_prop_user_id',
								'value'     => $userID,
								'compare'   => '='
							),
						),
					);

					$tickets_results = new WP_Query($args);

					if($tickets_results->post_count > 0)
					{
						while($tickets_results->have_posts())
						{
							$tickets_results->the_post(); 
							get_template_part('templates/user_profile_visitor_listing_unit'); 
						}

						wp_reset_query();
					}
					else
					{
						echo $no_results;
					}
					
					
				}
				if($my_page == 'earnings')
				{				    
				    // elenco biglietti collegati alle proprietà dell'utente loggato
				    $args = array(
				    'post_type'         => 'woovouchercodes',
				    'post_status'       => 'publish',
				    'paged'             => $paged,
				    'posts_per_page'    => -1,
				    'meta_key'			=> '_wpw_pcv_schedule_time',
				    'orderby'			=> 'meta_value',
				    'order'             => 'DESC',
				    'meta_query' => array(
				    array(
				    'key'       => '_wpw_pcv_assign_prop_user_id',
				    'value'     => $userID,
				    'compare'   => '='
                    ),
                        array(
                            'key'     => '_wpw_pcv_status',
                            'value'   => array('performed','checked','payed'),
                            'compare' => 'in'
                        )
                    ),
                    );

                    $wpw_pcv_public = new Wpw_Pcv_Public();
                    $responser=$wpw_pcv_public->wpw_pcv_tickets_get_total_by_current_user();
                    $currency                   =   esc_html( get_option('wp_estate_currency_symbol', '') );

				    ?>
.


					<h3 style="text-align:right;"><?php print __('Total money recived: ','iklvb-wpresidence');?><?php print $responser.' '.$currency;  ?></h3>
				    <div class="col-xs-2 col-sm-2 titleTable"><h4><?php print __('Visitor','iklvb-wpresidence');?> | <?php print __('Property','iklvb-wpresidence');?></h4></div>
				    
				    <div class="col-xs-1 col-sm-1 titleTable"><h4><?php print __('Date','iklvb-wpresidence');?></h4></div>
				    <div class="col-xs-3 col-sm-3 titleTable"><h4><?php print __('Status','iklvb-wpresidence');?></h4></div>
				    <div class="col-xs-2 col-sm-2 titleTable"><h4><?php print __('Value','iklvb-wpresidence');?></h4></div>
				    <div class="col-xs-4 col-sm-4 titleTable"><h4><?php print __('Operations','iklvb-wpresidence');?></h4></div>
				    <?php 
				    $tickets_results = new WP_Query($args);
				    
				    if($tickets_results->post_count > 0)
				    {
				        while($tickets_results->have_posts())
				        {
				            $tickets_results->the_post();
				            get_template_part('templates/user_profile_earnings_listing_unit');
				        }
				        
				        wp_reset_query();
				    }
				    else
				    {
				        echo $no_results;
				    }
				    
				}

				?>
		
				</div>
			</div>
		</div>	
    </div>
  
  
</div>   
<?php get_footer(); ?>