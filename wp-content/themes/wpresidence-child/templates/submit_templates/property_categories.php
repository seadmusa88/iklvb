<?php
global $prop_action_category;
global $prop_action_category_selected;
global $prop_category_selected;
global $submission_page_fields;

global $event_schedule_day;
global $event_schedule_hour;
global $event_ticket_type;
global $event_guest_limit;

global $event_guest_count;

global $property_price; 
global $property_label; 
global $property_label_before; 


$is_property_event = iklvb_is_property_event($prop_action_category_selected);
$is_property_for_rent = iklvb_is_property_for_rent($prop_action_category_selected);


wp_enqueue_script("js-property-categories", get_template_directory_uri() ."-child/js/property_categories.js", array("jquery"), LAST_TIME_UPDATE, true);

?>



<?php if(   is_array($submission_page_fields) && 
            (   in_array('prop_action_category', $submission_page_fields) || 
                in_array('prop_category', $submission_page_fields)
            )
        ) { ?>   

    <div class="col-md-12 add-estate profile-page profile-onprofile row" id="property_categories_wrapper"> 
        <div class="submit_container"> 
            <div class="col-md-4 profile_label">
                <div class="user_details_row"><?php esc_html_e('Select Categories','wpresidence');?></div> 
                <div class="user_profile_explain"><?php esc_html_e('Selecting a category will make it easier for users to find you property in search results.','wpresidence')?></div>
            </div> 

            <?php if(is_array($submission_page_fields) && in_array('prop_action_category', $submission_page_fields)) { ?>    
                <p class="col-md-4"><label for="prop_action_category"> <?php esc_html_e('Listed In ','wpresidence'); $prop_action_category;?></label>
                    <?php 
	                    $args = array(
                            'class'       => 'select-submit2',
                            'hide_empty'  => false,
                            'selected'    => $prop_action_category_selected,
                            'name'        => 'prop_action_category',
                            'id'          => 'prop_action_category_submit',
                            'orderby'     => 'NAME',
                            'order'       => 'DESC',
                            // 'show_option_none'   => esc_html__('None','wpresidence'), // 2019-02-02 Lorenzo M.
                            'show_option_none'   => '',
                            'taxonomy'    => 'property_action_category',
                            'hierarchical'=> true							
                        );
					
						/*
						// se si tratta di evento e ci sono già degli ospiti
						// limito i valori della select SOLO alla action "event" (o "evento")
						if($event_guest_count > 0 && $is_property_event)
						{
							$term_1 = get_term_by('slug', 'event',  'property_action_category');
							$term_2 = get_term_by('slug', 'evento', 'property_action_category');

							$args['include'] = array($term_1->term_id, $term_2->term_id);
						}
						*/

						// 2019-02-02 Lorenzo M.
						if($event_guest_count > 0)
						{
							$args['include'] = array($prop_action_category_selected);
						}
						
						
			            wp_dropdown_categories( $args );  
					?>
                </p>   
            <?php }?>
				
			<?php if(is_array($submission_page_fields) && in_array('prop_category', $submission_page_fields)) { ?>
                <p class="col-md-4"><label for="prop_category"><?php esc_html_e('Category','wpresidence');?></label>
                    <?php 
                        $args=array(
                                'class'       => 'select-submit2',
                                'hide_empty'  => false,
                                'selected'    => $prop_category_selected,
                                'name'        => 'prop_category',
                                'id'          => 'prop_category_submit',
                                'orderby'     => 'NAME',
                                'order'       => 'ASC',
                                'show_option_none'   => esc_html__('None','wpresidence'),
                                'taxonomy'    => 'property_category',
                                'hierarchical'=> true
                            );
                        wp_dropdown_categories( $args ); ?>
                </p>
            <?php }?>
				
		</div>	
				
		<?php 	
		

		
		
		
		// formato data e ora impostati in wordpress
		$date_format = get_option('date_format');
		$time_format = get_option('time_format');
		$date_format_short = $date_format;

		// formato data per jQuery datepicker
		switch($date_format)
		{
			case 'F j, Y'	: 	$date_format_datepiker = 'MM d, yy';	$date_format_short = 'M j, Y'; break;
			case 'm/d/Y'	: 	$date_format_datepiker = 'mm/dd/yy';	break;
			case 'd/m/Y'	: 	$date_format_datepiker = 'dd/mm/yy';	break;		
			case 'Y-m-d'	: 	
			default			:	$date_format_datepiker = 'yy-mm-dd';	break;
		}
		
		
		$min_date = date('Y\, m\, d');	// data minima del calendario
		$max_date = '';					// data massima del calendario
		
		
		// preparo i valori per la select delle ore
		$schedule_hour_option = '<option value="">--</option>';
		for ($i = 9; $i <= 19; $i++)
		{
			for ($j = 0; $j <= 45; $j+=30) // Lorenzo M. // cambiare 30 in 15 per intervalli inferiori
			{
				$show_j = $j;
				if($j == 0) $show_j = '00';
				$val = str_pad($i, 2, '0', STR_PAD_LEFT).':'.$show_j;
				
				if($event_guest_count > 0)
				{	
					// evento con almeno 1 ospite: non posso cambiare l'ora dell'evento
					if($event_schedule_hour == $val)
					{
						$schedule_hour_option = '<option value="'.$val.'" selected>'.date_i18n($time_format,strtotime($val)).'</option>';
					}
				}
				else
				{
					$schedule_hour_option .= '<option value="'.$val.'" '.($event_schedule_hour == $val ? 'selected' : '').'>'.date_i18n($time_format,strtotime($val)).'</option>';
				}
			}
		}
		
		
		// lista tipi di biglietto
		$field_key = get_acf_field_key('acf_property-custom-fields', 'event_ticket_type');
		$field = get_field_object($field_key);
		$field_choices = $field['choices'];
		
		$ticket_type_option = '<option value="">--</option>';
		foreach($field_choices as $key => $value)
		{
			if($event_guest_count > 0)
			{
				// evento con almeno 1 ospite: non posso cambiare il tipo di biglietto
				if($event_ticket_type == $key)
				{
					$ticket_type_option = '<option value="'.$key.'" selected>'.$value.'</option>';
				}
			}
			else
			{
				$ticket_type_option .= '<option value="'.$key.'" '.($event_ticket_type == $key ? 'selected' : '').'>'.$value.'</option>';
			}
		}
		
		
		
		
		// 2018-08-31 Lorenzo M.
		
		if($event_guest_count > 0)
		{
			echo '
				<div class="submit_container" style="">
					<p class="col-md-4">	</p>
					<p class="col-md-8" style="padding-right: 15px; padding-left: 15px;">
						<label style="color: orange;">'.__('There are confirmed visitors for this event, you cannot change type of ad.','iklvb-wpresidence').'</label>
					</p>
				</div>
			';
		}
		
		echo '
			<div class="submit_container event_fields" style="'.($is_property_event === false ? 'display:none;' : '').'">
				<p class="col-md-4">	</p>

				<p class="col-md-4">	
					<label>'.__('Schedule Day','iklvb-wpresidence').'</label>
					<input name="event_schedule_day" id="event_schedule_day" type="input" readonly
						placeholder="'. __('Day','wpestate').'" 
						class="form-control datepicker readonly-to-default schedule_day_custom'.($event_guest_count > 0 ? '_disabled' : '').'"
						min="'.$min_date.'"
						max="'.$max_date.'"
						date-format="'.$date_format_datepiker.'"
						value="'.(strlen($event_schedule_day) ? date_i18n($date_format, strtotime($event_schedule_day)) : '').'"
						aria-required="true">
				</p>
				<p class="col-md-4">	
					<label>'.__('Schedule Time','iklvb-wpresidence').'</label>
					<select name="event_schedule_hour" id="event_schedule_hour" class="form-control">
						'.$schedule_hour_option.'
					</select>
				</p>
			</div>	
			<div class="submit_container event_fields" style="'.($is_property_event === false ? 'display:none;' : '').'">
				<p class="col-md-4">	</p>

				<p class="col-md-4">	
					<label>'.__('Ticket type','iklvb-wpresidence').'</label>
					<select name="event_ticket_type" id="event_ticket_type" class="form-control">
						'.$ticket_type_option.'
					</select>
				</p>
				<p class="col-md-4">	
					<label>'.__('Guest limit (max)','iklvb-wpresidence').'</label>
					<input type="text" name="event_guest_limit" id="event_guest_limit" class="form-control" value="'.$event_guest_limit.'">
				</p>
			</div>
			
			'.($event_guest_count > 0 ? '
			<div class="submit_container event_fields" style="'.($is_property_event === false ? 'display:none;' : '').'">
				<p class="col-md-4">	</p>
				<p class="col-md-8" style="padding-right: 15px; padding-left: 15px;">
					<label style="color: orange;">'.__('There are confirmed visitors for this event, you can not change date, time and type of ticket.','iklvb-wpresidence').'</label>
				</p>
			</div>
			' : '').'
		';
					
		?>
				
    </div>

<?php }?>

<?php 
	if(   is_array($submission_page_fields) && 
           (    in_array('property_label', $submission_page_fields) || 
                in_array('property_price', $submission_page_fields) || 
                in_array('property_label_before', $submission_page_fields) 
            )
        ) { 
		
		$property_label_option = '<option value=""></option>';
		$property_label_option_values = array(
			__('Day', 'iklvb-wpresidence'),
			__('Week', 'iklvb-wpresidence'),
			__('Month', 'iklvb-wpresidence'),
			__('Year', 'iklvb-wpresidence'),
		);
		
		foreach($property_label_option_values as $value)
		{
			$property_label_option .= '<option value="'.$value.'" '.($property_label == $value ? 'selected' : '').'>'.$value.'</option>';
		}
		
?>    
    <div class="col-md-12 add-estate profile-page profile-onprofile row"> 

        <div class="submit_container">

            <div class="col-md-4 profile_label">
                <!--<div class="submit_container_header"><?php esc_html_e('Property Description & Price','wpresidence');?></div>-->
                <div class="user_details_row"><?php esc_html_e('Property Price','wpresidence');?></div> 
                <div class="user_profile_explain"><?php esc_html_e('Adding a price will make it easier for users to find your property in search results.','wpresidence')?></div>
            </div>


            <div class="col-md-8">
                <?php if(   is_array($submission_page_fields) && in_array('property_price', $submission_page_fields)) { ?>
                    <p class="col-md-12 half_form">
                        <label for="property_price">*<?php esc_html_e('Price in ','wpresidence');print esc_html( get_option('wp_estate_currency_symbol', '') ).' '; esc_html_e('(only numbers)','wpresidence').' '; esc_html_e('(mandatory)','wpresidence'); ?>  </label>
                        <input type="text" id="property_price" class="form-control" size="40" name="property_price" value="<?php print $property_price;?>">
                    </p>
                <?php }?>

                <?php if(   is_array($submission_page_fields) && in_array('property_label', $submission_page_fields)) { ?>    
                    <p class="col-md-6 half_form property_label_wrapper" style="<?php echo ($is_property_for_rent === false ? 'display:none;' : ''); ?>">
                        <label for="property_label"><?php esc_html_e('After Price Label (ex: "/month")','wpresidence');?></label>
                        <!-- input type="text" id="property_label" class="form-control" size="40" name="property_label" value="<?php print $property_label;?>" -->
						<select name="property_label" id="property_label" class="form-control">
							<?php echo $property_label_option; ?>
						</select>
                    </p> 
                <?php }?>

                <?php if(   is_array($submission_page_fields) && in_array('property_label_before', $submission_page_fields)) { ?>
                    <p class="col-md-6 half_form">
                        <label for="property_label_before"><?php esc_html_e('Before Price Label (ex: "from ")','wpresidence');?></label>
                        <input type="text" id="property_label_before" class="form-control" size="40" name="property_label_before" value="<?php print $property_label_before;?>">
                    </p>
                <?php }?>
            </div>

        </div>
    </div>
<?php } ?>