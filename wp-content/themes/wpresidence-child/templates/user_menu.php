<?php 
$current_user           =   wp_get_current_user();
$userID                 =   $current_user->ID;
$user_login             =   $current_user->user_login;  
$user_agent_id          =   intval( get_user_meta($userID,'user_agent_id',true));
$add_link               =   wpestate_get_template_link('user_dashboard_add.php');
$add_agent              =   wpestate_get_template_link('user_dashboard_add_agent.php');
$dash_profile           =   wpestate_get_template_link('user_dashboard_profile.php');
$dash_favorite          =   wpestate_get_template_link('user_dashboard_favorite.php');
$dash_link              =   wpestate_get_template_link('user_dashboard.php');
$agent_list_link        =   wpestate_get_template_link('user_dashboard_agent_list.php');
$dash_searches          =   wpestate_get_template_link('user_dashboard_searches.php');
$dash_showing           =   wpestate_get_template_link('user_dashboard_showing.php');
$subscription           =   wpestate_get_template_link('user_dashboard_subscription.php');
$activeprofile          =   '';
$activedash             =   '';
$activeadd              =   '';
$activefav              =   '';
$activesearch           =   '';
$activeinvoices         =   '';
$activesubscr			=	'';
$user_pack              =   get_the_author_meta( 'package_id' , $userID );    
$clientId               =   esc_html( get_option('wp_estate_paypal_client_id','') );
$clientSecret           =   esc_html( get_option('wp_estate_paypal_client_secret','') );  
$user_registered        =   get_the_author_meta( 'user_registered' , $userID );
$user_package_activation=   get_the_author_meta( 'package_activation' , $userID );
$home_url               =   home_url();
$dash_invoices          =   wpestate_get_template_link('user_dashboard_invoices.php');
// $dash_inbox             =   wpestate_get_template_link('user_dashboard_inbox.php');
// $dash_tickets           =   wpestate_get_template_link('user_dashboard_inbox.php');
$dash_tickets           =   (ICL_LANGUAGE_CODE != 'en' ? '/'.ICL_LANGUAGE_CODE : '').'/my-visits/';
$dash_visitors          =   (ICL_LANGUAGE_CODE != 'en' ? '/'.ICL_LANGUAGE_CODE : '').'/my-visitors/';
$dash_earnings          =   (ICL_LANGUAGE_CODE != 'en' ? '/'.ICL_LANGUAGE_CODE : '').'/my-earnings/';
$activeinbox            =   '';
$activeshowing          =   '';
$activeaddagent         =   '';
$activeagentlist        =   '';
$active_tickets			= '';
$active_visitors		= '';
$active_earnings		= '';
$activeorders = '';
$activedownloads = '';

if ( basename( get_page_template() ) == 'user_dashboard.php' ){
    $activedash  =   'user_tab_active';    
}else if ( basename( get_page_template() ) == 'user_dashboard_add.php' ){
    $activeadd   =   'user_tab_active';
}else if ( basename( get_page_template() ) == 'user_dashboard_subscription.php' ){
    $activesubscr   =   'user_tab_active';
}else if ( basename( get_page_template() ) == 'user_dashboard_profile.php' ){
    $activeprofile   =   'user_tab_active';
}else if ( basename( get_page_template() ) == 'user_dashboard_favorite.php' ){
    $activefav   =   'user_tab_active';
}else if( basename( get_page_template() ) == 'user_dashboard_searches.php' ){
    $activesearch  =   'user_tab_active';
}else if( basename( get_page_template() ) == 'user_dashboard_invoices.php' ){
    $activeinvoices  =   'user_tab_active';
}else if( basename( get_page_template() ) == 'user_dashboard_add_agent.php' ){
    $activeaddagent =   'user_tab_active';
}else if( basename( get_page_template() ) == 'user_dashboard_agent_list.php' ){
    $activeagentlist =   'user_tab_active';
}else if( basename( $_SERVER['REQUEST_URI'] ) == 'my-earnings' ){
    $active_earnings =   'user_tab_active';
}else if( basename( $_SERVER['REQUEST_URI'] ) == 'my-visits' ){
    $active_tickets =   'user_tab_active';
}else if( basename( $_SERVER['REQUEST_URI'] ) == 'my-visitors' ){
    $active_visitors =   'user_tab_active';
}else if( basename( get_page_template() ) == 'user_dashboard_showing.php' ){
    $activeshowing =   'user_tab_active';
}else if( basename( $_SERVER['REQUEST_URI'] ) == 'orders' || strpos($_SERVER['REQUEST_URI'], '/view-order/') ){
    $activeorders =   'user_tab_active'; 
}else if( basename( $_SERVER['REQUEST_URI'] ) == 'downloads' ){
    $activedownloads =   'user_tab_active';
}


$no_unread=  intval(get_user_meta($userID,'unread_mess',true));
    
$user_role = get_user_meta( $current_user->ID, 'user_estate_role', true) ;
?>



<?php
if ( $user_agent_id!=0 && get_post_status($user_agent_id)=='pending'  ){
    echo '<div class="user_dashboard_app">'.esc_html__('Your account is pending approval. Please wait for admin to approve it. ','wpresidence').'</div>';
}
if ( $user_agent_id!=0 && get_post_status($user_agent_id)=='disabled' ){
    echo '<div class="user_dashboard_app">'.esc_html__('Your account is disabled.','wpresidence').'</div>';
}
?>

<div class="user_tab_menu">

    <div class="user_dashboard_links">
        <?php if( $dash_profile!=$home_url && $dash_profile!=''){ ?>
            <a href="<?php print esc_url($dash_profile);?>"  class="<?php print $activeprofile; ?>"><i class="fa fa-lg fa-fw fa-cog"></i> <?php esc_html_e('My Profile','wpresidence');?></a>
        <?php } ?>
        <?php 
        if( $dash_link!=$home_url && $dash_link!=''){
            if($user_agent_id==0 || ( $user_agent_id!=0 && get_post_status($user_agent_id)!='pending' && get_post_status($user_agent_id)!='disabled') ){?>
            <a href="<?php print esc_url($dash_link);?>" class="<?php print $activedash; ?>"> <i class="fa fa-lg fa-fw fa-map-marker"></i> <?php esc_html_e('My Business','iklvb-wpresidence');?></a>
        <?php } 
        }
			
        if( $subscription != $home_url && $subscription != '')
		{
            if($user_agent_id==0 || ( $user_agent_id!=0 && get_post_status($user_agent_id)!='pending' && get_post_status($user_agent_id)!='disabled') ){?>
            <a href="<?php print esc_url ($subscription);?>" class="<?php print $activesubscr; ?>"> <i class="fa fa-lg fa-fw fa-money"></i><?php esc_html_e('My Subscription','iklvb-wpresidence');?></a>  
			<?php 
			}
        } 
		
        if( $add_link!=$home_url && $add_link!='')
		{
            if($user_agent_id==0 || ( $user_agent_id!=0 && get_post_status($user_agent_id)!='pending' && get_post_status($user_agent_id)!='disabled') ){?>
            <a href="<?php print esc_url ($add_link);?>" class="<?php print $activeadd; ?>"> <i class="fa fa-lg fa-fw fa-plus"></i><?php esc_html_e('Add New Property','wpresidence');?></a>  
			<?php 
			}
        } 
      
        if($user_role==3 || $user_role ==4){
            if( $user_agent_id!=0 && get_post_status($user_agent_id)!='pending'){
            ?>
            <a href="<?php print esc_url ($add_agent);?>"            class="<?php print $activeaddagent; ?>"> <i class="fa fa-lg fa-fw fa-user-plus"></i><?php esc_html_e('Add New Agent','wpresidence');?></a>  
            <a href="<?php print esc_url ($agent_list_link);?>"      class="<?php print $activeagentlist; ?>"> <i class="fa fa-lg fa-fw fa-user"></i><?php esc_html_e('Agent list','wpresidence');?></a>  
            
        <?php
            }
        }
        ?>
        <?php if( $dash_favorite!=$home_url && $dash_favorite!=''){ ?>
            <a href="<?php print esc_url($dash_favorite);?>" class="<?php print $activefav; ?>"><i class="fa fa-lg fa-fw fa-heart"></i> <?php esc_html_e('My Favorites','iklvb-wpresidence');?></a>
        <?php } ?>
        <?php if( $dash_searches!=$home_url && $dash_searches!=''){ ?>
            <a href="<?php print esc_url($dash_searches);?>" class="<?php print $activesearch; ?>"><i class="fa fa-lg fa-fw fa-search"></i> <?php esc_html_e('My Saved','iklvb-wpresidence');?></a>
        <?php } 
		/*
        if( $dash_invoices!=$home_url && $dash_invoices!=''){ ?>
            <a href="<?php print esc_url($dash_invoices);?>" class="<?php print $activeinvoices; ?>"><i class="fa fa-lg fa-fw fa-file-text-o"></i> <?php esc_html_e('My Invoices','wpresidence');?></a>
        <?php } 
		*/
        ?>
			
        <a href="<?php echo wc_get_page_permalink( 'myaccount' ).'orders/';?>" class="<?php print $activeorders; ?>"><i class="fa fa-lg fa-fw fa-ticket"></i> <?php esc_html_e('My Tickets','iklvb-wpresidence');?></a>
		
        <?php
        /*
		<a href="<?php echo wc_get_page_permalink( 'myaccount' ).'downloads/';?>" class="<?php print $activedownloads; ?>"><i class="fa fa-lg fa-fw fa-file-text-o"></i> <?php esc_html_e('My Tickets','iklvb-wpresidence');?></a>
		*/
		
        if($dash_tickets != $home_url && $dash_tickets != ''){ 
		?>
            <a href="<?php print esc_url($dash_tickets);?>" class="<?php print $active_tickets; ?>"><i class="fa fa-lg fa-fw fa-user"></i> 
                <?php esc_html_e('My Visits','iklvb-wpresidence'); 
                    if  ($no_unread>0){
                        echo '<div class="unread_mess">'.$no_unread.'</div>';
                    }
                ?>
            </a>
        <?php }
		
        if($dash_visitors != $home_url && $dash_visitors != ''){ 
		?>
            <a href="<?php print esc_url($dash_visitors);?>" class="<?php print $active_visitors; ?>"><i class="fa fa-lg fa-fw fa-users"></i> 
                <?php esc_html_e('My Visitors','iklvb-wpresidence'); 
                    if  ($no_unread>0){
                        echo '<div class="unread_mess">'.$no_unread.'</div>';
                    }
                ?>
            </a>
        <?php }
        
        if($dash_earnings != $home_url && $dash_earnings != ''){
            ?>
            <a href="<?php print esc_url($dash_earnings);?>" class="<?php print $active_earnings; ?>"><i class="fa fa-lg fa-fw fa-users"></i> 
                <?php esc_html_e('My earnings','iklvb-wpresidence'); 
                    if  ($no_unread>0){
                        echo '<div class="unread_mess">'.$no_unread.'</div>';
                    }
                ?>
            </a>
        <?php }
		/*
        if($dash_inbox!=$home_url && $dash_inbox!=''){ ?>
            <a href="<?php print esc_url($dash_inbox);?>" class="<?php print $activeinbox; ?>"><i class="fa fa-lg fa-fw fa-envelope-o"></i> 
                <?php esc_html_e('Inbox','wpresidence'); 
                    if  ($no_unread>0){
                        echo '<div class="unread_mess">'.$no_unread.'</div>';
                    }
                ?>
            </a>
        <?php }
		*/
		
		/** 
        if( $dash_showing!=$home_url && $dash_showing!=''){ ?>
            <a href="<?php print esc_url($dash_showing);?>" class="<?php print $activeshowing; ?>"><i class="fa fa-lg fa-fw fa-file-text-o"></i> <?php esc_html_e('Calendar','wpresidence');?></a>
        <?php } 
		 */
		
		?>
            
             
            
        <a href="<?php echo wp_logout_url( home_url() );?>" title="Logout"><i class="fa fa-lg fa-fw fa-power-off"></i> <?php esc_html_e('Log Out','wpresidence');?></a>
    </div>
    
</div>

