<?php

global $post;
global $userID;
global $current_user;
global $download_url_list;

// pre($post);
// pre(get_post_meta($post->ID));
// pre($post->ID);

// recupero i campi del voucher
$vou_assign_prop		=   get_post_meta($post->ID, '_wpw_pcv_assign_prop', true);
$vou_customer_user_id	=   get_post_meta($post->ID, '_wpw_pcv_customer_user_id', true);
$vou_action_type		=   get_post_meta($post->ID, '_wpw_pcv_action_type', true);
$vou_price_range_min	=   get_post_meta($post->ID, '_wpw_pcv_price_range_min', true);
$vou_price_range_max	=   get_post_meta($post->ID, '_wpw_pcv_price_range_max', true);
$vou_event_ticket_type	=   get_post_meta($post->ID, '_wpw_pcv_event_ticket_type', true);
$vou_status				=   get_post_meta($post->ID, '_wpw_pcv_status', true);
$vou_schedule_time		=   get_post_meta($post->ID, '_wpw_pcv_schedule_time', true);
$vou_time_update		=   get_post_meta($post->ID, '_wpw_pcv_time_update', true);
$vou_next_move			=   get_post_meta($post->ID, '_wpw_pcv_next_move', true);
$vou_purchased_codes	=   get_post_meta($post->ID, '_woo_vou_purchased_codes', true);
$vou_expire_date		=   get_post_meta($post->ID, '_woo_vou_exp_date', true);
$currency                   =   esc_html( get_option('wp_estate_currency_symbol', '') );
// init
$vou_next_move_class	= '';

// controllo
if(empty($vou_next_move)) $vou_next_move = 'owner_first';


// valuta impostata in woocommerce
$currency = get_woocommerce_currency_symbol();

// formato data e ora impostati in wordpress
$date_format = get_option('date_format');
$time_format = get_option('time_format');

// preparo i campi input
$schedule_day = make_schedule_day(WPW_PCV_TIME_NEXT_MOVE_OWNER * 2, $vou_expire_date);
$schedule_hour = make_schedule_hour();

if($vou_action_type == 'event' || $vou_action_type == 'evento')
{
	$vou_action_type_more_info = $vou_event_ticket_type;
}
else
{
	$vou_action_type_more_info = $vou_price_range_min.' '.$currency.' / '.$vou_price_range_max.' '.$currency;
}
			
			
// oggi
$now = date('Y-m-d H:i:00');

$vou_schedule_month		= '';
$vou_schedule_day		= '';
$vou_schedule_hour		= '';

$ticket_action_message	= '';

if(!empty($vou_schedule_time))
{
	$vou_schedule_month		= date_i18n('M', strtotime($vou_schedule_time));
	$vou_schedule_day		= date_i18n('d', strtotime($vou_schedule_time));
	$vou_schedule_hour		= date_i18n($time_format, strtotime($vou_schedule_time));
}

// recupero i campi dal prodotto collegato al voucher
$product_title	= get_the_title($post->post_parent);

// id del profilo collegato a questo utente (chi ha acquistato il biglietto)
// (es. se si tratta di agente/agenzia/costruttore)
$vou_customer_user_agent_id = get_user_meta($vou_customer_user_id,'user_agent_id',true);

// nome del visitatore (chi ha acquistato il biglietto)
if(is_numeric($vou_customer_user_agent_id))
{
	$vou_customer_user_agent_name = get_the_title($vou_customer_user_agent_id);
}
else
{
	$first_name = get_the_author_meta( 'first_name' , $vou_customer_user_id );
	$last_name = get_the_author_meta( 'last_name' , $vou_customer_user_id );
	$vou_customer_user_agent_name = trim($first_name.' '.$last_name);
}

//
$order_id = $post->post_title;








// immagine del profilo dell'agente >>>
$user_thumb_img = WPESTATE_PLUGIN_DIR_URL.'img/default_user.png';
	
if(is_numeric($vou_customer_user_agent_id))
{
	$user_thumb_id = get_post_thumbnail_id($vou_customer_user_agent_id);
	$user_thumb = wp_get_attachment_image_src($user_thumb_id,'thumbnail');
	if(!empty($user_thumb[0])) $user_thumb_img = $user_thumb[0];	
	$user_thumb_img_html = '<a href="'.get_the_permalink($vou_customer_user_agent_id).'" style="background-image:url('.$user_thumb_img.')" class="agent-image-cover"></a>';	
}
elseif(is_numeric($vou_customer_user_id))
{
	$user_thumb_id = get_the_author_meta('small_custom_picture',$vou_customer_user_id);
	$user_thumb = wp_get_attachment_image_src($user_thumb_id,'thumbnail');
	if(!empty($user_thumb[0])) $user_thumb_img = $user_thumb[0];	
	$user_thumb_img_html = '<span style="background-image:url('.$user_thumb_img.')" class="agent-image-cover img48"></span>';
}
else
{
	$user_thumb_img_html = '<span style="background-image:url('.$user_thumb_img.')" class="agent-image-cover img48"></span>';
}
// <<<



// property main image >>>
if(strlen($vou_assign_prop))
{
	$prop_thumb_id = get_post_thumbnail_id($vou_assign_prop);
	$prop_thumb = wp_get_attachment_image_src($prop_thumb_id,'thumbnail');
	$prop_thumb_img = $prop_thumb[0];
	if(empty($prop_thumb_img)) $prop_thumb_img = WPESTATE_PLUGIN_DIR_URL.'img/default_property_featured.jpg';
	$prop_thumb_img_html = '<a href="'.get_the_permalink($vou_assign_prop).'" style="background-image:url('.$prop_thumb_img.')" class="property-image-cover img48"></a>';
}
else
{
	$prop_thumb_img = WPESTATE_PLUGIN_DIR_URL.'img/default_property_featured.jpg';
	$prop_thumb_img_html = '<span style="background-image:url('.$prop_thumb_img.')" class="property-image-cover img48"></span>';
}
// <<<



// bottoni >>>
$ticket_confirm_button	= '
	<a href="" class="wpresidence_button" data-id="'.$post->ID.'" data-action="confirm" title="'.__('confirm date','iklvb-wpresidence').'">
		<i class="fa fa-check-circle fa-3x text-success"></i>
	</a>
';
$ticket_confirm_button_disabled = '
	<a class="wpresidence_button"><i class="fa fa-check-circle fa-3x text-disabled"></i></a>
';

$ticket_cancel_button	= '
	<a href="" class="wpresidence_button" data-id="'.$post->ID.'" data-action="cancel-toggle" title="'.__('cancel request','iklvb-wpresidence').'">
		<i class="fa fa-times-circle fa-3x text-danger"></i>
	</a>
';
$ticket_cancel_button_disabled = '
	<a class="wpresidence_button"><i class="fa fa-times-circle fa-3x text-disabled"></i></a>
';

$ticket_change_button	= '
	<a href="" class="wpresidence_button" data-id="'.$post->ID.'" data-action="change-toggle" title="'.__('change date','iklvb-wpresidence').'">
		<i class="fa fa-refresh fa-3x text-warning"></i>
	</a>
';
$ticket_change_button_disabled = '
	<a class="wpresidence_button"><i class="fa fa-refresh fa-3x text-disabled"></i></a>
';

$cash_url = '/cash-in-iklvb-tickets/?ticket_number='.$vou_purchased_codes;
$ticket_cash_button	= '
	<a href="'.$cash_url.'" target="_blank" class="wpresidence_button" data-id="'.$post->ID.'" data-action="cash" title="'.__('cash this ticket','iklvb-wpresidence').'">
		<i class="fa fa-ticket fa-3x text-color-performed"></i>
	</a>
';
// <<<




// se si tratta di un evento >>>
if($vou_action_type == 'event' || $vou_action_type == 'evento') 
{
	$ticket_change_button = $ticket_change_button_disabled;
}
// <<<




// messaggio per status "confirmed" >>>
if($vou_status == 'confirmed')
{
	// pulsante conferma (non serve in questo caso)
	$ticket_confirm_button = $ticket_confirm_button_disabled;
	
	// disabilito azioni modifica a annulla
	// quando mancano 24 ore dalla data confermata
	if($now > date('Y-m-d H:i:00', strtotime($vou_schedule_time.' -'.WPW_PCV_TIME_BEFORE_SCHEDULE.' seconds')))
	{
		$ticket_change_button = $ticket_change_button_disabled;
		$ticket_cancel_button = $ticket_cancel_button_disabled;

		$ticket_action_message = '
			<div class="col-xs-12">
				<p>
					'.__('This meeting will take place in the next 24 hours.','iklvb-wpresidence').'<br>
					'.__('You can not change or cancel the confirmed date.','iklvb-wpresidence').'
				</p>
			</div>
		';
	}	
}
// <<<




// messaggio per status "assigned" >>>
if($vou_status == 'assigned')
{	
	switch($vou_next_move)
	{
		case 'owner_first': 
			$ticket_action_message = '
				<div class="col-xs-12">
					<p>'.__('Waiting for <strong>your</strong> action','iklvb-wpresidence').'<br> 
						('.__('within the','iklvb-wpresidence').' 
						'.date_i18n($date_format, strtotime($vou_time_update.' +'.WPW_PCV_TIME_NEXT_MOVE_OWNER_FIRST.' seconds')).', 
						'.date_i18n($time_format, strtotime($vou_time_update.' +'.WPW_PCV_TIME_NEXT_MOVE_OWNER_FIRST.' seconds')).')
					</p>
				</div>
			';
			$vou_next_move_class = 'next-move-active';
			break;
		
		case 'visitor':
			$ticket_action_message = '
				<div class="col-xs-12">
					<p>'.__('Waiting for a response from','iklvb-wpresidence').' <strong>'.$vou_customer_user_agent_name.'</strong><br>
						('.__('within the','iklvb-wpresidence').' 
						'.date_i18n($date_format, strtotime($vou_time_update.' +'.WPW_PCV_TIME_NEXT_MOVE_VISITOR.' seconds')).', 
						'.date_i18n($time_format, strtotime($vou_time_update.' +'.WPW_PCV_TIME_NEXT_MOVE_VISITOR.' seconds')).')
					</p>
				</div>
			';
			$ticket_confirm_button = $ticket_confirm_button_disabled;
			$ticket_change_button = $ticket_change_button_disabled;
			break;
		
		case 'owner':
			$ticket_action_message = '
				<div class="col-xs-12">
					<p>'.__('Waiting for <strong>your</strong> action','iklvb-wpresidence').'<br> 
						('.__('within the','iklvb-wpresidence').' 
						'.date_i18n($date_format, strtotime($vou_time_update.' +'.WPW_PCV_TIME_NEXT_MOVE_OWNER.' seconds')).', 
						'.date_i18n($time_format, strtotime($vou_time_update.' +'.WPW_PCV_TIME_NEXT_MOVE_OWNER.' seconds')).')
					</p>
				</div>
			';
			$vou_next_move_class = 'next-move-active';
			break;
	}
}
// <<<



// messaggio per status "expired" >>>
if($vou_status == 'expired')
{
	// prex(get_post_meta($post->ID));
	
}
// <<<





if($vou_status == 'assigned' || $vou_status == 'confirmed')
{
	$ticket_action_message .= '
		<div class="col-xs-12">
					'.$ticket_confirm_button.'
					'.$ticket_change_button.'
					'.$ticket_cancel_button.'
		</div>';/*
			<div class="row row-change-data-fields">
				<div class="col-xs-6 col-sm-3">
					'.$schedule_day.'
				</div>
				<div class="col-xs-6 col-sm-3">
					'.$schedule_hour.'
				</div>
				<div class="col-xs-12 col-sm-3">
					<a href="" class="wpresidence_button" data-id="'.$post->ID.'" data-action="change-toggle" title="'.__('close','iklvb-wpresidence').'">
						<i class="fa fa-times-circle fa-3x text-danger"></i>
					</a>
					<a href="" class="wpresidence_button" data-id="'.$post->ID.'" data-action="change" title="'.__('send new date','iklvb-wpresidence').'">
						<i class="fa fa-check-circle fa-3x text-success"></i>
					</a>
				</div>
			</div>
			<div class="row row-confirm-cancel">
				<div class="col-xs-12 col-xs-8">
					<p>'.__('Are you sure you want to cancel this request? <br>(The ticket will be available again)','iklvb-wpresidence').'</p>
				</div>
				<div class="col-xs-12 col-sm-4">
					<a href="" class="wpresidence_button" data-id="'.$post->ID.'" data-action="cancel-toggle" title="'.__('No','iklvb-wpresidence').'">
						<i class="fa fa-times-circle fa-3x text-danger"></i>
					</a>
					<a href="" class="wpresidence_button" data-id="'.$post->ID.'" data-action="cancel" title="'.__('Yes','iklvb-wpresidence').'">
						<i class="fa fa-check-circle fa-3x text-success"></i>
					</a>
				</div>
			</div>
			<input type="hidden" name="ajax_ticket_nonce_'.$post->ID.'" class="ajax_ticket_nonce" value="'. wp_create_nonce( 'ajax-ticket-'.$post->ID ).'">
		</div>
	';*/
}


//$vou_status = 'payed';
if($vou_status == 'checked')
{
    $ticket_action_message .= '        
				<div class="col-xs-12">
					'.__('waiting for payment','iklvb-wpresidence').'
				</div>	    
	';
        
}
if($vou_status == 'payed')
{
    $ticket_action_message .= '
				<div class="col-xs-12">
					'.__('payed','iklvb-wpresidence').'
				</div>
	';
}
if($vou_status == 'performed') {
    $ticket_action_message .= '
        
				<div class="col-xs-12">
					'.$ticket_cash_button.'
				</div>
					    
	';
    
}


//include_once( WPW_PCV_INC_DIR.'/class-wpw-pcv-public.php' );
$wpw_pcv_public = new Wpw_Pcv_Public();
$response=$wpw_pcv_public->wpw_pcv_tickets_get_by_tickte_code($vou_purchased_codes);
$currency                   =   esc_html( get_option('wp_estate_currency_symbol', '') );

// messaggio per status "expired" >>>
if($vou_status == 'performed' || $vou_status == 'checked' || $vou_status == 'payed')
{
	

    echo '
	<div class="row tickets-listing status-'.$vou_status.' '.$vou_next_move_class.'" data-status="'.$vou_status.'" id="row-'.$post->ID.'">
		<div class="col-xs-2 col-sm-2 tickets-img">
			<span class="img_50">'.$user_thumb_img_html.'</span>       
			<span class="img_50">'.$prop_thumb_img_html.'</span>
		</div>
        <div class="col-xs-1 col-sm-1 tickets-date">
			<div class="tickets-date-wrapper">
				<span class="">'.$vou_schedule_day.' '.strtoupper($vou_schedule_month).'</span>                
				<span class="hour">'.$vou_schedule_hour.'</span>
			</div>
		</div>
		<div class="col-xs-3 col-sm-3 tickets-info">		
			<div class="col-xs-12 col-sm-12">			
				'.get_ticket_status_html($vou_status).'	
			</div>												
			<div class="col-xs-12 col-sm-12">
				<div class="alert-ok message_ok_json"></div>
				<div class="alert-error message_error_default">'.__('<strong>ERROR</strong>: Some error occur while submitting request... try again...','iklvb-wpresidence').'</div>
				<div class="alert-error message_error_json"></div>
			</div>			
		</div>
		<div class="col-xs-2 col-sm-2 tickets-price">
            <span class="price">'.$response['price'].' '.$currency.'.</span>
        </div>
        <div class="col-xs-3 col-sm-3 tickets-action">
            <span class="action">'.$ticket_action_message.'</span>
        </div>
        <div class="col-xs-1 col-sm-1">
				<a class="tickets-toggle-wrapper" data-id="'.$post->ID.'"><i class="fa fa-chevron-down"></i></a>
	    </div>	
		<div class="col-xs-12 tickets-details">
			<div class="row">				
				<div class="col-xs-12 col-sm-4">	
                    <h3>'.__('Tour detais','iklvb-wpresidence').'</h3>		
                    <div class="col-xs-12 col-sm-12">			
    					<span class="customer-name">'.__('Visitor','iklvb-wpresidence').': <strong>'.$vou_customer_user_agent_name.'</strong></span>
    					<span class="property-title">'.__('Property','iklvb-wpresidence').': <strong>'.get_the_title($vou_assign_prop).'</strong></span>
                    </div>
				</div>
				<div class="col-xs-12 col-sm-8">
					<h3>'.__('Ticket detais','iklvb-wpresidence').'</h3>		
						<div class="col-xs-12 col-sm-6">							
							<span class="ticket-code"><strong>'.__('Number','iklvb-wpresidence').':</strong>
							'.($vou_status != 'assigned' && $vou_status != 'confirmed' ? '
								'.$vou_purchased_codes.'<br>
							' : '
								<span class="text-info">'.__('available after the meeting date','iklvb-wpresidence').'</span>
							').'
							</span>
						</div>

						<div class="col-xs-12 col-sm-12">
							<span class="ticket-amount"><strong>'.__('Amount','iklvb-wpresidence').':</strong>'.$response['price_cash_in'].' '.$currency.'</span>
						</div>
                        <div class="col-xs-12 col-sm-12">
							<span class="ticket-code"><strong>'.__('Cash in','iklvb-wpresidence').':</strong> From 1 jun etc etc *</span>
						</div>				
				</div>
			
			</div>
		</div>
		
		<div class="image-loader"><img src="'.get_template_directory_uri().'-child/img/loading_icon.gif"></div>
			
	</div>
';


}
// <<<




