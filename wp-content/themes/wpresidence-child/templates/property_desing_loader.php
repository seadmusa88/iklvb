<?php

global  $wp_estate_global_page_template; 
global  $wp_estate_local_page_template;
global  $options;

// init object
$ticket_obj = new Wpw_Pcv_Public();
$prop_action_category		= get_the_terms($post->ID,'property_action_category');
if(isset($prop_action_category[0]))
{
    switch($prop_action_category[0]->slug)
    {
        case 'for-sale':	$prop_action_type = 'for_sale';
            break;

        case 'in-vendita':	$prop_action_type = 'in_vendita';
            break;

        case 'for-rent':	$prop_action_type = 'for_rent';
            break;

        case 'in-affitto':	$prop_action_type = 'in_affitto';
            break;


        case 'evento':
        case 'event':		$prop_action_type = 'event';
            break;
    }
}

//var_dump($prop_action_category);

$response_ticket=$ticket_obj->wpw_pcv_tickets_get_by_current_user_customer("Available",$prop_action_type);
//var_dump($response_ticket);
$array_ticket_number=array();
$array_ticket_price=array();
$price          =   floatval   ( get_post_meta($post->ID, 'property_price', true) );


for ($i = 0; $i < count($response_ticket); $i++)
{

    preg_match_all('!\d+\.?\d+!', $response_ticket[$i]["product_title"] ,$match);
    $prezzo_trovato= str_replace('.', '', $match[0][0]);
    $prezzo_trovato = (float) $prezzo_trovato;
    if($prezzo_trovato>=$price) {
        $array_ticket_number[]= $response_ticket[$i]["ticket_number"];
        $array_ticket_price[] = $response_ticket[$i]["product_title"];
    }
}



?>





<div class="row estate_property_first_row" data-prp-listingid="<?php echo $post->ID;?>" >
    <?php get_template_part('templates/breadcrumbs'); ?>
    <div class="col-xs-12 <?php print esc_html($options['content_class']);?> full_width_prop">
        
        <?php get_template_part('templates/ajax_container'); ?>
        
        <?php while (have_posts()) : the_post();
            $post_title = get_post_meta($post->ID, 'post_show_title', true);
            if ($post_title != 'no') { ?>
                <h1 class="entry-title"><?php the_title(); ?></h1>
            <?php } ?>
         
            <div class="single-content page_template_loader">
            <?php 
            
            $page_to_load='';
            
            if ($wp_estate_local_page_template!=0){
                $page_to=$wp_estate_local_page_template;
            }else{
               $page_to= $wp_estate_global_page_template;
            }
            
            $the_query = new WP_Query( 'page_id='.$page_to );
                while ( $the_query->have_posts() ) :
                        $the_query->the_post();
                      the_content();
                endwhile;
                wp_reset_postdata();

            ?></div><!-- single content-->

                   
     
        
        <?php endwhile; // end of the loop. ?>

		<div class="single-content page_template_loader">
			<div class="vc_row wpb_row vc_row-fluid vc_row">
				<div class="wpb_column vc_column_container vc_col-sm-8 vc_column">	
					<div class="vc_column-inner ">	
						<div class="wpb_wrapper">	
							
							<!-- #reviews start -->
							<?php 
							if(get_option('wp_estate_show_reviews_prop',true)=='yes'){
								get_template_part ('/templates/property_reviews');
							}	
							?>
							<!-- #reviews end -->
							
							<!-- #comments start -->
							<?php comments_template('', true);?> 	
							<!-- end comments -->  
						</div>
					</div>
				</div>
			</div>
		</div>
		
    </div>
    
<?php     
//get_template_part ('/templates/image_gallery'); 
include(locate_template('sidebar.php')); ?>
</div>

    <script>
        function autocomplete(inp, arr,arr_price) {
            /*the autocomplete function takes two arguments,
            the text field element and an array of possible autocompleted values:*/
            var currentFocus;
            /*execute a function when someone writes in the text field:*/
            inp.addEventListener("mouseover", function(e) {
                var a, b, i, val = this.value;
                /*close any already open lists of autocompleted values*/
                closeAllLists();
                currentFocus = -1;
                /*create a DIV element that will contain the items (values):*/
                a = document.createElement("DIV");
                a.setAttribute("id", this.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                /*append the DIV element as a child of the autocomplete container:*/
                this.parentNode.appendChild(a);
                /*for each item in the array...*/
                for (i = 0; i < arr.length; i++) {
                    /*check if the item starts with the same letters as the text field value:*/

                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    /*make the matching letters bold:*/
                    b.innerHTML = "<strong>" + arr_price[i].substr(0, val.length) + "</strong>";
                    b.innerHTML += arr_price[i].substr(val.length);
                    /*insert a input field that will hold the current array item's value:*/
                    b.innerHTML += "<input type='hidden' value='" + arr_price[i] + "' name='"+arr[i]+"'>";
                    /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function(e) {
                        /*insert the value for the autocomplete text field:*/
                        inp.value = this.getElementsByTagName("input")[0].name;
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                    });
                    a.appendChild(b);

                }
            });
            /*execute a function presses a key on the keyboard:*/
            inp.addEventListener("keydown", function(e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    /*If the arrow DOWN key is pressed,
                    increase the currentFocus variable:*/
                    currentFocus++;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 38) { //up
                    /*If the arrow UP key is pressed,
                    decrease the currentFocus variable:*/
                    currentFocus--;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 13) {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                    e.preventDefault();
                    if (currentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[currentFocus].click();
                    }
                }
            });
            function addActive(x) {
                /*a function to classify an item as "active":*/
                if (!x) return false;
                /*start by removing the "active" class on all items:*/
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                /*add class "autocomplete-active":*/
                x[currentFocus].classList.add("autocomplete-active");
            }
            function removeActive(x) {
                /*a function to remove the "active" class from all autocomplete items:*/
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
            }
            function closeAllLists(elmnt) {
                /*close all autocomplete lists in the document,
                except the one passed as an argument:*/
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                }
            }
            /*execute a function when someone clicks in the document:*/
            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });
        }


        /*An array containing all the country names in the world:*/

        var passedArrayPrice =
            <?php echo '["' . implode('", "', $array_ticket_price) . '"]' ?>;
        var passedArray =
            <?php echo '["' . implode('", "', $array_ticket_number) . '"]' ?>;


        /*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
        autocomplete(document.getElementById("ticket_number"), passedArray,passedArrayPrice);
    </script>

<?php 
$mapargs = array(
        'post_type'         =>  'estate_property',
        'post_status'       =>  'publish',
        'p'                 =>  $post->ID );
  
$selected_pins  =   wpestate_listing_pins($mapargs,1);
wp_localize_script('googlecode_property', 'googlecode_property_vars2', 
            array('markers2'          =>  $selected_pins));

get_footer(); 
exit();
?>