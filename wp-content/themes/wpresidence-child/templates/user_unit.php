<?php
global $options;
global $isdashabord;
global $align;
global $show_remove_fav;
global $is_shortcode;
global $row_number_col;
global $no_listins_per_row;
$col_class  =   'col-md-4';


//
$author_info = get_userdata($post->ID);


if($options['content_class']=='col-md-12' && $show_remove_fav!=1){
    $col_class  =   'col-md-3';
    $col_org    =   3;

}

if($no_listins_per_row==3){
    $col_class  =   'col-md-6';
    $col_org    =   6;
    if($options['content_class']=='col-md-12'){
        $col_class  =   'col-md-4';
        $col_org    =   4;
    }
    
}else{   
    $col_class  =   'col-md-4';
    $col_org    =   4;
    if($options['content_class']=='col-md-12'){
        $col_class  =   'col-md-3';
        $col_org    =   3;
    }
}

// if template is vertical
if($align=='col-md-12'){
     $col_class  =  'col-md-12';
     $col_org    =  12;
}

$preview        =   array();
$preview[0]     =   '';
$words          =   55;
$link           =   get_author_posts_url($post->ID);

/*
$title          =   get_the_title();

if (mb_strlen ($title)>90 ){
    $title          =   mb_substr($title,0,90).'...';
}
*/

if(isset($is_shortcode) && $is_shortcode==1 ){
    // $col_class='col-md-'.$row_number_col.' shortcode-col';
    // $col_class=' shortcode-col';
}

?>  

<div  class="<?php echo esc_html($col_class);?>  listing_wrapper blog2v"> 
    <div class="property_listing" data-link="<?php echo esc_url($link); ?>">
        <?php
		/*
        if (has_post_thumbnail()):
       
            $pinterest  =   wp_get_attachment_image_src(get_post_thumbnail_id(),'property_full_map');
            $preview    =   wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
            $compare    =   wp_get_attachment_image_src(get_post_thumbnail_id(), 'slider_thumb');
            $extra= array(
                'data-original'=>$preview[0],
                'class'	=> 'lazyload img-responsive',    
            );
         
            $thumb_prop = get_the_post_thumbnail( $post->ID, 'property_listings',$extra );    
            if($thumb_prop ==''){
                $thumb_prop_default =  get_template_directory_uri().'/img/defaults/default_property_listings.jpg';
                $thumb_prop         =  '<img src="'.$thumb_prop_default.'" class="b-lazy img-responsive wp-post-image  lazy-hidden" alt="no thumb" />';   
            }
            $featured   = intval  ( get_post_meta( $post->ID, 'prop_featured', true ) );
        
            
            if( $thumb_prop!='' ){
                print '<div class="blog_unit_image">';
                print  $thumb_prop;
                print '</div>'; 
            }
           
        endif;
		 * 
		 */
		
		$preview		 = wp_get_attachment_image_src(get_the_author_meta('small_custom_picture',$post->ID),array('400','400'));
		$preview_img     = $preview[0];
		if(empty($preview_img)) $preview_img = get_template_directory_uri().'-child/img/default_user_400x400.png';
		$thumb_prop      =  '<img src="'.$preview_img.'" class="b-lazy img-responsive wp-post-image  lazy-hidden" alt="no thumb" />'; 
		
		print '<div class="blog_unit_image">';
        print  $thumb_prop;
        print '</div>'; 
		
        ?>

  
           <h4>
               <a href="<?php echo $link; ?>">
                <?php 
                    
                    echo $author_info->first_name.' '.$author_info->last_name; 
                     
                ?>
               </a> 
           </h4>
        
           <div class="blog_unit_meta">
            <?php print get_the_date('M d, Y');?>
            
           </div>
           
            <div class="listing_details the_grid_view">
                <?php   
               
                if( has_post_thumbnail() ){
                   //echo wpestate_strip_words( get_the_excerpt(),18).' ...';
                   echo  wpestate_strip_excerpt_by_char(get_the_excerpt(),115,$post->ID);
                } else{
                    // echo wpestate_strip_words( get_the_excerpt(),40).' ...';
                    echo  wpestate_strip_excerpt_by_char(get_the_excerpt(),200,$post->ID);
                } ?>
            </div>
       
        
           
             <a class="read_more" href="<?php echo $link; ?>"> <?php esc_html_e('Continue Reading','wpresidence'); ?><i class="fa fa-angle-right"></i> </a>
           
     
        </div>          
    </div>      