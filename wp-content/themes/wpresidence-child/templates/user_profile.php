<?php

$current_user			= wp_get_current_user();

$userID                 =   $current_user->ID;
$user_login             =   $current_user->user_login;
$first_name             =   get_the_author_meta( 'first_name' , $userID );
$last_name              =   get_the_author_meta( 'last_name' , $userID );
$user_email             =   get_the_author_meta( 'user_email' , $userID );
$user_mobile            =   get_the_author_meta( 'mobile' , $userID );
$user_phone             =   get_the_author_meta( 'phone' , $userID );
$description            =   get_the_author_meta( 'description' , $userID );
$facebook               =   get_the_author_meta( 'facebook' , $userID );
$twitter                =   get_the_author_meta( 'twitter' , $userID );
$linkedin               =   get_the_author_meta( 'linkedin' , $userID );
$pinterest              =   get_the_author_meta( 'pinterest' , $userID );
$userinstagram          =   get_the_author_meta( 'instagram' , $userID );
$agent_custom_data      =   get_the_author_meta( 'agent_custom_data' , $userID );
$user_skype             =   get_the_author_meta( 'skype' , $userID );
$website                =   get_the_author_meta( 'website' , $userID );

$billing_first_name		=   get_the_author_meta( 'billing_first_name' , $userID );
$billing_last_name		=   get_the_author_meta( 'billing_last_name' , $userID );
$billing_address_1		=   get_the_author_meta( 'billing_address_1' , $userID );
$billing_city			=   get_the_author_meta( 'billing_city' , $userID );
$billing_postcode		=   get_the_author_meta( 'billing_postcode' , $userID );
$billing_country		=   get_the_author_meta( 'billing_country' , $userID );
$billing_state			=   get_the_author_meta( 'billing_state' , $userID );
$billing_fiscal_code	=   get_the_author_meta( 'billing_fiscal_code' , $userID );


$user_title             =   get_the_author_meta( 'title' , $userID );
$user_custom_picture    =   get_the_author_meta( 'custom_picture' , $userID );
$user_small_picture     =   get_the_author_meta( 'small_custom_picture' , $userID );
$image_id               =   get_the_author_meta( 'small_custom_picture',$userID); 
$about_me               =   get_the_author_meta( 'description' , $userID );

if($user_custom_picture=='')
{
    $user_custom_picture=get_template_directory_uri().'/img/default_user.png';
}



// compongo il campo stato usando la select già  pronta di woocommerce
$countries_obj   = new WC_Countries();
$allowed_countries   = $countries_obj->get_allowed_countries();
$default_country = $countries_obj->get_base_country();
$allowed_states = $countries_obj->get_states( $default_country );
	
$billing_country_setup = array
(
	'type' => 'select',
	// 'label' => __('my_account_billing_country','iklvb-wpresidence'),
	// 'placeholder' => __('my_account_billing_country','iklvb-wpresidence'),
	'required' => true,
	'options' => $allowed_countries,
	'value' => 'IT',
	'class' => array
	(
		0 => 'form-row-last',
		1 => 'address-field',
		2 => 'country-field',
		3 => 'form-control-select',
	),
);


// compongo il campo provincia usando la select già  pronta di woocommerce
// *** predisposizione ma non usato per ora... ***
$billing_state_setup = array
(
	'type' => 'select',
	// 'label' => __('my_account_billing_state','iklvb-wpresidence'),
	// 'placeholder' => __('my_account_billing_state','iklvb-wpresidence'),
	'required' => true,
	'options' => $allowed_states,
	'value' => '',
	'class' => array
	(
		0 => 'form-row-last',
		1 => 'address-field',
		2 => 'state-field',
		3 => 'form-control-select',
	),
);

// includo js per la gestione delle select di stato e provincia
$handle = 'wc-country-select';
wp_enqueue_script($handle, get_site_url().'/wp-content/plugins/woocommerce/assets/js/frontend/country-select.min.js', array('jquery'), true);



?>

<div class="col-md-12 user_profile_div"> 
    <div id="profile_message">
		<div class="alert-error" id="profile_empty_fields"><?php echo __('<strong>ERROR</strong>: There are empty fields to complete.','iklvb-wpresidence'); ?></div>
		<div class="alert-error" id="profile_invalid_user_email"><?php echo __('<strong>ERROR</strong>: Your email is not valid.','iklvb-wpresidence'); ?></div>
		<div class="alert-error" id="profile_invalid_vat"><?php echo __('<strong>ERROR</strong>: Billing Vat Number is not valid.','iklvb-wpresidence'); ?></div>
		<div class="alert-error" id="profile_invalid_fiscal_code"><?php echo __('<strong>ERROR</strong>: Billing Fiscal Code is not valid.','iklvb-wpresidence'); ?></div>
		<div class="alert-error" id="profile_update_error"><?php echo __('<strong>ERROR</strong>: Save failed, profile not updated...','iklvb-wpresidence'); ?></div>
		<div class="alert-ok" id="profile_update_ok"><?php echo __('Profile updated.','iklvb-wpresidence'); ?></div>		
		<div id="image-loader"><img src="<?php echo get_template_directory_uri(); ?>-child/img/loading_icon.gif"></div>		
    </div> 


<div class="add-estate profile-page profile-onprofile row row-first"> 
    <div class="col-md-4 profile_label">
        <div class="user_details_row"><?php _e('User Details','wpestate');?></div> 
        <div class="user_profile_explain"><?php _e('Add your contact information.','wpestate')?></div>

    </div>
          
    <div class="col-md-4">
        <p>
            <label for="firstname"><?php _e('First Name','wpestate');?> <span class="required">*</span></label>
            <input type="text" id="firstname" class="form-control" value="<?php echo esc_html($first_name);?>"  name="firstname">
        </p>

        <p>
            <label for="secondname"><?php _e('Last Name','wpestate');?> <span class="required">*</span></label>
            <input type="text" id="secondname" class="form-control" value="<?php echo esc_html($last_name);?>"  name="secondname">
        </p>
        <p>
            <label for="useremail"><?php _e('Email','wpestate');?> <span class="required">*</span></label>
            <input type="text" id="useremail"  class="form-control" value="<?php echo esc_html($user_email);?>"  name="useremail">
        </p>
    </div>  

    <div class="col-md-4">
        <p>
            <label for="userphone"><?php _e('Phone', 'wpestate'); ?> <span class="required">*</span></label>
            <input type="text" id="userphone" class="form-control" value="<?php echo esc_html($user_phone); ?>"  name="userphone">
        </p>
        <p>
            <label for="usermobile"><?php _e('Mobile', 'wpestate'); ?></label>
            <input type="text" id="usermobile" class="form-control" value="<?php echo esc_html($user_mobile); ?>"  name="usermobile">
        </p>

        <p>
            <label for="userskype"><?php _e('Skype', 'wpestate'); ?></label>
            <input type="text" id="userskype" class="form-control" value="<?php echo esc_html($user_skype); ?>"  name="userskype">
        </p>
        <?php   wp_nonce_field( 'profile_ajax_nonce', 'security-profile' );   ?>
       
    </div>
</div>
	
	
	
	
	
	
<div class="add-estate profile-page profile-onprofile row"> 
    <div class="col-md-4 profile_label">
        <div class="user_details_row"><?php _e('User Billing Info','wpestate');?></div> 
        <div class="user_profile_explain"><?php _e('Add your billing information.','wpestate')?></div>
    </div>
          
    <div class="col-md-4">
        <p>
            <label for="user_billing_first_name"><?php _e('Billing First Name','wpestate');?> <span class="required">*</span></label>
            <input type="text" id="user_billing_first_name" class="form-control" value="<?php echo esc_html($billing_first_name);?>"  name="user_billing_first_name">
        </p>	
        <p>
            <label for="user_billing_last_name"><?php _e('Billing Last Name','wpestate');?> <span class="required">*</span></label>
            <input type="text" id="user_billing_last_name" class="form-control" value="<?php echo esc_html($billing_last_name);?>"  name="user_billing_last_name">
        </p>		
        <p>
            <label for="user_billing_address_1"><?php _e('Billing Address','wpestate');?> <span class="required">*</span></label>
            <input type="text" id="user_billing_address_1" class="form-control" value="<?php echo esc_html($billing_address_1);?>"  name="user_billing_address_1">
        </p>
        <p>
            <label for="user_billing_fiscal_code"><?php _e('Billing Fiscal Code','woocommerce');?> <span class="required">*</span></label>
            <input type="text" id="user_billing_fiscal_code" class="form-control input-fiscal-code" value="<?php echo esc_html($billing_fiscal_code);?>"  name="user_billing_fiscal_code">
        </p>
	</div>
	
	<div class="col-md-4">
        <p>
            <label for="user_billing_country"><?php _e('Billing Country','woocommerce');?> <span class="required">*</span></label>
            <?php woocommerce_form_field('user_billing_country',$billing_country_setup,strlen($billing_country) ? $billing_country : $billing_country_setup['value'] ); ?>
        </p>
        <p>
            <label for="user_billing_state"><?php _e('Billing Province','woocommerce');?> <span class="required">*</span></label>
            <input type="text" id="user_billing_state" class="form-control" value="<?php echo esc_html($billing_state);?>"  name="user_billing_state">
            <?php // woocommerce_form_field('user_billing_state',$billing_state_setup,strlen($billing_state) ? $billing_state : $billing_state_setup['value'] ); ?>
        </p>
        <p>
            <label for="user_billing_postcode"><?php _e('Billing Postcode','woocommerce');?> <span class="required">*</span></label>
            <input type="text" id="user_billing_postcode"  class="form-control" value="<?php echo esc_html($billing_postcode);?>"  name="user_billing_postcode">
        </p>
        <p>
            <label for="user_billing_city"><?php _e('Billing City','wpestate');?> <span class="required">*</span></label>
            <input type="text" id="user_billing_city" class="form-control" value="<?php echo esc_html($billing_city);?>"  name="user_billing_city">
        </p>
    </div>  
</div>  
	






	
                             
<div class="add-estate profile-page profile-onprofile row">       
    <div class="col-md-4 profile_label">
        <div class="user_details_row"><?php _e('User Details','wpestate');?></div> 
        <div class="user_profile_explain"><?php _e('Add your social media information.','wpestate')?></div>

    </div>
    <div class="col-md-4">
        <p>
            <label for="userfacebook"><?php _e('Facebook Url', 'wpestate'); ?></label>
            <input type="text" id="userfacebook" class="form-control" value="<?php echo esc_html($facebook); ?>"  name="userfacebook">
        </p>

        <p>
            <label for="usertwitter"><?php _e('Twitter Url', 'wpestate'); ?></label>
            <input type="text" id="usertwitter" class="form-control" value="<?php echo esc_html($twitter); ?>"  name="usertwitter">
        </p>

        <p>
            <label for="userlinkedin"><?php _e('Linkedin Url', 'wpestate'); ?></label>
            <input type="text" id="userlinkedin" class="form-control"  value="<?php echo esc_html($linkedin); ?>"  name="userlinkedin">
        </p>
    </div>
    <div class="col-md-4">
        <p>
            <label for="userinstagram"><?php _e('Instagram Url','wpestate');?></label>
            <input type="text" id="userinstagram" class="form-control" value="<?php echo esc_html($userinstagram);?>"  name="userinstagram">
        </p> 

        <p>
            <label for="userpinterest"><?php _e('Pinterest Url','wpestate');?></label>
            <input type="text" id="userpinterest" class="form-control" value="<?php echo esc_html($pinterest);?>"  name="userpinterest">
        </p> 

        <p>
            <label for="website"><?php _e('Website Url (without http)','wpestate');?></label>
            <input type="text" id="website" class="form-control" value="<?php echo esc_html($website);?>"  name="website">
        </p>
    </div> 
</div>
	
	
	
<div class="add-estate profile-page profile-onprofile row"> 
    
    <div class="col-md-4 profile_label">
        <div class="user_details_row"><?php _e('Photo','wpestate');?></div> 
        <div class="user_profile_explain"><?php _e('Upload your profile photo.','wpestate')?></div>
    </div>

    <div class="profile_div col-md-4" id="profile-div">
        <?php print '<img id="profile-image" src="'.$user_custom_picture.'" alt="user image" data-profileurl="'.$user_custom_picture.'" data-smallprofileurl="'.$image_id.'" >';

        //print '/ '.$user_small_picture;?>

        <div id="upload-container">                 
            <div id="aaiu-upload-container">                 

                <button id="aaiu-uploader" class="wpresidence_button wpresidence_success"><?php _e('Upload  profile image.','wpestate');?></button>
                <div id="aaiu-upload-imagelist">
                    <ul id="aaiu-ul-list" class="aaiu-upload-list"></ul>
                </div>
            </div>  
        </div>
        <span class="upload_explain"><?php _e('*minimum 500px x 500px','wpestate');?></span>                    
    </div>
</div>

	

<div class="add-estate profile-page profile-onprofile row profile-page-submit">
    <div class="col-md-4 profile_label">
        <div class="user_details_row"><?php _e('User Details','wpestate');?></div> 
        <div class="user_profile_explain"><?php _e('Add some information about yourself.','wpestate')?></div>
    </div>
    <div class="col-md-8">
         <p>
            <label for="usertitle"><?php _e('Title/Position','wpestate');?></label>
            <input type="text" id="usertitle" class="form-control" value="<?php echo esc_html($user_title);?>"  name="usertitle">
        </p>
        <p class="fullp-button">
            <button class="wpresidence_button profile_button" id="update_profile_custom"><?php _e('Update profile', 'wpestate'); ?></button>
            <button class="wpresidence_button" id="delete_profile"><?php _e('Delete profile', 'wpestate'); ?></button>
        </p>
    </div>
</div>
      
<?php   get_template_part('templates/change_pass_template'); ?>
</div>