<?php

global $post;
global $userID;
global $current_user;
global $download_url_list;

// pre($post);
//prex(get_post_meta($post->ID));

// recupero i campi del voucher
$vou_assign_prop			=   get_post_meta($post->ID, '_wpw_pcv_assign_prop', true);
$vou_customer_user_id		=   get_post_meta($post->ID, '_wpw_pcv_customer_user_id', true);
$vou_action_type			=   get_post_meta($post->ID, '_wpw_pcv_action_type', true);
$vou_price_range_min		=   get_post_meta($post->ID, '_wpw_pcv_price_range_min', true);
$vou_price_range_max		=   get_post_meta($post->ID, '_wpw_pcv_price_range_max', true);
$vou_event_ticket_type		=   get_post_meta($post->ID, '_wpw_pcv_event_ticket_type', true);
$vou_status					=   get_post_meta($post->ID, '_wpw_pcv_status', true);
$vou_schedule_time			=   get_post_meta($post->ID, '_wpw_pcv_schedule_time', true);
$vou_time_update			=   get_post_meta($post->ID, '_wpw_pcv_time_update', true);
$vou_next_move				=   get_post_meta($post->ID, '_wpw_pcv_next_move', true);
$vou_purchased_codes		=   get_post_meta($post->ID, '_woo_vou_purchased_codes', true);
$vou_expire_date			=   get_post_meta($post->ID, '_woo_vou_exp_date', true);

// init
$vou_next_move_class		= '';

// controllo
if(empty($vou_next_move)) $vou_next_move = 'owner_first';


// valuta impostata in woocommerce
$currency = get_woocommerce_currency_symbol();

// formato data e ora impostati in wordpress
$date_format = get_option('date_format');
$time_format = get_option('time_format');

// preparo i campi input
$schedule_day = make_schedule_day(WPW_PCV_TIME_NEXT_MOVE_VISITOR * 2, $vou_expire_date);
$schedule_hour = make_schedule_hour();
					

			
			
// oggi
$now = date('Y-m-d H:i:00');

$vou_schedule_month		= '';
$vou_schedule_day		= '';
$vou_schedule_hour		= '';

$ticket_action_message	= '';

if(!empty($vou_schedule_time))
{
	$vou_schedule_month		= date_i18n('M', strtotime($vou_schedule_time));
	$vou_schedule_day		= date_i18n('d', strtotime($vou_schedule_time));
	$vou_schedule_hour		= date_i18n($time_format, strtotime($vou_schedule_time));
}

// recupero i campi dal prodotto collegato al voucher
$product_title = get_the_title($post->post_parent);

// id del profilo collegato a questo utente
// (es. se si tratta di agente/agenzia/costruttore)
$vou_assign_prop_user_agent_id = get_user_meta($vou_customer_user_id,'user_agent_id',true);

// nome del proprietario dell'immobile
if(is_numeric($vou_assign_prop_user_agent_id))
{
	$vou_assign_prop_agent_name = get_the_title($vou_assign_prop_user_agent_id);
}
else
{
	$first_name = get_the_author_meta( 'first_name' , $vou_customer_user_id );
	$last_name = get_the_author_meta( 'last_name' , $vou_customer_user_id );
	$vou_assign_prop_agent_name = trim($first_name.' '.$last_name);
}

// 
$order_id = $post->post_title;








// immagine del profilo dell'agente / utente >>>
$user_thumb_img = WPESTATE_PLUGIN_DIR_URL.'img/default_user.png';

if(is_numeric($vou_assign_prop_user_agent_id))
{
	$user_thumb_id = get_post_thumbnail_id($vou_assign_prop_user_agent_id);
	$user_thumb = wp_get_attachment_image_src($user_thumb_id,'thumbnail');
	if(!empty($user_thumb[0])) $user_thumb_img = $user_thumb[0];	
	$user_thumb_img_html = '<a href="'.get_the_permalink($vou_assign_prop_user_agent_id).'" style="background-image:url('.$user_thumb_img.')" class="agent-image-cover"></a>';
}
elseif(is_numeric($vou_customer_user_id))
{
	$user_thumb_id = get_the_author_meta('small_custom_picture',$vou_customer_user_id);
	$user_thumb = wp_get_attachment_image_src($user_thumb_id,'thumbnail');
	if(!empty($user_thumb[0])) $user_thumb_img = $user_thumb[0];	
	$user_thumb_img_html = '<span style="background-image:url('.$user_thumb_img.')" class="agent-image-cover"></span>';
}
else
{
	$user_thumb_img_html = '<span style="background-image:url('.$user_thumb_img.')" class="agent-image-cover"></span>';
}
// <<<



// property main image >>>
if(strlen($vou_assign_prop))
{
	$prop_thumb_id = get_post_thumbnail_id($vou_assign_prop);
	$prop_thumb = wp_get_attachment_image_src($prop_thumb_id,'thumbnail');
	$prop_thumb_img = $prop_thumb[0];
	if(empty($prop_thumb_img)) $prop_thumb_img = WPESTATE_PLUGIN_DIR_URL.'img/default_property_featured.jpg';
	//$prop_thumb_img_html = '<a href="'.get_the_permalink($vou_assign_prop).'" style="background-image:url('.$prop_thumb_img.')" class="property-image-cover"></a>';
	$prop_thumb_img_html = '<span style="background-image:url('.$prop_thumb_img.')" class="property-image-cover"></span>';
}
else
{
	$prop_thumb_img = WPESTATE_PLUGIN_DIR_URL.'img/default_property_featured.jpg';
	$prop_thumb_img_html = '<span style="background-image:url('.$prop_thumb_img.')" class="property-image-cover"></span>';
}
// <<<


/*
// bottoni >>>
$download_url = (isset($download_url_list[$vou_purchased_codes]) ? $download_url_list[$vou_purchased_codes]['download_url'] : '');
$ticket_download_button = '
	<a href="'.$download_url.'" class="wpresidence_button" data-action="download" target="_blank" title="DOWNLOAD PDF">
		<i class="fa fa-cloud-download fa-3x text-color-confirmed"></i>
	</a>
';

$ticket_confirm_button	= '
	<a href="" class="wpresidence_button" data-id="'.$post->ID.'" data-action="confirm" title="'.__('confirm date','iklvb-wpresidence').'">
		<i class="fa fa-check-circle fa-3x text-success"></i>
	</a>
';
$ticket_confirm_button_disabled = '
	<a class="wpresidence_button"><i class="fa fa-check-circle fa-3x text-disabled"></i></a>
';

$ticket_cancel_button	= '
	<a href="" class="wpresidence_button" data-id="'.$post->ID.'" data-action="cancel-toggle" title="'.__('cancel request','iklvb-wpresidence').'">
		<i class="fa fa-times-circle fa-3x text-danger"></i>
	</a>
';
$ticket_cancel_button_disabled = '
	<a class="wpresidence_button"><i class="fa fa-times-circle fa-3x text-disabled"></i></a>
';

$ticket_change_button	= '
	<a href="" class="wpresidence_button" data-id="'.$post->ID.'" data-action="change-toggle" title="'.__('change date','iklvb-wpresidence').'">
		<i class="fa fa-refresh fa-3x text-color-assigned"></i>
	</a>
';
$ticket_change_button_disabled = '
	<a class="wpresidence_button"><i class="fa fa-refresh fa-3x text-disabled"></i></a>
';
// <<<
*/




/*
// messaggio per status "confirmed" >>>
if($vou_status == 'confirmed')
{
	// pulsante conferma (non serve in questo caso)
	$ticket_confirm_button = '';
	
	// disabilito azioni modifica a annulla
	// quando mancano 24 ore dalla data confermata
	if($now > date('Y-m-d H:i:00', strtotime($vou_schedule_time.' -'.WPW_PCV_TIME_BEFORE_SCHEDULE.' seconds')))
	{
		$ticket_change_button = $ticket_change_button_disabled;
		$ticket_cancel_button = $ticket_cancel_button_disabled;
		
		$ticket_action_message = '
			<div class="col-xs-12">
				<p>
					'.__('This meeting will take place in the next 24 hours.','iklvb-wpresidence').'<br>
					'.__('You can not change or cancel the confirmed date.','iklvb-wpresidence').'
				</p>
			</div>
		';
	}
}
else
{
	// visualizzo il pulsante download solo quando lo status è confirmed
	$ticket_download_button	= '';
}
// <<<
*/


/*
// messaggio per status "assigned" >>>
if($vou_status == 'assigned')
{
	switch($vou_next_move)
	{
		case 'owner_first': 
			$ticket_action_message = '
				<div class="col-xs-12">
					<p>'.__('Waiting for a response from','iklvb-wpresidence').' <strong>'.$vou_assign_prop_agent_name.'</strong><br>
						('.__('within the','iklvb-wpresidence').' 
						'.date_i18n($date_format, strtotime($vou_time_update.' +'.WPW_PCV_TIME_NEXT_MOVE_OWNER_FIRST.' seconds')).', 
						'.date_i18n($time_format, strtotime($vou_time_update.' +'.WPW_PCV_TIME_NEXT_MOVE_OWNER_FIRST.' seconds')).')
					</p>
				</div>
			';
			$ticket_confirm_button = $ticket_confirm_button_disabled;
			$ticket_change_button = $ticket_change_button_disabled;
			break;
		
		case 'visitor':
			$ticket_action_message = '
				<div class="col-xs-12">
					<p>'.__('Waiting for <strong>your</strong> action','iklvb-wpresidence').'<br> 
						('.__('within the','iklvb-wpresidence').' 
						'.date_i18n($date_format, strtotime($vou_time_update.' +'.WPW_PCV_TIME_NEXT_MOVE_VISITOR.' seconds')).', 
						'.date_i18n($time_format, strtotime($vou_time_update.' +'.WPW_PCV_TIME_NEXT_MOVE_VISITOR.' seconds')).')
					</p>
				</div>
			';
			$vou_next_move_class = 'next-move-active';
			break;
		
		case 'owner':
			$ticket_action_message = '
				<div class="col-xs-12">
					<p>'.__('Waiting for a response from','iklvb-wpresidence').' <strong>'.$vou_assign_prop_agent_name.'</strong><br> 
						('.__('within the','iklvb-wpresidence').' 
						'.date_i18n($date_format, strtotime($vou_time_update.' +'.WPW_PCV_TIME_NEXT_MOVE_OWNER.' seconds')).', 
						'.date_i18n($time_format, strtotime($vou_time_update.' +'.WPW_PCV_TIME_NEXT_MOVE_OWNER.' seconds')).')
					</p>
				</div>
			';
			$ticket_confirm_button = $ticket_confirm_button_disabled;
			$ticket_change_button = $ticket_change_button_disabled;
			break;
	}	
}
// <<<
*/


/*
if($vou_status == 'assigned' || $vou_status == 'confirmed')
{
	$ticket_action_message .= '
		<div class="col-xs-12 row-actions">
			<div class="row row-buttons">
				<div class="col-xs-12">
					'.$ticket_download_button.'
					'.$ticket_confirm_button.'
					'.$ticket_change_button.'
					'.$ticket_cancel_button.'			
				</div>
			</div>
			<div class="row row-change-data-fields">
				<div class="col-xs-6 col-sm-3">
					'.$schedule_day.'
				</div>
				<div class="col-xs-6 col-sm-3">
					'.$schedule_hour.'
				</div>
				<div class="col-xs-12 col-sm-3">
					<a href="" class="wpresidence_button" data-id="'.$post->ID.'" data-action="change-toggle" title="'.__('close','iklvb-wpresidence').'">
						<i class="fa fa-times-circle fa-3x text-danger"></i>
					</a>
					<a href="" class="wpresidence_button" data-id="'.$post->ID.'" data-action="change" title="'.__('send new date','iklvb-wpresidence').'">
						<i class="fa fa-check-circle fa-3x text-success"></i>
					</a>
				</div>
			</div>
			<div class="row row-confirm-cancel">
				<div class="col-xs-12 col-sm-8">
					<p>'.__('Are you sure you want to cancel this request? <br>(The ticket will be available again)','iklvb-wpresidence').'</p>
				</div>
				<div class="col-xs-12 col-sm-4">
					<a href="" class="wpresidence_button" data-id="'.$post->ID.'" data-action="cancel-toggle" title="'.__('No','iklvb-wpresidence').'">
						<i class="fa fa-times-circle fa-3x text-danger"></i>
					</a>
					<a href="" class="wpresidence_button" data-id="'.$post->ID.'" data-action="cancel" title="'.__('Yes','iklvb-wpresidence').'">
						<i class="fa fa-check-circle fa-3x text-success"></i>
					</a>
				</div>
			</div>
			<input type="hidden" name="ajax_ticket_nonce_'.$post->ID.'" class="ajax_ticket_nonce" value="'. wp_create_nonce( 'ajax-ticket-'.$post->ID ).'">
		</div>
	';
}
// debug 
else
{
	$ticket_action_message .= '
		<div class="col-xs-12 row-actions">
			<div class="row row-buttons">
				<div class="col-xs-12">
					'.$ticket_download_button.'
				</div>
			</div>
		</div>
	';
}
*/




/*
// se si tratta di un evento >>>
if($vou_action_type == 'event' || $vou_action_type == 'evento')
{
	$ticket_change_button = $ticket_change_button_disabled;
	$vou_action_type_more_info = ucfirst($vou_event_ticket_type);
	$product_title 	= 'Ticket Event ('.ucfirst($vou_event_ticket_type).')';
}
else
{
	$vou_action_type_more_info = $vou_price_range_min.' '.$currency.' / '.$vou_price_range_max.' '.$currency;
	// $product_title = 'Ticket Tour '.ucwords(__(str_replace('_',' ',$vou_action_type),'iklvb-wpresidence')).' ('.$vou_price_range_min.' '.$currency.' / '.$vou_price_range_max.' '.$currency.')';
	$product_title = 'Ticket Tour '.ucwords(__(str_replace('_',' ',$vou_action_type),'iklvb-wpresidence')).' '.__('up to price of','iklvb-wpresidence').' '.$vou_price_range_max.' '.$currency; // 2018-10-23 Lorenzo M.
}
// <<<
*/


echo '
	<div class="row tickets-listing status-'.$vou_status.' '.$vou_next_move_class.'" data-status="'.$vou_status.'" id="row-'.$post->ID.'">

		<div class="col-xs-4 tickets-date">
			<div class="tickets-date-wrapper">
				<span class="month">'.strtoupper($vou_schedule_month).'</span>
				<span class="day">'.$vou_schedule_day.'</span>
				<span class="hour">'.$vou_schedule_hour.'</span>
			</div>
		</div>

		<div class="col-xs-8 tickets-img">
			<span class="">'.$user_thumb_img_html.'</span>
			<span class="">'.$prop_thumb_img_html.'</span>
			'.get_ticket_action_html($vou_action_type).'
		</div>
		
	</div>
';