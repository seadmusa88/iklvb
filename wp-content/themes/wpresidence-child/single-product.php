<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

get_header();
global $post;
$post = get_post(get_option("woocommerce_shop_page_id"));
setup_postdata($post);
?>
<div class="col-md-12 iklvb-product-single">
	<!-- div class="vc_row wpb_row vc_row-fluid gray full-width page-header vertical-align-table">
		<div class="vc_row wpb_row vc_inner vc_row-fluid full-width padding-top-bottom-50 vertical-align-cell">
			<div class="vc_row wpb_row vc_inner vc_row-fluid">
				<div class="page-header-left">
					<h1><?php // the_title(); ?></h1>
				</div>
				<div class="page-header-right">
					<div class="bread-crumb-container">
						<label><?php // _e("YOU ARE HERE:", 'carservice'); ?></label>
						<ul class="bread-crumb">
							<li>
								<a href="<?php // echo esc_url(get_home_url()); ?>" title="<?php // esc_attr_e('Home', 'carservice'); ?>">
									<?php // _e('HOME', 'carservice'); ?>
								</a>
							</li>
							<li class="separator">
								&#47;
							</li>
							<li>
								<?php // the_title(); ?>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div -->
	<!-- div class="clearfix" -->
		<!-- div class="vc_row wpb_row vc_row-fluid margin-top-70" -->
			<!-- div class="vc_col-sm-9 wpb_column vc_column_container " -->
				<div class="wpb_wrapper">
					<?php
					if(have_posts()):
						woocommerce_content();
					endif;
					?>
				<!-- /div> 
			</div -->
			<!-- div class="vc_col-sm-3 wpb_column vc_column_container" -->
				<div class="wpb_wrapper">
					<div class="wpb_widgetised_column wpb_content_element clearfix">
						<div class="wpb_wrapper">
							<?php dynamic_sidebar("sidebar-shop"); ?>
						</div>
					</div>
				</div>
			<!-- /div>
		</div -->
	<!-- /div -->
</div>
<?php
global $post;
$post = get_post(get_option("woocommerce_shop_page_id"));
setup_postdata($post);
get_footer(); 
?>