<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>
<div class="product_meta">
	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php 
	
	/*if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

		<span class="sku_wrapper"><?php esc_html_e( 'SKU:', 'woocommerce' ); ?> <span class="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></span></span>

	<?php endif; 
	 * 
	 */
	?>
	
	<?php // echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>

	<?php // echo wc_get_product_tag_list( $product->get_id(), ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>
		
	<?php 
	
	$action_type = get_post_meta($product->id, 'action_type', true);
	
	
	$out = '';
	
	if(ICL_LANGUAGE_CODE == 'it')
	{
		// echo $action_type; // debug
		
		switch($action_type)
		{
			case 'evento':
				$out = '<a href="/it/biglietti-eventi-in-proprieta">Biglietti eventi in proprietà</a>';
				break;
			case 'in_vendita':
				$out = '<a href="/it/biglietti-proprieta-in-vendita">Biglietti proprietà in vendita</a>';
				break;
			case 'in_affitto':
				$out = '<a href="/it/biglietti-proprieta-in-affitto">Biglietti proprietà in affitto</a>';
				break;
		}
	}
	
	if(ICL_LANGUAGE_CODE == 'en')
	{
		switch($action_type)
		{
			case 'event':
				$out = '<a href="/tickets-events-in-properties">Tickets events in properties</a>';
				break;
			case 'for_sale':
				$out = '<a href="/tickets-properties-for-sale">Tickets properties for sale</a>';
				break;
			case 'for_rent':
				$out = '<a href="/tickets-properties-for-rent">Tickets properties for rent</a>';
				break;
		}		
	}
	
	if(!empty($out))
	{
		$out = '<span class="posted_in">'.__('Category:','woocommerce').' '.$out.'</span>';
	}
	
	echo $out;


	?>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>
