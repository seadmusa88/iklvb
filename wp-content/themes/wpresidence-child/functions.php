<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// query string per evitare di avere problemi con la cache del browser
// define('LAST_TIME_UPDATE','2018-09-07-155257');
define('LAST_TIME_UPDATE',date('YY-m-d-his'));

// valore in giorni per stabilire il tempo massimo di incasso del biglietto dopo la scadenza 
define('CASHIN_DAY_RANGE', '30');


if( !defined( 'WPR_CHILD_DIR' ) ) {
  define( 'WPR_CHILD_DIR', dirname( __FILE__ ) );      // Plugin dir
}




// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:
if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css',array('wpestate_bootstrap','wpestate_bootstrap_theme'), LAST_TIME_UPDATE); 
    }
endif;
load_child_theme_textdomain('wpresidence', get_stylesheet_directory().'/languages');
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css' );

// END ENQUEUE PARENT ACTION




/**
 * Recommended way to include parent theme styles.
 *
 * (Please see http://codex.wordpress.org/Child_Themes#How_to_Create_a_Child_Theme)
 * 
 */
add_action('wp_enqueue_scripts', 'theme_enqueue_styles', 100);
function theme_enqueue_styles() 
{
    wp_enqueue_style('child-style', get_stylesheet_directory_uri().'/style.css', array(), LAST_TIME_UPDATE);
}

add_action('admin_enqueue_scripts', 'theme_admin_styles');
function theme_admin_styles ()
{
	wp_enqueue_style('child-admin-style', get_stylesheet_directory_uri().'/css/admin.css', array(), LAST_TIME_UPDATE);
}

add_action('wp_enqueue_scripts', 'theme_my_media_styles', 100);
function theme_my_media_styles ()
{
	wp_enqueue_style('child-my-media-style', get_stylesheet_directory_uri().'/css/my_media.css', array(), LAST_TIME_UPDATE);
}

add_action('wp_enqueue_scripts', 'theme_addon_styles', 100);
function theme_addon_styles ()
{
	wp_enqueue_style('child-addon-style', get_stylesheet_directory_uri().'/css/bootstrap-xl/bootstrap-xl.css', array(), LAST_TIME_UPDATE);
}





/**
 * 2018-07-19 Lorenzo M.
 * 
 * includo file con function prese dal tema genitore e personalizzate per il tema child
 * 
 */
include(locate_template('libs/estate_listing_address.php'));
include(locate_template('libs/woo_voucher_custom.php'));
include(locate_template('libs/woo_voucher_custom_download.php'));
include(locate_template('libs/woo_voucher_custom_pdf_template_shortcode.php'));
include(locate_template('libs/wpestate_ajax_property_booking_cancel.php'));
include(locate_template('libs/wpestate_ajax_property_booking_change.php'));
include(locate_template('libs/wpestate_ajax_property_booking_confirm.php'));
include(locate_template('libs/wpestate_ajax_register_user.php'));
include(locate_template('libs/wpestate_ajax_update_profile.php'));
include(locate_template('libs/wpestate_ajax_update_ticket_cash_in.php'));
include(locate_template('libs/wpestate_ajax_update_ticket_listing_single.php'));
include(locate_template('libs/wpestate_ajax_update_ticket_payed.php'));
include(locate_template('libs/wpestate_build_dropdown_adv.php'));
include(locate_template('libs/wpestate_build_unit_show_detail.php'));
include(locate_template('libs/wpestate_get_action_select_list.php'));
include(locate_template('libs/wpestate_get_category_select_list.php'));
include(locate_template('libs/wpestate_login_form_function.php'));
include(locate_template('libs/wpestate_price_form_adv_search.php'));
include(locate_template('libs/wpestate_register_as_user.php'));
include(locate_template('libs/wpestate_scripts.php'));
include(locate_template('libs/wpestate_show_price_without_label_after.php'));
include(locate_template('libs/wpestate_show_search_field.php'));
include(locate_template('libs/wpestate_show_search_field_10.php'));
include(locate_template('libs/wpestate_timer_stop.php'));
include(locate_template('libs/wpestate_woocommerce.php'));


// function per la gestione delle email
include(locate_template('email-templates/email-functions.php'));







/**
 * 2018-07-16 Lorenzo M.
 * 
 * load delle traduzioni specifiche per il cliente
 * i file sono nella cartella /languages del tema child
 */
add_action('after_setup_theme', 'my_custom_locale');
function my_custom_locale() 
{
    load_textdomain('iklvb-wpresidence', get_stylesheet_directory().'/languages/iklvb-wpresidence-'.get_locale().'.mo');
    load_textdomain('wpwpcv', get_stylesheet_directory().'/languages/woo-property-check-voucher-'.get_locale().'.mo');
}





/**
 * 2018-06-22 Lorenzo M.
 * 
 * personalizzo la funzione per individuare il nuovo template di pagina usato per la home page
 * 
 */
function wpestate_half_map_conditions($pos_id)
{   
	if( 
		!is_category() && !is_tax() && ( basename(get_page_template($pos_id)) == 'property_list_half.php' 
		|| basename(get_page_template($pos_id)) == 'property_list_3-4.php')   // 2018-06-22 Lorenzo M.
	)
	{
		return true;
	} 
	else if( (  is_tax('') ) &&  get_option('wp_estate_property_list_type','')==2)
	{
		$taxonomy    = get_query_var('taxonomy');
		if( $taxonomy == 'property_category_agent' || 
			$taxonomy == 'property_action_category_agent' || 
			$taxonomy == 'property_city_agent' || 
			$taxonomy == 'property_area_agent' ||
			$taxonomy == 'property_county_state_agent' ||
			$taxonomy == 'category_agency' ||
			$taxonomy == 'action_category_agency' ||
			$taxonomy == 'city_agency' ||
			$taxonomy == 'area_agency' ||
			$taxonomy == 'county_state_agency' ||
			$taxonomy == 'property_category_developer' ||
			$taxonomy == 'property_action_developer' ||
			$taxonomy == 'property_city_developer' ||
			$taxonomy == 'property_area_developer' ||
			$taxonomy == 'property_county_state_developer' 

				){
			return false;
		}else{
			return true;
		}
	} else if(  is_page_template('advanced_search_results.php') &&  get_option('wp_estate_property_list_type_adv','')==2){
		 return true;   
	}else{ 
		return false; 
	}

}




/**
 * 2018-06-23 Lorenzo M.
 * 
 * includo js aggiuntivo per
 * - gestire l'icona a piede di pagina in position absolute
 * - nuove chiamate ajax update per pagine profilo utente
 */
add_action("wp_enqueue_scripts", "cs_theme_enqueue_scripts");
function cs_theme_enqueue_scripts()
{
	wp_enqueue_script("js-init-custom", get_stylesheet_directory_uri() ."/js/control_custom.js", array("jquery"), LAST_TIME_UPDATE, true);
	wp_enqueue_script("js-ajax-custom", get_stylesheet_directory_uri() ."/js/ajaxcalls_custom.js", array("jquery"), LAST_TIME_UPDATE, true);
	
	// nome della pagina in cui mi trovo
	// global $pagenow;
	
	if(is_user_logged_in())
	{
		wp_enqueue_script("js-validate-email", get_stylesheet_directory_uri() ."/js/validateEmail.js", array("jquery"), LAST_TIME_UPDATE, true);
		wp_enqueue_script("js-validate-cf", get_stylesheet_directory_uri() ."/js/validateItTaxCode.js", array("jquery"), LAST_TIME_UPDATE, true);
		wp_enqueue_script("js-validate-vat", get_stylesheet_directory_uri() ."/js/validateItVatNumber.js", array("jquery"), LAST_TIME_UPDATE, true);
	}
}


// update_post_meta(27424, '_wpw_pcv_status', 'checked');

/**
 * 2018-11-12 Lorenzo M.
 * 
 * aggiungo js nelle pagine admin
 * 
 */
add_action("admin_enqueue_scripts", "cs_admin_enqueue_scripts");
function cs_admin_enqueue_scripts()
{
	global $pagenow;
	
	if($pagenow == 'admin.php' && isset($_GET['page']) && $_GET['page'] == 'woo-vou-codes')
	{
		wp_enqueue_script("js-ajax-custom-woo-vou", get_stylesheet_directory_uri() ."/js/woo_voucher_custom_ajax_call.js", array("jquery"), LAST_TIME_UPDATE, true);
	}
}


/**
 * 2019-11-20 Lorenzo M.
 * 
 * carico un js personalizzato per la gestione del form submit di una nuova property dal front-end
 * 
 */
add_action("wp_enqueue_scripts", "cs_theme_front_end_enqueue_scripts");
function cs_theme_front_end_enqueue_scripts()
{
    if( is_page_template('front_property_submit.php') )
	{
        wp_enqueue_script('front_end_submit', get_stylesheet_directory_uri().'/js/front_end_submit.js',array('jquery'), '1.0', true);       
    }
}


/**
 * 2018-06-25 Lorenzo M.
 * 
 * Change add to cart button text on shop page in WooCommerce?
 * https://barn2.co.uk/kb/changing-add-cart-text-product-table/
 * 
 */
add_filter( 'add_to_cart_text', 'wcpt_modify_add_to_cart_text', 999 );
add_filter( 'woocommerce_product_single_add_to_cart_text', 'wcpt_modify_add_to_cart_text', 999 );

function wcpt_modify_add_to_cart_text( $text ) 
{
	$text = __('Add to My Tickets','iklvb-wpresidence'); // change as required
    return $text;
 }
 
 
 
 
/**
 * 2018-06-26 Lorenzo M.
 * 
 * rimuovo i pulsanti "aggiungi al carrello" da tutte le pagine tranne che per il singolo prodotto 
 * https://stackoverflow.com/questions/12135190/woocommerce-how-to-remove-the-add-to-cart-button-on-product-listing
 * 
 */
add_action( 'woocommerce_after_shop_loop_item', 'remove_add_to_cart_buttons', 1 );
function remove_add_to_cart_buttons() 
{
	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
}





/**
 * 2018-07-06 Lorenzo M.
 * 
 * nuovo widget personalizzato
 * 
 */
require('libs/widgets/advanced_search_v2.php');
register_widget('Advanced_Search_widget_v2');




/**
 * 2018-07-09 Lorenzo M.
 * 
 * fix rendere modulo per visual composer 
 */
if( !function_exists('wpestate_estate_property_design_related_listings') )
{
	function wpestate_estate_property_design_related_listings($attributes,$content = null)
	{   
		global $post;
		global $propid;

		$return_string =    '<div class="wpestate_estate_property_design_related_listings">'; 
		$return_string .=   '<div class="contaner"';
		$return_string .=   '<div class="row"';
		$return_string .=   wpestate_show_related_listings($propid);
		$return_string .=   '</div>'; 
		$return_string .=   '</div>'; 
		$return_string .=   '</div>'; 
		return $return_string;
	}
}


/**
 * 2018-07-16 Lorenzo M.
 * 
 * define the woocommerce_login_form_end callback 
 */
add_action( 'woocommerce_login_form_end', 'action_woocommerce_login_form_end', 10, 0 ); 
function action_woocommerce_login_form_end(  ) 
{ 
	$register_url = '/create-account/';
    echo '<p>';
	echo sprintf( __('If you are not registered go to this page to <strong><a href="%s">create a new account</a></strong>.', 'iklvb-wpresidence'), $register_url);
    echo '</p>';
} 








/**
 * 2018-07-18 Lorenzo M.
 * 
 * gestione visualizzazione campi aggiuntivi 
 * in pagina profilo utente (admin)
 */
add_filter( 'woocommerce_customer_meta_fields', 'custom_woocommerce_billing_fields' );
function custom_woocommerce_billing_fields( $fields ) 
{
	// id dell'utente che viene editato
	global $user_id;
		
	// prex($fields);
	
	// controllo
	if (!current_user_can('edit_user', $user_id)) return false;
	
	// aggiungo codice fiscale
	$fields['billing']['fields']['billing_fiscal_code'] = array
	(
		'label'       => __('Fiscal Code', 'iklvb-wpresidence'),
		'required'    => true,
		'clear'       => false,
		'type'        => 'text',
		'class'       => ''
	);
	
	// aggiungo partita iva
	$fields['billing']['fields']['billing_vat'] = array
	(
		'label'       => __('Vat Number', 'iklvb-wpresidence'),
		'required'    => true,
		'clear'       => false,
		'type'        => 'text',
		'class'       => ''	
	);

	// rendo nome azienda campo richiesto
	$fields['billing']['fields']['billing_company']['required'] = true;

	// ottengo il valore del ruolo utente
	$user_role = intval(get_user_meta( $user_id, 'user_estate_role', true) );

	if($user_role == 1)
	{	
		// rimuovo i campi che non voglio
		unset( $fields['billing']['fields']['billing_company'] );
		unset( $fields['billing']['fields']['billing_vat'] );
	}
	elseif($user_role == 2)
	{
		// rimuovo i campi che non voglio
		unset( $fields['billing']['fields']['billing_first_name'] );
		unset( $fields['billing']['fields']['billing_last_name'] );
		
		// rendo piva campo opzionale (NON richiesto)
		$fields['billing']['fields']['billing_vat']['required'] = false;
	}
	else
	{
		// rimuovo i campi che non voglio
		unset( $fields['billing']['fields']['billing_first_name'] );
		unset( $fields['billing']['fields']['billing_last_name'] );
		unset( $fields['billing']['fields']['billing_fiscal_code'] );		
	}
	
	// campi da rimuovere sempre 
	unset( $fields['billing']['fields']['billing_address_2'] );
	unset( $fields['billing']['fields']['billing_email'] );
	
	// altro
	$fields['billing']['fields']['billing_state']['description'] = '';
	$fields['shipping']['fields']['shipping_state']['description'] = '';
	
	return $fields;
}








/**
 * 2018-07-17 Lorenzo M.
 * 
 * Remove the "Additional Info" order notes
 * https://www.skyverge.com/blog/how-to-simplify-free-woocommerce-checkout/
 */
add_filter( 'woocommerce_enable_order_notes_field', '__return_false' );




/**
 * 2018-07-17 Lorenzo M.
 * 
 * Unset and customize the fields in a page checkout
 * https://www.skyverge.com/blog/how-to-simplify-free-woocommerce-checkout/
 */
add_filter( 'woocommerce_checkout_fields', 'iklvb_woocommerce_checkout_fields' );
function iklvb_woocommerce_checkout_fields( $fields ) 
{   
	// voglio la chiave 'user' prima della chiave 'billing'
	$fields = array('user' => array()) + $fields;
	
	// recupero i dati dell'utente loggato
	$current_user =   wp_get_current_user();
	
	// ottengo il valore del ruolo utente
	$user_role = intval(get_user_meta( $current_user->ID, 'user_estate_role', true) );
	
	// controllo se è stato compilato il valore richiesto
	$user_first_name = get_user_meta( $current_user->ID, 'first_name', true);	
	if(empty($user_first_name))
	{
		$fields['user']['user_first_name'] = array(
			'label'     => __('Your First Name','iklvb-wpresidence'),
			'required'  => true,
			'type'      => 'text',
		);
	}
	
	// controllo se è stato compilato il valore richiesto
	$user_last_name = get_user_meta( $current_user->ID, 'last_name', true);	
	if(empty($user_last_name))
	{
		$fields['user']['user_last_name'] = array(
			'label'     => __('Your Last Name','iklvb-wpresidence'),
			'required'  => true,
			'type'      => 'text',
		);
	}
	
	// controllo se è stato compilato il valore richiesto
	$user_phone = get_user_meta( $current_user->ID, 'phone', true);	
	if($user_role == 3) $user_phone = get_user_meta( $current_user->ID, 'agency_phone', true);	
	if($user_role == 4) $user_phone = get_user_meta( $current_user->ID, 'developer_phone', true);	
	if(empty($user_phone))
	{
		$fields['user']['user_phone'] = array(
			'label'     => __('Your Phone','iklvb-wpresidence'),
			'required'  => true,
			'type'      => 'text',
		);
	}

	// campi di fatturazione
	$fields['billing']['billing_vat'] = array(
		'label'     => __('Vat Number','iklvb-wpresidence'),
		'required'  => true,
		'type'      => 'text',
	);	
	
	$fields['billing']['billing_fiscal_code'] = array(
		'label'     => __('Fiscal Code','iklvb-wpresidence'),
		'required'  => true,
		'type'      => 'text',
	);

	$fields['billing']['billing_company']['required'] = true;


	
    // list of the billing field keys to REMOVE
	if($user_role == 1)
	{
		$billing_keys = array(
			'billing_company',
			'billing_phone',
			'billing_address_2',
			'billing_email',
			'billing_vat',
		);
	}
	elseif($user_role == 2)
	{
		$billing_keys = array(
			'billing_first_name',
			'billing_last_name',
			'billing_phone',
			'billing_address_2',
			'billing_email',
		);
		
		// in questo capo PIVA è opzionale
		$fields['billing']['billing_vat']['required'] = false;
	}
	else
	{
		$billing_keys = array(
			'billing_first_name',
			'billing_last_name',
			'billing_phone',
			'billing_address_2',
			'billing_email',
			'billing_fiscal_code',
		);		
	}
	
    // REMOVE each of those unwanted fields
    foreach( $billing_keys as $key ) unset( $fields['billing'][$key] );
	
    return $fields;
}



// add_filter('woocommerce_before_checkout_form','custom_checkout_text');
add_filter('woocommerce_after_checkout_billing_form','custom_checkout_text');
function custom_checkout_text($checkout)
{
	echo '<p>Your Checkout Data Profile:</p>';
	
	// ottengo il valore del ruolo utente
	$current_user =   wp_get_current_user();
	$user_role = intval(get_user_meta( $current_user->ID, 'user_estate_role', true) );
	
	// compongo tutti i campi da visualizzare
	$user_first_name = trim($checkout->get_value('first_name'));
	$user_last_name = trim($checkout->get_value('last_name'));
	$user_email = trim($checkout->get_value('email'));
	
	$user_phone = trim($checkout->get_value('phone'));
	if($user_role == 3) $user_phone = trim($checkout->get_value('agency_phone'));
	if($user_role == 4) $user_phone = trim($checkout->get_value('developer_phone'));	
	
	$billing_first_name = trim($checkout->get_value('billing_first_name'));
	$billing_last_name = trim($checkout->get_value('billing_last_name'));
	$billing_company = trim($checkout->get_value('billing_company')); 
	
	$billing_address_1 = trim($checkout->get_value('billing_address_1'));
	$billing_city = trim($checkout->get_value('billing_city'));
	$billing_postcode = trim($checkout->get_value('billing_postcode'));
	$billing_state = trim($checkout->get_value('billing_state'));
	$billing_country = trim($checkout->get_value('billing_country'));
	
	$billing_vat = trim($checkout->get_value('billing_vat'));
	$billing_fiscal_code = trim($checkout->get_value('billing_fiscal_code'));
	
	// contatore errori per decidere se visualizzare il link alla pagina edit profilo
	$error_count = 0;
	
	$user_name = trim($user_first_name.' '.$user_last_name);
	if(empty($user_first_name) || empty($user_last_name)) 
	{
		$error_count++;
		$user_name .= ' <span class="error">('.__('some fields are empty...', 'iklvb-wpresidence').')</span>';
	}
	
	if(empty($user_email)) 
	{
		$error_count++;
		$user_email .= ' <span class="error">('.__('the field is empty...', 'iklvb-wpresidence').')</span>';
	}
	elseif(!is_valid_email($user_email)) 
	{
		$error_count++;
		$user_email .= ' <span class="error">('.__('the value is not valid...', 'iklvb-wpresidence').')</span>';
	}
	
	if(empty($user_phone)) 
	{
		$error_count++;
		$user_phone .= ' <span class="error">('.__('the field is empty...', 'iklvb-wpresidence').')</span>';
	}

	// testo da visualizzare nella pagina checkout come riasssunto dei dati già inseriti
	echo '
		<div class="container container-message-checkout">
			<div class="row">
				<div class="col-md-4">'.__('Your Name', 'iklvb-wpresidence').':</div>
				<div class="col-md-8">'.$user_name.'</div>
			</div>		
			<div class="row">
				<div class="col-md-4">'.__('Your Email', 'iklvb-wpresidence').':</div>
				<div class="col-md-8">'.$user_email.'</div>
			</div>
			<div class="row">					
				<div class="col-md-4">'.__('Your Phone', 'iklvb-wpresidence').':</div>
				<div class="col-md-8">'.$user_phone.'</div>
			</div>
		</div>
	';	
	
	// compongo i campi da visualizzare in base al tipo di utente
	if($user_role == 1)
	{
		$billing_name = trim($billing_first_name.' '.$billing_last_name);
		if(empty($billing_first_name) || empty($billing_last_name)) 
		{
			$error_count++;
			$billing_name .= ' <span class="error">('.__('some fields are empty...', 'iklvb-wpresidence').')</span>';
		}
		
		$billing_address_full = trim($billing_address_1.', '.$billing_city.' - '.$billing_postcode.' - '.$billing_state.' ('.$billing_country.')');
		if(empty($billing_address_1) || empty($billing_city) || empty($billing_postcode) || empty($billing_state) || empty($billing_country)) 
		{
			$error_count++;
			$billing_address_full .= ' <span class="error">('.__('some fields are empty...', 'iklvb-wpresidence').')</span>';
		}

		if(empty($billing_fiscal_code)) 
		{
			$error_count++;
			$billing_fiscal_code .= ' <span class="error">('.__('the field is empty...', 'iklvb-wpresidence').')</span>';
		}
		elseif(!is_valid_fiscal_code($billing_fiscal_code)) 
		{
			$error_count++;
			$billing_fiscal_code .= ' <span class="error">('.__('the value is not valid...', 'iklvb-wpresidence').')</span>';
		}
		
		// testo da visualizzare nella pagina checkout come riassunto dei dati già inseriti
		echo '
			<div class="container container-message-checkout">
				<div class="row">
					<div class="col-md-4">'.__('Billing Name', 'iklvb-wpresidence').':</div>
					<div class="col-md-8">'.$billing_name.'</div>
				</div>		
				<div class="row">
					<div class="col-md-4">'.__('Billing Address', 'iklvb-wpresidence').':</div>
					<div class="col-md-8">'.$billing_address_full.'</div>
				</div>
				<div class="row">					
					<div class="col-md-4">'.__('Billing Fiscal Code', 'iklvb-wpresidence').':</div>
					<div class="col-md-8">'.$billing_fiscal_code.'</div>
				</div>
			</div>
		';
	}
	elseif($user_role == 2)
	{		
		if(empty($billing_company)) 
		{
			$error_count++;
			$billing_company .= ' <span class="error">('.__('the field is empty...', 'iklvb-wpresidence').')</span>';
		}
		
		$billing_address_full = trim($billing_address_1.', '.$billing_city.' - '.$billing_postcode.' - '.$billing_state.' ('.$billing_country.')');
		if(empty($billing_address_1) || empty($billing_city) || empty($billing_postcode) || empty($billing_state) || empty($billing_country)) 
		{
			$error_count++;
			$billing_address_full .= ' <span class="error">('.__('some fields are empty...', 'iklvb-wpresidence').')</span>';
		}

		if(empty($billing_fiscal_code)) 
		{
			$error_count++;
			$billing_fiscal_code .= ' <span class="error">('.__('the field is empty...', 'iklvb-wpresidence').')</span>';
		}
		elseif(!is_valid_fiscal_code($billing_fiscal_code)) 
		{
			$error_count++;
			$billing_fiscal_code .= ' <span class="error">('.__('the value is not valid...', 'iklvb-wpresidence').')</span>';
		}
		
		if(strlen($billing_vat) && !is_valid_vat($billing_vat)) 
		{
			$error_count++;
			$billing_vat .= ' <span class="error">('.__('the value is not valid...', 'iklvb-wpresidence').')</span>';
		}
		
		// testo da visualizzare nella pagina checkout come riassunto dei dati già inseriti
		echo '
			<div class="container container-message-checkout">
				<div class="row">
					<div class="col-md-4">'.__('Billing Company / Freelance', 'iklvb-wpresidence').':</div>
					<div class="col-md-8">'.$billing_company.'</div>
				</div>		
				<div class="row">
					<div class="col-md-4">'.__('Billing Address', 'iklvb-wpresidence').':</div>
					<div class="col-md-8">'.$billing_address_full.'</div>
				</div>
				<div class="row">					
					<div class="col-md-4">'.__('Billing Fiscal Code', 'iklvb-wpresidence').':</div>
					<div class="col-md-8">'.$billing_fiscal_code.'</div>
				</div>
				'.(strlen($billing_vat) ? '
				<div class="row">					
					<div class="col-md-4">'.__('Billing Vat', 'iklvb-wpresidence').':</div>
					<div class="col-md-8">'.$billing_vat.'</div>
				</div>
				' : '').'
			</div>
		';
	}
	else
	{
		if(empty($billing_company)) 
		{
			$error_count++;
			$billing_company .= ' <span class="error">('.__('the field is empty...', 'iklvb-wpresidence').')</span>';
		}
		
		$billing_address_full = trim($billing_address_1.', '.$billing_city.' - '.$billing_postcode.' - '.$billing_state.' ('.$billing_country.')');
		if(empty($billing_address_1) || empty($billing_city) || empty($billing_postcode) || empty($billing_state) || empty($billing_country)) 
		{
			$error_count++;
			$billing_address_full .= ' <span class="error">('.__('some fields are empty...', 'iklvb-wpresidence').')</span>';
		}

		if(empty($billing_vat)) 
		{
			$error_count++;
			$billing_vat .= ' <span class="error">('.__('the field is empty...', 'iklvb-wpresidence').')</span>';
		}
		elseif(!is_valid_vat($billing_vat)) 
		{
			$error_count++;
			$billing_vat .= ' <span class="error">('.__('the value is not valid...', 'iklvb-wpresidence').')</span>';
		}
		
		// testo da visualizzare nella pagina checkout come riasssunto dei dati già inseriti
		echo '
			<div class="container container-message-checkout">
				<div class="row">
					<div class="col-md-4">'.__('Billing Company', 'iklvb-wpresidence').':</div>
					<div class="col-md-8">'.$billing_company.'</div>
				</div>		
				<div class="row">
					<div class="col-md-4">'.__('Billing Address', 'iklvb-wpresidence').':</div>
					<div class="col-md-8">'.$billing_address_full.'</div>
				</div>
				<div class="row">					
					<div class="col-md-4">'.__('Billing Vat', 'iklvb-wpresidence').':</div>
					<div class="col-md-8">'.$billing_vat.'</div>
				</div>
			</div>
		';		
	}
	
	// link a pagina edit profilo
	$profile_edit_url = '/my-profile/';
	
	// se sono presenti dei campi vuoti con valore non valido
	if($error_count > 0)
	{
		echo '<p>';
	    echo sprintf( __('There are some errors or some data is missing, before proceeding <strong><a href="%s">edit your profile data</a></strong>.', 'iklvb-wpresidence'), $profile_edit_url);
		echo '</p>';
	}
	else
	{
		echo '<p>';
	    echo sprintf( __('If before proceeding you want to change your data go to <strong><a href="%s">edit your profile data</a></strong>.', 'iklvb-wpresidence'), $profile_edit_url);
		echo '</p>';		
	}
}





/**
 * 2018-07-20 Lorenzo M.
 * 
 * verifico ancora una volta i campi di fatturazione
 * prima di completare l'ordine
 * 
 * controllo solo cf e piva
 * gli alri campi vengono già controllati che non siano vuoti (grazie al valore required = true)
 * 
 * https://stackoverflow.com/questions/28603144/custom-validation-of-woocommerce-checkout-fields
 */
add_action('woocommerce_checkout_process', 'iklvb_woocommerce_checkout_process');

function iklvb_woocommerce_checkout_process() 
{ 
	// ottengo il valore del ruolo utente
	$current_user =   wp_get_current_user();
	$user_role = intval(get_user_meta( $current_user->ID, 'user_estate_role', true) );
	
    $billing_vat = get_user_meta($current_user->ID,'billing_vat',true);
    $billing_fiscal_code = get_user_meta($current_user->ID,'billing_fiscal_code',true);
    
	// controllo i campi in base al tipo di utente
	if($user_role == 1 || $user_role == 2)
	{
		if(strlen($billing_fiscal_code) && !is_valid_fiscal_code($billing_fiscal_code)) 
		{
			wc_add_notice( __( '<strong>ERROR</strong>: Billing Fiscal Code is not valid.', 'iklvb-wpresidence' ), 'error' );
		}
	}
	
	if($user_role != 1)
	{
		if(strlen($billing_vat) && !is_valid_vat($billing_vat)) 
		{
			wc_add_notice( __( '<strong>ERROR</strong>: Billing Vat Number is not valid.', 'iklvb-wpresidence' ), 'error' );
		}
	}
}









/**
 * 2018-07-18 Lorenzo M.
 * 
 * controllo i campi salvati su db 
 * e visualizzo i messaggi di errore relativi ai campi richiesti e ai campi non validi
 */
add_action('admin_notices', 'iklvb_user_edit_errors', 100);
function iklvb_user_edit_errors() 
{
	// nome della pagina in cui mi trovo
	global $pagenow;
	
	if(is_admin() && $pagenow == 'user-edit.php')
	{
		// id dell'utente che viene editato
		global $user_id;
		
		$user_role = intval(get_user_meta( $user_id, 'user_estate_role', true) );
		
		$billing_first_name = get_user_meta( $user_id, 'billing_first_name', true);
		$billing_last_name = get_user_meta( $user_id, 'billing_last_name', true);
		$billing_company = get_user_meta( $user_id, 'billing_company', true);
		$billing_address_1 = get_user_meta( $user_id, 'billing_address_1', true);
		$billing_city = get_user_meta( $user_id, 'billing_city', true);
		$billing_postcode = get_user_meta( $user_id, 'billing_postcode', true);
		$billing_country = get_user_meta( $user_id, 'billing_country', true);
		$billing_state = get_user_meta( $user_id, 'billing_state', true);
		$billing_vat = get_user_meta( $user_id, 'billing_vat', true);
		$billing_fiscal_code = get_user_meta( $user_id, 'billing_fiscal_code', true);
		
		$my_errors = new WP_Error();

		// utente privato
		if($user_role == 1)
		{
			if(empty($billing_first_name)) $my_errors->add('empty', __('<strong>ERROR</strong>: Billing First Name is empty.', 'iklvb-wpresidence'));
			if(empty($billing_last_name)) $my_errors->add('empty', __('<strong>ERROR</strong>: Billing Last Name is empty.', 'iklvb-wpresidence'));
		}
		else
		{
			if(empty($billing_company)) $my_errors->add('empty', __('<strong>ERROR</strong>: Billing Company is empty.', 'iklvb-wpresidence'));			
		}
		
		
		if(empty($billing_address_1)) $my_errors->add('empty', __('<strong>ERROR</strong>: Billing Address is empty.', 'iklvb-wpresidence'));			
		if(empty($billing_city)) $my_errors->add('empty', __('<strong>ERROR</strong>: Billing City is empty.', 'iklvb-wpresidence'));			
		if(empty($billing_postcode)) $my_errors->add('empty', __('<strong>ERROR</strong>: Billing Postocde is empty.', 'iklvb-wpresidence'));			
		if(empty($billing_country)) $my_errors->add('empty', __('<strong>ERROR</strong>: Billing Country is empty.', 'iklvb-wpresidence'));			
		if(empty($billing_state)) $my_errors->add('empty', __('<strong>ERROR</strong>: Billing State is empty.', 'iklvb-wpresidence'));			
		
		
		if($user_role == 1)
		{
			if(empty($billing_fiscal_code))
			{
				$my_errors->add('empty', __('<strong>ERROR</strong>: Billing Fiscal Code is empty.', 'iklvb-wpresidence'));
			}
			elseif(!is_valid_fiscal_code($billing_fiscal_code))
			{
				$my_errors->add('invalid', __('<strong>ERROR</strong>: Billing Fiscal Code is not valid.', 'iklvb-wpresidence'));
			}
		}
		elseif($user_role == 2)
		{
			if(empty($billing_fiscal_code))
			{
				$my_errors->add('empty', __('<strong>ERROR</strong>: Billing Fiscal Code is empty.', 'iklvb-wpresidence'));
			}
			elseif(!is_valid_fiscal_code($billing_fiscal_code))
			{
				$my_errors->add('invalid', __('<strong>ERROR</strong>: Billing Fiscal Code is not valid.', 'iklvb-wpresidence'));
			}
			
			if(strlen($billing_vat) && !is_valid_vat($billing_vat))
			{
				$my_errors->add('invalid', __('<strong>ERROR</strong>: Billing Vat Number is not valid.', 'iklvb-wpresidence'));
			}
		}
		else
		{
			if(empty($billing_vat))
			{
				$my_errors->add('empty', __('<strong>ERROR</strong>: Billing Vat Number is empty.', 'iklvb-wpresidence'));
			}
			elseif(!is_valid_vat($billing_vat))
			{
				$my_errors->add('invalid', __('<strong>ERROR</strong>: Billing Vat Number is not valid.', 'iklvb-wpresidence'));
			}
		}
		
		if($my_errors->get_error_codes())
		{
			echo '<div class="notice notice-error">';
			foreach($my_errors->get_error_messages() as $err) echo '<p>'.$err.'</p>';
			echo '</div>';				
		}
	}
}




/**
 * 2018-07-19 Lorenzo M.
 * 
 * modifico la funzione che si ocuupa di eseguire mebed video di youtube
 * aggiungendo il paramentro autoplay richiesto dal cliente
 * 
 */
if( !function_exists('custom_youtube_video') ):
    function  custom_youtube_video($video_id){
        $protocol = is_ssl() ? 'https' : 'http';
        return $return_string='
        <div style="max-width:100%;" class="video">
            <iframe id="player_2" title="YouTube video player" src="'.$protocol.'://www.youtube.com/embed/' . $video_id  . '?autoplay=1&wmode=transparent&amp;rel=0" allowfullscreen ></iframe>
        </div>';
    }
endif; // end   custom_youtube_video  




/**
 * Lorenzo M. 2018-08-14
 * 
 * aggiungo controllo headers per alcune pagine amministrative di woocommerce
 * e altre pagine custom
 */
if( !function_exists('wpestate_is_user_dashboard') ):
function wpestate_is_user_dashboard(){
   
    if ( basename( get_page_template() ) == 'user_dashboard.php'          || 
        basename( get_page_template() ) == 'user_dashboard_add.php'      ||
        basename( get_page_template() ) == 'user_dashboard_profile.php'  ||
        basename( get_page_template() ) == 'user_dashboard_favorite.php' ||
        basename( get_page_template() ) == 'user_dashboard_searches.php' ||
        basename( get_page_template() ) == 'user_dashboard_floor.php' ||
        basename( get_page_template() ) == 'user_dashboard_search_result.php' ||
        basename( get_page_template() ) == 'user_dashboard_invoices.php' ||
        basename( get_page_template() ) == 'user_dashboard_add_agent.php' ||
        basename( get_page_template() ) == 'user_dashboard_agent_list.php' || 
        basename( get_page_template() ) == 'user_dashboard_inbox.php' 
			
		|| basename( $_SERVER['REQUEST_URI'] ) == 'orders'
		|| basename( $_SERVER['REQUEST_URI'] ) == 'downloads'	
        ){
     
        return true;
    }else{
        return false;
    }
}
endif;



/**
 * 2018-08-19 Lorenzo M. 
 * 
 * preparo il testo da utilizzare per visualizzare a video lo stato del ticket
 * 
 * @param string $_status
 * @return type
 */
function get_ticket_status_html($_status = '')
{
	// controllo
	if(empty($_status)) $_status = 'available';
	
	// init
	$html = '';
	$description = '';
	
	switch($_status)
	{
		case 'available': 
			$description = 'The ticket is available for use.';
			break;
		
		case 'assigned': 
			$description = 'The ticket has been assigned to a property, the date of visit is being confirmed.';
			break;
		
		case 'confirmed': 
			$description = 'Visit date has been confirmed, owner and visitor are awaiting the meeting. The date can be changed up to 24 hours in advance.';
			break;
		
		case 'performed': 
			$description = 'Visit date confirmed and already passed: meeting occurred.';
			break;
		
		case 'checked': 
			$description = 'The owner of the building has executed the request for collection of the ticket.';
			break;
		
		case 'payed': 
			$description = 'iKLVB made the payment to the owner of the property.';
			break;
		
		case 'expired': 
			$description = 'The ticket is no longer usable because the expiration date has expired.';
			break;
	}
	
	// html per indicare lo status del biglietto
	$html = '
		<div class="ticket-status">
			<div title="'. __($description,'iklvb-wpresidence') .'">
				<span class="tag-text tag-round tag-color-'.$_status.'"></span>
				<span class="ticket-status-text">'. ucfirst(__($_status,'iklvb-wpresidence')) .'</span>
			</div>
		</div>
	';
	
	return $html;
}





/**
 * Lorenzo M. 2018-08-19
 * 
 * preparo il testo da utilizzare per visualizzare a video il tipo di action
 * 
 * @param string $_action
 * @return type
 */
function get_ticket_action_html($_action = '')
{
	// init
	$html = '';
	$icon_name = '';
	
	switch($_action)
	{
		case 'for_sale': 
			$icon_name = 'home';
			$description = 'Ticket TOUR for SALE';
			break;
		
		case 'in_vendita': 
			$icon_name = 'home';
			$description = 'Biglietto TOUR in VENDITA';
			break;
		
		case 'for_rent': 
			$icon_name = 'flag';
			$description = 'Ticket TOUR for RENT';
			break;
		
		case 'in_affitto': 
			$icon_name = 'flag';
			$description = 'Biglietto TOUR in AFFITTO';
			break;
		
		case 'event': 
			$icon_name = 'ticket';
			$description = 'Ticket EVENT';
			break;		
		
		case 'evento': 
			$icon_name = 'ticket';
			$description = 'Biglietto EVENTO';
			break;		
	}
	
	// html per indicare il tipo di action del biglietto
	$html = '
		<div class="ticket-action">
			<div title="'. __($description,'iklvb-wpresidence') .'">
				<span class="tag-text tag-round tag-color-action"><i class="fa fa-'.$icon_name.'"></i></span>
				<span class="ticket-action-text">'. ucfirst(__($_action,'iklvb-wpresidence')) .'</span>
			</div>
		</div>
	';
	
	return $html;
}






/**
 * Lorenzo M. 2018-08-26
 * cerco il biglietto più adatto da acquistare per la proprietà indicata (TOUR)
 */
function get_tour_ticket_link($_prop_action_type,$_prop_price)
{
	// init
	$results = array(
		'price_range_min' => '',
		'price_range_max' => '',
		'shop_permalink' => '/shop/',
	);
	
	// ricerca
	$args = array(
		'post_type'         => 'product',
		'post_status'       => 'publish',
		'posts_per_page'    => -1,	
		'orderby'			=> 'price_range_max',	// devo considerare il biglietto con il prezzo più vicino a quello della property // 2019-02-02 Lorenzo M.
		'order'				=> 'ASC',
		'meta_query' => array(
			array(
				'key'       => 'action_type',
				'value'     => $_prop_action_type,
				'compare'   => '='
			),
			/*
			 * 2018-10-22 Lorenzo M.
			 * il cliente non vuole più considerare il limite inferiore di prezzo
			 * 			 * 
			array(
				'key'       => 'price_range_min',
				'value'     => $_prop_price,
				'compare'   => '<=',
				'type'		=> 'NUMERIC'
			),
			 * 
			 */
			array(
				'key'       => 'price_range_max',
				'value'     => $_prop_price,
				'compare'   => '>=',
				'type'		=> 'NUMERIC'
			),
		),
	);

	// query
	$tickets_results = new WP_Query($args);
	
	// controllo i risultati
	if($tickets_results->post_count != 0)
	{
		$ticket_id = $tickets_results->post->ID;
		$results['price_range_min'] = get_post_meta($ticket_id, 'price_range_min',	true);
		$results['price_range_max'] = get_post_meta($ticket_id, 'price_range_max',	true);
		$results['shop_permalink'] = get_permalink($ticket_id);
	}
	
	// return
	return $results;	
}


/**
 * Lorenzo M. 2018-08-26
 * cerco il biglietto più adatto da acquistare per la proprietà indicata (EVENT)
 */
function get_event_ticket_link($_prop_event_ticket_type)
{
	// init
	$results = array(
		'shop_permalink' => '/shop/',
	);
	
	// ricerca
	$args = array(
		'post_type'         => 'product',
		'post_status'       => 'publish',
		'posts_per_page'    => -1,
		'meta_query' => array(
			array(
				'key'       => 'action_type',
				'value'     => 'event',
				'compare'   => '='
			),
			array(
				'key'       => 'event_ticket_type',
				'value'     => $_prop_event_ticket_type,
				'compare'   => '='
			),
		),
	);

	// query
	$tickets_results = new WP_Query($args);
	
	// controllo i risultati
	if($tickets_results->post_count != 0)
	{
		$ticket_id = $tickets_results->post->ID;
		$results['shop_permalink'] = get_permalink($ticket_id);
	}
	
	// return
	return $results;	
}





/**
 * Lorenzo M. 2018-08-29
 * 
 * compongo il campo input datepiker
 * 
 * @param type $_next_move
 * @return string
 */
function make_schedule_day($_min_date = '',$_max_date = '')
{
	// controllo
	if(empty($_min_date)) $_min_date = WPW_PCV_TIME_NEXT_MOVE_VISITOR;
	
	// formato data impostato in wordpress
	$date_format = get_option('date_format');
	
	// formato data per jQuery datepicker
	switch($date_format)
	{
		case 'F j, Y'	: 	$date_format_datepiker = 'MM d, yy';	/* $date_format = 'M j, Y';	*/ break;
		case 'm/d/Y'	: 	$date_format_datepiker = 'mm/dd/yy';	break;
		case 'd/m/Y'	: 	$date_format_datepiker = 'dd/mm/yy';	break;		
		case 'Y-m-d'	: 	
		default			:	$date_format_datepiker = 'yy-mm-dd';	break;
	}

	// data minima e massima per il calendario
	$min_date = date('Y\, m\, d', strtotime('+'.$_min_date.' seconds'));
	$max_date = (empty($_max_date) ? '' : date('Y\, m\, d', strtotime($_max_date)));

	// campo datepicker
	$schedule_day = '
		<label>'.__('Select a Date','iklvb-wpresidence').'</label>
		<input name="schedule_day" type="input" readonly
			placeholder="'. __('Day','wpestate').'" 
			class="form-control datepicker readonly-to-default schedule_day_custom"
			min="'.$min_date.'"
			max="'.$max_date.'"
			date-format="'.$date_format_datepiker.'"
			aria-required="true">
	';	
	
	return $schedule_day;
}



/**
 * Lorenzo M. 2018-08-29
 * 
 * compongo il campo select con gli intervalli di tempo 
 * 
 * @return string
 */
function make_schedule_hour()
{
	// formato ora impostato in wordpress
	$time_format = get_option('time_format');
	
	// preparo i valori per la select delle ore
	$schedule_hour_option = '<option value="">--</option>';
	for ($i = 0; $i <= 23; $i++)
	{
		for ($j = 0; $j <= 45; $j+=30) // Lorenzo M. // cambiare 30 in 15 per intervalli inferiori
		{
			$show_j = $j;
			if($j == 0) $show_j = '00';

			$val = str_pad($i, 2, '0', STR_PAD_LEFT).':'.$show_j;
			$schedule_hour_option .= '<option value="'.$val.'">'.date_i18n($time_format,strtotime($val)).'</option>';
		}
	}

	// select per le ore
	$schedule_hour = '
		<label>'.__('Select a Time','iklvb-wpresidence').'</label>
		<select name="schedule_hour" class="form-control schedule_hour">
			'.$schedule_hour_option.'
		</select>
	';
	
	return $schedule_hour;
}













function iklvb_get_user_image($_user_id = '', $_class = '', $_style = '', $_get_url = false)
{
	$preview_img  = get_template_directory_uri().'-child/img/default_user.png';
	
	// id del profilo collegato a questo utente (chi ha acquistato il biglietto)
	// (es. se si tratta di agente/agenzia/costruttore)
	$user_agent_id = get_user_meta($_user_id,'user_agent_id',true);
	
	if(is_numeric($user_agent_id))
	{
		$preview         = wp_get_attachment_image_src(get_post_thumbnail_id($user_agent_id), 'thumbnail');
		$preview_img     = $preview[0];
	}
	
	if($preview_img == '' && is_numeric($_user_id) && $_user_id > 0)
	{
		$preview		 = wp_get_attachment_image_src(get_the_author_meta('small_custom_picture',$_user_id),'thumbnail');
		$preview_img     = $preview[0];
	}

	$out = '<img src="'.$preview_img.'" style="'.$_style.'" class="'.$_class.'">';
	
	if($_get_url) $out = $preview_img;
	
	return $out;
}



function iklvb_get_user_image_cover($_user_id = '', $_class = 'agent-image-cover', $_style = '')
{
	
	$agent_permalink = '';
	$preview_img  = get_template_directory_uri().'-child/img/default_user.png';
	   
	// id del profilo collegato a questo utente (chi ha acquistato il biglietto)
	// (es. se si tratta di agente/agenzia/costruttore)
	$user_agent_id = get_user_meta($_user_id,'user_agent_id',true);
	
	if(is_numeric($user_agent_id))
	{
		$preview            = wp_get_attachment_image_src(get_post_thumbnail_id($user_agent_id), 'thumbnail');
		$preview_img        = $preview[0];
		$agent_permalink	= get_the_permalink($user_agent_id);
	}
	
	if( ($preview_img == '' || empty($user_agent_id) ) && is_numeric($_user_id) && $_user_id > 0)
	{
		$preview		 = wp_get_attachment_image_src(get_the_author_meta('small_custom_picture',$_user_id),'thumbnail');
		$preview_img     = $preview[0];
		$agent_permalink = '';
	}

	$replace = '<a '.(strlen($agent_permalink) ? 'href="'.$agent_permalink.'"' : '').' style="background-image:url('.$preview_img.');'.$_style.'" class="'.$_class.'"></a>';
	
	return $replace;
}



function iklvb_get_user_agent_name($_user_id)
{
	$user_agent_name = '';
	
	// id del profilo collegato a questo utente (chi ha acquistato il biglietto)
	// (es. se si tratta di agente/agenzia/costruttore)
	$user_agent_id = get_user_meta($_user_id,'user_agent_id',true);

	// nome del visitatore (chi ha acquistato il biglietto)
	if(is_numeric($user_agent_id))
	{
		$user_agent_name = get_the_title($user_agent_id);
	}
	
	if($user_agent_name == '' && is_numeric($_user_id) && $_user_id > 0)
	{
		$first_name = get_the_author_meta('first_name',$_user_id);
		$last_name = get_the_author_meta('last_name',$_user_id);
		$user_agent_name = trim($first_name.' '.$last_name);
	}
	
	return $user_agent_name;
}



/**
 * property address
 * 
 * @param type $_property_id
 */
function iklvb_get_property_address($_property_id = '')
{
	$property_address_full = '';
	
	if(is_numeric($_property_id))
	{
	    $property_address   = esc_html( get_post_meta($_property_id, 'property_address', true) );
		$property_city      = strip_tags ( get_the_term_list($_property_id, 'property_city', '', ', ', '') );
		$property_county    = esc_html( get_post_meta($_property_id, 'property_county', true) );		// provincia
		$property_country   = esc_html(get_post_meta($_property_id, 'property_country', true) );		// stato
		
		$property_address_full = $property_address.', '.$property_city.' ('.$property_county.') - '.$property_country;
	}
	
	return $property_address_full;
}



/**
 * property main image
 * 
 * @param type $_property_id
 */
function iklvb_get_property_image($_property_id = '', $_class = '', $_style = '')
{
	$preview_img = '';
	
	if(is_numeric($_property_id))
	{
		$preview            = wp_get_attachment_image_src(get_post_thumbnail_id($_property_id), 'thumbnail');
		$preview_img        = $preview[0];
	}

	if($preview_img == '')
	{
	   $preview_img  = get_template_directory_uri().'-child/img/default_property_featured.png';
	}

	$replace = '<img src="'.$preview_img.'" style="'.$_style.'" class="'.$_class.'">';
	
	return $replace;
}


function iklvb_get_schedule_box($_time)
{
	// formato data e ora impostati in wordpress
	$time_format = get_option('time_format');

	$vou_schedule_month		= date_i18n('M', strtotime($_time));
	$vou_schedule_day		= date_i18n('d', strtotime($_time));
	$vou_schedule_hour		= date_i18n($time_format, strtotime($_time));
	
	$html = '
		<div style="width:100px; height:100px; display: inline-block; margin:10px 5px 0px 5px; padding:0; border:0; vertical-align: top;">
			<span style="display: inline-block; color:#212121; text-align: center; width: 100%; margin: 0; font-size: 18pt; line-height: 1.2; font-family:Arial Black; font-weight: 700;">'.strtoupper($vou_schedule_month).'</span>
			<span style="display: inline-block; color:#212121; text-align: center; width: 100%; margin: 0; font-size: 28pt; line-height: 1.2; font-family:Arial Black; font-weight: 700;">'.$vou_schedule_day.'</span>
			<span style="display: inline-block; color:#212121; text-align: center; width: 100%; margin: 0; font-size: 16pt; line-height: 1.2;">'.$vou_schedule_hour.'</span>
		</div>
	';
	
	return $html;
}



/**
 * controllo se si tratta di una property di tipo evento
 * 
 * @param type $_action_category_id
 * @return boolean
 */
function iklvb_is_property_event($_action_category_id = '')
{
	$category = get_term_by('id', $_action_category_id, 'property_action_category'); 
	$slug = $category->slug;
	
	if(strpos($slug,'event') !== false) return true;
	
	return false;
}



/**
 * controllo se si tratta di una property in affitto
 * 
 * @param type $_action_category_id
 * @return boolean
 */
function iklvb_is_property_for_rent($_action_category_id = '')
{
	$category = get_term_by('id', $_action_category_id, 'property_action_category'); 
	$slug = $category->slug;
	
	if($slug === 'for-rent' || $slug === 'in-affitto') return true;
	
	return false;
}







/**
 * 2018-09-26 Lorenzo M.
 * 
 * array di campi di un gruppo di ACF
 * (versione custom: ho cambito il pramaetro title in slug)
 * 
 * source:
 * https://wordpress.stackexchange.com/questions/95126/acf-get-fields-from-group
 * 
 * @param type $_slug
 * @return boolean
 */
function get_acf_group_fields($_group_slug = '')
{
	$group = get_page_by_path($_group_slug, OBJECT, 'acf');
	if(empty($group)) return false;

	$meta = get_post_meta($group->ID);
	$acf_fields = array();

	foreach($meta as $key => $value)
	{
		$acf_meta_key = stristr($key,'field_'); // acf fields all start with "field_"
		if($acf_meta_key) $acf_fields[] = get_field_object($key);
	}

	// returns an array of field objects
	return $acf_fields; 
}



function get_acf_field_key($_group_slug = '', $_field_name = '')
{
	// init
	$field_key = false;
	
	// gruppo di campi
	$fields = get_acf_group_fields($_group_slug);

	// ciclo tra i campi del gruppo
	foreach($fields as $field)
	{
		if($field['name'] == $_field_name) $field_key = $field['key'];
	}
	
	return $field_key;
}


///////////////////////////////////////////////////////////////////////////////////////////
/////// Define thumb sizes
///////////////////////////////////////////////////////////////////////////////////////////

if( !function_exists('wpestate_image_size') ): 
    function wpestate_image_size(){
        add_image_size('user_picture_profile', 255, 143, true);
        //  add_image_size('agent_picture_single_page', 320, 180, true);
        add_image_size('agent_picture_thumb' , 120, 120, true);
        add_image_size('blog_thumb'          , 272, 189, true);
        add_image_size('blog_unit'           , 1110, 385, true);
        add_image_size('slider_thumb'        , 143,  83, true);
        add_image_size('property_featured_sidebar',768,662,true);
        //  add_image_size('property_featured_sidebar',261,225,true);
        // add_image_size('blog-full'           , 940, 529, true);
        // add_image_size('property_listings'   , 525, 328, true); // 1.62 was 265/163 until v1.12
        add_image_size('property_listings'   , 640, 400, true); // 1.62 was 265/163 until v1.12
        add_image_size('property_full'       , 980, 777, true);
        add_image_size('listing_full_slider' , 835, 467, true);
        add_image_size('listing_full_slider_1', 1110, 623, true);
        add_image_size('property_featured'   , 940, 390, true);
        add_image_size('property_full_map'   , 1920, 790, true);
        add_image_size('property_map1'       , 400, 161, true);
        add_image_size('widget_thumb'        , 105, 70, true);
        add_image_size('user_thumb'          , 45, 45, true);
        add_image_size('custom_slider_thumb'          , 36, 36, true);
        
        set_post_thumbnail_size(  250, 220, true);
    }
endif;



/**
 * 2018-11-18 Lorenzo M.
 * 
 * nuova dimensione per immagini thumb in pagina carrello
 * 
 * source:
 * https://iconicwp.com/blog/manage-woocommerce-product-image-sizes-3-3/
 * 
 */
add_action( 'after_setup_theme', 'iconic_modify_theme_support', 10 );
function iconic_modify_theme_support() 
{
    $theme_support = get_theme_support( 'woocommerce' );
    $theme_support = is_array( $theme_support ) ? $theme_support[0] : array();
 
    // $theme_support['single_image_width'] = 800;
    $theme_support['thumbnail_image_width'] = 150;
 
    remove_theme_support( 'woocommerce' );
    add_theme_support( 'woocommerce', $theme_support );
}
 



/**
 * 2018-12-06 Lorenzo M.
 * 
 * non voglio usare la pagina /shop/ di woocommerce
 * e quindi gestisco il suo redirect
 * 
 * source:
 * https://stackoverflow.com/questions/37350704/woocommerce-how-to-check-if-page-is-shop-in-functions-php
 */
add_action( 'template_redirect', 'custom_template_redirect' );
function custom_template_redirect() 
{
    if( is_shop() )
	{
		wp_redirect('https://www.iklvb.com/tickets-properties-for-sale/');
		exit;
	}
}



/**
 * 2018-12-06 Lorenzo M.
 * 
 * source:
 * https://businessbloomer.com/woocommerce-redirect-checkout-add-cart/
 * 
 * @snippet       Redirect to Checkout Upon Add to Cart - WooCommerce
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=21607
 * @author        Rodolfo Melogli
 * @compatible    WC 3.3.3
 */
add_filter( 'woocommerce_add_to_cart_redirect', 'bbloomer_redirect_checkout_add_cart' );
function bbloomer_redirect_checkout_add_cart( $url ) 
{
    $url = get_permalink( get_option( 'woocommerce_checkout_page_id' ) ); 
    return $url;
}
 



/**
 * 2019-11-22 Lorenzo M.
 * 
 * personalizzo la funzione che costruisce l'output delle card/design
 * per inserire personalizzazioni come la data in formato "nice_time"
 * 
 */
function wpestate_build_unit_show_detail($element,$propID,$property_unit_slider,$text,$icon)
{
    $element = strtolower($element);
       
    switch ($element) {
        case 'share':
            $link=get_permalink($propID);
            if ( has_post_thumbnail() ){
                $pinterest = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full_map');
            }
            $protocol = is_ssl() ? 'https' : 'http';
            print '
                <div class="share_unit">
                <a href="'.$protocol.'://www.facebook.com/sharer.php?u='.esc_url($link).'&amp;t='.urlencode(get_the_title($propID)).'" target="_blank" class="social_facebook"></a>
                <a href="'.$protocol.'://twitter.com/home?status='.urlencode(get_the_title($propID).' '.$link).'" class="social_tweet" target="_blank"></a>
                <a href="'.$protocol.'://plus.google.com/share?url='.esc_url($link).'" onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;" target="_blank" class="social_google"></a> 
                <a href="'.$protocol.'://pinterest.com/pin/create/button/?url='.esc_url($link).'&amp;media=';
                    if (isset( $pinterest[0])){ print esc_url($pinterest[0]); }
                    print '&amp;description='.urlencode(get_the_title($propID)).'" target="_blank" class="social_pinterest"></a>
                </div>';
            if($text==''){
                if($icon!=''){
                    if ( strpos($icon, 'fa-') !== false){
                        print '<span class="share_list text_share"  data-original-title="'.esc_html__('share','wpresidence').'" ><i class="fa '.$icon.'" aria-hidden="true"></i></span>';
                    }else{
                        print '<span class="share_list text_share"  data-original-title="'.esc_html__('share','wpresidence').'" ><img src="'.$icon.'" alt="featured_icon"></span>';
                    }
                }else{
                    print '<span class="share_list"  data-original-title="'.esc_html__('share','wpresidence').'" ></span>';
                }
                
            }else{
               print '<span class="share_list text_share"  data-original-title="'.esc_html__('share','wpresidence').'" >'.$text.'</span>';
            }       
               
        break;
        
        
        case 'link_to_page':
         
            $link=get_permalink($propID);        
            if($text==''){
                if ( strpos($icon, 'fa-') !== false){
                    print '<a href="'.$link.'"><i class="fa '.$icon.'" aria-hidden="true"></i></a>';
                }else{
                    print '<a href="'.$link.'"><img src="'.$icon.'" alt="details"></a>';
                }
            }else{
               print '<a href="'.$link.'">'.str_replace('_',' ',$text).'</a>';
               
            }       
               
        break;
       
        case 'favorite':
            $current_user   =   wp_get_current_user();
            $userID         =   $current_user->ID;
            $user_option    =   'favorites'.$userID;
            $favorite_class =   'icon-fav-off';
            $fav_mes        =   esc_html__('add to favorites','wpresidence');
            $user_option    =   'favorites'.$userID;
            $curent_fav     =   get_option($user_option);
            if($curent_fav){
                if ( in_array ($propID,$curent_fav) ){
                    $favorite_class =   'icon-fav-on';   
                    $fav_mes        =   esc_html__('remove from favorites','wpresidence');
                } 
            }
        print '<span class="icon-fav custom_fav '.esc_html($favorite_class).'" data-original-title="'.$fav_mes.'" data-postid="'.intval($propID).'"></span>';
        
        break;
        
               
        case 'compare':
         
          //    
            $compare   = wp_get_attachment_image_src(get_post_thumbnail_id(), 'slider_thumb');           
            if($text==''){
              
                if($icon!=''){
                    if ( strpos($icon, 'fa-') !== false){
                        print '<span class="compare-action text_compare" data-original-title="'.esc_html__('compare','wpresidence').'" data-pimage="';
                        if( isset($compare[0])){print esc_html($compare[0]);} 
                        print '" data-pid="'.intval($propID).'"><i class="fa '.$icon.'" aria-hidden="true"></i></span>';
                       
                    }else{
                        print '<span class="compare-action text_compare" data-original-title="'.esc_html__('compare','wpresidence').'" data-pimage="';
                        if( isset($compare[0])){print esc_html($compare[0]);} 
                        print '" data-pid="'.intval($propID).'"><img src="'.$icon.'" alt="featured_icon"></span>';
                    }
                }else{
                    print '<span class="compare-action" data-original-title="'.esc_html__('compare','wpresidence').'" data-pimage="';
                    if( isset($compare[0])){print esc_html($compare[0]);} 
                    print '" data-pid="'.intval($propID).'"></span>';
                }
                
            }else{
               print '<span class="compare-action text_compare" data-original-title="'.esc_html__('compare','wpresidence').'" data-pimage="';
               if( isset($compare[0])){print esc_html($compare[0]);} 
               print '" data-pid="'.intval($propID).'">'.$text.'</span>';
               
            }       
               
        break;
        
        
         case 'property_status':
            $prop_stat              =   esc_html( get_post_meta($propID, 'property_status', true) );
            if ($prop_stat != 'normal') {
                $ribbon_class = str_replace(' ', '-', $prop_stat);
                if (function_exists('icl_translate') ){
                    $prop_stat     =   icl_translate('wpestate','wp_estate_property_status'.$prop_stat, $prop_stat );
                }
                print stripslashes($prop_stat) ;
            }
        break;
        
        
    
            
        case 'icon':
            if ( strpos($icon, 'fa-') !== false){
                print '<i class="fa '.$icon.'" aria-hidden="true"></i>';
            }else{
                print '<img src="'.$icon.'" alt="featured_icon">';
            }
        break;
        
        
        
        case 'featured_icon':
            if(intval  ( get_post_meta($propID, 'prop_featured', true) )==1){
                
                if($text!=''){
                    print $text;
                }else{
                    if ( strpos($icon, 'fa-') !== false){
                        print '<i class="fa '.$icon.'" aria-hidden="true"></i>';
                    }else{
                        print '<img src="'.$icon.'" alt="featured_icon">';
                    }
                }
                
               
            }
        break;
        
        case 'text':
            if (function_exists('icl_translate') ){
                print stripslashes(str_replace('_',' ',$text));
            }else{
                $meta_value =stripslashes(str_replace('_',' ',$text));
                $meta_value = apply_filters( 'wpml_translate_single_string', $meta_value, 'wpestate', 'wp_estate_custom_unit_'.$meta_value );
                print $meta_value;
            }
        break;
        
        case 'image':
            wpestate_build_unit_show_detail_image($propID,$property_unit_slider);
        break;
    
        case 'description':
            print wpestate_strip_excerpt_by_char(get_the_excerpt(),115,$propID);
        break;
    
        case 'title':
            print '<h4><a href="'.get_permalink($propID).'">'.get_the_title($propID).'</a></h4>';
        break;
    
        case 'property_price':
            $currency                   =   esc_html( get_option('wp_estate_currency_symbol', '') );
            $where_currency             =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
            wpestate_show_price($propID,$currency,$where_currency);
        break;
    
        case 'property_category';
            print get_the_term_list($propID, 'property_category', '', ', ', '') ;
        break;
    
        case 'property_action_category';
            print get_the_term_list($propID, 'property_action_category', '', ', ', '') ;
        break;
        
        case 'property_city';
            print get_the_term_list($propID, 'property_city', '', ', ', '') ;
        break;
        
        case 'property_area';
            print get_the_term_list($propID, 'property_area', '', ', ', '') ;
        break;
        
        case 'property_county_state';
            print  get_the_term_list($propID, 'property_county_state', '', ', ', '') ;
        break;
        
        case 'property_agent';
            $agent_id   = intval( get_post_meta($propID, 'property_agent', true) );
            print '<a href="'.get_permalink($agent_id).'">'.get_the_title($agent_id).'</a>';
        break;
        
        case 'property_agent_picture';
            $agent_id   = intval( get_post_meta($propID, 'property_agent', true) );
            $preview            = wp_get_attachment_image_src(get_post_thumbnail_id($agent_id), 'agent_picture_thumb');
            $preview_img         = $preview[0];
            print '<a href="'.get_permalink($agent_id).'" class="property_unit_custom_agent_face" style="background-image:url('.$preview_img.')"></a>';
        break;
        
        case 'custom_div';
            print '';
        break;
	
        case 'property_size';
            //print wpestate_sizes_no_format ( floatval ( get_post_meta($propID, 'property_size', true) ) );
            print wpestate_get_converted_measure( $propID, 'property_size' ); 
        break;
	
        case 'property-date';
            print nice_time( get_post_meta( $propID, 'property-date', true )); 
        break;
		
        default:
           			
			if (function_exists('icl_translate') ){
                print  get_post_meta($propID, $element, true);
            }else{
                $meta_value = get_post_meta($propID, $element, true);;
                $meta_value = apply_filters( 'wpml_translate_single_string', $meta_value, 'wpestate', 'wp_estate_custom_unit_'.$meta_value );
                print $meta_value;
            }
    }
    
  
}



/**
 * 2019-11-22 Lorenzo M.
 * 
 * visualizzo il tempo testuale trascorso dopo la pubblicazione
 * 
 * @param type $_date_sql
 * @return string
 */
function nice_time($_date_sql) 
{	
	// calcolo la differenza
	$delta = time() - strtotime($_date_sql);
	
	// intervalli di tempo
	if($delta < 60)						return								__('1 minute ago','iklvb-wpresidence');	
	if($delta < (45 * 60))				return floor($delta / 60) .		' '.__('minutes ago','iklvb-wpresidence');	
	if($delta < (120 * 60))				return								__('about 1 hour ago','iklvb-wpresidence');	
	if($delta < (24 * 60 * 60))			return floor($delta / 3600) .	' '.__('hours ago','iklvb-wpresidence');
	if($delta < (48 * 60 * 60))			return								__('1 day ago','iklvb-wpresidence');
	if($delta < (7 * 24 * 60 * 60))		return floor($delta / 86400) .		__('days ago','iklvb-wpresidence');
	if($delta < (2 * 7 * 24 * 60 * 60))	return								__('1 week ago','iklvb-wpresidence');		// minore di 2 settimane
	if($delta < (3 * 7 * 24 * 60 * 60))	return								__('2 weeks ago','iklvb-wpresidence');		// minore di 3 settimane 

	// di default visualizzo la data
	return date_localized($_date_sql);
}


/**
 * 2019-11-21 Lorenzo M.
 * 
 * ottengo la data con mese e giorno testuali con supporto multilingua
 * 
 * @param type $_date_sql
 * @param type $_string_format
 * @return string
 */
function date_localized($_date_sql, $_string_format = 'short')
{
	// init
	$lang = ICL_LANGUAGE_CODE;
	
	// questo funziona sul server hosting
	$lang_localized_values = array('it' => 'it_IT', 'en' => 'en_UK', 'de' => 'germany');
	$str_ore_values = array('it' => 'ore', 'en' => 'at', 'de' => '');
	
	// setta la mappa locale del sistema operativo in base alla lingua scelta per la visualizzazione
	// setlocale(LC_TIME, 'it_IT'); // esempio
	setlocale(LC_TIME, $lang_localized_values[$lang]);

	// divido la stringa della data
	$Y = date('Y', strtotime($_date_sql));
	$m = date('m', strtotime($_date_sql));
	$d = date('j', strtotime($_date_sql));	// d = 01-31  j = 1-31
	$H = date('H', strtotime($_date_sql));
	$i = date('i', strtotime($_date_sql));
	
	// creo un timestamp
	$time = mktime(0,0,0,$m,$d,$Y);
	
	// ottengo i nomi del giorno e del mese
	$day      = ucfirst(utf8_encode(strftime('%A', $time)));
	$month    = ucfirst(utf8_encode(strftime('%B', $time)));

	// per ora è previsto solo questo ordine degli elementi della data
	switch($_string_format)
	{
		case 'short':	$out = $d.' '.$month.' '.$Y; break;
		case 'all'	:	$out = $day.' '.$d.' '.$month.' '.$Y.' - '.$str_ore_values[$lang].' '.$H.':'.$i; break;
		default		:	$out = ''; break;
	}
	
	return $out;
}















/**
 * controllo se una email è valida
 * 
 * ping sul record MX del dominio della mail => attualmente disabilitato, 
 *		crea problemi in sviluppo locale (windows) // Lorenzo 2015-05-18
 * 
 * @param type $_email
 * @return boolean
 */
function is_valid_email($_email) 
{
	$email = trim($_email);
	$email = filter_var($email, FILTER_SANITIZE_EMAIL);
	$email = filter_var($email, FILTER_VALIDATE_EMAIL);
	
	if($email !== $_email) return false;
	
	return true;
}




/**
 * controllo validità partita iva
 * 
 * @param type $pi
 * @return boolean
 */
function is_valid_vat($pi)
{
	// 2018-08-29 Lorenzo M. 
	// al cliente NON serve questo controllo al momento
	return true;
	
	if($pi == '') return false;
	if(strlen($pi) != 11) return false;
	if(preg_match("/^[0-9]+\$/", $pi) != 1) return false;
	
	$s = 0;
	for($i = 0; $i <= 9; $i += 2) $s += ord($pi[$i]) - ord('0');
	for($i = 1; $i <= 9; $i += 2) 
	{
		$c = 2 * (ord($pi[$i]) - ord('0'));
		if($c > 9) $c = $c - 9;
		$s += $c;
	}
	if((10 - $s%10)%10 != ord($pi[10]) - ord('0')) return false;
	return true;
}


/**
 * controllo validità codice fiscale
 * 
 * @param type $cf
 * @return boolean
 */
function is_valid_fiscal_code($cf)
{
	// 2018-08-29 Lorenzo M. 
	// al cliente NON serve questo controllo al momento
	return true;
	
    if($cf == '') return false;
    if(strlen($cf) != 16) return false;
    $cf = strtoupper($cf);
    if(preg_match("/^[A-Z0-9]+\$/", $cf) != 1) return false;

    $s = 0;
    for($i = 1; $i <= 13; $i += 2)
	{
		$c = $cf[$i];
		if('0' <= $c && $c <= '9') $s += ord($c) - ord('0'); else $s += ord($c) - ord('A');
    }
    for($i = 0; $i <= 14; $i += 2)
	{
		$c = $cf[$i];
		switch($c)
		{
			case '0':  $s += 1;  break;
			case '1':  $s += 0;  break;
			case '2':  $s += 5;  break;
			case '3':  $s += 7;  break;
			case '4':  $s += 9;  break;
			case '5':  $s += 13;  break;
			case '6':  $s += 15;  break;
			case '7':  $s += 17;  break;
			case '8':  $s += 19;  break;
			case '9':  $s += 21;  break;
			case 'A':  $s += 1;  break;
			case 'B':  $s += 0;  break;
			case 'C':  $s += 5;  break;
			case 'D':  $s += 7;  break;
			case 'E':  $s += 9;  break;
			case 'F':  $s += 13;  break;
			case 'G':  $s += 15;  break;
			case 'H':  $s += 17;  break;
			case 'I':  $s += 19;  break;
			case 'J':  $s += 21;  break;
			case 'K':  $s += 2;  break;
			case 'L':  $s += 4;  break;
			case 'M':  $s += 18;  break;
			case 'N':  $s += 20;  break;
			case 'O':  $s += 11;  break;
			case 'P':  $s += 3;  break;
			case 'Q':  $s += 6;  break;
			case 'R':  $s += 8;  break;
			case 'S':  $s += 12;  break;
			case 'T':  $s += 14;  break;
			case 'U':  $s += 16;  break;
			case 'V':  $s += 10;  break;
			case 'W':  $s += 22;  break;
			case 'X':  $s += 25;  break;
			case 'Y':  $s += 24;  break;
			case 'Z':  $s += 23;  break;
		}
    }
    if(chr($s%26 + ord('A')) != $cf[15]) return false;
    return true;
}






/**
 * debug veloce di array e classi
 * 
 * @param type $_values
 * @param type $_return
 * @return type
 */
function pre($_values = '', $_return = false) 
{
	if($_return)
	{
		return '<pre class="debug">'.print_r($_values, true).'</pre>';		
	}
	else
	{
		echo '<pre class="debug">';
		print_r($_values);
		echo '</pre>';
	}
}


/**
 * debug ancora più veloce del precedente 
 * 
 * @param type $_values
 * @param type $_return
 */
function prex($_values = '', $_return = false) 
{
	pre($_values, $_return);
	exit;	
}


/**
 * debug con backtrace
 * 
 */
function preb()
{
	prex( debug_backtrace());
}



