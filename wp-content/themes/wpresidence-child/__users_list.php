<?php
// Template Name: Users list
// Wp Estate Pack
if(!function_exists('wpestate_residence_functionality')){
    esc_html_e('This page will not work without WpResidence Core Plugin, Please activate it from the plugins menu!','wpresidence');
    exit();
}

get_header();
wp_suspend_cache_addition(true);
$options=wpestate_page_details($post->ID);
// global $no_listins_per_row;
// $no_listins_per_row       =   intval( get_option('wp_estate_agent_listings_per_row', '') );

$no_listins_per_row = 4;

$col_class=4;
if($options['content_class']=='col-md-12'){
    $col_class=3;
}

if($no_listins_per_row == 3){
    $col_class  =   '6';
    $col_org    =   6;
    if($options['content_class']=='col-md-12'){
        $col_class  =   '4';
        $col_org    =   4;
    }
}else{   
    $col_class  =   '4';
    $col_org    =   4;
    if($options['content_class']=='col-md-12'){
        $col_class  =   '3';
        $col_org    =   3;
    }
}


?>

<div class="row">
    <?php get_template_part('templates/breadcrumbs'); ?>
    <div class=" <?php print esc_html($options['content_class']);?> ">
        <?php get_template_part('templates/ajax_container'); ?>
        <?php 
        while (have_posts()) : the_post(); 
            if ( esc_html (get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
            <h1 class="entry-title"><?php the_title(); ?></h1>
            <?php } ?>
            <div class="single-content"><?php the_content();?></div>
            <?php
        endwhile; 
        ?>                 
        
        <div id="listing_ajax_container_agent"> 
        <?php
		
		$current_page = get_query_var('paged') ? (int) get_query_var('paged') : 1;
		
        $args = array(
			'role'				=> 'subscriber',
			'orderby'			=> 'display_name', 
			'order'				=> 'ASC', 
			'paged'             => $current_page,
			'number'		    => 999,
			'meta_query'			=> array(
				'relation'		=> 'AND',
				array(
					'key'		=> 'user_estate_role',
					'value'		=> '1',
					'compare'	=> '='
				)
			),					
		);

        $user_selection = new WP_User_Query($args);

		// Get the results
		$users = $user_selection->get_results();
		
        $per_row_class='';
        // $no_listins_per_row = get_option('wp_estate_agent_listings_per_row',true);
        if( $no_listins_per_row==4){
            $per_row_class =' agents_4per_row ';
        }
        
        
        //while ($user_selection->have_posts()): 
			//$user_selection->the_post();
		
		foreach($users as $post)
		{
			// print '<div class="col-md-'.$col_org.$per_row_class.' listing_wrapper">';
			get_template_part('templates/user_unit'); 
            // print '</div>';
		}
		
        // endwhile;
		?> 
        </div>
        <?php kriesi_pagination($user_selection->max_num_pages, $range = 2); ?>         
       
    </div><!-- end 9col container-->
    
<?php  include(locate_template('sidebar.php')); 
wp_suspend_cache_addition(false);?>
</div>   
<?php get_footer(); ?>