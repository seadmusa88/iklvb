<?php
// Single Agency
// Wp Estate Pack
get_header();
$options                    =   wpestate_page_details($post->ID);
$show_compare               =   1;
$currency                   =   esc_html( get_option('wp_estate_currency_symbol', '') );
$where_currency             =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
$options['content_class']='col-md-8';
?>

<div class="row">
    <?php get_template_part('templates/breadcrumbs'); ?>
    <div class=" <?php print esc_html($options['content_class']);?> ">
  
        <div id="content_container"> 
        <?php 
        while (have_posts()) : the_post(); 
            $agency_id              = get_the_ID();
            $thumb_id               = get_post_thumbnail_id($post->ID);
            $preview                = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
            $preview_img            = $preview[0];
            $agency_skype           = esc_html( get_post_meta($post->ID, 'developer_skype', true) );
            $agency_phone           = esc_html( get_post_meta($post->ID, 'developer_phone', true) );
            $agency_mobile          = esc_html( get_post_meta($post->ID, 'developer_mobile', true) );
            $agency_email           = is_email( get_post_meta($post->ID, 'developer_email', true) );
            $agency_posit           = esc_html( get_post_meta($post->ID, 'developer_position', true) );
            $agency_facebook        = esc_html( get_post_meta($post->ID, 'developer_facebook', true) );
            $agency_twitter         = esc_html( get_post_meta($post->ID, 'developer_twitter', true) );
            $agency_linkedin        = esc_html( get_post_meta($post->ID, 'developer_linkedin', true) );
            $agency_pinterest       = esc_html( get_post_meta($post->ID, 'developer_pinterest', true) );
            $agency_instagram       = esc_html( get_post_meta($post->ID, 'developer_instagram', true) );
            $agency_urlc            = esc_html( get_post_meta($post->ID, 'developer_website', true) );
            $agency_opening_hours   = esc_html( get_post_meta($post->ID, 'developer_opening_hours', true) );
            $name                   = get_the_title($post->ID);
        ?>
        <?php endwhile; // end of the loop.    ?>
         
       <div class="container_agent">
        <div class="single-content single-agent">    
           
            
            <div class="agency_content_wrapper">
				<div class="row">
					<div class="col-xs-4 agency_content agentpic-wrapper-">
						<div class="agent-listing-img-wrapper" data-link="<?php // print  $link; ?>">
							<div style="background-image:url(<?php print $preview_img;?>);" class="agent-image-cover"></div>
						</div>
					</div>
					<div class="col-xs-8 agency_details-">
						<h1><?php echo $name; ?></h1>
						<div class="agent_position"><?php echo __('Real Estate Developer','iklvb-wpresidence'); ?></div>
					</div>
				</div>
            
				<div class="row">   
					<div class="col-sm-4">
            
					</div>	
					<div class="agent_content- col-sm-8">
						<div class="agency_content">
							<h4 class=""><?php esc_html_e('About Us','wpresidence');?></h4>
							<?php 
							the_content();
							?>
						</div>

					</div>	
                </div>
            
				<?php get_template_part('templates/developer_listings');  ?>
				<?php get_template_part('templates/agency_agents');  ?>               
            
                <?php       
                $wp_estate_show_reviews     =    get_option('wp_estate_show_reviews_block','');         
                if(is_array($wp_estate_show_reviews) && in_array('developer', $wp_estate_show_reviews)){
                        get_template_part('templates/developer_reviews');  
                }
                ?>
    
          
          </div> 
        </div>  
            
                
        
        
        </div> 
    </div><!-- end 9col container-->    
         
  
</div>  
            
<?php  include(locate_template('sidebar.php')); ?>
</div>
        

<?php
get_footer(); 
?>