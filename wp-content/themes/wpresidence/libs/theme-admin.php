<?php
if( !function_exists('wpestate_new_general_set') ):
function wpestate_new_general_set() {  
    

    if( wpestate_show_license_form()==0)return;
   
  
    if($_SERVER['REQUEST_METHOD'] === 'POST'){	

        $allowed_html   =   array();
        
        // cusotm fields
        if( isset( $_POST['add_field_name'] ) ){
            $new_custom=array();  
            foreach( $_POST['add_field_name'] as $key=>$value ){
                $temp_array=array();
                $temp_array[0]=$value;
                $temp_array[1]= wp_kses( $_POST['add_field_label'][sanitize_key($key)] ,$allowed_html);
                $temp_array[2]= wp_kses( $_POST['add_field_type'][sanitize_key($key)] ,$allowed_html);
                $temp_array[3]= wp_kses ( $_POST['add_field_order'][sanitize_key($key)],$allowed_html);
                $temp_array[4]=  ( $_POST['add_dropdown_order'][sanitize_key($key)]);
                $new_custom[]=$temp_array;
            }

          
            usort($new_custom,"wpestate_sorting_function");
            update_option( 'wp_estate_custom_fields', $new_custom );   
        }
       
       // multiple currencies
        if( isset( $_POST['add_curr_name'] ) ){
            foreach( $_POST['add_curr_name'] as $key=>$value ){
                $temp_array=array();
                $temp_array[0]=$value;
                $temp_array[1]= wp_kses( $_POST['add_curr_label'][sanitize_key($key)] ,$allowed_html);
                $temp_array[2]= wp_kses( $_POST['add_curr_value'][sanitize_key($key)] ,$allowed_html);
                $temp_array[3]= wp_kses( $_POST['add_curr_order'][sanitize_key($key)] ,$allowed_html);
                $new_custom_cur[]=$temp_array;
            }
            
            update_option( 'wp_estate_multi_curr', $new_custom_cur );   

       }else{
           
       }

       
       

        if( isset( $_POST['theme_slider'] ) ){
            update_option( 'wp_estate_theme_slider', true);  
        }
        
       
        $permission_array=array(
            'add_field_name',
            'add_field_label',
            'add_field_type',
            'add_field_order',
            'adv_search_how',
            'adv_search_what',
            'adv_search_label',
        );
        
        $tags_array=array(
            'co_address',
            'direct_payment_details',
            'new_user',
            'admin_new_user',
            'purchase_activated',
            'password_reset_request',
            'password_reseted',
            'purchase_activated',
            'approved_listing',
            'new_wire_transfer',
            'admin_new_wire_transfer',
            'admin_expired_listing',
            'matching_submissions',
            'paid_submissions',
            'featured_submission',
            'account_downgraded',
            'membership_cancelled',
            'downgrade_warning',
            'free_listing_expired',
            'new_listing_submission' ,
            'listing_edit',
            'recurring_payment',
            'subject_new_user',
            'subject_admin_new_user',
            'subject_purchase_activated',
            'subject_password_reset_request',
            'subject_password_reseted',
            'subject_purchase_activated',
            'subject_approved_listing',
            'subject_new_wire_transfer',
            'subject_admin_new_wire_transfer',
            'subject_admin_expired_listing',
            'subject_matching_submissions',
            'subject_paid_submissions',
            'subject_featured_submission',
            'subject_account_downgraded',
            'subject_membership_cancelled',
            'subject_downgrade_warning',
            'subject_free_listing_expired',
            'subject_new_listing_submission' ,
            'subject_listing_edit',
            'subject_recurring_payment',
            'header5_info_widget1_text1',
            'header5_info_widget1_text2',
            'header5_info_widget2_text1',
            'header5_info_widget2_text2',
            'header5_info_widget3_text1',
            'header5_info_widget3_text2',
			
			/* new fields */
			'agent_added',
			'account_downgraded',
			'free_listing_expired',
			'membership_activated',
			'agent_update_profile'
			
        );
        
        
        //$variable!='add_field_name'&& $variable!='add_field_label' && $variable!='add_field_type' && $variable!='add_field_order' && $variable!= 'adv_search_how' && $variable!='adv_search_what' && $variable!='adv_search_label'
		
	 
        foreach($_POST as $variable=>$value){

	
		
            if ($variable!='submit'){
                if (!in_array($variable, $permission_array)){
                    $variable   =   sanitize_key($variable);
                    if( in_array($variable, $tags_array) ){
                        $allowed_html_br=array(
                                'br'        => array(),
                                'del'        => array(),
                                'blockquote'        => array(),
								'ul'        => array(
									'style' => array()
								),
								'p'        => array(
									'style' => array()
								),
								'li'        => array(
									'style' => array()
								),
								'ol'        => array(
									'style' => array()
								),
                                'img'        => array(
									'src' => array(),
									'style' => array(),
									'class' => array(),
									'width' => array(),
									'height' => array(), 
									
								),
								'span'        => array(
									'style' => array()
								),
                                'em'        => array(),
                                'strong'    => array(),
                                 'a'         => array(
                                                'href' => array(),
                                                'title' => array() ,
                                                'tel'       =>  array(),
                                                'style'       =>  array(),
                                            ),
                        );
					
                        $postmeta   =   wp_kses($value,$allowed_html_br);
                    }else{
						
                        $postmeta   =   wp_kses($value,$allowed_html);
                    
                    }   
					
			
                    update_option( wpestate_limit64('wp_estate_'.$variable), $postmeta );                
                }else{
					
                    update_option( 'wp_estate_'.$variable, $value );
                }	
            }	
        }
        
        if( isset($_POST['is_custom']) && $_POST['is_custom']== 1 && !isset($_POST['add_field_name']) ){
            update_option( 'wp_estate_custom_fields', '' ); 
        }
        
        if( isset($_POST['is_custom_cur']) && $_POST['is_custom_cur']== 1 && !isset($_POST['add_curr_name']) ){
            update_option( 'wp_estate_multi_curr', '' );
        }
    
        if (isset($_POST['show_save_search'])){
            $allowed_html=array();
            $show_save_search= wp_kses( $_POST['show_save_search'],$allowed_html );
            $search_alert= wp_kses( $_POST['search_alert'],$allowed_html );
            wp_estate_schedule_email_events( $show_save_search,$search_alert);
          
        }
        
       
        if ( isset( $_POST['paid_submission']) ){
            if ( $_POST['paid_submission']=='membership'){
                wp_estate_schedule_user_check();  
            }else{
                wp_clear_scheduled_hook('wpestate_check_for_users_event');
            }
        }
        
        if ( isset($_POST['auto_curency']) ){
            if( $_POST['auto_curency']=='yes' ){
                wp_estate_enable_load_exchange();
            }else{
                wp_clear_scheduled_hook('wpestate_load_exchange_action');
            }
        }
        
        if(isset($_POST['url_rewrites'])){
            flush_rewrite_rules();
        }
        
 
        if ( isset( $_POST['is_submit_page'] ) && $_POST['is_submit_page']== 1 ){
            
            if( !isset($_POST['mandatory_page_fields'])){
                update_option('wp_estate_mandatory_page_fields','');
            } 
            if( !isset($_POST['submission_page_fields'])){
                update_option('wp_estate_submission_page_fields','');
            }             
            
        }

       
        
}
    

    
$allowed_html   =   array();  
$active_tab = isset( $_GET[ 'tab' ] ) ? wp_kses( $_GET[ 'tab' ],$allowed_html ) : 'general_settings';  


print '<div class="wrap">';
    if(!function_exists('wpestate_residence_functionality')){
        print '<div class="wpestate_error">';
            esc_html_e('This page will not work without WpResidence Core Plugin, Please activate it from the plugins menu!','wpresidence');
        print'</div>';
    }

    print '<div class="wrap-topbar">';
        
        $hidden_tab='none';
        if(isset($_POST['hidden_tab'])) {
            $hidden_tab= esc_attr( $_POST['hidden_tab'] );
        }
        
        $hidden_sidebar='none';
        if(isset($_POST['hidden_sidebar'])) {
            $hidden_sidebar= esc_attr( $_POST['hidden_sidebar'] );
        }
        
        print '<input type="hidden" id="hidden_tab" name="hidden_tab" value="'.$hidden_tab.'">';        
        print '<input type="hidden" id="hidden_sidebar"  name="hidden_sidebar" value="'.$hidden_sidebar.'">';
        
        print   '<div id="general_settings" data-menu="general_settings_sidebar" class="admin_top_bar_button"> 
                    <img src="'.get_template_directory_uri().'/img/admin/general.png'.'" alt="general settings">'.esc_html__('General','wpresidence').'
                </div>';
        
        print   '<div id="social_contact" data-menu="social_contact_sidebar" class="admin_top_bar_button"> 
                    <img src="'.get_template_directory_uri().'/img/admin/contact.png'.'" alt="general settings">'.esc_html__('Social & Contact','wpresidence').'
                </div>';
        
        print   '<div id="map_settings" data-menu="map_settings_sidebar" class="admin_top_bar_button"> 
                    <img src="'.get_template_directory_uri().'/img/admin/map.png'.'" alt="general settings">'.esc_html__('Map','wpresidence').'
                </div>';
         
        print   '<div id="design_settings" data-menu="design_settings_sidebar" class="admin_top_bar_button"> 
                    <img src="'.get_template_directory_uri().'/img/admin/design.png'.'" alt="general settings">'.esc_html__('Design','wpresidence').'
                </div>';
        
        print   '<div id="advanced_settings" data-menu="advanced_settings_sidebar" class="admin_top_bar_button"> 
                    <img src="'.get_template_directory_uri().'/img/admin/advanced.png'.'" alt="general settings">'.esc_html__('Advanced','wpresidence').'
                </div>';
            
        print   '<div id="membership_settings" data-menu="membership_settings_sidebar"  class="admin_top_bar_button"> 
                    <img src="'.get_template_directory_uri().'/img/admin/membership.png'.'" alt="general settings">'.esc_html__('Membership','wpresidence').'
                </div>';
        
        print   '<div id="advanced_search_settings" data-menu="advanced_search_settings_sidebar"  class="admin_top_bar_button"> 
                    <img src="'.get_template_directory_uri().'/img/admin/search.png'.'" alt="general settings">'.esc_html__('Search','wpresidence').'
                </div>';
        
        print   '<div id="help_custom" data-menu="help_custom_sidebar"  class="admin_top_bar_button"> 
                    <img src="'.get_template_directory_uri().'/img/admin/help.png'.'" alt="general settings">'.esc_html__('Help & Custom','wpresidence').'
                </div>';
        
        print '<div class="theme_details">'. wp_get_theme().'</div>';
        
    print '</div>';


    print '
    <div id="wpestate_sidebar_menu">
        <div id="general_settings_sidebar" class="theme_options_sidebar">
            <ul>
                <li data-optiontab="global_settings_tab" class="selected_option">'.esc_html__('Global Theme Settings','wpresidence').'</li>
                <li data-optiontab="user_role_options_tab"   class="">'.esc_html__('User Role Settings','wpresidence').'</li>
                <li data-optiontab="appearance_options_tab"   class="">'.esc_html__('Appearance','wpresidence').'</li>
                <li data-optiontab="logos_favicon_tab"   class="">'.esc_html__('Logos & Favicon','wpresidence').'</li>
                <li data-optiontab="header_settings_tab"   class="">'.esc_html__('Header','wpresidence').'</li>
                <li data-optiontab="footer_settings_tab"   class="">'.esc_html__('Footer','wpresidence').'</li>
                <li data-optiontab="price_curency_tab"   class="">'.esc_html__('Price & Currency','wpresidence').'</li>
                <li data-optiontab="custom_fields_tab"   class="">'.esc_html__('Custom Fields','wpresidence').'</li>
                <li data-optiontab="ammenities_features_tab"   class="">'.esc_html__('Features & Amenities','wpresidence').'</li>
                <li data-optiontab="listing_labels_tab"   class="">'.esc_html__('Listings Labels','wpresidence').'</li>   
                <li data-optiontab="theme_slider_tab"   class="">'.esc_html__('Theme Slider','wpresidence').'</li>   
                <li data-optiontab="property_rewrite_page_tab" class="">'.esc_html__('Edit Property, Agent, Agency and Developer text links','wpresidence').'</li>
                <li data-optiontab="splash_page_page_tab" class="">'.esc_html__('Splash Page','wpresidence').'</li>  


            </ul>
        </div>
        
        <div id="social_contact_sidebar" class="theme_options_sidebar" style="display:none;">
            <ul>
                <li data-optiontab="contact_details_tab" class="">'.esc_html__('Contact Page Details','wpresidence').'</li>
                <li data-optiontab="social_accounts_tab" class="">'.esc_html__('Social Accounts','wpresidence').'</li>
                <li data-optiontab="social_login_tab" class="">'.esc_html__('Social Login','wpresidence').'</li>
                <li data-optiontab="contact7_tab" class="">'.esc_html__('Contact 7 Settings','wpresidence').'</li>
                <li data-optiontab="twitter_widget_tab" class="">'.esc_html__('Twitter Widget','wpresidence').'</li>
            </ul>
        </div>
        

        <div id="map_settings_sidebar" class="theme_options_sidebar" style="display:none;">
            <ul>
                <li data-optiontab="general_map_tab" class="">'.esc_html__('Map Settings','wpresidence').'</li>
                <li data-optiontab="pin_management_tab" class="">'.esc_html__('Pins Management','wpresidence').'</li>
                <li data-optiontab="generare_pins_tab" class="">'.esc_html__('Generate Pins','wpresidence').'</li>
            </ul>
        </div>
        

        <div id="design_settings_sidebar" class="theme_options_sidebar" style="display:none;">
            <ul>
                <li data-optiontab="general_design_settings_tab" class="">'.esc_html__('General Design Settings','wpresidence').'</li>
                <li data-optiontab="property_page_tab" class="">'.esc_html__('Property Page','wpresidence').'</li>
                <li data-optiontab="print_page_tab" class="">'.esc_html__('Property Print Page Design','wpresidence').'</li>
                <li data-optiontab="custom_colors_tab" class="">'.esc_html__('Custom Colors','wpresidence').'</li>
                <li data-optiontab="mainmenu_design_elements_tab" class="">'.esc_html__('Header Design & Colors','wpresidence').'</li>
                <li data-optiontab="mobile_design_elements_tab" class="">'.esc_html__('Mobile Menu Colors','wpresidence').'</li>
                <li data-optiontab="property_list_design_tab" class="">'.esc_html__('Property, Agent, Blog Lists Design','wpresidence').'</li>
                <li data-optiontab="widget_design_elements_tab" class="">'.esc_html__('Sidebar Widget Design','wpresidence').'</li>
                <li data-optiontab="wpestate_user_dashboard_design_tab" class="">'.esc_html__('User Dashboard Design','wpresidence').'</li>
                <li data-optiontab="custom_fonts_tab" class="">'.esc_html__('Fonts','wpresidence').'</li>
                <li data-optiontab="custom_css_tab" class="">' . esc_html__('Custom CSS', 'wpresidence') . '</li>
                <li data-optiontab="property_page__design_tab" class="">'.esc_html__('Property Unit/Card Design - BETA','wpresidence').'</li>
            </ul>
        </div>
        
        <div id="advanced_search_settings_sidebar" class="theme_options_sidebar" style="display:none;">
            <ul>
                <li data-optiontab="advanced_search_settings_tab" class="">'.esc_html__('Advanced Search Settings','wpresidence').'</li>
                <li data-optiontab="advanced_search_form_tab" class="">'.esc_html__('Advanced Search Form','wpresidence').'</li>
                <li data-optiontab="geo_location_search_tab" class="">'.esc_html__('Geo Location Search','wpresidence').'</li>
                <li data-optiontab="save_search_tab" class="">'.esc_html__('Save Search Settings ','wpresidence').'</li>
                <li data-optiontab="advanced_search_colors_tab" class="">'.esc_html__('Advanced Search Colors','wpresidence').'</li>
            </ul>
        </div>
        
        <div id="membership_settings_sidebar" class="theme_options_sidebar" style="display:none;">
            <ul>
                <li data-optiontab="membership_settings_tab" class="">'.esc_html__('Membership Settings','wpresidence').'</li>
                <li data-optiontab="property_submission_page_tab" class="">'.esc_html__('Property Submission Page','wpresidence').'</li>
                <li data-optiontab="paypal_settings_tab" class="">'.esc_html__('Paypal Settings','wpresidence').'</li>
                <li data-optiontab="stripe_settings_tab" class="">'.esc_html__('Stripe Settings','wpresidence').'</li>
            </ul>
        </div>

 
        <div id="advanced_settings_sidebar" class="theme_options_sidebar" style="display:none;">
             <ul>
                <li data-optiontab="email_management_tab" class="selected_option">'.esc_html__('Email Management','wpresidence').'</li>
                <li data-optiontab="speed_management_tab" class="selected_option">'.esc_html__('Site Speed','wpresidence').'</li>
                <li data-optiontab="export_settings_tab" class="selected_option">'.esc_html__('Export Options','wpresidence').'</li>
                <li data-optiontab="import_settings_tab" class="selected_option">'.esc_html__('Import Options','wpresidence').'</li>
                <li data-optiontab="recaptcha_tab" class="selected_option">'.esc_html__('reCaptcha settings','wpresidence').'</li>
                <li data-optiontab="yelp_tab" class="selected_option">'.esc_html__('Yelp settings','wpresidence').'</li>
                <li data-optiontab="optima_express_tab" class="selected_option">'.esc_html__('Optima Express  settings','wpresidence').'</li>
            </ul>
        </div>
        
        <div id="help_custom_sidebar" class="theme_options_sidebar" style="display:none;">
             <ul>
                <li data-optiontab="help_custom_tab" class="selected_option">'.esc_html__('Help & Custom','wpresidence').'</li>
            </ul>
        </div>
    </div>';
    
    

    print ' <div id="wpestate_wrapper_admin_menu"> 
                <div id="general_settings_sidebar_tab" class="theme_options_wrapper_tab">
                    <form method="post" action="" >
                        <div id="global_settings_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('General Settings','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';    
                            new_wpestate_theme_admin_general_settings();
                        print '        
                        </div>
                    </form>
                    
                    <form method="post" action="" >
                        <div id="user_role_options_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('User Role Settings','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';    
                            wpestate_theme_admin_user_role_settings();
                        print '        
                        </div>
                    </form>
                    

                    <form method="post" action="">
                    <div id="appearance_options_tab" class="theme_options_tab" style="display:none;">
                        <h1>'.esc_html__('Appearance','wpresidence').'</h1>
                        <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                        <div class="theme_option_separator"></div>';
                        new_wpestate_appeareance();
                    print '        
                    </div>
                    </form>

                    <form method="post" action="">
                    <div id="logos_favicon_tab" class="theme_options_tab" style="display:none;">
                        <h1>'.esc_html__('Logos & Favicon','wpresidence').'</h1>
                        <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                        <div class="theme_option_separator"></div>';
                        new_wpestate_theme_admin_logos_favicon();
                    print '        
                    </div>
                    </form>

                    <form method="post" action="">
                    <div id="header_settings_tab" class="theme_options_tab" style="display:none;">
                        <h1>'.esc_html__('Header Settings','wpresidence').'</h1>
                        <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                        <div class="theme_option_separator"></div>';
                        new_wpestate_header_settings();
                    print '        
                    </div>
                    </form>

                    <form method="post" action="">
                    <div id="footer_settings_tab" class="theme_options_tab" style="display:none;">
                        <h1>'.esc_html__('Footer Settings','wpresidence').'</h1>
                        <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                        <div class="theme_option_separator"></div>';
                        new_wpestate_footer_settings();
                    print '        
                    </div>
                    </form>

                    <form method="post" action="">
                    <div id="price_curency_tab" class="theme_options_tab" style="display:none;">
                        <h1>'.esc_html__('Price & Currency','wpresidence').'</h1>
                        <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                        <div class="theme_option_separator"></div>';
                        new_wpestate_price_currency();
                    print '        
                    </div>
                    </form>

                    <form method="post" action="">
                    <div id="custom_fields_tab" class="theme_options_tab" style="display:none;">
                        <h1>'.esc_html__('Custom Fields','wpresidence').'</h1>
                        <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                        <div class="theme_option_separator"></div>';
                        new_wpestate_custom_fields();
                    print '        
                    </div>
                    </form>
                    
                    <form method="post" action="">
                    <div id="ammenities_features_tab" class="theme_options_tab" style="display:none;">
                        <h1>'.esc_html__('Features & Amenities','wpresidence').'</h1>
                        <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                        <div class="theme_option_separator"></div>';
                        new_wpestate_ammenities_features();
                    print '        
                    </div>
                    </form>

                    <form method="post" action="">
                    <div id="listing_labels_tab" class="theme_options_tab" style="display:none;">
                        <h1>'.esc_html__('Listings Labels','wpresidence').'</h1>
                        <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                        <div class="theme_option_separator"></div>';
                        new_wpestate_listing_labels();
                    print '        
                    </div>
                    </form>
                    

                    <form method="post" action="">
                    <div id="property_rewrite_page_tab" class="theme_options_tab" style="display:none;">
                        <h1>'.esc_html__('Property and Agent Links','wpresidence').'</h1>
                        <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                        <div class="theme_option_separator"></div>';
                        new_wpestate_property_links();
                    print '        
                    </div>
                    </form>
                    

                    <form method="post" action="">
                    <div id="splash_page_page_tab" class="theme_options_tab" style="display:none;">
                        <h1>'.esc_html__('Splash Page','wpresidence').'</h1>
                        <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                        <div class="theme_option_separator"></div>';
                        wpestate_splash_page();
                    print '        
                    </div>
                    </form>





                    <form method="post" action="">
                    <div id="theme_slider_tab" class="theme_options_tab" style="display:none;">
                        <h1>'.esc_html__('Theme Slider ','wpresidence').'</h1>
                        <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                        <div class="theme_option_separator"></div>';
                        new_wpestate_theme_slider();
                    print '        
                    </div>
                    </form>
 
                </div>
                
                <div id="social_contact_sidebar_tab" class="theme_options_wrapper_tab" style="display:none">
                        <form method="post" action="">
                        <div id="contact_details_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Contact Page Details','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_theme_contact_details();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="social_accounts_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Social Accounts ','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_theme_social_accounts();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="social_login_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Social Login ','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_theme_social_login();
                        print '        
                        </div>
                        </form>

                        <form method="post" action="">
                        <div id="contact7_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Contact 7 Settings','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_contact7();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="twitter_widget_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Twitter Widget','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_theme_twitter_widget();
                        print '        
                        </div>
                        </form>
                        
                </div> 




                <div id="map_settings_sidebar_tab" class="theme_options_wrapper_tab" style="display:none">
                        <form method="post" action="">
                        <div id="general_map_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Map  Settings','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_map_details();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="pin_management_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Pin Management','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_pin_management();
                        print '        
                        </div>
                        </form>

                        <form method="post" action="">
                        <div id="generare_pins_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Generate Pins','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_generate_pins();
                        print '        
                        </div>
                        </form>
                </div> 




                
                <div id="design_settings_sidebar_tab" class="theme_options_wrapper_tab" style="display:none">
  
                        <form method="post" action="">
                        <div id="general_design_settings_tab" class="theme_options_tab" style="display:none;" >
                            <h1>'.esc_html__('General Design Settings','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            wpestate_general_design_settings();
                        print '        
                        </div>
                        </form>

                        <form method="post" action="">
                        <div id="property_page_tab" class="theme_options_tab" style="display:none;" >
                            <h1>'.esc_html__('Property Page Settings','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_property_page_details();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="print_page_tab" class="theme_options_tab" style="display:none;" >
                            <h1>'.esc_html__('Print Page Design','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            wpestate_print_page_design();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="custom_colors_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Custom Colors Settings','wpresidence').'</h1>
                            <span class="header_explanation">'.esc_html__('***Please understand that we cannot add here color controls for all theme elements & details. Doing that will result in a overcrowded and useless interface. These small details need to be addressed via custom css code','wpresidence').'</span>    
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_custom_colors();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="mainmenu_design_elements_tab" class="theme_options_tab" style="display:none;" >
                            <h1>'.esc_html__('Header Design & Colors','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_main_menu_design();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="mobile_design_elements_tab" class="theme_options_tab" style="display:none;" >
                            <h1>'.esc_html__('Mobile Menu Colors','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_mobile_menu_design();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="property_list_design_tab" class="theme_options_tab" style="display:none;" >
                            <h1>'.esc_html__('Property, Agent, Blog Lists Design','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_property_list_design_details();
                        print '        
                        </div>
                        </form>
                        

                        <form method="post" action="">
                        <div id="widget_design_elements_tab" class="theme_options_tab" style="display:none;" >
                            <h1>'.esc_html__('Sidebar Widget Tab','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_widget_design_elements_details();
                        print '        
                        </div>
                        </form>                        

                        <form method="post" action="">
                        <div id="wpestate_user_dashboard_design_tab" class="theme_options_tab" style="display:none;" >
                            <h1>' . esc_html__('User Dashboard Design', 'wpresidence') . '</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="' . esc_html__('Save Changes', 'wpresidence') . '" />
                            <div class="theme_option_separator"></div>';
                            wpestate_user_dashboard_design();
                        print '        
                        </div>
                        </form>
                        
  
                        <form method="post" action="">
                        <div id="custom_fonts_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Custom Fonts','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_custom_fonts();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="custom_css_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Custom CSS','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_custom_css();
                        print '        
                        </div>
                        </form>
                                                
                        <form method="post" action="">
                        <div id="property_page__design_tab" class="theme_options_tab" style="display:none;" >
                            <h1>'.esc_html__('Property Card Design','wpresidence').'</h1>
                            <div class="theme_option_separator"></div>';
                            new_wpestate_property_page_design_details();
                        print '        
                        </div>
                        </form>                     
                     

                </div> 
                
                <div id="advanced_search_settings_sidebar_tab" class="theme_options_wrapper_tab"  style="display:none">
                        <form method="post" action="">
                        <div id="advanced_search_settings_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Advanced Search Settings','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_advanced_search_settings();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="advanced_search_form_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Advanced Search Form','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_advanced_search_form();
                        print '        
                        </div>
                        </form>
                        
  
                        <form method="post" action="">
                        <div id="geo_location_search_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Geo Location Search','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            wpestate_geo_location_tab();
                        print '        
                        </div>
                        </form>

                        <form method="post" action="">
                        <div id="save_search_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Save Search Settings','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            wpestate_save_search_tab();
                        print '        
                        </div>
                        </form>

                        <form method="post" action="">
                        <div id="advanced_search_colors_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Advanced Search Colors','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            wpestate_search_colors_tab();
                        print '        
                        </div>
                        </form>

                       
                </div> 

              
                <div id="membership_settings_sidebar_tab" class="theme_options_wrapper_tab" style="display:none">
                        <form method="post" action="">
                        <div id="membership_settings_tab" class="theme_options_tab"  style="display:none;">
                            <h1>'.esc_html__('Membership Settings','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_membership_settings();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="paypal_settings_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('PaypPal Settings','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_paypal_settings();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="stripe_settings_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Stripe Settings','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_stripe_settings();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="property_submission_page_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Property Submission Page','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_property_submission_tab();
                        print '        
                        </div>
                        </form>





                </div> 
                
             

                <div id="advanced_settings_sidebar_tab" class="theme_options_wrapper_tab" style="display:none">
                
                        <form method="post" action="">
                        <div id="email_management_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Email Management','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_email_management();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="speed_management_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Site Speed','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_site_speed();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="export_settings_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Export Options','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_export_settings();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="import_settings_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Import Options','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            new_wpestate_import_options_tab();
                        print '        
                        </div>
                        </form>

                        
                        <form method="post" action="">
                        <div id="recaptcha_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('reCaptcha settings','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            estate_recaptcha_settings();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="yelp_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Yelp settings','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            estate_yelp_settings();
                        print '        
                        </div>
                        </form>
                        
                        <form method="post" action="">
                        <div id="optima_express_tab" class="theme_options_tab" style="display:none;">
                            <h1>'.esc_html__('Optima Express settings','wpresidence').'</h1>
                            <input type="submit" name="submit"  class="new_admin_submit new_admin_submit_right" value="'.esc_html__('Save Changes','wpresidence').'" />
                            <div class="theme_option_separator"></div>';
                            optima_express_settings();
                        print '        
                        </div>
                        </form>
                        


     
                </div> 


                <div id="help_custom_sidebar_tab" class="theme_options_wrapper_tab">
                    <form method="post" action="">
                    <div id="help_custom_tab" class="theme_options_tab" style="display:none;">
                        <h1>'.esc_html__('Help&Custom','wpresidence').'</h1>
                        <div class="theme_option_separator"></div>';
                        new_wpestate_help_custom();
                    print '        
                    </div>
                    </form>
                </div>


           </div>';

print '</div>';
   
}
endif; // end   wpestate_new_general_set  


if( !function_exists('wpestate_generate_file_pins') ):
function   wpestate_generate_file_pins(){
    print '<div class="wpestate-tab-container">';
    print '<h1 class="wpestate-tabh1">'.esc_html__('Generate pins','wpresidence').'</h1>';
     print '<a href="http://help.wpresidence.net/article/google-maps-settings-how-read-from-file-works/" target="_blank" class="help_link">'.esc_html__('help','wpresidence').'</a>';
  
    print '<table class="form-table">   <tr valign="top">
           <td>';  
          
  
    
    print '</td>
           </tr></table>';
    print '</div>';   
}
endif;


if( !function_exists('wpestate_show_advanced_search_options') ):

function  wpestate_show_advanced_search_options($i,$adv_search_what){
    $return_string='';

    $curent_value='';
    if(isset($adv_search_what[$i])){
        $curent_value=$adv_search_what[$i];        
    }
    
   // $curent_value=$adv_search_what[$i];
    $admin_submission_array=array('types',
                                  'categories',
                                  'county / state',
                                  'cities',
                                  'areas',
                                  'property price',
                                  'property size',
                                  'property lot size',
                                  'property rooms',
                                  'property bedrooms',
                                  'property bathrooms',
                                  'property address',                               
                                  'property zip',
                                  'property country',
                                  'property status',
                                  'property id',
                                  'keyword'
                                );
    
    foreach($admin_submission_array as $value){

        $return_string.='<option value="'.$value.'" '; 
        if($curent_value==$value){
             $return_string.= ' selected="selected" ';
        }
        $return_string.= '>'.$value.'</option>';    
    }
    
    $i=0;
    $custom_fields = get_option( 'wp_estate_custom_fields', true); 
    if( !empty($custom_fields)){  
        while($i< count($custom_fields) ){          
            $name =   $custom_fields[$i][0];
            $type =   $custom_fields[$i][1];
            $slug =   str_replace(' ','-',$name);

            $return_string.='<option value="'.$slug.'" '; 
            if($curent_value==$slug){
               $return_string.= ' selected="selected" ';
            }
            $return_string.= '>'.$name.'</option>';    
            $i++;  
        }
    }  
    $slug='none';
    $name='none';
    $return_string.='<option value="'.$slug.'" '; 
    if($curent_value==$slug){
        $return_string.= ' selected="selected" ';
    }
    $return_string.= '>'.$name.'</option>';    

       
    return $return_string;
}
endif; // end   wpestate_show_advanced_search_options  



if( !function_exists('wpestate_show_advanced_search_how') ):
function  wpestate_show_advanced_search_how($i,$adv_search_how){
    $return_string='';
    $curent_value='';
    if (isset($adv_search_how[$i])){
         $curent_value=$adv_search_how[$i];
    }
   
    
    
    $admin_submission_how_array=array('equal',
                                      'greater',
                                      'smaller',
                                      'like',
                                      'date bigger',
                                      'date smaller');
    
    foreach($admin_submission_how_array as $value){
        $return_string.='<option value="'.$value.'" '; 
        if($curent_value==$value){
             $return_string.= ' selected="selected" ';
        }
        $return_string.= '>'.$value.'</option>';    
    }
    return $return_string;
}
endif; // end   wpestate_show_advanced_search_how  





if( !function_exists('wpestate_dropdowns_theme_admin') ):
    function wpestate_dropdowns_theme_admin($array_values,$option_name,$pre=''){
        
        $dropdown_return    =   '';
        $option_value       =   esc_html ( get_option('wp_estate_'.$option_name,'') );
        foreach($array_values as $value){
            $dropdown_return.='<option value="'.$value.'"';
              if ( $option_value == $value ){
                $dropdown_return.='selected="selected"';
            }
            $dropdown_return.='>'.$pre.$value.'</option>';
        }
        
        return $dropdown_return;
        
    }
endif;




if( !function_exists('wpestate_dropdowns_theme_admin_with_key') ):
    function wpestate_dropdowns_theme_admin_with_key($array_values,$option_name){
        
        $dropdown_return    =   '';
        $option_value       =   esc_html ( get_option('wp_estate_'.$option_name,'') );
        foreach($array_values as $key=>$value){
            $dropdown_return.='<option value="'.$key.'"';
              if ( $option_value == $key ){
                $dropdown_return.='selected="selected"';
            }
            $dropdown_return.='>'.$value.'</option>';
        }
        
        return $dropdown_return;
        
    }
endif;






/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///  Membership Settings
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if( !function_exists('wpestate_theme_admin_membershipsettings') ):
function wpestate_theme_admin_membershipsettings(){
    $price_submission               =   floatval( get_option('wp_estate_price_submission','') );
    $price_featured_submission      =   floatval( get_option('wp_estate_price_featured_submission','') );    
    $paypal_client_id               =   esc_html( get_option('wp_estate_paypal_client_id','') );
    $paypal_client_secret           =   esc_html( get_option('wp_estate_paypal_client_secret','') );
    $paypal_api_username            =   esc_html( get_option('wp_estate_paypal_api_username','') );
    $paypal_api_password            =   esc_html( get_option('wp_estate_paypal_api_password','') );
    $paypal_api_signature           =   esc_html( get_option('wp_estate_paypal_api_signature','') );
    $paypal_rec_email               =   esc_html( get_option('wp_estate_paypal_rec_email','') );
    $free_feat_list                 =   esc_html( get_option('wp_estate_free_feat_list','') );
    $free_mem_list                  =   esc_html( get_option('wp_estate_free_mem_list','') );
    $cache_array                    =   array('yes','no');  
    $stripe_secret_key              =   esc_html( get_option('wp_estate_stripe_secret_key','') );
    $stripe_publishable_key         =   esc_html( get_option('wp_estate_stripe_publishable_key','') );
    
    $args=array(
        'a' => array(
            'href' => array(),
            'title' => array()
        ),
        'br' => array(),
        'em' => array(),
        'strong' => array(),
    );
     $direct_payment_details         =   wp_kses( get_option('wp_estate_direct_payment_details','') ,$args);
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    $free_mem_list_unl='';
    if ( intval( get_option('wp_estate_free_mem_list_unl', '' ) ) == 1){
        $free_mem_list_unl=' checked="checked" ';  
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    $paypal_array                   =   array( 'sandbox','live' );
    $paypal_api_select              =   wpestate_dropdowns_theme_admin($paypal_array,'paypal_api');

    $submission_curency_array       =   array(get_option('wp_estate_submission_curency_custom',''),'USD','EUR','AUD','BRL','CAD','CZK','DKK','HKD','HUF','ILS','INR','JPY','MYR','MXN','NOK','NZD','PHP','PLN','GBP','SGD','SEK','CHF','TWD','THB','TRY');
    $submission_curency_symbol      =   wpestate_dropdowns_theme_admin($submission_curency_array,'submission_curency');
    
    $paypal_array                   =   array('no','per listing','membership');
    $paid_submission_symbol         =   wpestate_dropdowns_theme_admin($paypal_array,'paid_submission');
    $admin_submission_symbol        =   wpestate_dropdowns_theme_admin($cache_array,'admin_submission');
    $user_agent_symbol              =   wpestate_dropdowns_theme_admin($cache_array,'user_agent');
    $enable_paypal_symbol           =   wpestate_dropdowns_theme_admin($cache_array,'enable_paypal');
    $enable_stripe_symbol           =   wpestate_dropdowns_theme_admin($cache_array,'enable_stripe');
    $enable_direct_pay_symbol       =   wpestate_dropdowns_theme_admin($cache_array,'enable_direct_pay');
   
    
    
    $free_feat_list_expiration= intval ( get_option('wp_estate_free_feat_list_expiration','') );
    
    print '<div class="wpestate-tab-container">';
    print '<h1 class="wpestate-tabh1">'.esc_html__('Membership & Payment Settings','wpresidence').'</h1>';  
    print '<a href="http://help.wpresidence.net/article/free-submission/" target="_blank" class="help_link">'.esc_html__('help','wpresidence').'</a>';
  
    print '
        <table class="form-table">
        
        <tr valign="top">
            <th scope="row"><label for="admin_submission">'.esc_html__('Submited Listings should be approved by admin?','wpresidence').'</label></th>
           
            <td> <select id="admin_submission" name="admin_submission">
                    '.$admin_submission_symbol.'
		 </select>
            </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="user_agent">'.esc_html__('Front end registred users should be saved as agents?','wpresidence').'</label></th>
           
            <td> <select id="user_agent" name="user_agent">
                    '.$user_agent_symbol.'
		 </select>
            </td>
        </tr>
        

         <tr valign="top">
            <th scope="row"><label for="paid_submission">'.esc_html__('Enable Paid Submission ?','wpresidence').'</label></th>
           
            <td> <select id="paid_submission" name="paid_submission">
                    '.$paid_submission_symbol.'
		 </select>
            </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="enable_paypal">'.esc_html__('Enable Paypal?','wpresidence').'</label></th>
           
            <td> <select id="enable_paypal" name="enable_paypal">
                    '.$enable_paypal_symbol.'
		 </select>
            </td>
        </tr>

     
        
        <tr valign="top">
            <th scope="row"><label for="enable_stripe">'.esc_html__('Enable Stripe?','wpresidence').'</label></th>
           
            <td> <select id="enable_stripe" name="enable_stripe">
                    '.$enable_stripe_symbol.'
		 </select>
            </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="enable_direct_pay">'.esc_html__('Enable Direct Payment / Wire Payment?','wpresidence').'</label></th>
           
            <td> <select id="enable_direct_pay" name="enable_direct_pay">
                    '.$enable_direct_pay_symbol.'
		 </select>
            </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="submission_curency">'.esc_html__('Currency For Paid Submission','wpresidence').'</label></th>
            <td>
                <select id="submission_curency" name="submission_curency">
                    '.$submission_curency_symbol.'
                </select> 
            </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="submission_curency_custom">'.esc_html__('Custom Currency Symbol - *select it from the list above after you add it.','wpresidence').'</label></th>
            <td>
               <input type="text" id="submission_curency_custom" name="submission_curency_custom" class="regular-text"  value="'.get_option('wp_estate_submission_curency_custom','').'"/>
            </td>
        </tr>

         <tr valign="top">
            <th scope="row"><label for="paypal_client_id">'.esc_html__('Paypal Client id','wpresidence').'</label></th>
            <td><input  type="text" id="paypal_client_id" name="paypal_client_id" class="regular-text"  value="'.$paypal_client_id.'"/> </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="paypal_client_secret ">'.esc_html__('Paypal Client Secret Key ','wpresidence').'</label></th>
            <td><input  type="text" id="paypal_client_secret" name="paypal_client_secret"  class="regular-text" value="'.$paypal_client_secret.'"/> </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="paypal_api">'.esc_html__('Paypal & Stripe Api ','wpresidence').'</label></th>
            <td>
              <select id="paypal_api" name="paypal_api">
                    '.$paypal_api_select.'
                </select>
            </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="paypal_api_username">'.esc_html__('Paypal Api User Name ','wpresidence').'</label></th>
            <td><input  type="text" id="paypal_api_username" name="paypal_api_username"  class="regular-text" value="'.$paypal_api_username.'"/> </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="paypal_api_password ">'.esc_html__('Paypal API Password ','wpresidence').'</label></th>
            <td><input  type="text" id="paypal_api_password" name="paypal_api_password"  class="regular-text" value="'.$paypal_api_password.'"/> </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="paypal_api_signature">'.esc_html__('Paypal API Signature','wpresidence').'</label></th>
            <td><input  type="text" id="paypal_api_signature" name="paypal_api_signature"  class="regular-text" value="'.$paypal_api_signature.'"/> </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="paypal_rec_email">'.esc_html__('Paypal receiving email','wpresidence').'</label></th>
            <td><input  type="text" id="paypal_rec_email" name="paypal_rec_email"  class="regular-text" value="'.$paypal_rec_email.'"/> </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="stripe_secret_key">'.esc_html__('Stripe Secret Key','wpresidence').'</label></th>
            <td><input  type="text" id="stripe_secret_key" name="stripe_secret_key"  class="regular-text" value="'.$stripe_secret_key.'"/> </td>
        </tr>
       
        <tr valign="top">
            <th scope="row"><label for="stripe_publishable_key">'.esc_html__('Stripe Publishable Key','wpresidence').'</label></th>
            <td><input  type="text" id="stripe_publishable_key" name="stripe_publishable_key" class="regular-text" value="'.$stripe_publishable_key.'"/> </td>
        </tr>
        

        <tr valign="top">
            <th scope="row"><label for="direct_payment_details">'.esc_html__('Wire instructions for direct payment','wpresidence').'</label></th>
            <td><textarea id="direct_payment_details" name="direct_payment_details"  style="width:325px;" class="regular-text" >'.$direct_payment_details.'</textarea> </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="price_submission">'.esc_html__('Price Per Submission (for "per listing" mode)','wpresidence').'</label></th>
           <td><input  type="text" id="price_submission" name="price_submission"  value="'.$price_submission.'"/> </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="price_featured_submission">'.esc_html__('Price to make the listing featured (for "per listing" mode)','wpresidence').'</label></th>
           <td><input  type="text" id="price_featured_submission" name="price_featured_submission"  value="'.$price_featured_submission.'"/> </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="free_mem_list">'.esc_html__('Free Membership - no of listings (for "membership" mode)','wpresidence').' </label></th>
            <td>
                <input  type="text" id="free_mem_list" name="free_mem_list" style="margin-right:20px;"  value="'.$free_mem_list.'"/> 
       
                <input type="hidden" name="free_mem_list_unl" value="">
                <input type="checkbox"  id="free_mem_list_unl" name="free_mem_list_unl" value="1" '.$free_mem_list_unl.' />
                <label for="free_mem_list_unl">'.esc_html__('Unlimited listings ?','wpresidence').'</label>
            </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="free_feat_list">'.esc_html__('Free Membership - no of featured listings (for "membership" mode)','wpresidence').' </label></th>
            <td>
                <input  type="text" id="free_feat_list" name="free_feat_list" style="margin-right:20px;"    value="'.$free_feat_list.'"/>
              
            </td>
        </tr>
        
  
        <tr valign="top">
            <th scope="row"><label for="free_feat_list_expiration">'.esc_html__('Free Membership Listings - no of days until a free listing will expire. *Starts from the moment the property is published on the website. (for "membership" mode) ','wpresidence').' </label></th>
            <td>
                <input  type="text" id="free_feat_list_expiration" name="free_feat_list_expiration" style="margin-right:20px;"    value="'.$free_feat_list_expiration.'"/>
              
            </td>
        </tr>

        </table>
        <p class="submit">
            <input type="submit" name="submit" id="submit" class="button-primary" value="'.esc_html__('Save Changes','wpresidence').'" />
        </p>  
    ';
    print '</div>';
}
endif; // end   wpestate_theme_admin_membershipsettings  






/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///  General Settings
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   

if( !function_exists('wpestate_export_theme_options') ):
function wpestate_export_theme_options(){
    $export_options = array(    
        'wp_estate_show_reviews_prop',
        'wp_estate_enable_direct_mess',
        'wp_estate_admin_approves_reviews',
        'wp_estate_header5_info_widget1_icon',
        'wp_estate_header5_info_widget1_text1',
        'wp_estate_header5_info_widget1_text2',
        'wp_estate_header5_info_widget2_icon',
        'wp_estate_header5_info_widget2_text1',
        'wp_estate_header5_info_widget2_text2',
        'wp_estate_header5_info_widget3_text2',
        'wp_estate_header5_info_widget3_text1',
        'wp_estate_header5_info_widget3_icon',
        'wp_estate_spash_header_type',
        'wp_estate_splash_image',
        'wp_estate_splash_slider_gallery',
        'wp_estate_splash_slider_transition',
        'wp_estate_splash_video_mp4',
        'wp_estate_splash_video_webm',
        'wp_estate_splash_video_ogv',
        'wp_estate_splash_video_cover_img',
        'wp_estate_splash_overlay_image',
        'wp_estate_splash_overlay_color',
        'wp_estate_splash_overlay_opacity',
        'wp_estate_splash_page_title',
        'wp_estate_splash_page_subtitle',
        'wp_estate_splash_page_logo_link',    
        'wp_estate_theme_slider_height',
        'wp_estate_sticky_search',
        'wp_estate_use_geo_location',
        'wp_estate_geo_radius_measure',
        'wp_estate_initial_radius',
        'wp_estate_min_geo_radius',
        'wp_estate_max_geo_radius',
        'wp_estate_paralax_header',
        'wp_estate_keep_max',
        'wp_estate_adv_back_color_opacity',
        'wp_estate_search_on_start',
        'wp_estate_use_float_search_form',
        'wp_estate_float_form_top',
        'wp_estate_float_form_top_tax',
        'wp_estate_use_price_pins',
        'wp_estate_use_price_pins_full_price',
        'wp_estate_use_single_image_pin',
        'wpestate_export_theme_options',
        'wp_estate_mobile_header_background_color',
        'wp_estate_mobile_header_icon_color',
        'wp_estate_mobile_menu_font_color',
        'wp_estate_mobile_menu_hover_font_color',
        'wp_estate_mobile_item_hover_back_color',
        'wp_estate_mobile_menu_backgound_color',
        'wp_estate_mobile_menu_border_color',
        'wp_estate_crop_images_lightbox',
        'wp_estate_show_lightbox_contact',
        'wp_estate_submission_page_fields',
        'wp_estate_mandatory_page_fields',
        'wp_estate_url_rewrites',
        'wp_estate_print_show_subunits',
        'wp_estate_print_show_agent',
        'wp_estate_print_show_description',
        'wp_estate_print_show_adress',
        'wp_estate_print_show_details',
        'wp_estate_print_show_features',
        'wp_estate_print_show_floor_plans',
        'wp_estate_print_show_images',
        'wp_estate_show_header_dashboard',
        'wp_estate_user_dashboard_menu_color',
        'wp_estate_user_dashboard_menu_hover_color',
        'wp_estate_user_dashboard_menu_color_hover',
        'wp_estate_user_dashboard_menu_back',
        'wp_estate_user_dashboard_package_back',
        'wp_estate_user_dashboard_package_color',
        'wp_estate_user_dashboard_buy_package',
        'wp_estate_user_dashboard_package_select',
        'wp_estate_user_dashboard_content_back',
        'wp_estate_user_dashboard_content_button_back',
        'wp_estate_user_dashboard_content_color',
        'wp_estate_property_multi_text',                
        'wp_estate_property_multi_child_text', 
        'wp_estate_theme_slider_type',
        'wp_estate_adv6_taxonomy',
        'wp_estate_adv6_taxonomy_terms',   
        'wp_estate_adv6_max_price',     
        'wp_estate_adv6_min_price',
        'wp_estate_adv_search_fields_no',
        'wp_estate_search_fields_no_per_row',
        'wp_estate_property_sidebar',
        'wp_estate_property_sidebar_name',
        'wp_estate_show_breadcrumbs',
        'wp_estate_global_property_page_template',
        'wp_estate_p_fontfamily',
        'wp_estate_p_fontsize',
        'wp_estate_p_fontsubset',
        'wp_estate_p_lineheight',
        'wp_estate_p_fontweight',
        'wp_estate_h1_fontfamily',
        'wp_estate_h1_fontsize',
        'wp_estate_h1_fontsubset',
        'wp_estate_h1_lineheight',
        'wp_estate_h1_fontweight',
        'wp_estate_h2_fontfamily',
        'wp_estate_h2_fontsize',
        'wp_estate_h2_fontsubset',
        'wp_estate_h2_lineheight',
        'wp_estate_h2_fontweight',
        'wp_estate_h3_fontfamily',
        'wp_estate_h3_fontsize',
        'wp_estate_h3_fontsubset',
        'wp_estate_h3_lineheight',
        'wp_estate_h3_fontweight',
        'wp_estate_h4_fontfamily',
        'wp_estate_h4_fontsize',
        'wp_estate_h4_fontsubset',
        'wp_estate_h4_lineheight',
        'wp_estate_h4_fontweight',
        'wp_estate_h5_fontfamily',
        'wp_estate_h5_fontsize',
        'wp_estate_h5_fontsubset',
        'wp_estate_h5_lineheight',
        'wp_estate_h5_fontweight',
        'wp_estate_h6_fontfamily',
        'wp_estate_h6_fontsize',
        'wp_estate_h6_fontsubset',
        'wp_estate_h6_lineheight',
        'wp_estate_h6_fontweight',
        'wp_estate_menu_fontfamily',
        'wp_estate_menu_fontsize',
        'wp_estate_menu_fontsubset',
        'wp_estate_menu_lineheight',
        'wp_estate_menu_fontweight',
        'wp_estate_transparent_logo_image',
        'wp_estate_stikcy_logo_image',
        'wp_estate_logo_image',
        'wp_estate_sidebar_boxed_font_color',
        'wp_estate_sidebar_heading_background_color',
        'wp_estate_map_controls_font_color',
        'wp_estate_map_controls_back',
        'wp_estate_transparent_menu_hover_font_color',
        'wp_estate_transparent_menu_font_color',
        'top_menu_hover_back_font_color',
        'wp_estate_top_menu_hover_type',
        'wp_estate_top_menu_hover_font_color',
        'wp_estate_menu_item_back_color',
        'wp_estate_sticky_menu_font_color',
        'wp_estate_top_menu_font_size',
        'wp_estate_menu_item_font_size',
        'wpestate_uset_unit',
        'wp_estate_sidebarwidget_internal_padding_top',
        'wp_estate_sidebarwidget_internal_padding_left',
        'wp_estate_sidebarwidget_internal_padding_bottom',
        'wp_estate_sidebarwidget_internal_padding_right',
        'wp_estate_widget_sidebar_border_size',
        'wp_estate_widget_sidebar_border_color',
        'wp_estate_unit_border_color',
        'wp_estate_unit_border_size',
        'wp_estate_blog_unit_min_height',
        'wp_estate_agent_unit_min_height',
        'wp_estate_agent_listings_per_row',
        'wp_estate_blog_listings_per_row',
        'wp_estate_content_area_back_color',
        'wp_estate_contentarea_internal_padding_top',
        'wp_estate_contentarea_internal_padding_left',
        'wp_estate_contentarea_internal_padding_bottom',
        'wp_estate_contentarea_internal_padding_right',
        'wp_estate_property_unit_color',
        'wp_estate_propertyunit_internal_padding_top',
        'wp_estate_propertyunit_internal_padding_left',
        'wp_estate_propertyunit_internal_padding_bottom',
        'wp_estate_propertyunit_internal_padding_right',       
        'wpestate_property_unit_structure',
        'wpestate_property_page_content',
        'wp_estate_main_grid_content_width',
        'wp_estate_main_content_width',
        'wp_estate_header_height',
        'wp_estate_sticky_header_height',
        'wp_estate_border_radius_corner',
        'wp_estate_cssbox_shadow',
        'wp_estate_prop_unit_min_height',
        'wp_estate_border_bottom_header',
        'wp_estate_sticky_border_bottom_header',
        'wp_estate_listings_per_row',
        'wp_estate_unit_card_type',
        'wp_estate_prop_unit_min_height',
        'wp_estate_main_grid_content_width',
        'wp_estate_header_height',
        'wp_estate_sticky_header_height',
        'wp_estate_border_bottom_header_sticky_color',
        'wp_estate_border_bottom_header_color',
        'wp_estate_show_top_bar_user_login',
        'wp_estate_show_top_bar_user_menu',
        'wp_estate_currency_symbol',
        'wp_estate_where_currency_symbol',
        'wp_estate_measure_sys',
        'wp_estate_facebook_login',
        'wp_estate_google_login',
        'wp_estate_yahoo_login',
        'wp_estate_wide_status',
        'wp_estate_header_type',
        'wp_estate_prop_no',
        'wp_estate_prop_image_number',
        'wp_estate_show_empty_city',
        'wp_estate_blog_sidebar',
        'wp_estate_blog_sidebar_name',
        'wp_estate_blog_unit',
        'wp_estate_general_latitude',
        'wp_estate_general_longitude',
        'wp_estate_default_map_zoom',
        'wp_estate_cache',
        'wp_estate_show_adv_search_map_close',
        'wp_estate_pin_cluster',
        'wp_estate_zoom_cluster',
        'wp_estate_hq_latitude',
        'wp_estate_hq_longitude',
        'wp_estate_idx_enable',
        'wp_estate_geolocation_radius',
        'wp_estate_min_height',
        'wp_estate_max_height',
        'wp_estate_keep_min',
        'wp_estate_paid_submission',
        'wp_estate_admin_submission',
        'wp_estate_admin_submission_user_role',
        'wp_estate_price_submission',
        'wp_estate_price_featured_submission',
        'wp_estate_submission_curency',
        'wp_estate_free_mem_list',
        'wp_estate_free_feat_list',
        'wp_estate_free_feat_list_expiration',
        'wp_estate_free_pack_image_included',
        'wp_estate_custom_advanced_search',
        'wp_estate_adv_search_type',
        'wp_estate_show_adv_search',
        'wp_estate_show_adv_search_map_close',
        'wp_estate_cron_run',
        'wp_estate_show_no_features',
        'wp_estate_property_features_text',
        'wp_estate_property_description_text',
        'wp_estate_property_details_text',
        'wp_estate_status_list',
        'wp_estate_slider_cycle',
        'wp_estate_show_save_search',
        'wp_estate_search_alert',
        'wp_estate_adv_search_type',
        'wp_estate_color_scheme',
        'wp_estate_main_color',
        'wp_estate_second_color',
        'wp_estate_background_color',
        'wp_estate_content_back_color',
        'wp_estate_header_color',
        'wp_estate_breadcrumbs_font_color',
        'wp_estate_font_color',
        'wp_estate_menu_items_color',
        'wp_estate_link_color',
        'wp_estate_headings_color',
        'wp_estate_sidebar_heading_boxed_color',
        'wp_estate_sidebar_widget_color',
        'wp_estate_footer_back_color',
        'wp_estate_footer_font_color',
        'wp_estate_footer_copy_color',
        'wp_estate_footer_copy_back_color',
        'wp_estate_menu_font_color',
        'wp_estate_menu_hover_back_color',
        'wp_estate_menu_hover_font_color',
        'wp_estate_menu_border_color',
        'wp_estate_top_bar_back',
        'wp_estate_top_bar_font',
        'wp_estate_adv_search_back_color',
        'wp_estate_adv_search_font_color',
        'wp_estate_box_content_back_color',
        'wp_estate_box_content_border_color',
        'wp_estate_hover_button_color',
        'wp_estate_show_g_search',
        'wp_estate_show_adv_search_extended',
        'wp_estate_readsys',
        'wp_estate_map_max_pins',
        'wp_estate_ssl_map',
        'wp_estate_enable_stripe',    
        'wp_estate_enable_paypal',    
        'wp_estate_enable_direct_pay',    
        'wp_estate_global_property_page_agent_sidebar',
        'wp_estate_global_prpg_slider_type',
        'wp_estate_global_prpg_content_type',
        'wp_estate_logo_margin',
        'wp_estate_header_transparent',
        'wp_estate_default_map_type',
        'wp_estate_prices_th_separator',
        'wp_estate_multi_curr',
        'wp_estate_date_lang',
        'wp_estate_blog_unit',
        'wp_estate_enable_autocomplete',
        'wp_estate_visible_user_role_dropdown',
        'wp_estate_visible_user_role',
        'wp_estate_enable_user_pass',
        'wp_estate_auto_curency',
        'wp_estate_status_list',
        'wp_estate_custom_fields',
        'wp_estate_subject_password_reset_request',
        'wp_estate_password_reset_request',
        'wp_estate_subject_password_reseted',
        'wp_estate_password_reseted',
        'wp_estate_subject_purchase_activated',
        'wp_estate_purchase_activated',
        'wp_estate_subject_approved_listing',
        'wp_estate_approved_listing',
        'wp_estate_subject_new_wire_transfer',
        'wp_estate_new_wire_transfer',
        'wp_estate_subject_admin_new_wire_transfer',
        'wp_estate_admin_new_wire_transfer',
        'wp_estate_subject_admin_new_user',
        'wp_estate_admin_new_user',
        'wp_estate_subject_new_user',
        'wp_estate_new_user',
        'wp_estate_subject_admin_expired_listing',
        'wp_estate_admin_expired_listing',
        'wp_estate_subject_matching_submissions',
        'wp_estate_subject_paid_submissions',
        'wp_estate_paid_submissions',
        'wp_estate_subject_featured_submission',
        'wp_estate_featured_submission',
        'wp_estate_subject_account_downgraded',
        'wp_estate_account_downgraded',
        'wp_estate_subject_membership_cancelled',
        'wp_estate_membership_cancelled',
        'wp_estate_subject_downgrade_warning',
        'wp_estate_downgrade_warning',
        'wp_estate_subject_membership_activated',
        'wp_estate_membership_activated',
        'wp_estate_subject_free_listing_expired',
        'wp_estate_free_listing_expired',
        'wp_estate_subject_new_listing_submission',
        'wp_estate_new_listing_submission',
        'wp_estate_subject_listing_edit',
        'wp_estate_listing_edit',
        'wp_estate_subject_recurring_payment',
        'wp_estate_subject_recurring_payment',
         'wp_estate_custom_css',
        'wp_estate_company_name',
        'wp_estate_telephone_no',
        'wp_estate_mobile_no',
        'wp_estate_fax_ac',
        'wp_estate_skype_ac',
        'wp_estate_co_address',
        'wp_estate_facebook_link',
        'wp_estate_twitter_link',
        'wp_estate_pinterest_link',
        'wp_estate_instagram_link',
        'wp_estate_linkedin_link',
        'wp_estate_contact_form_7_agent',
        'wp_estate_contact_form_7_contact',
        'wp_estate_global_revolution_slider',
        'wp_estate_repeat_footer_back',
        'wp_estate_prop_list_slider',
        'wp_estate_agent_sidebar',
        'wp_estate_agent_sidebar_name',
        'wp_estate_property_list_type',
        'wp_estate_property_list_type_adv',
        'wp_estate_prop_unit',
        'wp_estate_general_font',
        'wp_estate_headings_font_subset',
        'wp_estate_copyright_message',
        'wp_estate_show_graph_prop_page',
        'wp_estate_map_style',
        'wp_estate_submission_curency_custom',
        'wp_estate_free_mem_list_unl',
        'wp_estate_adv_search_what',
        'wp_estate_adv_search_how',
        'wp_estate_adv_search_label',
        'wp_estate_show_save_search',
        'wp_estate_show_adv_search_slider',
        'wp_estate_show_adv_search_visible',
        'wp_estate_show_slider_price',
        'wp_estate_show_dropdowns',
        'wp_estate_show_slider_min_price',
        'wp_estate_show_slider_max_price',
        'wp_estate_wp_estate_adv_back_color',
        'wp_estate_adv_font_color',
        'wp_estate_feature_list',
        'wp_estate_show_no_features',
        'wp_estate_advanced_exteded',
        'wp_estate_adv_search_what',
        'wp_estate_adv_search_how',
        'wp_estate_adv_search_label',
        'wp_estate_adv_search_type',
        'wp_estate_property_adr_text',
        'wp_estate_property_features_text',
        'wp_estate_property_description_text',
        'wp_estate_property_details_text',
        'wp_estate_new_status',
        'wp_estate_status_list',
        'wp_estate_theme_slider',
        'wp_estate_slider_cycle',
        'wp_estate_use_mimify',
        'wp_estate_currency_label_main',
        'wp_estate_footer_background',
        'wp_estate_wide_footer',
        'wp_estate_show_footer',
    '   wp_estate_show_footer_copy',
        'wp_estate_footer_type',
        'wp_estate_logo_header_type',
        'wp_estate_wide_header',
        'wp_estate_logo_header_align',
        'wp_estate_text_header_align',
        'wp_estate_general_country',
        'wp_estate_show_submit'
        );
    
  
    
    $return_exported_data=array();
    // esc_html( get_option('wp_estate_where_currency_symbol') );
    foreach($export_options as $option){
        $real_option=get_option($option);
        
        if(is_array($real_option)){
            $return_exported_data[$option]= get_option($option) ;
        }else{
            $return_exported_data[$option]=esc_html( get_option($option) );
        }
     
    }
    
    if(function_exists('wpestate_return_imported_data_encoded')){
        return wpestate_return_imported_data_encoded($return_exported_data);
    }else{
        return '';
    }
    
}
endif;


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///  Social $  Contact
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


if( !function_exists('wpestate_theme_admin_social') ):
function wpestate_theme_admin_social(){
    $fax_ac                     =   esc_html (  stripslashes( get_option('wp_estate_fax_ac','') ) );
    $skype_ac                   =   esc_html (  stripslashes( get_option('wp_estate_skype_ac','') ) );
    $telephone_no               =   esc_html (  stripslashes( get_option('wp_estate_telephone_no','') ) );
    $mobile_no                  =   esc_html (  stripslashes( get_option('wp_estate_mobile_no','') ) );
    $company_name               =   esc_html (  stripslashes(get_option('wp_estate_company_name','') ) );
    $email_adr                  =   esc_html ( get_option('wp_estate_email_adr','') );
    $duplicate_email_adr        =   esc_html ( get_option('wp_estate_duplicate_email_adr','') );   
    $co_address                 =   esc_html ( stripslashes( get_option('wp_estate_co_address','') ) );
    $facebook_link              =   esc_html ( get_option('wp_estate_facebook_link','') );
    $twitter_link               =   esc_html ( get_option('wp_estate_twitter_link','') );
    $google_link                =   esc_html ( get_option('wp_estate_google_link','') );
    $linkedin_link              =   esc_html ( get_option('wp_estate_linkedin_link','') );
    $pinterest_link             =   esc_html ( get_option('wp_estate_pinterest_link','') );  
    $instagram_link             =   esc_html ( get_option('wp_estate_instagram_link','') );  
    $twitter_consumer_key       =   esc_html ( get_option('wp_estate_twitter_consumer_key','') );
    $twitter_consumer_secret    =   esc_html ( get_option('wp_estate_twitter_consumer_secret','') );
    $twitter_access_token       =   esc_html ( get_option('wp_estate_twitter_access_token','') );
    $twitter_access_secret      =   esc_html ( get_option('wp_estate_twitter_access_secret','') );
    $twitter_cache_time         =   intval   ( get_option('wp_estate_twitter_cache_time','') );
    $zillow_api_key             =   esc_html ( get_option('wp_estate_zillow_api_key','') );
    $facebook_api               =   esc_html ( get_option('wp_estate_facebook_api','') );
    $facebook_secret            =   esc_html ( get_option('wp_estate_facebook_secret','') );
    $company_contact_image      =   esc_html( get_option('wp_estate_company_contact_image','') );
    $google_oauth_api           =   esc_html ( get_option('wp_estate_google_oauth_api','') );
    $google_oauth_client_secret =   esc_html ( get_option('wp_estate_google_oauth_client_secret','') );
    $google_api_key             =   esc_html ( get_option('wp_estate_google_api_key','') );
    
    
    $social_array               =   array('no','yes');
   
    $facebook_login_select      = wpestate_dropdowns_theme_admin($social_array,'facebook_login');
    $google_login_select        = wpestate_dropdowns_theme_admin($social_array,'google_login');
    $yahoo_login_select         = wpestate_dropdowns_theme_admin($social_array,'yahoo_login');
    $contact_form_7_contact     = stripslashes( esc_html( get_option('wp_estate_contact_form_7_contact','') ) );
    $contact_form_7_agent       = stripslashes( esc_html( get_option('wp_estate_contact_form_7_agent','') ) );
    
    print '<div class="wpestate-tab-container">';
    print '<h1 class="wpestate-tabh1">Social</h1>';
    print '<a href="http://help.wpresidence.net/article/social-contact/" target="_blank" class="help_link">'.esc_html__('help','wpresidence').'</a>';
   
    print '<table class="form-table">     
        <tr valign="top">
            <th scope="row"><label for="company_contact_image">'.esc_html__('Image for Contact Page','wpresidence').'</label></th>
            <td>
	        <input id="company_contact_image" type="text" size="36" name="company_contact_image" value="'.$company_contact_image.'" />
		<input id="company_contact_image_button" type="button"  class="upload_button button" value="'.esc_html__('Upload Image','wpresidence').'" />
            </td>
        </tr> 
        
        <tr valign="top">
            <th scope="row"><label for="company_name">'.esc_html__('Company Name','wpresidence').'</label></th>
            <td>  <input id="company_name" type="text" size="36" name="company_name" value="'.$company_name.'" /></td>
        </tr>   
        
    	<tr valign="top">
            <th scope="row"><label for="email_adr">'.esc_html__('Email','wpresidence').'</label></th>
            <td>  <input id="email_adr" type="text" size="36" name="email_adr" value="'.$email_adr.'" /></td>
        </tr>    
        
        <tr valign="top">
            <th scope="row"><label for="duplicate_email_adr">'.esc_html__('Send all contact emails to:','wpresidence').'</label></th>
            <td>  <input id="duplicate_email_adr" type="text" size="36" name="duplicate_email_adr" value="'.$duplicate_email_adr.'" /></td>
        </tr> 
        
        <tr valign="top">
            <th scope="row"><label for="telephone_no">'.esc_html__('Telephone','wpresidence').'</label></th>
            <td>  <input id="telephone_no" type="text" size="36" name="telephone_no" value="'.$telephone_no.'" /></td>
        </tr> 
        
        <tr valign="top">
            <th scope="row"><label for="mobile_no">'.esc_html__('Mobile','wpresidence').'</label></th>
            <td>  <input id="mobile_no" type="text" size="36" name="mobile_no" value="'.$mobile_no.'" /></td>
        </tr> 
        
         <tr valign="top">
            <th scope="row"><label for="fax_ac">'.esc_html__('Fax','wpresidence').'</label></th>
            <td>  <input id="fax_ac" type="text" size="36" name="fax_ac" value="'.$fax_ac.'" /></td>
        </tr> 
        
        <tr valign="top">
            <th scope="row"><label for="skype_ac">'.esc_html__('Skype','wpresidence').'</label></th>
            <td>  <input id="skype_ac" type="text" size="36" name="skype_ac" value="'.$skype_ac.'" /></td>
        </tr> 
        
        <tr valign="top">
            <th scope="row"><label for="co_address">'.esc_html__('Address','wpresidence').'</label></th>
            <td><textarea cols="57" rows="2" name="co_address" id="co_address">'.$co_address.'</textarea></td>
        </tr> 
        
        <tr valign="top">
            <th scope="row"><label for="facebook_link">'.esc_html__('Facebook Link','wpresidence').'</label></th>
            <td>  <input id="facebook_link" type="text" size="36" name="facebook_link" value="'.$facebook_link.'" /></td>
        </tr>        
        
        <tr valign="top">
            <th scope="row"><label for="twitter_link">'.esc_html__('Twitter Page Link','wpresidence').'</label></th>
            <td>  <input id="twitter_link" type="text" size="36" name="twitter_link" value="'.$twitter_link.'" /></td>
        </tr>
         
        <tr valign="top">
            <th scope="row"><label for="google_link">'.esc_html__('Google+ Link','wpresidence').'</label></th>
            <td>  <input id="google_link" type="text" size="36" name="google_link" value="'.$google_link.'" /></td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="pinterest_link">'.esc_html__('Pinterest Link','wpresidence').'</label></th>
            <td>  <input id="pinterest_link" type="text" size="36" name="pinterest_link" value="'.$pinterest_link.'" /></td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="linkedin_link">'.esc_html__('Linkedin Link','wpresidence').'</label></th>
            <td>  <input id="linkedin_link" type="text" size="36" name="linkedin_link" value="'.$linkedin_link.'" /></td>
        </tr>
        

        <tr valign="top">
            <th scope="row"><label for="twitter_consumer_key">'.esc_html__('Twitter Consumer Key','wpresidence').'</label></th>
            <td>  <input id="twitter_consumer_key" type="text" size="36" name="twitter_consumer_key" value="'.$twitter_consumer_key.'" /></td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="twitter_consumer_secret">'.esc_html__('Twitter Consumer Secret','wpresidence').'</label></th>
            <td>  <input id="twitter_consumer_secret" type="text" size="36" name="twitter_consumer_secret" value="'.$twitter_consumer_secret.'" /></td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="twitter_access_token">'.esc_html__('Twitter Access Token','wpresidence').'</label></th>
            <td>  <input id="twitter_account" type="text" size="36" name="twitter_access_token" value="'.$twitter_access_token.'" /></td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="twitter_access_secret">'.esc_html__('Twitter Access Token Secret','wpresidence').'</label></th>
            <td>  <input id="twitter_access_secret" type="text" size="36" name="twitter_access_secret" value="'.$twitter_access_secret.'" /></td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="twitter_cache_time">'.esc_html__('Twitter Cache Time in hours','wpresidence').'</label></th>
            <td>  <input id="twitter_cache_time" type="text" size="36" name="twitter_cache_time" value="'.$twitter_cache_time.'" /></td>
        </tr>
         
        <tr valign="top">
            <th scope="row"><label for="facebook_api">'.esc_html__('Facebook Api Key (for Facebook login)','wpresidence').'</label></th>
            <td>  <input id="facebook_api" type="text" size="36" name="facebook_api" value="'.$facebook_api.'" /></td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="facebook_secret">'.esc_html__('Facebook secret code (for Facebook login) ','wpresidence').'</label></th>
            <td>  <input id="facebook_secret" type="text" size="36" name="facebook_secret" value="'.$facebook_secret.'" /></td>
        </tr>
       
        <tr valign="top">
            <th scope="row"><label for="google_oauth_api">'.esc_html__('Google OAuth client id (for Google login)','wpresidence').'</label></th>
            <td>  <input id="google_oauth_api" type="text" size="36" name="google_oauth_api" value="'.$google_oauth_api.'" /></td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="google_oauth_client_secret">'.esc_html__('Google Client Secret (for Google login)','wpresidence').'</label></th>
            <td>  <input id="google_oauth_client_secret" type="text" size="36" name="google_oauth_client_secret" value="'.$google_oauth_client_secret.'" /></td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="google_api_key">'.esc_html__('Google Api key (for Google login)','wpresidence').'</label></th>
            <td>  <input id="google_api_key" type="text" size="36" name="google_api_key" value="'.$google_api_key.'" /></td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="facebook_login">'.esc_html__('Allow login via Facebook ? ','wpresidence').'</label></th>
            <td> <select id="facebook_login" name="facebook_login">
                    '.$facebook_login_select.'
                </select>
            </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="google_login">'.esc_html__('Allow login via Google ?','wpresidence').' </label></th>
            <td> <select id="google_login" name="google_login">
                    '.$google_login_select.'
                </select>
            </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="yahoo_login">'.esc_html__('Allow login via Yahoo ? ','wpresidence').'</label></th>
            <td> <select id="yahoo_login" name="yahoo_login">
                    '.$yahoo_login_select.'
                </select>
            </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="contact_form_7_agent">'.esc_html__('Contact form 7 code for agent (ex: [contact-form-7 id="2725" title="contact me"])','wpresidence').'</label></th>
            <td> 
                <input type="text" size="36" id="contact_form_7_agent" name="contact_form_7_agent" value="'.$contact_form_7_agent.'" />
            </td>
        </tr>
        
        <tr valign="top">
            <th scope="row"><label for="contact_form_7_contact">'.esc_html__('Contact form 7 code for contact page template (ex: [contact-form-7 id="2725" title="contact me"])','wpresidence').'</label></th>
            <td> 
                 <input type="text" size="36" id="contact_form_7_contact" name="contact_form_7_contact" value="'.$contact_form_7_contact.'" />
            </td>
        </tr>
        

    </table>
    <p class="submit">
      <input type="submit" name="submit" id="submit" class="button-primary"  value="'.esc_html__('Save Changes','wpresidence').'" />
    </p>';
print '</div>';
}
endif; // end   wpestate_theme_admin_social  








/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///  help and custom
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if( !function_exists('wpestate_theme_admin_help') ):
function wpestate_theme_admin_help(){
    print '<div class="wpestate-tab-container">';
    print '<h1 class="wpestate-tabh1">'.esc_html__('Help','wpresidence').'</h1>';
    print '<table class="form-table">  
 
        <tr valign="top">
            <td> '.esc_html__('For theme help please check http://help.wpresidence.net/. If your question is not here, please go to http://support.wpestate.org, create an account and post a ticket. The registration is simple and as soon as you send a ticket we are notified. We usually answer in the next 24h (except weekends). Please use this system and not the email. It will help us answer your questions much faster. Thank you!','wpresidence').'
            </td>             
        </tr>
        
        <tr valign="top">
            <td> '.esc_html__('For custom work on this theme please go to  <a href="http://support.wpestate.org/" target="_blank">http://support.wpestate.org</a>, create a ticket with your request and we will offer a free quote.','wpresidence').'
            </td>             
        </tr>
        
        <tr valign="top">
            <td> '.esc_html__('For help files please go to  <a href="http://help.wpresidence.net/">http://help.wpresidence.net</a>.','wpresidence').'
            </td>             
        </tr>
        
         
        <tr valign="top">
            <td>  '.esc_html__('Subscribe to our mailing list in order to receive news about new features and theme upgrades. <a href="http://eepurl.com/CP5U5">Subscribe Here!</a>','wpresidence').'
            </td>             
        </tr>
        </table>
        
      ';
    print '</div>';
}
endif; // end   wpestate_theme_admin_help  



if( !function_exists('wpestate_general_country_list') ):
    function wpestate_general_country_list($selected){
        $countries = array(esc_html__("Afghanistan","wpresidence"),esc_html__("Albania","wpresidence"),esc_html__("Algeria","wpresidence"),esc_html__("American Samoa","wpresidence"),esc_html__("Andorra","wpresidence"),esc_html__("Angola","wpresidence"),esc_html__("Anguilla","wpresidence"),esc_html__("Antarctica","wpresidence"),esc_html__("Antigua and Barbuda","wpresidence"),esc_html__("Argentina","wpresidence"),esc_html__("Armenia","wpresidence"),esc_html__("Aruba","wpresidence"),esc_html__("Australia","wpresidence"),esc_html__("Austria","wpresidence"),esc_html__("Azerbaijan","wpresidence"),esc_html__("Bahamas","wpresidence"),esc_html__("Bahrain","wpresidence"),esc_html__("Bangladesh","wpresidence"),esc_html__("Barbados","wpresidence"),esc_html__("Belarus","wpresidence"),esc_html__("Belgium","wpresidence"),esc_html__("Belize","wpresidence"),esc_html__("Benin","wpresidence"),esc_html__("Bermuda","wpresidence"),esc_html__("Bhutan","wpresidence"),esc_html__("Bolivia","wpresidence"),esc_html__("Bosnia and Herzegowina","wpresidence"),esc_html__("Botswana","wpresidence"),esc_html__("Bouvet Island","wpresidence"),esc_html__("Brazil","wpresidence"),esc_html__("British Indian Ocean Territory","wpresidence"),esc_html__("Brunei Darussalam","wpresidence"),esc_html__("Bulgaria","wpresidence"),esc_html__("Burkina Faso","wpresidence"),esc_html__("Burundi","wpresidence"),esc_html__("Cambodia","wpresidence"),esc_html__("Cameroon","wpresidence"),esc_html__("Canada","wpresidence"),esc_html__("Cape Verde","wpresidence"),esc_html__("Cayman Islands","wpresidence"),esc_html__("Central African Republic","wpresidence"),esc_html__("Chad","wpresidence"),esc_html__("Chile","wpresidence"),esc_html__("China","wpresidence"),esc_html__("Christmas Island","wpresidence"),esc_html__("Cocos (Keeling) Islands","wpresidence"),esc_html__("Colombia","wpresidence"),esc_html__("Comoros","wpresidence"),esc_html__("Congo","wpresidence"),esc_html__("Congo, the Democratic Republic of the","wpresidence"),esc_html__("Cook Islands","wpresidence"),esc_html__("Costa Rica","wpresidence"),esc_html__("Cote d'Ivoire","wpresidence"),esc_html__("Croatia (Hrvatska)","wpresidence"),esc_html__("Cuba","wpresidence"),esc_html__('Curacao','wpresidence'),esc_html__("Cyprus","wpresidence"),esc_html__("Czech Republic","wpresidence"),esc_html__("Denmark","wpresidence"),esc_html__("Djibouti","wpresidence"),esc_html__("Dominica","wpresidence"),esc_html__("Dominican Republic","wpresidence"),esc_html__("East Timor","wpresidence"),esc_html__("Ecuador","wpresidence"),esc_html__("Egypt","wpresidence"),esc_html__("El Salvador","wpresidence"),esc_html__("Equatorial Guinea","wpresidence"),esc_html__("Eritrea","wpresidence"),esc_html__("Estonia","wpresidence"),esc_html__("Ethiopia","wpresidence"),esc_html__("Falkland Islands (Malvinas)","wpresidence"),esc_html__("Faroe Islands","wpresidence"),esc_html__("Fiji","wpresidence"),esc_html__("Finland","wpresidence"),esc_html__("France","wpresidence"),esc_html__("France Metropolitan","wpresidence"),esc_html__("French Guiana","wpresidence"),esc_html__("French Polynesia","wpresidence"),esc_html__("French Southern Territories","wpresidence"),esc_html__("Gabon","wpresidence"),esc_html__("Gambia","wpresidence"),esc_html__("Georgia","wpresidence"),esc_html__("Germany","wpresidence"),esc_html__("Ghana","wpresidence"),esc_html__("Gibraltar","wpresidence"),esc_html__("Greece","wpresidence"),esc_html__("Greenland","wpresidence"),esc_html__("Grenada","wpresidence"),esc_html__("Guadeloupe","wpresidence"),esc_html__("Guam","wpresidence"),esc_html__("Guatemala","wpresidence"),esc_html__("Guinea","wpresidence"),esc_html__("Guinea-Bissau","wpresidence"),esc_html__("Guyana","wpresidence"),esc_html__("Haiti","wpresidence"),esc_html__("Heard and Mc Donald Islands","wpresidence"),esc_html__("Holy See (Vatican City State)","wpresidence"),esc_html__("Honduras","wpresidence"),esc_html__("Hong Kong","wpresidence"),esc_html__("Hungary","wpresidence"),esc_html__("Iceland","wpresidence"),esc_html__("India","wpresidence"),esc_html__("Indonesia","wpresidence"),esc_html__("Iran (Islamic Republic of)","wpresidence"),esc_html__("Iraq","wpresidence"),esc_html__("Ireland","wpresidence"),esc_html__("Israel","wpresidence"),esc_html__("Italy","wpresidence"),esc_html__("Jamaica","wpresidence"),esc_html__("Japan","wpresidence"),esc_html__("Jordan","wpresidence"),esc_html__("Kazakhstan","wpresidence"),esc_html__("Kenya","wpresidence"),esc_html__("Kiribati","wpresidence"),esc_html__("Korea, Democratic People's Republic of","wpresidence"),esc_html__("Korea, Republic of","wpresidence"),esc_html__("Kuwait","wpresidence"),esc_html__("Kyrgyzstan","wpresidence"),esc_html__("Lao, People's Democratic Republic","wpresidence"),esc_html__("Latvia","wpresidence"),esc_html__("Lebanon","wpresidence"),esc_html__("Lesotho","wpresidence"),esc_html__("Liberia","wpresidence"),esc_html__("Libyan Arab Jamahiriya","wpresidence"),esc_html__("Liechtenstein","wpresidence"),esc_html__("Lithuania","wpresidence"),esc_html__("Luxembourg","wpresidence"),esc_html__("Macau","wpresidence"),esc_html__("Macedonia (FYROM)","wpresidence"),esc_html__("Madagascar","wpresidence"),esc_html__("Malawi","wpresidence"),esc_html__("Malaysia","wpresidence"),esc_html__("Maldives","wpresidence"),esc_html__("Mali","wpresidence"),esc_html__("Malta","wpresidence"),esc_html__("Marshall Islands","wpresidence"),esc_html__("Martinique","wpresidence"),esc_html__("Mauritania","wpresidence"),esc_html__("Mauritius","wpresidence"),esc_html__("Mayotte","wpresidence"),esc_html__("Mexico","wpresidence"),esc_html__("Micronesia, Federated States of","wpresidence"),esc_html__("Moldova, Republic of","wpresidence"),esc_html__("Monaco","wpresidence"),esc_html__("Mongolia","wpresidence"),esc_html__("Montserrat","wpresidence"),esc_html__("Morocco","wpresidence"),esc_html__("Mozambique","wpresidence"),esc_html__("Montenegro","wpresidence"),esc_html__("Myanmar","wpresidence"),esc_html__("Namibia","wpresidence"),esc_html__("Nauru","wpresidence"),esc_html__("Nepal","wpresidence"),esc_html__("Netherlands","wpresidence"),esc_html__("Netherlands Antilles","wpresidence"),esc_html__("New Caledonia","wpresidence"),esc_html__("New Zealand","wpresidence"),esc_html__("Nicaragua","wpresidence"),esc_html__("Niger","wpresidence"),esc_html__("Nigeria","wpresidence"),esc_html__("Niue","wpresidence"),esc_html__("Norfolk Island","wpresidence"),esc_html__("Northern Mariana Islands","wpresidence"),esc_html__("Norway","wpresidence"),esc_html__("Oman","wpresidence"),esc_html__("Pakistan","wpresidence"),esc_html__("Palau","wpresidence"),esc_html__("Panama","wpresidence"),esc_html__("Papua New Guinea","wpresidence"),esc_html__("Paraguay","wpresidence"),esc_html__("Peru","wpresidence"),esc_html__("Philippines","wpresidence"),esc_html__("Pitcairn","wpresidence"),esc_html__("Poland","wpresidence"),esc_html__("Portugal","wpresidence"),esc_html__("Puerto Rico","wpresidence"),esc_html__("Qatar","wpresidence"),esc_html__("Reunion","wpresidence"),esc_html__("Romania","wpresidence"),esc_html__("Russian Federation","wpresidence"),esc_html__("Rwanda","wpresidence"),esc_html__("Saint Kitts and Nevis","wpresidence"),esc_html__("Saint Martin","wpresidence"),esc_html__("Saint Lucia","wpresidence"),esc_html__("Saint Vincent and the Grenadines","wpresidence"),esc_html__("Samoa","wpresidence"),esc_html__("San Marino","wpresidence"),esc_html__("Sao Tome and Principe","wpresidence"),esc_html__("Saudi Arabia","wpresidence"),esc_html__("Senegal","wpresidence"),esc_html__("Seychelles","wpresidence"),esc_html__("Serbia","wpresidence"),esc_html__("Sierra Leone","wpresidence"),esc_html__("Singapore","wpresidence"),esc_html__("Slovakia (Slovak Republic)","wpresidence"),esc_html__("Slovenia","wpresidence"),esc_html__("Solomon Islands","wpresidence"),esc_html__("Somalia","wpresidence"),esc_html__("South Africa","wpresidence"),esc_html__("South Georgia and the South Sandwich Islands","wpresidence"),esc_html__("Spain","wpresidence"),esc_html__("Sri Lanka","wpresidence"),esc_html__("St. Helena","wpresidence"),esc_html__("St. Pierre and Miquelon","wpresidence"),esc_html__("Sudan","wpresidence"),esc_html__("Suriname","wpresidence"),esc_html__("Svalbard and Jan Mayen Islands","wpresidence"),esc_html__("Swaziland","wpresidence"),esc_html__("Sweden","wpresidence"),esc_html__("Switzerland","wpresidence"),esc_html__("Syrian Arab Republic","wpresidence"),esc_html__("Taiwan, Province of China","wpresidence"),esc_html__("Tajikistan","wpresidence"),esc_html__("Tanzania, United Republic of","wpresidence"),esc_html__("Thailand","wpresidence"),esc_html__("Togo","wpresidence"),esc_html__("Tokelau","wpresidence"),esc_html__("Tonga","wpresidence"),esc_html__("Trinidad and Tobago","wpresidence"),esc_html__("Tunisia","wpresidence"),esc_html__("Turkey","wpresidence"),esc_html__("Turkmenistan","wpresidence"),esc_html__("Turks and Caicos Islands","wpresidence"),esc_html__("Tuvalu","wpresidence"),esc_html__("Uganda","wpresidence"),esc_html__("Ukraine","wpresidence"),esc_html__("United Arab Emirates","wpresidence"),esc_html__("United Kingdom","wpresidence"),esc_html__("United States","wpresidence"),esc_html__("United States Minor Outlying Islands","wpresidence"),esc_html__("Uruguay","wpresidence"),esc_html__("Uzbekistan","wpresidence"),esc_html__("Vanuatu","wpresidence"),esc_html__("Venezuela","wpresidence"),esc_html__("Vietnam","wpresidence"),esc_html__("Virgin Islands (British)","wpresidence"),esc_html__("Virgin Islands (U.S.)","wpresidence"),esc_html__("Wallis and Futuna Islands","wpresidence"),esc_html__("Western Sahara","wpresidence"),esc_html__("Yemen","wpresidence"),esc_html__("Zambia","wpresidence"),esc_html__("Zimbabwe","wpresidence"));
        $country_select='<select id="general_country" style="width: 200px;" name="general_country">';

        foreach($countries as $country){
            $country_select.='<option value="'.$country.'"';  
            if($selected==$country){
                $country_select.='selected="selected"';
            }
            $country_select.='>'.$country.'</option>';
        }

        $country_select.='</select>';
        return $country_select;
    }
endif; // end   wpestate_general_country_list  


function wpestate_sorting_function($a, $b) {
    return $a[3] - $b[3];
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///  Wpestate Price settings
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////






if( !function_exists('new_wpestate_theme_admin_general_settings') ):
function new_wpestate_theme_admin_general_settings(){
    
    $cache_array                    =   array('yes','no');
    $social_array                   =   array('no','yes');
    
    $general_country                =   esc_html( get_option('wp_estate_general_country') );
    $measure_sys='';
    $measure_array = array( 
		
		array( 'name' => esc_html__('feet','wpresidence'), 'unit'  => esc_html__('ft','wpresidence'), 'is_square' => 0 ),
		array( 'name' => esc_html__('meters','wpresidence'), 'unit'  => esc_html__('m','wpresidence'), 'is_square' => 0 ),
		array( 'name' => esc_html__('acres','wpresidence'), 'unit'  => esc_html__('ac','wpresidence'), 'is_square' => 1 ),
		array( 'name' => esc_html__('yards','wpresidence'), 'unit'  => esc_html__('yd','wpresidence'), 'is_square' => 0 ),
		array( 'name' => esc_html__('hectares','wpresidence'), 'unit'  => esc_html__('ha','wpresidence'), 'is_square' => 1 ),
    );
    update_option( 'wpestate_measurement_units',  $measure_array);
	
	
    $measure_array_status= esc_html( get_option('wp_estate_measure_sys','') );

    foreach($measure_array as $single_unit ){
		

            $measure_sys.='<option value="'.$single_unit['unit'].'"';
            if ($measure_array_status==$single_unit['unit']){
                $measure_sys.=' selected="selected" ';
            }
			if( $single_unit['is_square'] === 1 ){
				$measure_sys.='>'.$single_unit['name'].' - '.$single_unit['unit'].'</option>';
			}else{
				$measure_sys.='>'.esc_html__('square','wpresidence').' '.$single_unit['name'].' - '.$single_unit['unit'].'<sup>2</sup></option>';
			}
            
    }
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Country','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Select default country','wpresidence').'</div>    
        '.wpestate_general_country_list($general_country).'
    </div>';
    
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Measurement Unit','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Select the measurement unit you will use on the website','wpresidence').'</div>    
            <select id="measure_sys" name="measure_sys">
                '.$measure_sys.'
            </select>
    </div>';

    $enable_autocomplete_symbol = wpestate_dropdowns_theme_admin($cache_array,'enable_autocomplete');
      
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Enable Autocomplete in Front End Submission Form','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('If yes, the address field in front end submission form will use Google Places autocomplete.','wpresidence').'</div>    
            <select id="enable_autocomplete" name="enable_autocomplete">
                '.$enable_autocomplete_symbol.'
            </select>
    </div>';
    
    
    $date_languages=array(  'xx'=> 'default',
                            'af'=>'Afrikaans',
                            'ar'=>'Arabic',
                            'ar-DZ' =>'Algerian',
                            'az'=>'Azerbaijani',
                            'be'=>'Belarusian',
                            'bg'=>'Bulgarian',
                            'bs'=>'Bosnian',
                            'ca'=>'Catalan',
                            'cs'=>'Czech',
                            'cy-GB'=>'Welsh/UK',
                            'da'=>'Danish',
                            'de'=>'German',
                            'el'=>'Greek',
                            'en-AU'=>'English/Australia',
                            'en-GB'=>'English/UK',
                            'en-NZ'=>'English/New Zealand',
                            'eo'=>'Esperanto',
                            'es'=>'Spanish',
                            'et'=>'Estonian',
                            'eu'=>'Karrikas-ek',
                            'fa'=>'Persian',
                            'fi'=>'Finnish',
                            'fo'=>'Faroese',
                            'fr'=>'French',
                            'fr-CA'=>'Canadian-French',
                            'fr-CH'=>'Swiss-French',
                            'gl'=>'Galician',
                            'he'=>'Hebrew',
                            'hi'=>'Hindi',
                            'hr'=>'Croatian',
                            'hu'=>'Hungarian',
                            'hy'=>'Armenian',
                            'id'=>'Indonesian',
                            'ic'=>'Icelandic',
                            'it'=>'Italian',
                            'it-CH'=>'Italian-CH',
                            'ja'=>'Japanese',
                            'ka'=>'Georgian',
                            'kk'=>'Kazakh',
                            'km'=>'Khmer',
                            'ko'=>'Korean',
                            'ky'=>'Kyrgyz',
                            'lb'=>'Luxembourgish',
                            'lt'=>'Lithuanian',
                            'lv'=>'Latvian',
                            'mk'=>'Macedonian',
                            'ml'=>'Malayalam',
                            'ms'=>'Malaysian',
                            'nb'=>'Norwegian',
                            'nl'=>'Dutch',
                            'nl-BE'=>'Dutch-Belgium',
                            'nn'=>'Norwegian-Nynorsk',
                            'no'=>'Norwegian',
                            'pl'=>'Polish',
                            'pt'=>'Portuguese',
                            'pt-BR'=>'Brazilian',
                            'rm'=>'Romansh',
                            'ro'=>'Romanian',
                            'ru'=>'Russian',
                            'sk'=>'Slovak',
                            'sl'=>'Slovenian',
                            'sq'=>'Albanian',
                            'sr'=>'Serbian',
                            'sr-SR'=>'Serbian-i18n',
                            'sv'=>'Swedish',
                            'ta'=>'Tamil',
                            'th'=>'Thai',
                            'tj'=>'Tajiki',
                            'tr'=>'Turkish',
                            'uk'=>'Ukrainian',
                            'vi'=>'Vietnamese',
                            'zh-CN'=>'Chinese',
                            'zh-HK'=>'Chinese-Hong-Kong',
                            'zh-TW'=>'Chinese Taiwan',
        );  

    
    $date_lang_symbol =  wpestate_dropdowns_theme_admin_with_key($date_languages,'date_lang');
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Language for datepicker','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('This applies for the calendar field type available for properties.','wpresidence').'</div>    
        <select id="date_lang" name="date_lang">
            '.$date_lang_symbol.'
         </select>
    </div>';
      
    $google_analytics_code          =   esc_html ( get_option('wp_estate_google_analytics_code','') );
  
    
    
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Google Analytics Tracking id','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Google Analytics Tracking id (ex UA-41924406-1)','wpresidence').'</div>    
        <input  name="google_analytics_code" id="google_analytics_code" value="'.$google_analytics_code.'"></input>
    </div>';
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
   
}
endif; // end   wpestate_theme_admin_general_settings 


////////////////////////
// User Role Settings
////////////////////
if( !function_exists('wpestate_theme_admin_user_role_settings') ):
function wpestate_theme_admin_user_role_settings(){

    $visible_user_role_select      =  array( esc_html__('User','wpresidence') ,esc_html__('Agent','wpresidence'),esc_html__('Agency','wpresidence'),esc_html__('Developer','wpresidence'));
    $cache_array                    =   array('yes','no');
    $visible_user_role_dropdown = wpestate_dropdowns_theme_admin($cache_array,'visible_user_role_dropdown');
      
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Display user roles dropdown in register forms','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('This applies for for all register forms.','wpresidence').'</div>    
            <select id="visible_user_role_dropdown" name="visible_user_role_dropdown">
                '.$visible_user_role_dropdown.'
            </select>
    </div>';
    
 
    
    $submission_user_role_select    =   array( esc_html__('User','wpresidence') ,esc_html__('Single Agent','wpresidence'),esc_html__('Agency','wpresidence'),esc_html__('Developer','wpresidence'));
    
    $visible_user_role     =    get_option('wp_estate_visible_user_role','');

    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Select user roles to display in register forms','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('This applies for all register forms. *Hold CTRL for multiple selection.','wpresidence').'</div>    
		<input type="hidden" name="visible_user_role" />
        <select id="visible_user_role" name="visible_user_role[]" multiple="multiple">';
        
        foreach($submission_user_role_select as $key=>$value){
            
            print '<option value ="'.$value.'" ';
            if(is_array($visible_user_role) && in_array($value, $visible_user_role)){
                print ' selected="selected" ';
            }
            print '>'.$value.'</option>';
        }
            
            
    print'
        </select>
    </div>';
    
    // show reviews on agent, agency, developer page
    $wp_estate_show_reviews_out    =   array( 'agent' => esc_html__('Agent','wpresidence') , 'agency' => esc_html__('Agency','wpresidence'),  'developer' => esc_html__('Developer','wpresidence') );
    
    $wp_estate_show_reviews     =    get_option('wp_estate_show_reviews_block','');
 
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Select Taxonomies, where you want to show review block','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('*Hold CTRL for multiple selection.','wpresidence').'</div>    
	
		<input type="hidden" name="show_reviews_block"  />	
        <select id="show_reviews_block" name="show_reviews_block[]" multiple="multiple">';
        
        foreach($wp_estate_show_reviews_out as $key=>$value){
            
            print '<option value ="'.$key.'" ';
            if(is_array($wp_estate_show_reviews) && in_array($key, $wp_estate_show_reviews)){
                print ' selected="selected" ';
            }
            print '>'.$value.'</option>';
        }
            
            
    print'
        </select>
    </div>';
    
    $admin_approves_reviews_symbol    = wpestate_dropdowns_theme_admin($cache_array,'admin_approves_reviews');

    
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Admin Should approve the reviews','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('If yes, the reviews can be found in comments section','wpresidence').'</div>    
            <select id="admin_approves_reviews" name="admin_approves_reviews">
                '.$admin_approves_reviews_symbol.'
            </select>
    </div>';
    
       
    $submission_user_role_select    =   array(esc_html__('Agent','wpresidence'),esc_html__('Agency','wpresidence'),esc_html__('Developer','wpresidence'));
    
    $admin_submission_user_role     =    get_option('wp_estate_admin_submission_user_role','');

    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Select wich user role the admin should approve?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Users will be automatically approved. *Hold CTRL for multiple selection.','wpresidence').'</div>    
	
		<input type="hidden" name="admin_submission_user_role" />
        <select id="admin_submission_user_role" name="admin_submission_user_role[]" multiple="multiple">';
        
        foreach($submission_user_role_select as $key=>$value){
            
            print '<option value ="'.$value.'" ';
            if(is_array($admin_submission_user_role) && in_array($value, $admin_submission_user_role)){
                print ' selected="selected" ';
            }
            print '>'.$value.'</option>';
        }
            
            
    print'
        </select>
    </div>';
    
       $enable_user_pass_symbol    = wpestate_dropdowns_theme_admin($cache_array,'enable_user_pass');

    
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Users can type the password on registration form','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('If no, users will get the auto generated password via email','wpresidence').'</div>    
            <select id="enable_user_pass" name="enable_user_pass">
                '.$enable_user_pass_symbol.'
            </select>
    </div>';
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif; // end   User Role Settings


if( !function_exists('new_wpestate_theme_admin_logos_favicon') ):
function new_wpestate_theme_admin_logos_favicon(){
    $cache_array                    =   array('yes','no');
    $social_array                   =   array('no','yes');
    $logo_image                     =   esc_html( get_option('wp_estate_logo_image','') );
    //$footer_logo_image              =   esc_html( get_option('wp_estate_footer_logo_image','') );
    $mobile_logo_image              =   esc_html( get_option('wp_estate_mobile_logo_image','') );
    $favicon_image                  =   esc_html( get_option('wp_estate_favicon_image','') );
    
    
  
    
    
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Your Favicon','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Upload site favicon in .ico, .png, .jpg or .gif format','wpresidence').'</div>    
            <input id="favicon_image" type="text" size="36" name="favicon_image" value="'.$favicon_image.'" />
            <input id="favicon_image_button" type="button"  class="upload_button button" value="'.esc_html__('Upload Favicon','wpresidence').'" />
       
    </div>';   
     
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('To add Retina logo versions, create retina logo, add _2x at the end of name of the original file (for ex logo_2x.jpg) and upload it in the same uploads folder as the non retina logo.','wpresidence').'</div>
    </div>';
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Your Logo','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('We will use the image at the uploaded size. So make sure it fits your design. If you add images directly into the input fields (without Upload button) please use the full image path. For ex: http://www.wpresidence..... . If you use the "upload"  button use also "Insert into Post" button from the pop up window.','wpresidence').'</div>    
            <input id="logo_image" type="text" size="36" name="logo_image" value="'.$logo_image.'" />
            <input id="logo_image_button" type="button"  class="upload_button button" value="'.esc_html__('Upload Logo','wpresidence').'" /></br>
            '.'
       
    </div>';
    $stikcy_logo_image    =   esc_html( get_option('wp_estate_stikcy_logo_image','') );
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Your Sticky Logo','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('If you add images directly into the input fields (without Upload button) please use the full image path. For ex: http://www.wpresidence..... . If you use the "upload"  button use also "Insert into Post" button from the pop up window.','wpresidence').'</div>    
            <input id="stikcy_logo_image" type="text" size="36" name="stikcy_logo_image" value="'.$stikcy_logo_image.'" />
            <input id="stikcy_logo_image_button" type="button"  class="upload_button button" value="'.esc_html__('Upload Logo','wpresidence').'" /></br>
            '.'
       
    </div>';
    
    $transparent_logo_image    =   esc_html( get_option('wp_estate_transparent_logo_image','') );
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Your Transparent Header Logo','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('If you add images directly into the input fields (without Upload button) please use the full image path. For ex: http://www.wpresidence..... . If you use the "upload"  button use also "Insert into Post" button from the pop up window.','wpresidence').'</div>    
            <input id="transparent_logo_image" type="text" size="36" name="transparent_logo_image" value="'.$transparent_logo_image.'" />
            <input id="transparent_logo_image_button" type="button"  class="upload_button button" value="'.esc_html__('Upload Logo','wpresidence').'" /></br>
            '.'
       
    </div>';
     
     
    $logo_margin                =   intval( get_option('wp_estate_logo_margin','') ); 
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Margin Top for logo','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Add logo margin top number.','wpresidence').'</div>    
        <input type="text" id="logo_margin" name="logo_margin" value="'.$logo_margin.'"> 
    </div>';
        
     /* 
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Retina Logo','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Retina ready logo (add _2x after the name. For ex logo_2x.jpg) ','wpresidence').'</div>    
            <input id="footer_logo_image" type="text" size="36" name="footer_logo_image" value="'.$footer_logo_image.'" />
            <input id="footer_logo_image_button" type="button"  class="upload_button button" value="'.esc_html__('Upload Logo','wpresidence').'" />
       
    </div>';
     */
      
    
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Mobile/Tablets Logo','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Upload mobile logo in jpg or png format.','wpresidence').'</div>    
            <input id="mobile_logo_image" type="text" size="36" name="mobile_logo_image" value="'.$mobile_logo_image.'" />
            <input id="mobile_logo_image_button" type="button"  class="upload_button button" value="'.esc_html__('Upload Logo','wpresidence').'" />
       
    </div>';
      
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
       
}
endif;


if( !function_exists('new_wpestate_header_settings') ):
function new_wpestate_header_settings(){
    $cache_array                =   array('yes','no');
     
    $show_top_bar_user_menu_symbol      = wpestate_dropdowns_theme_admin($cache_array,'show_top_bar_user_menu');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show top bar widget menu ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Enable or disable top bar widget area.','wpresidence').'</div>    
       <select id="show_top_bar_user_menu" name="show_top_bar_user_menu">
            '.$show_top_bar_user_menu_symbol.'
        </select>
    </div>';
    
    $show_top_bar_user_menu_mobile_symbol      = wpestate_dropdowns_theme_admin($cache_array,'show_top_bar_user_menu_mobile');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show top bar on mobile devices?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Enable or disable top bar on mobile devices','wpresidence').'</div>    
       <select id="show_top_bar_user_menu_mobile" name="show_top_bar_user_menu_mobile">
            '.$show_top_bar_user_menu_mobile_symbol.'
        </select>
    </div>';
    
    $show_top_bar_user_login_symbol     = wpestate_dropdowns_theme_admin($cache_array,'show_top_bar_user_login');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show user login menu in header ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Enable or disable user login menu in header.','wpresidence').'</div>    
        <select id="show_top_bar_user_login" name="show_top_bar_user_login">
            '.$show_top_bar_user_login_symbol.'
        </select>
    </div>';
    
    $show_submit_symbol     = wpestate_dropdowns_theme_admin($cache_array,'show_submit');
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Show submit property button in header?','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Submit property will only work with theme register/login.','wpresidence').'</div>    
            <select id="show_submit" name="show_submit">
                '.$show_submit_symbol.'
            </select>
        </div>';
    
          
    $cache_array                =   array('no','yes');
    $header_transparent_select  =   wpestate_dropdowns_theme_admin($cache_array,'header_transparent');
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Global transparent header?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Enable or disable the use of transparent header globally.','wpresidence').'</div>    
        <select id="header_transparent" name="header_transparent">
            '.$header_transparent_select.'
        </select>
    </div>';
    
    
    $header_array_logo  =   array(
                            'type1',
                            'type2',
                            'type3',
                            'type4',
                            'type5',
                        );
    $logo_header_select   = wpestate_dropdowns_theme_admin($header_array_logo,'logo_header_type');

    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Header Type?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Select header type.Header type 4 will NOT work with half map property list template.','wpresidence').'</div>    
        <select id="logo_header_type" name="logo_header_type">
            '.$logo_header_select.'
        </select>
    </div>';
    
    
    $header_array_logo_align  =   array(
                            'left',
                            'center',
                            'right',
                        );
 
    
    $logo_header_align_select   = wpestate_dropdowns_theme_admin($header_array_logo_align,'logo_header_align');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Header Align(Logo Position)?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Select header alignment.Please note that there is no "center" align for type 3 and 4.','wpresidence').'</div>    
        <select id="logo_header_align" name="logo_header_align">
            '.$logo_header_align_select.'
        </select>
    </div>';
           
    
    $text_header_align_select   = wpestate_dropdowns_theme_admin($header_array_logo_align,'text_header_align');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Header 3&4 Text Align?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Select a text alignment for header 3&4.','wpresidence').'</div>    
        <select id="text_header_align" name="text_header_align">
            '.$text_header_align_select.'
        </select>
    </div>';
    
    
    
    $cache_array                =   array('no','yes');
    $wide_header_select  =   wpestate_dropdowns_theme_admin($cache_array,'wide_header');
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Wide Header ? ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('make the header 100%.','wpresidence').'</div>    
        <select id="wide_header" name="wide_header">
            '.$wide_header_select.'
        </select>
    </div>';
    
    
    $header_array   =   array(
                            'none',
                            'image',
                            'theme slider',
                            'revolution slider',
                            'google map'
                            );
    $header_select   = wpestate_dropdowns_theme_admin_with_key($header_array,'header_type');

    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Media Header Type?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Select what media header to use globally.','wpresidence').'</div>    
        <select id="header_type" name="header_type">
            '.$header_select.'
        </select>
    </div>';
       
    
    
   
          
   
    $global_revolution_slider   =   get_option('wp_estate_global_revolution_slider','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Global Revolution Slider','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('If media header is set to Revolution Slider, type the slider name and save.','wpresidence').'</div>    
        <input type="text" id="global_revolution_slider" name="global_revolution_slider" value="'.$global_revolution_slider.'">   
    </div>';
    
    
    $global_header              =   get_option('wp_estate_global_header','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Global Header Static Image','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('If media header is set to image, add the image below. ','wpresidence').'</div>    
        <input id="global_header" type="text" size="36" name="global_header" value="'.$global_header.'" />
        <input id="global_header_button" type="button"  class="upload_button button" value="'.esc_html__('Upload Header Image','wpresidence').'" />
    </div>';
    
    $cache_array                =   array('yes','no');
    $paralax_header_select      =   wpestate_dropdowns_theme_admin($cache_array,'paralax_header');
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Parallax efect for image/video header media ? ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Enable parallax efect for image/video media header.','wpresidence').'</div>    
        <select id="paralax_header" name="paralax_header">
            '.$paralax_header_select.'
        </select>
    </div>';
 
    
    
    
    $header5_info_widget1_icon   =   get_option('wp_estate_header5_info_widget1_icon','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Header 5 - Info widget1 - icon ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Header 5 - Info widget1 - icon. Ex: fa fa-phone ','wpresidence').'</div>    
        <input type="text" id="header5_info_widget1_icon" name="header5_info_widget1_icon" value="'.$header5_info_widget1_icon.'">   
    </div>';
    
    $header5_info_widget1_text1   =   get_option('wp_estate_header5_info_widget1_text1','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Header 5 - Info widget2 - First line of text ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Header 5 - Info widget2 - First line of text','wpresidence').'</div>    
        <input type="text" id="header5_info_widget1_text1" name="header5_info_widget1_text1" value="'.$header5_info_widget1_text1.'">   
    </div>';
    
    $header5_info_widget1_text2   =   get_option('wp_estate_header5_info_widget1_text2','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Header 5 - Info widget2 - Second line of text ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Header 5 - Info widget1 - Second line of text','wpresidence').'</div>    
        <input type="text" id="header5_info_widget1_text2" name="header5_info_widget1_text2" value="'.$header5_info_widget1_text2.'">   
    </div>';

    
    $header5_info_widget2_icon   =   get_option('wp_estate_header5_info_widget2_icon','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Header 5 - Info widget2 - icon ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Header 5 - Info widget2 - icon.  Ex: fa fa-phone','wpresidence').'</div>    
        <input type="text" id="header5_info_widget2_icon" name="header5_info_widget2_icon" value="'.$header5_info_widget2_icon.'">   
    </div>';
    
    $header5_info_widget2_text1   =   get_option('wp_estate_header5_info_widget2_text1','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Header 5 - Info widget 1 - First line of text ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Header 5 - Info widget 1 - First line of text','wpresidence').'</div>    
        <input type="text" id="header5_info_widget2_text1" name="header5_info_widget2_text1" value="'.$header5_info_widget2_text1.'">   
    </div>';
    
    $header5_info_widget2_text2   =   get_option('wp_estate_header5_info_widget2_text2','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Header 5 - Info widget 2 - Second line of text ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Header 5 - Info widget 2  - Second line of text','wpresidence').'</div>    
        <input type="text" id="header5_info_widget2_text2" name="header5_info_widget2_text2" value="'.$header5_info_widget2_text2.'">   
    </div>';
    
    
 
    $wp_estate_header5_info_widget3_icon   =   get_option('wp_estate_header5_info_widget3_icon','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Header 5 - Info widget 3 - icon ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Header 5 - Info widget 3 - icon. Ex: fa fa-phone','wpresidence').'</div>    
        <input type="text" id="header5_info_widget3_icon" name="header5_info_widget3_icon" value="'.$wp_estate_header5_info_widget3_icon.'">   
    </div>';
    
    $header5_info_widget3_text1   =   esc_html(get_option('wp_estate_header5_info_widget3_text1',''));
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Header 5 - Info widget 3 - First line of text ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Header 5 - Info widget 3 - First line of text','wpresidence').'</div>    
        <input type="text" id="header5_info_widget3_text1" name="header5_info_widget3_text1" value="'.$header5_info_widget3_text1.'">   
    </div>';
    
    $header5_info_widget3_text2   =   get_option('wp_estate_header5_info_widget3_text2','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Header 5 - Info widget 3 - Second line of text ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Header 5 - Info widget 3 - Second line of text','wpresidence').'</div>    
        <input type="text" id="header5_info_widget3_text2" name="header5_info_widget3_text2" value="'.$header5_info_widget3_text2.'">   
    </div>';
    
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
       
}
endif;



if( !function_exists('new_wpestate_footer_settings') ):
function new_wpestate_footer_settings(){
    //wide_footer
    //show_footer
    //show_footer_copy
    //footer_type
    
    
    $cache_array            =   array('yes','no');
    $cache_array1           =   array('no','yes');
    $show_footer_select     =   wpestate_dropdowns_theme_admin($cache_array,'show_footer');
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show Footer ? ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Show Footer ?','wpresidence').'</div>    
        <select id="show_footer" name="show_footer">
            '.$show_footer_select.'
        </select>
    </div>';
    
    $show_show_footer_copy_select  =   wpestate_dropdowns_theme_admin($cache_array,'show_footer_copy');
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show Footer Copyright Area? ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Show Footer Copyright Area?.','wpresidence').'</div>    
        <select id="show_footer_copy" name="show_footer_copy">
            '.$show_show_footer_copy_select.'
        </select>
    </div>';
    
    $show_sticky_footer_select  =   wpestate_dropdowns_theme_admin($cache_array1,'show_sticky_footer');
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Use Sticky Footer? ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Use Sticky Footer?','wpresidence').'</div>    
        <select id="show_sticky_footer" name="show_sticky_footer">
            '.$show_sticky_footer_select.'
        </select>
    </div>';
    
    
    
    $copyright_message          =   esc_html (stripslashes( get_option('wp_estate_copyright_message','') ) );   
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Copyright Message','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Type here the copyright message that will appear in footer.','wpresidence').'</div>    
        <textarea cols="57" rows="2" id="copyright_message" name="copyright_message">'.$copyright_message.'</textarea></td>  
    </div>';
    
    $footer_background          =   get_option('wp_estate_footer_background','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Background for Footer','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Insert background footer image below.','wpresidence').'</div>    
        <input id="footer_background" type="text" size="36" name="footer_background" value="'.$footer_background.'" />
        <input id="footer_background_button" type="button"  class="upload_button button" value="'.esc_html__('Upload Background Image for Footer','wpresidence').'" />
                 
    </div>';
    

    $repeat_array=array('repeat','repeat x','repeat y','no repeat');
    $repeat_footer_back_symbol  = wpestate_dropdowns_theme_admin($repeat_array,'repeat_footer_back');

    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Repeat Footer background ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Set repeat options for background footer image.','wpresidence').'</div>    
        <select id="repeat_footer_back" name="repeat_footer_back">
            '.$repeat_footer_back_symbol.'
        </select>     
    </div>';
    
    

    
    $cache_array                =   array('no','yes');
    $wide_footer_select  =   wpestate_dropdowns_theme_admin($cache_array,'wide_footer');
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Wide Footer ? ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('make the footer 100%.','wpresidence').'</div>    
        <select id="wide_footer" name="wide_footer">
            '.$wide_footer_select.'
        </select>
    </div>';
    
    
    
    
    $wide_array=array(
        "1"  =>     esc_html__("4 equal columns","wpresidence"),
        "2"  =>     esc_html__("3 equal columns","wpresidence"),
        "3"  =>     esc_html__("2 equal columns","wpresidence"),
        "4"  =>     esc_html__("100% width column","wpresidence"),
        "5"  =>     esc_html__("3 columns: 1/2 + 1/4 + 1/4","wpresidence"),
        "6"  =>     esc_html__("3 columns: 1/4 + 1/2 + 1/4","wpresidence"),
        "7"  =>     esc_html__("3 columns: 1/4 + 1/4 + 1/2","wpresidence"),
        "8"  =>     esc_html__("2 columns: 2/3 + 1/3","wpresidence"),
        "9"  =>     esc_html__("2 columns: 1/3 + 2/3","wpresidence"),
        );
    
    
    
    $footer_type_symbol   = wpestate_dropdowns_theme_admin_with_key($wide_array,'footer_type');
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Footer Type','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Footer Type','wpresidence').'</div>    
        <select id="footer_type" name="footer_type">
            '.$footer_type_symbol.'
        </select>
    </div>';
    
    
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
       
}
endif;




if( !function_exists('new_wpestate_export_settings') ):
function  new_wpestate_export_settings(){
          
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Export Theme Options','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Export Theme Options ','wpresidence').'</div>    
            <textarea  rows="15" style="width:100%;" id="export_theme_options" onclick="this.focus();this.select()" name="export_theme_options">'.wpestate_export_theme_options().'</textarea>
       
    </div>';
   
}
endif;


if( !function_exists('new_wpestate_import_options_tab') ):
function new_wpestate_import_options_tab(){
    
    if(isset($_POST['import_theme_options']) && $_POST['import_theme_options']!=''){
        
        $data='';
        if(function_exists('wpestate_return_imported_data')){
            $data =wpestate_return_imported_data();
        }
        
        if ($data !== false && !empty($data) && is_array($data)) {
            foreach($data as $key=>$value){
                update_option($key, $value);          
            }
        
            print'<div class="estate_option_row">
            <div class="label_option_row">'.esc_html__('Import Completed','wpresidence') .'</div>
            </div>';
            update_option('wp_estate_import_theme_options','') ;
   
        }else{
            print'<div class="estate_option_row">
            <div class="label_option_row">'.esc_html__('The inserted code is not a valid one','wpresidence') .'</div>
            </div>';
            update_option('wp_estate_import_theme_options','') ;
        }

    }else{
        print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Import Theme Options','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Import Theme Options ','wpresidence').'</div>    
            <textarea  rows="15" style="width:100%;" id="import_theme_options" name="import_theme_options"></textarea>
        </div>';
        print ' <div class="estate_option_row_submit">
        <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Import','wpresidence').'" />
        </div>';
    } 
               
}
endif;


if( !function_exists('new_wpestate_theme_contact_details') ):
function  new_wpestate_theme_contact_details (){
    
    $company_contact_image      =   esc_html( get_option('wp_estate_company_contact_image','') );
       
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Image for Contact Page','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Add the image for the contact page contact area. Minim 350px wide for a nice design. ','wpresidence').'</div>    
        <input id="company_contact_image" type="text" size="36" name="company_contact_image" value="'.$company_contact_image.'" />
        <input id="company_contact_image_button" type="button"  class="upload_button button" value="'.esc_html__('Upload Image','wpresidence').'" />
    </div>';
    
    
    $company_name               =   esc_html ( stripslashes(get_option('wp_estate_company_name','') ) );
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Company Name','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Company name for contact page','wpresidence').'</div>    
        <input id="company_name" type="text" size="36" name="company_name" value="'.$company_name.'" />
    </div>';
             
    $email_adr                  =   esc_html ( get_option('wp_estate_email_adr','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Email','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('company email','wpresidence').'</div>    
      <input id="email_adr" type="text" size="36" name="email_adr" value="'.$email_adr.'" />
    </div>';
    
    
    $duplicate_email_adr        =   esc_html ( get_option('wp_estate_duplicate_email_adr','') );   
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Duplicate Email','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Send all contact emails to','wpresidence').'</div>    
      <input id="duplicate_email_adr" type="text" size="36" name="duplicate_email_adr" value="'.$duplicate_email_adr.'" />
    </div>';
    
    $telephone_no               =   esc_html ( get_option('wp_estate_telephone_no','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Telephone','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Company phone number.','wpresidence').'</div>    
    <input id="telephone_no" type="text" size="36" name="telephone_no" value="'.$telephone_no.'" />
    </div>';
     
    $mobile_no                  =   esc_html ( get_option('wp_estate_mobile_no','') );
     print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Mobile','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('company mobile','wpresidence').'</div>    
    <input id="mobile_no" type="text" size="36" name="mobile_no" value="'.$mobile_no.'" />
    </div>';
     
    $fax_ac                     =   esc_html ( get_option('wp_estate_fax_ac','') );
     print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Fax','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('company fax','wpresidence').'</div>    
    <input id="fax_ac" type="text" size="36" name="fax_ac" value="'.$fax_ac.'" />
    </div>';
     
    $skype_ac                   =   esc_html ( get_option('wp_estate_skype_ac','') );
     print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Skype','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Company skype','wpresidence').'</div>    
    <input id="skype_ac" type="text" size="36" name="skype_ac" value="'.$skype_ac.'" />
    </div>';
    
    $co_address                 =   esc_html ( stripslashes( get_option('wp_estate_co_address','') ) );
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Company Address','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Type company address','wpresidence').'</div>    
        <textarea cols="57" rows="2" name="co_address" id="co_address">'.$co_address.'</textarea>
    </div>';
    
    $hq_latitude                    =   esc_html ( get_option('wp_estate_hq_latitude') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Contact Page - Company HQ Latitude','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Set company pin location for contact page template. Latitude must be a number (ex: 40.577906).','wpresidence').'</div>    
        <input  type="text" id="hq_latitude"  name="hq_latitude"   value="'.$hq_latitude.'"/>
    </div>'; 
        
    $hq_longitude                   =   esc_html ( get_option('wp_estate_hq_longitude') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Contact Page - Company HQ Longitude','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Set company pin location for contact page template. Longitude must be a number (ex: -74.155058).','wpresidence').'</div>    
        <input  type="text" id="hq_longitude" name="hq_longitude"  value="'.$hq_longitude.'"/>
    </div>';  
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
    
}
endif;


if( !function_exists('new_wpestate_theme_social_accounts') ):
function new_wpestate_theme_social_accounts(){
    
    $facebook_link              =   esc_html ( get_option('wp_estate_facebook_link','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Facebook Link','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Facebook page url, with https://','wpresidence').'</div>    
        <input id="facebook_link" type="text" size="36" name="facebook_link" value="'.$facebook_link.'" />
    </div>';
    
      
    $twitter_link               =   esc_html ( get_option('wp_estate_twitter_link','') );
      print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Twitter page link','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Twitter page link, with https://','wpresidence').'</div>    
       <input id="twitter_link" type="text" size="36" name="twitter_link" value="'.$twitter_link.'" />
    </div>';
      
      
    $google_link                =   esc_html ( get_option('wp_estate_google_link','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Google+ Link','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Google+ page link, with https://','wpresidence').'</div>    
       <input id="google_link" type="text" size="36" name="google_link" value="'.$google_link.'" />
    </div>';
      
    $linkedin_link              =   esc_html ( get_option('wp_estate_linkedin_link','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Linkedin Link','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__(' Linkedin page link, with https://','wpresidence').'</div>    
        <input id="linkedin_link" type="text" size="36" name="linkedin_link" value="'.$linkedin_link.'" />
    </div>';
      
    $pinterest_link             =   esc_html ( get_option('wp_estate_pinterest_link','') );  
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Pinterest Link','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Pinterest page link, with https://','wpresidence').'</div>    
        <input id="pinterest_link" type="text" size="36" name="pinterest_link" value="'.$pinterest_link.'" />
    </div>';
      
    $instagram_link             =   esc_html ( get_option('wp_estate_instagram_link','') );  
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Instagram Link','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Instagram page link, with https://','wpresidence').'</div>    
        <input id="instagram_link" type="text" size="36" name="instagram_link" value="'.$instagram_link.'" />
    </div>';      
          
    $zillow_api_key             =   esc_html ( get_option('wp_estate_zillow_api_key','') );  
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Zillow api key','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Zillow api key is required for Zillow Widget.','wpresidence').'</div>    
        <input id="zillow_api_key" type="text" size="36" name="zillow_api_key" value="'.$zillow_api_key.'" />
    </div>';
      
    
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;

if( !function_exists('new_wpestate_theme_social_login') ):
function new_wpestate_theme_social_login(){

    $social_array               =   array('no','yes');
   
    $facebook_login_select      = wpestate_dropdowns_theme_admin($social_array,'facebook_login');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Allow login via Facebook ? ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Enable or disable Facebook login. ','wpresidence').'</div>    
        <select id="facebook_login" name="facebook_login">
            '.$facebook_login_select.'
        </select>
    </div>';
    
    
    $facebook_api               =   esc_html ( get_option('wp_estate_facebook_api','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Facebook Api key','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Facebook Api key is required for Facebook login.','wpresidence').'</div>    
        <input id="facebook_api" type="text" size="36" name="facebook_api" value="'.$facebook_api.'" />
    </div>';
      
    
    $facebook_secret            =   esc_html ( get_option('wp_estate_facebook_secret','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Facebook Secret','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Facebook Secret is required for Facebook login.','wpresidence').'</div>    
        <input id="facebook_secret" type="text" size="36" name="facebook_secret" value="'.$facebook_secret.'" />
    </div>';
    
    $google_login_select        = wpestate_dropdowns_theme_admin($social_array,'google_login');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Allow login via Google ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Enable or disable Google login.','wpresidence').'</div>    
        <select id="google_login" name="google_login">
            '.$google_login_select.'
        </select>
    </div>';
    
    
     $google_oauth_api           =   esc_html ( get_option('wp_estate_google_oauth_api','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Google Oauth Api','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Google Oauth Api is required for Google Login','wpresidence').'</div>    
        <input id="google_oauth_api" type="text" size="36" name="google_oauth_api" value="'.$google_oauth_api.'" />
    </div>';
      
    $google_oauth_client_secret =   esc_html ( get_option('wp_estate_google_oauth_client_secret','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Google Oauth Client Secret','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Google Oauth Client Secret is required for Google Login.','wpresidence').'</div>    
        <input id="google_oauth_client_secret" type="text" size="36" name="google_oauth_client_secret" value="'.$google_oauth_client_secret.'" />
    </div>';
      
    $google_api_key             =   esc_html ( get_option('wp_estate_google_api_key','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Google api key','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Google api key is required for Google Login.','wpresidence').'</div>    
        <input id="google_api_key" type="text" size="36" name="google_api_key" value="'.$google_api_key.'" />
    </div>';
      
    $yahoo_login_select         = wpestate_dropdowns_theme_admin($social_array,'yahoo_login');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Allow login via Yahoo ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Enable or disable Yahoo login.','wpresidence').'</div>    
        <select id="yahoo_login" name="yahoo_login">
            '.$yahoo_login_select.'
        </select>
    </div>';
      
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';

}
endif;

if( !function_exists('new_wpestate_contact7') ):
function new_wpestate_contact7(){
    
    $contact_form_7_contact     = stripslashes( esc_html( get_option('wp_estate_contact_form_7_contact','') ) );
    $contact_form_7_agent       = stripslashes( esc_html( get_option('wp_estate_contact_form_7_agent','') ) );
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Contact form 7 code for agent','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Contact form 7 code for agent (ex: [contact-form-7 id="2725" title="contact me"])','wpresidence').'</div>    
         <input type="text" size="36" id="contact_form_7_agent" name="contact_form_7_agent" value="'.$contact_form_7_agent.'" />
    </div>';
    
      
  
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Contact form 7 code for contact page','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Contact form 7 code for contact page template (ex: [contact-form-7 id="2725" title="contact me"])','wpresidence').'</div>    
         <input type="text" size="36" id="contact_form_7_contact" name="contact_form_7_contact" value="'.$contact_form_7_contact.'" />
    </div>';
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////new_wpestate_theme_twitter_widget
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if( !function_exists('new_wpestate_theme_twitter_widget') ):
function new_wpestate_theme_twitter_widget(){
          
    $twitter_consumer_key       =   esc_html ( get_option('wp_estate_twitter_consumer_key','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Twitter consumer_key','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Twitter consumer_key is required for theme Twitter widget.','wpresidence').'</div>    
        <input id="twitter_consumer_key" type="text" size="36" name="twitter_consumer_key" value="'.$twitter_consumer_key.'" />
    </div>';
      
    $twitter_consumer_secret    =   esc_html ( get_option('wp_estate_twitter_consumer_secret','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Twitter Consumer Secret','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Twitter Consumer Secret is required for theme Twitter widget.','wpresidence').'</div>    
        <input id="twitter_consumer_secret" type="text" size="36" name="twitter_consumer_secret" value="'.$twitter_consumer_secret.'" />
    </div>';
      
    $twitter_access_token       =   esc_html ( get_option('wp_estate_twitter_access_token','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Twitter Access Token','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Twitter Access Token is required for theme Twitter widget.','wpresidence').'</div>    
        <input id="twitter_account" type="text" size="36" name="twitter_access_token" value="'.$twitter_access_token.'" />
    </div>';
      
    $twitter_access_secret      =   esc_html ( get_option('wp_estate_twitter_access_secret','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Twitter Access Secret','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Twitter Access Secret is required for theme Twitter widget.','wpresidence').'</div>    
        <input id="twitter_access_secret" type="text" size="36" name="twitter_access_secret" value="'.$twitter_access_secret.'" />
    </div>';
      
    
    $twitter_cache_time         =   intval   ( get_option('wp_estate_twitter_cache_time','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Twitter Cache Time','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Twitter Cache Time','wpresidence').'</div>    
       <input id="twitter_cache_time" type="text" size="36" name="twitter_cache_time" value="'.$twitter_cache_time.'" />
    </div>';

    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;//end new_wpestate_theme_twitter_widget



if( !function_exists('new_wpestate_appeareance') ):
function new_wpestate_appeareance(){
    $cache_array                =   array('yes','no');
  
    $wide_array=array(
            "1"  =>  esc_html__("wide","wpresidence"),
            "2"  =>  esc_html__("boxed","wpresidence")
         );
    $wide_status_symbol   = wpestate_dropdowns_theme_admin_with_key($wide_array,'wide_status');
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Wide or Boxed?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Choose the theme layout: wide or boxed.','wpresidence').'</div>    
        <select id="wide_status" name="wide_status">
            '.$wide_status_symbol.'
        </select>
    </div>';

    
    $prop_no                    =   intval   ( get_option('wp_estate_prop_no','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Properties List - Properties number per page','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Set how many properties to show per page in lists.','wpresidence').'</div>    
        <input type="text" id="prop_no" name="prop_no" value="'.$prop_no.'"> 
    </div>';
    

    
    $prop_list_slider = array( 
                "0"  =>  esc_html__("no ","wpresidence"),
                "1"  =>  esc_html__("yes","wpresidence")
                );
    $prop_unit_slider_symbol = wpestate_dropdowns_theme_admin_with_key($prop_list_slider,'prop_list_slider');
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Use Slider in Property Unit','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Enable / Disable the image slider in property unit (used in lists).','wpresidence').'</div>    
        <select id="prop_list_slider" name="prop_list_slider">
            '.$prop_unit_slider_symbol.'
        </select>  
    </div>';
     
    $prop_list_array=array(
               "1"  =>  esc_html__("standard ","wpresidence"),
               "2"  =>  esc_html__("half map","wpresidence")
            );
    $property_list_type_symbol   = wpestate_dropdowns_theme_admin_with_key($prop_list_array,'property_list_type');
   
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Property List Type for Taxonomy pages','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Select standard or half map style for property taxonomies pages.','wpresidence').'</div>    
        <select id="property_list_type" name="property_list_type">
            '.$property_list_type_symbol.'
        </select>
    </div>';
    
    
    
   
    $property_list_type_symbol_adv   = wpestate_dropdowns_theme_admin_with_key($prop_list_array,'property_list_type_adv');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Property List Type for Advanced Search','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Select standard or half map style for advanced search results page.','wpresidence').'</div>    
        <select id="property_list_type_adv" name="property_list_type_adv">
            '.$property_list_type_symbol_adv.'
        </select>
    </div>';
    
    
    
    
    $prop_unit_array    =   array(
                                'grid'    =>esc_html__('grid','wpresidence'),
                                'list'    => esc_html__('list','wpresidence')
                            );
    $prop_unit_select_view   = wpestate_dropdowns_theme_admin_with_key($prop_unit_array,'prop_unit');
    

    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Property List display(*global option)','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Select grid or list style for properties list pages.','wpresidence').'</div>    
        <select id="prop_unit" name="prop_unit">
            '.$prop_unit_select_view.'
        </select>
    </div>';
    
    $blog_sidebar_array=array('no sidebar','right','left');
    $agent_sidebar_pos_select     = wpestate_dropdowns_theme_admin($blog_sidebar_array,'agent_sidebar');

    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Agent Sidebar Position','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Where to show the sidebar in agent page.','wpresidence').'</div>    
       <select id="agent_sidebar" name="agent_sidebar">
            '.$agent_sidebar_pos_select.'
        </select>
    </div>';
    
    
    $agent_sidebar_name          =   esc_html ( get_option('wp_estate_agent_sidebar_name','') );
    $agent_sidebar_name_select='';
    foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) { 
        $agent_sidebar_name_select.='<option value="'.($sidebar['id'] ).'"';
            if($agent_sidebar_name==$sidebar['id']){ 
                $agent_sidebar_name_select.=' selected="selected"';
            }
        $agent_sidebar_name_select.=' >'.ucwords($sidebar['name']).'</option>';
    } 
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Agent page Sidebar','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('What sidebar to show in agent page.','wpresidence').'</div>    
       <select id="agent_sidebar_name" name="agent_sidebar_name">
            '.$agent_sidebar_name_select.'
        </select>
    </div>';
    
    
    $blog_sidebar_select ='';
    $blog_sidebar= esc_html ( get_option('wp_estate_blog_sidebar','') );
    $blog_sidebar_array=array('no sidebar','right','left');

    foreach($blog_sidebar_array as $value){
            $blog_sidebar_select.='<option value="'.$value.'"';
            if ($blog_sidebar==$value){
                    $blog_sidebar_select.='selected="selected"';
            }
            $blog_sidebar_select.='>'.$value.'</option>';
    }
    
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Blog Category/Archive Sidebar Position','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Where to show the sidebar for blog category/archive list.','wpresidence').'</div>    
        <select id="blog_sidebar" name="blog_sidebar">
            '.$blog_sidebar_select.'
        </select>
    </div>';
    
    
    
    $blog_sidebar_name          =   esc_html ( get_option('wp_estate_blog_sidebar_name','') );
   
    $blog_sidebar_name_select='';
    foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) { 
        $blog_sidebar_name_select.='<option value="'.($sidebar['id'] ).'"';
            if($blog_sidebar_name==$sidebar['id']){ 
               $blog_sidebar_name_select.=' selected="selected"';
            }
        $blog_sidebar_name_select.=' >'.ucwords($sidebar['name']).'</option>';
    } 
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Blog Category/Archive Sidebar','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('What sidebar to show for blog category/archive list.','wpresidence').'</div>    
        <select id="blog_sidebar_name" name="blog_sidebar_name">
            '.$blog_sidebar_name_select.'
        </select>
    </div>';
    
    
    $blog_unit_array    =   array(
                        'grid'    =>esc_html__('grid','wpresidence'),
                        'list'      => esc_html__('list','wpresidence')
                        );
    
    $blog_unit_select = wpestate_dropdowns_theme_admin_with_key($blog_unit_array,'blog_unit');
      
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Blog Category/Archive List type','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Select list or grid style for Blog Category/Archive list type.','wpresidence').'</div>    
        <select id="blog_unit" name="blog_unit">
            '.$blog_unit_select.'
        </select>
    </div>';
    
    
    
      
    
    
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;


if( !function_exists('new_wpestate_property_page_details') ):
function new_wpestate_property_page_details(){
    $sidebar_agent                  =   array('yes','no');
    $slider_type                    =   array('vertical','horizontal','full width header','gallery','multi image slider');
    $social_array                   =   array('no','yes');
    $content_type                   =   array('accordion','tabs');
   
    
    $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page_property_design.php'
            ));

    $global_property_page_template_options='<option value="">'.esc_html__('default','wpresidence').'</option>';
    $wp_estate_global_page_template               =   esc_html( get_option('wp_estate_global_property_page_template') );
    foreach($pages as $page){
        $global_property_page_template_options.='<option value="'.$page->ID.'"'; 
        if($wp_estate_global_page_template==$page->ID){
            $global_property_page_template_options.=' selected="selected" ';
        }
        $global_property_page_template_options.=' >'.$page->post_title.'</option>';       
    }
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Use a custom property page template','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Pick a custom property page template you made. ','wpresidence').'</div>    
        <select id="global_property_page_template" name="global_property_page_template">
            '.$global_property_page_template_options.'
        </select> 
    </div>';
    
    
    $blog_sidebar_array=array('no sidebar','right','left');
    $property_sidebar_pos_select     = wpestate_dropdowns_theme_admin($blog_sidebar_array,'property_sidebar');

    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Property Sidebar Position','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Where to show the sidebar in property page.','wpresidence').'</div>    
       <select id="property_sidebar" name="property_sidebar">
            '.$property_sidebar_pos_select.'
        </select>
    </div>';
    
    
    $property_sidebar_name          =   esc_html ( get_option('wp_estate_property_sidebar_name','') );
    $property_sidebar_name_select='';
    foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) { 
        $property_sidebar_name_select.='<option value="'.($sidebar['id'] ).'"';
            if($property_sidebar_name==$sidebar['id']){ 
                $property_sidebar_name_select.=' selected="selected"';
            }
        $property_sidebar_name_select.=' >'.ucwords($sidebar['name']).'</option>';
    } 
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Property page Sidebar','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('What sidebar to show in property page.','wpresidence').'</div>    
       <select id="property_sidebar_name" name="property_sidebar_name">
            '.$property_sidebar_name_select.'
        </select>
    </div>';
    
    
    
    
    $enable_global_property_page_agent_sidebar_symbol           =   wpestate_dropdowns_theme_admin($sidebar_agent,'global_property_page_agent_sidebar');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Add Agent on Sidebar','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Show agent and contact form on sidebar. ','wpresidence').'</div>    
        <select id="global_property_page_agent_sidebar" name="global_property_page_agent_sidebar">
            '.$enable_global_property_page_agent_sidebar_symbol.'
        </select> 
    </div>';
    
    $global_prpg_slider_type_symbol                             =   wpestate_dropdowns_theme_admin($slider_type,'global_prpg_slider_type');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Slider Type','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('What property slider type to show on property page.','wpresidence').'</div>    
        <select id="global_prpg_slider_type" name="global_prpg_slider_type">
            '.$global_prpg_slider_type_symbol.'
        </select> 
    </div>';
    
    $global_prpg_content_type_symbol                            =   wpestate_dropdowns_theme_admin($content_type,'global_prpg_content_type');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show Content as ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Select tabs or accordion style for property info.','wpresidence').'</div>    
        <select id="global_prpg_content_type" name="global_prpg_content_type">
            '.$global_prpg_content_type_symbol.'
        </select> 
    </div>';
    
    $walkscore_api                                              =   esc_html ( get_option('wp_estate_walkscore_api','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Walkscore APi Key','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Walkscore info doesn\'t show if you don\'t add the API.','wpresidence').'</div>    
        <input type="text" name="walkscore_api" id="walkscore_api" value="'.$walkscore_api.'"> 
    </div>';
    
    
    $show_graph_prop_page                                       =   wpestate_dropdowns_theme_admin($social_array,'show_graph_prop_page');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show Graph on Property Page','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Enable or disable the display of number of view by day graphic.','wpresidence').'</div>    
        <select id="show_graph_prop_page" name="show_graph_prop_page">
            '.$show_graph_prop_page.'
        </select> 
    </div>';
    
    $show_reviews_prop                                       =   wpestate_dropdowns_theme_admin($social_array,'show_reviews_prop');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show Reviews on Property Page','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Show Reviews on Property Page.','wpresidence').'</div>    
        <select id="show_reviews_prop" name="show_reviews_prop">
            '.$show_reviews_prop.'
        </select> 
    </div>';
  
    $enable_direct_mess                                      =   wpestate_dropdowns_theme_admin($social_array,'enable_direct_mess');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Enable Direct Message','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('If set to no you will need to delete Inbox page template.','wpresidence').'</div>    
        <select id="enable_direct_mess" name="enable_direct_mess">
            '.$enable_direct_mess.'
        </select> 
    </div>';
    
    
    $show_lightbox_contact                                       =   wpestate_dropdowns_theme_admin($social_array,'show_lightbox_contact');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show Contact Form on lightbox','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Enable or disable the contact form on lightbox.','wpresidence').'</div>    
        <select id="show_lightbox_contact" name="show_lightbox_contact">
            '.$show_lightbox_contact.'
        </select> 
    </div>';
    
    $crop_images_lightbox                                       =   wpestate_dropdowns_theme_admin($social_array,'crop_images_lightbox');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Crop Images on lightbox','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Images will have the same size. If set to no you will need to make sure that images are about the same size','wpresidence').'</div>    
        <select id="crop_images_lightbox" name="crop_images_lightbox">
            '.$crop_images_lightbox.'
        </select> 
    </div>';
   
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';  
}
endif;


if( !function_exists('wpestate_general_design_settings') ):
function wpestate_general_design_settings(){
    
    $main_grid_content_width                                              =   esc_html ( get_option('wp_estate_main_grid_content_width','') );
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Main Grid Width in px','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('This option defines the main content width. Default value is 1200px','wpresidence').'</div>    
        <input type="text" name="main_grid_content_width" id="main_grid_content_width" value="'.$main_grid_content_width.'"> 
    </div>';
    
    $main_content_width                                              =   esc_html ( get_option('wp_estate_main_content_width','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Content Width (In Percent)','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Using this option you can define the width of the content in percent.Sidebar will occupy the rest of the main content space.','wpresidence').'</div>    
        <input type="text" name="main_content_width" id="main_content_width" value="'.$main_content_width.'"> 
    </div>';
    

       
    $wp_estate_contentarea_internal_padding_top          = get_option('wp_estate_contentarea_internal_padding_top','');
    $wp_estate_contentarea_internal_padding_left         = get_option('wp_estate_contentarea_internal_padding_left','');
    $wp_estate_contentarea_internal_padding_bottom       = get_option('wp_estate_contentarea_internal_padding_bottom','');
    $wp_estate_contentarea_internal_padding_right        = get_option('wp_estate_contentarea_internal_padding_right','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Content Area Internal Padding','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Content Area Internal Padding (top,left,bottom,right) ','wpresidence').'</div>    
        <input  style="width:100px;min-width:100px;" type="text" id="wp_estate_contentarea_internal_padding_top" name="contentarea_internal_padding_top"  value="'.$wp_estate_contentarea_internal_padding_top.'"/> 
        <input  style="width:100px;min-width:100px;" type="text" id="wp_estate_contentarea_internal_padding_left" name="contentarea_internal_padding_left"  value="'.$wp_estate_contentarea_internal_padding_left.'"/> 
        <input  style="width:100px;min-width:100px;" type="text" id="wp_estate_contentarea_internal_padding_bottom" name="contentarea_internal_padding_bottom"  value="'.$wp_estate_contentarea_internal_padding_bottom.'"/> 
        <input  style="width:100px;min-width:100px;" type="text" id="wp_estate_contentarea_internal_padding_right" name="contentarea_internal_padding_right"  value="'.$wp_estate_contentarea_internal_padding_right.'"/> 
    </div>';
    
    
    $wp_estate_content_area_back_color             =  esc_html ( get_option('wp_estate_content_area_back_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Content Area Background Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Content Area Background Color','wpresidence').'</div>    
        <input type="text" name="content_area_back_color" value="'.$wp_estate_content_area_back_color.'" maxlength="7" class="inptxt" />
        <div id="content_area_back_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$wp_estate_content_area_back_color.';"></div></div>
    </div>';
    
    $yesno=array('yes','no');
    $enable_show_breadcrumbs           =   wpestate_dropdowns_theme_admin($yesno,'show_breadcrumbs');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show Breadcrumbs','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Show Breadcrumbs?','wpresidence').'</div>    
        <select id="show_breadcrumbs" name="show_breadcrumbs">
            '.$enable_show_breadcrumbs.'
        </select> 
    </div>';
    
        
    $border_radius_corner      = get_option('wp_estate_border_radius_corner','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Border Corner Radius','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Border Corner Radius for unit elements, like property unit, agent unit or blog unit etc','wpresidence').'</div>    
        <input  type="text" id="border_radius_corner" name="border_radius_corner"  value="'.$border_radius_corner.'"/> 
    </div>';
    
    
    $cssbox_shadow      = get_option('wp_estate_cssbox_shadow','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Box Shadow on elements like property unit ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Box Shadow on elements like property unit. Type none for no shadow or put the css values like  0px 2px 0px 0px rgba(227, 228, 231, 1) ','wpresidence').'</div>    
        <input  type="text" id="cssbox_shadow" name="cssbox_shadow"  value="'.$cssbox_shadow.'"/> 
    </div>';
    
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>'; 
    
}
endif;



if( !function_exists('new_wpestate_price_currency') ):
function new_wpestate_price_currency(){
    
     
    $custom_fields = get_option( 'wp_estate_multi_curr', true);     
    $current_fields='';
    
    $currency_symbol                =   esc_html( get_option('wp_estate_currency_symbol') );
    
    $where_currency_symbol_array    =   array('before','after');
    $where_currency_symbol          =   wpestate_dropdowns_theme_admin($where_currency_symbol_array,'where_currency_symbol');
   
    $enable_auto_symbol_array       =   array('yes','no');
    $enable_auto_symbol             =   wpestate_dropdowns_theme_admin($enable_auto_symbol_array,'auto_curency');
    
    
    $i=0;
    if( !empty($custom_fields)){    
        while($i< count($custom_fields) ){
            $current_fields.='
                <div class=field_row>
                <div    class="field_item"><strong>'.esc_html__('Currency Symbol','wpresidence').'</strong></br><input   type="text" name="add_curr_name[]"   value="'.$custom_fields[$i][0].'"  ></div>
                <div    class="field_item"><strong>'.esc_html__('Currency Label','wpresidence').'</strong></br><input  type="text" name="add_curr_label[]"   value="'.$custom_fields[$i][1].'"  ></div>
                <div    class="field_item"><strong>'.esc_html__('Currency Value','wpresidence').'</strong></br><input  type="text" name="add_curr_value[]"   value="'.$custom_fields[$i][2].'"  ></div>
                <div    class="field_item"><strong>'.esc_html__('Currency Position','wpresidence').'</strong></br><input  type="text" name="add_curr_order[]"   value="'.$custom_fields[$i][3].'"  ></div>
                
                <a class="deletefieldlink" href="#">'.esc_html__('delete','wpresidence').'</a>
            </div>';    
            $i++;
        }
    }
    
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Price - thousands separator','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Set the thousand separator for price numbers.','wpresidence').'</div>    
        <input type="text" name="prices_th_separator" id="prices_th_separator" value="'.  stripslashes ( get_option('wp_estate_prices_th_separator','') ).'"> 
    </div>';
 
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Currency symbol','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Set currency symbol for property price.','wpresidence').'</div>    
        <input  type="text" id="currency_symbol" name="currency_symbol"  value="'.$currency_symbol.'"/>
    </div>';
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Currency label - will appear on front end','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Set the currency label for multi-currency widget dropdown.','wpresidence').'</div>    
        <input  type="text" id="currency_label_main"  name="currency_label_main"   value="'. get_option('wp_estate_currency_label_main','').'" size="40"/>
    </div>';
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Where to show the currency symbol?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Set the position for the currency symbol.','wpresidence').'</div>    
        <select id="where_currency_symbol" name="where_currency_symbol">
            '.$where_currency_symbol.'
        </select> 
    </div>';

    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Enable auto loading of exchange rates from free.currencyconverterapi.com (1 time per day)?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Symbol must be set according to international standards. Complete list is here http://www.xe.com/iso4217.php.','wpresidence').'</div>    
        <select id="auto_curency" name="auto_curency">
            '.$enable_auto_symbol.'
        </select> 
    </div>';

 
    print'<div class="estate_option_row"> 
        <h3 style="margin-left:10px;width:100%;float:left;">'.esc_html__('Add Currencies for Multi Currency Widget.','wpresidence').'</h3>
     
        <div id="custom_fields">
             '.$current_fields.'
            <input type="hidden" name="is_custom_cur" value="1">   
        </div>

     
        <div class="add_curency">
            <div class="cur_explanations">'.esc_html__('Currency','wpresidence').'</div>
            <input  type="text" id="currency_name"  name="currency_name"   value=""/>
        
            <div class="cur_explanations">'.esc_html__('Currency label - will appear on front end','wpresidence').'</div>
            <input  type="text" id="currency_label"  name="currency_label"   value="" />   

            <div class="cur_explanations">'.esc_html__('Currency value compared with the base currency value.','wpresidence').'</div>
            <input  type="text" id="currency_value"  name="currency_value"   value="" />
           
            <div class="cur_explanations">'.esc_html__('Show currency before or after price - in front pages','wpresidence').'</div>
                <select id="where_cur" name="where_cur"  >
                    <option value="before"> before </option>
                    <option value="after">  after </option>
                </select>
        </div><br>
                     
         <a href="#" id="add_curency">'.esc_html__(' click to add currency','wpresidence').'</a><br>
     </div> ';
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;

if( !function_exists('new_wpestate_map_details') ):
function new_wpestate_map_details(){
    $cache_array                    =   array('yes','no');
    $cache_array2                   =   array('no','yes');
    $show_filter_map_symbol         =   wpestate_dropdowns_theme_admin($cache_array,'show_filter_map');
    $home_small_map_symbol          =   wpestate_dropdowns_theme_admin($cache_array,'home_small_map');
    $show_adv_search_symbol_map_close   =   wpestate_dropdowns_theme_admin($cache_array,'show_adv_search_map_close');
    
    
    $path=estate_get_pin_file_path(); 
   
    if ( file_exists ($path) && is_writable ($path) ){
    }else{
        print ' <div class="notice_file">'.esc_html__('the file Google map does NOT exist or is NOT writable','wpresidence').'</div>';
    }
    
    $readsys_symbol                 =   wpestate_dropdowns_theme_admin($cache_array,'readsys');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Use file reading for pins? ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Use file reading for pins? (*recommended for over 200 listings. Read the manual for diffrences between file and mysql reading)','wpresidence').'</div>    
        <select id="readsys" name="readsys">
            '.$readsys_symbol.'
        </select>
    </div>';
       
    
     
    $map_max_pins                 =   intval( get_option('wp_estate_map_max_pins') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Maximum number of pins to show on the map. ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('A high number will increase the response time and server load. Use a number that works for your current hosting situation. Put -1 for all pins.','wpresidence').'</div>    
        <input  type="text" id="map_max_pins" name="map_max_pins" class="regular-text" value="'.$map_max_pins.'"/>
  
    </div>';
    
    $ssl_map_symbol                 =   wpestate_dropdowns_theme_admin($cache_array,'ssl_map');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Use Google maps with SSL ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Set to Yes if you use SSL.','wpresidence').'</div>    
        <select id="ssl_map" name="ssl_map">
            '.$ssl_map_symbol.'
        </select>
    </div>';  

    $api_key                        =   esc_html( get_option('wp_estate_api_key') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Google Maps API KEY','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('The Google Maps JavaScript API v3 REQUIRES an API key to function correctly. Get an APIs Console key and post the code in Theme Options. You can get it from ','wpresidence').'<a href="https://developers.google.com/maps/documentation/javascript/tutorial#api_key" target="_blank">'.esc_html__('here','wpresidence').'</a></div>    
        <input  type="text" id="api_key" name="api_key" class="regular-text" value="'.$api_key.'"/>
    </div>'; 

    $general_latitude               =   esc_html( get_option('wp_estate_general_latitude') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Starting Point Latitude','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Applies for global header media with google maps. Add only numbers (ex: 40.577906).','wpresidence').'</div>    
    <input  type="text" id="general_latitude"  name="general_latitude"   value="'.$general_latitude.'"/>
    </div>'; 
    
    $general_longitude              =   esc_html( get_option('wp_estate_general_longitude') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Starting Point Longitude','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Applies for global header media with google maps. Add only numbers (ex: -74.155058).','wpresidence').'</div>    
    <input  type="text" id="general_longitude" name="general_longitude"  value="'.$general_longitude.'"/>
    </div>'; 
       
    $default_map_zoom               =   intval   ( get_option('wp_estate_default_map_zoom','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Default Map zoom (1 to 20)','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Applies for global header media with google maps, except advanced search results, properties list and taxonomies pages.','wpresidence').'</div>    
    <input type="text" id="default_map_zoom" name="default_map_zoom" value="'.$default_map_zoom.'">   
    </div>'; 
    
    $map_types = array('SATELLITE','HYBRID','TERRAIN','ROADMAP');
    $default_map_type_symbol               =   wpestate_dropdowns_theme_admin($map_types,'default_map_type');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Map Type','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('The type selected applies for Google Maps header. ','wpresidence').'</div>    
        <select id="default_map_type" name="default_map_type">
            '.$default_map_type_symbol.'
        </select> 
    </div>'; 
        
    $cache_symbol                   =   wpestate_dropdowns_theme_admin($cache_array,'cache');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Use Cache for Google maps ?(*cache will renew itself every 3h)','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('If set to yes, new property pins will update on the map every 3 hours.','wpresidence').'</div>    
        <select id="cache" name="cache">
            '.$cache_symbol.'
        </select>
    </div>'; 

    $pin_cluster_symbol             =   wpestate_dropdowns_theme_admin($cache_array,'pin_cluster');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Use Pin Cluster on map','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('If yes, it groups nearby pins in cluster.','wpresidence').'</div>    
        <select id="pin_cluster" name="pin_cluster">
            '.$pin_cluster_symbol.'
        </select>
    </div>'; 
       
    $zoom_cluster                   =   esc_html ( get_option('wp_estate_zoom_cluster ','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Maximum zoom level for Cloud Cluster to appear','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Pin cluster disappears when map zoom is less than the value set in here. ','wpresidence').'</div>    
        <input id="zoom_cluster" type="text" size="36" name="zoom_cluster" value="'.$zoom_cluster.'" />
    </div>';      
    
    $idx_symbol             =   wpestate_dropdowns_theme_admin($cache_array,'idx_enable');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Enable dsIDXpress to use the map','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Enable only if you activate dsIDXpres optional plugin. See help for details.','wpresidence').'</div>    
        <select id="idx_enable" name="idx_enable">
            '.$idx_symbol.'
        </select>
    </div>';     
    
    
    $geolocation_radius         =   esc_html ( get_option('wp_estate_geolocation_radius','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Geolocation Circle over map (in meters)','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Controls circle radius value for user geolocation pin. Type only numbers (ex: 400).','wpresidence').'</div>    
       <input id="geolocation_radius" type="text" size="36" name="geolocation_radius" value="'.$geolocation_radius.'" />
    </div>'; 
       
    $min_height                     =   intval   ( get_option('wp_estate_min_height','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Height of the Google Map when closed','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Applies for header google maps when set as global header media type.','wpresidence').'</div>    
       <input id="min_height" type="text" size="36" name="min_height" value="'.$min_height.'" />
    </div>';  
      
    $max_height                     =   intval   ( get_option('wp_estate_max_height','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Height of Google Map when open','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Applies for header google maps when set as global header media type.','wpresidence').'</div>    
       <input id="max_height" type="text" size="36" name="max_height" value="'.$max_height.'" />
    </div>'; 
      
    $keep_min_symbol                    =   wpestate_dropdowns_theme_admin($cache_array2,'keep_min');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Force Google Map at the "closed" size ? ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Applies for header google maps when set as global header media type, except property page.','wpresidence').'</div>    
        <select id="keep_min" name="keep_min">
            '.$keep_min_symbol.'
        </select>
    </div>'; 
    
    $keep_max_symbol                    =   wpestate_dropdowns_theme_admin($cache_array2,'keep_max');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Force Google Map at the full screen size ? ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Applies for header google maps when set as global header media type, except property page.','wpresidence').'</div>    
        <select id="keep_max" name="keep_max">
            '.$keep_max_symbol.'
        </select>
    </div>'; 
     
    
    $show_g_search_symbol               =   wpestate_dropdowns_theme_admin($cache_array2,'show_g_search');

    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show Google Search over Map?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Enable or disable the Google Maps search bar.','wpresidence').'</div>    
        <select id="show_g_search" name="show_g_search">
            '.$show_g_search_symbol.'
        </select>
    </div>'; 
     
    $map_style  =   esc_html ( get_option('wp_estate_map_style','') );    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Style for Google Map. Use https://snazzymaps.com/ to create styles','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Copy/paste below the custom map style code.','wpresidence').'</div>    
        <textarea id="map_style" style="width:100%;height:350px;" name="map_style">'.stripslashes($map_style).'</textarea>
    </div>'; 

     print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';

}   
endif;






    
if( !function_exists('new_wpestate_custom_colors') ):      
function new_wpestate_custom_colors(){
    $menu_items_color               =  esc_html ( get_option('wp_estate_menu_items_color','') );
    $agent_color                    =  esc_html ( get_option('wp_estate_agent_color','') );
    $color_scheme_array=array('no','yes');
    $color_scheme_select   = wpestate_dropdowns_theme_admin($color_scheme_array,'color_scheme');
    /*
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Use Custom Colors ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('You must set YES and save for your custom colors to apply.','wpresidence').'</div>    
        <select id="color_scheme" name="color_scheme">
            '.$color_scheme_select.'
         </select>
    </div>'; 
       */
    $main_color                     =  esc_html ( get_option('wp_estate_main_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Main Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Main Color','wpresidence').'</div>    
        <input type="text" name="main_color" maxlength="7" class="inptxt " value="'.$main_color.'"/>
        <div id="main_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$main_color.';"  ></div></div>
    </div>';
    
    $second_color                   = esc_html ( get_option('wp_estate_second_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Second Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Second Color','wpresidence').'</div>    
        <input type="text" name="second_color" maxlength="7" class="inptxt " value="'.$second_color.'"/>
        <div id="second_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$second_color.';"  ></div></div>
    </div>';
    
    
    $background_color               =  esc_html ( get_option('wp_estate_background_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Background Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Background Color','wpresidence').'</div>    
        <input type="text" name="background_color" maxlength="7" class="inptxt " value="'.$background_color.'"/>
        <div id="background_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$background_color.';"  ></div></div>
    </div>'; 

    $content_back_color             =  esc_html ( get_option('wp_estate_content_back_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Content Background Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Content Background Color','wpresidence').'</div>    
        <input type="text" name="content_back_color" value="'.$content_back_color.'" maxlength="7" class="inptxt" />
        <div id="content_back_color" class="colorpickerHolder" ><div class="sqcolor"  style="background-color:#'.$content_back_color.';" ></div></div>
    </div>'; 
        
    $breadcrumbs_font_color         =  esc_html ( get_option('wp_estate_breadcrumbs_font_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Breadcrumbs, Meta and Second Line Font Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Breadcrumbs, Meta and Second Line Font Color','wpresidence').'</div>    
        <input type="text" name="breadcrumbs_font_color" value="'.$breadcrumbs_font_color.'" maxlength="7" class="inptxt" />
        <div id="breadcrumbs_font_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$breadcrumbs_font_color.';" ></div></div>
    </div>';  
    
    
    $font_color                     =  esc_html ( get_option('wp_estate_font_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Font Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Font Color','wpresidence').'</div>    
        <input type="text" name="font_color" value="'.$font_color.'" maxlength="7" class="inptxt" />
        <div id="font_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$font_color.';" ></div></div>
    </div>';
       
    $link_color                     =  esc_html ( get_option('wp_estate_link_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Link Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Link Color','wpresidence').'</div>    
        <input type="text" name="link_color" value="'.$link_color.'" maxlength="7" class="inptxt" />
        <div id="link_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$link_color.';" ></div></div>
    </div>';
  
    $headings_color                 =  esc_html ( get_option('wp_estate_headings_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Headings Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Headings Color','wpresidence').'</div>    
        <input type="text" name="headings_color" value="'.$headings_color.'" maxlength="7" class="inptxt" />
        <div id="headings_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$headings_color.';" ></div></div>
    </div>';
        
        
    $footer_back_color              =  esc_html ( get_option('wp_estate_footer_back_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Footer Background Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Footer Background Color','wpresidence').'</div>    
        <input type="text" name="footer_back_color" value="'.$footer_back_color.'" maxlength="7" class="inptxt" />
        <div id="footer_back_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$footer_back_color.';" ></div></div>
    </div>';
        
    $footer_font_color              =  esc_html ( get_option('wp_estate_footer_font_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Footer Font Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Footer Font Color','wpresidence').'</div>    
        <input type="text" name="footer_font_color" value="'.$footer_font_color.'" maxlength="7" class="inptxt" />
        <div id="footer_font_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$footer_font_color.';" ></div></div>
    </div>';
        
    $footer_copy_color              =  esc_html ( get_option('wp_estate_footer_copy_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Footer Copyright Font Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Footer Copyright Font Color','wpresidence').'</div>    
        <input type="text" name="footer_copy_color" value="'.$footer_copy_color.'" maxlength="7" class="inptxt" />
        <div id="footer_copy_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$footer_copy_color.';" ></div></div>
    </div>';
        
  
    $footer_copy_back_color              =  esc_html ( get_option('wp_estate_footer_copy_back_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Footer Copyright Area Background Font Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Footer Copyright Area Background Font Color','wpresidence').'</div>    
        <input type="text" name="footer_copy_back_color" value="'.$footer_copy_back_color.'" maxlength="7" class="inptxt" />
        <div id="footer_copy_back_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$footer_copy_back_color.';" ></div></div>
    </div>';
              
        
    $box_content_back_color         =  esc_html ( get_option('wp_estate_box_content_back_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Boxed Content Background Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Boxed Content Background Color','wpresidence').'</div>    
        <input type="text" name="box_content_back_color" value="'.$box_content_back_color.'" maxlength="7" class="inptxt" />
        <div id="box_content_back_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$box_content_back_color.';"></div></div>
    </div>';
        
    $box_content_border_color       =  esc_html ( get_option('wp_estate_box_content_border_color','') );
     print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Border Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Border Color','wpresidence').'</div>    
        <input type="text" name="box_content_border_color" value="'.$box_content_border_color.'" maxlength="7" class="inptxt" />
        <div id="box_content_border_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$box_content_border_color.';"></div></div>
    </div>';
       
    $hover_button_color             =  esc_html ( get_option('wp_estate_hover_button_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Hover Button Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Hover Button Color','wpresidence').'</div>    
        <input type="text" name="hover_button_color" value="'.$hover_button_color.'" maxlength="7" class="inptxt" />
        <div id="hover_button_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$hover_button_color.';"></div></div>
    </div>';
     
    $map_controls_back            =  esc_html ( get_option('wp_estate_map_controls_back','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Map Controls Background Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Map Controls Background Color','wpresidence').'</div>    
        <input type="text" name="map_controls_back" value="'.$map_controls_back.'" maxlength="7" class="inptxt" />
        <div id="map_controls_back" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$map_controls_back.';"></div></div>
    </div>';
    
    $map_controls_font_color            =  esc_html ( get_option('wp_estate_map_controls_font_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Map Controls Font Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Map Controls Font Color','wpresidence').'</div>    
        <input type="text" name="map_controls_font_color" value="'.$map_controls_font_color.'" maxlength="7" class="inptxt" />
        <div id="map_controls_font_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$map_controls_font_color.';"></div></div>
    </div>';
        
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';  
     
}
endif;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///  new_wpestate_custom_css
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if( !function_exists('new_wpestate_custom_css') ):      
function new_wpestate_custom_css(){
    $custom_css                     =  esc_html ( stripslashes( get_option('wp_estate_custom_css','') ) );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Custom Css','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Overwrite theme css using custom css.','wpresidence').'</div>    
        <textarea cols="57" rows="15" name="custom_css" id="custom_css">'.$custom_css.'</textarea>
    </div>';
        
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';  

}
endif;//end new_wpestate_custom_css


if( !function_exists('new_wpestate_custom_fonts') ):    
function   new_wpestate_custom_fonts(){
   /*   $google_fonts_array = array(                          
                                                            "Abel" => "Abel",
                                                            "Abril Fatface" => "Abril Fatface",
                                                            "Aclonica" => "Aclonica",
                                                            "Acme" => "Acme",
                                                            "Actor" => "Actor",
                                                            "Adamina" => "Adamina",
                                                            "Advent Pro" => "Advent Pro",
                                                            "Aguafina Script" => "Aguafina Script",
                                                            "Aladin" => "Aladin",
                                                            "Aldrich" => "Aldrich",
                                                            "Alegreya" => "Alegreya",
                                                            "Alegreya SC" => "Alegreya SC",
                                                            "Alex Brush" => "Alex Brush",
                                                            "Alfa Slab One" => "Alfa Slab One",
                                                            "Alice" => "Alice",
                                                            "Alike" => "Alike",
                                                            "Alike Angular" => "Alike Angular",
                                                            "Allan" => "Allan",
                                                            "Allerta" => "Allerta",
                                                            "Allerta Stencil" => "Allerta Stencil",
                                                            "Allura" => "Allura",
                                                            "Almendra" => "Almendra",
                                                            "Almendra SC" => "Almendra SC",
                                                            "Amaranth" => "Amaranth",
                                                            "Amatic SC" => "Amatic SC",
                                                            "Amethysta" => "Amethysta",
                                                            "Andada" => "Andada",
                                                            "Andika" => "Andika",
                                                            "Angkor" => "Angkor",
                                                            "Annie Use Your Telescope" => "Annie Use Your Telescope",
                                                            "Anonymous Pro" => "Anonymous Pro",
                                                            "Antic" => "Antic",
                                                            "Antic Didone" => "Antic Didone",
                                                            "Antic Slab" => "Antic Slab",
                                                            "Anton" => "Anton",
                                                            "Arapey" => "Arapey",
                                                            "Arbutus" => "Arbutus",
                                                            "Architects Daughter" => "Architects Daughter",
                                                            "Arimo" => "Arimo",
                                                            "Arizonia" => "Arizonia",
                                                            "Armata" => "Armata",
                                                            "Artifika" => "Artifika",
                                                            "Arvo" => "Arvo",
                                                            "Asap" => "Asap",
                                                            "Asset" => "Asset",
                                                            "Astloch" => "Astloch",
                                                            "Asul" => "Asul",
                                                            "Atomic Age" => "Atomic Age",
                                                            "Aubrey" => "Aubrey",
                                                            "Audiowide" => "Audiowide",
                                                            "Average" => "Average",
                                                            "Averia Gruesa Libre" => "Averia Gruesa Libre",
                                                            "Averia Libre" => "Averia Libre",
                                                            "Averia Sans Libre" => "Averia Sans Libre",
                                                            "Averia Serif Libre" => "Averia Serif Libre",
                                                            "Bad Script" => "Bad Script",
                                                            "Balthazar" => "Balthazar",
                                                            "Bangers" => "Bangers",
                                                            "Basic" => "Basic",
                                                            "Battambang" => "Battambang",
                                                            "Baumans" => "Baumans",
                                                            "Bayon" => "Bayon",
                                                            "Belgrano" => "Belgrano",
                                                            "Belleza" => "Belleza",
                                                            "Bentham" => "Bentham",
                                                            "Berkshire Swash" => "Berkshire Swash",
                                                            "Bevan" => "Bevan",
                                                            "Bigshot One" => "Bigshot One",
                                                            "Bilbo" => "Bilbo",
                                                            "Bilbo Swash Caps" => "Bilbo Swash Caps",
                                                            "Bitter" => "Bitter",
                                                            "Black Ops One" => "Black Ops One",
                                                            "Bokor" => "Bokor",
                                                            "Bonbon" => "Bonbon",
                                                            "Boogaloo" => "Boogaloo",
                                                            "Bowlby One" => "Bowlby One",
                                                            "Bowlby One SC" => "Bowlby One SC",
                                                            "Brawler" => "Brawler",
                                                            "Bree Serif" => "Bree Serif",
                                                            "Bubblegum Sans" => "Bubblegum Sans",
                                                            "Buda" => "Buda",
                                                            "Buenard" => "Buenard",
                                                            "Butcherman" => "Butcherman",
                                                            "Butterfly Kids" => "Butterfly Kids",
                                                            "Cabin" => "Cabin",
                                                            "Cabin Condensed" => "Cabin Condensed",
                                                            "Cabin Sketch" => "Cabin Sketch",
                                                            "Caesar Dressing" => "Caesar Dressing",
                                                            "Cagliostro" => "Cagliostro",
                                                            "Calligraffitti" => "Calligraffitti",
                                                            "Cambo" => "Cambo",
                                                            "Candal" => "Candal",
                                                            "Cantarell" => "Cantarell",
                                                            "Cantata One" => "Cantata One",
                                                            "Cardo" => "Cardo",
                                                            "Carme" => "Carme",
                                                            "Carter One" => "Carter One",
                                                            "Caudex" => "Caudex",
                                                            "Cedarville Cursive" => "Cedarville Cursive",
                                                            "Ceviche One" => "Ceviche One",
                                                            "Changa One" => "Changa One",
                                                            "Chango" => "Chango",
                                                            "Chau Philomene One" => "Chau Philomene One",
                                                            "Chelsea Market" => "Chelsea Market",
                                                            "Chenla" => "Chenla",
                                                            "Cherry Cream Soda" => "Cherry Cream Soda",
                                                            "Chewy" => "Chewy",
                                                            "Chicle" => "Chicle",
                                                            "Chivo" => "Chivo",
                                                            "Coda" => "Coda",
                                                            "Coda Caption" => "Coda Caption",
                                                            "Codystar" => "Codystar",
                                                            "Comfortaa" => "Comfortaa",
                                                            "Coming Soon" => "Coming Soon",
                                                            "Concert One" => "Concert One",
                                                            "Condiment" => "Condiment",
                                                            "Content" => "Content",
                                                            "Contrail One" => "Contrail One",
                                                            "Convergence" => "Convergence",
                                                            "Cookie" => "Cookie",
                                                            "Copse" => "Copse",
                                                            "Corben" => "Corben",
                                                            "Cousine" => "Cousine",
                                                            "Coustard" => "Coustard",
                                                            "Covered By Your Grace" => "Covered By Your Grace",
                                                            "Crafty Girls" => "Crafty Girls",
                                                            "Creepster" => "Creepster",
                                                            "Crete Round" => "Crete Round",
                                                            "Crimson Text" => "Crimson Text",
                                                            "Crushed" => "Crushed",
                                                            "Cuprum" => "Cuprum",
                                                            "Cutive" => "Cutive",
                                                            "Damion" => "Damion",
                                                            "Dancing Script" => "Dancing Script",
                                                            "Dangrek" => "Dangrek",
                                                            "Dawning of a New Day" => "Dawning of a New Day",
                                                            "Days One" => "Days One",
                                                            "Delius" => "Delius",
                                                            "Delius Swash Caps" => "Delius Swash Caps",
                                                            "Delius Unicase" => "Delius Unicase",
                                                            "Della Respira" => "Della Respira",
                                                            "Devonshire" => "Devonshire",
                                                            "Didact Gothic" => "Didact Gothic",
                                                            "Diplomata" => "Diplomata",
                                                            "Diplomata SC" => "Diplomata SC",
                                                            "Doppio One" => "Doppio One",
                                                            "Dorsa" => "Dorsa",
                                                            "Dosis" => "Dosis",
                                                            "Dr Sugiyama" => "Dr Sugiyama",
                                                            "Droid Sans" => "Droid Sans",
                                                            "Droid Sans Mono" => "Droid Sans Mono",
                                                            "Droid Serif" => "Droid Serif",
                                                            "Duru Sans" => "Duru Sans",
                                                            "Dynalight" => "Dynalight",
                                                            "EB Garamond" => "EB Garamond",
                                                            "Eater" => "Eater",
                                                            "Economica" => "Economica",
                                                            "Electrolize" => "Electrolize",
                                                            "Emblema One" => "Emblema One",
                                                            "Emilys Candy" => "Emilys Candy",
                                                            "Engagement" => "Engagement",
                                                            "Enriqueta" => "Enriqueta",
                                                            "Erica One" => "Erica One",
                                                            "Esteban" => "Esteban",
                                                            "Euphoria Script" => "Euphoria Script",
                                                            "Ewert" => "Ewert",
                                                            "Exo" => "Exo",
                                                            "Expletus Sans" => "Expletus Sans",
                                                            "Fanwood Text" => "Fanwood Text",
                                                            "Fascinate" => "Fascinate",
                                                            "Fascinate Inline" => "Fascinate Inline",
                                                            "Federant" => "Federant",
                                                            "Federo" => "Federo",
                                                            "Felipa" => "Felipa",
                                                            "Fjord One" => "Fjord One",
                                                            "Flamenco" => "Flamenco",
                                                            "Flavors" => "Flavors",
                                                            "Fondamento" => "Fondamento",
                                                            "Fontdiner Swanky" => "Fontdiner Swanky",
                                                            "Forum" => "Forum",
                                                            "Francois One" => "Francois One",
                                                            "Fredericka the Great" => "Fredericka the Great",
                                                            "Fredoka One" => "Fredoka One",
                                                            "Freehand" => "Freehand",
                                                            "Fresca" => "Fresca",
                                                            "Frijole" => "Frijole",
                                                            "Fugaz One" => "Fugaz One",
                                                            "GFS Didot" => "GFS Didot",
                                                            "GFS Neohellenic" => "GFS Neohellenic",
                                                            "Galdeano" => "Galdeano",
                                                            "Gentium Basic" => "Gentium Basic",
                                                            "Gentium Book Basic" => "Gentium Book Basic",
                                                            "Geo" => "Geo",
                                                            "Geostar" => "Geostar",
                                                            "Geostar Fill" => "Geostar Fill",
                                                            "Germania One" => "Germania One",
                                                            "Give You Glory" => "Give You Glory",
                                                            "Glass Antiqua" => "Glass Antiqua",
                                                            "Glegoo" => "Glegoo",
                                                            "Gloria Hallelujah" => "Gloria Hallelujah",
                                                            "Goblin One" => "Goblin One",
                                                            "Gochi Hand" => "Gochi Hand",
                                                            "Gorditas" => "Gorditas",
                                                            "Goudy Bookletter 1911" => "Goudy Bookletter 1911",
                                                            "Graduate" => "Graduate",
                                                            "Gravitas One" => "Gravitas One",
                                                            "Great Vibes" => "Great Vibes",
                                                            "Gruppo" => "Gruppo",
                                                            "Gudea" => "Gudea",
                                                            "Habibi" => "Habibi",
                                                            "Hammersmith One" => "Hammersmith One",
                                                            "Handlee" => "Handlee",
                                                            "Hanuman" => "Hanuman",
                                                            "Happy Monkey" => "Happy Monkey",
                                                            "Henny Penny" => "Henny Penny",
                                                            "Herr Von Muellerhoff" => "Herr Von Muellerhoff",
                                                            "Holtwood One SC" => "Holtwood One SC",
                                                            "Homemade Apple" => "Homemade Apple",
                                                            "Homenaje" => "Homenaje",
                                                            "IM Fell DW Pica" => "IM Fell DW Pica",
                                                            "IM Fell DW Pica SC" => "IM Fell DW Pica SC",
                                                            "IM Fell Double Pica" => "IM Fell Double Pica",
                                                            "IM Fell Double Pica SC" => "IM Fell Double Pica SC",
                                                            "IM Fell English" => "IM Fell English",
                                                            "IM Fell English SC" => "IM Fell English SC",
                                                            "IM Fell French Canon" => "IM Fell French Canon",
                                                            "IM Fell French Canon SC" => "IM Fell French Canon SC",
                                                            "IM Fell Great Primer" => "IM Fell Great Primer",
                                                            "IM Fell Great Primer SC" => "IM Fell Great Primer SC",
                                                            "Iceberg" => "Iceberg",
                                                            "Iceland" => "Iceland",
                                                            "Imprima" => "Imprima",
                                                            "Inconsolata" => "Inconsolata",
                                                            "Inder" => "Inder",
                                                            "Indie Flower" => "Indie Flower",
                                                            "Inika" => "Inika",
                                                            "Irish Grover" => "Irish Grover",
                                                            "Istok Web" => "Istok Web",
                                                            "Italiana" => "Italiana",
                                                            "Italianno" => "Italianno",
                                                            "Jim Nightshade" => "Jim Nightshade",
                                                            "Jockey One" => "Jockey One",
                                                            "Jolly Lodger" => "Jolly Lodger",
                                                            "Josefin Sans" => "Josefin Sans",
                                                            "Josefin Slab" => "Josefin Slab",
                                                            "Judson" => "Judson",
                                                            "Julee" => "Julee",
                                                            "Junge" => "Junge",
                                                            "Jura" => "Jura",
                                                            "Just Another Hand" => "Just Another Hand",
                                                            "Just Me Again Down Here" => "Just Me Again Down Here",
                                                            "Kameron" => "Kameron",
                                                            "Karla" => "Karla",
                                                            "Kaushan Script" => "Kaushan Script",
                                                            "Kelly Slab" => "Kelly Slab",
                                                            "Kenia" => "Kenia",
                                                            "Khmer" => "Khmer",
                                                            "Knewave" => "Knewave",
                                                            "Kotta One" => "Kotta One",
                                                            "Koulen" => "Koulen",
                                                            "Kranky" => "Kranky",
                                                            "Kreon" => "Kreon",
                                                            "Kristi" => "Kristi",
                                                            "Krona One" => "Krona One",
                                                            "La Belle Aurore" => "La Belle Aurore",
                                                            "Lancelot" => "Lancelot",
                                                            "Lato" => "Lato",
                                                            "League Script" => "League Script",
                                                            "Leckerli One" => "Leckerli One",
                                                            "Ledger" => "Ledger",
                                                            "Lekton" => "Lekton",
                                                            "Lemon" => "Lemon",
                                                            "Lilita One" => "Lilita One",
                                                            "Limelight" => "Limelight",
                                                            "Linden Hill" => "Linden Hill",
                                                            "Lobster" => "Lobster",
                                                            "Lobster Two" => "Lobster Two",
                                                            "Londrina Outline" => "Londrina Outline",
                                                            "Londrina Shadow" => "Londrina Shadow",
                                                            "Londrina Sketch" => "Londrina Sketch",
                                                            "Londrina Solid" => "Londrina Solid",
                                                            "Lora" => "Lora",
                                                            "Love Ya Like A Sister" => "Love Ya Like A Sister",
                                                            "Loved by the King" => "Loved by the King",
                                                            "Lovers Quarrel" => "Lovers Quarrel",
                                                            "Luckiest Guy" => "Luckiest Guy",
                                                            "Lusitana" => "Lusitana",
                                                            "Lustria" => "Lustria",
                                                            "Macondo" => "Macondo",
                                                            "Macondo Swash Caps" => "Macondo Swash Caps",
                                                            "Magra" => "Magra",
                                                            "Maiden Orange" => "Maiden Orange",
                                                            "Mako" => "Mako",
                                                            "Marck Script" => "Marck Script",
                                                            "Marko One" => "Marko One",
                                                            "Marmelad" => "Marmelad",
                                                            "Marvel" => "Marvel",
                                                            "Mate" => "Mate",
                                                            "Mate SC" => "Mate SC",
                                                            "Maven Pro" => "Maven Pro",
                                                            "Meddon" => "Meddon",
                                                            "MedievalSharp" => "MedievalSharp",
                                                            "Medula One" => "Medula One",
                                                            "Megrim" => "Megrim",
                                                            "Merienda One" => "Merienda One",
                                                            "Merriweather" => "Merriweather",
                                                            "Metal" => "Metal",
                                                            "Metamorphous" => "Metamorphous",
                                                            "Metrophobic" => "Metrophobic",
                                                            "Michroma" => "Michroma",
                                                            "Miltonian" => "Miltonian",
                                                            "Miltonian Tattoo" => "Miltonian Tattoo",
                                                            "Miniver" => "Miniver",
                                                            "Miss Fajardose" => "Miss Fajardose",
                                                            "Modern Antiqua" => "Modern Antiqua",
                                                            "Molengo" => "Molengo",
                                                            "Monofett" => "Monofett",
                                                            "Monoton" => "Monoton",
                                                            "Monsieur La Doulaise" => "Monsieur La Doulaise",
                                                            "Montaga" => "Montaga",
                                                            "Montez" => "Montez",
                                                            "Montserrat" => "Montserrat",
                                                            "Moul" => "Moul",
                                                            "Moulpali" => "Moulpali",
                                                            "Mountains of Christmas" => "Mountains of Christmas",
                                                            "Mr Bedfort" => "Mr Bedfort",
                                                            "Mr Dafoe" => "Mr Dafoe",
                                                            "Mr De Haviland" => "Mr De Haviland",
                                                            "Mrs Saint Delafield" => "Mrs Saint Delafield",
                                                            "Mrs Sheppards" => "Mrs Sheppards",
                                                            "Muli" => "Muli",
                                                            "Mystery Quest" => "Mystery Quest",
                                                            "Neucha" => "Neucha",
                                                            "Neuton" => "Neuton",
                                                            "News Cycle" => "News Cycle",
                                                            "Niconne" => "Niconne",
                                                            "Nixie One" => "Nixie One",
                                                            "Nobile" => "Nobile",
                                                            "Nokora" => "Nokora",
                                                            "Norican" => "Norican",
                                                            "Nosifer" => "Nosifer",
                                                            "Nothing You Could Do" => "Nothing You Could Do",
                                                            "Noticia Text" => "Noticia Text",
                                                            "Nova Cut" => "Nova Cut",
                                                            "Nova Flat" => "Nova Flat",
                                                            "Nova Mono" => "Nova Mono",
                                                            "Nova Oval" => "Nova Oval",
                                                            "Nova Round" => "Nova Round",
                                                            "Nova Script" => "Nova Script",
                                                            "Nova Slim" => "Nova Slim",
                                                            "Nova Square" => "Nova Square",
                                                            "Numans" => "Numans",
                                                            "Nunito" => "Nunito",
                                                            "Odor Mean Chey" => "Odor Mean Chey",
                                                            "Old Standard TT" => "Old Standard TT",
                                                            "Oldenburg" => "Oldenburg",
                                                            "Oleo Script" => "Oleo Script",
                                                            "Open Sans" => "Open Sans",
                                                            "Open Sans Condensed" => "Open Sans Condensed",
                                                            "Orbitron" => "Orbitron",
                                                            "Original Surfer" => "Original Surfer",
                                                            "Oswald" => "Oswald",
                                                            "Over the Rainbow" => "Over the Rainbow",
                                                            "Overlock" => "Overlock",
                                                            "Overlock SC" => "Overlock SC",
                                                            "Ovo" => "Ovo",
                                                            "Oxygen" => "Oxygen",
                                                            "PT Mono" => "PT Mono",
                                                            "PT Sans" => "PT Sans",
                                                            "PT Sans Caption" => "PT Sans Caption",
                                                            "PT Sans Narrow" => "PT Sans Narrow",
                                                            "PT Serif" => "PT Serif",
                                                            "PT Serif Caption" => "PT Serif Caption",
                                                            "Pacifico" => "Pacifico",
                                                            "Parisienne" => "Parisienne",
                                                            "Passero One" => "Passero One",
                                                            "Passion One" => "Passion One",
                                                            "Patrick Hand" => "Patrick Hand",
                                                            "Patua One" => "Patua One",
                                                            "Paytone One" => "Paytone One",
                                                            "Permanent Marker" => "Permanent Marker",
                                                            "Petrona" => "Petrona",
                                                            "Philosopher" => "Philosopher",
                                                            "Piedra" => "Piedra",
                                                            "Pinyon Script" => "Pinyon Script",
                                                            "Plaster" => "Plaster",
                                                            "Play" => "Play",
                                                            "Playball" => "Playball",
                                                            "Playfair Display" => "Playfair Display",
                                                            "Podkova" => "Podkova",
                                                            "Poiret One" => "Poiret One",
                                                            "Poller One" => "Poller One",
                                                            "Poly" => "Poly",
                                                            "Pompiere" => "Pompiere",
                                                            "Pontano Sans" => "Pontano Sans",
                                                            "Port Lligat Sans" => "Port Lligat Sans",
                                                            "Port Lligat Slab" => "Port Lligat Slab",
                                                            "Prata" => "Prata",
                                                            "Preahvihear" => "Preahvihear",
                                                            "Press Start 2P" => "Press Start 2P",
                                                            "Princess Sofia" => "Princess Sofia",
                                                            "Prociono" => "Prociono",
                                                            "Prosto One" => "Prosto One",
                                                            "Puritan" => "Puritan",
                                                            "Quantico" => "Quantico",
                                                            "Quattrocento" => "Quattrocento",
                                                            "Quattrocento Sans" => "Quattrocento Sans",
                                                            "Questrial" => "Questrial",
                                                            "Quicksand" => "Quicksand",
                                                            "Qwigley" => "Qwigley",
                                                            "Radley" => "Radley",
                                                            "Raleway" => "Raleway",
                                                            "Rammetto One" => "Rammetto One",
                                                            "Rancho" => "Rancho",
                                                            "Rationale" => "Rationale",
                                                            "Redressed" => "Redressed",
                                                            "Reenie Beanie" => "Reenie Beanie",
                                                            "Revalia" => "Revalia",
                                                            "Ribeye" => "Ribeye",
                                                            "Ribeye Marrow" => "Ribeye Marrow",
                                                            "Righteous" => "Righteous",
                                                            "Rochester" => "Rochester",
                                                            "Rock Salt" => "Rock Salt",
                                                            "Rokkitt" => "Rokkitt",
                                                            "Ropa Sans" => "Ropa Sans",
                                                            "Rosario" => "Rosario",
                                                            "Rosarivo" => "Rosarivo",
                                                            "Rouge Script" => "Rouge Script",
                                                            "Ruda" => "Ruda",
                                                            "Ruge Boogie" => "Ruge Boogie",
                                                            "Ruluko" => "Ruluko",
                                                            "Ruslan Display" => "Ruslan Display",
                                                            "Russo One" => "Russo One",
                                                            "Ruthie" => "Ruthie",
                                                            "Sail" => "Sail",
                                                            "Salsa" => "Salsa",
                                                            "Sancreek" => "Sancreek",
                                                            "Sansita One" => "Sansita One",
                                                            "Sarina" => "Sarina",
                                                            "Satisfy" => "Satisfy",
                                                            "Schoolbell" => "Schoolbell",
                                                            "Seaweed Script" => "Seaweed Script",
                                                            "Sevillana" => "Sevillana",
                                                            "Shadows Into Light" => "Shadows Into Light",
                                                            "Shadows Into Light Two" => "Shadows Into Light Two",
                                                            "Shanti" => "Shanti",
                                                            "Share" => "Share",
                                                            "Shojumaru" => "Shojumaru",
                                                            "Short Stack" => "Short Stack",
                                                            "Siemreap" => "Siemreap",
                                                            "Sigmar One" => "Sigmar One",
                                                            "Signika" => "Signika",
                                                            "Signika Negative" => "Signika Negative",
                                                            "Simonetta" => "Simonetta",
                                                            "Sirin Stencil" => "Sirin Stencil",
                                                            "Six Caps" => "Six Caps",
                                                            "Slackey" => "Slackey",
                                                            "Smokum" => "Smokum",
                                                            "Smythe" => "Smythe",
                                                            "Sniglet" => "Sniglet",
                                                            "Snippet" => "Snippet",
                                                            "Sofia" => "Sofia",
                                                            "Sonsie One" => "Sonsie One",
                                                            "Sorts Mill Goudy" => "Sorts Mill Goudy",
                                                            "Special Elite" => "Special Elite",
                                                            "Spicy Rice" => "Spicy Rice",
                                                            "Spinnaker" => "Spinnaker",
                                                            "Spirax" => "Spirax",
                                                            "Squada One" => "Squada One",
                                                            "Stardos Stencil" => "Stardos Stencil",
                                                            "Stint Ultra Condensed" => "Stint Ultra Condensed",
                                                            "Stint Ultra Expanded" => "Stint Ultra Expanded",
                                                            "Stoke" => "Stoke",
                                                            "Sue Ellen Francisco" => "Sue Ellen Francisco",
                                                            "Sunshiney" => "Sunshiney",
                                                            "Supermercado One" => "Supermercado One",
                                                            "Suwannaphum" => "Suwannaphum",
                                                            "Swanky and Moo Moo" => "Swanky and Moo Moo",
                                                            "Syncopate" => "Syncopate",
                                                            "Tangerine" => "Tangerine",
                                                            "Taprom" => "Taprom",
                                                            "Telex" => "Telex",
                                                            "Tenor Sans" => "Tenor Sans",
                                                            "The Girl Next Door" => "The Girl Next Door",
                                                            "Tienne" => "Tienne",
                                                            "Tinos" => "Tinos",
                                                            "Titan One" => "Titan One",
                                                            "Trade Winds" => "Trade Winds",
                                                            "Trocchi" => "Trocchi",
                                                            "Trochut" => "Trochut",
                                                            "Trykker" => "Trykker",
                                                            "Tulpen One" => "Tulpen One",
                                                            "Ubuntu" => "Ubuntu",
                                                            "Ubuntu Condensed" => "Ubuntu Condensed",
                                                            "Ubuntu Mono" => "Ubuntu Mono",
                                                            "Ultra" => "Ultra",
                                                            "Uncial Antiqua" => "Uncial Antiqua",
                                                            "UnifrakturCook" => "UnifrakturCook",
                                                            "UnifrakturMaguntia" => "UnifrakturMaguntia",
                                                            "Unkempt" => "Unkempt",
                                                            "Unlock" => "Unlock",
                                                            "Unna" => "Unna",
                                                            "VT323" => "VT323",
                                                            "Varela" => "Varela",
                                                            "Varela Round" => "Varela Round",
                                                            "Vast Shadow" => "Vast Shadow",
                                                            "Vibur" => "Vibur",
                                                            "Vidaloka" => "Vidaloka",
                                                            "Viga" => "Viga",
                                                            "Voces" => "Voces",
                                                            "Volkhov" => "Volkhov",
                                                            "Vollkorn" => "Vollkorn",
                                                            "Voltaire" => "Voltaire",
                                                            "Waiting for the Sunrise" => "Waiting for the Sunrise",
                                                            "Wallpoet" => "Wallpoet",
                                                            "Walter Turncoat" => "Walter Turncoat",
                                                            "Wellfleet" => "Wellfleet",
                                                            "Wire One" => "Wire One",
                                                            "Yanone Kaffeesatz" => "Yanone Kaffeesatz",
                                                            "Yellowtail" => "Yellowtail",
                                                            "Yeseva One" => "Yeseva One",
                                                            "Yesteryear" => "Yesteryear",
                                                            "Zeyada" => "Zeyada",
                                                    ); */
$google_fonts_array = array(
                'ABeeZee',
                'Abel',
                'Abril+Fatface',
                'Aclonica',
                'Acme',
                'Actor',
                'Adamina',
                'Advent+Pro',
                'Aguafina+Script',
                'Akronim',
                'Aladin',
                'Aldrich',
                'Alef',
                'Alegreya',
                'Alegreya+Sans',
                'Alegreya+Sans+SC',
                'Alegreya+SC',
                'Alex+Brush',
                'Alfa+Slab+One',
                'Alice',
                'Alike',
                'Alike+Angular',
                'Allan',
                'Allerta',
                'Allerta+Stencil',
                'Allura',
                'Almendra',
                'Almendra+Display',
                'Almendra+SC',
                'Amarante',
                'Amaranth',
                'Amatic+SC',
                'Amethysta',
                'Amiri',
                'Amita',
                'Anaheim',
                'Andada',
                'Andika',
                'Angkor',
                'Annie+Use+Your+Telescope',
                'Anonymous+Pro',
                'Antic',
                'Antic+Didone',
                'Antic+Slab',
                'Anton',
                'Arapey',
                'Arbutus',
                'Arbutus+Slab',
                'Architects+Daughter',
                'Archivo+Black',
                'Archivo+Narrow',
                'Arimo',
                'Arizonia',
                'Armata',
                'Artifika',
                'Arvo',
                'Arya',
                'Asap',
                'Asar',
                'Asset',
                'Astloch',
                'Asul',
                'Atomic+Age',
                'Aubrey',
                'Audiowide',
                'Autour+One',
                'Average',
                'Average+Sans',
                'Averia+Gruesa+Libre',
                'Averia+Libre',
                'Averia+Sans+Libre',
                'Averia+Serif+Libre',
                'Bad+Script',
                'Balthazar',
                'Bangers',
                'Basic',
                'Battambang',
                'Baumans',
                'Bayon',
                'Belgrano',
                'Belleza',
                'BenchNine',
                'Bentham',
                'Berkshire+Swash',
                'Bevan',
                'Bigelow+Rules',
                'Bigshot+One',
                'Bilbo',
                'Bilbo+Swash+Caps',
                'Biryani',
                'Bitter',
                'Black+Ops+One',
                'Bokor',
                'Bonbon',
                'Boogaloo',
                'Bowlby+One',
                'Bowlby+One+SC',
                'Brawler',
                'Bree+Serif',
                'Bubblegum+Sans',
                'Bubbler+One',
                'Buda',
                'Buenard',
                'Butcherman',
                'Butterfly+Kids',
                'Cabin',
                'Cabin+Condensed',
                'Cabin+Sketch',
                'Caesar+Dressing',
                'Cagliostro',
                'Calligraffitti',
                'Cambay',
                'Cambo',
                'Candal',
                'Cantarell',
                'Cantata+One',
                'Cantora+One',
                'Capriola',
                'Cardo',
                'Carme',
                'Carrois+Gothic',
                'Carrois+Gothic+SC',
                'Carter+One',
                'Catamaran',
                'Caudex',
                'Cedarville+Cursive',
                'Ceviche+One',
                'Changa+One',
                'Chango',
                'Chau+Philomene+One',
                'Chela+One',
                'Chelsea+Market',
                'Chenla',
                'Cherry+Cream+Soda',
                'Cherry+Swash',
                'Chewy',
                'Chicle',
                'Chivo',
                'Chonburi',
                'Cinzel',
                'Cinzel+Decorative',
                'Clicker+Script',
                'Coda',
                'Coda+Caption',
                'Codystar',
                'Combo',
                'Comfortaa',
                'Coming+Soon',
                'Concert+One',
                'Condiment',
                'Content',
                'Contrail+One',
                'Convergence',
                'Cookie',
                'Copse',
                'Corben',
                'Courgette',
                'Cousine',
                'Coustard',
                'Covered+By+Your+Grace',
                'Crafty+Girls',
                'Creepster',
                'Crete+Round',
                'Crimson+Text',
                'Croissant+One',
                'Crushed',
                'Cuprum',
                'Cutive',
                'Cutive+Mono',
                'Damion',
                'Dancing+Script',
                'Dangrek',
                'Dawning+of+a+New+Day',
                'Days+One',
                'Dekko',
                'Delius',
                'Delius+Swash+Caps',
                'Delius+Unicase',
                'Della+Respira',
                'Denk+One',
                'Devonshire',
                'Dhurjati',
                'Didact+Gothic',
                'Diplomata',
                'Diplomata+SC',
                'Domine',
                'Donegal+One',
                'Doppio+One',
                'Dorsa',
                'Dosis',
                'Dr+Sugiyama',
                'Droid+Sans',
                'Droid+Sans+Mono',
                'Droid+Serif',
                'Duru+Sans',
                'Dynalight',
                'Eagle+Lake',
                'Eater',
                'EB+Garamond',
                'Economica',
                'Eczar',
                'Ek+Mukta',
                'Electrolize',
                'Elsie',
                'Elsie+Swash+Caps',
                'Emblema+One',
                'Emilys+Candy',
                'Engagement',
                'Englebert',
                'Enriqueta',
                'Erica+One',
                'Esteban',
                'Euphoria+Script',
                'Ewert',
                'Exo',
                'Exo+2',
                'Expletus+Sans',
                'Fanwood+Text',
                'Fascinate',
                'Fascinate+Inline',
                'Faster+One',
                'Fasthand',
                'Fauna+One',
                'Federant',
                'Federo',
                'Felipa',
                'Fenix',
                'Finger+Paint',
                'Fira+Mono',
                'Fira+Sans',
                'Fjalla+One',
                'Fjord+One',
                'Flamenco',
                'Flavors',
                'Fondamento',
                'Fontdiner+Swanky',
                'Forum',
                'Francois+One',
                'Freckle+Face',
                'Fredericka+the+Great',
                'Fredoka+One',
                'Freehand',
                'Fresca',
                'Frijole',
                'Fruktur',
                'Fugaz+One',
                'Gabriela',
                'Gafata',
                'Galdeano',
                'Galindo',
                'Gentium+Basic',
                'Gentium+Book+Basic',
                'Geo',
                'Geostar',
                'Geostar+Fill',
                'Germania+One',
                'GFS+Didot',
                'GFS+Neohellenic',
                'Gidugu',
                'Gilda+Display',
                'Give+You+Glory',
                'Glass+Antiqua',
                'Glegoo',
                'Gloria+Hallelujah',
                'Goblin+One',
                'Gochi+Hand',
                'Gorditas',
                'Goudy+Bookletter+1911',
                'Graduate',
                'Grand+Hotel',
                'Gravitas+One',
                'Great+Vibes',
                'Griffy',
                'Gruppo',
                'Gudea',
                'Gurajada',
                'Habibi',
                'Halant',
                'Hammersmith+One',
                'Hanalei',
                'Hanalei+Fill',
                'Handlee',
                'Hanuman',
                'Happy+Monkey',
                'Headland+One',
                'Henny+Penny',
                'Herr+Von+Muellerhoff',
                'Hind',
                'Holtwood+One+SC',
                'Homemade+Apple',
                'Homenaje',
                'Iceberg',
                'Iceland',
                'IM+Fell+Double+Pica',
                'IM+Fell+Double+Pica+SC',
                'IM+Fell+DW+Pica',
                'IM+Fell+DW+Pica+SC',
                'IM+Fell+English',
                'IM+Fell+English+SC',
                'IM+Fell+French+Canon',
                'IM+Fell+French+Canon+SC',
                'IM+Fell+Great+Primer',
                'IM+Fell+Great+Primer+SC',
                'Imprima',
                'Inconsolata',
                'Inder',
                'Indie+Flower',
                'Inika',
                'Inknut+Antiqua',
                'Irish+Grover',
                'Istok+Web',
                'Italiana',
                'Italianno',
                'Itim',
                'Jacques+Francois',
                'Jacques+Francois+Shadow',
                'Jaldi',
                'Jim+Nightshade',
                'Jockey+One',
                'Jolly+Lodger',
                'Josefin+Sans',
                'Josefin+Slab',
                'Joti+One',
                'Judson',
                'Julee',
                'Julius+Sans+One',
                'Junge',
                'Jura',
                'Just+Another+Hand',
                'Just+Me+Again+Down+Here',
                'Kadwa',
                'Kalam',
                'Kameron',
                'Kantumruy',
                'Karla',
                'Karma',
                'Kaushan+Script',
                'Kavoon',
                'Kdam+Thmor',
                'Keania+One',
                'Kelly+Slab',
                'Kenia',
                'Khand',
                'Khmer',
                'Khula',
                'Kite+One',
                'Knewave',
                'Kotta+One',
                'Koulen',
                'Kranky',
                'Kreon',
                'Kristi',
                'Krona+One',
                'Kurale',
                'La+Belle+Aurore',
                'Laila',
                'Lakki+Reddy',
                'Lancelot',
                'Lateef',
                'Lato',
                'League+Script',
                'Leckerli+One',
                'Ledger',
                'Lekton',
                'Lemon',
                'Libre+Baskerville',
                'Life+Savers',
                'Lilita+One',
                'Lily+Script+One',
                'Limelight',
                'Linden+Hill',
                'Lobster',
                'Lobster+Two',
                'Londrina+Outline',
                'Londrina+Shadow',
                'Londrina+Sketch',
                'Londrina+Solid',
                'Lora',
                'Love+Ya+Like+A+Sister',
                'Loved+by+the+King',
                'Lovers+Quarrel',
                'Luckiest+Guy',
                'Lusitana',
                'Lustria',
                'Macondo',
                'Macondo+Swash+Caps',
                'Magra',
                'Maiden+Orange',
                'Mako',
                'Mallanna',
                'Mandali',
                'Marcellus',
                'Marcellus+SC',
                'Marck+Script',
                'Margarine',
                'Marko+One',
                'Marmelad',
                'Martel',
                'Martel+Sans',
                'Marvel',
                'Mate',
                'Mate+SC',
                'Maven+Pro',
                'McLaren',
                'Meddon',
                'MedievalSharp',
                'Medula+One',
                'Megrim',
                'Meie+Script',
                'Merienda',
                'Merienda+One',
                'Merriweather',
                'Merriweather+Sans',
                'Metal',
                'Metal+Mania',
                'Metamorphous',
                'Metrophobic',
                'Michroma',
                'Milonga',
                'Miltonian',
                'Miltonian+Tattoo',
                'Miniver',
                'Miss+Fajardose',
                'Modak',
                'Modern+Antiqua',
                'Molengo',
                'Molle',
                'Monda',
                'Monofett',
                'Monoton',
                'Monsieur+La+Doulaise',
                'Montaga',
                'Montez',
                'Montserrat',
                'Montserrat+Alternates',
                'Montserrat+Subrayada',
                'Moul',
                'Moulpali',
                'Mountains+of+Christmas',
                'Mouse+Memoirs',
                'Mr+Bedfort',
                'Mr+Dafoe',
                'Mr+De+Haviland',
                'Mrs+Saint+Delafield',
                'Mrs+Sheppards',
                'Muli',
                'Mystery+Quest',
                'Neucha',
                'Neuton',
                'New+Rocker',
                'News+Cycle',
                'Niconne',
                'Nixie+One',
                'Nobile',
                'Nokora',
                'Norican',
                'Nosifer',
                'Nothing+You+Could+Do',
                'Noticia+Text',
                'Noto+Sans',
                'Noto+Serif',
                'Nova+Cut',
                'Nova+Flat',
                'Nova+Mono',
                'Nova+Oval',
                'Nova+Round',
                'Nova+Script',
                'Nova+Slim',
                'Nova+Square',
                'NTR',
                'Numans',
                'Nunito',
                'Odor+Mean+Chey',
                'Offside',
                'Old+Standard+TT',
                'Oldenburg',
                'Oleo+Script',
                'Oleo+Script+Swash+Caps',
                'Open+Sans',
                'Open+Sans+Condensed',
                'Oranienbaum',
                'Orbitron',
                'Oregano',
                'Orienta',
                'Original+Surfer',
                'Oswald',
                'Over+the+Rainbow',
                'Overlock',
                'Overlock+SC',
                'Ovo',
                'Oxygen',
                'Oxygen+Mono',
                'Pacifico',
                'Palanquin',
                'Palanquin+Dark',
                'Paprika',
                'Parisienne',
                'Passero+One',
                'Passion+One',
                'Pathway+Gothic+One',
                'Patrick+Hand',
                'Patrick+Hand+SC',
                'Patua+One',
                'Paytone+One',
                'Peddana',
                'Peralta',
                'Permanent+Marker',
                'Petit+Formal+Script',
                'Petrona',
                'Philosopher',
                'Piedra',
                'Pinyon+Script',
                'Pirata+One',
                'Plaster',
                'Play',
                'Playball',
                'Playfair+Display',
                'Playfair+Display+SC',
                'Podkova',
                'Poiret+One',
                'Poller+One',
                'Poly',
                'Pompiere',
                'Pontano+Sans',
                'Poppins',
                'Port+Lligat+Sans',
                'Port+Lligat+Slab',
                'Pragati+Narrow',
                'Prata',
                'Preahvihear',
                'Press+Start+2P',
                'Princess+Sofia',
                'Prociono',
                'Prosto+One',
                'PT+Mono',
                'PT+Sans',
                'PT+Sans+Caption',
                'PT+Sans+Narrow',
                'PT+Serif',
                'PT+Serif+Caption',
                'Puritan',
                'Purple+Purse',
                'Quando',
                'Quantico',
                'Quattrocento',
                'Quattrocento+Sans',
                'Questrial',
                'Quicksand',
                'Quintessential',
                'Qwigley',
                'Racing+Sans+One',
                'Radley',
                'Rajdhani',
                'Raleway',
                'Raleway+Dots',
                'Ramabhadra',
                'Ramaraja',
                'Rambla',
                'Rammetto+One',
                'Ranchers',
                'Rancho',
                'Ranga',
                'Rationale',
                'Ravi+Prakash',
                'Redressed',
                'Reenie+Beanie',
                'Revalia',
                'Rhodium+Libre',
                'Ribeye',
                'Ribeye+Marrow',
                'Righteous',
                'Risque',
                'Roboto',
                'Roboto+Condensed',
                'Roboto+Mono',
                'Roboto+Slab',
                'Rochester',
                'Rock+Salt',
                'Rokkitt',
                'Romanesco',
                'Ropa+Sans',
                'Rosario',
                'Rosarivo',
                'Rouge+Script',
                'Rozha+One',
                'Rubik',
                'Rubik+Mono+One',
                'Rubik+One',
                'Ruda',
                'Rufina',
                'Ruge+Boogie',
                'Ruluko',
                'Rum+Raisin',
                'Ruslan+Display',
                'Russo+One',
                'Ruthie',
                'Rye',
                'Sacramento',
                'Sahitya',
                'Sail',
                'Salsa',
                'Sanchez',
                'Sancreek',
                'Sansita+One',
                'Sarala',
                'Sarina',
                'Sarpanch',
                'Satisfy',
                'Scada',
                'Scheherazade',
                'Schoolbell',
                'Seaweed+Script',
                'Sevillana',
                'Seymour+One',
                'Shadows+Into+Light',
                'Shadows+Into+Light+Two',
                'Shanti',
                'Share',
                'Share+Tech',
                'Share+Tech+Mono',
                'Shojumaru',
                'Short+Stack',
                'Siemreap',
                'Sigmar+One',
                'Signika',
                'Signika+Negative',
                'Simonetta',
                'Sintony',
                'Sirin+Stencil',
                'Six+Caps',
                'Skranji',
                'Slabo+13px',
                'Slabo+27px',
                'Slackey',
                'Smokum',
                'Smythe',
                'Sniglet',
                'Snippet',
                'Snowburst+One',
                'Sofadi+One',
                'Sofia',
                'Sonsie+One',
                'Sorts+Mill+Goudy',
                'Source+Code+Pro',
                'Source+Sans+Pro',
                'Source+Serif+Pro',
                'Special+Elite',
                'Spicy+Rice',
                'Spinnaker',
                'Spirax',
                'Squada+One',
                'Sree+Krushnadevaraya',
                'Stalemate',
                'Stalinist+One',
                'Stardos+Stencil',
                'Stint+Ultra+Condensed',
                'Stint+Ultra+Expanded',
                'Stoke',
                'Strait',
                'Sue+Ellen+Francisco',
                'Sumana',
                'Sunshiney',
                'Supermercado+One',
                'Sura',
                'Suranna',
                'Suravaram',
                'Suwannaphum',
                'Swanky+and+Moo+Moo',
                'Syncopate',
                'Tangerine',
                'Taprom',
                'Tauri',
                'Teko',
                'Telex',
                'Tenali+Ramakrishna',
                'Tenor+Sans',
                'Text+Me+One',
                'The+Girl+Next+Door',
                'Tienne',
                'Tillana',
                'Timmana',
                'Tinos',
                'Titan+One',
                'Titillium+Web',
                'Trade+Winds',
                'Trocchi',
                'Trochut',
                'Trykker',
                'Tulpen+One',
                'Ubuntu',
                'Ubuntu+Condensed',
                'Ubuntu+Mono',
                'Ultra',
                'Uncial+Antiqua',
                'Underdog',
                'Unica+One',
                'UnifrakturCook',
                'UnifrakturMaguntia',
                'Unkempt',
                'Unlock',
                'Unna',
                'Vampiro+One',
                'Varela',
                'Varela+Round',
                'Vast+Shadow',
                'Vesper+Libre',
                'Vibur',
                'Vidaloka',
                'Viga',
                'Voces',
                'Volkhov',
                'Vollkorn',
                'Voltaire',
                'VT323',
                'Waiting+for+the+Sunrise',
                'Wallpoet',
                'Walter+Turncoat',
                'Warnes',
                'Wellfleet',
                'Wendy+One',
                'Wire+One',
                'Work+Sans',
                'Yanone+Kaffeesatz',
                'Yantramanav',
                'Yellowtail',
                'Yeseva+One',
                'Yesteryear',
                'Zeyada'
            );
    $font_select='';
    /*foreach($google_fonts_array as $key=>$value){
        $font_select.='<option value="'.$key.'">'.$value.'</option>';
    }
    */
    foreach($google_fonts_array as $value){
        $font_select.='<option value="'.$value.'">'.str_replace('+',' ',$value).'</option>';
    }
    
    
    $general_font_select='';
    $general_font= esc_html ( get_option('wp_estate_general_font','') );
    if($general_font!='x'){
        $general_font_select='<option value="'.$general_font.'">'.$general_font.'</option>';
    }
    /*
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Main Font','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Replace theme font with another Google Font from the list below.','wpresidence').'</div>    
        <select id="general_font" name="general_font">
            '.$general_font_select.'
            <option value="">- original font -</option>
            '.$font_select.'                   
        </select> 
    </div>';
    

    
    $headings_font_subset   =   esc_html ( get_option('wp_estate_headings_font_subset','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Font subset','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Specify font subset(s) if you don\'t use latin language.','wpresidence').'</div>    
        <input type="text" id="headings_font_subset" name="headings_font_subset" value="'.$headings_font_subset.'">  
    </div>';
    */
    
    
    
    $font_weight_array = array( 
            "normal"=>"Normal",
            "bold"=>"Bold",
            "bolder"=>"Bolder",
            "lighter"=>"Lighter",
            "100"=>"100",
            "200"=>"200",
            "300"=>"300",
            "400"=>"400",
            "500"=>"500",
            "600"=>"600",
            "700"=>"700",
            "800"=>"800",
            "900"=>"900",
            "initial"=> "Initial"    
    );

    $font_weight='';
    foreach($font_weight_array as $key=>$value){
        $font_weight.='<option value="'.$key.'">'.$value.'</option>';
    }
   
    
    
    // H1 Typography
    $h1_fontfamily='';
    $h1_fontfamily= esc_html ( get_option('wp_estate_h1_fontfamily','') );
    if($h1_fontfamily!='x'){
        $h1_fontfamily='<option value="'.$h1_fontfamily.'">'.$h1_fontfamily.'</option>';
    }
    $h1_fontsize =   esc_html( get_option('wp_estate_h1_fontsize') );
    $h1_fontsubset =   esc_html( get_option('wp_estate_h1_fontsubset') );
    $h1_lineheight =   esc_html( get_option('wp_estate_h1_lineheight') ); 
    $h1_fontweight='';
    $h1_fontweight= esc_html ( get_option('wp_estate_h1_fontweight','') );
    if($h1_fontweight!='x'){
        $h1_fontweight='<option value="'.$h1_fontweight.'">'.$h1_fontweight.'</option>';
    }

//var_dump($h1_fontweight);
    // H2 Typography
    $h2_fontfamily='';
    $h2_fontfamily= esc_html ( get_option('wp_estate_h2_fontfamily','') );
    if($h2_fontfamily!='x'){
        $h2_fontfamily='<option value="'.$h2_fontfamily.'">'.$h2_fontfamily.'</option>';
    }
    $h2_fontsize =   esc_html( get_option('wp_estate_h2_fontsize') );
    $h2_fontsubset =   esc_html( get_option('wp_estate_h2_fontsubset') );
    $h2_lineheight =   esc_html( get_option('wp_estate_h2_lineheight')) ;
    $h2_fontweight='';
    $h2_fontweight= esc_html ( get_option('wp_estate_h2_fontweight','') );
    if($h2_fontweight!='x'){
        $h2_fontweight='<option value="'.$h2_fontweight.'">'.$h2_fontweight.'</option>';
    }

    // H3 Typography
    $h3_fontfamily='';
    $h3_fontfamily= esc_html ( get_option('wp_estate_h3_fontfamily','') );
    if($h3_fontfamily!='x'){
        $h3_fontfamily='<option value="'.$h3_fontfamily.'">'.$h3_fontfamily.'</option>';
    }
    $h3_fontsize =   esc_html( get_option('wp_estate_h3_fontsize') );
    $h3_fontsubset =   esc_html( get_option('wp_estate_h3_fontsubset') );
    $h3_lineheight =   esc_html( get_option('wp_estate_h3_lineheight') );
    $h3_fontweight='';
    $h3_fontweight= esc_html ( get_option('wp_estate_h3_fontweight','') );
    if($h3_fontweight!='x'){
        $h3_fontweight='<option value="'.$h3_fontweight.'">'.$h3_fontweight.'</option>';
    }

    // H4 Typography
    $h4_fontfamily='';
    $h4_fontfamily= esc_html ( get_option('wp_estate_h4_fontfamily','') );
    if($h4_fontfamily!='x'){
        $h4_fontfamily='<option value="'.$h4_fontfamily.'">'.$h4_fontfamily.'</option>';
    }
    $h4_fontsize =   esc_html( get_option('wp_estate_h4_fontsize') );
    $h4_fontsubset =   esc_html( get_option('wp_estate_h4_fontsubset') );
    $h4_lineheight =   esc_html( get_option('wp_estate_h4_lineheight') );
    $h4_fontweight='';
    $h4_fontweight= esc_html ( get_option('wp_estate_h4_fontweight','') );
    if($h4_fontweight!='x'){
        $h4_fontweight='<option value="'.$h4_fontweight.'">'.$h4_fontweight.'</option>';
    }


    // H5 Typography
    $h5_fontfamily='';
    $h5_fontfamily= esc_html ( get_option('wp_estate_h5_fontfamily','') );
    if($h5_fontfamily!='x'){
        $h5_fontfamily='<option value="'.$h5_fontfamily.'">'.$h5_fontfamily.'</option>';
    }
    $h5_fontsize =   esc_html( get_option('wp_estate_h5_fontsize') );
    $h5_fontsubset =   esc_html( get_option('wp_estate_h5_fontsubset') );
    $h5_lineheight =   esc_html( get_option('wp_estate_h5_lineheight') );
    $h5_fontweight='';
    $h5_fontweight= esc_html ( get_option('wp_estate_h5_fontweight','') );
    if($h5_fontweight!='x'){
        $h5_fontweight='<option value="'.$h5_fontweight.'">'.$h5_fontweight.'</option>';
    }

    // H6 Typography
    $h6_fontfamily='';
    $h6_fontfamily= esc_html ( get_option('wp_estate_h6_fontfamily','') );
    if($h6_fontfamily!='x'){
        $h6_fontfamily='<option value="'.$h6_fontfamily.'">'.$h6_fontfamily.'</option>';
    }
    $h6_fontsize =   esc_html( get_option('wp_estate_h6_fontsize') );
    $h6_fontsubset =   esc_html( get_option('wp_estate_h6_fontsubset') );
    $h6_lineheight =   esc_html( get_option('wp_estate_h6_lineheight') );
    $h6_fontweight='';
    $h6_fontweight= esc_html ( get_option('wp_estate_h6_fontweight','') );
    if($h6_fontweight!='x'){
        $h6_fontweight='<option value="'.$h6_fontweight.'">'.$h6_fontweight.'</option>';
    }

    // H6 Typography
    $p_fontfamily='';
    $p_fontfamily= esc_html ( get_option('wp_estate_p_fontfamily','') );
    if($p_fontfamily!='x'){
        $p_fontfamily='<option value="'.$p_fontfamily.'">'.$p_fontfamily.'</option>';
    }
    $p_fontsize =   esc_html( get_option('wp_estate_p_fontsize') );
    $p_fontsubset =   esc_html( get_option('wp_estate_p_fontsubset') );
    $p_lineheight =   esc_html( get_option('wp_estate_p_lineheight') );
    $p_fontweight='';
    $p_fontweight= esc_html ( get_option('wp_estate_p_fontweight','') );
    if($p_fontweight!='x'){
        $p_fontweight='<option value="'.$p_fontweight.'">'.$p_fontweight.'</option>';
    }

    // Menu Typography
    $menu_fontfamily='';
    $menu_fontfamily= esc_html ( get_option('wp_estate_menu_fontfamily','') );
    if($menu_fontfamily!='x'){
        $menu_fontfamily='<option value="'.$menu_fontfamily.'">'.$menu_fontfamily.'</option>';
    }
    $menu_fontsize =   esc_html( get_option('wp_estate_menu_fontsize') );
    $menu_fontsubset =   esc_html( get_option('wp_estate_menu_fontsubset') );
    $menu_lineheight =   esc_html( get_option('wp_estate_menu_lineheight') );
    $menu_fontweight='';
    $menu_fontweight= esc_html ( get_option('wp_estate_menu_fontweight','') );
    if($menu_fontweight!='x'){
        $menu_fontweight='<option value="'.$menu_fontweight.'">'.$menu_fontweight.'</option>';
    }

     // sidebar Typography
    $sidebar_fontfamily='';
    $sidebar_fontfamily= esc_html ( get_option('wp_estate_sidebar_fontfamily','') );
    if($sidebar_fontfamily!='x'){
        $sidebar_fontfamily='<option value="'.$sidebar_fontfamily.'">'.$sidebar_fontfamily.'</option>';
    }
    $sidebar_fontsize =   esc_html( get_option('wp_estate_sidebar_fontsize') );
    $sidebar_fontsubset =   esc_html( get_option('wp_estate_sidebar_fontsubset') );
    $sidebar_lineheight =   esc_html( get_option('wp_estate_sidebar_lineheight') );
    $sidebar_fontweight='';
    $sidebar_fontweight= esc_html ( get_option('wp_estate_sidebar_fontweight','') );
    if($sidebar_fontweight!='x'){
        $sidebar_fontweight='<option value="'.$sidebar_fontweight.'">'.$sidebar_fontweight.'</option>';
    }


     // footer Typography
    $footer_fontfamily='';
    $footer_fontfamily= esc_html ( get_option('wp_estate_footer_fontfamily','') );
    if($footer_fontfamily!='x'){
        $footer_fontfamily='<option value="'.$footer_fontfamily.'">'.$footer_fontfamily.'</option>';
    }
    $footer_fontsize =   esc_html( get_option('wp_estate_footer_fontsize') );
    $footer_fontsubset =   esc_html( get_option('wp_estate_footer_fontsubset') );
    $footer_lineheight =   esc_html( get_option('wp_estate_footer_lineheight') );
    $footer_fontweight='';
    $footer_fontweight= esc_html ( get_option('wp_estate_footer_fontweight','') );
    if($footer_fontweight!='x'){
        $footer_fontweight='<option value="'.$footer_fontweight.'">'.$footer_fontweight.'</option>';
    }



    print'<div class="estate_option_row">
    <table>
    <th style="text-align:left;"><div class="label_option_row">'.esc_html__('H1 Font','wpresidence').'</div><th>
    <tr><td><div class="option_row_explain">'.esc_html__('Font Family:','wpresidence').'</div>    
        <select id="h1_fontfamily" name="h1_fontfamily">
            '.$h1_fontfamily.'
            <option value="">- original font -</option>
            '.$font_select.'                   
        </select> </td>
        <td>
        <div class="option_row_explain">'.esc_html__('Font Subset:','wpresidence').'</div>    
             <input type="text" id="h1_fontsubset" name="h1_fontsubset" value="'.$h1_fontsubset.'">
        </td>
        <td>
    <div class="option_row_explain">'.esc_html__('Font Size:','wpresidence').'</div>    
         <input type="number" id="h1_fontsize" name="h1_fontsize" value="'.$h1_fontsize.'" placeholder="in px"></td>
    </tr>
    <tr>
        <td>
        <div class="option_row_explain">'.esc_html__('Line Height:','wpresidence').'</div>    
             <input type="number" id="h1_lineheight" name="h1_lineheight" value="'.$h1_lineheight.'" placeholder="in px">
        </td>
        <td><div class="option_row_explain">'.esc_html__('Font Weight:','wpresidence').'</div>    
        <select id="h1_fontweight" name="h1_fontweight">
            '.$h1_fontweight.'
            <option value="">Original font weight</option>
            '.$font_weight.'                   
        </select> </td>
    </tr>
</table>
</div>
<div class="estate_option_row">
<table>
    <th style="text-align:left;"><div class="label_option_row">'.esc_html__('H2 Font','wpresidence').'</div><th>
    <tr><td><div class="option_row_explain">'.esc_html__('Font Family:','wpresidence').'</div>    
        <select id="h2_fontfamily" name="h2_fontfamily">
            '.$h2_fontfamily.'
            <option value="">- original font -</option>
            '.$font_select.'                   
        </select> </td>
        <td>
        <div class="option_row_explain">'.esc_html__('Font Subset:','wpresidence').'</div>    
             <input type="text" id="h2_fontsubset" name="h2_fontsubset" value="'.$h2_fontsubset.'">
        </td>
    <td>
    <div class="option_row_explain">'.esc_html__('Font Size:','wpresidence').'</div>    
         <input type="number" id="h2_fontsize" name="h2_fontsize" value="'.$h2_fontsize.'" placeholder="in px"></td>
    </tr>
    <tr><td>
    <div class="option_row_explain">'.esc_html__('Line Height:','wpresidence').'</div>    
         <input type="number" id="h2_lineheight" name="h2_lineheight" value="'.$h2_lineheight.'" placeholder="in px"></td>
         <td><div class="option_row_explain">'.esc_html__('Font Weight:','wpresidence').'</div>    
        <select id="h2_fontweight" name="h2_fontweight">
            '.$h2_fontweight.'
            <option value="">Original font weight</option>
            '.$font_weight.'                   
        </select> </td>
    </tr>
</table>
</div>
<div class="estate_option_row">
<table>
    <th style="text-align:left;"><div class="label_option_row">'.esc_html__('H3 Font','wpresidence').'</div><th>
    <tr><td><div class="option_row_explain">'.esc_html__('Font Family:','wpresidence').'</div>    
        <select id="h3_fontfamily" name="h3_fontfamily">
            '.$h3_fontfamily.'
            <option value="">- original font -</option>
            '.$font_select.'                   
        </select> </td>
<td>
        <div class="option_row_explain">'.esc_html__('Font Subset:','wpresidence').'</div>    
             <input type="text" id="h3_fontsubset" name="h3_fontsubset" value="'.$h3_fontsubset.'">
        </td>
    <td>
    <div class="option_row_explain">'.esc_html__('Font Size:','wpresidence').'</div>    
         <input type="number" id="h3_fontsize" name="h3_fontsize" value="'.$h3_fontsize.'" placeholder="in px"></td>
    </tr><td>
    <div class="option_row_explain">'.esc_html__('Line Height:','wpresidence').'</div>    
         <input type="number" id="h3_lineheight" name="h3_lineheight" value="'.$h3_lineheight.'" placeholder="in px"></td>
         </td>
        <td><div class="option_row_explain">'.esc_html__('Font Weight:','wpresidence').'</div>    
        <select id="h3_fontweight" name="h3_fontweight">
            '.$h3_fontweight.'
            <option value="">Original font weight</option>
            '.$font_weight.'                   
        </select> </td>
    <tr>
</table>
</div>
<div class="estate_option_row">
<table>
   <th style="text-align:left;"><div class="label_option_row">'.esc_html__('H4 Font','wpresidence').'</div><th>
    <tr><td><div class="option_row_explain">'.esc_html__('Font Family:','wpresidence').'</div>    
        <select id="h4_fontfamily" name="h4_fontfamily">
            '.$h4_fontfamily.'
            <option value="">- original font -</option>
            '.$font_select.'                   
        </select> </td>
        <td>
        <div class="option_row_explain">'.esc_html__('Font Subset:','wpresidence').'</div>    
             <input type="text" id="h4_fontsubset" name="h4_fontsubset" value="'.$h4_fontsubset.'">
        </td>
    <td>
    <div class="option_row_explain">'.esc_html__('Font Size:','wpresidence').'</div>    
         <input type="number" id="h4_fontsize" name="h4_fontsize" value="'.$h4_fontsize.'" placeholder="in px"></td>
         </tr><tr><td>
    <div class="option_row_explain">'.esc_html__('Line Height:','wpresidence').'</div>    
         <input type="number" id="h4_lineheight" name="h4_lineheight" value="'.$h4_lineheight.'" placeholder="in px"></td>
        <td><div class="option_row_explain">'.esc_html__('Font Weight:','wpresidence').'</div>    
        <select id="h4_fontweight" name="h4_fontweight">
            '.$h4_fontweight.'
            <option value="">Original font weight</option>
            '.$font_weight.'                   
        </select> </td> 
    </tr>
</table>
</div>
<div class="estate_option_row">
<table>
    <th style="text-align:left;"><div class="label_option_row">'.esc_html__('H5 Font','wpresidence').'</div><th>
    <tr><td><div class="option_row_explain">'.esc_html__('Font Family:','wpresidence').'</div>    
        <select id="h5_fontfamily" name="h5_fontfamily">
            '.$h5_fontfamily.'
            <option value="">- original font -</option>
            '.$font_select.'                   
        </select> </td>
        <td>
        <div class="option_row_explain">'.esc_html__('Font Subset:','wpresidence').'</div>    
             <input type="text" id="h5_fontsubset" name="h5_fontsubset" value="'.$h5_fontsubset.'">
        </td>
    <td>
    <div class="option_row_explain">'.esc_html__('Font Size:','wpresidence').'</div>    
         <input type="number" id="h5_fontsize" name="h5_fontsize" value="'.$h5_fontsize.'" placeholder="in px"></td>
         </tr><tr><td>
    <div class="option_row_explain">'.esc_html__('Line Height:','wpresidence').'</div>    
         <input type="number" id="h5_lineheight" name="h5_lineheight" value="'.$h5_lineheight.'" placeholder="in px"></td>
        <td><div class="option_row_explain">'.esc_html__('Font Weight:','wpresidence').'</div>    
        <select id="h5_fontweight" name="h5_fontweight">
            '.$h5_fontweight.'
            <option value="">Original font weight</option>
            '.$font_weight.'                   
        </select> </td>
    </tr>
</table>
</div>
<div class="estate_option_row">
<table>
    <th style="text-align:left;"><div class="label_option_row">'.esc_html__('H6 Font','wpresidence').'</div><th>
    <tr><td><div class="option_row_explain">'.esc_html__('Font Family:','wpresidence').'</div>    
        <select id="h6_fontfamily" name="h6_fontfamily">
            '.$h6_fontfamily.'
            <option value="">- original font -</option>
            '.$font_select.'                   
        </select> </td>
        <td>
        <div class="option_row_explain">'.esc_html__('Font Subset:','wpresidence').'</div>    
             <input type="text" id="h6_fontsubset" name="h6_fontsubset" value="'.$h6_fontsubset.'">
        </td>
    <td>
    <div class="option_row_explain">'.esc_html__('Font Size:','wpresidence').'</div>    
         <input type="number" id="h6_fontsize" name="h6_fontsize" value="'.$h6_fontsize.'" placeholder="in px"></td>
         </tr><tr><td>
    <div class="option_row_explain">'.esc_html__('Line Height:','wpresidence').'</div>    
         <input type="number" id="h6_lineheight" name="h6_lineheight" value="'.$h6_lineheight.'" placeholder="in px"></td>
         <td><div class="option_row_explain">'.esc_html__('Font Weight:','wpresidence').'</div>    
        <select id="h6_fontweight" name="h6_fontweight">
            '.$h6_fontweight.'
            <option value="">Original font weight</option>
            '.$font_weight.'                   
        </select> </td>
    </tr>
</table>
</div>
<div class="estate_option_row">
<table>
    <th style="text-align:left;"><div class="label_option_row">'.esc_html__('Paragraph Font','wpresidence').'</div><th>
    <tr><td><div class="option_row_explain">'.esc_html__('Font Family:','wpresidence').'</div>    
        <select id="p_fontfamily" name="p_fontfamily">
            '.$p_fontfamily.'
            <option value="">- original font -</option>
            '.$font_select.'                   
        </select> </td>
        <td>
        <div class="option_row_explain">'.esc_html__('Font Subset:','wpresidence').'</div>    
             <input type="text" id="p_fontsubset" name="p_fontsubset" value="'.$p_fontsubset.'">
        </td>
        <td>
    <div class="option_row_explain">'.esc_html__('Font Size:','wpresidence').'</div>    
         <input type="number" id="p_fontsize" name="p_fontsize" value="'.$p_fontsize.'" placeholder="in px"></td>
    </tr>
    <tr>
        <td>
        <div class="option_row_explain">'.esc_html__('Line Height:','wpresidence').'</div>    
             <input type="number" id="p_lineheight" name="p_lineheight" value="'.$p_lineheight.'" placeholder="in px">
        </td>
        <td><div class="option_row_explain">'.esc_html__('Font Weight:','wpresidence').'</div>    
        <select id="p_fontweight" name="p_fontweight">
            '.$p_fontweight.'
            <option value="">Original font weight</option>
            '.$font_weight.'                   
        </select> </td>
    </tr>
</table>
</div>
<div class="estate_option_row">
<table>
    <th style="text-align:left;"><div class="label_option_row">'.esc_html__('Menu Font','wpresidence').'</div><th>
    <tr><td><div class="option_row_explain">'.esc_html__('Font Family:','wpresidence').'</div>    
        <select id="menu_fontfamily" name="menu_fontfamily">
            '.$menu_fontfamily.'
            <option value="">- original font -</option>
            '.$font_select.'                   
        </select> </td>
        <td>
        <div class="option_row_explain">'.esc_html__('Font Subset:','wpresidence').'</div>    
             <input type="text" id="menu_fontsubset" name="menu_fontsubset" value="'.$menu_fontsubset.'">
        </td>
        <td>
    <div class="option_row_explain">'.esc_html__('Font Size:','wpresidence').'</div>    
         <input type="number" id="menu_fontsize" name="menu_fontsize" value="'.$menu_fontsize.'" placeholder="in px"></td>
    </tr>
    <tr>
        <td>
        <div class="option_row_explain">'.esc_html__('Line Height:','wpresidence').'</div>    
             <input type="number" id="menu_lineheight" name="menu_lineheight" value="'.$menu_lineheight.'" placeholder="in px">
        </td>
        <td><div class="option_row_explain">'.esc_html__('Font Weight:','wpresidence').'</div>    
        <select id="menu_fontweight" name="menu_fontweight">
            '.$menu_fontweight.'
            <option value="">Original font weight</option>
            '.$font_weight.'                   
        </select> </td>
    </tr>
</table>
</div>';
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
     print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';  
}
endif;











if( !function_exists('new_wpestate_membership_settings') ):    
function new_wpestate_membership_settings(){
    $free_mem_list                  =   esc_html( get_option('wp_estate_free_mem_list','') );
    $cache_array                    =   array('yes','no');  
   
    $paypal_array                   =   array('no','per listing','membership');
    $paid_submission_symbol         =   wpestate_dropdowns_theme_admin($paypal_array,'paid_submission');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Enable Paid Submission ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('No = submission is free. Paid listing = submission requires user to pay a fee for each listing. Membership = submission is based on user membership package.','wpresidence').'</div>    
        <select id="paid_submission" name="paid_submission">
            '.$paid_submission_symbol.'
        </select>
    </div>';
    
    
    $admin_submission_symbol        =   wpestate_dropdowns_theme_admin($cache_array,'admin_submission');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Submited Listings should be approved by admin?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('If yes, admin publishes each property submitted in front end manually.','wpresidence').'</div>    
        <select id="admin_submission" name="admin_submission">
            '.$admin_submission_symbol.'
        </select>
    </div>';
    
    
    $prop_image_number                   =   intval   ( get_option('wp_estate_prop_image_number','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Maximum no of images per property (only front-end upload). Works when submission is free or requires user to pay a fee for each listing','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('The maximum no of images a user can upload with the front end submit form. Use 0 for unlimited. This value is NOT used in case of membership mode (each package will have its own max image no set)','wpresidence').'</div>    
        <input type="text" id="prop_no" name="prop_image_number" value="'.$prop_image_number.'"> 
    </div>';
    
    $paypal_array                   =   array( 'sandbox','live' );
    $paypal_api_select              =   wpestate_dropdowns_theme_admin($paypal_array,'paypal_api');  
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Paypal & Stripe Api ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Sandbox = test API. LIVE = real payments API. Update PayPal and Stripe settings according to API type selection.','wpresidence').'</div>    
        <select id="paypal_api" name="paypal_api">
            '.$paypal_api_select.'
        </select>
    </div>';       
      
    $submission_curency_array       =   array(get_option('wp_estate_submission_curency_custom',''),'USD','EUR','AUD','BRL','CAD','CZK','DKK','HKD','HUF','ILS','INR','JPY','MYR','MXN','NOK','NZD','PHP','PLN','GBP','SGD','SEK','CHF','TWD','THB','TRY');
    $submission_curency_symbol      =   wpestate_dropdowns_theme_admin($submission_curency_array,'submission_curency');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Currency For Paid Submission','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('The currency in which payments are processed.','wpresidence').'</div>    
        <select id="submission_curency" name="submission_curency">
            '.$submission_curency_symbol.'
        </select> 
    </div>'; 
       
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Custom Currency Symbol - *select it from the list above after you add it.','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Add your own currency for Wire payments. ','wpresidence').'</div>    
        <input type="text" id="submission_curency_custom" name="submission_curency_custom" class="regular-text"  value="'.get_option('wp_estate_submission_curency_custom','').'"/>
    </div>'; 

      
    $enable_direct_pay_symbol       =   wpestate_dropdowns_theme_admin($cache_array,'enable_direct_pay');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Enable Direct Payment / Wire Payment?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Enable or disable the wire payment option.','wpresidence').'</div>    
        <select id="enable_direct_pay" name="enable_direct_pay">
            '.$enable_direct_pay_symbol.'
        </select>
    </div>'; 
    
    
    $args=array(
        'a' => array(
            'href' => array(),
            'title' => array()
        ),
        'br' => array(),
        'em' => array(),
        'strong' => array(),
    );
    $direct_payment_details         =   wp_kses( get_option('wp_estate_direct_payment_details','') ,$args);
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Wire instructions for direct payment','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('If wire payment is enabled, type the instructions below.','wpresidence').'</div>    
        <textarea id="direct_payment_details" rows="5" style="width:700px;" name="direct_payment_details"   class="regular-text" >'.$direct_payment_details.'</textarea> 
    </div>';   
    
    $price_submission               =   floatval( get_option('wp_estate_price_submission','') );   
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Price Per Submission (for "per listing" mode)','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Use .00 format for decimals (ex: 5.50). Do not set price as 0!','wpresidence').'</div>    
       <input  type="text" id="price_submission" name="price_submission"  value="'.$price_submission.'"/> 
    </div>'; 
      
    
    $price_featured_submission      =   floatval( get_option('wp_estate_price_featured_submission','') ); 
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Price to make the listing featured (for "per listing" mode)','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Use .00 format for decimals (ex: 1.50). Do not set price as 0!','wpresidence').'</div>    
       <input  type="text" id="price_featured_submission" name="price_featured_submission"  value="'.$price_featured_submission.'"/>
    </div>'; 
       
    
    $free_mem_list_unl='';
    if ( intval( get_option('wp_estate_free_mem_list_unl', '' ) ) == 1){
        $free_mem_list_unl=' checked="checked" ';  
    }
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Free Membership - no of listings (for "membership" mode)','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('If you change this value, the new value applies for new registered users. Old value applies for older registered accounts.','wpresidence').'</div>    
        <input  type="text" id="free_mem_list" name="free_mem_list" style="margin-right:20px;"  value="'.$free_mem_list.'"/> 
        <input type="hidden" name="free_mem_list_unl" value="">
        <input type="checkbox"  id="free_mem_list_unl" name="free_mem_list_unl" value="1" '.$free_mem_list_unl.' />
        <label for="free_mem_list_unl">'.esc_html__('Unlimited listings ?','wpresidence').'</label>
    </div>'; 
     
    $free_feat_list                 =   esc_html( get_option('wp_estate_free_feat_list','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Free Membership - no of featured listings (for "membership" mode)','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('If you change this value, the new value applies for new registered users. Old value applies for older registered accounts.','wpresidence').'</div>    
         <input  type="text" id="free_feat_list" name="free_feat_list" style="margin-right:20px;"    value="'.$free_feat_list.'"/>
    </div>';
        
    $free_feat_list_expiration      =  intval ( get_option('wp_estate_free_feat_list_expiration','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Free Membership Listings - no of days until a free listing will expire. *Starts from the moment the property is published on the website. (for "membership" mode) ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Option applies for each free published listing.','wpresidence').'</div>    
        <input  type="text" id="free_feat_list_expiration" name="free_feat_list_expiration" style="margin-right:20px;"    value="'.$free_feat_list_expiration.'"/>
    </div>';
    
    $free_pack_image_included       = intval ( get_option('wp_estate_free_pack_image_included','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Free Membership Images - no of images per listing ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Option applies for each free published listing.','wpresidence').'</div>    
        <input  type="text" id="free_pack_image_included" name="free_pack_image_included" style="margin-right:20px;"    value="'.$free_pack_image_included.'"/>
    </div>';
  
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';  
     
}    
endif;



if( !function_exists('new_wpestate_stripe_settings') ):  
 function  new_wpestate_stripe_settings(){
    $cache_array                    =   array('yes','no');  
      
    $enable_stripe_symbol           =   wpestate_dropdowns_theme_admin($cache_array,'enable_stripe');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Enable Stripe?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('You can enable or disable Stripe payment buttons.','wpresidence').'</div>    
        <select id="enable_stripe" name="enable_stripe">
            '.$enable_stripe_symbol.'
        </select>
    </div>';
    
    $stripe_secret_key              =   esc_html( get_option('wp_estate_stripe_secret_key','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Stripe Secret Key','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Info is taken from your account at https://dashboard.stripe.com/login','wpresidence').'</div>    
       <input  type="text" id="stripe_secret_key" name="stripe_secret_key"  class="regular-text" value="'.$stripe_secret_key.'"/> 
    </div>';
        
    $stripe_publishable_key         =   esc_html( get_option('wp_estate_stripe_publishable_key','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Stripe Publishable Key','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Info is taken from your account at https://dashboard.stripe.com/login','wpresidence').'</div>    
       <input  type="text" id="stripe_publishable_key" name="stripe_publishable_key" class="regular-text" value="'.$stripe_publishable_key.'"/>
    </div>';
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';  
}
endif;
 
 
 
if( !function_exists('new_wpestate_paypal_settings') ):   
function     new_wpestate_paypal_settings(){
      $cache_array                    =   array('yes','no');  
    $enable_paypal_symbol           =   wpestate_dropdowns_theme_admin($cache_array,'enable_paypal');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Enable Paypal?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('You can enable or disable PayPal buttons.','wpresidence').'</div>    
        <select id="enable_paypal" name="enable_paypal">
            '.$enable_paypal_symbol.'
        </select>
    </div>';
    
    $paypal_client_id               =   esc_html( get_option('wp_estate_paypal_client_id','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Paypal Client id','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('PayPal business account is required. Info is taken from https://developer.paypal.com/. See help.','wpresidence').'</div>    
        <input  type="text" id="paypal_client_id" name="paypal_client_id" class="regular-text"  value="'.$paypal_client_id.'"/>
    </div>'; 
     
    $paypal_client_secret           =   esc_html( get_option('wp_estate_paypal_client_secret','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Paypal Client Secret Key','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Info is taken from https://developer.paypal.com/ See help.','wpresidence').'</div>    
        <input  type="text" id="paypal_client_secret" name="paypal_client_secret"  class="regular-text" value="'.$paypal_client_secret.'"/> 
    </div>'; 
    
            
    $paypal_rec_email               =   esc_html( get_option('wp_estate_paypal_rec_email','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Paypal receiving email','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Info is taken from https://www.paypal.com/ or http://sandbox.paypal.com/ See help.','wpresidence').'</div>    
       <input  type="text" id="paypal_rec_email" name="paypal_rec_email"  class="regular-text" value="'.$paypal_rec_email.'"/>
    </div>';
    
     $paypal_api_username            =   esc_html( get_option('wp_estate_paypal_api_username','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Paypal Api User Name - Obsolete starting 1.30.1','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Info is taken from https://www.paypal.com/ or http://sandbox.paypal.com/ See help.','wpresidence').'</div>    
       <input  type="text" id="paypal_api_username" name="paypal_api_username"  class="regular-text" value="'.$paypal_api_username.'"/>
    </div>'; 
    
    $paypal_api_password            =   esc_html( get_option('wp_estate_paypal_api_password','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Paypal API Password - Obsolete starting 1.30.1','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Info is taken from https://www.paypal.com/ or http://sandbox.paypal.com/ See help.','wpresidence').'</div>    
       <input  type="text" id="paypal_api_password" name="paypal_api_password"  class="regular-text" value="'.$paypal_api_password.'"/>
    </div>'; 
        
    $paypal_api_signature           =   esc_html( get_option('wp_estate_paypal_api_signature','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Paypal API Signature - Obsolete starting 1.30.1','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Info is taken from https://www.paypal.com/ or http://sandbox.paypal.com/ See help.','wpresidence').'</div>    
       <input  type="text" id="paypal_api_signature" name="paypal_api_signature"  class="regular-text" value="'.$paypal_api_signature.'"/>
    </div>';

    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';  
}
endif; 
   
   
   
   
   
   
   
if( !function_exists('new_wpestate_pin_management') ):   
function new_wpestate_pin_management(){  
    $pins       =   array();
    $taxonomy   =   'property_action_category';
    $tax_terms  =   get_terms($taxonomy,'hide_empty=0');

    $taxonomy_cat = 'property_category';
    $categories = get_terms($taxonomy_cat,'hide_empty=0');

    // add only actions
    foreach ($tax_terms as $tax_term) {
        $name                    =  sanitize_key ( wpestate_limit64('wp_estate_'.$tax_term->slug) );
        $limit54                 =  sanitize_key ( wpestate_limit54($tax_term->slug) );
        $pins[$limit54]          =  esc_html( get_option($name) );  
    } 

    // add only categories
    foreach ($categories as $categ) {
        $name                           =   sanitize_key( wpestate_limit64('wp_estate_'.$categ->slug));
        $limit54                        =   sanitize_key(wpestate_limit54($categ->slug));
        $pins[$limit54]                 =   esc_html( get_option($name) );
    }
    
    // add combinations
    foreach ($tax_terms as $tax_term) {
        foreach ($categories as $categ) {
            $limit54            =   sanitize_key ( wpestate_limit27($categ->slug).wpestate_limit27($tax_term->slug) );
            $name               =   'wp_estate_'.$limit54;
            $pins[$limit54]     =   esc_html( get_option($name) ) ;        
        }
    }

  
    $name='wp_estate_idxpin';
    $pins['idxpin']=esc_html( get_option($name) );  

    $name='wp_estate_userpin';
    $pins['userpin']=esc_html( get_option($name) );  
   
    $taxonomy = 'property_action_category';
    $tax_terms = get_terms($taxonomy,'hide_empty=0');

    $taxonomy_cat = 'property_category';
    $categories = get_terms($taxonomy_cat,'hide_empty=0');

    print'<p class="admin-exp">'.esc_html__('Add new Google Maps pins for single actions / single categories. For speed reason, you MUST add pins if you change categories and actions names.','wpresidence').'</p>';
    print '<p class="admin-exp" >'.esc_html__('If you add images directly into the input fields (without Upload button) please use the full image path. For ex: http://www.wpresidence..... . If you use the "upload"  button use also "Insert into Post" button from the pop up window.','wpresidence');
    print '<p class="admin-exp" >'.esc_html__('Pins retina version must be uploaded at the same time (same folder) as the original pin and the name of the retina file should be with_2x at the end.','wpresidence').' <a href="http://help.wpresidence.net/2015/10/29/retina-pin-images/" target="_blank">'.esc_html__('For help go here!','wpresidence').'</a>';
      
    
    $cache_array=array('no','yes');
    $use_price_pins                 =   wpestate_dropdowns_theme_admin($cache_array,'use_price_pins');
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Use price Pins ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Use price Pins ?(The css class for price pins is "wpestate_marker" . Each pin has also receive a class with the name of the category or action: For example "wpestate_marker apartments sales")','wpresidence').'</div>    
        <select id="use_price_pins" name="use_price_pins">
            '.$use_price_pins.'
        </select>
    </div>';
    
    
    $use_price_pins_full_price                 =   wpestate_dropdowns_theme_admin($cache_array,'use_price_pins_full_price ');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Use Full Price Pins ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('If not we will show prices without before and after label and in this format : 5,23m or 6.83k ','wpresidence').'</div>    
        <select id="use_price_pins_full_price" name="use_price_pins_full_price">
            '.$use_price_pins_full_price.'
        </select>
    </div>';
    
    
    $use_single_image_pin                 =   wpestate_dropdowns_theme_admin($cache_array,'use_single_image_pin');
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Use single Image Pin ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('We will use 1 single pins for all markers. This option will decrease the loading time on you maps.','wpresidence').'</div>    
        <select id="use_single_image_pin" name="use_single_image_pin">
            '.$use_single_image_pin.'
        </select>
    </div>';
    
   
    foreach ($tax_terms as $tax_term) { 
            $limit54   =  $post_name  =   sanitize_key(wpestate_limit54($tax_term->slug));
            print'<div class="estate_option_row">
            <div class="label_option_row">'.esc_html__('For action ','wpresidence').'<strong>'.$tax_term->name.' </strong></div>
            <div class="option_row_explain">'.esc_html__('Image size must be 44px x 48px. ','wpresidence').'</div>    
                <input type="text"    class="pin-upload-form" size="36" name="'.$post_name.'" value="'.$pins[$limit54].'" />
                <input type="button"  class="upload_button button pin-upload" value="'.esc_html__('Upload Pin','wpresidence').'" />
            </div>';
            
               
    }
     
    
    foreach ($categories as $categ) {  
            $limit54   =   $post_name  =   sanitize_key(wpestate_limit54($categ->slug));
            print'<div class="estate_option_row">
            <div class="label_option_row">'.esc_html__('For category: ','wpresidence').'<strong>'.$categ->name.' </strong>  </div>
            <div class="option_row_explain">'.esc_html__('Image size must be 44px x 48px. ','wpresidence').'</div>    
                <input type="text"    class="pin-upload-form" size="36" name="'.$post_name.'" value="'.$pins[$limit54].'" />
                <input type="button"  class="upload_button button pin-upload" value="'.esc_html__('Upload Pin','wpresidence').'"  />
            </div>';
                 
    }
    
    
    print '<p class="admin-exp">'.esc_html__('Add new Google Maps pins for actions & categories combined (example: \'apartments in sales\')','wpresidence').'</p>';  
      
    foreach ($tax_terms as $tax_term) {
    
        foreach ($categories as $categ) {
            $limit54=sanitize_key(wpestate_limit27($categ->slug)).sanitize_key( wpestate_limit27($tax_term->slug) );
            
            print'<div class="estate_option_row">
            <div class="label_option_row">'.esc_html__('For action','wpresidence').' <strong>'.$tax_term->name.'</strong>, '.esc_html__('category','wpresidence').': <strong>'.$categ->name.'</strong>   </div>
            <div class="option_row_explain">'.esc_html__('Image size must be 44px x 48px.','wpresidence').'  </div>    
                <input id="'.$limit54.'" type="text" size="36" name="'. $limit54.'" value="'.$pins[$limit54].'" />
                <input type="button"  class="upload_button button pin-upload" value="'.esc_html__('Upload Pin','wpresidence').'" />
            </div>';
                
        }
    }


    print'<div class="estate_option_row">
            <div class="label_option_row">'.esc_html__('For IDX (if plugin is enabled) ','wpresidence').'</div>
            <div class="option_row_explain">'.esc_html__('For IDX (if plugin is enabled) ','wpresidence').'</div>    
                <input id="idxpin" type="text" size="36" name="idxpin" value="'.$pins['idxpin'].'" />
                <input type="button"  class="upload_button button pin-upload" value="'.esc_html__('Upload Pin','wpresidence').'" />
            </div>';
    
    
     print'<div class="estate_option_row">
            <div class="label_option_row">'.esc_html__('Userpin in geolocation','wpresidence').'</div>
            <div class="option_row_explain">'.esc_html__('Userpin in geolocation','wpresidence').'</div>    
                <input id="userpin" type="text" size="36" name="userpin" value="'.$pins['userpin'].'" />
                <input type="button"  class="upload_button button pin-upload" value="'.esc_html__('Upload Pin','wpresidence').'" />
            </div>';
     
     print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>'; 
}
endif;



if( !function_exists('wpestate_geo_location_tab') ):   
    function    wpestate_geo_location_tab(){
 
        $value_array=array('no','yes');
        $use_geo_location_select = wpestate_dropdowns_theme_admin($value_array,'use_geo_location');
        print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Use Geo Location Search','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('If yes, the geo location search will appear above half map search fields','wpresidence').'</div>    
            <select id="use_geo_location" name="use_geo_location">
                '.$use_geo_location_select.'
            </select> 
        </div>';
        
        
        $initial_radius            =    esc_html( get_option('wp_estate_initial_radius') ) ;      
        print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Initial area radius ','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Initial area radius ','wpresidence').'</div>    
            <input  type="text" id="initial_radius"  name="initial_radius"   value="'.$initial_radius.'" size="40"/>
        </div>';
        
        $min_geo_radius            =    esc_html( get_option('wp_estate_min_geo_radius') ) ;      
        print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Minimum radius vale','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Minimum radius value','wpresidence').'</div>    
            <input  type="text" id="min_geo_radius"  name="min_geo_radius"   value="'.$min_geo_radius.'" size="40"/>
        </div>';
        
        
        $max_geo_radius           =    esc_html( get_option('wp_estate_max_geo_radius') ) ;      
        print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Maximum radius value','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Maximum radius value','wpresidence').'</div>    
            <input  type="text" id="max_geo_radius"  name="max_geo_radius"   value="'.$max_geo_radius.'" size="40"/>
        </div>';
        
        
        $value_array_geo=array( esc_html__('miles','wpresidence'),esc_html__('km','wpresidence') );
        $geo_radius_measure_select = wpestate_dropdowns_theme_admin($value_array_geo,'geo_radius_measure');
        print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Use Geo Location Search','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('If yes, the geo location search will appear above half map search fields','wpresidence').'</div>    
            <select id="geo_radius_measure" name="geo_radius_measure">
                '.$geo_radius_measure_select.'
            </select> 
        </div>';
        
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
    }
endif;



if( !function_exists('wpestate_save_search_tab') ):   
function    wpestate_save_search_tab(){
    $cache_array                    =   array('yes','no'); 
    $show_save_search_select            = wpestate_dropdowns_theme_admin($cache_array,'show_save_search');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Use Saved Search Feature ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('If yes, user can save his searchs from Advanced Search Results, if he is logged in with a registered account.','wpresidence').'</div>    
        <select id="show_save_search" name="show_save_search">
            '.$show_save_search_select.'
        </select> 
    </div>';
    
    
    $period_array   =array( 0 =>esc_html__('daily','wpresidence'),
                            1 =>esc_html__('weekly','wpresidence') 
                            );
    
    $search_alert_select = wpestate_dropdowns_theme_admin_with_key($period_array,'search_alert');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Send emails','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Send weekly or daily an email alert with new published properties matching user saved searches.','wpresidence').'</div>    
        <select id="search_alert" name="search_alert">
            '.$search_alert_select.'
        </select>
    </div>';

    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;

if( !function_exists('wpestate_search_colors_tab') ):   
function    wpestate_search_colors_tab(){
    
    
    
    $adv_back_color              =  esc_html ( get_option('wp_estate_adv_back_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Advanced Search Background Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Advanced Search Background Color','wpresidence').'</div>    
        <input type="text" name="adv_back_color" value="'.$adv_back_color.'" maxlength="7" class="inptxt" />
        <div id="adv_back_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$adv_back_color.';" ></div></div>
    </div>';
        
    $adv_back_color_opacity             =  esc_html ( get_option('wp_estate_adv_back_color_opacity','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Advanced Search Background color Opacity','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Values between 0 -invisible and 1 - fully visible','wpresidence').'</div>    
        <input type="text" name="adv_back_color_opacity" value="'.$adv_back_color_opacity.'"  class="inptxt" />
    </div>';

    
    $adv_font_color              =  esc_html ( get_option('wp_estate_adv_font_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Advanced Search Font Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Advanced Search Font Color','wpresidence').'</div>    
        <input type="text" name="adv_font_color" value="'.$adv_font_color.'" maxlength="7" class="inptxt" />
        <div id="adv_font_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$adv_font_color.';" ></div></div>
    </div>';
    
    $adv_search_back_color          =  esc_html ( get_option('wp_estate_adv_search_back_color ','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Map Advanced Search Button Background Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Map Advanced Search Button Background Color','wpresidence').'</div>    
        <input type="text" name="adv_search_back_color" value="'.$adv_search_back_color.'" maxlength="7" class="inptxt" />
        <div id="adv_search_back_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$adv_search_back_color.';"></div></div>
    </div>';
    
    $adv_search_font_color          =  esc_html ( get_option('wp_estate_adv_search_font_color','') );  
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Advanced Search Fields Font Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Advanced Search Fields Font Color','wpresidence').'</div>    
        <input type="text" name="adv_search_font_color" value="'.$adv_search_font_color.'" maxlength="7" class="inptxt" />
        <div id="adv_search_font_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$adv_search_font_color.';"></div></div>
    </div>';
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;

if( !function_exists('new_wpestate_advanced_search_form') ):   
function    new_wpestate_advanced_search_form(){
       
    $value_array=array('no','yes');
    $custom_advanced_search_select = wpestate_dropdowns_theme_admin($value_array,'custom_advanced_search');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Use Custom Fields For Advanced Search ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('If yes, you can set your own custom fields in the  spots available. See help for correct setup.','wpresidence').'</div>    
        <select id="custom_advanced_search" name="custom_advanced_search">
            '.$custom_advanced_search_select.'
        </select> 
    </div>';
  
    
    $search_on_start_select = wpestate_dropdowns_theme_admin($value_array,'search_on_start');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Put Search form before the header media ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Works with "Use FLoat Form" options set to no ! Doesn\'t apply to search type 3.','wpresidence').'</div>    
        <select id="search_on_start" name="search_on_start">
            '.$search_on_start_select.'
        </select> 
    </div>';
  
    $sticky_search_select = wpestate_dropdowns_theme_admin($value_array,'sticky_search');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Use sticky search ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('This will replace the sticky header.  Doesn\'t apply to search type 3','wpresidence').'</div>    
        <select id="sticky_search" name="sticky_search">
            '.$sticky_search_select.'
        </select> 
    </div>';
    
    
    $use_float_search_form_select = wpestate_dropdowns_theme_admin($value_array,'use_float_search_form');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Use Float Search Form ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('The search form is "floating" over the media header and you set the position.','wpresidence').'</div>    
        <select id="use_float_search_form" name="use_float_search_form">
            '.$use_float_search_form_select.'
        </select> 
    </div>';
    
    
        
    $float_form_top             =    esc_html( get_option('wp_estate_float_form_top') ) ;      
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Distance betwen search form and the top margin of the browser: Ex 200px or 20%','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Distance betwen search form and the top margin of the browser: Ex 200px or 20%.','wpresidence').'</div>    
        <input  type="text" id="float_form_top"  name="float_form_top"   value="'.$float_form_top.'" size="40"/>
    </div>';
    
    
           
    $float_form_top_tax             =    esc_html( get_option('wp_estate_float_form_top_tax')  );      
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Distance betwen search form and the top margin of the browser in px Ex 200px or 20% - for taxonomy, category and archives pages','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Distance betwen search form and the top margin of the browser in px Ex 200px or 20% - for taxonomy, category and archives pages.','wpresidence').'</div>    
        <input  type="text" id="float_form_top_tax"  name="float_form_top_tax"   value="'.$float_form_top_tax.'" size="40"/>
    </div>';
  
    $adv_search_fields_no             =    ( floatval( get_option('wp_estate_adv_search_fields_no') ) );      
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('No of Search fields','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('No of Search fields.','wpresidence').'</div>    
        <input  type="text" id="adv_search_fields_no"  name="adv_search_fields_no"   value="'.$adv_search_fields_no.'" size="40"/>
    </div>';
    
    $adv_search_fields_no_per_row             =    ( floatval( get_option('wp_estate_search_fields_no_per_row') ) );      
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('No of Search fields per row','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('No of Search fields per row (Possible values: 1,2,3,4).','wpresidence').'</div>    
        <input  type="text" id="search_fields_no_per_row"  name="search_fields_no_per_row"   value="'.$adv_search_fields_no_per_row.'" size="40"/>
    </div>';
    
    $tax_array      =array( 'none'                      =>esc_html__('none','wpresidence'),
                            'property_category'         =>esc_html__('Categories','wpresidence'),
                            'property_action_category'  =>esc_html__('Action Categories','wpresidence'),
                            'property_city'             =>esc_html__('City','wpresidence'),
                            'property_area'             =>esc_html__('Area','wpresidence'),
                            'property_county_state'     =>esc_html__('County State','wpresidence'),
                            );
    
    $adv6_taxonomy_select   =   wpestate_dropdowns_theme_admin_with_key($tax_array,'adv6_taxonomy');
    $adv6_taxonomy          =   get_option('wp_estate_adv6_taxonomy');
    $adv6_taxonomy_terms    =   get_option('wp_estate_adv6_taxonomy_terms');     
    $adv6_max_price         =   get_option('wp_estate_adv6_max_price');     
    $adv6_min_price         =   get_option('wp_estate_adv6_min_price');     
    //adv6_taxonomy
    //$adv6_taxonomy_terms
    //adv6_min_price
    //adv6_max_price
    
    

        
    print'<div class="estate_option_row var_price_sliders">
    <div class="label_option_row">'.esc_html__('Select Taxonomy for tabs options in Advanced Search Type 6, Type 7, Type 8, Type 9','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('This applies for  the search over media header.','wpresidence').'</div>    
        <select id="adv6_taxonomy" name="adv6_taxonomy">
            '.$adv6_taxonomy_select.'
        </select> 
    

        
		<input type="hidden" name="adv6_taxonomy_terms" />
        <select id="adv6_taxonomy_terms" name="adv6_taxonomy_terms[]" multiple="multiple" style="';
        if($adv6_taxonomy==''){print 'display:none;';}
        print 'height:200px;">'; 
        
        if($adv6_taxonomy !=='' ){
            $terms = get_terms( array(
                'taxonomy' => $adv6_taxonomy,
                'hide_empty' => false,
                'orderby'   =>'ID',
                'order'     =>'ASC'
            ) );
       
            foreach($terms as $term){
                print '<option value="'.$term->term_id.'" ';
                if(is_array($adv6_taxonomy_terms) && in_array($term->term_id, $adv6_taxonomy_terms) ){
                    print ' selected= "selected" ';
                }
                
                print' >';
                if(isset($term->name)){
                    print $term->name;
                }
                print '</option>';
            }      
    
        }
        
        
        print'</select>';

        print '<div style="margin-bottom:30px;"></div>';
        $i=0;
        
        if(is_array($adv6_taxonomy_terms)){

            print '<div class="label_option_row" style="margin-bottom:10px;">'.esc_html__('Price SLider values for advanced search with tabs','wpresidence').'</div>';
            foreach ($adv6_taxonomy_terms as $term_id){
                $term = get_term( $term_id, $adv6_taxonomy);

                print '<div class="field_row">
                    <div class="field_item">'.esc_html__('Price Slider Values(min/max) for ','wpresidence').$term->name.'</div>

                    <div class="field_item">
                       <input type="text" id="adv6_min_price" name="adv6_min_price[]" value="';

                        if( isset( $adv6_min_price[$i]) ){
                            print $adv6_min_price[$i];
                        }
                        print'">
                    </div>

                    <div class="field_item">
                        <input type="text" id="adv6_max_price" name="adv6_max_price[]" value="';
                        if( isset( $adv6_max_price[$i]) ){
                            print $adv6_max_price[$i];
                        }
                        print'">
                    </div>
                </div>';
                $i++;

            }
            
        }
       

print'</div>';

      print'<div class="" style="width:100%;float:left;margin-bottom:20px;"></div>';
    
    
    
    $custom_advanced_search= get_option('wp_estate_custom_advanced_search','');
    $adv_search_what    = get_option('wp_estate_adv_search_what','');
    $adv_search_how     = get_option('wp_estate_adv_search_how','');
    $adv_search_label   = get_option('wp_estate_adv_search_label','');

      
    print '<div class="estate_option_row">';
    print '<div id="custom_fields_search">';   
    print '<div class="field_row">
    <div class="field_item"><strong>'.esc_html__('Place in advanced search form','wpresidence').'</strong></div>
    <div class="field_item"><strong>'.esc_html__('Search field','wpresidence').'</strong></div>
    <div class="field_item"><strong>'.esc_html__('How it will compare','wpresidence').'</strong></div>
    <div class="field_item"><strong>'.esc_html__('Label on Front end','wpresidence').'</strong></div>
    </div>';
    
   
        
        
    $i=0;
    while( $i < $adv_search_fields_no ){
        $i++;
    
        print '<div class="field_row">
        <div class="field_item">'.esc_html__('Spot no ','wpresidence').$i.'</div>
        
        <div class="field_item">
            <select id="adv_search_what'.$i.'" name="adv_search_what[]">';
                print   wpestate_show_advanced_search_options($i-1,$adv_search_what);
            print'</select>
        </div>
        
        <div class="field_item">
            <select id="adv_search_how'.$i.'" name="adv_search_how[]">';
                print  wpestate_show_advanced_search_how($i-1,$adv_search_how);
        
                $new_val=''; 
                if( isset($adv_search_label[$i-1]) ){
                    $new_val=$adv_search_label[$i-1]; 
                }
        print '</select>
        </div>
        
        <div class="field_item"><input type="text" id="adv_search_label'.$i.'" name="adv_search_label[]" value="'.$new_val.'"></div>
        </div>';

    }
    print'</div>';
    print'    
        <p style="margin-left:10px;">
         '.esc_html__('*Do not duplicate labels and make sure search fields do not contradict themselves','wpresidence').'</br>
        '.esc_html__('*Labels will not apply for taxonomy dropdowns fields','wpresidence').'</br>
      
        </p>';
    
    print'</div>';
        
    $feature_list       =   esc_html( get_option('wp_estate_feature_list') );
    $feature_list_array =   explode( ',',$feature_list);
    foreach($feature_list_array as $checker => $value){
        $feature_list_array[$checker]= stripslashes($value);
    }
    
  
    $advanced_exteded =  get_option('wp_estate_advanced_exteded');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Amenities and Features for Advanced Search?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Select which features and amenities show in search.','wpresidence').'</div>';    
        
        print ' <p style="margin-left:10px;">  '.esc_html__('*Hold CTRL for multiple selection','wpresidence').'</p>
		
        <input type="hidden" name="advanced_exteded[]" value="none">
        <select name="advanced_exteded[]" multiple="multiple" style="height:400px;">';
        foreach($feature_list_array as $checker => $value){
            $value          =   stripslashes($value);
            $post_var_name  =   str_replace(' ','_', trim($value) );
            
            
            print '<option value="'.$post_var_name.'"' ;
            if(is_array($advanced_exteded)){
                if( in_array ($post_var_name,$advanced_exteded) ){
                    print ' selected="selected" ';
                } 
            }
            
            print '>'.stripslashes($value).'</option>';                
        }
        print '</select>';
        
    print'</div>';
         
    
 
       
      
        
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';  

       // wpestate_theme_admin_adv_search();
}
endif;



if( !function_exists('new_wpestate_advanced_search_settings') ):  
 function new_wpestate_advanced_search_settings(){
    $cache_array                    =   array('yes','no');
    
    $custom_advanced_search= get_option('wp_estate_custom_advanced_search','');
    $adv_search_what    = get_option('wp_estate_adv_search_what','');
    $adv_search_how     = get_option('wp_estate_adv_search_how','');
    $adv_search_label   = get_option('wp_estate_adv_search_label','');
    
    
    
    $value_array=array('no','yes');
    $search_array = array (1,2,3,4,5,6,'7-obsolete',8,'9-obsolete','10','11');
    $show_adv_search_type= wpestate_dropdowns_theme_admin($search_array,'adv_search_type',esc_html__('Type','wpresidence').' ');
    
    $show_adv_search_tax_select         = wpestate_dropdowns_theme_admin($cache_array,'show_adv_search_tax');
    $show_adv_search_general_select     = wpestate_dropdowns_theme_admin($cache_array,'show_adv_search_general');
    $show_adv_search_slider_select      = wpestate_dropdowns_theme_admin($cache_array,'show_adv_search_slider');
    $show_adv_search_visible_select     = wpestate_dropdowns_theme_admin($cache_array,'show_adv_search_visible');
    $show_adv_search_extended_select    = wpestate_dropdowns_theme_admin($cache_array,'show_adv_search_extended');
    $show_slider_price_select           = wpestate_dropdowns_theme_admin($cache_array,'show_slider_price');
    $show_dropdowns_select              = wpestate_dropdowns_theme_admin($cache_array,'show_dropdowns');
    
    
    
    

    
 
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Advanced Search Type ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('This applies for the search over header type. IMPORTANT! Enable Advanced Search Custom Fields - YES, for Type 5, Type 6, Type 8, Type 10 and Type 11. Types 7 & 9 look can be obtained with type 6 and 8 with float form set to yes.','wpresidence').'</div>    
        <select id="adv_search_type" name="adv_search_type">
                    '.$show_adv_search_type.'
                </select> 
    </div>';           
     
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show Advanced Search ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Disables or enables the display of advanced search ','wpresidence').'</div>    
        <select id="show_adv_search_general" name="show_adv_search_general">
            '.$show_adv_search_general_select.'
        </select>
    </div>';    
     
    $show_empty_city_status_symbol      = wpestate_dropdowns_theme_admin($cache_array,'show_empty_city');  
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show Cities and Areas with 0 properties in advanced search','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Enable or disable listing empty city or area categories in dropdowns.','wpresidence').'</div>    
        <select id="show_empty_city" name="show_empty_city">
            '.$show_empty_city_status_symbol.'
        </select>
    </div>';
        
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show Advanced Search in Taxonomies, Categories or Archives ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Disables or enables the display of advanced search in taxonomies, categories and archives','wpresidence').'</div>    
        <select id="show_adv_search_tax" name="show_adv_search_tax">
            '.$show_adv_search_tax_select.'
        </select>
    </div>';  
        
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Keep Advanced Search visible? (*only for Type 1,3 and 4)','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('If no, advanced search over header will display in closed position by default. ','wpresidence').'</div>    
        <select id="show_adv_search_visible" name="show_adv_search_visible">
            '.$show_adv_search_visible_select.'
        </select>
    </div>';
     
     
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show Amenities and Features fields?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Select what features from Advanced Search Form. *for speed reasons, the "features checkboxes" will not filter the pins on the map','wpresidence').'</div>    
        <select id="show_adv_search_extended" name="show_adv_search_extended">
            '.$show_adv_search_extended_select.'
        </select>
    </div>';
        
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show Dropdowns for beds, bathrooms or rooms?(*only works with Custom Fields - YES)','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Custom Fields are enabled and set from Advanced Search Form.','wpresidence').'</div>    
        <select id="show_dropdowns" name="show_dropdowns">
            '.$show_dropdowns_select.'
        </select>
    </div>';
      
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show Slider for Price?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('If no, price field can still be used in search and it will be input type.','wpresidence').'</div>    
        <select id="show_slider_price" name="show_slider_price">
            '.$show_slider_price_select.'
        </select>
    </div>';
        
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Minimum and Maximum value for Price Slider','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Type only numbers!','wpresidence').'</div>    
        <input type="text" name="show_slider_min_price"  class="inptxt " value="'.floatval(get_option('wp_estate_show_slider_min_price','')).'"/>
        -   
        <input type="text" name="show_slider_max_price"  class="inptxt " value="'.floatval(get_option('wp_estate_show_slider_max_price','')).'"/>
    </div>';
           
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';  
 } 
 endif;       
 
 
 
if( !function_exists('new_wpestate_custom_fields') ):   
function new_wpestate_custom_fields(){
   
    $custom_fields = get_option( 'wp_estate_custom_fields', true);     
    $current_fields='';

    
    $i=0;
    if( !empty($custom_fields)){    
        while($i< count($custom_fields) ){
            $current_fields.='
                <div class=field_row>
                <div    class="field_item"><strong>'.esc_html__('Field Name','wpresidence').'</strong></br><input  type="text" name="add_field_name[]"   value="'.stripslashes( $custom_fields[$i][0] ).'"  ></div>
                <div    class="field_item"><strong>'.esc_html__('Field Label','wpresidence').'</strong></br><input  type="text" name="add_field_label[]"   value="'.stripslashes( $custom_fields[$i][1]).'"  ></div>
                <div    class="field_item"><strong>'.esc_html__('Field Type','wpresidence').'</strong></br>'.wpestate_fields_type_select($custom_fields[$i][2]).'</div>
                <div    class="field_item"><strong>'.esc_html__('Field Order','wpresidence').'</strong></br><input  type="text" name="add_field_order[]" value="'.$custom_fields[$i][3].'"></div>     
                <div    class="field_item newfield"><strong>'.esc_html__('Dropdown values','wpresidence').'</strong></br><textarea name="add_dropdown_order[]">';
                
                if( isset($custom_fields[$i][4])){
                    $current_fields.= $custom_fields[$i][4];
                }    
                $current_fields.='</textarea></div>     
             
                <a class="deletefieldlink" href="#">'.esc_html__('delete','wpresidence').'</a>
            </div>';    
            $i++;
        }
    }
 

    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Custom Fields list','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Add, edit or delete property custom fields.','wpresidence').'</div>    
        <div id="custom_fields_wrapper">
        '.$current_fields.'
        <input type="hidden" name="is_custom" value="1">   
        </div>
    </div>';
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Add New Custom Field','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Fill the form in order to add a new custom field','wpresidence').'</div>  
     
        <div class="add_curency">
            <div class="cur_explanations">'.esc_html__('Field name','wpresidence').'</div>
            <input  type="text" id="field_name"  name="field_name"   value=""/>
            
            <div class="cur_explanations">'.esc_html__('Field Label','wpresidence').'</div>
             <input  type="text" id="field_label"  name="field_label"   value="" />
            
            <div class="cur_explanations">'.esc_html__('Field Type','wpresidence').'</div>
                <select id="field_type" name="field_type">
                    <option value="short text">short text</option>
                    <option value="long text">long text</option>
                    <option value="numeric">numeric</option>
                    <option value="date">date</option>
                    <option value="dropdown">dropdown</option>
                </select>
            

            <div class="cur_explanations">'.esc_html__(' Order in listing page','wpresidence').'</div>
            <input  type="text" id="field_order"  name="field_order"   value="" />
                
            <div class="cur_explanations">'.esc_html__('Dropdown values separated by "," (only for dropdown field type)','wpresidence').'</div>
            <textarea id="drodown_values"  name="drodown_values"  style="width:300px;"></textarea>
            
            </br>
            <a href="#" id="add_field">'.esc_html__(' click to add field','wpresidence').'</a>
        </div>
        
        
    </div>'; 
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';   

}
endif;


if( !function_exists('new_wpestate_ammenities_features') ):   
function new_wpestate_ammenities_features(){
    $feature_list                           =   esc_html( get_option('wp_estate_feature_list') );
    $feature_list                           =   str_replace(', ',',&#13;&#10;',$feature_list);
    
    $cache_array=array('yes','no');
    $show_no_features_symbol =  wpestate_dropdowns_theme_admin($cache_array,'show_no_features');
    
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Add New Element in Features and Amenities','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Type and add a new item in features and amenities list.','wpresidence').'</div>    
        <input  type="text" id="new_feature"  name="new_feature"   value="type here feature name.. " size="40"/><br>
        <a href="#" id="add_feature"> click to add feature </a><br>
    </div>';
  
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Features and Amenities list','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('List of already added features and amenities','wpresidence').'</div>   
    <div class="option_row_explain">'.esc_html__('After adding a new feature,  make sure you select it to be part of the property submission form from Membership -> Property Submission Page. ','wpresidence').'</div>   
        <textarea id="feature_list" name="feature_list" rows="15" cols="42">'.stripslashes( $feature_list).'</textarea> 
    </div>';
      
     print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show the Features and Amenities that are not available','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Show on property page the features and amenities that are not selected?','wpresidence').'</div>    
        <select id="show_no_features" name="show_no_features">
            '.$show_no_features_symbol.'
        </select> 
    </div>';
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;






if( !function_exists('wpestate_splash_page') ):   
function wpestate_splash_page(){
    $type_array=array('image','video','image slider');
    $spash_header_type_symbol =  wpestate_dropdowns_theme_admin($type_array,'spash_header_type');
   
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__(' Select the splash page type.','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Important: Create also a page with template "Splash Page" to see how your splash settings apply ','wpresidence').'</div>    
        <select id="spash_header_type" name="spash_header_type">
            '.$spash_header_type_symbol.'
        </select> 
    </div>';
    
   
    

    
    $splash_image                  =   esc_html( get_option('wp_estate_splash_image','') );
    print'<div class="estate_option_row splash_image_info">
        <div class="label_option_row">'.esc_html__('Splash Image','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Splash Image, .png, .jpg or .gif format','wpresidence').'</div>    
            <input id="splash_image" type="text" size="36" name="splash_image" value="'.$splash_image.'" />
            <input id="splash_image_button" type="button"  class="upload_button button" value="'.esc_html__('Upload Image','wpresidence').'" />     
    </div>';   
    
    
    $splash_slider_gallery                  =   esc_html( get_option('wp_estate_splash_slider_gallery','') );
    print'<div class="estate_option_row splash_slider_info" id="splash_slider_images">
        <div class="label_option_row">'.esc_html__(' Slider Images','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Slider Images, .png, .jpg or .gif format','wpresidence').'</div>    
            <input type="hidden" id="splash_slider_gallery" type="text" size="36" name="splash_slider_gallery" value="'.$splash_slider_gallery.'" />
            <input id="splash_slider_gallery_button" type="button"  class="upload_button button" value="'.esc_html__('Select Images','wpresidence').'" /> ';
    
    $splash_slider_gallery_array= explode(',', $splash_slider_gallery);
    print '  <div class="splash_thumb_wrapepr">';
    if(is_array($splash_slider_gallery_array)){
        foreach ($splash_slider_gallery_array as $image_id) {
            if($image_id!=''){
                $preview            =   wp_get_attachment_image_src($image_id, 'thumbnail');
                if($preview[0]!=''){
                    print '<div class="uploaded_thumb" data-imageid="'.$image_id.'">
                        <img  src="'.$preview[0].'"  alt="slider" />
                        <span class="splash_attach_delete">x</span>
                    </div>';
                }
            }
        }
    }
    
    print'            
     </div>     
    </div>';   
    
    
    $splash_slider_transition             =  esc_html ( get_option('wp_estate_splash_slider_transition','') );
    print'<div class="estate_option_row splash_slider_info">
    <div class="label_option_row">'.esc_html__('Slider Transition','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Slider Transition Period','wpresidence').'</div>    
        <input type="text" name="splash_slider_transition" value="'.$splash_slider_transition.'"  class="inptxt" />
    </div>';
    
    
    $splash_video_mp4                  =   esc_html( get_option('wp_estate_splash_video_mp4','') );
    print'<div class="estate_option_row splash_video_info">
        <div class="label_option_row">'.esc_html__('Splash Video in mp4 format','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Splash Video in mp4 format ','wpresidence').'</div>    
            <input id="splash_video_mp4" type="text" size="36" name="splash_video_mp4" value="'.$splash_video_mp4.'" />
            <input id="splash_video_mp4_button" type="button"  class="upload_button button" value="'.esc_html__('Upload Video','wpresidence').'" />    
    </div>';  
    
    $splash_video_webm                  =   esc_html( get_option('wp_estate_splash_video_webm','') );
    print'<div class="estate_option_row splash_video_info">
        <div class="label_option_row">'.esc_html__('Splash Video in webm format','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Splash Video in webm format ','wpresidence').'</div>    
            <input id="splash_video_webm" type="text" size="36" name="splash_video_webm" value="'.$splash_video_webm.'" />
            <input id="splash_video_webm_button" type="button"  class="upload_button button" value="'.esc_html__('Upload Video','wpresidence').'" />    
    </div>';  
    
    $splash_video_ogv                  =   esc_html( get_option('wp_estate_splash_video_ogv','') );
    print'<div class="estate_option_row splash_video_info">
        <div class="label_option_row">'.esc_html__('Splash Video in ogv format','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Splash Video in ogv format ','wpresidence').'</div>    
            <input id="splash_video_ogv" type="text" size="36" name="splash_video_ogv" value="'.$splash_video_ogv.'" />
            <input id="splash_video_ogv_button" type="button"  class="upload_button button" value="'.esc_html__('Upload Video','wpresidence').'" />    
    </div>';  
    
    $splash_video_cover_img                  =   esc_html( get_option('wp_estate_splash_video_cover_img','') );
    print'<div class="estate_option_row splash_video_info">
        <div class="label_option_row">'.esc_html__('Cover Image for video','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Cover Image for video','wpresidence').'</div>    
            <input id="splash_video_cover_img" type="text" size="36" name="splash_video_cover_img" value="'.$splash_video_cover_img.'" />
            <input id="splash_video_cover_img_button" type="button"  class="upload_button button" value="'.esc_html__('Upload Image','wpresidence').'" />    
    </div>';  
    
    
    
    $splash_overlay_image                  =   esc_html( get_option('wp_estate_splash_overlay_image','') );
    print'<div class="estate_option_row ">
        <div class="label_option_row">'.esc_html__('Overlay Image','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Overlay Image, .png, .jpg or .gif format','wpresidence').'</div>    
            <input id="wp_estate_splash_overlay_image" type="text" size="36" name="splash_overlay_image" value="'.$splash_overlay_image.'" />
            <input id="wp_estate_splash_overlay_image_button" type="button"  class="upload_button button" value="'.esc_html__('Upload Image','wpresidence').'" />     
    </div>';   
    
    
    $splash_overlay_color                     =  esc_html ( get_option('wp_estate_splash_overlay_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Overlay Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Overlay Color','wpresidence').'</div>    
        <input type="text" name="splash_overlay_color" maxlength="7" class="inptxt " value="'.$splash_overlay_color.'"/>
        <div id="splash_overlay_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$splash_overlay_color.';"  ></div></div>
    </div>';  
  
    
    $splash_overlay_opacity             =  esc_html ( get_option('wp_estate_splash_overlay_opacity','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Overlay Opacity','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Overlay Opacity- values from 0 to 1 , Ex: 0.4','wpresidence').'</div>    
        <input type="text" name="splash_overlay_opacity" value="'.$splash_overlay_opacity.'"  class="inptxt" />
    </div>';
    
    
    $splash_page_title            =  stripslashes( esc_html ( get_option('wp_estate_splash_page_title','') ) );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Splash Page Title','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Splash Page Title','wpresidence').'</div>    
        <input type="text" name="splash_page_title" value="'.$splash_page_title.'"  class="inptxt" />
    </div>';
    
    $splash_page_subtitle            =  stripslashes ( esc_html ( get_option('wp_estate_splash_page_subtitle','') ) );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Splash Page Subtitle','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Splash Page Subtitle','wpresidence').'</div>    
        <input type="text" name="splash_page_subtitle" value="'.$splash_page_subtitle .'"  class="inptxt" />
    </div>';
    
   
    
    
    $splash_page_logo_link            =  esc_html ( get_option('wp_estate_splash_page_logo_link','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Logo Link','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('In case you want to send users to another page','wpresidence').'</div>    
        <input type="text" name="splash_page_logo_link" value="'.$splash_page_logo_link.'"  class="inptxt" />
    </div>';
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;





if( !function_exists('new_wpestate_listing_labels') ):   
function new_wpestate_property_links(){
  
 
    $rewrites =  get_option('wp_estate_url_rewrites');
    $i=0;
    while ($i<=23){
        if(!isset( $rewrites[$i])){
            $rewrites[$i]='';
        }
        $i++;
    }
 
    $links_to_rewrite = array(
        'property_page'         =>  array(
                                        $rewrites[0],
                                        esc_html__('Property Page','wpresidence')
                                    ),
        
        'property_category'     =>  array(
                                         $rewrites[1],
                                        esc_html__('Property Categories Page','wpresidence')
                                    ),
        
        'property_action_category'     =>  array(
                                        $rewrites[2],
                                        esc_html__('Property Action Category Page','wpresidence')
                                    ),
        'property_city'     =>  array(
                                        $rewrites[3],
                                        esc_html__('Property City Page','wpresidence')
                                    ),
        
        'property_area'     =>  array(
                                        $rewrites[4],
                                        esc_html__('Property Area Page','wpresidence')
                                    ),
        
        'property_county_state'     =>  array(
                                         $rewrites[5],
                                        esc_html__('Property County/State Page','wpresidence')
                                    ),
        'agent_page'     =>  array(
                                         $rewrites[6],
                                        esc_html__('Agent Page','wpresidence')
                                    ),
        'agent_category'     =>  array(
                                         $rewrites[7],
                                        esc_html__('Agent Categories Page','wpresidence')
                                    ),
        
        'agent_action_category'     =>  array(
                                        $rewrites[8],
                                        esc_html__('Agent Action Category Page','wpresidence')
                                    ),
        'agent_city'     =>  array(
                                        $rewrites[9],
                                        esc_html__('Agent City Page','wpresidence')
                                    ),
        
        'agent_area'     =>  array(
                                        $rewrites[10],
                                        esc_html__('Agent Area Page','wpresidence')
                                    ),
        
        'agent_county_state'     =>  array(
                                         $rewrites[11],
                                        esc_html__('Agent County/State Page','wpresidence')
                                    ),

        'category_agency'     =>  array(
                                         $rewrites[12],
                                        esc_html__('Agency Category Page','wpresidence')
                                    ),
        'action_category_agency'     =>  array(
                                         $rewrites[13],
                                        esc_html__('Agency Action Category Page','wpresidence')
                                    ),
        'city_agency'     =>  array(
                                         $rewrites[14],
                                        esc_html__('Agency City Page','wpresidence')
                                    ),
        'area_agency'     =>  array(
                                         $rewrites[15],
                                        esc_html__('Agency Area Page','wpresidence')
                                    ),
        'county_state_agency'     =>  array(
                                         $rewrites[16],
                                        esc_html__('Agency County/State Page','wpresidence')
                                    ),
       
        'property_category_developer'     =>  array(
                                         $rewrites[17],
                                        esc_html__('Developer Cateogory Page','wpresidence')
                                    ),
        'property_action_developer'     =>  array(
                                         $rewrites[18],
                                        esc_html__('Developer Action Category  Page','wpresidence')
                                    ),
        'property_city_developer'     =>  array(
                                         $rewrites[19],
                                        esc_html__('Developer City Page','wpresidence')
                                    ),
        'property_area_developer'     =>  array(
                                         $rewrites[20],
                                        esc_html__('Developer Area Page','wpresidence')
                                    ),
        'property_county_state_developer'     =>  array(
                                         $rewrites[21],
                                        esc_html__('Developer County/State Page','wpresidence')
                                    ),
        'single_page_agency'     =>  array(
                                         $rewrites[22],
                                        esc_html__('Agency Page','wpresidence')
                                    ),
        'single_page_developer'     =>  array(
                                         $rewrites[23],
                                        esc_html__('Developer Page','wpresidence')
                                    ),
    );
    
    $i=0;
      print'<div class="estate_option_row">'.esc_html__('You cannot use special characters like "&". After changing the url you may need to wait for a few minutes until WordPress changes all the urls. In case your new names do not update automatically, go to Settings - Permalinks and Save again the "Permalinks Settings" - option "Post name"','wpresidence').'</div>';
    
    
    foreach ($links_to_rewrite as $key=>$value){
        print'<div class="estate_option_row">
        <div class="label_option_row">'.$value[1].'</div>
        <div class="option_row_explain">'.esc_html__('Custom link for ','wpresidence').' '.$value[1].'</div>    
           '.esc_url( home_url() ).'/ <input  type="text" id="'.$value[1].'"  name="url_rewrites[]"   value="'.$rewrites[$i].'"/> /....
        </div>';
        $i++;
    }
      
      
      
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;


if( !function_exists('new_wpestate_listing_labels') ):   
function new_wpestate_listing_labels(){
    $cache_array                            =   array('yes','no');
    $status_list                            =   esc_html(stripslashes( get_option('wp_estate_status_list') ) );
    $status_list                            =   str_replace(', ',',&#13;&#10;',$status_list);
    $property_adr_text                      =   stripslashes ( esc_html( get_option('wp_estate_property_adr_text') ) );
    $property_description_text              =   stripslashes ( esc_html( get_option('wp_estate_property_description_text') ) );
    $property_details_text                  =   stripslashes ( esc_html( get_option('wp_estate_property_details_text') ) );
    $property_features_text                 =   stripslashes ( esc_html( get_option('wp_estate_property_features_text') ) );
   
    $property_multi_text                      =   stripslashes ( esc_html( get_option('wp_estate_property_multi_text') ) );
    $property_multi_child_text                      =   stripslashes ( esc_html( get_option('wp_estate_property_multi_child_text') ) );
   
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Multi Unit Label','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__(' Custom title instead of Multi Unit label.','wpresidence').'</div>    
        <input  type="text" id="property_multi_text"  name="property_multi_text"   value="'.$property_multi_text.'"/>
    </div>';
    
     print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Multi Unit Label (*for sub unit)','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__(' Custom title instead of Multi Unit label(*for sub unit).','wpresidence').'</div>    
        <input  type="text" id="property_multi_child_text"  name="property_multi_child_text"   value="'.$property_multi_child_text.'"/>
    </div>';
     
 
        
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Property Address Label','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__(' Custom title instead of Property Address label.','wpresidence').'</div>    
        <input  type="text" id="property_adr_text"  name="property_adr_text"   value="'.$property_adr_text.'"/>
    </div>';
              
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Property Features Label','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Update; Custom title instead of Features and Amenities label.','wpresidence').'</div>    
        <input  type="text" id="property_features_text"  name="property_features_text"   value="'.$property_features_text.'" size="40"/>
    </div>';
                
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Property Description Label','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Custom title instead of Description label.','wpresidence').'</div>    
        <input  type="text" id="property_description_text"  name="property_description_text"   value="'.$property_description_text.'" size="40"/>
    </div>';

    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Property Details Label','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Custom title instead of Property Details label. ','wpresidence').'</div>    
        <input  type="text" id="property_details_text"  name="property_details_text"   value="'.$property_details_text.'" size="40"/>
    </div>';

    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Property Status ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Property Status (* you may need to add new css classes - please see the help files) ','wpresidence').'</div>    
        <input  type="text" id="new_status"  name="new_status"   value="'.esc_html__('type here the new status... ','wpresidence').'"/></br>
        <a href="#new_status" id="add_status">'.esc_html__('click to add new status','wpresidence').'</a><br>
        <textarea id="status_list" name="status_list" rows="7" style="width:300px;">'.$status_list.'</textarea>  
    </div>';
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif; 

if( !function_exists('new_wpestate_theme_slider') ):   
 function new_wpestate_theme_slider(){
    $theme_slider   =   get_option( 'wp_estate_theme_slider', true); 
    $slider_cycle   =   get_option( 'wp_estate_slider_cycle', true); 
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Select Properties','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Select properties for slider - *hold CTRL for multiple select ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Due to speed reason we only show here the first 50 listings. If you want to add other listings into the theme slider please go and edit the property (in wordpress admin) and select "Property in theme Slider" in Property Details tab.','wpresidence').'</div>';    
        
        $args = array(  'post_type'                 =>  'estate_property',
                        'post_status'               =>  'publish',
                        'paged'                     =>  1,
                        'posts_per_page'            =>  50,
                        'cache_results'             =>  false,
                        'update_post_meta_cache'    =>  false,
                        'update_post_term_cache'    =>  false,
                );

        $recent_posts = new WP_Query($args);
        print '
		<input type="hidden" name="theme_slider" />
		<select name="theme_slider[]"  id="theme_slider"  multiple="multiple">';
        while ($recent_posts->have_posts()): $recent_posts->the_post();
             $theid=get_the_ID();
             print '<option value="'.$theid.'" ';
             if( is_array($theme_slider) && in_array($theid, $theme_slider) ){
                 print ' selected="selected" ';
             }
             print'>'.get_the_title().'</option>';
        endwhile;
        print '</select>';
     
    print '</div>';
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Number of milisecons before auto cycling an item','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Number of milisecons before auto cycling an item (5000=5sec).Put 0 if you don\'t want to autoslide. ','wpresidence').'</div>    
        <input  type="text" id="slider_cycle" name="slider_cycle"  value="'.$slider_cycle.'"/> 
    </div>';
    
   
    $cache_array                    =   array('type1', 'type2', 'type3');  
    $theme_slider_type_select       =   wpestate_dropdowns_theme_admin($cache_array,'theme_slider_type');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Design Type?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Select the design type.','wpresidence').'</div>    
        <select id="theme_slider_type" name="theme_slider_type">
            '.$theme_slider_type_select.'
        </select>
    </div>';
    
    $theme_slider_height   =   get_option( 'wp_estate_theme_slider_height', true); 
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Height in px (put 0 for full screen)','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Height in px(put 0 for full screen, Default :580px)','wpresidence').'</div>    
        <input  type="text" id="theme_slider_height" name="theme_slider_height"  value="'.$theme_slider_height.'"/> 
    </div>';
    
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
     
 }
 endif;
 
 
 if( !function_exists('new_wpestate_email_management') ):   
 function new_wpestate_email_management(){
     
        $emails=array(
            'new_user'                  =>  esc_html__('New user  notification','wpresidence'),
            'admin_new_user'            =>  esc_html__('New user admin notification','wpresidence'),
            'purchase_activated'        =>  esc_html__('Purchase Activated','wpresidence'),
            'password_reset_request'    =>  esc_html__('Password Reset Request','wpresidence'),
            'password_reseted'          =>  esc_html__('Password Reseted','wpresidence'),
            'purchase_activated'        =>  esc_html__('Purchase Activated','wpresidence'),
            'approved_listing'          =>  esc_html__('Approved Listings','wpresidence'),
            'new_wire_transfer'         =>  esc_html__('New wire Transfer','wpresidence'),
            'admin_new_wire_transfer'   =>  esc_html__('Admin - New wire Transfer','wpresidence'),
            'admin_expired_listing'     =>  esc_html__('Admin - Expired Listing','wpresidence'),
            'matching_submissions'      =>  esc_html__('Matching Submissions','wpresidence'),
            'paid_submissions'          =>  esc_html__('Paid Submission','wpresidence'),
            'featured_submission'       =>  esc_html__('Featured Submission','wpresidence'),
            'account_downgraded'        =>  esc_html__('Account Downgraded','wpresidence'),
            'membership_cancelled'      =>  esc_html__('Membership Cancelled','wpresidence'),
            'downgrade_warning'         =>  esc_html__('Membership Expiration Warning','wpresidence'),
            'free_listing_expired'      =>  esc_html__('Free Listing Expired','wpresidence'),
            'new_listing_submission'    =>  esc_html__('New Listing Submission','wpresidence'),
            'listing_edit'              =>  esc_html__('Listing Edit','wpresidence'),
            'recurring_payment'         =>  esc_html__('Recurring Payment','wpresidence'),
            'membership_activated'      =>  esc_html__('Membership Activated','wpresidence'),
            'agent_update_profile'      =>  esc_html__('Update Profile','wpresidence'),
            'agent_added'               =>  esc_html__('New Agent','wpresidence')
           
        );
        
        

            
        print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Global variables: %website_url as website url,%website_name as website name, %user_email as user_email, %username as username','wpresidence').'</div>
        </div>';
        $cnt = 3;
        foreach ($emails as $key=>$label ){
            $value          = stripslashes( get_option('wp_estate_'.$key,'') );
            $value_subject  = stripslashes( get_option('wp_estate_subject_'.$key,'') );
			
	  
            print'<div class="estate_option_row">
            <div class="label_option_row">'.esc_html__('Subject for','wpresidence').' '.$label.'</div>
            <div class="option_row_explain">'.esc_html__('Email subject for','wpresidence').' '.$label.'</div>
            <input type="text" style="width:100%" name="subject_'.$key.'" value="'.$value_subject.'" />
            </br>
            <div class="label_option_row">'.esc_html__('Content for','wpresidence').' '.$label.'</div>
            <div class="option_row_explain">'.esc_html__('Email content for','wpresidence').' '.$label.'</div>';
			
			
			wp_editor( $value, $key, array( 'textarea_rows' => 15, 'teeny' => true )  );
		
			print '
            <div class="extra_exp_new"> ';
            if(function_exists('wpestate_emails_extra_details')){
                print wpestate_emails_extra_details($key);
            }
            print'</div>
            </div>';

        }
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
        
}
endif;


 if( !function_exists('new_wpestate_site_speed') ):   
function new_wpestate_site_speed(){
      
    $cache_array=array('no','yes');
    $mimify_css_js=  wpestate_dropdowns_theme_admin($cache_array,'use_mimify');
    
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Speed Advices','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('1. If you are NOT using "Ultimate Addons for Visual Composer" please disable it or just disable the modules you don\'t use. It will reduce the size of javascript files you are loading and increase the site speed!    ','wpresidence').'</div>    
    <div class="option_row_explain">'.esc_html__('2. Use the EWWW Image Optimizer (or WP Smush IT) plugin to optimise images- optimised images increase the site speed.','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('3. Create a free account on Cloudflare (https://www.cloudflare.com/) and use this CDN.','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('4. If you are using custom categories make sure you are adding the custom pins images on Theme Options -> Map -> Pin Management. The site will get slow if it needs to look for images that don\'t exist.','wpresidence').'</div>
    </div>';
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Minify css and js files','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('The system will use the minified versions of the css and js files','wpresidence').'</div>    
        <select id="use_mimify" name="use_mimify">
            '.$mimify_css_js.'
        </select> 
    </div>';
    
    
    $remove_script_version=  wpestate_dropdowns_theme_admin($cache_array,'remove_script_version');
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Remove script version - Following Envato guidelines we removed this feature.Please use a cache plugin in order to remove the script version.','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('The system will remove the script version when it is included. This doest not actually improve speed, but improves test score on speed tools pages.','wpresidence').'</div>    
        <select id="remove_script_version" name="remove_script_version">
            '.$remove_script_version.'
        </select> 
    </div>';
     
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Enable Browser Cache','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Add this code in your .httaces file(copy paste at the end). It will activate the browser cache and speed up your site.','wpresidence').'</div>    
       <textarea rows="15" style="width:100%;" onclick="this.focus();this.select();">      
<IfModule mod_deflate.c>
# Insert filters
AddOutputFilterByType DEFLATE text/plain
AddOutputFilterByType DEFLATE text/html
AddOutputFilterByType DEFLATE text/xml
AddOutputFilterByType DEFLATE text/css
AddOutputFilterByType DEFLATE application/xml
AddOutputFilterByType DEFLATE application/xhtml+xml
AddOutputFilterByType DEFLATE application/rss+xml
AddOutputFilterByType DEFLATE application/javascript
AddOutputFilterByType DEFLATE application/x-javascript
AddOutputFilterByType DEFLATE application/x-httpd-php
AddOutputFilterByType DEFLATE application/x-httpd-fastphp
AddOutputFilterByType DEFLATE image/svg+xml
# Drop problematic browsers
BrowserMatch ^Mozilla/4 gzip-only-text/html
BrowserMatch ^Mozilla/4\.0[678] no-gzip
BrowserMatch \bMSI[E] !no-gzip !gzip-only-text/html
# Make sure proxies dont deliver the wrong content
Header append Vary User-Agent env=!dont-vary
</IfModule>
## EXPIRES CACHING ##
<IfModule mod_expires.c>
ExpiresActive On
ExpiresByType image/jpg "access 1 year"
ExpiresByType image/jpeg "access 1 year"
ExpiresByType image/gif "access 1 year"
ExpiresByType image/png "access 1 year"
ExpiresByType text/css "access 1 month"
ExpiresByType text/html "access 1 month"
ExpiresByType application/pdf "access 1 month"
ExpiresByType text/x-javascript "access 1 month"
ExpiresByType application/x-shockwave-flash "access 1 month"
ExpiresByType image/x-icon "access 1 year"
ExpiresDefault "access 1 month"
</IfModule>
## EXPIRES CACHING ##
       </textarea>
    </div>';
     
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;









if( !function_exists('new_wpestate_generate_pins') ):   
 function new_wpestate_generate_pins(){
    
    if (isset($_POST['startgenerating']) == 1){
        //wpestate_generate_file_pins();
        if ( get_option('wp_estate_readsys','') =='yes' ){
        $path=estate_get_pin_file_path(); 
   
            if ( file_exists ($path) && is_writable ($path) ){
                wpestate_listing_pins_for_file();
                print'<div class="estate_option_row">
                <div class="label_option_row">'. esc_html__('File was generated','wpresidence').'</div>
                </div>';
            }else{
                print'<div class="estate_option_row">
                <div class="label_option_row">'.esc_html__('the file Google map does NOT exist or is NOT writable','wpresidence') .'</div>
                </div>';
            }
   
        }else{
          
            print'<div class="estate_option_row">
            <div class="label_option_row">'.  esc_html__('Pin Generation works only if the file reading option in Google Map setting is set to yes','wpresidence').'</div>
            </div>';
    
        }
    
    
    }else{  
     
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Generate the pins','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Generate the pins for the read from file map option','wpresidence').'</div>    
       
    <input type="hidden" name="startgenerating" value="1" />
        
    </div>';
    
    }
    
     print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Generate Pins','wpresidence').'" />
    </div>';
    
}
endif;


if( !function_exists('new_wpestate_help_custom') ):   
function new_wpestate_help_custom(){
  
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Help and Custom','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Help and custom work','wpresidence').'</div>    
       <p> '.esc_html__('For theme help please check','wpresidence').' <a href="https://help.wpresidence.net/">help.wpresidence.net</a>.'.esc_html__(' If your question is not here, please go to http://support.wpestate.org, create an account and post a ticket. The registration is simple and as soon as you send a ticket we are notified. We usually answer in the next 24h (except weekends). Please use this system and not the email. It will help us answer your questions much faster. Thank you!','wpresidence').' </p>
       <p> '.esc_html__('For custom work on this theme please go to  <a href="http://support.wpestate.org/" target="_blank">http://support.wpestate.org</a>, create a ticket with your request and we will offer a free quote.','wpresidence').' </p>
       <p> '.esc_html__('Subscribe to our mailing list in order to receive news about new features and theme upgrades. <a href="http://eepurl.com/CP5U5">Subscribe Here!</a>','wpresidence').' </p>
                   
    </div>';
        
}
endif;



if(!function_exists('estate_yelp_settings')):
function estate_yelp_settings(){
    $yelp_terms             =   get_option('wp_estate_yelp_categories','');    
    $yelp_client_id         =   get_option('wp_estate_yelp_client_id','');
    $yelp_client_api_key_2018  =   get_option('wp_estate_yelp_client_api_key_2018','');
    $yelp_client_secret     =   get_option('wp_estate_yelp_client_secret','');
      $yelp_results_no        = get_option('wp_estate_yelp_results_no','');
    if(!is_array($yelp_terms)){
        $yelp_terms=array();
    }
    
   
    
    $yelp_terms_array = 
            array (
                'active'            =>  array( 'category' => esc_html__('Active Life','wpresidence'),
                                                'category_sign' => 'fa fa-bicycle'),
                'arts'              =>  array( 'category' => esc_html__('Arts & Entertainment','wpresidence'), 
                                               'category_sign' => 'fa fa-music') ,
                'auto'              =>  array( 'category' => esc_html__('Automotive','wpresidence'), 
                                                'category_sign' => 'fa fa-car' ),
                'beautysvc'         =>  array( 'category' => esc_html__('Beauty & Spas','wpresidence'), 
                                                'category_sign' => 'fa fa-female' ),
                'education'         => array(  'category' => esc_html__('Education','wpresidence'),
                                                'category_sign' => 'fa fa-graduation-cap' ),
                'eventservices'     => array(  'category' => esc_html__('Event Planning & Services','wpresidence'), 
                                                'category_sign' => 'fa fa-birthday-cake' ),
                'financialservices' => array(  'category' => esc_html__('Financial Services','wpresidence'), 
                                                'category_sign' => 'fa fa-money' ),                
                'food'              => array(  'category' => esc_html__('Food','wpresidence'), 
                                                'category_sign' => 'fa fa fa-cutlery' ),
                'health'            => array(  'category' => esc_html__('Health & Medical','wpresidence'), 
                                                'category_sign' => 'fa fa-medkit' ),
                'homeservices'      => array(  'category' =>esc_html__('Home Services ','wpresidence'), 
                                                'category_sign' => 'fa fa-wrench' ),
                'hotelstravel'      => array(  'category' => esc_html__('Hotels & Travel','wpresidence'), 
                                                'category_sign' => 'fa fa-bed' ),
                'localflavor'       => array(  'category' => esc_html__('Local Flavor','wpresidence'), 
                                                'category_sign' => 'fa fa-coffee' ),
                'localservices'     => array(  'category' => esc_html__('Local Services','wpresidence'), 
                                                'category_sign' => 'fa fa-dot-circle-o' ),
                'massmedia'         => array(  'category' => esc_html__('Mass Media','wpresidence'),
                                                'category_sign' => 'fa fa-television' ),
                'nightlife'         => array(  'category' => esc_html__('Nightlife','wpresidence'),
                                                'category_sign' => 'fa fa-glass' ),
                'pets'              => array(  'category' => esc_html__('Pets','wpresidence'),
                                                'category_sign' => 'fa fa-paw' ),
                'professional'      => array(  'category' => esc_html__('Professional Services','wpresidence'), 
                                                'category_sign' => 'fa fa-suitcase' ),
                'publicservicesgovt'=> array(  'category' => esc_html__('Public Services & Government','wpresidence'),
                                                'category_sign' => 'fa fa-university' ),
                'realestate'        => array(  'category' => esc_html__('Real Estate','wpresidence'), 
                                                'category_sign' => 'fa fa-building-o' ),
                'religiousorgs'     => array(  'category' => esc_html__('Religious Organizations','wpresidence'), 
                                                'category_sign' => 'fa fa-cloud' ),
                'restaurants'       => array(  'category' => esc_html__('Restaurants','wpresidence'),
                                                'category_sign' => 'fa fa-cutlery' ),
                'shopping'          => array(  'category' => esc_html__('Shopping','wpresidence'),
                                                'category_sign' => 'fa fa-shopping-bag' ),
                'transport'         => array(  'category' => esc_html__('Transportation','wpresidence'),
                                                'category_sign' => 'fa fa-bus' )
    );
    print '<div class="estate_option_row">'.esc_html__('Please note that Yelp is not working for all countries. See here ','wpresidence').'<a href="https://www.yelp.com/factsheet">https://www.yelp.com/factsheet</a>'.esc_html__(' the list of countries where Yelp is available.','wpresidence').'</br></div>';
//    
//    print'<div class="estate_option_row">
//    <div class="label_option_row">'.esc_html__('Yelp Api v2.0 Consumer Key','wpresidence').'</div>
//    <div class="option_row_explain">'.esc_html__('Get this detail after you signup here ','wpresidence').'<a target="_blank" href="https://www.yelp.com/developers/v2/">https://www.yelp.com/developers/v2/</a></div>    
//        <input  type="text" id="yelp_api_id" name="yelp_api_id"  value="'.$yelp_api_id.'"/> 
//    </div>';
//     
//    
//    print'<div class="estate_option_row">
//    <div class="label_option_row">'.esc_html__('Yelp Api v.2.0 Consumer Secret','wpresidence').'</div>
//    <div class="option_row_explain">'.esc_html__('Get this detail after you signup here ','wpresidence').'<a target="_blank" href="https://www.yelp.com/developers/v2/">https://www.yelp.com/developers/v2/</a></div>    
//        <input  type="text" id="yelp_api_secret" name="yelp_api_secret"  value="'.$yelp_api_secret.'"/> 
//    </div>';
//    
//    print'<div class="estate_option_row">
//    <div class="label_option_row">'.esc_html__('Yelp Api v.2.0 Token','wpresidence').'</div>
//    <div class="option_row_explain">'.esc_html__('Get this detail after you signup here ','wpresidence').'<a target="_blank" href="https://www.yelp.com/developers/v2/">https://www.yelp.com/developers/v2/</a></div>    
//        <input  type="text" id="yelp_api_token" name="yelp_api_token"  value="'.$yelp_api_token.'"/> 
//    </div>';
//        
//    print'<div class="estate_option_row">
//    <div class="label_option_row">'.esc_html__('Yelp Api v.2.0 Token Secret','wpresidence').'</div>
//    <div class="option_row_explain">'.esc_html__('Get this detail after you signup here ','wpresidence').'<a target="_blank" href="https://www.yelp.com/developers/v2/">https://www.yelp.com/developers/v2/</a></div>    
//        <input  type="text" id="yelp_api_token_secret" name="yelp_api_token_secret"  value="'.$yelp_api_token_secret.'"/> 
//    </div>';
//    
//    
//      print'<div class="estate_option_row">
//    <div class="label_option_row">'.esc_html__('Yelp Api v.2.0 Token Secret','wpresidence').'</div>
//    <div class="option_row_explain">'.esc_html__('Get this detail after you signup here ','wpresidence').'<a target="_blank" href="https://www.yelp.com/developers/v2/">https://www.yelp.com/developers/v2/</a></div>    
//        <input  type="text" id="yelp_api_token_secret" name="yelp_api_token_secret"  value="'.$yelp_api_token_secret.'"/> 
//    </div>';
      
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Yelp Api  Client Id','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Get this detail after you signup here ','wpresidence').'<a target="_blank" href="https://www.yelp.com/developers/v3/manage_app">https://www.yelp.com/developers/v3/manage_app</a></div>    
        <input  type="text" id="yelp_client_id" name="yelp_client_id"  value="'.$yelp_client_id.'"/> 
    </div>';
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Yelp Api Client Key','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Get this detail after you signup here ','wpresidence').'<a target="_blank" href="https://www.yelp.com/developers/v3/manage_app">https://www.yelp.com/developers/v3/manage_app</a></div>    
        <input  type="text" id="yelp_client_api_key_2018" name="yelp_client_api_key_2018"  value="'.$yelp_client_api_key_2018.'"/> 
    </div>'; 
       
       
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Yelp Categories ','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Yelp Categories to show on front page ','wpresidence').'</div>  

		<input type="hidden" name="yelp_categories" />
        <select name="yelp_categories[]" style="height:400px;" id="yelp_categories" multiple>';
        foreach($yelp_terms_array as $key=>$term){
            print '<option value="'.$key.'" ' ;
            $keyx = array_search ($key,$yelp_terms) ;
            if( $keyx!==false ){
                print 'selected= "selected" ';
            }
            print'>'.$term['category'].'</option>';
        }
    print'</select>
    </div>';
    
       
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Yelp - no of results','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Yelp - no of results ','wpresidence').'</div>    
        <input  type="text" id="yelp_results_no" name="yelp_results_no"  value="'.$yelp_results_no.'"/> 
    </div>';
    
    $cache_array=array('miles','kilometers');
    $yelp_dist_measure=  wpestate_dropdowns_theme_admin($cache_array,'yelp_dist_measure');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Yelp Distance Measurement Unit','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Yelp Distance Measurement Unit','wpresidence').'</div>    
       <select id="yelp_dist_measure" name="yelp_dist_measure">
            '.$yelp_dist_measure.'
        </select> 
    </div>';
    
    
      print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
     
     
    
}endif;





if(!function_exists('estate_recaptcha_settings')):
function estate_recaptcha_settings(){
    $reCaptha_sitekey       = get_option('wp_estate_recaptha_sitekey','');
    $reCaptha_secretkey     = get_option('wp_estate_recaptha_secretkey','');
    $cache_array=array('no','yes');
    $use_captcha=  wpestate_dropdowns_theme_admin($cache_array,'use_captcha');
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Use reCaptcha on register ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('This helps preventing registration spam.','wpresidence').'</div>    
        <select id="use_captcha" name="use_captcha">
            '.$use_captcha.'
        </select> 
    </div>';
    
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('reCaptha site key','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Get this detail after you signup here ','wpresidence').'<a target="_blank" href="https://www.google.com/recaptcha/intro/index.html">https://www.google.com/recaptcha/intro/index.html</a></div>    
        <input  type="text" id="recaptha_sitekey" name="recaptha_sitekey"  value="'.$reCaptha_sitekey.'"/> 
    </div>';
    
     print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('reCaptha secret key','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Get this detail after you signup here ','wpresidence').'<a target="_blank" href="https://www.google.com/recaptcha/intro/index.html">https://www.google.com/recaptcha/intro/index.html</a></div>    
        <input  type="text" id="recaptha_secretkey" name="recaptha_secretkey"  value="'.$reCaptha_secretkey.'"/> 
    </div>';
     
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;



if(!function_exists('optima_express_settings')):
function optima_express_settings(){
   
    $cache_array=array('no','yes');
    $use_optima=  wpestate_dropdowns_theme_admin($cache_array,'use_optima');
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Use Optima Express plugin (idx plugin by ihomefinder) - you will need to enable the plugin ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Enable compatibility mode with Optima Express plugin','wpresidence').'</div>    
        <select id="use_optima" name="use_optima">
            '.$use_optima.'
        </select> 
    </div>';
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;



if (!function_exists('new_widget_design_elements_details')):
function new_widget_design_elements_details(){
    
    $sidebarwidget_internal_padding_top          = get_option('wp_estate_sidebarwidget_internal_padding_top','');
    $sidebarwidget_internal_padding_left         = get_option('wp_estate_sidebarwidget_internal_padding_left','');
    $sidebarwidget_internal_padding_bottom       = get_option('wp_estate_sidebarwidget_internal_padding_bottom','');
    $sidebarwidget_internal_padding_right        = get_option('wp_estate_sidebarwidget_internal_padding_right','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Widget Internal Padding','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Widget Internal Padding (top,left,bottom,right) ','wpresidence').'</div>    
        <input  style="width:100px;min-width:100px;" type="text" id="sidebarwidget_internal_padding_top" name="sidebarwidget_internal_padding_top"  value="'.$sidebarwidget_internal_padding_top.'"/> 
        <input  style="width:100px;min-width:100px;" type="text" id="sidebarwidget_internal_padding_left" name="sidebarwidget_internal_padding_left"  value="'.$sidebarwidget_internal_padding_left.'"/> 
        <input  style="width:100px;min-width:100px;" type="text" id="sidebarwidget_internal_padding_bottom" name="sidebarwidget_internal_padding_bottom"  value="'.$sidebarwidget_internal_padding_bottom.'"/> 
        <input  style="width:100px;min-width:100px;" type="text" id="sidebarwidget_internal_padding_right" name="sidebarwidget_internal_padding_right"  value="'.$sidebarwidget_internal_padding_right.'"/> 
    </div>';
        
    $sidebar_widget_color           =  esc_html ( get_option('wp_estate_sidebar_widget_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Sidebar Widget Background Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Sidebar Widget Background Color','wpresidence').'</div>    
        <input type="text" name="sidebar_widget_color" value="'.$sidebar_widget_color.'" maxlength="7" class="inptxt" />
        <div id="sidebar_widget_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$sidebar_widget_color.';" ></div></div>
    </div>';
          
        
    $sidebar_heading_boxed_color    =  esc_html ( get_option('wp_estate_sidebar_heading_boxed_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Sidebar Heading Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Sidebar Heading Color','wpresidence').'</div>    
        <input type="text" name="sidebar_heading_boxed_color" value="'.$sidebar_heading_boxed_color.'" maxlength="7" class="inptxt" />
        <div id="sidebar_heading_boxed_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$sidebar_heading_boxed_color.';"></div></div>
    </div>';      
    
    $sidebar_heading_background_color         =  esc_html ( get_option('wp_estate_sidebar_heading_background_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Sidebar Heading Background Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Sidebar Heading Background Color','wpresidence').'</div>    
        <input type="text" name="sidebar_heading_background_color" value="'.$sidebar_heading_background_color.'" maxlength="7" class="inptxt" />
        <div id="sidebar_heading_background_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$sidebar_heading_background_color.';"></div></div>
    </div>';
    
    
    $sidebar_boxed_font_color            =  esc_html ( get_option('wp_estate_sidebar_boxed_font_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Widget Font color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Widget Font color','wpresidence').'</div>    
        <input type="text" name="sidebar_boxed_font_color" value="'.$sidebar_boxed_font_color.'" maxlength="7" class="inptxt" />
        <div id="sidebar_boxed_font_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$sidebar_boxed_font_color.';"></div></div>
    </div>';
   
    
    $wp_estate_widget_sidebar_border_size      = get_option('wp_estate_widget_sidebar_border_size','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Widget Border Size','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Widget Border Size','wpresidence').'</div>    
        <input  type="text" id="widget_sidebar_border_size " name="widget_sidebar_border_size"  value="'.$wp_estate_widget_sidebar_border_size.'"/> 
    </div>';
    
    
    $widget_sidebar_border_color            =  esc_html ( get_option('wp_estate_widget_sidebar_border_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Widget Border Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Widget Border Color','wpresidence').'</div>    
        <input type="text" name="widget_sidebar_border_color" value="'.$widget_sidebar_border_color.'" maxlength="7" class="inptxt" />
        <div id="widget_sidebar_border_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$widget_sidebar_border_color.';"></div></div>
    </div>';
   
    

       
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;


if( !function_exists('new_wpestate_mobile_menu_design')):
function new_wpestate_mobile_menu_design(){

    $mobile_header_background_color          =  esc_html ( get_option('wp_estate_mobile_header_background_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Mobile header background color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Mobile header background color','wpresidence').'</div>    
        <input type="text" name="mobile_header_background_color" value="'.$mobile_header_background_color.'" maxlength="7" class="inptxt" />
        <div id="mobile_header_background_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$mobile_header_background_color.';" ></div></div>
    </div>';   
    
    
    $mobile_header_icon_color          =  esc_html ( get_option('wp_estate_mobile_header_icon_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Mobile header icon color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Mobile header icon color','wpresidence').'</div>    
        <input type="text" name="mobile_header_icon_color" value="'.$mobile_header_icon_color.'" maxlength="7" class="inptxt" />
        <div id="mobile_header_icon_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$mobile_header_icon_color.';" ></div></div>
    </div>';  
    
    
    $mobile_menu_font_color          =  esc_html ( get_option('wp_estate_mobile_menu_font_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Mobile menu font color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Mobile menu font color','wpresidence').'</div>    
        <input type="text" name="mobile_menu_font_color" value="'.$mobile_menu_font_color.'" maxlength="7" class="inptxt" />
        <div id="mobile_menu_font_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$mobile_menu_font_color.';" ></div></div>
    </div>'; 
    
    
    $mobile_menu_hover_font_color    =esc_html(get_option('wp_estate_mobile_menu_hover_font_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Mobile menu hover font color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Mobile menu hover font color','wpresidence').'</div>    
        <input type="text" name="mobile_menu_hover_font_color" value="'.$mobile_menu_hover_font_color.'" maxlength="7" class="inptxt" />
        <div id="mobile_menu_hover_font_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$mobile_menu_hover_font_color.';" ></div></div>
    </div>';
    
    
    $mobile_item_hover_back_color         =  esc_html ( get_option('wp_estate_mobile_item_hover_back_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Mobile menu item hover background color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Mobile menu item hover background color','wpresidence').'</div>    
        <input type="text" name="mobile_item_hover_back_color" value="'.$mobile_item_hover_back_color.'" maxlength="7" class="inptxt" />
        <div id="mobile_item_hover_back_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$mobile_item_hover_back_color.';" ></div></div>
    </div>'; 
    

    $mobile_menu_backgound_color = esc_html(get_option('wp_estate_mobile_menu_backgound_color', ''));
    print'<div class="estate_option_row">
    <div class="label_option_row">' . esc_html__('Mobile menu background color', 'wpresidence') . '</div>
    <div class="option_row_explain">' . esc_html__('Mobile menu background color', 'wpresidence') . '</div>    
        <input type="text" name="mobile_menu_backgound_color" value="' .$mobile_menu_backgound_color. '" maxlength="7" class="inptxt" />
        <div id="mobile_menu_backgound_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$mobile_menu_backgound_color. ';" ></div></div>
    </div>';

    $mobile_menu_border_color = esc_html(get_option('wp_estate_mobile_menu_border_color', ''));
    print'<div class="estate_option_row">
    <div class="label_option_row">' . esc_html__('Mobile menu item border color', 'wpresidence') . '</div>
    <div class="option_row_explain">' . esc_html__('Mobile menu item border color', 'wpresidence') . '</div>    
        <input type="text" name="mobile_menu_border_color" value="' . $mobile_menu_border_color . '" maxlength="7" class="inptxt" />
        <div id="mobile_menu_border_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#' .$mobile_menu_border_color . ';" ></div></div>
    </div>';
    
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;    
    


if( !function_exists('new_wpestate_main_menu_design')):
function new_wpestate_main_menu_design(){
    
    $header_height                                              =   esc_html ( get_option('wp_estate_header_height','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Header Height','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Header Height in px','wpresidence').'</div>    
        <input type="text" name="header_height" id="header_height" value="'.$header_height.'"> 
    </div>';
    
    $sticky_header_height                                              =   esc_html ( get_option('wp_estate_sticky_header_height','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Sticky Header Height','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Sticky Header Height in px','wpresidence').'</div>    
        <input type="text" name="sticky_header_height" id="sticky_header_height" value="'.$sticky_header_height.'"> 
    </div>';
      
    
    $wp_estate_top_menu_font_size     = get_option('wp_estate_top_menu_font_size','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Top Menu Font Size','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Top Menu Font Size','wpresidence').'</div>    
        <input  type="text" id="top_menu_font_size" name="top_menu_font_size"  value="'.$wp_estate_top_menu_font_size.'"/> 
    </div>';
        
    $wp_estate_menu_item_font_size     = get_option('wp_estate_menu_item_font_size','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Menu Item Font Size','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Menu Item Font Size','wpresidence').'</div>    
        <input  type="text" id="menu_item_font_size" name="menu_item_font_size"  value="'.$wp_estate_menu_item_font_size.'"/> 
    </div>'; 
    
       $border_bottom_header                                              =   esc_html ( get_option('wp_estate_border_bottom_header','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Border Bottom Header Height','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Border Bottom Header Height in px','wpresidence').'</div>    
        <input type="text" name="border_bottom_header" id="border_bottom_header" value="'.$border_bottom_header.'"> 
    </div>';
    
    $sticky_border_bottom_header                                            =   esc_html ( get_option('wp_estate_sticky_border_bottom_header','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Border Bottom Sticky Header Height','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Border Bottom Sticky Header Height px','wpresidence').'</div>    
        <input type="text" name="sticky_border_bottom_header" id="sticky_border_bottom_header" value="'.$sticky_border_bottom_header.'"> 
    </div>';
    
        
    $border_bottom_header_color             =  esc_html ( get_option('wp_estate_border_bottom_header_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Header Border Bottom Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Header Border Bottom Color','wpresidence').'</div>    
        <input type="text" name="border_bottom_header_color" value="'.$border_bottom_header_color.'" maxlength="7" class="inptxt" />
        <div id="border_bottom_header_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$border_bottom_header_color.';"></div></div>
    </div>';
     
        
    $border_bottom_header_sticky_color             =  esc_html ( get_option('wp_estate_border_bottom_header_sticky_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Sticky Header Border Bottom Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Sticky Header Border Bottom Color','wpresidence').'</div>    
        <input type="text" name="border_bottom_header_sticky_color" value="'.$border_bottom_header_sticky_color.'" maxlength="7" class="inptxt" />
        <div id="border_bottom_header_sticky_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$border_bottom_header_sticky_color.';"></div></div>
    </div>';
    
     $top_bar_back = esc_html(get_option('wp_estate_top_bar_back', ''));
    print'<div class="estate_option_row">
    <div class="label_option_row">' . esc_html__('Top Bar Background Color (Header Widget Menu)', 'wpresidence') . '</div>
    <div class="option_row_explain">' . esc_html__('Top Bar Background Color (Header Widget Menu)', 'wpresidence') . '</div>    
        <input type="text" name="top_bar_back" value="' . $top_bar_back . '" maxlength="7" class="inptxt" />
        <div id="top_bar_back" class="colorpickerHolder"><div class="sqcolor" style="background-color:#' . $top_bar_back . ';"></div></div>
    </div>';

    $top_bar_font = esc_html(get_option('wp_estate_top_bar_font', ''));
    print'<div class="estate_option_row">
    <div class="label_option_row">' . esc_html__('Top Bar Font Color (Header Widget Menu)', 'wpresidence') . '</div>
    <div class="option_row_explain">' . esc_html__('Top Bar Font Color (Header Widget Menu)', 'wpresidence') . '</div>    
        <input type="text" name="top_bar_font" value="' . $top_bar_font . '" maxlength="7" class="inptxt" />
        <div id="top_bar_font" class="colorpickerHolder"><div class="sqcolor" style="background-color:#' . $top_bar_font . ';"></div></div>
    </div>';
    
    $header_color = esc_html(get_option('wp_estate_header_color', ''));
    print'<div class="estate_option_row">
    <div class="label_option_row">' . esc_html__('Top Menu & Sticky Menu Background Color', 'wpresidence') . '</div>
    <div class="option_row_explain">' . esc_html__('For Menu Area when Header type 5 applies main color ', 'wpresidence') . '</div>    
        <input type="text" name="header_color" value="' . $header_color . '" maxlength="7" class="inptxt" />
        <div id="header_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#' . $header_color . ';" ></div></div>
    </div>';
    
        
    $sticky_menu_font_color                =  esc_html ( get_option('wp_estate_sticky_menu_font_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Sticky Menu Font Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Sticky Menu Font Color','wpresidence').'</div>    
        <input type="text" name="sticky_menu_font_color" value="'.$sticky_menu_font_color.'"  maxlength="7" class="inptxt" />
        <div id="sticky_menu_font_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$sticky_menu_font_color.';" ></div></div>
    </div>';
           

        
    $menu_font_color                =  esc_html ( get_option('wp_estate_menu_font_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Top Menu Font Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Top Menu Font Color','wpresidence').'</div>    
        <input type="text" name="menu_font_color" value="'.$menu_font_color.'"  maxlength="7" class="inptxt" />
        <div id="menu_font_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$menu_font_color.';" ></div></div>
    </div>';
        
    $top_menu_hover_font_color                =  esc_html ( get_option('wp_estate_top_menu_hover_font_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Top Menu Hover Font Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Top Menu Hover Font Color','wpresidence').'</div>    
        <input type="text" name="top_menu_hover_font_color" value="'.$top_menu_hover_font_color.'"  maxlength="7" class="inptxt" />
        <div id="top_menu_hover_font_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$top_menu_hover_font_color.';" ></div></div>
    </div>';
       
    $top_menu_hover_back_font_color                =  esc_html ( get_option('wp_estate_top_menu_hover_back_font_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Top Menu Hover Background Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Top Menu Hover Background Color (*applies on some hover types)','wpresidence').'</div>    
        <input type="text" name="top_menu_hover_back_font_color" value="'.$top_menu_hover_back_font_color.'"  maxlength="7" class="inptxt" />
        <div id="top_menu_hover_back_font_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$top_menu_hover_back_font_color.';" ></div></div>
    </div>';
    
        
    $cache_array=array(1,2,3,4,5,6);
    $top_menu_hover_type=  wpestate_dropdowns_theme_admin($cache_array,'top_menu_hover_type');
    
    print'<div class="estate_option_row">
    <img  style="border:1px solid #FFE7E7;margin-bottom:10px;" src="'. get_template_directory_uri().'/img/menu_types.png" alt="logo"/>
                      
    <div class="label_option_row">'.esc_html__('Top Menu Hover Type','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('For Hover Type 1, 2, 5, 6 - setup Top Menu Hover Font Color option','wpresidence').'</br>'.esc_html__('For Hover Type 3, 4 - setup Top Menu Hover Background Color option','wpresidence').'</div>    
        <select id="top_menu_hover_type" name="top_menu_hover_type">
            '.$top_menu_hover_type.'
        </select> 
    </div>';
    
    $transparent_menu_font_color                =  esc_html ( get_option('wp_estate_transparent_menu_font_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Transparent Header - Top Menu Font Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Transparent Header - Top Menu Font Color','wpresidence').'</div>    
        <input type="text" name="transparent_menu_font_color" value="'.$transparent_menu_font_color.'"  maxlength="7" class="inptxt" />
        <div id="transparent_menu_font_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$transparent_menu_font_color.';" ></div></div>
    </div>';
        
    
    $transparent_menu_hover_font_color               =  esc_html ( get_option('wp_estate_transparent_menu_hover_font_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Transparent Header - Top Menu Hover Font Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Transparent Header - Top Menu Hover Font Color','wpresidence').'</div>    
        <input type="text" name="transparent_menu_hover_font_color" value="'.$transparent_menu_hover_font_color.'"  maxlength="7" class="inptxt" />
        <div id="transparent_menu_hover_font_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$transparent_menu_hover_font_color.';" ></div></div>
    </div>';
    
    
    $menu_items_color        =  esc_html ( get_option('wp_estate_menu_items_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Menu Item Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Menu Item Color','wpresidence').'</div>    
        <input type="text" name="menu_items_color" value="'.$menu_items_color.'"  maxlength="7" class="inptxt" />
        <div id="menu_items_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$menu_items_color.';" ></div></div>
    </div>';
    
    $menu_hover_font_color          =  esc_html ( get_option('wp_estate_menu_hover_font_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Menu Item hover font color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Menu Item hover font color','wpresidence').'</div>    
        <input type="text" name="menu_hover_font_color" value="'.$menu_hover_font_color.'" maxlength="7" class="inptxt" />
        <div id="menu_hover_font_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$menu_hover_font_color.';" ></div></div>
    </div>';
    
     
    $menu_item_back_color         =  esc_html ( get_option('wp_estate_menu_item_back_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Menu Item Back Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Menu Item  Back Color','wpresidence').'</div>    
       <input type="text" name="menu_item_back_color" value="'.$menu_item_back_color.'"  maxlength="7" class="inptxt" />
        <div id="menu_item_back_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$menu_item_back_color.';"></div></div>
    </div>';
    
    
    $menu_hover_back_color          =  esc_html ( get_option('wp_estate_menu_hover_back_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Menu Item Hover Back Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Menu Item Hover Back Color','wpresidence').'</div>    
       <input type="text" name="menu_hover_back_color" value="'.$menu_hover_back_color.'"  maxlength="7" class="inptxt" />
        <div id="menu_hover_back_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$menu_hover_back_color.';"></div></div>
    </div>';
 
       
    $menu_border_color          =  esc_html ( get_option('wp_estate_menu_border_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Menu border color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Menu border color','wpresidence').'</div>    
        <input type="text" name="menu_border_color" value="'.$menu_border_color.'" maxlength="7" class="inptxt" />
        <div id="menu_border_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$menu_border_color.';" ></div></div>
    </div>';
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;




if( !function_exists('new_wpestate_property_list_design_details')):
function new_wpestate_property_list_design_details(){
   
    $cache_array =   array(   0 =>esc_html__('default','wpresidence'),
                                1 =>esc_html__('type 1','wpresidence'), 
                                2 =>esc_html__('type 2','wpresidence'),
                                3 =>esc_html__('type 3','wpresidence'), 
                                4 =>esc_html__('type 4','wpresidence'),
                            );
    $unit_card_type=  wpestate_dropdowns_theme_admin_with_key($cache_array,'unit_card_type');
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Unit Card Type','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Unit Card Type','wpresidence').'</div>    
        <select id="unit_card_type" name="unit_card_type">
            '.$unit_card_type.'
        </select> 
    </div>';
    
    
    $cache_array=array(3,4);
    $listings_per_row=  wpestate_dropdowns_theme_admin($cache_array,'listings_per_row');
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('No of property listings per row when the page is without sidebar','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('When the page is with sidebar the no of listings per row will be 2 or 3 - depending on your selection','wpresidence').'</div>    
        <select id="listings_per_row" name="listings_per_row">
            '.$listings_per_row.'
        </select> 
    </div>';
    
    $agent_listings_per_row=  wpestate_dropdowns_theme_admin($cache_array,'agent_listings_per_row');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('No of agent listings per row when the page is without sidebar','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('When the page is with sidebar the no of listings per row will be 2 or 3 - depending on your selection','wpresidence').'</div>    
        <select id="agent_listings_per_row" name="agent_listings_per_row">
            '.$agent_listings_per_row.'
        </select> 
    </div>';
    
      
    $blog_listings_per_row=  wpestate_dropdowns_theme_admin($cache_array,'blog_listings_per_row');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('No of blog listings per row when the page is without sidebar','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('When the page is with sidebar the no of listings per row will be 2 or 3 - depending on your selection','wpresidence').'</div>    
        <select id="blog_listings_per_row" name="blog_listings_per_row">
            '.$blog_listings_per_row.'
        </select> 
    </div>';
    
    
    $prop_unit_min_height     = get_option('wp_estate_prop_unit_min_height','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Property Unit/Card min height','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Property Unit/Card min height ','wpresidence').'</div>    
        <input  type="text" id="prop_unit_min_height" name="prop_unit_min_height"  value="'.$prop_unit_min_height.'"/> 
    </div>';
    
    
    $agent_unit_min_height     = get_option('wp_estate_agent_unit_min_height','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Agent Unit/Card min height','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Agent Unit/Card min height(works on agent lists and agent taxonomy) ','wpresidence').'</div>    
        <input  type="text" id="agent_unit_min_height" name="agent_unit_min_height"  value="'.$agent_unit_min_height.'"/> 
    </div>';
    
    $blog_unit_min_height     = get_option('wp_estate_blog_unit_min_height','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Blog Unit/Card min height','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Blog Unit/Card min height ','wpresidence').'</div>    
        <input  type="text" id="blog_unit_min_height" name="blog_unit_min_height"  value="'.$blog_unit_min_height.'"/> 
    </div>';
      
    $propertyunit_internal_padding_top          = get_option('wp_estate_propertyunit_internal_padding_top','');
    $propertyunit_internal_padding_left         = get_option('wp_estate_propertyunit_internal_padding_left','');
    $propertyunit_internal_padding_bottom       = get_option('wp_estate_propertyunit_internal_padding_bottom','');
    $propertyunit_internal_padding_right        = get_option('wp_estate_propertyunit_internal_padding_right','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Property,Agent and Blog Unit/Card Internal Padding','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Property,Agent and Blog Unit/Card Internal Padding (top,left,bottom,right)','wpresidence').'</div>    
        <input  style="width:100px;min-width:100px;" type="text" id="propertyunit_internal_padding_top" name="propertyunit_internal_padding_top"  value="'.$propertyunit_internal_padding_top.'"/> 
        <input  style="width:100px;min-width:100px;" type="text" id="propertyunit_internal_padding_left" name="$propertyunit_internal_padding_left"  value="'.$propertyunit_internal_padding_left.'"/> 
        <input  style="width:100px;min-width:100px;" type="text" id="propertyunit_internal_padding_bottom" name="propertyunit_internal_padding_bottom"  value="'.$propertyunit_internal_padding_bottom.'"/> 
        <input  style="width:100px;min-width:100px;" type="text" id="propertyunit_internal_padding_right" name="propertyunit_internal_padding_right"  value="'.$propertyunit_internal_padding_right.'"/> 
    </div>';
    
    $property_unit_color            =  esc_html ( get_option('wp_estate_property_unit_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Property,Agent and Blog Unit/Card Backgrond Color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Property,Agent and Blog Unit/Card Backgrond Color','wpresidence').'</div>    
        <input type="text" name="property_unit_color" value="'.$property_unit_color .'" maxlength="7" class="inptxt" />
        <div id="property_unit_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$property_unit_color .';"></div></div>
    </div>';
    
    $unit_border_size     = get_option('wp_estate_unit_border_size','');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Unit border size','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Unit border size','wpresidence').'</div>    
        <input  type="text" id="unit_border_size" name="unit_border_size"  value="'.$unit_border_size.'"/> 
    </div>';
     
    $unit_border_color            =  esc_html ( get_option('wp_estate_unit_border_color','') );
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Unit/Card border color','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Unit/Card border color','wpresidence').'</div>    
        <input type="text" name="unit_border_color" value="'.$unit_border_color .'" maxlength="7" class="inptxt" />
        <div id="unit_border_color" class="colorpickerHolder"><div class="sqcolor" style="background-color:#'.$unit_border_color .';"></div></div>
    </div>';

    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
    
}
endif;







if( !function_exists('new_wpestate_property_page_design_details')):
function new_wpestate_property_page_design_details(){
 
    print'<div class="estate_option_row" style="border:none;padding-bottom: 0px!important;">';
        $content        = get_option('wpestate_property_page_content',true);

          
        $wpestate_uset_unit     = intval ( get_option('wpestate_uset_unit','') );
        print'<div class="estate_option_row" style="border:none;">
        <div class="label_option_row" style="margin-left: -230px;">'.esc_html__('Use this unit/card','wpresidence').'</div>
       
        <input  type="checkbox" id="use_unit_design" name="use_unit_design" ';
            if( $wpestate_uset_unit==1){
                print ' checked="checked" ';
            }
        print ' value="1"/> 
        </div>';

        print '<div>'.esc_html__('This property unit builder is a very complex feature, with a lot of options, and because of that it may not work for all design idees. We will continue to improve it, but please be aware that css problems may appear and those will have to be solved by manually adding css rules in the code.','wpresidence'
                ).'</div>';
        
        print '<div id="property_page_design_wrapper">';
            print '<div id="property_page_content" class="elements_container">
            <div class="property_page_content_wrapper">'. html_entity_decode ( stripslashes($content) ).'</div>
            <div class="add_me_new_row">+</div></div>';
        print '</div>';
    print'</div>';
     
    
    print ' <div class="estate_option_row_submit">
    <div id="save_prop_design">'.esc_html__('Save Design','wpresidence').'</div>
  
    </div>';
    
    $extra_elements = array( 
        'Icon'          =>  'icon',
        'Text'          =>  'text',
        'Featured'      =>  'featured_icon',
        'Status'        =>  'property_status',
        'Share'         =>  'share',
        'Favorite'      =>  'favorite',
        'Compare'       =>  'compare',
        'Custom Div'    =>  'custom_div',
        'Link to page'  =>  'link_to_page'
        
        );
    $design_elements=wpestate_all_prop_details_prop_unit();
    
    print '<div class="modal fade" tabindex="-1" role="dialog" id="modal_el_pick">
        <div id="modal_el_pick_close">x</div>';
    
    foreach($extra_elements as $key=>$value){
        print '<div class="prop_page_design_el_modal" data-tip="'.$value.'">'.$key.'</div>';
    }
    
    foreach($design_elements as $key=>$value){
        print '<div class="prop_page_design_el_modal" data-tip="'.$value.'">'.$key.'</div>';
    }
    
    print'
    </div><!-- /.modal -->';
    

    print '<div class="modal fade" tabindex="-1" role="dialog" id="modal_el_options">
        <div class="modal_el_options_content">
            
            <div class="modal_el_options_content_element" id="icon-image-row">
                <label for="icon-image">Icon/Image</label>
                <input type="text" id="icon-image" name="icon-image">
            </div>
            
            <div class="modal_el_options_content_element" id="custom-text-row">
                <label for="text">Text</label>
                <input type="text" id="custom-text" name="custom-text">
            </div>

            <div class="modal_el_options_content_element">
                <label for="margin-top">Margin Top/Top </label>
                <input type="text" id="margin-top" name="margin-top">
            </div>
            
            <div class="modal_el_options_content_element">
                <label for="margin-left">Margin Left/Left </label>
                <input type="text" id="margin-left" name="margin-left">
            </div>

            <div class="modal_el_options_content_element">
                <label for="margin-left">Margin Bottom/Bottom </label>
                <input type="text" id="margin-bottom" name="margin-bottom">
            </div>

            <div class="modal_el_options_content_element">
                <label for="margin-right">Margin Right/Right</label>
                <input type="text" id="margin-right" name="margin-right">
            </div>
            

            
            <div class="modal_el_options_content_element">
                <label for="position-absolute">Position absolute?</label>
                <input type="checkbox" id="position-absolute" value="1">
            </div>
            
            
            <div class="modal_el_options_content_element">
                <label for="font-size">Font Size</label>
                <input type="text" id="font-size" name="font-size">
            </div>
            
            <div class="modal_el_options_content_element">
                <label for="font-color">Font Color</label>
                <input type="text" id="font-color" name="font-color">
            </div>
            
            <div class="modal_el_options_content_element">
                <label for="font-color">Back Color</label>
                <input type="text" id="back-color" name="back-color">
            </div>
            
            

            <div class="modal_el_options_content_element">
                <label for="padding-top">Padding Top</label>
                <input type="text" id="padding-top" name="padding-top">
            </div>
            
            <div class="modal_el_options_content_element">
                <label for="padding-left">Padding Left </label>
                <input type="text" id="padding-left" name="padding-left">
            </div>

            <div class="modal_el_options_content_element">
                <label for="padding-bottom">Padding Bottom</label>
                <input type="text" id="padding-bottom" name="padding-bottom">
            </div>

            <div class="modal_el_options_content_element">
                <label for="padding-right">Padding Right</label>
                <input type="text" id="padding-right" name="padding-right">
            </div>

            <div class="modal_el_options_content_element">
                <label for="padding-right">Align</label>
                <select id="text-align" name="text-align">
                    <option value=""></option>
                    <option value="left">left</option>
                    <option value="right">right</option>
                    <option value="center">center</option>
                </select>
            </div>
            
            <div class="modal_el_options_content_element">
                <label for="extra_css">Extra Css Class</label>
                <input type="text" id="extra_css" name="extra_css">
            </div>

            <input type="button" id="save_custom_el_css" value="apply changes">




        </div>
    <div id="modal_el_options_close">x</div>';
    
    print'
    </div><!-- /.modal -->';
    
}
endif;





if( !function_exists('wpestate_print_page_design')):
function wpestate_print_page_design(){
   
    
    $yesno=array('yes','no');
         
            
    $print_show_subunits           =   wpestate_dropdowns_theme_admin($yesno,'print_show_subunits');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show subunits section','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Show subunits section in print page?','wpresidence').'</div>    
        <select id="print_show_subunits" name="print_show_subunits">
            '.$print_show_subunits.'
        </select> 
    </div>';
    
    $print_show_agent           =   wpestate_dropdowns_theme_admin($yesno,'print_show_agent');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show agent details section','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Show agent details section in print page?','wpresidence').'</div>    
        <select id="print_show_agent" name="print_show_agent">
            '.$print_show_agent.'
        </select> 
    </div>';
    
    $print_show_description           =   wpestate_dropdowns_theme_admin($yesno,'print_show_description');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show description section','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Show description section in print page?','wpresidence').'</div>    
        <select id="print_show_description" name="print_show_description">
            '.$print_show_description.'
        </select> 
    </div>';
    
    
    $print_show_adress           =   wpestate_dropdowns_theme_admin($yesno,'print_show_adress');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show address section','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Show address section in print page?','wpresidence').'</div>    
        <select id="print_show_adress" name="print_show_adress">
            '.$print_show_adress.'
        </select> 
    </div>';
    
    
    $print_show_details           =   wpestate_dropdowns_theme_admin($yesno,'print_show_details');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show details section','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Show details section in print page?','wpresidence').'</div>    
        <select id="print_show_details" name="print_show_details">
            '.$print_show_details.'
        </select> 
    </div>';
    
    $print_show_features           =   wpestate_dropdowns_theme_admin($yesno,'print_show_features');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show features & amenities section','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Show features & amenities section in print page?','wpresidence').'</div>    
        <select id="print_show_features" name="print_show_features">
            '.$print_show_features.'
        </select> 
    </div>';
    
    
    $print_show_floor_plans           =   wpestate_dropdowns_theme_admin($yesno,'print_show_floor_plans');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show floor plans section','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Show floor plans section in print page?','wpresidence').'</div>    
        <select id="print_show_floor_plans" name="print_show_floor_plans">
            '.$print_show_floor_plans.'
        </select> 
    </div>';
    
    $print_show_images           =   wpestate_dropdowns_theme_admin($yesno,'print_show_images');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show gallery section','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Show gallery section in print page?','wpresidence').'</div>    
        <select id="print_show_images" name="print_show_images">
            '.$print_show_images.'
        </select> 
    </div>';
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;



if(!function_exists('wpestate_user_dashboard_design')):
function wpestate_user_dashboard_design(){ 
    
   $cache_array                =   array('yes','no');   
   $show_header_dashboard      = wpestate_dropdowns_theme_admin($cache_array,'show_header_dashboard');
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Show Header in Dashboard ?','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Enable or disable header in dashboard. The header will always be wide & type1 !','wpresidence').'</div>    
       <select id="show_header_dashboard" name="show_header_dashboard">
            '.$show_header_dashboard.'
        </select>
    </div>';
    
    $user_dashboard_menu_color  =  esc_html ( get_option('wp_estate_user_dashboard_menu_color','') );
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('User Dashboard Menu Color','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('User Dashboard Menu Color','wpresidence').'</div>    
            <input type="text" name="user_dashboard_menu_color" value="'.$user_dashboard_menu_color.'" maxlength="7" class="inptxt" />
            <div id="user_dashboard_menu_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$user_dashboard_menu_color.';" ></div></div>
        </div>';
    
    
    $user_dashboard_menu_hover_color      =  esc_html ( get_option('wp_estate_user_dashboard_menu_hover_color','') );
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('User Dashboard Menu Hover Color','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('User Dashboard Menu Hover Color','wpresidence').'</div>    
            <input type="text" name="user_dashboard_menu_hover_color" value="'.$user_dashboard_menu_hover_color.'" maxlength="7" class="inptxt" />
            <div id="user_dashboard_menu_hover_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$user_dashboard_menu_hover_color.';" ></div></div>
        </div>';  
    
    $user_dashboard_menu_color_hover  =  esc_html ( get_option('wp_estate_user_dashboard_menu_color_hover','') );
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('User Dashboard Menu Item Background Color','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('User Dashboard Menu Item Background Color','wpresidence').'</div>    
            <input type="text" name="user_dashboard_menu_color_hover" value="'.$user_dashboard_menu_color_hover.'" maxlength="7" class="inptxt" />
            <div id="user_dashboard_menu_color_hover" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$user_dashboard_menu_color_hover.';" ></div></div>
        </div>';
    
    $user_dashboard_menu_back      =  esc_html ( get_option('wp_estate_user_dashboard_menu_back ','') );
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('User Dashboard Menu Background','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('User Dashboard Menu Background','wpresidence').'</div>    
            <input type="text" name="user_dashboard_menu_back" value="'.$user_dashboard_menu_back.'" maxlength="7" class="inptxt" />
            <div id="user_dashboard_menu_back" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$user_dashboard_menu_back .';" ></div></div>
        </div>';
    
    
    $user_dashboard_package_back      =  esc_html ( get_option('wp_estate_user_dashboard_package_back ','') );
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('User Dashboard Package Background','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('User Dashboard Package Background','wpresidence').'</div>    
            <input type="text" name="user_dashboard_package_back" value="'.$user_dashboard_package_back.'" maxlength="7" class="inptxt" />
            <div id="user_dashboard_package_back" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$user_dashboard_package_back .';" ></div></div>
        </div>';
    
    $user_dashboard_package_color     =  esc_html ( get_option('wp_estate_user_dashboard_package_color ','') );
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('User Dashboard Package Color','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('User Dashboard Package Color','wpresidence').'</div>    
            <input type="text" name="user_dashboard_package_color" value="'.$user_dashboard_package_color.'" maxlength="7" class="inptxt" />
            <div id="user_dashboard_package_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$user_dashboard_package_color .';" ></div></div>
        </div>';
    
    
    $user_dashboard_buy_package     =  esc_html ( get_option('wp_estate_user_dashboard_buy_package','') );
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Dashboard Buy Package Select Background','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Dashboard Package Selected','wpresidence').'</div>    
            <input type="text" name="user_dashboard_buy_package" value="'.$user_dashboard_buy_package.'" maxlength="7" class="inptxt" />
            <div id="user_dashboard_buy_package" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$user_dashboard_buy_package .';" ></div></div>
        </div>';
    
    $user_dashboard_package_select     =  esc_html ( get_option('wp_estate_user_dashboard_package_select','') );
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Dashboard Package Select','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Dashboard Package Select','wpresidence').'</div>    
            <input type="text" name="user_dashboard_package_select" value="'.$user_dashboard_package_select.'" maxlength="7" class="inptxt" />
            <div id="user_dashboard_package_select" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$user_dashboard_package_select .';" ></div></div>
        </div>';
    
    $user_dashboard_content_back     =  esc_html ( get_option('wp_estate_user_dashboard_content_back ','') );
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Content Background Color','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Content Background Color','wpresidence').'</div>    
            <input type="text" name="user_dashboard_content_back" value="'.$user_dashboard_content_back.'" maxlength="7" class="inptxt" />
            <div id="user_dashboard_content_back" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$user_dashboard_content_back .';" ></div></div>
        </div>';
    
    $user_dashboard_content_button_back     =  esc_html ( get_option('wp_estate_user_dashboard_content_button_back  ','') );
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Content Button Background','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Content Button Background','wpresidence').'</div>    
            <input type="text" name="user_dashboard_content_button_back" value="'.$user_dashboard_content_button_back.'" maxlength="7" class="inptxt" />
            <div id="user_dashboard_content_button_back" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$user_dashboard_content_button_back .';" ></div></div>
        </div>';
    
    $user_dashboard_content_color     =  esc_html ( get_option('wp_estate_user_dashboard_content_color','') );
    print'<div class="estate_option_row">
        <div class="label_option_row">'.esc_html__('Content Text Color','wpresidence').'</div>
        <div class="option_row_explain">'.esc_html__('Content Text Color','wpresidence').'</div>    
            <input type="text" name="user_dashboard_content_color" value="'.$user_dashboard_content_color.'" maxlength="7" class="inptxt" />
            <div id="user_dashboard_content_color" class="colorpickerHolder" ><div class="sqcolor" style="background-color:#'.$user_dashboard_content_color .';" ></div></div>
        </div>';
    
    
    print ' <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;










if(!function_exists('new_property_submission_tab') ):
function new_property_submission_tab(){
    
   
   
    $all_submission_fields  =   wpestate_return_all_fields();
    $all_mandatory_fields   =   wpestate_return_all_fields(1);
  
    
    $submission_page_fields =   ( get_option('wp_estate_submission_page_fields','') );
    if(is_array($submission_page_fields)){
        $submission_page_fields =   array_map("wpestate_strip_array",$submission_page_fields);
    }

       
   


    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Select the Fields for property submission.','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Use CTRL to select multiple fields for property submission page.','wpresidence').'</div>    

		<input type="hidden" name="submission_page_fields" />
        <select id="submission_page_fields" name="submission_page_fields[]" multiple="multiple" style="height:400px">';

        foreach ($all_submission_fields as $key=>$value){
            print '<option value="'.$key.'"';
            if (is_array($submission_page_fields) && in_array($key, $submission_page_fields) ){
                print ' selected="selected" ';
            }
            print '>'.$value.'</option>';
        }    

        print'
        </select>

    </div>';

        
    $mandatory_fields           =   ( get_option('wp_estate_mandatory_page_fields','') );
    
    if(is_array($mandatory_fields)){
        $mandatory_fields           =   array_map("wpestate_strip_array",$mandatory_fields);
    }
    
    print'<div class="estate_option_row">
    <div class="label_option_row">'.esc_html__('Select the Mandatory Fields for property submission.','wpresidence').'</div>
    <div class="option_row_explain">'.esc_html__('Make sure the mandatory fields for property submission page are part of submit form (managed from the above setting). Use CTRL for multiple fields select..','wpresidence').'</div>    

		<input type="hidden" name="mandatory_page_fields" />
        <select id="mandatory_page_fields" name="mandatory_page_fields[]" multiple="multiple" style="height:400px">';

        foreach ($all_mandatory_fields as $key=>$value){
       
            print '<option value="'.stripslashes($key).'"';
            if (is_array($mandatory_fields) && in_array( addslashes($key), $mandatory_fields) ){
                print ' selected="selected" ';
            }
            print '>'.$value.'</option>';
        }    

        print'
        </select>

    </div>';
    
    
    print '<input type="hidden" value="1" name="is_submit_page"> <div class="estate_option_row_submit">
    <input type="submit" name="submit"  class="new_admin_submit " value="'.esc_html__('Save Changes','wpresidence').'" />
    </div>';
}
endif;



?>