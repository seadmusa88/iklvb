<?php
if( !function_exists('wpestate_clear_cache_theme') ):
function wpestate_clear_cache_theme() {  
    wpestate_delete_cache();
    print'<div class="wrap">'.esc_html__('Cache was cleared','wpresidence').'</div>';
    exit();
}
endif;
