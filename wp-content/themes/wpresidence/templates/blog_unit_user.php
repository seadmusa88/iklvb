<?php
// blog listing
global $options;
$link       =        get_permalink();
$thumb_id           = get_post_thumbnail_id($post->ID);
$preview            = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
$preview_img        = $preview[0];

$name               = get_the_title();

$user_id			= get_post_meta($post->ID,'user_meda_id',true);
$author = get_userdata($user_id);


$preview            = wp_get_attachment_image_src(get_the_author_meta('small_custom_picture',$author->ID),array('500','400'));
//$preview            = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
$preview_img        = $preview[0];

if($preview_img==''){
    $preview_img    =   get_template_directory_uri().'/img/default_user_agent.gif';
}


?>

<div class="blog-unit-wrapper">

    <div class="row">

        <div class="col-xs-4 agency_content agentpic-wrapper-">
            <div class="agent-listing-img-wrapper" data-link="<?php print esc_url($link);?>">
                <div style="background-image:url(<?php print $preview_img;?>);" class="agent-image-cover-list"></div>


                <?php
                /*
                if ( 'estate_agent' != get_post_type($prop_id)) { ?>
                    <a href="<?php print esc_url($link);?>" style="background-image:url(<?php print $preview_img;?>);" class="agent-image-cover"></a>
                <?php
                }else{ ?>
                    <a style="background-image:url(<?php print $preview_img;?>);" class="agent-image-cover"></a>
                <?php }
                 *
                 */
                ?>

                <?php
                /*
                <div class="listing-cover"></div>
                <div class="listing-cover-title"><a href="<?php print esc_url($link);?>"><?php print esc_html($name);?></a></div>
                */
                ?>
            </div>

            <!-- div class="agent_unit_social_single">
                <div class="social-wrapper">

                    <?php
            /*
            if($agent_facebook!=''){
                print ' <a href="'. $agent_facebook.'" target="_blank"><i class="fa fa-facebook"></i></a>';
            }

            if($agent_twitter!=''){
                print ' <a href="'.$agent_twitter.'" target="_blank"><i class="fa fa-twitter"></i></a>';
            }
            if($agent_linkedin!=''){
                print ' <a href="'.$agent_linkedin.'" target="_blank"><i class="fa fa-linkedin"></i></a>';
            }
            if($agent_pinterest!=''){
                print ' <a href="'. $agent_pinterest.'" target="_blank"><i class="fa fa-pinterest"></i></a>';
            }
            if($agent_instagram!=''){
                print ' <a href="'. $agent_instagram.'" target="_blank"><i class="fa fa-instagram"></i></a>';
            }
             *
             */
            ?>

                 </div>
            </div -->
        </div>

        <div class="col-xs-8 agent_details-">
            <?php
            /*
            <div class="mydetails">
                <?php esc_html_e('My details ','wpresidence');?>
            </div>
            */

            $author         = get_post_field( 'post_author', $post->ID) ;
            $agency_post    = get_the_author_meta('user_id',$author);




            print ' <a href="'.get_permalink($agency_post).'"><h3>'.get_the_title($agency_post).'</h3></a>';


            print '
            <div class="agent_position">';
             echo esc_html__('Private user','wpredidence');
            print'</div>';


            /*
            if ($agent_phone) {
                print '<div class="agent_detail agent_phone_class"><i class="fa fa-phone"></i><a href="tel:' . $agent_phone . '">' . $agent_phone . '</a></div>';
            }
            if ($agent_mobile) {
                print '<div class="agent_detail agent_mobile_class"><i class="fa fa-mobile"></i><a href="tel:' . $agent_mobile . '">' . $agent_mobile . '</a></div>';
            }

            if ($agent_email) {
                print '<div class="agent_detail agent_email_class"><i class="fa fa-envelope-o"></i><a href="mailto:' . $agent_email . '">' . $agent_email . '</a></div>';
            }

            if ($agent_skype) {
                print '<div class="agent_detail agent_skype_class"><i class="fa fa-skype"></i>' . $agent_skype . '</div>';
            }

            if ($agent_urlc) {
                print '<div class="agent_detail agent_web_class"><i class="fa fa-desktop"></i><a href="http://'.$agent_urlc.'" target="_blank">'.$agent_urlc.'</a></div>';
            }


            if($agent_member){
                print '<div class="agent_detail agent_web_class"><strong>'.esc_html__('Member of:','wpresidence').'</strong> '.$agent_member.'</div>';

            }
            */

            ?>
        </div>
    </div>
</div>

