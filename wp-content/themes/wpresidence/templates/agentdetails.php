<?php
global $options;
global $prop_id;
global $post;
global $agent_url;
global $agent_urlc;
global $link;
global $agent_facebook;
global $agent_posit;
global $agent_twitter; 
global $agent_linkedin; 
global $agent_instagram;
global $agent_pinterest; 
global $agent_member;

$pict_size=5;
$content_size=7;

if ($options['content_class']=='col-md-12'){
   $pict_size=5; 
   $content_size=7; 
}

if ( get_post_type($prop_id) == 'estate_property' ){
    $pict_size=5;
    $content_size=7;
    if ($options['content_class']=='col-md-12'){
       $pict_size=3; 
       $content_size=9;
    }   
}


if($preview_img==''){
    $preview_img    =   get_template_directory_uri().'/img/default_user_agent.gif';
}
$attachment = get_post( $attachment_id );

//print 'post author: '. $post->post_author.'.';
$args = array(
    'post_type'         =>  'estate_user',
    'post_status'       => 'publish',
    'meta_query' => array(
        array(
            'key'     => 'user_meda_id',
            'value'   => $post->post_author
        ),
    ),
);

$prop_selection = new WP_Query($args);
$i=0;
while ($prop_selection->have_posts()): $prop_selection->the_post();
$linkuser=get_permalink($post);
$i++;
endwhile;
//print get_post_type($prop_selection->post_author);
//print $i;
if ($i==0)
    $linkuser="#";
?>
      
             
<div class="wpestate_agent_details_wrapper">
    <div class="col-md-2 col-sm-2 col-xs-2"> 
        <a href="<?php print $linkuser;?>" class="reviewer_image" style="background-image:url('<?php print $preview_img;?>')">                 
        </a>
	</div>
    
</div>




<?php 
if ( 'estate_agent' == get_post_type($prop_id)) { ?>
        <div class="agent_content col-md-12">
            <h4><?php esc_html_e('About Me ','wpresidence'); ?></h4>    
            <?php the_content();?>
        </div>
<?php }
?>