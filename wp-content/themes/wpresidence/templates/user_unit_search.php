<?php
global $options;

$thumb_id           = get_post_thumbnail_id($post->ID);
$preview            = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');

$name               = get_the_title();
$link               = get_permalink();
$counter            = '';


$user_for_id = intval(get_post_meta($post->ID,'user_meda_id',true));
if($user_for_id!=0){
$counter            =   count_user_posts($user_for_id,'estate_property',true);
}


$extra= array(
        'data-original'=>$preview[0],
        'class'	=> 'lazyload img-responsive',    
        );


$user_id			= get_post_meta($post->ID,'user_meda_id',true);
$author = get_userdata($user_id);


$preview            = wp_get_attachment_image_src(get_the_author_meta('small_custom_picture',$author->ID),array('400','400'));
$preview_img        = $preview[0];
$thumb_prop    = '<img src="'.$preview_img.'" alt="agent-images">';

if($preview_img==''){
    $thumb_prop = '<img src="'.get_template_directory_uri().'/img/default_user_search.png" alt="agent-images">';
}

$col_class=4;
if($options['content_class']=='col-md-12'){
    $col_class=3;
}


           
?>




    <div class="agent_unit" data-link="<?php print esc_url($link);?>">
    <div class="agent-unit-img-wrapper">
        <?php if($user_for_id!=0){ ?>
            <div class="agent_card_my_listings">
                <?php print $counter.' ';
                if($counter!=1){
                    esc_html_e('listings','wpresidence');
                }else{
                    esc_html_e('listing','wpresidence');
                }
                ?>
            </div>
        <?php } ?>


            
            
            <div class="prop_new_details_back"></div>
            <?php 
                print $thumb_prop; 
            ?>
        </div>    
            
        <div class="">
            <?php
            print '<h4> <a href="' . $link . '">' . $name. '</a></h4>
            <div class="agent_position">'. $agent_posit .'</div>';
           

            ?>
           
          
        </div> 
          
        <a href="<?php print esc_url($link); ?>"  class=" agent_unit_button agent_unit_contact_me" ><?php esc_html_e('Contact me','wpresidence');?></a>
        
        <div class="agent_unit_social agent_list">
            <div class="social-wrapper"> 
               
               <?php
               

                ?>
            </div>
        </div>
    </div>
<!-- </div>    -->