     
<div id="results">
    <div class="results_header">
        <?php esc_html_e('We found ','wpresidence'); ?> <span id="results_no">0</span> <?php esc_html_e('results.','wpresidence'); ?>  
        <span id="preview_view_all"><?php esc_html_e('View results','wpresidence');?></span>
        <!--<span id="showinpage"> <?php //esc_html_e('Do you want to load the results now ?','wpresidence');?> </span>-->
    </div>
    <div id="results_wrapper">
    </div>
</div>