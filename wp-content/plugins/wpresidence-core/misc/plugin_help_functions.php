<?php


if ( ! function_exists( 'wpestate_admin_bar_menu' ) ) {
	function wpestate_admin_bar_menu() {
            global $wp_admin_bar;
            $theme_data = wp_get_theme();
		

            if ( ! current_user_can( 'manage_options' ) || ! is_admin_bar_showing() ) { return;
            }
            
                $wp_admin_bar->add_menu(array(
                        'id' => 'theme_options',
                        'title' => esc_html__( 'WpResidence Options', 'wpresidence-core' ),
                        'href' => admin_url( 'admin.php?page=libs%2Ftheme-admin.php' ),
                ));
                
         

                $wp_admin_bar->add_menu(
                        array(
                                'title' => esc_html__( 'Clear WpResidence Cache', 'wpresidence-core' ),
                                'id' => 'clear_cache',
                                'href' => wp_nonce_url( admin_url( 'admin-post.php?action=wpestate_purge_cache' ) , 'theme_purge_cache' ),
                        )
                );
            }
}
add_action( 'admin_bar_menu', 'wpestate_admin_bar_menu', 100 );


////////////////////////////////////////////////////////////////////////////////
/// print page function 
////////////////////////////////////////////////////////////////////////////////


  add_action( 'wp_ajax_nopriv_wpestate_ajax_create_print', 'wpestate_ajax_create_print' );  
  add_action( 'wp_ajax_wpestate_ajax_create_print', 'wpestate_ajax_create_print' );  
  
  if( !function_exists('wpestate_ajax_create_print') ):
  function wpestate_ajax_create_print(){ 
      
    if(!isset($_POST['propid'])|| !is_numeric($_POST['propid'])){
        exit('out pls1');
    }  
      
    $post_id            = intval($_POST['propid']);
    
    $the_post= get_post( $post_id); 
    if($the_post->post_type!='estate_property' || $the_post->post_status!='publish'){
        exit('out pls2');
    }
    
    $unit               = esc_html( get_option('wp_estate_measure_sys', '') );
    $currency           = esc_html( get_option('wp_estate_currency_symbol', '') );
    $where_currency     = esc_html( get_option('wp_estate_where_currency_symbol', '') );
    $property_address   = esc_html( get_post_meta($post_id, 'property_address', true) );
    $property_city      = strip_tags ( get_the_term_list($post_id, 'property_city', '', ', ', '') );
    $property_area      = strip_tags ( get_the_term_list($post_id, 'property_area', '', ', ', '') );
    $property_county    = esc_html( get_post_meta($post_id, 'property_county', true) );
    $property_zip       = esc_html(get_post_meta($post_id, 'property_zip', true) );
    $property_country   = esc_html(get_post_meta($post_id, 'property_country', true) );
    $ref_code           = get_post_meta($post_id, 'reference_code', true); 
      
    $property_size                  = wpestate_get_converted_measure( $post_id, 'property_size' ); 
    $property_bedrooms              = floatval ( get_post_meta($post_id, 'property_bedrooms', true) );
    $property_bathrooms             = floatval ( get_post_meta($post_id, 'property_bathrooms', true) );     
    $property_year                  = floatval ( get_post_meta($post_id, 'property_year', true) );  
                  
             
    $image_id           = get_post_thumbnail_id($post_id);
    $full_img           = wp_get_attachment_image_src($image_id, 'full');
    $full_img           = $full_img [0];
  
  
    $title              = get_the_title($post_id); 
    $page_object        = get_page( $post_id );
    $content            = $page_object->post_content;
    
   
    $content            = apply_filters('the_content',$content);

    
    $price              = floatval   ( get_post_meta($post_id, 'property_price', true) );
  
    if ($price != 0) {
        $price = wpestate_show_price($post_id,$currency,$where_currency,1);    
    }else{
        $price='';
    }
    
    $feature_list_array =   array();
    $feature_list       =   esc_html( get_option('wp_estate_feature_list') );
    $feature_list_array =   explode( ',',$feature_list);
    $all_features   ='';
    if ( !count( $feature_list_array )==0 ){
        foreach($feature_list_array as $checker => $value){
            $post_var_name=  str_replace(' ','_', trim($value) );
            if (esc_html( get_post_meta($post_id, $post_var_name, true) ) == 1) {
                 $all_features   .='<div class="print-right-row">'. trim($value).'</div>';
            }
        }
    }                    
                        
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // get thumbs
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    $arguments = array( 
        'numberposts'   => -1,
        'post_type'     => 'attachment', 
        'post_parent'   => $post_id,
        'post_status'   => null,
        'exclude'       => $image_id,
        'orderby'       => 'menu_order',
        'order'         => 'ASC'
    );
    $post_attachments = get_posts($arguments);

    
    $agent_email    =   '';
    $agent_skype    =   '';
    $agent_mobile   =   '';
    $agent_phone    =   '';
    $name           =   '';
    $preview_img    =   '';
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // get agent details
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    $author_id      =  wpsestate_get_author($post_id);
   
    $agent_assigned_post    =   intval(get_post_meta($post_id,'property_agent',true));
    $user_assinged_agent    =   intval(get_post_meta($post_id,'user_meda_id',true));
    
    $user_role      = intval (get_user_meta( $author_id, 'user_estate_role', true) );

  
    if($user_assinged_agent==0){
            $thumb_id       = get_post_thumbnail_id($agent_assigned_post);
            $preview        = wp_get_attachment_image_src(get_post_thumbnail_id($agent_assigned_post), 'property_listings');
            $preview_img    = $preview[0];
            $agent_skype    = esc_html( get_post_meta($agent_assigned_post, 'agent_skype', true) );
            $agent_phone    = esc_html( get_post_meta($agent_assigned_post, 'agent_phone', true) );
            $agent_mobile   = esc_html( get_post_meta($agent_assigned_post, 'agent_mobile', true) );
            $agent_email    = esc_html( get_post_meta($agent_assigned_post, 'agent_email', true) );
            $agent_pitch    = esc_html( get_post_meta($agent_assigned_post, 'agent_pitch', true) );
            $agent_posit    = esc_html( get_post_meta($agent_assigned_post, 'agent_position', true) );
            $link           = get_permalink($agent_assigned_post);
            $name           = get_the_title($agent_assigned_post);
    }
    
     
    if( $user_role==1 ){
            $user_id=$author_id;
            $preview_img    =   get_the_author_meta( 'custom_picture',$user_id  );
            if($preview_img==''){
                $preview_img=get_template_directory_uri().'/img/default-user.png';
            }
       
            $agent_skype         = get_the_author_meta( 'skype' ,$user_id );
            $agent_phone         = get_the_author_meta( 'phone' ,$user_id );
            $agent_mobile        = get_the_author_meta( 'mobile' ,$user_id );
            $agent_email         = get_the_author_meta( 'user_email',$user_id );
            $agent_pitch         = '';
            $agent_posit         = get_the_author_meta( 'title',$user_id  );
            $agent_facebook      = get_the_author_meta( 'facebook',$user_id  );
            $agent_twitter       = get_the_author_meta( 'twitter',$user_id  );
            $agent_linkedin      = get_the_author_meta( 'linkedin' ,$user_id );
            $agent_pinterest     = get_the_author_meta( 'pinterest',$user_id  );
            $agent_urlc          = get_the_author_meta( 'website' ,$user_id );
            $link                = get_permalink();
            $name                = get_the_author_meta( 'first_name',$user_id ).' '.get_the_author_meta( 'last_name',$user_id);
            $agent_member        = get_the_author_meta( 'agent_member' ,$user_id );
    }else if($user_role==2){
            $agent_id       = get_user_meta($author_id,'user_agent_id',true);
            $thumb_id       = get_post_thumbnail_id($agent_id);
            $preview        = wp_get_attachment_image_src(get_post_thumbnail_id($agent_id), 'property_listings');
            $preview_img    = $preview[0];
            $agent_skype    = esc_html( get_post_meta($agent_id, 'agent_skype', true) );
            $agent_phone    = esc_html( get_post_meta($agent_id, 'agent_phone', true) );
            $agent_mobile   = esc_html( get_post_meta($agent_id, 'agent_mobile', true) );
            $agent_email    =  get_the_author_meta( 'user_email' , $author_id );
            $agent_pitch    = esc_html( get_post_meta($agent_id, 'agent_pitch', true) );
            $agent_posit    = esc_html( get_post_meta($agent_id, 'agent_position', true) );
            $link           = get_permalink($agent_id);
            $name           = get_the_title($agent_id);
    }else if($user_role==3){//agency
            $agent_id       = get_user_meta($author_id,'user_agent_id',true);
            $thumb_id       = get_post_thumbnail_id($agent_id);
            $preview        = wp_get_attachment_image_src(get_post_thumbnail_id($agent_id), 'property_listings');
    
            $preview_img    = $preview[0];
            $agent_skype    = esc_html( get_post_meta($agent_id, 'agency_skype', true) );
            $agent_phone    = esc_html( get_post_meta($agent_id, 'agency_phone', true) );
            $agent_mobile   = esc_html( get_post_meta($agent_id, 'agency_mobile', true) );
            $agent_email    =  get_the_author_meta( 'user_email' , $author_id );
            $agent_pitch    = esc_html( get_post_meta($agent_id, 'agency_pitch', true) );
            $agent_posit    = esc_html( get_post_meta($agent_id, 'agency_position', true) );
            $link           = get_permalink($agent_id);
            $name           = get_the_title($agent_id);
    }else if($user_role==4){//developer
            $agent_id       =get_user_meta($author_id,'user_agent_id',true);
            $thumb_id       = get_post_thumbnail_id($agent_id);
            $preview        = wp_get_attachment_image_src(get_post_thumbnail_id($agent_id), 'property_listings');
            $preview_img    = $preview[0];
            $agent_skype    = esc_html( get_post_meta($agent_id, 'developer_skype', true) );
            $agent_phone    = esc_html( get_post_meta($agent_id, 'developer_phone', true) );
            $agent_mobile   = esc_html( get_post_meta($agent_id, 'developer_mobile', true) );
            $agent_email    =  get_the_author_meta( 'user_email' , $author_id );
            $agent_pitch    = esc_html( get_post_meta($agent_id, 'developer_pitch', true) );
            $agent_posit    = esc_html( get_post_meta($agent_id, 'developer_position', true) );
            $link           = get_permalink($agent_id);
            $name           = get_the_title($agent_id);
    }
    

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // end get agent details
    /////////////////////////////////////////////////////////////////////////////////////////////////////
        
    print  '<html><head><title>'.$title.'</title><link href="'.get_stylesheet_uri().'" rel="stylesheet" type="text/css" />';
    
     
    if(is_child_theme()){
        print '<link href="'.get_template_directory_uri().'/style.css" rel="stylesheet" type="text/css" />';   
    }
    
    if(is_rtl()){
        print '<link href="'.get_template_directory_uri().'/rtl.css" rel="stylesheet" type="text/css" />';
    }
    print '</head>';
    $protocol = is_ssl() ? 'https' : 'http';
    print  '<body class="print_body" >';

    $logo=get_option('wp_estate_logo_image','');
    if ( $logo!='' ){
       print '<img src="'.$logo.'" class="img-responsive printlogo" alt="logo"/>';	
    } else {
       print '<img class="img-responsive printlogo" src="'. get_template_directory_uri().'/img/logo.png" alt="logo"/>';
    }

    print '<h1 class="print_title">'.$title.'</h1>';
    print '<div class="print-price">'.esc_html__('Price','wpresidence-core').': '.$price.'</div>';
    print '<div class="print-addr">'. $property_address. ', ' . $property_city.', '.$property_area.'</div>';
    print '<div class="print-col-img"><img src="'.$full_img.'">';
    print '<img class="print_qrcode" src="https://chart.googleapis.com/chart?cht=qr&chs=110x110&chl='. urlencode( get_permalink($post_id)) .'&choe=UTF-8" title="'.urlencode($title).'" />';
    print'</div>';
    
        
    $property_description_text  =   esc_html( get_option('wp_estate_property_description_text') );
    $property_details_text      =   esc_html( get_option('wp_estate_property_details_text') );
    $property_features_text     =   esc_html( get_option('wp_estate_property_features_text') );
    $property_adr_text          =   stripslashes ( esc_html( get_option('wp_estate_property_adr_text') ) );
    $print_show_images          =   get_option('wp_estate_print_show_images','');
    $print_show_floor_plans     =   get_option('wp_estate_print_show_floor_plans','');
    $print_show_features        =   get_option('wp_estate_print_show_features','');
    $print_show_details         =   get_option('wp_estate_print_show_details','');
    $print_show_adress          =   get_option('wp_estate_print_show_adress','');
    $print_show_description     =   get_option('wp_estate_print_show_description','');
    $print_show_agent          =    get_option('wp_estate_print_show_agent','');
    $print_show_subunits        =   get_option('wp_estate_print_show_subunits','');
     
     
    if($print_show_subunits == 'yes'){ 
        global $property_subunits_master;
        $has_multi_units=intval(get_post_meta($post_id, 'property_has_subunits', true));
        $property_subunits_master=intval(get_post_meta($post_id, 'property_subunits_master', true));

        print '<div class="print_property_subunits_wrapper">';
            if($has_multi_units==1){
                print '<h2 class="print_header">'.esc_html__('Available Units','wpresidence-core').'</h2>';
                wpestate_shortcode_multi_units($post_id,$property_subunits_master,1);
            }else{
                if($property_subunits_master!=0){
                    wpestate_shortcode_multi_units($post_id,$property_subunits_master,1);
                }
            }
        print '</div>';
    }
   
 
    if( $print_show_agent == 'yes'){    
  

        print '<h2 class="print_header">'.esc_html__('Agent','wpresidence-core').'</h2>';
        if ( $preview_img!='' ){
            print '<div class="print-col-img agent_print_image"><img src="'.$preview_img.'"></div>';
        }
        print '<div class="print_agent_wrapper">';
            if ($name!='')
                print '<div class="listing_detail_agent col-md-4 agent_name"><strong>'.esc_html__('Name','wpresidence-core').':</strong> '.$name.'</div>';
            if($agent_phone!='')
                print '<div class="listing_detail_agent col-md-4"><strong>'.esc_html__('Telephone','wpresidence-core').':</strong> '.$agent_phone.'</div>';
            if($agent_mobile!='')
                print '<div class="listing_detail_agent col-md-4"><strong>'.esc_html__('Mobile','wpresidence-core').':</strong> '.$agent_mobile.'</div>';
            if($agent_skype!='')
                print '<div class="listing_detail_agent col-md-4"><strong>'.esc_html__('Skype','wpresidence-core').':</strong> '.$agent_skype.'</div>';
            if($agent_email!='')
                print '<div class="listing_detail_agent col-md-4"><strong>'.esc_html__('Email','wpresidence-core').':</strong> '.$agent_email.'</div>';
        print '</div>';
        print '</div>';
        print '<div class="printbreak"></div>';
    }
  
    if( $print_show_description == 'yes' ){ 
        print '<h2 class="print_header">'.esc_html__('Property Description','wpresidence-core').'</h2><div class="print-content">'.$content.wpestate_energy_save_features($post_id).'</div></div>';
    }
      
    if($print_show_adress  == 'yes' ){ 
        print '<h2 class="print_header">';
            if($property_adr_text!=''){
                echo esc_html($property_adr_text);
            } else{
                esc_html_e('Property Address','wpresidence-core');
            }        
        print '</h2>';

      
        print estate_listing_address_print($post_id); 
    }   
    
    if($print_show_details == 'yes' ){ 
        print '<h2 class="print_header">';
            if($property_adr_text!=''){
                echo esc_html($property_details_text);
            } else{
                esc_html_e('Property Details','wpresidence-core');
            }   
        print '</h2>';
        print estate_listing_details($post_id);
    }
    
    if($print_show_features == 'yes'){ 
        print '<h2 class="print_header">';
            if($property_adr_text!=''){
                echo esc_html($property_features_text);
            } else{
                esc_html_e('Features and Amenities','wpresidence-core');
            }
        print '</h2>';
        print estate_listing_features($post_id,3,1);
    }
    
    if($print_show_floor_plans  == 'yes' ){ 
        print '<h2 class="print_header">'.esc_html__('Floor Plans','wpresidence-core').'</h2>';   
        estate_floor_plan($post_id,1);
        print '<div class="printbreak"></div>';
    }
    
    if($print_show_images  == 'yes' ){ 
        print '<h2 class="print_header">'.esc_html__('Images','wpresidence-core').'</h2>';                   
        foreach ($post_attachments as $attachment) {
            $original       =   wp_get_attachment_image_src($attachment->ID, 'full');
             print '<div class="print-col-img printimg"><img src="'. $original[0].'"></div>';
        }
    }
  
    print '<div class="print_spacer"></div>';
    print '</body></html>';die();
  } 

endif;

          


////////////////////////////////////////////////////////////////////////////////
/// Add new profile fields
////////////////////////////////////////////////////////////////////////////////

add_filter('user_contactmethods', 'wpestate_modify_contact_methods');     
if( !function_exists('wpestate_modify_contact_methods') ):

function wpestate_modify_contact_methods($profile_fields) {

	// Add new fields
        $profile_fields['facebook']                     = 'Facebook';
        $profile_fields['twitter']                      = 'Twitter';
        $profile_fields['linkedin']                     = 'Linkedin';
        $profile_fields['pinterest']                    = 'Pinterest';
        $profile_fields['instagram']                    = 'Instagram';
        $profile_fields['website']                          = 'Website';
	$profile_fields['phone']                        = 'Phone';
        $profile_fields['mobile']                       = 'Mobile';
	$profile_fields['skype']                        = 'Skype';
	$profile_fields['title']                        = 'Title/Position';
        $profile_fields['custom_picture']               = 'Picture Url';
        $profile_fields['small_custom_picture']         = 'Small Picture Url';
        $profile_fields['package_id']                   = 'Package Id';
        $profile_fields['package_activation']           = 'Package Activation';
        $profile_fields['package_listings']             = 'Listings available';
        $profile_fields['package_featured_listings']    = 'Featured Listings available';
       // $profile_fields['pack_image_included']          = 'Package Images';
        $profile_fields['profile_id']                   = 'Paypal Recuring Profile';
        $profile_fields['user_agent_id']                = 'User Agent / Agency / Developer ID';
        $profile_fields['stripe']                       = 'Stripe Consumer Profile';
        $profile_fields['stripe_subscription_id']       = 'Stripe Subscription ID';
        $profile_fields['has_stripe_recurring']         = 'Has Stripe Recurring';
        $profile_fields['paypal_agreement']                   = esc_html__('Paypal Recuring Profile- rest api','wpresidence-core');
        $profile_fields['user_estate_role']             = 'User Role (1, 2 , 3 or 4): 1 = simple user, 2 = agent, 3 = agency, 4 = developer';
	return $profile_fields;
}

endif; // end   wpestate_modify_contact_methods 



////////////////////////////////////////////////////////////////////////////////
/// Google analytics
////////////////////////////////////////////////////////////////////////////////


function wpestate_core_add_to_footer(){
    $ga = esc_html(get_option('wp_estate_google_analytics_code', ''));
    if ($ga != '') { ?>

    <script>
        //<![CDATA[
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', '<?php echo esc_html($ga); ?>', '<?php     echo esc_html($_SERVER['SERVER_NAME']); ?>');
      ga('send', 'pageview');
    //]]>
    </script>


    <?php } 
}







