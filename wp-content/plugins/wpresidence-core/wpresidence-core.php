<?php
/*
 *  Plugin Name: Wpresidence -Theme Core Functionality
 *  Plugin URI:  https://themeforest.net/user/annapx
 *  Description: Adds functionality to WpResidence
 *  Version:     1.1
 *  Author:      wpestate
 *  Author URI:  https://wpestate.org
 *  License:     GPL2
 *  Text Domain: wpestate
 *  Domain Path: /languages
 * 
*/

define('WPESTATE_PLUGIN_URL',  plugins_url() );
define('WPESTATE_PLUGIN_DIR_URL',  plugin_dir_url(__FILE__) );
define('WPESTATE_PLUGIN_PATH',  plugin_dir_path(__FILE__) );
define('WPESTATE_PLUGIN_BASE',  plugin_basename(__FILE__) );

add_action( 'wp_enqueue_scripts', 'wpestate_residence_enqueue_styles' );
add_action( 'admin_enqueue_scripts', 'wpestate_residence_enqueue_styles_admin'); 
add_action( 'plugins_loaded', 'wpestate_residence_functionality_loaded' ); 
register_activation_hook( __FILE__, 'wpestate_residence_functionality' );
register_deactivation_hook( __FILE__, 'wpestate_residence_deactivate' );





function wpestate_residence_functionality_loaded(){
    $my_theme = wp_get_theme();
    $version = floatval( $my_theme->get( 'Version' ));

    if($version< 1.4 && $version!=1){
        deactivate_plugins( plugin_basename( __FILE__ ) );
        wp_die( 'This plugin requires  WpResidence 1.40 or higher.','wpresidence' ); 
    }

    
    
    load_plugin_textdomain( 'wpresidence-core', false, dirname( WPESTATE_PLUGIN_BASE ) . '/languages' );
    wpestate_shortcodes();
    add_action('widgets_init', 'register_wpestate_widgets' );
    add_action('wp_footer', 'wpestate_core_add_to_footer');
    
}

function wpestate_residence_functionality(){
}

function wpestate_residence_deactivate(){
}


function wpestate_residence_enqueue_styles() {
}


function wpestate_residence_enqueue_styles_admin(){
}


require_once(WPESTATE_PLUGIN_PATH . 'misc/metaboxes.php');
require_once(WPESTATE_PLUGIN_PATH . 'misc/plugin_help_functions.php');
require_once(WPESTATE_PLUGIN_PATH . 'misc/emailfunctions.php');
require_once(WPESTATE_PLUGIN_PATH . 'misc/3rd_party_code.php');

require_once(WPESTATE_PLUGIN_PATH . 'widgets.php');
require_once(WPESTATE_PLUGIN_PATH . 'shortcodes/shortcodes_install.php');
require_once(WPESTATE_PLUGIN_PATH . 'shortcodes/shortcodes.php');
require_once(WPESTATE_PLUGIN_PATH . 'shortcodes/property_page_shortcodes.php');

require_once(WPESTATE_PLUGIN_PATH . 'post-types/agents.php');
require_once(WPESTATE_PLUGIN_PATH . 'post-types/agency.php');
require_once(WPESTATE_PLUGIN_PATH . 'post-types/developers.php');
require_once(WPESTATE_PLUGIN_PATH . 'post-types/invoices.php');
require_once(WPESTATE_PLUGIN_PATH . 'post-types/searches.php');
require_once(WPESTATE_PLUGIN_PATH . 'post-types/membership.php');
require_once(WPESTATE_PLUGIN_PATH . 'post-types/property.php');
require_once(WPESTATE_PLUGIN_PATH . 'post-types/messages.php');

require_once WPESTATE_PLUGIN_PATH.'resources/src/Google_Client.php';
require_once WPESTATE_PLUGIN_PATH.'resources/src/contrib/Google_Oauth2Service.php';

$facebook_status    =   esc_html( get_option('wp_estate_facebook_login','') );
if($facebook_status=='yes'){
    require_once WPESTATE_PLUGIN_PATH.'resources/facebook_sdk5/Facebook/autoload.php';
}
        

$enable_stripe_status   =   esc_html ( get_option('wp_estate_enable_stripe','') );
 
if($enable_stripe_status==='yes'){
    require_once(WPESTATE_PLUGIN_PATH.'resources/stripe/lib/Stripe.php');
}

$walkscore_api= esc_html ( get_option('wp_estate_walkscore_api','') );
if($walkscore_api!=''){
    require_once(WPESTATE_PLUGIN_PATH.'resources/WalkScore.php');
}

$yelp_client_id         =   get_option('wp_estate_yelp_client_id','');
$yelp_client_secret     =   get_option('wp_estate_yelp_client_secret','');
$yelp_client_api_key_2018  =   get_option('wp_estate_yelp_client_api_key_2018','');
if($yelp_client_api_key_2018!=='' && $yelp_client_id!==''  ){
    require_once(WPESTATE_PLUGIN_PATH.'resources/yelp_fusion.php');
}
$yahoo_status       =   esc_html( get_option('wp_estate_yahoo_login','') );
if($yahoo_status=='yes'){
    require_once(WPESTATE_PLUGIN_PATH.'resources/openid.php');
}

function wpestate_return_imported_data(){
    return  @unserialize(base64_decode( trim($_POST['import_theme_options']) ) );
}

function wpestate_return_imported_data_encoded($return_exported_data){
    return base64_encode( serialize( $return_exported_data) );
}



add_action( 'plugins_loaded', 'wpestate_check_current_user' );
function wpestate_check_current_user() {
    $current_user = wp_get_current_user();
    if (!current_user_can('manage_options') ) { 
        show_admin_bar(false); 
    }
}