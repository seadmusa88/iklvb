<?php

///////////////////////////////////////////////////////////////////////////////////////////
/////// register shortcodes
///////////////////////////////////////////////////////////////////////////////////////////

function wpestate_shortcodes(){
    wpestate_register_shortcodes();
    wpestate_tiny_short_codes_register();
    add_filter('widget_text', 'do_shortcode');
}

///////////////////////////////////////////////////////////////////////////////////////////
// register tiny plugins functions
///////////////////////////////////////////////////////////////////////////////////////////

function wpestate_tiny_short_codes_register() {
    if (!current_user_can('edit_posts') && !current_user_can('edit_pages')) {
        return;
    }
    
    if (get_user_option('rich_editing') == 'true') {
        add_filter('mce_external_plugins', 'wpestate_add_plugin');
        add_filter('mce_buttons_3', 'wpestate_register_button');    
    }

}

/////////////////////////////////////////////////////////////////////////////////////////
/////// push the code into Tiny buttons array
///////////////////////////////////////////////////////////////////////////////////////////

function wpestate_register_button($buttons) {
    array_push($buttons, "|", "slider_recent_items");     
    array_push($buttons, "|", "testimonials");
    array_push($buttons, "|", "testimonial_slider");
    array_push($buttons, "|", "recent_items");  
    array_push($buttons, "|", "featured_agent"); 
    array_push($buttons, "|", "featured_article");
    array_push($buttons, "|", "featured_property");
    array_push($buttons, "|", "list_items_by_id"); 
    array_push($buttons, "|", "login_form"); 
    array_push($buttons, "|", "register_form");
    array_push($buttons, "|", "advanced_search");
    array_push($buttons, "|", "font_awesome");
    array_push($buttons, "|", "spacer"); 
    array_push($buttons, "|", "icon_container");
    array_push($buttons, "|", "list_agents");
    array_push($buttons, "|", "places_list");
    array_push($buttons, "|", "listings_per_agent");
    array_push($buttons, "|", "property_page_map");
    array_push($buttons, "|", "contact_us_form");  
    array_push($buttons, "|", "estate_membership_packages"); 

	//slick function	
    array_push($buttons, "|", "places_slider");  
    return $buttons;
}



///////////////////////////////////////////////////////////////////////////////////////////
/////// poins to the right js 
///////////////////////////////////////////////////////////////////////////////////////////

function wpestate_add_plugin($plugin_array) {   
    $plugin_array['slider_recent_items']        = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['testimonials']               = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['testimonial_slider']         = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['recent_items']               = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['featured_agent']             = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['featured_article']           = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['featured_property']          = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['login_form']                 = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['register_form']              = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['list_items_by_id']           = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['advanced_search']            = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['font_awesome']               = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['spacer']                     = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['icon_container']             = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['list_agents']                = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['places_list']                = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['listings_per_agent']         = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['property_page_map']          = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['estate_membership_packages'] = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
    $plugin_array['estate_featured_user_role']  = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
	
	// slick function
	$plugin_array['places_slider']  = WPESTATE_PLUGIN_DIR_URL . '/js/shortcodes.js';
	
    
    return $plugin_array;
}

///////////////////////////////////////////////////////////////////////////////////////////
/////// register shortcodes
///////////////////////////////////////////////////////////////////////////////////////////


function wpestate_register_shortcodes() {
    add_shortcode('contact_us_form', 'wpestate_contact_us_form');  
    add_shortcode('slider_recent_items', 'wpestate_slider_recent_posts_pictures');      
    add_shortcode('spacer', 'wpestate_spacer_shortcode_function');
    add_shortcode('recent-posts', 'wpestate_recent_posts_function');
    add_shortcode('testimonial', 'wpestate_testimonial_function');
    add_shortcode('testimonial_slider', 'wpestate_testimonial_slider_function');
    add_shortcode('recent_items', 'wpestate_recent_posts_pictures_new');
    add_shortcode('featured_agent', 'wpestate_featured_agent');
    add_shortcode('featured_article', 'wpestate_featured_article');
    add_shortcode('featured_property', 'wpestate_featured_property');
    add_shortcode('login_form', 'wpestate_login_form_function');
    add_shortcode('register_form', 'wpestate_register_form_function');
    add_shortcode('list_items_by_id', 'wpestate_list_items_by_id_function');
    add_shortcode('advanced_search', 'wpestate_advanced_search_function');
    add_shortcode('font_awesome', 'wpestate_font_awesome_function');
    add_shortcode('icon_container', 'wpestate_icon_container_function');
    add_shortcode('list_agents','wpestate_list_agents_function');
    add_shortcode('places_list', 'wpestate_places_list_function');
    add_shortcode('listings_per_agent', 'wplistingsperagent_shortcode_function' );
    add_shortcode('property_page_map', 'wpestate_property_page_map_function' );
    add_shortcode('test_sh', 'wpestate_test_sh' );
    
    add_shortcode('estate_property_page_tab', 'wpestate_property_page_design_tab' );
    add_shortcode('estate_property_page_acc', 'wpestate_property_page_design_acc' );
    add_shortcode('estate_property_simple_detail','wpestate_estate_property_simple_detail');
    add_shortcode('estate_property_details_section','wpestate_estate_property_details_section');
    add_shortcode('estate_property_slider_section','wpestate_estate_property_slider_section');
    add_shortcode('estate_property_design_agent','wpestate_estate_property_design_agent');
    add_shortcode('estate_property_design_agent_contact','wpestate_estate_property_design_agent_contact');
    add_shortcode('estate_property_design_related_listings','wpestate_estate_property_design_related_listings');
    add_shortcode('estate_property_design_intext_details','wpestate_estate_property_design_intext_details');
    add_shortcode('estate_property_design_gallery','wpestate_estate_property_design_gallery');
    add_shortcode('estate_property_design_agent_details_intext_details','wpestate_estate_property_design_agent_details_intext_details');
    add_shortcode('estate_membership_packages','wpestate_membership_packages_function');
    add_shortcode('estate_featured_user_role','wpestate_featured_user_role_shortcode');
	
	// slick new shortcode
	add_shortcode('places_slider','wpestate_places_slider');
}



////////////////////////////////////////////////////////////////////////////////
// add shortcodes to visual composer
////////////////////////////////////////////////////////////////////////////////


// slider
// agent
// contact
// similar listing

add_action( 'vc_before_init', 'wpestate_vc_shortcodes' );


if( function_exists('vc_map') ):
    


    
    if( !function_exists('wpestate_vc_shortcodes')):
        function wpestate_vc_shortcodes(){   
        
            $places                             =   array();    
            $all_places                         =   array();
            $city_array                         =   array();
            $area_array                         =   array();
            $category_array                     =   array();
            $action_array                       =   array();
            $global_categories                  =   array();
            $membership_packages                =   array();			
            $agency_developers_array            =   array();
            $agent_array                        =   array();
            $article_array                      =   array();
            // fixed tax arrays
            $property_city_values               =   array();
            $property_area_values               =   array();
            $property_county_state_values       =   array();
            $property_category_values           =   array();
            $property_action_category_values    =   array();
            // agent tax arrays
            
            
            $out_agent_tax_array=get_transient('wpestate_js_composer_out_agent_tax_array');
                    
            if($out_agent_tax_array === false ){
                $out_agent_tax_array = array();
                $agent_tax_list = array( 'property_category_agent', 'property_action_category_agent', 'property_city_agent', 'property_area_agent', 'property_county_state_agent' );
                foreach( $agent_tax_list as $single_agent_tax ){
                        // getting agent tax data
                        $terms_city = get_terms( array(
                                'taxonomy' => $single_agent_tax,
                                'hide_empty' => false,
                        ) );

                        foreach($terms_city as $term){					 
                                $temp_array=array();
                                $temp_array['label'] = $term->name;
                                $temp_array['value'] = $term->term_id;
                                $out_agent_tax_array[$single_agent_tax][] = $temp_array;
                        }

                        // getting agent tax data END
                }
                set_transient('wpestate_js_composer_out_agent_tax_array',$out_agent_tax_array,60*60*4);
            }
            // agent tax arrays END

			
			
			
			

            // gettings agent packages
            $agent_array = get_transient('wpestate_js_composer_agent_array');
            
            if($agent_array ===false ){
                $args_inner = array(
                    'post_type' => array( 'estate_agent',
                                        'estate_agency',
                                        'estate_developer'),
                    'showposts' => -1
                );
                $all_agent_packages = get_posts( $args_inner );
                if( count($all_agent_packages) > 0 ){
                        foreach( $all_agent_packages as $single_package ){
                                $temp_array=array();
                                $temp_array['label'] = $single_package->post_title;
                                $temp_array['value'] = $single_package->ID;

                                $agent_array[] = $temp_array;
                        }
                }
                set_transient('wpestate_js_composer_agent_array',$agent_array,60*60*4);
            }
            
            
            // gettings agent packages END
			
			
            // gettings article packages
            
            $article_array = get_transient ('wpestate_js_composer_article_array');
            if($article_array === false){
                $args_inner = array(
                        'post_type' => array( 'post'),
                        'showposts' => -1
                );
                $all_article_packages = get_posts( $args_inner );
                if( count($all_article_packages) > 0 ){
                        foreach( $all_article_packages as $single_package ){
                                $temp_array=array();
                                $temp_array['label'] = $single_package->post_title;
                                $temp_array['value'] = $single_package->ID;

                                $article_array[] = $temp_array;
                        }
                }
                set_transient('wpestate_js_composer_article_array',$article_array,60*60*4);
            }
            // gettings article packages END
			
			
            
            

            // gettings agency/developers packages
            $agency_developers_array = get_transient ('wpestate_js_composer_article_agency_developers_array');
            
            if($agency_developers_array===false){
                $args_inner = array(
                        'post_type' => array( 'estate_agency', 'estate_developer' ),
                        'showposts' => -1
                );
                $all_agency_dev_packages = get_posts( $args_inner );
                if( count($all_agency_dev_packages) > 0 ){
                        foreach( $all_agency_dev_packages as $single_package ){
                                $temp_array=array();
                                $temp_array['label'] = $single_package->post_title;
                                $temp_array['value'] = $single_package->ID;

                                $agency_developers_array[] = $temp_array;
                        }
                }
                set_transient ('wpestate_js_composer_article_agency_developers_array',$agency_developers_array,60*60*4);
            }
            // gettings agency/developers packages END


            
            
            // gettings membership packages
            $membership_packages = get_transient ('wpestate_js_composer_membership_packages');
            if($membership_packages === false){
                $args_inner = array(
                        'post_type' => 'membership_package',
                        'showposts' => -1
                );
                $all_memb_packages = get_posts( $args_inner );
                if( count($all_memb_packages) > 0 ){
                        foreach( $all_memb_packages as $single_package ){
                                $temp_array=array();
                                $temp_array['label'] = $single_package->post_title;
                                $temp_array['value'] = $single_package->ID;

                                $membership_packages[] = $temp_array;
                        }
                }
                set_transient ('wpestate_js_composer_membership_packages',$membership_packages,60*60*4);
            }
            // gettings membership packages END
		
            
            
            $property_city_values = get_transient('wpestate_js_composer_property_city_values');
		
            if($property_city_values===false){
                $terms_city = get_terms( array(
                    'taxonomy' => 'property_city',
                    'hide_empty' => false,
                ) );
                $all_tax=array();
                foreach($terms_city as $term){
                    $places[$term->name]= $term->term_id;
                    $temp_array=array();
                    $temp_array['label'] = $term->name;
                    $temp_array['value'] = $term->term_id;
                    $all_tax[]=$temp_array;

                    // tax based_array
                    $property_city_values[] = $temp_array;
                }
                set_transient('wpestate_js_composer_property_city_values',$property_city_values,60*60*4);
            }
            
            
            
            
            
            $property_area_values = get_transient('wpestate_js_composer_property_area_values');
            if($property_area_values===false){
                $terms_city = get_terms( array(
                    'taxonomy' => 'property_area',
                    'hide_empty' => false,
                ) );

                foreach($terms_city as $term){
                    $places[$term->name]= $term->term_id;
                    $temp_array=array();
                    $temp_array['label'] = $term->name;
                    $temp_array['value'] = $term->term_id;
                    $all_places[]=$temp_array;
                    $area_array[]=$temp_array;
                    $all_tax[]=$temp_array;

                    // tax based_array
                    $property_area_values[] = $temp_array;

                }
                set_transient('wpestate_js_composer_property_area_values',$property_area_values,60*60*4);
            }
            
            
            
            
            $property_county_state_values = get_transient('wpestate_js_composer_property_county_state_values');
            
            if($property_county_state_values===false){
                $terms_city = get_terms( array(
                    'taxonomy' => 'property_county_state',
                    'hide_empty' => false,
                ) );

                foreach($terms_city as $term){
                    $places[$term->name]= $term->term_id;
                    $temp_array=array();
                    $temp_array['label'] = $term->name;
                    $temp_array['value'] = $term->term_id;
                    $all_places[]=$temp_array;
                    $area_array[]=$temp_array;
                    $all_tax[]=$temp_array;

                    // tax based_array
                    $property_county_state_values[] = $temp_array;

                }
                set_transient('wpestate_js_composer_property_county_state_values',$property_county_state_values,60*60*4);
            }
            

            
            
            $property_category_values = get_transient('wpestate_js_composer_property_category');
            if($property_category_values===false){
                $terms_category = get_terms( array(
                    'taxonomy' => 'property_category',
                    'hide_empty' => false,
                ) );

                foreach($terms_category as $term){

                    $temp_array=array();
                    $temp_array['label'] = $term->name;
                    $temp_array['value'] = $term->term_id;

                    $category_array[]=$temp_array;
                    $global_categories[]=$temp_array;
                    $all_tax[]=$temp_array;

                    // tax based_array
                    $property_category_values[] = $temp_array;
                }
                set_transient('wpestate_js_composer_property_category',$property_category_values,60*60*4);
            }
            
            
            
            $property_action_category_values = get_transient('wpestate_js_composer_property_action');
            if($property_action_category_values===false){
                $terms_category = get_terms( array(
                    'taxonomy' => 'property_action_category',
                    'hide_empty' => false,
                ) );

                foreach($terms_category as $term){

                    $temp_array=array();
                    $temp_array['label'] = $term->name;
                    $temp_array['value'] = $term->term_id;
                    $all_tax[]=$temp_array;
                    $action_array[]=$temp_array;

                    // tax based_array
                    $property_action_category_values[] = $temp_array;

                }
                set_transient('wpestate_js_composer_property_action',$property_action_category_values,60*60*4);
            }
    
            
            
            $all_tax1 =    get_transient('wpestate_js_composer_all_tax');
            if($all_tax1===false){
                set_transient('wpestate_js_composer_all_tax',$all_tax,60*60*4);
            }
            $all_tax=$all_tax1;
            
            
            $agent_single_details = array(
                'Name'          =>  'name',
                'Main Image'    =>  'image',
                'Description'   =>  'description',
                'Page Link'     =>  'page_link',
                'Agent Skype'   =>  'agent_skype',
                'Agent Phone'   =>  'agent_phone',
                'Agent Mobile'  =>  'agent_mobile',
                'Agent email'   =>  'agent_email',
                'Agent position'                =>  'agent_position',
                'Agent Facebook'                =>  'agent_facebook',
                'Agent Twitter'                 =>  'agent_twitter',
                'Agent Linkedin'                => 'agent_linkedin',
                'Agent Pinterest'               => 'agent_pinterest',
                'Agent Instagram'               => 'agent_instagram',
                'Agent website'                 => 'agent_website',
                'Agent category'                => 'property_category_agent', 
                'Agent action category'         => 'property_action_category_agent', 
                'Agent city category'           => 'property_city_agent',
                'Agent Area category'           => 'property_area_agent',
                'Agent County/State category'   => 'property_county_state_agent'
            );
            $agent_explanations='';
            foreach($agent_single_details as $key=>$value){
                $agent_explanations.=' for '.$key.' use this string: {'.$value.'}</br>';       
            }       


            vc_map(
                array(
                    "name" => esc_html__("Property Page Only - Text with Agent details ","wpresidence"),//done
                    "base" => "estate_property_design_agent_details_intext_details",
                    "class" => "",
                    "category" => esc_html__('WpResidence - Property Page Design','wpresidence-core'),
                    'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                    'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                    'weight'=>99,
                    'icon'   =>'wpestate_vc_logo2',
                    'description'=>'',

                    "params" => array(
                        array(
                            "type"          =>  "textarea_html",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Text","wpresidence"),
                            "param_name"    =>  "content",
                            "value"         =>  '',
                            "description"   =>  $agent_explanations
                        ),
                          array(
                            'type' => 'css_editor',
                            'heading' => esc_html__( 'Css', 'wpresidence-core' ),
                            'param_name' => 'css',
                            'group' => esc_html__( 'Design options', 'wpresidence-core' ),
                        ),
                    ) 

                   )

                );





            $slider_details=array('horizontal','vertical');
            vc_map(
            array(
                "name" => esc_html__("Property Page Only - Property Gallery","wpresidence"),//done
                "base" => "estate_property_design_gallery",
                "class" => "",
                "category" => esc_html__('WpResidence - Property Page Design','wpresidence-core'),
                'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                'weight'=>99,
                'icon'   =>'wpestate_vc_logo2',
                'description'=>'',


                "params" => array(
                    array(
                        "type"          =>  "textfield",
                        "holder"        =>  "div",
                        "class"         =>  "",
                        "heading"       =>  esc_html__("Thumbnail max width in px","wpresidence"),
                        "param_name"    =>  "maxwidth",
                        "value"         =>  '200',
                        "description"   =>  esc_html__("Thumbnail max width in px (*height is auto calculated based on image ratio)","wpresidence")
                    ),
                    array(
                        "type"          =>  "textfield",
                        "holder"        =>  "div",
                        "class"         =>  "",
                        "heading"       =>  esc_html__("Thumbnail right & bottom margin in px","wpresidence"),
                        "param_name"    =>  "margin",
                        "value"         =>  '10',
                        "description"   =>  esc_html__("Thumbnail right & bottom margin in px","wpresidence")
                    ),
                    array(
                        "type"          =>  "textfield",
                        "holder"        =>  "div",
                        "class"         =>  "",
                        "heading"       =>  esc_html__("Maximum no of thumbs","wpresidence"),
                        "param_name"    =>  "image_no",
                        "value"         =>  '4',
                        "description"   =>  esc_html__("Maximum no of thumbs","wpresidence")
                    ),
                    array(
                        'type' => 'css_editor',
                        'heading' => esc_html__( 'Css', 'wpresidence-core' ),
                        'param_name' => 'css',
                        'group' => esc_html__( 'Design options', 'wpresidence-core' ),
                    ),
                ) 

               )

            );


            $slider_details=array('horizontal','vertical');
            vc_map(
            array(
                "name" => esc_html__("Property Page Only - Related Listings","wpresidence"),//done
                "base" => "estate_property_design_related_listings",
                "class" => "",
                "category" => esc_html__('WpResidence - Property Page Design','wpresidence-core'),
                'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                'weight'=>99,
                'icon'   =>'wpestate_vc_logo2',
                'description'=>'',

                "params" => ""


               )

            );







            $slider_details=array('horizontal','vertical');
            vc_map(
            array(
                "name" => esc_html__("Property Page Only - Contact Form Agent","wpresidence"),//done
                "base" => "estate_property_design_agent_contact",
                "class" => "",
                "category" => esc_html__('WpResidence - Property Page Design','wpresidence-core'),
                'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                'weight'=>99,
                'icon'   =>'wpestate_vc_logo2',
                'description'=>'',

                "params" => ""


               )

            );





            $slider_details=array('horizontal','vertical');

            $agent_card=array('one column','two columns');
            vc_map(
            array(
                "name" => esc_html__("Property Page Only - Agent Card","wpresidence"),//done
                "base" => "estate_property_design_agent",
                "class" => "",
                "category" => esc_html__('WpResidence - Property Page Design','wpresidence-core'),
                'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                'weight'=>99,
                'icon'   =>'wpestate_vc_logo2',
                'description'=>'',
                'params' => array(
                    array(
                        "type"          =>  "dropdown",
                        "holder"        =>  "div",
                        "class"         =>  "",
                        "heading"       =>  esc_html__("Colums : 1 or 2","wpresidence"),
                        "param_name"    =>  "columns",
                        "value"         =>  $agent_card,
                        "description"   =>  esc_html__("One column means that agent details go below the image, two columns means agent details are on the right side of the image.","wpresidence"),
                    ),
                    array(
                        'type' => 'css_editor',
                        'heading' => esc_html__( 'Css', 'wpresidence-core' ),
                        'param_name' => 'css',
                        'group' => esc_html__( 'Design options', 'wpresidence-core' ),
                    ),
                )




               )

            );











            $slider_details=array('horizontal','vertical');
            $yesno=array('no','yes');
            vc_map(
            array(
                "name" => esc_html__("Property Page Only - Property Image & Video Slider","wpresidence"),//done
                "base" => "estate_property_slider_section",
                "class" => "",
                "category" => esc_html__('WpResidence - Property Page Design','wpresidence-core'),
                'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                'weight'=>99,
                'icon'   =>'wpestate_vc_logo2',
                'description'=>'',

                "params" => array(
                    array(
                        "type"          =>  "dropdown",
                        "holder"        =>  "div",
                        "class"         =>  "",
                        "heading"       =>  esc_html__("Slider Type","wpresidence"),
                        "param_name"    =>  "detail",
                        "value"         =>  $slider_details,
                        "description"   =>  esc_html__("Slider Type","wpresidence")
                    ),
                     array(
                        "type"          =>  "dropdown",
                        "holder"        =>  "div",
                        "class"         =>  "",
                        "heading"       =>  esc_html__("Show map in slider type?","wpresidence"),
                        "param_name"    =>  "showmap",
                        "value"         =>  $yesno,
                        "description"   =>  esc_html__("Make sure you are using only 1 map per page in order to avoid conflicts","wpresidence")
                    ),
                    array(
                        'type' => 'css_editor',
                        'heading' => esc_html__( 'Css', 'wpresidence-core' ),
                        'param_name' => 'css',
                        'group' => esc_html__( 'Design options', 'wpresidence-core' ),
                    ),
                ) 

               )

            );









            $details_list=array(
                'none',
                'Description',
                'Property Address',
                'Property Details',
                'Amenities and Features',
                'Map',
                'Virtual Tour',
                'Walkscore',
                'Floor Plans',
                'Page Views',
                'What\'s Nearby',
                'Subunits',
            );

            vc_map(
                array(
                    "name" => esc_html__("Property Page Only - Details Section","wpresidence"),//done
                    "base" => "estate_property_details_section",
                    "class" => "",
                    "category" => esc_html__('WpResidence - Property Page Design','wpresidence-core'),
                    'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                    'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                    'weight'=>99,
                    'icon'   =>'wpestate_vc_logo2',
                    'description'=>'',

                    "params" => array(
                        array(
                            "type"          =>  "dropdown",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Select the property section","wpresidence"),
                            "param_name"    =>  "detail",
                            "value"         =>  $details_list,
                            "description"   =>  esc_html__("Select a property section from the theme default details elements.","wpresidence")
                        ),
                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Columns","wpresidence"),
                            "param_name"    =>  "columns",
                            "value"         =>  '3',
                            "description"   =>  esc_html__("Columns (*works only for address, property details and features and amenities)","wpresidence"),
                        ),
                        array(
                            'type' => 'css_editor',
                            'heading' => esc_html__( 'Css', 'wpresidence-core' ),
                            'param_name' => 'css',
                            'group' => esc_html__( 'Design options', 'wpresidence-core' ),
                        ),




                    ) 

                   )

                );







            $single_details = array(
                'none'          =>  'none',
                'Title'         =>  'title',
                'Description'   =>  'description',
                'Categories'    =>  'property_category',
                'Action'        =>  'property_action_category',
                'City'          =>  'property_city',
                'Neighborhood'  =>  'property_area',
                'County / State'=>  'property_county_state',
                'Address'       =>  'property_address',
                'Zip'           =>  'property_zip',
                'Country'       =>  'property_country',
                'Status'        =>  'property_status',
                'Price'         =>  'property_price',
                'Price Label'   =>  'property_label',
                'Price Label before'=>  'property_label_before',
                'Size'              =>  'property_size',
                'Lot Size'          =>  'property_lot_size',
                'Rooms'             =>  'property_rooms',
                'Bedrooms'          =>  'property_bedrooms',
                'Bathrooms'         =>  'property_bathrooms',
                'Download Pdf'      =>  'property_pdf',
                'Agent'             =>  'property_agent',

            );

            $custom_fields = get_option( 'wp_estate_custom_fields', true);    
            if( !empty($custom_fields)){  
                $i=0;
                while($i< count($custom_fields) ){     
                    $name =   $custom_fields[$i][0]; 
                    $slug         =     wpestate_limit45(sanitize_title( $name )); 
                    $slug         =     sanitize_key($slug); 
                    $single_details[str_replace('-',' ',$name)]=     $slug;
                    $i++;
               }
            }

            $feature_list       =   stripslashes( esc_html( get_option('wp_estate_feature_list') ) );
            $feature_list_array =   explode( ',',$feature_list);

            foreach($feature_list_array as $key => $value){
                $value                  =   stripslashes($value);
                $post_var_name          =   str_replace(' ','_', trim($value) );
                $input_name             =   wpestate_limit45(sanitize_title( $post_var_name ));
                $input_name             =   sanitize_key($input_name);
                $single_details[$value] =   $input_name;
            }


            vc_map(
                array(
                    "name" => esc_html__("Property Page Only - Single Detail","wpresidence"),//done
                    "base" => "estate_property_simple_detail",
                    "class" => "",
                    "category" => esc_html__('WpResidence - Property Page Design','wpresidence-core'),
                    'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                    'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                    'weight'=>99,
                    'icon'   =>'wpestate_vc_logo2',
                    'description'=>'',

                    "params" => array(
                        array(
                            "type"          =>  "dropdown",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Select single detail","wpresidence"),
                            "param_name"    =>  "detail",
                            "value"         =>  $single_details,
                            "description"   =>  esc_html__("Select one single detail from dropdown","wpresidence")
                        ),
                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Element Label","wpresidence"),
                            "param_name"    =>  "label",
                            "value"         =>  'Label:',
                            "description"   =>  esc_html__("Element Label","wpresidence"),
                        ),




                    ) 

                   )

                );


            $single_details['Favorite action']   =  'favorite_action';
            $single_details['Page_views']        =  'page_views';
            $single_details['Print Action']      =  'print_action';
            $single_details['Facebook share']    =  'facebook_share';
            $single_details['Twiter share']      =  'twiter_share';
            $single_details['Google+ share']     =  'google_share';
            $single_details['Pinterest share']   =  'pinterest_share';


            $explanations=' for Wordpress property id use this string: {prop_id}</br>';
            $explanations.=' for Property url use this string: {prop_url}</br>';
            unset($single_details['none']);
            foreach($single_details as $key=>$value){
                $explanations.=' for '.$key.' use this string: {'.$value.'}</br>';

            }       

             vc_map(
                array(
                    "name" => esc_html__("Property Page Only - Text with Details ","wpresidence"),//done
                    "base" => "estate_property_design_intext_details",
                    "class" => "",
                    "category" => esc_html__('WpResidence - Property Page Design','wpresidence-core'),
                    'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                    'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                    'weight'=>99,
                    'icon'   =>'wpestate_vc_logo2',
                    'description'=>'',

                    "params" => array(
                        array(
                            "type"          =>  "textarea_html",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Text","wpresidence"),
                            "param_name"    =>  "content",
                            "value"         =>  '',
                            "description"   =>  $explanations
                        ),
                        array(
                            'type' => 'css_editor',
                            'heading' => esc_html__( 'Css', 'wpresidence-core' ),
                            'param_name' => 'css',
                            'group' => esc_html__( 'Design options', 'wpresidence-core' ),
                        ),
                    ) 

                   )

                );



             vc_map(
                array(
                    "name" => esc_html__("Property Page Only - Details as Accordion","wpresidence"),//done
                    "base" => "estate_property_page_acc",
                    "class" => "",
                    "category" => esc_html__('WpResidence - Property Page Design','wpresidence-core'),
                    'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                    'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                    'weight'=>99,
                    'icon'   =>'wpestate_vc_logo2',
                    'description'=>'',

                    "params" => array(
                          array(
                            "type"          =>  "dropdown",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Accordion Open/Close Status","wpresidence"),
                            "param_name"    =>  "style",
                            "value"         =>  array(1=>esc_html__("all open","wpresidence"),2=>esc_html__("all closed","wpresidence"),3=>esc_html__("only the first one open","wpresidence"),),
                            "description"   =>  esc_html__("Accordion Open/Close status","wpresidence")
                        ),


                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Description","wpresidence"),
                            "param_name"    =>  "description",
                            "value"         =>  esc_html__("Description","wpresidence"),
                            "description"   =>  esc_html__("Description Label in the tab. Set it blank if you don't want it to appear.","wpresidence")
                        ),

                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Property Address","wpresidence"),
                            "param_name"    =>  "property_address",
                            "value"         =>   esc_html__("Property Address","wpresidence"),
                            "description"   =>  esc_html__("Property Address Label in the tab. Set it blank if you don't want it to appear.","wpresidence")
                        ),

                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Property Details","wpresidence"),
                            "param_name"    =>  "property_details",
                            "value"         =>   esc_html__("Property Details","wpresidence"),
                            "description"   =>  esc_html__("property_details Label in the tab. Set it blank if you don't want it to appear.","wpresidence")
                        ),

                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Amenities and Features","wpresidence"),
                            "param_name"    =>  "amenities_features",
                            "value"         =>  esc_html__("Amenities and Features","wpresidence"),
                            "description"   =>  esc_html__("Amenities and Features Label in the tab. Set it blank if you don't want to appear.","wpresidence")
                        ),

                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Map","wpresidence"),
                            "param_name"    =>  "map",
                            "value"         =>  esc_html__("Map","wpresidence"),
                            "description"   =>  esc_html__("Map label in the tab.  Set it blank if you don't want it to appear. Remember to have only one map per property page to avoid conflicts.","wpresidence")
                        ),
                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Walkscore","wpresidence"),
                            "param_name"    =>  "walkscore",
                            "value"         =>  esc_html__("Walkscore","wpresidence"),
                            "description"   =>  esc_html__("Walkscore Label in the tab. Set it blank if you don't want it to appear.","wpresidence")
                        ),
                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Floor Plans","wpresidence"),
                            "param_name"    =>  "floor_plans",
                            "value"         => esc_html__("Floor Plans","wpresidence"),
                            "description"   =>  esc_html__("Floor Plans Label in the tab. Set it blank if you don't want to appear.","wpresidence")
                        ),
                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Page Views","wpresidence"),
                            "param_name"    =>  "page_views",
                            "value"         =>  esc_html__("Page Views","wpresidence"),
                            "description"   =>  esc_html__("Page Views in the tab. Set it blank if you don't want it to appear.","wpresidence")
                        ),

                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Virtual Tour","wpresidence"),
                            "param_name"    =>  "virtual_tour",
                            "value"         =>  esc_html__("Virtual Tour","wpresidence"),
                            "description"   =>  esc_html__("Virtual Tour in the tab. Set it blank if you don't want it to appear.","wpresidence")
                        ),

                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Yelp Details","wpresidence"),
                            "param_name"    =>  "yelp_details",
                            "value"         =>  esc_html__("Yelp Views","wpresidence"),
                            "description"   =>  esc_html__("Yelp Views in the tab. Set it blank if you don't want it to appear.","wpresidence")
                        ),

                        array(
                            'type' => 'css_editor',
                            'heading' => esc_html__( 'Css', 'wpresidence-core' ),
                            'param_name' => 'css',
                            'group' => esc_html__( 'Design options', 'wpresidence-core' ),
                        ),
                    ) 

                   )

                );




            vc_map(
                array(
                    "name" => esc_html__("Property Page Only - Details as Tabs","wpresidence"),//done
                    "base" => "estate_property_page_tab",
                    "class" => "",
                    "category" => esc_html__('WpResidence - Property Page Design','wpresidence-core'),
                    'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                    'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                    'weight'=>99,
                    'icon'   =>'wpestate_vc_logo2',
                    'description'=>'',

                    "params" => array(


                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Description","wpresidence"),
                            "param_name"    =>  "description",
                            "value"         =>  esc_html__("Description","wpresidence"),
                            "description"   =>  esc_html__("Description Label in the tab. Set it blank if you don't want it to appear.","wpresidence")
                        ),

                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Property Address","wpresidence"),
                            "param_name"    =>  "property_address",
                            "value"         =>   esc_html__("Property Address","wpresidence"),
                            "description"   =>  esc_html__("Property Address Label in the tab. Set it blank if you don't want it to appear.","wpresidence")
                        ),

                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Property Details","wpresidence"),
                            "param_name"    =>  "property_details",
                            "value"         =>   esc_html__("Property Details","wpresidence"),
                            "description"   =>  esc_html__("Property Details Label in the tab. Set it blank if you don't want it to appear.","wpresidence")
                        ),

                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Amenities and Features","wpresidence"),
                            "param_name"    =>  "amenities_features",
                            "value"         =>  esc_html__("Amenities and Features","wpresidence"),
                            "description"   =>  esc_html__("Amenities and Features Label in the tab. Set it blank if you don't want it to appear.","wpresidence")
                        ),

                         array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Map","wpresidence"),
                            "param_name"    =>  "map",
                            "value"         =>  esc_html__("Map","wpresidence"),
                            "description"   =>  esc_html__("Map label in the tab.  Set it blank if you don't want it to appear. Remember to have only one map per property page to avoid conflicts.","wpresidence")
                        ),
                          array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Walkscore","wpresidence"),
                            "param_name"    =>  "walkscore",
                            "value"         =>  esc_html__("Walkscore","wpresidence"),
                            "description"   =>  esc_html__("Walkscore Label in the tab. Set it blank if you don't want it to appear.","wpresidence")
                        ),
                          array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Floor Plans","wpresidence"),
                            "param_name"    =>  "floor_plans",
                            "value"         => esc_html__("Floor Plans","wpresidence"),
                            "description"   =>  esc_html__("Floor Plans Label in the tab. Set it blank if you don't want it to appear.","wpresidence")
                        ),
                          array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Page Views","wpresidence"),
                            "param_name"    =>  "page_views",
                            "value"         =>  esc_html__("Page Views","wpresidence"),
                            "description"   =>  esc_html__("Page Views in the tab. Set it blank if you don't want it to appear.","wpresidence")
                        ),
                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Virtual Tour","wpresidence"),
                            "param_name"    =>  "virtual_tour",
                            "value"         =>  esc_html__("Virtual Tour","wpresidence"),
                            "description"   =>  esc_html__("Virtual Tour in the tab. Set it blank if you don't want it to appear.","wpresidence")
                        ),

                        array(
                            "type"          =>  "textfield",
                            "holder"        =>  "div",
                            "class"         =>  "",
                            "heading"       =>  esc_html__("Yelp Details","wpresidence"),
                            "param_name"    =>  "yelp_details",
                            "value"         =>  esc_html__("Yelp Views","wpresidence"),
                            "description"   =>  esc_html__("Yelp Views in the tab. Set it blank if you don't want it to appear.","wpresidence")
                        ),
                    ) 

                   )

                );






            vc_map(
            array(
                "name" => esc_html__("Google Map with Property Marker","wpresidence"),//done
                "base" => "property_page_map",
                "class" => "",
                "category" => esc_html__('Content','wpresidence-core'),
                'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                'weight'=>100,
                'icon'   =>'wpestate_vc_logo',
                'description'=>esc_html__('Google Map with Property Marker','wpresidence-core'),

                "params" => array(

                    array(
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__("Property ID","wpresidence"),
                        "param_name" => "propertyid",
                        "value" => "",
                        "description" => esc_html__("Add the ID of the property you want to show","wpresidence")
                    ),




                ) 

               )

            );

        vc_map(
            array(
                "name" => esc_html__("Listings per Agent, Agency or Developer","wpresidence"),//done
                "base" => "listings_per_agent",
                "class" => "",
                "category" => esc_html__('Content','wpresidence-core'),
                'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                'weight'=>100,
                'icon'   =>'wpestate_vc_logo',
                'description'=>esc_html__('Listings per Agent, Agency or Developer','wpresidence-core'),

                "params" => array(

                   array(
                        "type" => "autocomplete",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__("Type Agent, Agency or Developert name","wpresidence"),
                        "param_name" => "agentid",
                        "value" => "",
                        "description" => esc_html__("Select one Agent, Agency or Developer","wpresidence"),
                        'settings' => array(
                                        'multiple' => false,
                                        'sortable' => true,
                                        'min_length' => 1,
                                        'no_hide' => true, 
                                        'groups' => false, 
                                        'unique_values' => true, 
                                        'display_inline' => true, 
                                        'values' => $agent_array,

                                )  ,
                    ),



                    array(
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__("Number of Listings","wpresidence"),
                        "param_name" => "nooflisting",
                        "value" => "",
                         "description" => esc_html__("Number of Listings to display","wpresidence")

                    ),

                ) 

               )
             );




             $list_cities_or_areas=array(1,2);
             vc_map( array(
                "name" => esc_html__( "Display Categories","wpresidence"),//done
                "base" => "places_list",
                "class" => "",
                "category" => esc_html__( 'Content','wpresidence-core'),
                'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                'weight'=>100,
                'icon'   =>'wpestate_vc_logo',
                'description'=>esc_html__( 'Display Categories','wpresidence-core'),  
                "params" => array(
                    array(
                        "type" => "autocomplete",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__( "Type Categories,Actions,Cities,Areas,Neighborhoods or County name you want to show","wpresidence"),
                        "param_name" => "place_list",
                        "value" => "",
                        "description" => esc_html__( "Type Categories,Actions,Cities,Areas,Neighborhoods or County name you want to show","wpresidence"),
                        'settings' => array(
                            'multiple' => true,
                            'sortable' => true,
                            'min_length' => 1,
                            'no_hide' => true, // In UI after select doesn't hide an select list
                            'groups' => false, // In UI show results grouped by groups
                            'unique_values' => true, // In UI show results except selected. NB! You should manually check values in backend
                            'display_inline' => true, // In UI show results inline view
                            'values' => $all_tax,
                      
                        )  ,
                    )  ,
                    array(
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__( "Items per row","wpresidence"),
                        "param_name" => "place_per_row",
                        "value" => "4",
                        "description" => esc_html__( "How many items listed per row? For type 2 use only 1.","wpresidence")
                    ),
                    array(
                        "type" => "dropdown",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__( "Type","wpresidence"),
                        "param_name" => "place_type",
                        "value" => $list_cities_or_areas,
                        "description" => esc_html__( "Select Item Type 1/2?","wpresidence")
                    ),

                    array(
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__( "Extra Class Name","wpresidence"),
                        "param_name" => "extra_class_name",
                        "value" => "",
                        "description" => esc_html__( "Extra Class Name","wpresidence")
                    )
                )    
            ) 
            );    
		
            $property_category_agent=''; 
            if(isset($out_agent_tax_array['property_category_agent'])){
                $property_category_agent= $out_agent_tax_array['property_category_agent'];
            }
            
            $property_action_category_agent='';	
            if( isset($out_agent_tax_array['property_action_category_agent'])){
                $property_action_category_agent=$out_agent_tax_array['property_action_category_agent'];
            }
            
            $property_city_agent='';
            if(isset( $out_agent_tax_array['property_city_agent'])){
                $property_city_agent= $out_agent_tax_array['property_city_agent'];
            }
            
            $property_area_agent='';
            if( isset( $out_agent_tax_array['property_area_agent'])){
               $property_area_agent= $out_agent_tax_array['property_area_agent'];
            }
            
            
            $random_pick=array('no','yes');
            vc_map(
            array(
               "name" => esc_html__("List Agents","wpresidence"),//done
               "base" => "list_agents",
               "class" => "",
               "category" => esc_html__('Content','wpresidence-core'),
               'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
               'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
               'weight'=>100,
               'icon'   =>'wpestate_vc_logo',
               'description'=>esc_html__('Agent Lists','wpresidence-core'),
               "params" => array(
                   array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Title","wpresidence"),
                     "param_name" => "title",
                     "value" => "",
                     "description" => esc_html__("Section Title","wpresidence")
                  ),

                   array(
                     "type" => "autocomplete",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Type Category names","wpresidence"),
                     "param_name" => "category_ids",
                     "value" => "",
                     "description" => esc_html__("list of agent category names","wpresidence"),
					 'settings' => array(
						'multiple' => true,
						'sortable' => true,
						'min_length' => 1,
						'no_hide' => true, 
						'groups' => false, 
						'unique_values' => true, 
						'display_inline' => true, 
						'values' => $property_category_agent,
						  
					),
                  ),
                     array(
                     "type" => "autocomplete",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Type Action names","wpresidence"),
                     "param_name" => "action_ids",
                     "value" => "",
                     "description" => esc_html__("list of agent action names","wpresidence"),
					 'settings' => array(
						'multiple' => true,
						'sortable' => true,
						'min_length' => 1,
						'no_hide' => true, 
						'groups' => false, 
						'unique_values' => true, 
						'display_inline' => true, 
						'values' => $property_action_category_agent,
						  
					),
                  ), 
                   array(
                     "type" => "autocomplete",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Type City names","wpresidence"),
                     "param_name" => "city_ids",
                     "value" => "",
                     "description" => esc_html__("list of agent city names","wpresidence"),
					 'settings' => array(
						'multiple' => true,
						'sortable' => true,
						'min_length' => 1,
						'no_hide' => true, 
						'groups' => false, 
						'unique_values' => true, 
						'display_inline' => true, 
						'values' => $property_city_agent,
						  
					),
                  ),
                    array(
                     "type" => "autocomplete",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Type Area names","wpresidence"),
                     "param_name" => "area_ids",
                     "value" => "",
                     "description" => esc_html__("list of agent area names","wpresidence"),
					 'settings' => array(
						'multiple' => true,
						'sortable' => true,
						'min_length' => 1,
						'no_hide' => true, 
						'groups' => false, 
						'unique_values' => true, 
						'display_inline' => true, 
						'values' => $property_area_agent,
						  
					),
                  ),
                   array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("No of items","wpresidence"),
                     "param_name" => "number",
                     "value" => 4,
                     "description" => esc_html__("how many items","wpresidence")
                  ) , 
                   array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("No of items per row","wpresidence"),
                     "param_name" => "rownumber",
                     "value" => 4,
                     "description" => esc_html__("The number of items per row","wpresidence")
                  ),
                   array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Link to global listing","wpresidence"),
                     "param_name" => "link",
                     "value" => "",
                     "description" => esc_html__("link to global listing","wpresidence")
                  ) ,array(
                     "type" => "dropdown",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Random Pick (yes/no) ","wpresidence"),
                     "param_name" => "random_pick",
                     "value" => $random_pick,
                     "description" => esc_html__("Choose if agents should display randomly on page refresh. ","wpresidence")
                  ) 
               )
            )
            );
            $featured_listings=array('no','yes');
            $items_type=array('properties','articles');
            vc_map(
            array(
                "name" => esc_html__("Recent Items Slider","wpresidence"),//done
                "base" => "slider_recent_items",
                "class" => "",
                "category" => esc_html__('Content','wpresidence-core'),
                'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                'weight'=>100,
                'icon'   =>'wpestate_vc_logo',
                'description'=>esc_html__('Recent Items Slider Shortcode','wpresidence-core'),
                "params" => array(
                    array(
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__("Title","wpresidence"),
                        "param_name" => "title",
                        "value" => "",
                        "description" => esc_html__("Section Title","wpresidence")
                   ),
                    array(
                        "type" => "dropdown",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__("What type of items","wpresidence"),
                        "param_name" => "type",
                        "value" => $items_type,
                        "description" => esc_html__("list properties or articles","wpresidence")
                    ),
                   
                    array(
                        "type" => "autocomplete",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__("Category Id's","wpresidence"),
                        "param_name" => "category_ids",
                        "value" => "",
                        "description" => esc_html__("list of category id's sepearated by comma (*only for properties)","wpresidence"),
            						"dependency" => array(
                  						"element" => "type",
                  						"value" => "properties"
                					),
						 'settings' => array(
                            'multiple' => true,
                            'sortable' => true,
                            'min_length' => 1,
                            'no_hide' => true, 
                            'groups' => false, 
                            'unique_values' => true, 
                            'display_inline' => true, 
                            'values' => $property_category_values,
                      
                        )  ,
                    ),
                    array(
                        "type" => "autocomplete",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__("Action Id's","wpresidence"),
                        "param_name" => "action_ids",
                        "value" => "",
                        "description" => esc_html__("list of action ids separated by comma (*only for properties)","wpresidence"),
						"dependency" => array(
      						"element" => "type",
      						"value" => "properties"
    					),
						 'settings' => array(
                            'multiple' => true,
                            'sortable' => true,
                            'min_length' => 1,
                            'no_hide' => true, 
                            'groups' => false, 
                            'unique_values' => true, 
                            'display_inline' => true, 
                            'values' => $property_action_category_values,
                      
						),
                    ), 
                    array(
                        "type" => "autocomplete",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__("City Id's ","wpresidence"),
                        "param_name" => "city_ids",
                        "value" => "",
                        "description" => esc_html__("list of city ids separated by comma (*only for properties)","wpresidence"),
            						"dependency" => array(
                  						"element" => "type",
                  						"value" => "properties"
                					),
						'settings' => array(
                            'multiple' => true,
                            'sortable' => true,
                            'min_length' => 1,
                            'no_hide' => true, 
                            'groups' => false, 
                            'unique_values' => true, 
                            'display_inline' => true, 
                            'values' => $property_city_values,
                      
						),
                    ),
                    array(
                       "type" => "autocomplete",
                       "holder" => "div",
                       "class" => "",
                       "heading" => esc_html__("Area Id's","wpresidence"),
                       "param_name" => "area_ids",
                       "value" => "",
                       "description" => esc_html__("list of area ids separated by comma (*only for properties)","wpresidence"),
					    "dependency" => array(
      						"element" => "type",
      						"value" => "properties"
    					),
						'settings' => array(
                            'multiple' => true,
                            'sortable' => true,
                            'min_length' => 1,
                            'no_hide' => true, 
                            'groups' => false, 
                            'unique_values' => true, 
                            'display_inline' => true, 
                            'values' => $property_area_values,
                      
						),
                    ),
                    array(
                       "type" => "autocomplete",
                       "holder" => "div",
                       "class" => "",
                       "heading" => esc_html__("County/State Id's","wpresidence"),
                       "param_name" => "state_ids",
                       "value" => "",
                       "description" => esc_html__("list of county/State ids separated by comma (*only for properties)","wpresidence"),
					   "dependency" => array(
      						"element" => "type",
      						"value" => "properties"
    					),
						'settings' => array(
                            'multiple' => true,
                            'sortable' => true,
                            'min_length' => 1,
                            'no_hide' => true,
                            'groups' => false, 
                            'unique_values' => true, 
                            'display_inline' => true, 
                            'values' => $property_county_state_values,
                      
						),
                    ),
                    array(
                       "type" => "textfield",
                       "holder" => "div",
                       "class" => "",
                       "heading" => esc_html__("No of items","wpresidence"),
                       "param_name" => "number",
                       "value" => 4,
                       "description" => esc_html__("how many items","wpresidence")
                    ), 

                    array(
                       "type" => "dropdown",
                       "holder" => "div",
                       "class" => "",
                       "heading" => esc_html__("Show featured listings only?","wpresidence"),
                       "param_name" => "show_featured_only",
                       "value" => $featured_listings,
                       "description" => esc_html__("Show featured listings only? (yes/no)","wpresidence"),
                       "dependency" => array(
                						"element" => "type",
                						"value" => "properties"
              					),
                    ),
                    array(
                       "type" => "textfield",
                       "holder" => "div",
                       "class" => "",
                       "heading" => esc_html__("Auto scroll period","wpresidence"),
                       "param_name" => "autoscroll",
                       "value" => "0",
                       "description" => esc_html__("Auto scroll period in seconds - 0 for manual scroll, 1000 for 1 second, 2000 for 2 seconds and so on.","wpresidence")
                    )  
					,array(
                        "type" => "dropdown",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__("Show Featured Listings First (yes/no)","wpresidence"),
                        "param_name" => "featured_first",
                        "value" => $random_pick,
                        "description" => esc_html__("If 'yes'  featured properties will be displayed first, and the rest by ID(adding date).If 'no' all will be order by  ID(adding date) ","wpresidence"),
                        "dependency" => array(
                						"element" => "type",
                						"value" => "properties"
              					),
                  ) 
                    
                )
            )
            );







              $image_efect=array('yes','no');
              vc_map( array(
               "name" => esc_html__("Icon content box","wpresidence"),//done
               "base" => "icon_container",
               "class" => "",
               "category" => esc_html__('Content','wpresidence-core'),
               'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
               'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
               'weight'=>100,
                'icon'   =>'wpestate_vc_logo',
                'description'=>esc_html__('Icon Content Box Shortcode','wpresidence-core'),  
               "params" => array(
                  array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Box Title","wpresidence"),
                     "param_name" => "title",
                     "value" => "Title",
                     "description" => esc_html__("Box Title goes here","wpresidence")
                  ),
                   array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Image url","wpresidence"),
                     "param_name" => "image",
                     "value" => "",
                     "description" => esc_html__("Image or Icon url","wpresidence")
                  ),
                   array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Content of the box","wpresidence"),
                     "param_name" => "content_box",
                     "value" => "Content of the box goes here",
                     "description" => esc_html__("Content of the box goes here","wpresidence")
                  )
                  ,
                   array(
                     "type" => "dropdown",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Image Effect","wpresidence"),
                     "param_name" => "image_effect",
                     "value" => $image_efect,
                     "description" => esc_html__("Use image effect? yes/no","wpresidence")
                  ) ,
                   array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Link","wpresidence"),
                     "param_name" => "link",
                     "value" => "",
                     "description" => esc_html__("The link with http:// in front","wpresidence")
                  )

               )
            ) );    




              vc_map(
                   array(
                   "name" => esc_html__("Spacer","wpresidence"),
                   "base" => "spacer",
                   "class" => "",
                   "category" => esc_html__('Content','wpresidence-core'),
                   'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                   'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                   'weight'=>102,
                    'icon'   =>'wpestate_vc_logo',
                    'description'=>esc_html__('Spacer Shortcode','wpresidence-core'),
                   "params" => array(
                       array(
                         "type" => "textfield",
                         "holder" => "div",
                         "class" => "",
                         "heading" => esc_html__("Spacer Type","wpresidence"),
                         "param_name" => "type",
                         "value" => "1",
                         "description" => esc_html__("Space Type : 1 with no middle line, 2 with middle line","wpresidence")
                      )   ,
                       array(
                         "type" => "textfield",
                         "holder" => "div",
                         "class" => "",
                         "heading" => esc_html__("Space height","wpresidence"),
                         "param_name" => "height",
                         "value" => "40",
                         "description" => esc_html__("Space height in px","wpresidence")
                      )   
                   )
                )   
            );
			
		 
            $featured_pack_sh=array('no','yes');
              vc_map(
                   array(
                   "name" => esc_html__("Membership Package","wpresidence"),
                   "base" => "estate_membership_packages",
				   
                   "class" => "",
                   "category" => esc_html__('Content','wpresidence-core'),
                   'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                   'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                   'weight'=>102,
                   'icon'   =>'wpestate_vc_logo',
                   'description'=>esc_html__('Membership Package','wpresidence-core'),
                   "params" => array(
                       array(
                            "type" => "autocomplete",
                            "holder" => "div",
                            "class" => "",
                            "heading" => esc_html__("Type Package name","wpresidence"),
                            "param_name" => 'package_id',
                            "value" => "",
                            "description" => esc_html__("Select only one Package","wpresidence"),
							'settings' => array(
								'multiple' => false,
								'sortable' => true,
								'min_length' => 1,
								'no_hide' => true, 
								'groups' => false, 
								'unique_values' => true, 
								'display_inline' => true, 
								'values' => $membership_packages,
						  
							)  ,
                           ),
						   
                        array(
                            "type" => "textfield",
                            "holder" => "div",
                            "class" => "",
                            "heading" => esc_html__("Content of the package box","wpresidence"),
                            "param_name" => "package_content",
                            "value" => "",
                            "description" => esc_html__("Add content for the package","wpresidence")
                       ),
                       array(
                            "type" => "dropdown",
                            "holder" => "div",
                            "class" => "",
                            "heading" => esc_html__("Make package featured","wpresidence"),
                            "param_name" => "pack_featured_sh",
                            "value" => $featured_pack_sh,
                            "description" => esc_html__("Make package featured (no/yes)","wpresidence")
                        ),
                   )
                )   
            );
                    vc_map(
                    array(
                    "name" => esc_html__("Featured Agency / Developer shortcode","wpresidence"),
                    "base" => "estate_featured_user_role",
                    "class" => "",
                    "category" => esc_html__('Content','wpresidence-core'),
                    'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                    'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                    'weight'=>102,
                    'icon'   =>'wpestate_vc_logo',
                    'description'=>esc_html__('Featured Developer / Agency','wpresidence-core'),
                    "params" => array(
                        array(
                            "type" => "autocomplete",
                            "holder" => "div",
                            "class" => "",
                            "heading" => esc_html__("Type Agency or Developer name","wpresidence"),
                            "param_name" => 'user_role_id',
                            "value" => "",
                            "description" => esc_html__("Select one Agency or Developer","wpresidence"),
							'settings' => array(
								'multiple' => false,
								'sortable' => true,
								'min_length' => 1,
								'no_hide' => true, 
								'groups' => false, 
								'unique_values' => true, 
								'display_inline' => true, 
								'values' => $agency_developers_array,
						  
							)  ,
                       ),
                        array(
                            "type" => "textfield",
                            "holder" => "div",
                            "class" => "",
                            "heading" => esc_html__("Status Text Label","wpresidence"),
                            "param_name" => "status",
                            "value" => "",
                            "description" => esc_html__("Status Text Label","wpresidence")
                        ),
                        array(
                            "type" => "textfield",
                            "holder" => "div",
                            "class" => "",
                            "heading" => esc_html__("Image","wpresidence"),
                            "param_name" => "user_shortcode_imagelink",
                            "value" => "",
                            "description" => esc_html__("Path to an image of your choice (best size 300px  x 300px) ","wpresidence")
                        ), 
                    )
                )   
            );
              
              
              
              

            $alignment_type=array('vertical','horizontal');
            vc_map( array(
               "name" => esc_html__("List items by ID","wpresidence"),//done
               "base" => "list_items_by_id",
               "class" => "",
               "category" => esc_html__('Content','wpresidence-core'),
               'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
               'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
               'weight'=>100,
                'icon'   =>'wpestate_vc_logo',
                'description'=>esc_html__('List Items by ID Shortcode','wpresidence-core'),
               "params" => array(
                    array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Title","wpresidence"),
                     "param_name" => "title",
                     "value" => "",
                     "description" => esc_html__("Section Title","wpresidence")
                  ),
                  array(
                     "type" => "dropdown",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("What type of items","wpresidence"),
                     "param_name" => "type",
                     "value" => $items_type,
                     "description" => esc_html__("List properties or articles","wpresidence")
                  ),
                   array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Items IDs","wpresidence"),
                     "param_name" => "ids",
                     "value" => "",
                     "description" => esc_html__("List of IDs separated by comma","wpresidence")
                  ),
                   array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("No of items","wpresidence"),
                     "param_name" => "number",
                     "value" => "3",
                     "description" => esc_html__("How many items do you want to show ?","wpresidence")
                  ) ,

                   array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("No of items per row","wpresidence"),
                     "param_name" => "rownumber",
                     "value" => 4,
                     "description" => esc_html__("The number of items per row","wpresidence")
                  ) , 
                   array(
                     "type" => "dropdown",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Vertical or horizontal ?","wpresidence"),
                     "param_name" => "align",
                     "value" => $alignment_type,
                     "description" => esc_html__("What type of alignment (vertical or horizontal) ?","wpresidence")
                  ),
                   array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Link to global listing","wpresidence"),
                     "param_name" => "link",
                     "value" => "#",
                     "description" => esc_html__("link to global listing with http","wpresidence")
                  )
               )
            ) );    


            $testimonials_type=array(1,2,3);
            vc_map(
                array(
                    'name'              =>  esc_html__("Testimonial",'wpresidence-core'),
                    'base'              =>  "testimonial",
                    'class'             =>  "",
                    'category'          =>  esc_html__('Content','wpresidence-core'),
                    'admin_enqueue_js'  =>  array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                    'admin_enqueue_css' =>  array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                    'weight'            =>  102,
                    'icon'              =>  'wpestate_vc_logo',
                    'description'       =>  esc_html__('Testiomonial Shortcode','wpresidence-core'),
                    'params'            =>  array(
                        array(
                            "type" => "textfield",
                            "holder" => "div",
                            "class" => "",
                            "heading" => esc_html__("Client Name","wpresidence"),
                            "param_name" => "client_name",
                            "value" => "Name Here",
                            "description" => esc_html__("Client name here","wpresidence")
                        ),
                        array(
                            "type" => "textfield",
                            "holder" => "div",
                            "class" => "",
                            "heading" => esc_html__("Title Client","wpresidence"),
                            "param_name" => "title_client",
                            "value" => "happy client",
                            "description" => esc_html__("title or client postion ","wpresidence")
                        ),
                        array(
                            "type" => "textfield",
                            "holder" => "div",
                            "class" => "",
                            "heading" => esc_html__("Image","wpresidence"),
                            "param_name" => "imagelinks",
                            "value" => "",
                            "description" => esc_html__("Path to client picture, (best size 120px  x 120px) ","wpresidence")
                        ) ,
                        array(
                            "type" => "textarea",
                            "holder" => "div",
                            "class" => "",
                            "heading" => esc_html__("Testimonial Text Here.","wpresidence"),
                            "param_name" => "testimonial_text",
                            "value" => "",
                            "description" => esc_html__("Testimonial Text Here. ","wpresidence")
                        ) ,
                        array(
                            "type" => "dropdown",
                            "holder" => "div",
                            "class" => "",
                            "heading" => esc_html__("Testimonial Type","wpresidence"),
                            "param_name" => "testimonial_type",
                            "value" => $testimonials_type,
                            "description" => esc_html__("Select 1,2,3","wpresidence")
                        ), array(
                            "type" => "textfield",
                            "holder" => "div",
                            "class" => "",
                            "heading" => esc_html__("Stars","wpresidence"),
                            "param_name" => "stars_client",
                            "value" => "5",
                            "description" => esc_html__("Only for type3: no of stars for reviews (from 1 to 5, increment by 0.5) ","wpresidence")
                        ), array(
                            "type" => "textfield",
                            "holder" => "div",
                            "class" => "",
                            "heading" => esc_html__("Title","wpresidence"),
                            "param_name" => "testimonial_title",
                            "value" => "Great House",
                            "description" => esc_html__("Only for type3:Testimonial Title ","wpresidence")
                        ),
                    )
                )   
            );
            $featured_listings=array('no','yes');
			
			
			
            vc_map(
            array(
               "name" => esc_html__("Recent Items","wpresidence"),//done
               "base" => "recent_items",
               "class" => "",
               "category" => esc_html__('Content','wpresidence-core'),
               'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
               'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
               'weight'=>100,
               'icon'   =>'wpestate_vc_logo',
               'description'=>esc_html__('Recent Items Shortcode','wpresidence-core'),
               "params" => array(
                   array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Title","wpresidence"),
                     "param_name" => "title",
                     "value" => "",
                     "description" => esc_html__("Section Title","wpresidence")
                  ),
                  array(
                     "type" => "dropdown",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("What type of items","wpresidence"),
                     "param_name" => "type",
                     "value" => $items_type,
                     "description" => esc_html__("list properties or articles","wpresidence")
                  ),
                    array(
                     "type" => "autocomplete",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("List of terms for the filter bar (*applies only for properties, not articles/posts)","wpresidence"),
                     "param_name" => "control_terms_id",
                     "value" => "",
                     "description" => esc_html__("Terms are items in any property taxonomy(Category, Action, City, Area or State/County)","wpresidence"),
                     "dependency" => array(
      									"element" => "type",
      									"value" => "properties"
    								),
					 'settings' => array(
                            'multiple' => true,
                            'sortable' => true,
                            'min_length' => 1,
                            'no_hide' => true, // In UI after select doesn't hide an select list
                            'groups' => false, // In UI show results grouped by groups
                            'unique_values' => true, // In UI show results except selected. NB! You should manually check values in backend
                            'display_inline' => true, // In UI show results inline view
                            'values' => $all_tax,
                      
                        )  ,
                  ),
                   array(
                     "type" => "autocomplete",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Type Category names","wpresidence"),
                     "param_name" => "category_ids",
                     "value" => "",
                     "description" => esc_html__("list of category id's sepearated by comma","wpresidence")   ,
                     "dependency" => array(
      									"element" => "type",
      									"value" => "properties"
    								),
					 'settings' => array(
                            'multiple' => true,
                            'sortable' => true,
                            'min_length' => 1,
                            'no_hide' => true, // In UI after select doesn't hide an select list
                            'groups' => false, // In UI show results grouped by groups
                            'unique_values' => true, // In UI show results except selected. NB! You should manually check values in backend
                            'display_inline' => true, // In UI show results inline view
                            'values' => $property_category_values,
                      
                        )  ,
                  ),
                     array(
                     "type" => "autocomplete",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Type Action names","wpresidence"),
                     "param_name" => "action_ids",
                     "value" => "",
                     "description" => esc_html__("list of action ids separated by comma (*only for properties)","wpresidence"),
                     "dependency" => array(
      									"element" => "type",
      									"value" => "properties"
    								),
					 'settings' => array(
                            'multiple' => true,
                            'sortable' => true,
                            'min_length' => 1,
                            'no_hide' => true, // In UI after select doesn't hide an select list
                            'groups' => false, // In UI show results grouped by groups
                            'unique_values' => true, // In UI show results except selected. NB! You should manually check values in backend
                            'display_inline' => true, // In UI show results inline view
                            'values' => $property_action_category_values,
                      
                    ),
                  ), 
                   array(
                     "type" => "autocomplete",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Type City names","wpresidence"),
                     "param_name" => "city_ids",
                     "value" => "",
                     "description" => esc_html__("list of city ids separated by comma (*only for properties)","wpresidence"),
                     "dependency" => array(
      									"element" => "type",
      									"value" => "properties"
    								),
					 'settings' => array(
                            'multiple' => true,
                            'sortable' => true,
                            'min_length' => 1,
                            'no_hide' => true, // In UI after select doesn't hide an select list
                            'groups' => false, // In UI show results grouped by groups
                            'unique_values' => true, // In UI show results except selected. NB! You should manually check values in backend
                            'display_inline' => true, // In UI show results inline view
                            'values' => $property_city_values,
                      
                    ),
                  ),
                    array(
                     "type" => "autocomplete",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Type Area names","wpresidence"),
                     "param_name" => "area_ids",
                     "value" => "",
                     "description" => esc_html__("list of area ids separated by comma (*only for properties)","wpresidence"),
                     "dependency" => array(
      									"element" => "type",
      									"value" => "properties"
    								),
					 'settings' => array(
                            'multiple' => true,
                            'sortable' => true,
                            'min_length' => 1,
                            'no_hide' => true, // In UI after select doesn't hide an select list
                            'groups' => false, // In UI show results grouped by groups
                            'unique_values' => true, // In UI show results except selected. NB! You should manually check values in backend
                            'display_inline' => true, // In UI show results inline view
                            'values' => $property_area_values,
                      
                    ),
                  ),
                    array(
                       "type" => "autocomplete",
                       "holder" => "div",
                       "class" => "",
                       "heading" => esc_html__("Type County/State names","wpresidence"),
                       "param_name" => "state_ids",
                       "value" => "",
                       "description" => esc_html__("list of county/State ids separated by comma (*only for properties)","wpresidence"),
                       "dependency" => array(
        									"element" => "type",
        									"value" => "properties"
      								),
					 'settings' => array(
                            'multiple' => true,
                            'sortable' => true,
                            'min_length' => 1,
                            'no_hide' => true, // In UI after select doesn't hide an select list
                            'groups' => false, // In UI show results grouped by groups
                            'unique_values' => true, // In UI show results except selected. NB! You should manually check values in backend
                            'display_inline' => true, // In UI show results inline view
                            'values' => $property_county_state_values,
                      
                    ),
                    ),
                   array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("No of items","wpresidence"),
                     "param_name" => "number",
                     "value" => 4,
                     "description" => esc_html__("how many items","wpresidence")
                  ) , 
                   array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("No of items per row","wpresidence"),
                     "param_name" => "rownumber",
                     "value" => 4,
                     "description" => esc_html__("The number of items per row","wpresidence")
                  ) , 
                   array(
                     "type" => "dropdown",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Vertical or horizontal ?","wpresidence"),
                     "param_name" => "align",
                     "value" => $alignment_type,
                     "description" => esc_html__("What type of alignment (vertical or horizontal) ?","wpresidence")
                  ),
                  array(
                     "type" => "dropdown",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Show featured listings only?","wpresidence"),
                     "param_name" => "show_featured_only",
                     "value" => $featured_listings,
                     "description" => esc_html__("Show featured listings only? (yes/no)","wpresidence")        ,
                     "dependency" => array(
      									"element" => "type",
      									"value" => "properties"
    								)
                  ) ,array(
                     "type" => "dropdown",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Random Pick (yes/no) ","wpresidence"),
                     "param_name" => "random_pick",
                     "value" => $random_pick,
                     "description" => esc_html__("Will deactivate theme cache and increase loading time. (*only for properties)","wpresidence")
                  ) ,array(
                     "type" => "dropdown",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Show Featured Listings First (yes/no)","wpresidence"),
                     "param_name" => "featured_first",
                     "value" => $random_pick,
                     "description" => esc_html__("If 'yes'  featured properties will be displayed first, and the rest by ID(adding date).If 'no' all will be order by  ID(adding date) ","wpresidence") ,
                     "dependency" => array(
      									"element" => "type",
      									"value" => "properties"
    								)
                  ) 
               )
            )
            );



            vc_map(
            array(
               "name" => esc_html__("Featured Agent","wpresidence"),
               "base" => "featured_agent",
               "class" => "",
               "category" => esc_html__('Content','wpresidence-core'),
               'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
               'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
               'weight'=>100,
               'icon'   =>'wpestate_vc_logo',
               'description'=>esc_html__('Featured Agent Shortcode','wpresidence-core'),
               "params" => array(
                  array(
                     "type" => "autocomplete",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Type Agent name","wpresidence"),
                     "param_name" => "id",
                     "value" => "",
                     "description" => esc_html__("Select one Agent","wpresidence"),
					 'settings' => array(
						'multiple' => false,
						'sortable' => true,
						'min_length' => 1,
						'no_hide' => true, 
						'groups' => false, 
						'unique_values' => true, 
						'display_inline' => true, 
						'values' => $agent_array,
						  
					),
                  ),
                   array(
                     "type" => "textarea",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Notes","wpresidence"),
                     "param_name" => "notes",
                     "value" => "",
                     "description" => esc_html__("Notes for featured agent","wpresidence")
                  )
               )
            )
            );
            
            $featured_art_type=array(1,2);
            vc_map(
               array(
               "name" => esc_html__("Featured Article","wpresidence"),
               "base" => "featured_article",
               "class" => "",
               "category" => esc_html__('Content','wpresidence-core'),
               'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
               'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
               'weight'=>100,
               'icon'   =>'wpestate_vc_logo',
               'description'=>esc_html__('Featured Article Shortcode','wpresidence-core'),
               "params" => array(
                  array(
                     "type" => "autocomplete",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Type article name","wpresidence"),
                     "param_name" => "id",
                     "value" => "",
                     "description" => esc_html__("Select one article","wpresidence"),
					 'settings' => array(
						'multiple' => false,
						'sortable' => true,
						'min_length' => 1,
						'no_hide' => true, 
						'groups' => false, 
						'unique_values' => true, 
						'display_inline' => true, 
						'values' => $article_array,
						  
					),
                  ),
                    array(
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__("Featured text (for type1)","wpresidence"),
                        "param_name" => "second_line",
                        "value" => "",
                        "description" => esc_html__("featured text","wpresidence")
                   ),
                    array(
                        "type" => "dropdown",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__("Design Type","wpresidence"),
                        "param_name" => "design_type",
                        "value" =>  $featured_art_type,
                        "description" => esc_html__("type 1 or 2","wpresidence")
                  )    
               )
            )
            );
            $featured_prop_type=array(1,2,3,4);
            vc_map(
            array(
               "name" => esc_html__("Featured Property","wpresidence"),
               "base" => "featured_property",
               "class" => "",
               "category" => esc_html__('Content','wpresidence-core'),
               'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
               'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
               'weight'=>100,
               'icon'   =>'wpestate_vc_logo',
               'description'=>esc_html__('Featured Property Shortcode','wpresidence-core'),
               "params" => array(
                  array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Property id","wpresidence"),
                     "param_name" => "id",
                     "value" => "",
                     "description" => esc_html__("Property id","wpresidence")
                  ),
                   array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Second Line","wpresidence"),
                     "param_name" => "sale_line",
                     "value" => "",
                     "description" => esc_html__("Second line text","wpresidence")
                  )  ,
                    array(
                     "type" => "dropdown",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Design Type","wpresidence"),
                     "param_name" => "design_type",
                     "value" =>  $featured_prop_type,
                     "description" => esc_html__("type 1, 2, 3 or 4","wpresidence")
                  )    
               )
            )
            );


            vc_map(array(
               "name" => esc_html__("Login Form","wpresidence"),
               "base" => "login_form",
               "class" => "",
               "category" => esc_html__('Content','wpresidence-core'),
               'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
               'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
               'weight'=>100,
               'icon'   =>'wpestate_vc_logo',
               'description'=>esc_html__('Login Form Shortcode','wpresidence-core'),  
               "params" => array( array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Register link text","wpresidence"),
                     "param_name" => "register_label",
                     "value" => "",
                     "description" => esc_html__("Register link text","wpresidence")
                    )     , 
                    array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Register page url","wpresidence"),
                     "param_name" => "register_url",
                     "value" => "",
                     "description" => esc_html__("Register page url","wpresidence")
                  )      )
            )
            );


            vc_map(
             array(
               "name" => esc_html__("Register Form","wpresidence"),
               "base" => "register_form",
               "class" => "",
               "category" => esc_html__('Content','wpresidence-core'),
               'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
               'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
               'weight'=>100,
               'icon'   =>'wpestate_vc_logo',
               'description'=>esc_html__('Register Form Shortcode','wpresidence-core'),    
               "params" => array()
            )

            );




            vc_map(
                array(
               "name" => esc_html__("Advanced Search","wpresidence"),
               "base" => "advanced_search",
               "class" => "",
               "category" => esc_html__('Content','wpresidence-core'),
               'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
               'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
               'weight'=>100,
               'icon'   =>'wpestate_vc_logo',
               'description'=>esc_html__('Advanced Search Shortcode','wpresidence-core'),     
               "params" => array(
                   array(
                     "type" => "textfield",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Title","wpresidence"),
                     "param_name" => "title",
                     "value" => "",
                     "description" => esc_html__("Section Title","wpresidence")
                  ))
            )


            );


            $text_align=array('left','right','center');
            vc_map(
                array(
               "name" => esc_html__("Contact Us","wpresidence"),
               "base" => "contact_us_form",
               "class" => "",
               "category" => esc_html__('Content','wpresidence-core'),
               'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
               'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
               'weight'=>100,
               'icon'   =>'wpestate_vc_logo',
               'description'=>esc_html__('Contact Form Shortcode','wpresidence-core'),     
               "params" => array(
                   array(
                     "type" => "dropdown",
                     "holder" => "div",
                     "class" => "",
                     "heading" => esc_html__("Text Align","wpresidence"),
                     "param_name" => "text_align",
                     "value" => $text_align,
                     "description" => esc_html__("Section Title","wpresidence")
                  ))
            )


            );
 
            vc_map(
                array(
                "name" => esc_html__("Testimonial Slider","wpresidence"),
                "base" => "testimonial_slider",
                "as_parent" => array('only' => 'testimonial'),
                "content_element" => true,
                "is_container" => true,
                "class" => "",
                "category" => esc_html__('Content','wpresidence-core'),
                'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                'weight'=>100,
                'icon'   =>'wpestate_vc_logo',
                'description'=>esc_html__('Testimonial Slider','wpresidence-core'),     
                "params" => array(
                    array(
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__("Title","wpresidence"),
                        "param_name" => "title",
                        "value" => "",
                        "description" => esc_html__("Section Title","wpresidence")
                    ), array(
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__("Visible Items","wpresidence"),
                        "param_name" => "visible_items",
                        "value" => "1",
                        "description" => esc_html__("How many items are visible","wpresidence")
                    )
                    , array(
                        "type"          =>  "dropdown",
                        "holder"        =>  "div",
                        "class"         =>  "",
                        "heading"       =>  esc_html__("Slider Type","wpresidence"),
                        "param_name"    =>  "slider_types",
                        "value"         =>  $testimonials_type,
                        "description"   =>  esc_html__("What type of slider do you use?","wpresidence")
                    ),
                    
                    
                    )
                )
                    
                    
                    


            );
			
			
			
			// places slider 
			
			
			
			// reproces autocomplete
			
                vc_map( array(
                "name" => esc_html__( "Display Places Slider","wpresidence"),//done
                "base" => "places_slider",
                "class" => "",
                "category" => esc_html__( 'Content','wpresidence-core'),
                'admin_enqueue_js' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.js'),
                'admin_enqueue_css' => array(WPESTATE_PLUGIN_DIR_URL.'/vc_extend/bartag.css'),
                'weight'=>100,
                'icon'   =>'wpestate_vc_logo',
                'description'=>esc_html__( 'Display Places Slider','wpresidence-core'),  
                "params" => array(
				             
                    array(
                        "type" => "autocomplete",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__( "Type Categories,Actions,Cities,Areas,Neighborhoods or County name you want to show","wpresidence"),
                        "param_name" => "place_list",
                        "value" => "",
                        "description" => esc_html__( "Type Categories,Actions,Cities,Areas,Neighborhoods or County name you want to show","wpresidence"),
                        'settings' => array(
                            'multiple' => true,
                            'sortable' => true,
                            'min_length' => 1,
                            'no_hide' => true, // In UI after select doesn't hide an select list
                            'groups' => false, // In UI show results grouped by groups
                            'unique_values' => true, // In UI show results except selected. NB! You should manually check values in backend
                            'display_inline' => true, // In UI show results inline view
                            'values' => $all_tax,
                      
                        )  ,
                    )  ,
                    array(
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__( "Items per row","wpresidence"),
                        "param_name" => "place_per_row",
                        "value" => "3",
                        "description" => esc_html__( "How many items listed per row? For type 2 use only 1.","wpresidence")
                    ),
	 

                    array(
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => esc_html__( "Extra Class Name","wpresidence"),
                        "param_name" => "extra_class_name",
                        "value" => "",
                        "description" => esc_html__( "Extra Class Name","wpresidence")
                    )
                )    
            ) 
            );
			
			
        }
    endif;    
    
    if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
        class WPBakeryShortCode_testimonial_slider extends WPBakeryShortCodesContainer {
        }
    }
    
    if ( class_exists( 'WPBakeryShortCode' ) ) {
        class WPBakeryShortCode_testimonial extends WPBakeryShortCode {
        }
    }
    
endif;

function custom_css_wpestate($class_string, $tag) {
  if ($tag =='vc_tabs' || $tag=='vc_tta_tabs' ) {
    $class_string .= ' wpestate_tabs'; 
  }

  if ($tag =='vc_tour' || $tag=='vc_tta_tour') {
    $class_string .= ' wpestate_tour'; 
  }
  
  if ($tag =='vc_accordion'|| $tag=='vc_tta_accordion' ) {
    $class_string .= ' wpestate_accordion'; 
  }
  
  if ($tag =='vc_accordion_tab' ) {
    $class_string .= ' wpestate_accordion_tab'; 
  }
  
  if ($tag =='vc_carousel' ) {
    $class_string .= ' wpestate_carousel'; 
  }

  if ($tag =='vc_progress_bar' ) {
    $class_string .= ' wpestate_progress_bar'; 
  }

  if ($tag =='vc_toggle' ) {
    $class_string .= ' wpestate_toggle'; 
  }
  
  if ($tag =='vc_message' ) {
    $class_string .= ' wpestate_message'; 
  }
  
  if ($tag =='vc_posts_grid' ) {
    $class_string .= ' wpestate_posts_grid'; 
  }
  
  if ($tag =='vc_cta_button' ) {
    $class_string .= ' wpestate_cta_button '; 
  }
  
  if ($tag =='vc_cta_button2' ) {
    $class_string .= ' wpestate_cta_button2 '; 
  }
  
  
  
  return $class_string.' '.$tag;
}


// Filter to Replace default css class for vc_row shortcode and vc_column
add_filter('vc_shortcodes_css_class', 'custom_css_wpestate', 10,2);


if( !function_exists('wpestate_limit64') ): 
    function wpestate_limit64($stringtolimit){
        return mb_substr($stringtolimit,0,64);
    }
endif;


if( !function_exists('wpestate_limit54') ): 
    function wpestate_limit54($stringtolimit){
        return mb_substr($stringtolimit,0,54);
    }
endif;

if( !function_exists('wpestate_limit50') ): 
    function wpestate_limit50($stringtolimit){ // 14
        return mb_substr($stringtolimit,0,50);
    }
endif;

if( !function_exists('wpestate_limit45') ): 
    function wpestate_limit45($stringtolimit){ // 19
        return mb_substr($stringtolimit,0,45);
    }
endif;

if( !function_exists('wpestate_limit27') ): 
    function wpestate_limit27($stringtolimit){ // 27
        return mb_substr($stringtolimit,0,27);
    }
endif;


?>