<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Gift Notification
 * 
 * Type : HTML
 * 
 * $voucode				: displays the voucher code
 * $first_name			: displays the first name of vendor
 * $last_name			: displays the last name of vendor
 * $redeem_date			: displays redeem date
 * $redeem_method		: displays the redeem method
 * $redeem_amount		: displays the redeem amount
 * $vou_redeem_method	: displays voucher redeem method
 * 
 * @package WooCommerce - PDF Vouchers
 * @since 3.3.1
 */
?>

<?php do_action( 'woocommerce_email_header', $email_heading ); ?>

<p><?php _e( 'Hello,', 'woovoucher' ); ?></p>

<p><?php echo sprintf( __('A voucher code <b>%s</b> has been redeemed by %s %s!', 'woovoucher'), $voucode, $first_name, $last_name );?></p>

<p><?php echo sprintf( __('Voucher Redeem Method: %s', 'woovoucher'), $vou_redeem_method ); ?></p>

<p><?php echo sprintf( __('Redeem Date & Time: %s', 'woovoucher'), $redeem_date ); ?></p>

<p><?php echo sprintf( __('Redeem Type: %s', 'woovoucher'), !empty( $redeem_method ) ? $redeem_method : __('Full', 'woovoucher') ); ?></p>

<?php if(!empty($redeem_method) && strtolower( $redeem_method ) == 'partial') { ?>
	<p><?php echo sprintf( __('Redeem Amount: %s', 'woovoucher'), $redeem_amount ); ?></p>
<?php } ?>

<?php do_action( 'woocommerce_email_footer' ); ?>