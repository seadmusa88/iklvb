<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Shortcodes Class
 * 
 * Handles shortcodes functionality of plugin
 * 
 * @package WooCommerce - PDF Vouchers
 * @since 1.0.0
 */
class WOO_Vou_Template_Shortcodes {
	
	public $model;
	
	function __construct() {
		
		global $woo_vou_model;
		$this->model	= $woo_vou_model;
	}
	
	/**
	 * Download PDF form frontend OR backend - Replace shortcodes with value
	 * 
	 * Adding All Shortcodes with value in voucher template html
	 * 
	 * @package WooCommerce - PDF Vouchers
	 * @since 1.0.0
	 */
	function woo_vou_pdf_template_replace_shortcodes( $voucher_template_html, $orderid, $item_key, $items, $voucodes, $productid, $woo_vou_details ) {
		
		// Creating order object for order id
		$woo_order = new WC_Order( $orderid );				
		//$items = $woo_order_details->get_items(); // no need to it. already passed in filter
		
		// Getting product name
		$woo_product_details = $this->model->woo_vou_get_product_details( $orderid, $items );		
		// get product
		$product = wc_get_product( $productid );				

		// Order id
		$woo_vou_details['orderid'] = $orderid;
		$payment_method_title = $this->model->woo_vou_get_payment_method_from_order($woo_order);
		
		// get payment method title
		$woo_vou_details['payment_method']	= $payment_method_title;

		// Get billing details
		$woo_vou_billing_details = $this->model->woo_vou_get_buyer_information($orderid);
		$billing_email = $woo_vou_billing_details['email'];
		$billing_first_name = $woo_vou_billing_details['first_name'];
		$billing_last_name = $woo_vou_billing_details['last_name'];
		$billing_phone = $woo_vou_billing_details['phone'];
		$billing_postcode = $woo_vou_billing_details['postcode'];
        $billing_city = $woo_vou_billing_details['city'];
        $billing_address = $woo_vou_billing_details['address_1'] . ' ' . $woo_vou_billing_details['address_2'];

		// Getting Buyer details
		$woo_vou_details['buyeremail'] 	= $billing_email;		
		$buyer_fname 	= $billing_first_name;
		$buyer_lname 	= $billing_last_name;
		$woo_vou_details['buyername'] = $buyer_fname .' '. $buyer_lname;			
		$woo_vou_details['buyerphone'] = $billing_phone;

		// Get date format from global setting
		$date_format = get_option( 'date_format' );

		// Get recipient data
		$recipient_data	= $this->model->woo_vou_get_recipient_data_using_item_key( $item_key );
		$woo_vou_details['recipientname']		= isset( $recipient_data['recipient_name'] ) ? $recipient_data['recipient_name'] 	: '';
		$woo_vou_details['recipientemail']		= isset( $recipient_data['recipient_email'] ) ? $recipient_data['recipient_email'] 	: '';
		$woo_vou_details['recipientmessage']	= isset( $recipient_data['recipient_message'] ) ? $recipient_data['recipient_message'] 	: '';
		$woo_vou_details['recipientgiftdate']   = isset( $recipient_data['recipient_giftdate'] ) ? date( $date_format, strtotime( $recipient_data['recipient_giftdate'] ) ) : '';

		// Getting Billing details
        $woo_vou_details['billing_postcode'] = $billing_postcode;
        $woo_vou_details['billing_city'] = $billing_city;	
        $woo_vou_details['billing_address'] = $billing_address;

		// If WC_Booking class exists
		if ( class_exists( 'WC_Booking' ) ) {
			
			// Declare varible for booking shortcodes
			$booking_start_date = $booking_start_time = $booking_end_time = '';
			$person_html_array  = array();

			// Getting booking ids
			$booking_ids = WC_Booking_Data_Store::get_booking_ids_from_order_item_id( $item_key );
			if ( !empty($booking_ids[0]) ) {
				
				// Getting booking post and product
				$booking 		= new WC_Booking( $booking_ids[0] );
				$product_id 	= $booking->get_product_id();
				$product    	= $booking->get_product( $product_id );
				$booking_start 	= $booking->get_start();
				$booking_end	= $booking->get_end();
				
				// Getting start date and time
				$booking_start_date = ( !empty($booking_start) )? date( 'd.m.Y', $booking_start ) : '' ;
				$booking_start_time = ( !empty($booking_start) )? date( 'H:i', $booking_start ) : '' ;
				$booking_end_time 	= ( !empty($booking_end) )? date( 'H:i', $booking_end ) : '' ;
				
				// Getting the person
				$person_counts 		= $booking->get_person_counts();
				$person_types 		= $product ? $product->get_person_types() : array();
				
				if ( count( $person_counts ) > 0 || count( $person_types ) > 0 ) {
		
					foreach ( $person_counts as $person_id => $person_count ) {
						$person_type = null;

						try {
							// Getting the person object
							$person_type = new WC_Product_Booking_Person_Type( $person_id );
						} catch ( Exception $e ) {
							// This person type was deleted from the database.
							unset( $person_counts[ $person_id ] );
						}
		
						if ( $person_type ) {
							$person_html_array[] = '<b>'.$person_type->get_name().': '.$person_count.'</b>';
						}
					}		
				}
		
			}

			// Adding the booking shortcodes
			$woo_vou_details['booking_date'] 	= ( !empty( $booking_start_date ) ) ? $booking_start_date : '';
			$woo_vou_details['booking_time'] 	= ( !empty( $booking_start_time ) && !empty( $booking_end_time ) ) ? ( $booking_start_time . ' - ' . $booking_end_time ) : '' ;
			$woo_vou_details['booking_persons'] = ( !empty($person_html_array) )? (implode( '<br>', $person_html_array )): '';

			// Adding WC Vendor shop name shortcode
			$woo_vou_details['wc_vendor_shopname'] = !empty( $wc_vendor_shopname ) ? $wc_vendor_shopname : '';
		}

		// Get custom shortcode value here...		
		//$woo_vou_details['custom_shortcode'] = $custom_value;
		
		$voucher_template_html = woo_vou_replace_all_shortcodes_with_value( $voucher_template_html, $woo_vou_details );
	 	
	  	return $voucher_template_html;
	}
	
	/**
	 * Preview PDF - Replace shortcodes with value
	 * 
	 * Adding All Shortcodes with value in voucher template html
	 * 
	 * @package WooCommerce - PDF Vouchers
	 * @since 1.0.0
	 */
	function woo_vou_pdf_template_preview_replace_shortcodes( $voucher_template_html, $voucher_template_id ) {				
		
		global $pdf_voucodes, $woo_vou_model;
		
		$model = $woo_vou_model;
		
		// Check if relative path is enabled
		$vou_relative_path_option	= get_option('vou_enable_relative_path');
		$woo_vou_img_path			= !empty( $vou_relative_path_option ) && $vou_relative_path_option == 'yes' ? WOO_VOU_IMG_DIR : WOO_VOU_IMG_URL;
		
		$woo_vou_details = array();
		
		// site url
		$woo_vou_details['siteurl'] = 'www.bebe.com';
		
		// site logo
		$vousitelogohtml = '';
		$vou_site_url = get_option( 'vou_site_logo' );
		if( !empty( $vou_site_url ) ) {
			if( !empty( $vou_relative_path_option ) && $vou_relative_path_option == 'yes' ) {
				
				$vou_site_attachment_id = $model->woo_vou_get_attachment_id_from_url( $vou_site_url ); 			// Get attachment _id from attachment_url
				$vousitelogohtml = '<img src="' . get_attached_file( $vou_site_attachment_id ) . '" alt="" />'; // Get relative path and append in image tag
			} else {
				$vousitelogohtml = '<img src="' . $vou_site_url . '" alt="" />';
			}
		}
		$woo_vou_details['sitelogo'] = $vousitelogohtml;
		
		// vendor's logo
		$vou_url = $woo_vou_img_path . '/vendor-logo.png';
		$voulogohtml = '<img src="' . $vou_url . '" alt="" />';
		$woo_vou_details['vendorlogo'] = $voulogohtml;
		
		// Vendor address
		$vendor_address = '<table><tr><td>'.__( 'Infiniti Mall Malad', 'woovoucher' ) . "</td></tr>";
		$vendor_address .= "<tr><td>" . __( 'GF 9 & 10, Link Road, Mindspace, Malad West', 'woovoucher' ) . "</td></tr>";
		$vendor_address .= "<tr><td>" . __( 'Mumbai, Maharashtra 400064', 'woovoucher' ) . "</td></tr></table>";
		$woo_vou_details['vendoraddress'] = $vendor_address;
		
		// Vendor Email
		$vendor_email 	= 'vendor_email@gmail.com';
		$woo_vou_details['vendoremail'] = nl2br( $vendor_email );
		
		// next month
		$nextmonth = mktime( date("H"),  date("i"), date("s"), date("m")+1,   date("d"),   date("Y") );
		$woo_vou_details['expiredate'] 		= $this->model->woo_vou_get_date_format( date('d-m-Y', $nextmonth ) );
		$woo_vou_details['expiredatetime'] 	= $this->model->woo_vou_get_date_format( date('d-m-Y H:i:s', $nextmonth ), true );
		
		// previous month
		$previousmonth = mktime( date("H"), date("i"), date("s"), date("m")-1,   date("d"),   date("Y") );
		$woo_vou_details['startdate'] 		= $this->model->woo_vou_get_date_format( date('d-m-Y', $previousmonth ) );
		$woo_vou_details['startdatetime'] 	= $this->model->woo_vou_get_date_format( date('d-m-Y H:i:s', $previousmonth ), true );
		
		// Redeem instruction
		$redeem_instruction = __( 'Redeem instructions :', 'woovoucher' );
		$redeem_instruction .= __( 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.', 'woovoucher' );
		$woo_vou_details['redeem'] = $redeem_instruction;
		
		// vendor locations
		$locations = '<strong>' . __( 'DELHI:', 'woovoucher' ) . '</strong> ' . __( 'Dlf Promenade Mall & Pacific Mall', 'woovoucher' );
		$locations .= "\n".'<strong>' . __( 'MUMBAI:', 'woovoucher' ) . '</strong> ' . __( 'Infiniti Mall, Malad & Phoenix MarketCity', 'woovoucher' );
		$locations .= "\n".'<strong>' . __( 'BANGALORE:', 'woovoucher' ) . '</strong> ' . __( 'Phoenix MarketCity Mall', 'woovoucher' );
		$locations .= "\n".'<strong>' . __( 'PUNE:', 'woovoucher' ) . '</strong> ' . __( 'Phoenix MarketCity Mall', 'woovoucher' );
		$woo_vou_details['location'] = nl2br($locations);
		
		// buyer information
		$woo_vou_details['buyername'] 	= __('WpWeb', 'woovoucher');
		$woo_vou_details['buyeremail'] = 'web101@gmail.com';
		$woo_vou_details['buyerphone'] = __( '9999999999','woovoucher' );

		// billing information
        $woo_vou_details['billing_postcode'] = __( '110070','woovoucher' );
        $woo_vou_details['billing_city'] = __( 'Delhi','woovoucher' );
        $woo_vou_details['billing_address'] = __( 'DLF Promenade, Plot No 3, Nelson Mandela Road','woovoucher' );

		// order & product related information
		$woo_vou_details['orderid'] = '101';
		$woo_vou_details['orderdate'] = date("d-m-Y");
		
		$woo_vou_details['productname']			= __('Test Product', 'woovoucher');
		$woo_vou_details['producttitle']		= __('Test Product', 'woovoucher');
		$woo_vou_details['variationname']		= __('Test Variation', 'woovoucher');
		$woo_vou_details['variationdesc']		= __('Test Variation Description', 'woovoucher');
		$woo_vou_details['productprice']		= '$'.number_format('10', 2);
		$woo_vou_details['regularprice']		= '$'.number_format('15', 2);
		$woo_vou_details['discounted_amount']  	= '$'.number_format('5', 2);
		$woo_vou_details['payment_method']		= 'Test Payment Method';
		$woo_vou_details['quantity']			= 1;
		$woo_vou_details['sku']    				= 'WooSKU';
		$woo_vou_details['productshortdesc']	= 'Product Short Description';	
		$woo_vou_details['productfulldesc']		= 'Product Full Description';
		
		// Voucher related information
		$recipient_gift_date = mktime( date("H"),  date("i"), date("s"), date("m"),   date("d")+7,   date("Y") );
		$codes 			= __( '[The voucher code will be inserted automatically here]', 'woovoucher' );
		$pdf_voucodes	= $codes;
		$woo_vou_details['codes'] 				= $codes;
		$woo_vou_details['recipientname']		= 'Test Name';
		$woo_vou_details['recipientemail']		= 'recipient@example.com';
		$woo_vou_details['recipientmessage']	= 'Test message';
		$woo_vou_details['recipientgiftdate'] 	= $this->model->woo_vou_get_date_format( date('d-m-Y', $recipient_gift_date ) );;

		// If WC_Booking class exists
		if ( class_exists( 'WC_Booking' ) ) {

			// Generate booking time
			$booking_date = mktime( date("H"), date("i"), date("s"), date("m"),   date("d")+3,   date("Y") );
			$woo_vou_details['booking_date'] 	= $this->model->woo_vou_get_date_format( date('d-m-Y', $booking_date ) ); // Value for booking date
			$woo_vou_details['booking_time'] 	= $this->model->woo_vou_get_date_format( date('H:i', $booking_date ), true ); // Value for booking time
			$woo_vou_details['booking_persons'] = '<b>'.__( 'No. of persons = 5','woovoucher' ).'</b>'; // Value for booking persons
		}

		// Assign default value for shop name
		$woo_vou_details['wc_vendor_shopname'] = class_exists( 'WC_Vendors' ) ? __('Vendor Shop', 'woovoucher') : '';

		// Get custom shortcode value here...		
		// $woo_vou_details['custom_shortcode'] = $custom_value;
		
		$voucher_template_html = woo_vou_replace_all_shortcodes_with_value( $voucher_template_html, $woo_vou_details );				
		  
	  	return $voucher_template_html;
	}

	public function woo_vou_pdf_preview_template_replace_shortcodes( $voucher_template_inner_html, $voucodes, $productid, $woo_vou_details ){

		global $pdf_voucodes, $woo_vou_model;
		
		$model = $woo_vou_model;

		// Get date format from global setting
		$date_format = get_option( 'date_format' );

		// Check if relative path is enabled
		$vou_relative_path_option	= get_option('vou_enable_relative_path');
		$woo_vou_img_path			= !empty( $vou_relative_path_option ) && $vou_relative_path_option == 'yes' ? WOO_VOU_IMG_DIR : WOO_VOU_IMG_URL;
		
		// buyer information
		$woo_vou_details['buyername'] 	= __( '{buyername} - This will be replaced with original value after placing the order.', 'woovoucher' );
		$woo_vou_details['buyeremail'] 	= __( '{buyeremail} - This will be replaced with original value after placing the order.', 'woovoucher' );
		$woo_vou_details['buyerphone'] 	= __( '{buyerphone} - This will be replaced with original value after placing the order.', 'woovoucher' );

		// billing information
        $woo_vou_details['billing_postcode'] 	= __( '{billing_postcode} - This will be replaced with original value after placing the order.', 'woovoucher' );
        $woo_vou_details['billing_city'] 		= __( '{billing_city} - This will be replaced with original value after placing the order.', 'woovoucher' );
        $woo_vou_details['billing_address'] 	= __( '{billing_address} - This will be replaced with original value after placing the order.', 'woovoucher' );

		// order & product related information
		$woo_vou_details['orderid'] 	= __( '{orderid} - This will be replaced with original value after placing the order.', 'woovoucher' );
		$woo_vou_details['orderdate'] 	= __( '{orderdate} - This will be replaced with original value after placing the order.', 'woovoucher' );
		
		$woo_vou_details['payment_method']	= __( '{payment_method} - This will be replaced with original value after placing the order.', 'woovoucher' );

		// Get custom shortcode value here...		
		// $woo_vou_details['custom_shortcode'] = $custom_value;
		$voucher_template_inner_html = woo_vou_replace_all_shortcodes_with_value( $voucher_template_inner_html, $woo_vou_details );				
		  
	  	return $voucher_template_inner_html;
	}

	/**
	 * Adding Hooks
	 * 
	 * Adding proper hoocks for the shortcodes.
	 * 
	 * @package WooCommerce - PDF Vouchers
	 * @since 1.0.0
	 */
	public function add_hooks() {
		
		// Add filter to replace all voucher template shortcodes
		add_filter( 'woo_vou_pdf_template_inner_html', array( $this, 'woo_vou_pdf_template_replace_shortcodes' ), 10, 7 );

		// Add filter to replace all voucher template shortcodes with suitable values
		add_filter( 'woo_vou_pdf_preview_template_inner_html', array( $this, 'woo_vou_pdf_preview_template_replace_shortcodes' ), 10, 4 );
		
		// Add filter to replace all voucher template shortcodes in preview pdf
		add_filter( 'woo_vou_pdf_template_preview_html', array( $this, 'woo_vou_pdf_template_preview_replace_shortcodes' ), 10, 2 );		
	}
}