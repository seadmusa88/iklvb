<?php
/*
Plugin Name: Woo Property check voucher
Plugin URI: http://www.worldwebtechnology.com
Description: Plugin for listing voucher products listing and design
Version: 1.0.0
Author: World Web
Author URI: http://www.worldwebtechnology.com
*/

/**
 * Basic plugin definitions 
 * 
 * @package Woo Property check voucher
 * @since 1.0.0
 */
if( !defined( 'WPW_PCV_DIR' ) ) {
  define( 'WPW_PCV_DIR', dirname( __FILE__ ) );      // Plugin dir
}
if( !defined( 'WPW_PCV_VERSION' ) ) {
  define( 'WPW_PCV_VERSION', '1.0.1' );      // Plugin Version
}
if( !defined( 'WPW_PCV_URL' ) ) {
  define( 'WPW_PCV_URL', plugin_dir_url( __FILE__ ) );   // Plugin url
}
if( !defined( 'WPW_PCV_INC_DIR' ) ) {
  define( 'WPW_PCV_INC_DIR', WPW_PCV_DIR.'/includes' );   // Plugin include dir
}
if( !defined( 'WPW_PCV_INC_URL' ) ) {
  define( 'WPW_PCV_INC_URL', WPW_PCV_URL.'includes' );    // Plugin include url
}
if(!defined('WPW_PCV_PREFIX')) {
  define('WPW_PCV_PREFIX', 'wpw_pcv'); // Plugin Prefix
}
if(!defined('WPW_PCV_VAR_PREFIX')) {
  define('WPW_PCV_VAR_PREFIX', '_wpw_pcv_'); // Variable Prefix
}

// Lorenzo M. 2018-08-27 >>>

// email di invio notifica fattura pagamento generata dal cliente
if(!defined('WPW_PCV_NOTIFY_EMAIL')) {
	define('WPW_PCV_NOTIFY_EMAIL', 'billing@iklvb.com'); // deciso con Luca // 2018-11-10
}

// intervallo di tempo a disposizione per la prima risposta da parte del proprietario dell'immobile
if(!defined('WPW_PCV_TIME_NEXT_MOVE_OWNER_FIRST')) {
	// valore in secondi
	define('WPW_PCV_TIME_NEXT_MOVE_OWNER_FIRST', 72 * 3600); // 3gg
}

// intervallo di tempo a disposizione per le successive risposte da parte del proprietario dell'immobile
if(!defined('WPW_PCV_TIME_NEXT_MOVE_OWNER')) {
	// valore in secondi
	define('WPW_PCV_TIME_NEXT_MOVE_OWNER', 24 * 3600); // 1gg
}

// intervallo di tempo a disposizione per le risposte da parte del cliente (visitatore)
if(!defined('WPW_PCV_TIME_NEXT_MOVE_VISITOR')) {
	// valore in secondi
	define('WPW_PCV_TIME_NEXT_MOVE_VISITOR',  24 * 3600); // 1gg
}

// intervallo di tempo PRIMA dell'incontro 'confirmed' entro le quali NON è più possibile modificare la data di incontro
if(!defined('WPW_PCV_TIME_BEFORE_SCHEDULE')) {
	// valore in secondi
	define('WPW_PCV_TIME_BEFORE_SCHEDULE',  24 * 3600); // 1gg
}

// intervallo di tempo DOPO l'incontro 'confirmed' entro le quali lo status passa a 'performed'
if(!defined('WPW_PCV_TIME_AFTER_SCHEDULE')) {
	// valore in secondi
	define('WPW_PCV_TIME_AFTER_SCHEDULE', 24 * 3600); // 1gg
}

// intervallo di tempo PRIMA della scadenza del biglietto entro le quali notificare la scadenza
// serve per invio notifica email al cliente cha ha acquistato il biglietto
if(!defined('WPW_PCV_TIME_BEFORE_EXPIRE')) {
	// valore in secondi
	define('WPW_PCV_TIME_BEFORE_EXPIRE', 360 * 3600); // 15gg
}

// <<<




/**
 * Load Text Domain
 *
 * This gets the plugin ready for translation.
 *
 * @package Woo Property check voucher
 * @since 1.0.0
 */

// 2018-11-10 Lorenzo M.
// non funziona...
// ho spostato i file nel template child...
//	load_plugin_textdomain( 'wpwpcv', false, dirname( plugin_basename(__FILE__ ) ) . '/languages/' );




/**
 * Activation Hook
 *
 * Register plugin activation hook.
 *
 * @package Woo Property check voucher
 * @since 1.0.0
 */
register_activation_hook( __FILE__, 'wpw_pcv_install' );

function wpw_pcv_install(){
	
}

/**
 * Deactivation Hook
 *
 * Register plugin deactivation hook.
 *
 * @package Woo Property check voucher
 * @since 1.0.0
 */
register_deactivation_hook( __FILE__, 'wpw_pcv_uninstall');

function wpw_pcv_uninstall(){
  
}


function wpw_pcv_register_post_types() {
	
	//social posing logs - post type
	$wpw_pcv_labels = array(
						    'name'				=> __('Owner Invoices',''),
						    'singular_name' 	=> __('Owner Invoice',''),
						    'all_items' 		=> __('Owner Invoices',''),
						    'view_item' 		=> __('View Owner Invoice',''),
						    'search_items' 		=> __('Search Owner Invoices',''),
						    'not_found' 		=> __('No Owner Invoice found',''),
						    'not_found_in_trash'=> __('No Owner Invoice found in Trash',''),
						    'parent_item_colon' => '',
						    'menu_name' 		=> __('Owner Invoices',''),
						);

	$wpw_pcv_args = array(
						    'labels' 				=> $wpw_pcv_labels,
						    'public' 				=> false,
						    'query_var' 			=> false,
						    'rewrite' 				=> false,
						    'capability_type' 		=> 'manage_options',
						    'hierarchical' 			=> false,
					 	); 
	
	//register social posing logs post type
	register_post_type( 'wpw_pcv_invoice', $wpw_pcv_args );
	
}
//register custom post type
add_action( 'init', 'wpw_pcv_register_post_types', 100 );

// Global variables
global $wpw_pcv_scripts, $wpw_pcv_model;

// Script class handles most of script functionalities of plugin
include_once( WPW_PCV_INC_DIR.'/class-wpw-pcv-scripts.php' );
$wpw_pcv_scripts = new Wpw_Pcv_Scripts();
$wpw_pcv_scripts->add_hooks();

// Model class handles most of model functionalities of plugin
include_once( WPW_PCV_INC_DIR.'/class-wpw-pcv-model.php' );
$wpw_pcv_model = new Wpw_Pcv_Model();


include_once( WPW_PCV_INC_DIR.'/class-wpw-pcv-public.php' );
$wpw_pcv_public = new Wpw_Pcv_Public();
$wpw_pcv_public->add_hooks();

// Shortcode class handles most of front end functionalities of plugin
include_once( WPW_PCV_INC_DIR.'/class-wpw-pcv-shortcode.php' );
$wpw_pcv_shortcode = new Wpw_Pcv_Shortcode();
$wpw_pcv_shortcode->add_hooks();

// includo function specifiche per questo plugin // Lorenzo M.
include_once( WPW_PCV_INC_DIR.'/class-wpw-pcv-function.php' );