<?php

add_filter( 'cron_schedules', 'cron_wpw_pcv_add_cron_interval' );
 
function cron_wpw_pcv_add_cron_interval( $schedules ) 
{
    $schedules['ten_minutes'] = array(
        'interval' => 600,
        'display'  => esc_html__( 'Every 10 Minutes' ),
    );
 
    return $schedules;
}









/**
 * Lorenzo M. 2018-08-27
 * 
 * cron per aggiornamento dello status dei biglietti
 * 
 */
add_action('wpw_pcv_voucher_status_update','cron_wpw_pcv_voucher_status_update');

function cron_wpw_pcv_voucher_status_update()
{
	// controllo scadenza biglietti >>>
	// i biglietti NON usati scadono	
	$args = array(
		'post_type'         => 'woovouchercodes',
		'post_status'       => 'publish',
		'posts_per_page'    => -1,
		'order'             => 'DESC',
		'meta_query' => array(
			'relation' => 'OR',
			array(
				'key'       => '_wpw_pcv_status',
				'value'     => 'available',
				'compare'   => '='
			),
			array(
				'key'       => '_wpw_pcv_status',
				'value'     => '',
				'compare'   => '='
			),
		),
	);

	$tickets_results = new WP_Query($args);
	
	if($tickets_results->post_count > 0)
	{
		foreach($tickets_results->posts as $ticket)
		{
			$vou_code					= get_post_meta($ticket->ID, '_woo_vou_purchased_codes', true);
			$vou_exp_date				= get_post_meta($ticket->ID, '_woo_vou_exp_date', true);
			
			$ticket_customer_user_id	= get_post_meta($ticket->ID,'_wpw_pcv_customer_user_id',true);
			$ticket_assign_prop			= get_post_meta($ticket->ID,'_wpw_pcv_assign_prop',true);
			$ticket_assign_prop_user_id = get_post_meta($ticket->ID,'_wpw_pcv_assign_prop_user_id',true);
			$ticket_schedule_time		= get_post_meta($ticket->ID,'_wpw_pcv_schedule_time', true);
			$ticket_action_type			= get_post_meta($ticket->ID,'_wpw_pcv_action_type', true);
			
			if(date('Y-m-d H:i:00') > $vou_exp_date)
			{
				update_post_meta($ticket->ID, '_wpw_pcv_status', 'expired');
				update_post_meta($ticket->ID, '_wpw_pcv_time_update', date('Y-m-d H:i:00'));
				
				/*
				 * NON INVIO NESSUNA NOTIFICA AL CLIENTE: DECISO DA LUCA
				 * 
				// preparo le parti del messaggio		
				$args = array(
					'visitor_id'	=> $ticket_customer_user_id,
					'owner_id'		=> $ticket_assign_prop_user_id,
					'property_id'	=> $ticket_assign_prop,
					'schedule_time'	=> $ticket_schedule_time
				);
	
				// invio email al cliente >>>
				$receiver_email = iklvb_get_email_visitor_from_user_agent_id($ticket_customer_user_id);
				

				// messaggio a seconda del tipo di biglietto
				$email_message_slug = '';
				$iklvb_email = iklvb_get_email_parts($email_message_slug,$args);

				// contenuto email in alto a destra (destinatario)
				global $email_header_right;
				$email_header_right = iklvb_get_user_agent_email_to_box($ticket_customer_user_id);		

				// intestazione
				$headers = array(
					'Content-Type: text/html; charset=UTF-8',
					'From:iKLVB <notification@iklvb.com>'
				);

				// invio della email
				wp_mail($receiver_email, $iklvb_email['subject'], $iklvb_email['message'], $headers);
				
				 * 
				 */
				// <<<
				
				// output for debug
				echo 'Ticket available > expired: '.$vou_code.' '.$vou_exp_date.'<br>';
			}			
		}
	}
	// <<<
	
	
	
	// controllo biglietti assigned che NON vengono confermati >>>
	// se trascorre l'intervallo previsto senza conferma 
	// ritorna 'available'
	$args = array(
		'post_type'         => 'woovouchercodes',
		'post_status'       => 'publish',
		'posts_per_page'    => -1,
		'order'             => 'DESC',
		'meta_query' => array(
			array(
				'key'       => '_wpw_pcv_status',
				'value'     => 'assigned',
				'compare'   => '='
			),
		),
	);

	$tickets_results = new WP_Query($args);
	
	if($tickets_results->post_count > 0)
	{
		foreach($tickets_results->posts as $ticket)
		{
			$vou_code			= get_post_meta($ticket->ID, '_woo_vou_purchased_codes', true);
			$vou_time_update	= get_post_meta($ticket->ID, '_wpw_pcv_time_update', true);
			$vou_next_move		= get_post_meta($ticket->ID, '_wpw_pcv_next_move', true);
			
			$ticket_customer_user_id	= get_post_meta($ticket->ID,'_wpw_pcv_customer_user_id',true);
			$ticket_assign_prop			= get_post_meta($ticket->ID,'_wpw_pcv_assign_prop',true);
			$ticket_assign_prop_user_id = get_post_meta($ticket->ID,'_wpw_pcv_assign_prop_user_id',true);
			$ticket_schedule_time		= get_post_meta($ticket->ID,'_wpw_pcv_schedule_time', true);
			$ticket_action_type			= get_post_meta($ticket->ID,'_wpw_pcv_action_type', true);
			
			if(empty($vou_time_update)) $vou_time_update = '2018-08-01 12:00:00';
			if(empty($vou_next_move)) $vou_next_move = 'owner_first';
			
			if(		( $vou_next_move == 'owner_first'	&& date('Y-m-d H:i:00') > date('Y-m-d H:i:00', strtotime($vou_time_update.' +'.WPW_PCV_TIME_NEXT_MOVE_OWNER_FIRST.' seconds')) )
				|| 	( $vou_next_move == 'owner'			&& date('Y-m-d H:i:00') > date('Y-m-d H:i:00', strtotime($vou_time_update.' +'.WPW_PCV_TIME_NEXT_MOVE_OWNER.' seconds')) )
				|| 	( $vou_next_move == 'visitor'		&& date('Y-m-d H:i:00') > date('Y-m-d H:i:00', strtotime($vou_time_update.' +'.WPW_PCV_TIME_NEXT_MOVE_VISITOR.' seconds')) ) )			
			{
				update_post_meta($ticket->ID, '_wpw_pcv_status', 'available');
				update_post_meta($ticket->ID, '_wpw_pcv_assign_prop', '');
				update_post_meta($ticket->ID, '_wpw_pcv_assign_prop_user_id', '');
				update_post_meta($ticket->ID, '_wpw_pcv_schedule_time', '');
				update_post_meta($ticket->ID, '_wpw_pcv_time_update', date('Y-m-d H:i:00'));
				
				// preparo le parti del messaggio		
				$args = array(
					'visitor_id'	=> $ticket_customer_user_id,
					'owner_id'		=> $ticket_assign_prop_user_id,
					'property_id'	=> $ticket_assign_prop,
					'schedule_time'	=> $ticket_schedule_time
				);
				

				// invio email al cliente >>>
				$receiver_email = iklvb_get_email_visitor_from_user_agent_id($ticket_customer_user_id);

				// messaggio a seconda del tipo di biglietto
				$email_message_slug = ($vou_next_move == 'visitor' ? 'tour-next-move-visitor-expired-cron-to-visitor' : 'tour-next-move-owner-expired-cron-to-visitor');
				$iklvb_email = iklvb_get_email_parts($email_message_slug,$args);

				// contenuto email in alto a destra (destinatario)
				global $email_header_right;
				$email_header_right = iklvb_get_user_agent_email_to_box($ticket_customer_user_id);		

				// intestazione
				$headers = array(
					'Content-Type: text/html; charset=UTF-8',
					'From:iKLVB <notification@iklvb.com>'
				);

				// invio della email
				wp_mail($receiver_email, $iklvb_email['subject'], $iklvb_email['message'], $headers);
				// <<<


				// invio email al proprietario >>>
				$receiver_email = iklvb_get_email_owner_from_property_id($ticket_assign_prop);

				// messaggio a seconda del tipo di biglietto
				$email_message_slug = ($vou_next_move == 'visitor' ? 'tour-next-move-visitor-expired-cron-to-owner' : 'tour-next-move-owner-expired-cron-to-owner');
				$iklvb_email = iklvb_get_email_parts($email_message_slug,$args);

				// contenuto email in alto a destra (destinatario)
				global $email_header_right;
				$email_header_right = iklvb_get_user_agent_email_to_box($ticket_assign_prop_user_id);		

				// intestazione
				$headers = array(
					'Content-Type: text/html; charset=UTF-8',
					'From:iKLVB <notification@iklvb.com>'
				);

				// invio della email
				wp_mail($receiver_email, $iklvb_email['subject'], $iklvb_email['message'], $headers);
				// <<<					
				
								
				// output for debug
				echo 'Ticket assigned > available: '.$vou_code.' '.$vou_time_update.'<br>';
			}
		}
	}
	// <<<
	
	
	// controllo biglietti confirmed >>>
	// prima della data della visita 
	// invio email di promemoria
	$args = array(
		'post_type'         => 'woovouchercodes',
		'post_status'       => 'publish',
		'posts_per_page'    => -1,
		'order'             => 'DESC',
		'meta_query' => array(
			array(
				'key'       => '_wpw_pcv_status',
				'value'     => 'confirmed',
				'compare'   => '='
			),
			array(
				'key'       => '_wpw_pcv_email_notify_sent',
				'value'     => '0',
				'compare'   => '='
			),
		),
	);

	$tickets_results = new WP_Query($args);
	
	if($tickets_results->post_count > 0)
	{
		foreach($tickets_results->posts as $ticket)
		{
			$vou_code					= get_post_meta($ticket->ID, '_woo_vou_purchased_codes', true);
			$vou_schedule_time			= get_post_meta($ticket->ID, '_wpw_pcv_schedule_time', true);
			
			$ticket_customer_user_id	= get_post_meta($ticket->ID,'_wpw_pcv_customer_user_id',true);
			$ticket_assign_prop			= get_post_meta($ticket->ID,'_wpw_pcv_assign_prop',true);
			$ticket_assign_prop_user_id = get_post_meta($ticket->ID,'_wpw_pcv_assign_prop_user_id',true);
			$ticket_schedule_time		= get_post_meta($ticket->ID,'_wpw_pcv_schedule_time', true);
			$ticket_action_type			= get_post_meta($ticket->ID,'_wpw_pcv_action_type', true);
			
			if(date('Y-m-d H:i:00') > date('Y-m-d H:i:00', strtotime($vou_schedule_time.' -'.WPW_PCV_TIME_BEFORE_SCHEDULE.' seconds')) )	
			{
				update_post_meta($ticket->ID, '_wpw_pcv_email_notify_sent', '1');
				
				// preparo le parti del messaggio		
				$args = array(
					'visitor_id'	=> $ticket_customer_user_id,
					'owner_id'		=> $ticket_assign_prop_user_id,
					'property_id'	=> $ticket_assign_prop,
					'schedule_time'	=> $ticket_schedule_time
				);
				
				
				// invio email al cliente >>>
				$receiver_email = iklvb_get_email_visitor_from_user_agent_id($ticket_customer_user_id);
				
				// messaggio a seconda del tipo di biglietto
				$email_message_slug = ($email_message_slug == 'event' ? 'event-near-schedule-date-cron-to-visitor' : 'tour-near-schedule-date-cron-to-visitor');
				$iklvb_email = iklvb_get_email_parts($email_message_slug,$args);

				// contenuto email in alto a destra (destinatario)
				global $email_header_right;
				$email_header_right = iklvb_get_user_agent_email_to_box($ticket_customer_user_id);		

				// intestazione
				$headers = array(
					'Content-Type: text/html; charset=UTF-8',
					'From:iKLVB <notification@iklvb.com>'
				);

				// invio della email
				wp_mail($receiver_email, $iklvb_email['subject'], $iklvb_email['message'], $headers);
				// <<<
				
				
				
				// invio email al proprietario >>>
				$receiver_email = iklvb_get_email_owner_from_property_id($ticket_assign_prop);
				
				// messaggio a seconda del tipo di biglietto
				$email_message_slug = ($email_message_slug == 'event' ? 'event-near-schedule-date-cron-to-owner' : 'tour-near-schedule-date-cron-to-owner');
				$iklvb_email = iklvb_get_email_parts($email_message_slug,$args);

				// contenuto email in alto a destra (destinatario)
				global $email_header_right;
				$email_header_right = iklvb_get_user_agent_email_to_box($ticket_assign_prop_user_id);		

				// intestazione
				$headers = array(
					'Content-Type: text/html; charset=UTF-8',
					'From:iKLVB <notification@iklvb.com>'
				);

				// invio della email
				wp_mail($receiver_email, $iklvb_email['subject'], $iklvb_email['message'], $headers);
				// <<<
				
				
				
				// output for debug
				echo 'Ticket near schedule notity sent: '.$vou_code.' '.$vou_schedule_time.'<br>';
			}
		}
	}
	// <<<
	
	
	// controllo biglietti confirmed >>>
	// dopo la data della visita 
	// considero i biglietti 'performed'
	$args = array(
		'post_type'         => 'woovouchercodes',
		'post_status'       => 'publish',
		'posts_per_page'    => -1,
		'order'             => 'DESC',
		'meta_query' => array(
			array(
				'key'       => '_wpw_pcv_status',
				'value'     => 'confirmed',
				'compare'   => '='
			),
		),
	);

	$tickets_results = new WP_Query($args);
	
	if($tickets_results->post_count > 0)
	{
		foreach($tickets_results->posts as $ticket)
		{
			$vou_code					= get_post_meta($ticket->ID, '_woo_vou_purchased_codes', true);
			$vou_schedule_time			= get_post_meta($ticket->ID, '_wpw_pcv_schedule_time', true);
			
			$ticket_customer_user_id	= get_post_meta($ticket->ID,'_wpw_pcv_customer_user_id',true);
			$ticket_assign_prop			= get_post_meta($ticket->ID,'_wpw_pcv_assign_prop',true);
			$ticket_assign_prop_user_id = get_post_meta($ticket->ID,'_wpw_pcv_assign_prop_user_id',true);
			$ticket_schedule_time		= get_post_meta($ticket->ID,'_wpw_pcv_schedule_time', true);
			$ticket_action_type			= get_post_meta($ticket->ID,'_wpw_pcv_action_type', true);

			if(date('Y-m-d H:i:00') > date('Y-m-d H:i:00', strtotime($vou_schedule_time.' +'.WPW_PCV_TIME_AFTER_SCHEDULE.' seconds')) )
			{
				update_post_meta($ticket->ID, '_wpw_pcv_status', 'performed');
				update_post_meta($ticket->ID, '_wpw_pcv_time_update', date('Y-m-d H:i:00'));
				
				// preparo le parti del messaggio		
				$args = array(
					'visitor_id'	=> $ticket_customer_user_id,
					'owner_id'		=> $ticket_assign_prop_user_id,
					'property_id'	=> $ticket_assign_prop,
					'schedule_time'	=> $ticket_schedule_time
				);
				
				
				// invio email al cliente >>>
				$receiver_email = iklvb_get_email_visitor_from_user_agent_id($ticket_customer_user_id);
				
				// messaggio a seconda del tipo di biglietto
				$email_message_slug = ($ticket_action_type == 'event' ? 'event-review-request-cron-to-visitor' : 'tour-review-request-cron-to-visitor');
				$iklvb_email = iklvb_get_email_parts($email_message_slug,$args);

				// contenuto email in alto a destra (destinatario)
				global $email_header_right;
				$email_header_right = iklvb_get_user_agent_email_to_box($ticket_customer_user_id);		

				// intestazione
				$headers = array(
					'Content-Type: text/html; charset=UTF-8',
					'From:iKLVB <notification@iklvb.com>'
				);

				// invio della email
				wp_mail($receiver_email, $iklvb_email['subject'], $iklvb_email['message'], $headers);
				// <<<
				
				
				
				// invio email al proprietario >>>
				$receiver_email = iklvb_get_email_owner_from_property_id($ticket_assign_prop);

				// messaggio a seconda del tipo di biglietto
				$email_message_slug = ($ticket_action_type == 'event' ? 'event-performed-cron-to-owner' : 'tour-performed-cron-to-owner');
				$iklvb_email = iklvb_get_email_parts($email_message_slug,$args);

				// contenuto email in alto a destra (destinatario)
				global $email_header_right;
				$email_header_right = iklvb_get_user_agent_email_to_box($ticket_assign_prop_user_id);		

				// intestazione
				$headers = array(
					'Content-Type: text/html; charset=UTF-8',
					'From:iKLVB <notification@iklvb.com>'
				);

				// invio della email
				wp_mail($receiver_email, $iklvb_email['subject'], $iklvb_email['message'], $headers);
				// <<<
				
				
				// output for debug
				echo 'Ticket confirmed > performed: '.$vou_code.' '.$vou_schedule_time.'<br>';
			}
		}
	}
	// <<<
	
}






/**
 * Lorenzo M. 2018-08-27
 * 
 * cron per invio notifica email di avviso scadenza dei biglietti non utilizzati
 * 
 */
add_action('wpw_pcv_voucher_expire_notify','cron_wpw_pcv_voucher_expire_notify');

function cron_wpw_pcv_voucher_expire_notify()
{
	// controllo biglietti che stanno per scadere >>>
	// se mi avvicino alla data di scadenza
	// invio una email di notifica
	$args = array(
		'post_type'         => 'woovouchercodes',
		'post_status'       => 'publish',
		'posts_per_page'    => -1,
		'order'             => 'DESC',
		'meta_query' => array(
			array(
				'key'       => '_wpw_pcv_status',
				'value'     => 'available',
				'compare'   => '='
			),
			array(
				'key'       => '_wpw_pcv_email_notify_sent',
				'value'     => '0',
				'compare'   => '='
			),
		),
	);

	$tickets_results = new WP_Query($args);
	
	if($tickets_results->post_count > 0)
	{
		foreach($tickets_results->posts as $ticket)
		{
			$vou_code		= get_post_meta($ticket->ID, '_woo_vou_purchased_codes', true);
			$vou_exp_date	= get_post_meta($ticket->ID, '_woo_vou_exp_date', true);
			
			$ticket_customer_user_id	= get_post_meta($ticket->ID,'_wpw_pcv_customer_user_id',true);
			$ticket_assign_prop			= get_post_meta($ticket->ID,'_wpw_pcv_assign_prop',true);
			$ticket_assign_prop_user_id = get_post_meta($ticket->ID,'_wpw_pcv_assign_prop_user_id',true);
			$ticket_schedule_time		= get_post_meta($ticket->ID,'_wpw_pcv_schedule_time', true);
			$ticket_action_type			= get_post_meta($ticket->ID,'_wpw_pcv_action_type', true);
			
			if(date('Y-m-d H:i:00') > date('Y-m-d H:i:00', strtotime($vou_exp_date.' -'.WPW_PCV_TIME_AFTER_SCHEDULE.' seconds')) )		
			{
				update_post_meta($ticket->ID, '_wpw_pcv_email_notify_sent', '1');
				update_post_meta($ticket->ID, '_wpw_pcv_time_update', date('Y-m-d H:i:00')); // tengo traccia della data di invio notifica
				
				// preparo le parti del messaggio		
				$args = array(
					'visitor_id'	=> $ticket_customer_user_id,
					'owner_id'		=> $ticket_assign_prop_user_id,
					'property_id'	=> $ticket_assign_prop,
					'schedule_time'	=> $ticket_schedule_time
				);
				
				
				// invio email al cliente >>>
				$receiver_email = iklvb_get_email_visitor_from_user_agent_id($ticket_customer_user_id);
				
				// messaggio a seconda del tipo di biglietto
				$email_message_slug = ($ticket_action_type == 'event' ? 'event-near-expire-cron-to-visitor' : 'tour-near-expire-cron-to-visitor');
				$iklvb_email = iklvb_get_email_parts($email_message_slug,$args);

				// contenuto email in alto a destra (destinatario)
				global $email_header_right;
				$email_header_right = iklvb_get_user_agent_email_to_box($ticket_customer_user_id);		

				// intestazione
				$headers = array(
					'Content-Type: text/html; charset=UTF-8',
					'From:iKLVB <notification@iklvb.com>'
				);

				// invio della email
				wp_mail($receiver_email, $iklvb_email['subject'], $iklvb_email['message'], $headers);
				// <<<
				
				// output for debug
				echo 'Ticket expire notity sent: '.$vou_code.' '.$vou_exp_date.'<br>';
			}
		}
	}
	// <<<
	
}



// controllo la presenza dei cron e se mancano li aggiungo 
if(!wp_next_scheduled( 'wpw_pcv_voucher_status_update')) wp_schedule_event(time(),'ten_minutes','wpw_pcv_voucher_status_update');
if(!wp_next_scheduled( 'wpw_pcv_voucher_expire_notify')) wp_schedule_event(time(),'daily',		'wpw_pcv_voucher_expire_notify');









/**
 * Lorenzo M. 2018-08-28
 * 
 * operazione una-tantum 
 * serve per update campi post_meta dei biglietti
 */
/*
function wpw_pvp_update_custom_post_meta()
{
	$args = array(
		'post_type'         => 'woovouchercodes',
		'post_status'       => 'publish',
		'posts_per_page'    => -1,
		'order'             => 'DESC',
	);
	
	$tickets_results = new WP_Query($args);
	
	if($tickets_results->post_count > 0)
	{
		foreach($tickets_results->posts as $ticket)
		{
			$vou_status = get_post_meta($ticket->ID, '_wpw_pcv_status', true);
			
			if($vou_status == 'available')
			{
				$vou_assign_prop = get_post_meta($ticket->ID, '_wpw_pcv_assign_prop', true);
				if(!empty($vou_assign_prop)) update_post_meta($ticket->ID,'_wpw_pcv_assign_prop','');
				
				$vou_assign_prop_user_id = get_post_meta($ticket->ID, '_wpw_pcv_assign_prop_user_id', true);
				if(!empty($vou_assign_prop_user_id)) update_post_meta($ticket->ID,'_wpw_pcv_assign_prop_user_id','');
			}
			
			

			// delete old test tickets 
			$vou_action_type = get_post_meta($ticket->ID, '_wpw_pcv_action_type', true);
			
			if($vou_action_type != 'for_sale' && $vou_action_type != 'for_rent' && $vou_action_type != 'event')
			{
				wp_delete_post($ticket->ID);
			}
			
			
			// _wpw_pcv_assign_prop_agent => _wpw_pcv_assign_prop_user_id
			$vou_assign_prop_agent = get_post_meta($ticket->ID, '_wpw_pcv_assign_prop_agent', true);
			
			if(is_numeric($vou_assign_prop_agent))
			{
				$vou_assign_prop_user_id = get_post_meta($vou_assign_prop_agent, 'user_meda_id', true);
				
				if(is_numeric($vou_assign_prop_user_id))
				{
					update_post_meta($ticket->ID, '_wpw_pcv_assign_prop_user_id', $vou_assign_prop_user_id);
				}
				else
				{
					$vou_assign_prop_id = get_post_meta($ticket->ID, '_wpw_pcv_assign_prop', true);
					$property_user_id = get_post_meta($vou_assign_prop_id, 'property_user', true);
					if(empty($property_user_id)) $property_user_id = get_post_meta($vou_assign_prop_id, 'original_author', true);
					
					if(is_numeric($property_user_id))
					{
						update_post_meta($ticket->ID, '_wpw_pcv_assign_prop_user_id', $property_user_id);
					}
				}
			}
			
			$vou_assign_prop_user_id = get_post_meta($ticket->ID, '_wpw_pcv_assign_prop_user_id', true);
			
			if(is_numeric($vou_assign_prop_user_id))
			{
				delete_post_meta($ticket->ID, '_wpw_pcv_assign_prop_agent');
			}
			
			
			// _wpw_pcv_customer_id => _wpw_pcv_customer_user_id
			$vou_customer_id = get_post_meta($ticket->ID, '_wpw_pcv_customer_id', true);
			
			if(is_numeric($vou_customer_id))
			{
				update_post_meta($ticket->ID, '_wpw_pcv_customer_user_id', $vou_customer_id);
			}
			
			$vou_customer_user_id = get_post_meta($ticket->ID, '_wpw_pcv_customer_user_id', true);
			
			if(is_numeric($vou_customer_user_id))
			{
				delete_post_meta($ticket->ID, '_wpw_pcv_customer_id');
			}
		}	
	}
}
/* --- */