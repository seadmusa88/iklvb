jQuery(document).ready(function ($)
{
	// controllo se nella pagina ci sono dei campi input da gestire con jQuery.datepicker
	$('#schedule_day_custom').each( function() 
	{
		var date_format = $(this).attr('date-format');
		if(date_format === '') date_format = 'yy-mm-dd';
		
		$(this).datepicker( 
		{
			dateFormat: date_format,
			minDate: new Date( $( this ).attr( 'min' ) ),
			maxDate: new Date( $( this ).attr( 'max' ) )
		}).datepicker('widget').wrap('<div class="ll-skin-melon"/>');
	});
	
	
	
	



	// gestione form di richiesta visita in pagina "property" >>>
	$('.submit_property_booking').click(function(e)
	{
		e.stopImmediatePropagation();
		e.preventDefault();
		
		// nascondo eventuali messaggi visualizzati in precedenza
		$('.alert-ok').hide();
		$('.alert-error').hide();
		
		// visualizzo il loader per far capire che la pagina sta lavorando
		$('#image-loader').show();
		
		// chiamata ajax
        $.ajax(
		{
            type	: 'POST',
			dataType: 'json',
			url		: WPWPCV.ajaxurl,
            data: {
                action			:   'wpestate_ajax_property_booking',
                property_id		:   $('#agent_property_id').val(),
                schedule_day	:   $('#schedule_day_custom').val(),
                schedule_hour	:   $('#schedule_hour').val(),
				ticket_number	:	$('#ticket_number').val(),
                nonce			:   $('#agent_property_ajax_nonce').val()
            },
            success: function(response) 
			{ 				
                if(response.type === 'success') 
				{
					// empty
                    $('#schedule_day_custom').val('');
                    $('#schedule_hour').val('');
                    $('#ticket_number').val('');
					
					// hide
					$('.agent_contanct_form .row.row-fields').hide();
					
					// show
					$('#message_ok_json').html(response.message).show();
					$('#image-confirm').show();
					$('.agent_contanct_form .row.row-confirm').show();
                }
				else if(response.type === 'error')
				{
					$('#message_error_json').html(response.message).show();
					if(response.focus === 'schedule_hour') $('#schedule_hour').focus(); 
					if(response.focus === 'ticket_number') $('#ticket_number').focus(); 
				}
				
				$('#image-loader').hide();
		    },
            error: function() 
			{
				$('#message_error_default').show();				
				$('#image-loader').hide();
            }
        });
    });





	/*
	$('.message_submit').click(function(e)
	{
		ticket_number = $('#ticket_number').val();
		$('.alert-message').text(''); // Lorenzo M. 2018-08-09

		if( ticket_number === '' ) 
		{
			$('#ticket_number').css({'border':'1px solid red'});
			$('#ticket_number').focus();
			// $('.alert-box').addClass('error');	// Lorenzo M. 2018-08-09
	        $('.alert-message').text('Enter your ticket number');
			valid = false;
		}
		else
		{
			$('#ticket_number').css({'border':''});
			valid = true;
		}

		if( !valid ) 
		{
			e.stopImmediatePropagation();
			e.preventDefault();
			return false;
		}

		if( ticket_number !== '' && !ticketvalid ) 
		{
			e.stopImmediatePropagation();
			e.preventDefault();
			// check ticket num is valid or not
			$.ajax(
			{ 
				type: 'POST', url: WPWPCV.ajaxurl,
	            data: 
				{
	                'action':'woo_vou_check_voucher_code',
	                'voucode':ticket_number,
	                'ajax':true
	            }, success: function (data) 
				{
	            	var response = $.parseJSON(data);
	            	if( response.error ) 
					{
	            		ticketvalid = false;
	           			valid = false;
	           			// $('.alert-box').addClass('error');	// Lorenzo M. 2018-08-09
	           			$('.alert-message').text( response.error);
	            	} 
					else
					{
	            		$.ajax(
						{ 
							type: 'POST', url: WPWPCV.ajaxurl,
							data: 
							{
								'action':'wpw_pcv_add_ticket_property',
								'operation':'add_property',
								'ticket_number':ticket_number,
								'prop_id':WPWPCV.post_id
							}, success: function (ticket_res ) 
							{ 
								// check ticket not assigned
								if( ticket_res.type == 'success') 
								{
									// $('.alert-box').removeClass('error');	// Lorenzo M. 2018-08-09
									$('.alert-message').text('');
									ticketvalid = true;
									valid = true;
									$('.message_submit').trigger('click');
								}
								else
								{
									// $('.alert-box').addClass('error');	// Lorenzo M. 2018-08-09
									$('.alert-message').text(ticket_res.message);
									ticketvalid = false;
									valid = true;
								}
							}
		           		}); 		
	            	} 
	            }
	        });
		}
	});
	*/






	jQuery(document).on( 'click', '.wpw-pcv-products-wrap .wpw-pcv-tab-title a', function()
	{
		var currentAttrValue = jQuery(this).attr('data-id');
		
		// Show/Hide Tabs
		jQuery('.wpw-pcv-products-wrap '+ currentAttrValue).show().siblings().hide();

		// Change/remove current tab to active
		jQuery(this).addClass('active').siblings().removeClass('active');
		equalheight('.wpw-pcv-products-container li');
		//e.preventDefault();
	});




	var stepform = $('#ticket-form');
	var voucher_price = 0;

	// $('#ticket_number').focus();
	

	stepform.validate(
	{
	    errorPlacement: function errorPlacement(error, element) { element.before(error); },
	    onkeyup: false,
	    onkeypress: false,
		onfocus:false,
		onfocusout:false,
	    rules: 
		{
	        'ticket_number[]': 
			{
	        	required: true,
	        	validTicket:true
	        },
	        company_place: 
			{
	        	required: true,
	        	lettersonly: true
	        },
	        company_nation: 
			{
	        	required: true,
	        	lettersonly: true
	        },
	        percentage_of_tax: 
			{
		      max: 100,
		      min: 0
		    },
		    invoice_number:
			{
		    	maxlength: 6,
		    	required: true
			}
	    }
	});

	$.validator.addMethod("lettersonly", function(value, element) 
	{
		return this.optional(element) || /^[a-z]+$/i.test(value);
	}, "Please enter only letters."); 

	$.validator.addMethod("validTicket", function(value, element) 
	{
		var validTicket = false;
		var element_id = element.id;
		var existing = 0;

		$('input.ticket_number').each( function()
		{
			if( $(this).val() === value)
			{
				existing = existing + 1;
			}
		});

		if( existing <= 1 ) 
		{
		  	$.ajax(
			{ 
				type: 'POST', 
				url: WPWPCV.ajaxurl,
				async: false,
	            data: 
				{
	                action			:	'wpw_pcv_ticket_validation',
	                ticket_number	:	value
	            }, 
				success: function (valid_res ) 
				{ 
					// check ticket not assigned
					if( valid_res.type === 'success') 
					{	
						if( ! $('#'+element_id).hasClass('added-price') ) 
						{
							$('#'+element_id).addClass('added-price');
							$('#'+element_id).attr('data-price', valid_res.price );
							$('#'+element_id).attr('data-ticket', value );	
							voucher_price = parseFloat(voucher_price) + parseFloat(valid_res.price);
						}
						validTicket = true;
						$('#calculate-price').text( formatUSD(voucher_price) );
	           		} 
					else
					{
	           			var check = $('#'+element_id).attr('data-price');
	           			if( typeof check !== 'undefined' && check !== false ) 
						{
	           				var check_price = $('#'+element_id).data('price');
	           				voucher_price = parseFloat(voucher_price) - parseFloat(check_price);
	           				$('#calculate-price').text( formatUSD(voucher_price) );
	           			}
	           			$('#'+element_id).removeClass('added-price');
						$('#'+element_id).removeAttr('data-price');
						$('#'+element_id).removeAttr('data-ticket');
						validTicket = false;
	           		}
	       		}
			});
		}

		return validTicket;
	}, "Please provide correct ticket number");

	stepform.steps(
	{
	    headerTag: "h3",
	    bodyTag: "section",
	    transitionEffect: "slideLeft",
	    labels: {
		    finish: 'Confirmation',
			next: 'Continue'
			// prev: 'Back'
    	},
    	onInit: function (event, current) 
		{
			$('.steps ul span.number').each( function(){
    			$(this).text( $(this).html().replace('.', ''));
    		});
			
			// $('.actions > ul > li:first-child').hide();	
			$('.actions > ul > li').hide();	

			jQuery(document).on('click', '#verify-tickets', function()
			{	
				stepform.validate();
			});
		
	    },
	    onStepChanging: function (event, currentIndex, newIndex)
	    {	
			var nextStep = '#wizard #wizard-p-' + currentIndex;
		    var totalHeight = 0;
		    var qty			= 0;

			// console.log(currentIndex);
			
		    // if( newIndex < currentIndex ) return false;

		    qty = $('input[name="ticket_number[]"].added-price').length;
		    $('#invoice_quantity').text(qty);
		    var tax_percent = $('#percentage_of_tax').val();

			/*
		    if( currentIndex == '0' )
			{
				$('.actions > ul > li:first-child').hide();			
			}	
				
		    if( currentIndex == '1' )
			{
				$('.actions > ul > li:first-child').show();			
			}	
			*/
		   
		    if( currentIndex === '2' )
			{
				// $('.actions > ul > li:first-child').show();
				
		    	$('tr.dynamic-item').remove();
		    	$('.ticket_number.added-price').each( function()
				{
		    		var check_price = $(this).attr('data-price');
		    		if( typeof check_price !== 'undefined' && check_price !== false )
					{
		    			inv_item = $('tr#invoice_items').after($('tr#invoice_items').clone());
		    			inv_item.removeAttr('id');
		    			inv_item.removeAttr('style');
		    			inv_item.attr('class', 'dynamic-item');
		    			var price = $(this).data('price');
		    			if( tax_percent != '' && typeof tax_percent !== 'undefined' )
						{
		    				tax_amount = ( price * tax_percent ) / 100;
		    				price = price - tax_amount;
		    			}

		    			inv_item.find('.inv-price').text('USD '+formatUSD(price) ) ;
		    			inv_item.find('.inv-total-price').text('USD '+formatUSD(price));
		    		}
		    	});

		    	
		    	var tax_amount  		= 0;
		    	var total_invoice_amt 	= 0;
		    	var ritenuta_amount 	= 0;
		    	var inv_amount_to_pay 	= 0;
		    	var stamp_amount		= 0;
		    	var d_tax_amount 		= 0; 

		    	$('input[name="ticket_number[]"].added-price').each( function()
				{
		    		$('#ticket-form').append('<input type="hidden" name="f_ticket_amount[]" value="'+$(this).data('price')+'">');
		    	});

		    	if( tax_percent !== '' && typeof tax_percent !== 'undefined' )
				{
		    		tax_amount = ( voucher_price * tax_percent ) / 100;
		    		$('#f_tax_amount').val(tax_amount);
		    		d_tax_amount = formatUSD(tax_amount);
		    		$('#p-user-tax').show();
		    	} 
				else
				{
		    		$('#p-user-tax').hide();
		    	}

		    	$('#inv-total-amount').text('USD '+formatUSD( voucher_price - tax_amount ) );
		    	$('#f_invoice_amt').val(voucher_price - tax_amount );

		    	if( tax_amount > 0 )
				{
		    		// total_invoice_amt = voucher_price + tax_amount;
		    		total_invoice_amt = voucher_price;
		    	}
				else
				{
		    		total_invoice_amt = voucher_price;
		    	}

		    	if( $('input[name="indentity"]:checked').val() === 'professional' && total_invoice_amt > 0 )
				{
		    		ritenuta_amount = ( total_invoice_amt * 20 ) / 100;
		    	}

		    	if( total_invoice_amt >= 77.47 ){
		    		stamp_amount = 2;
		    	}

				inv_amount_to_pay = total_invoice_amt + ritenuta_amount + stamp_amount;
				$('#f_amount_to_pay').val(inv_amount_to_pay);
				$('#inv-total-to-pay').text('USD '+ formatUSD(inv_amount_to_pay));

				$('#f_stamp_amount').val(stamp_amount);
				$('#inv-stamp-amount').text('USD '+ formatUSD(stamp_amount));
		    	
		    	$('#f_ritenuta_amount').val(ritenuta_amount);

		    	ritenuta_amount = formatUSD(ritenuta_amount);
		    	
		    	$('#inv-ritenuta-amount').text('USD '+ritenuta_amount);

		    	if( total_invoice_amt > 0 ) {
		    		$('#f_total_invoice_amt').val(total_invoice_amt);
		    		total_invoice_amt = formatUSD(total_invoice_amt);
		    	}

		    	$('#inv-tax-amount').text('USD '+d_tax_amount);
		    	$('#inv-cal-total-amount').text('USD '+total_invoice_amt);

		    	var place = $('#company_postal_code').val();
		    	if( place !== '')
		    		place = place + ' ';
		    	place += $('#company_place').val();

		    	var tax = $('#tax_name').val();
		    	if( tax !== '')
		    		tax = tax + ' ';

		    	tax += $('#tax_number').val();

		    	var apply_tax = $('#apply_tax_name').val();
		    	if( apply_tax !== '')
		    		apply_tax = 'Tax( '+apply_tax + ' ';

		    	if( tax_percent !== '' && typeof tax_percent !== 'undefined' )
		    		apply_tax += tax_percent+'% )';

		    	$('#p-company_name').text($('#company_name').val());
		    	$('#p-company_addr1').text($('#company_address').val());
		    	$('#p-company_addr2').text(place);
		    	$('#p-company_country').text($('#company_nation').val());
		    	$('#p-vat-number').text(tax);
		    	$('#p-email').text($('#company_email').val());
		    	$('#p-email').text($('#company_email').val());
		    	$('#p-invoice-number').text($('#invoice_number').val());
		    	$('#p-user-tax').text(apply_tax);
		    	$('#user-bank').text($('#bank_account').val());
		    	$('#bank-acc-number').text($('#bank_account_number').val());
		    	$('.actions > ul > li:nth-child(2) a').text('Confirmation');
			}

			if( currentIndex === '3' )
			{
				 $.ajax(
				{ 
					type: 'POST', url: WPWPCV.ajaxurl,async: false,
		            data: 
					{
		                'action':'wpw_pcv_submit_ticket_form',
		                'ticket_data':stepform.serialize(),
		            }, success: function (valid_res ) 
					{ 
		            	// $('body').html(valid_res);
		            	if( valid_res.type == 'success') 
						{
		            		return true;
		            	}
						else
						{
		            		alert('Something went wrong, please try again!');
		            		return false;
		            	}
		            }
		        });
				$('.actions > ul').attr('style', 'display:none');
			}
			
		    return stepform.valid();
	    },
		
	    onFinishing: function (event, currentIndex)
	    {	
	        stepform.validate().settings.ignore = ":disabled";
	        return stepform.valid();
	    },
	    onFinished: function (event, currentIndex)
		{
	        alert('Submitted');
	    }
	});
	
	$('[data-toggle="tooltip"]').tooltip();

	$('#add-more-ticket').click( function()
	{
		var cnt = $('#dynamic-ticket input').length;
		cnt = cnt + 1;

		var ticketObj = $('#ticket_number').clone(true).prop('id', 'ticket_number'+cnt ).appendTo('#dynamic-ticket');
		ticketObj.val('');
		ticketObj.removeClass('added-price');
		ticketObj.removeAttr('data-price');
		ticketObj.removeAttr('data-ticket');
		ticketObj.removeClass('error');
		ticketObj.rules('add', 
		{
	        validTicket:true
		});
	});

	function formatUSD( numbr )
	{
		numbr = parseFloat(numbr);
		numbr = numbr.toFixed(2).replace(/./g, function(c, i, a) 
		{
		    return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
		});
		return numbr;
	}


	/** equal height*/
	equalheight = function(container)
	{
		var currentTallest = 0,
		     currentRowStart = 0,
		     rowDivs = new Array(),
		     $el,
		     topPosition = 0;
	 
		$(container).each(function() 
		{
			$el = $(this);
			$($el).height('auto');
			topPostion = $el.position().top;

			if (currentRowStart !== topPostion) 
			{   
				for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) 
				{
					rowDivs[currentDiv].height(currentTallest);
				}
				rowDivs.length = 0; // empty the array
				currentRowStart = topPostion;
				currentTallest = $el.height();
				rowDivs.push($el);
			}
			else 
			{
				rowDivs.push($el);
				currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
			}
		   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) 
		   {
			rowDivs[currentDiv].height(currentTallest);
		   }
		});
	};


	$(window).load(function() 
	{
		equalheight('.wpw-pcv-products-container li');
	});


	$(window).resize(function()
	{
		equalheight('.wpw-pcv-products-container li');
	});
	
	

	
	
	
});
