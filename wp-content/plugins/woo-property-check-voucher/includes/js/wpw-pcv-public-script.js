jQuery(document).ready(function ($)
{
	// controllo se nella pagina ci sono dei campi input da gestire con jQuery.datepicker
	$('#schedule_day_custom').each( function() 
	{
		var date_format = $(this).attr('date-format');
		if(date_format === '') date_format = 'yy-mm-dd';
		
		$(this).datepicker( 
		{
			dateFormat: date_format,
			minDate: new Date( $( this ).attr( 'min' ) ),
			maxDate: new Date( $( this ).attr( 'max' ) )
		}).datepicker('widget').wrap('<div class="ll-skin-melon"/>');
	});
	
	
	
	



	// gestione form di richiesta visita in pagina "property" >>>
	$('.submit_property_booking').click(function(e)
	{
		e.stopImmediatePropagation();
		e.preventDefault();
		
		// nascondo eventuali messaggi visualizzati in precedenza
		$('.alert-ok').hide();
		$('.alert-error').hide();
		
		// visualizzo il loader per far capire che la pagina sta lavorando
		$('#image-loader').show();
		
		// chiamata ajax
        $.ajax(
		{
            type	: 'POST',
			dataType: 'json',
			url		: WPWPCV.ajaxurl,
            data: {
                action			:   'wpestate_ajax_property_booking',
                property_id		:   $('#agent_property_id').val(),
                schedule_day	:   $('#schedule_day_custom').val(),
                schedule_hour	:   $('#schedule_hour').val(),
				ticket_number	:	$('#ticket_number').val(),
                nonce			:   $('#agent_property_ajax_nonce').val()
            },
            success: function(response) 
			{ 				
                if(response.type === 'success') 
				{
					// empty
                    $('#schedule_day_custom').val('');
                    $('#schedule_hour').val('');
                    $('#ticket_number').val('');
					
					// hide
					$('.agent_contanct_form .row.row-fields').hide();
					
					// show
					$('#message_ok_json').html(response.message).show();
					$('#image-confirm').show();
					$('.agent_contanct_form .row.row-confirm').show();
                }
				else if(response.type === 'error')
				{
					$('#message_error_json').html(response.message).show();
					if(response.focus === 'schedule_hour') $('#schedule_hour').focus(); 
					if(response.focus === 'ticket_number') $('#ticket_number').focus(); 
				}
				
				$('#image-loader').hide();
		    },
            error: function() 
			{
				$('#message_error_default').show();				
				$('#image-loader').hide();
            }
        });
    });

























	/**
	 * Parte relativa al form della pagina cash-in
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */


	// init
	var stepform = $('#ticket-form');
	var total_voucher_price = 0;
	var tickets_processed = [];
	var tickets_quantity = 0;
	var apply_tax_percentage = 0;
	var invoice_description = '';
	
	
	
	jQuery(document).on( 'click', '.wpw-pcv-products-wrap .wpw-pcv-tab-title a', function()
	{
		var currentAttrValue = jQuery(this).attr('data-id');
		
		// Show/Hide Tabs
		jQuery('.wpw-pcv-products-wrap '+ currentAttrValue).show().siblings().hide();

		// Change/remove current tab to active
		jQuery(this).addClass('active').siblings().removeClass('active');
		equalheight('.wpw-pcv-products-container li');
	});





	// $('#ticket_number').focus();
	
	stepform.validate(
	{
	    errorPlacement: function errorPlacement(error, element) { element.before(error); },
	    onkeyup: false,
	    onkeypress: false,
		onfocus: false,
		onfocusout: false,		
	    rules: 
		{	
	        billing_city: 
			{
	        	required: true,
	        	lettersonly: true
	        },
	        billing_nation: 
			{
	        	required: true,
	        	lettersonly: true
	        },
	        apply_tax_percentage: 
			{
		      max: 100,
		      min: 0
		    },
		    invoice_number:
			{
		    	maxlength: 6,
		    	required: true
			}
	    }
	});
	
   
	$.validator.addMethod("lettersonly", function(value, element) 
	{
		return this.optional(element) || /^[a-z]+$/i.test(value);
	}, "Please enter only letters."); 



	function reset_ticket_number_field(element_id)
	{
		$('#'+element_id).removeClass('error');
		$('#'+element_id).removeClass('valid');
		$('label.error').hide();
	}



	
	jQuery(document).on('keydown', 'input.ticket_number', function()
	{
		// init
		var element_id = $(this).attr('id');
		
		// stile campi input a valore normale
		reset_ticket_number_field(element_id);		
	});

	
		
	
	jQuery(document).on('click', '#verify-tickets', function()
	{
		// init
		var element_id = $('input.ticket_number').attr('id');
		var ticket_number = $('input.ticket_number').val();

		// se il campo è vuoto non faccio niente
		if( ticket_number === '') return;
		
		// se il valore di questo ticket è già stato inserito non faccio niente
		if($.inArray( ticket_number, tickets_processed ) !== -1)
		{
			$('#'+element_id).addClass('error');
			$('#ticket-already-entered').show();
			return;			
		}
		

		//
		$('#image-loader-cash-in').show();
		reset_ticket_number_field(element_id);
		$('.actions > ul > li').hide();



		$.ajax(
		{ 
			type: 'POST', 
			url: WPWPCV.ajaxurl,
			data: 
			{
				action			: 'wpw_pcv_ticket_validation',
				ticket_number	: ticket_number,
				ajax			: true
			}, 
			success: function (valid_res ) 
			{ 						
				// check ticket as ok
				if( valid_res.type === 'success') 
				{	
					// metto in memoria il ticket inserito per evitare che venga incassato più volte
					tickets_processed.push(ticket_number);
					
					// inserisco i numero dei biglietti in un campo nascosto per l'invio finale del form
					$('#tickets_numbers').val(tickets_processed);
					
					// aggiorno il totale da incassare
					total_voucher_price = parseFloat(total_voucher_price) + (parseFloat(valid_res.price_cash_in)); 
					$('#calculate-price').text( formatEUR(total_voucher_price) );

					// visualizzo il campo in verde
					$('#'+element_id).addClass('valid');

					// ajax load info html del biglietto
					$.ajax(
					{
						type	: 'POST',
						dataType: 'html',
						url		: WPWPCV.ajaxurl,
						data: {
							action		: 'wpestate_ajax_update_ticket_cash_in',
							ticket_id	: valid_res.id
						},
						success: function(response) 
						{ 				
							$('#dynamic-ticket').append(response);
							reset_ticket_number_field(element_id);
							
							$('#'+element_id).val('');
							$('#image-loader-cash-in').hide();
							$('.actions > ul > li:nth-child(2)').show();
							
							invoice_description += ticket_number + ' - ' + valid_res.product_title + '<br>';
							$('#invoice_description').val(invoice_description);
							$('#inv-description').html(invoice_description);
							
							tickets_quantity = tickets_processed.length;
							$('#invoice_quantity').val(tickets_quantity);
							$('#inv-quantity').text(tickets_quantity);
						},
						error: function() 
						{
							// niente
						}	
					});

				} 
				// check ticket is not valid (used, not assigned, not valid, etc...)
				else
				{
					$('#'+element_id).addClass('error');
					$('#ticket-not-valid').show();
					$('#image-loader-cash-in').hide();
					
					if(tickets_processed.length > 0) 
					{
						// visualizzo il bottone procedi
						$('.actions > ul > li:nth-child(2)').show();
					}
				}
			}
		});
	});
	
	
	
	

	
	
	

	stepform.steps(
	{
	    headerTag: "h3",
	    bodyTag: "section",
	    transitionEffect: "slideLeft",
		
	    labels: {
			previous: $('#button_label_previous').val(),
			next: $('#button_label_next').val(),
		    finish: $('#button_label_finish').val()
    	},
		
    	onInit: function (event, current) 
		{
    		$('.steps ul span.number').each( function(){
    			$(this).text( $(this).html().replace('.', ''));
    		});

			$('.actions > ul > li').hide();
	        $('.actions > ul   li').css('width','100%');
			$('.actions > ul   li').css('margin','0');
				
	    },
		
	    onStepChanging: function (event, currentIndex, newIndex)
	    {
			// controllo dati anagrafici utente o ditta
			// ---------------------------------------------------------
		    if( currentIndex === 1 )
			{
				
			}


			// dopo inserimento dati di fatturazione
			// ---------------------------------------------------------
		    if( currentIndex === 2 )
			{
		    	var tax_amount  		= 0;
		    	var total_invoice_amt 	= 0;
		    	var ritenuta_amount 	= 0;
		    	var inv_amount_to_pay 	= 0;
		    	var stamp_amount		= 0;

				apply_tax_percentage = $('#apply_tax_percentage').val();
			   
		    	if( apply_tax_percentage !== '' && typeof apply_tax_percentage !== 'undefined' )
				{
		    		tax_amount = ( total_voucher_price * apply_tax_percentage ) / 100;
		    		$('#f_tax_amount').val(tax_amount);
		    	} 
				else
				{
		    		$('#inv-tax-calculation').hide();
		    	}

		    	$('#inv-total-price').text(formatEUR( total_voucher_price - tax_amount ) );
		    	$('#inv-total-amount').text(formatEUR( total_voucher_price - tax_amount ) );
		    	$('#f_invoice_amt').val(total_voucher_price - tax_amount );
			   
				total_invoice_amt = total_voucher_price;

		    	if( $('input[name="indentity"]:checked').val() === 'professional' && total_invoice_amt > 0 )
				{
		    		ritenuta_amount = ( total_invoice_amt * 20 ) / 100;
		    	}

		    	if( total_invoice_amt >= 77.47 )
				{
		    		stamp_amount = 2;
		    	}

				inv_amount_to_pay = total_invoice_amt + ritenuta_amount + stamp_amount;
				$('#f_amount_to_pay').val(inv_amount_to_pay);
				$('#inv-total-to-pay').text(formatEUR(inv_amount_to_pay));

				$('#f_stamp_amount').val(stamp_amount);
				$('#inv-stamp-amount').text(formatEUR(stamp_amount));
		    	
		    	$('#f_ritenuta_amount').val(ritenuta_amount);

		    	ritenuta_amount = formatEUR(ritenuta_amount);
		    	
		    	$('#inv-ritenuta-amount').text(formatEUR(ritenuta_amount));

		    	if( total_invoice_amt > 0 ) 
				{
		    		$('#f_total_invoice_amt').val(total_invoice_amt);
		    	}

		    	$('#inv-tax-amount').text(formatEUR(tax_amount));
		    	$('#inv-cal-total-amount').text(formatEUR(total_invoice_amt));

		    	var place = $('#billing_postcode').val();
		    	if( place !== '') place = place + ' ';
		    	place += $('#billing_city').val() + ' - ';
		    	place += $('#billing_country').val();

		    	var tax = $('#user_tax_name').val();
		    	if( tax !== '')	tax = tax + ' ';
		    	tax += $('#billing_tax_number').val();

		    	var apply_tax = $('#apply_tax_name').val();
		    	if( apply_tax !== '') apply_tax = apply_tax + ' ';
		    	if( apply_tax_percentage !== '' && typeof apply_tax_percentage !== 'undefined' ) apply_tax += apply_tax_percentage + '%';

		    	$('#p_billing_name').text($('#billing_name').val());
		    	$('#p_billing_address').text($('#billing_address').val());
		    	$('#p_billing_place').text(place);
		    	$('#p_billing_tax_number').text(tax);
		    	$('#p_billing_email').text($('#billing_email').val());
		    	$('#p_invoice_number').text($('#invoice_number').val());
		    	$('#p_apply_tax').text(apply_tax);
			}
			
			
			// dopo domande sulla fatturazione
			// ---------------------------------------------------------
		    if( currentIndex === 3 )
			{
				
			}
			
			
			// dopo inserimento dati di pagamento
			// ---------------------------------------------------------
		    if( currentIndex === 4 )
			{
				// debugger;
				$('#p_payment_method').text($('#payment_method option:selected').text());
		    	$('#p_payment_data').text($('#payment_data').val());
			}	
			
		    return stepform.valid();
	    },


		onStepChanged: function (event, currentIndex, newIndex)
		{
			// debugger;
			
			if( currentIndex === 0 )
			{
				// nel primo step voglio solo il bottone next
				$('.actions > ul > li:first-child').hide();
				$('.actions > ul   li').css('width','100%');
				$('.actions > ul   li').css('margin','0');
			}
			
			else if( currentIndex === 5 )
			{
				// debugger;
				$('.actions > ul > li:nth-child(2)').hide();
				$('.actions > ul > li:nth-child(3)').show();
			}
			
			else
			{
				// negli altri step voglio vedere 2 bottoni
				$('.actions > ul > li:nth-child(1)').show();
				$('.actions > ul > li:nth-child(2)').show();
				$('.actions > ul > li:nth-child(3)').hide();
				$('.actions > ul   li').css('width','');
				$('.actions > ul   li').css('margin','');
			}
			
		},



	    onFinishing: function (event, currentIndex)
	    {	
			// visualizzo l'immagine loader
			$('#image-loader-cash-in').show();
			
			
			// ultimo step
			// invio dati, email e redirect
			$.ajax(
			{ 
				type: 'POST', 
				url: WPWPCV.ajaxurl,
				data: 
				{
					action			: 'wpw_pcv_submit_cash_in_form',
					ticket_data		: stepform.serialize()
				}, 
				success: function (valid_res) 
				{
					if( valid_res.type === 'success') 
					{
						// url della pagina attuale senza eventuali parametri di query string
						var self_url = window.location.href.split("?")[0];
						
						// redirect verso pagina con messaggio di conferma
						$(location).attr('href', self_url + '?action=success');
						return true;
					}
					else
					{
						alert('Something went wrong, please try again!');
						$('#image-loader-cash-in').hide();
						return false;
					}
				}
			});
			
		    stepform.validate().settings.ignore = ":disabled";
			return stepform.valid();
	    },
		
	    onFinished: function (event, currentIndex)
		{
			// alert('Submitted');
	    }
	});
	
	
	
	
	function formatUSD( numbr )
	{
		numbr = parseFloat(numbr);
		numbr = numbr.toFixed(2).replace(/./g, function(c, i, a) 
		{
		    return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
		});
		return numbr;
	}
	
	function formatEUR( numbr )
	{
		numbr = parseFloat(numbr);
		numbr = numbr.toFixed(2).replace('.',',') + ' €';
		
		return numbr;
	}
	


	
	/**
	 * equal height
	 * 
	 * @param {type} container
	 * @returns {undefined}
	 */
	equalheight = function(container)
	{
		var currentTallest = 0,
		     currentRowStart = 0,
		     rowDivs = new Array(),
		     $el,
		     topPosition = 0;
	 
		$(container).each(function() 
		{
			$el = $(this);
			$($el).height('auto');
			topPostion = $el.position().top;

			if (currentRowStart !== topPostion) 
			{   
				for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) 
				{
					rowDivs[currentDiv].height(currentTallest);
				}
				rowDivs.length = 0; // empty the array
				currentRowStart = topPostion;
				currentTallest = $el.height();
				rowDivs.push($el);
			}
			else 
			{
				rowDivs.push($el);
				currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
			}
		   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) 
		   {
			rowDivs[currentDiv].height(currentTallest);
		   }
		});
	};


	$(window).load(function() 
	{
		equalheight('.wpw-pcv-products-container li');
	});


	$(window).resize(function()
	{
		equalheight('.wpw-pcv-products-container li');
	});
	
});
