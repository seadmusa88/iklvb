<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Plugin Model Class
 *
 * Handles generic functionailties
 *
 * @package Woo Property check voucher
 * @since 1.0.0
 */

class Wpw_Pcv_Model {
 	 	
 	//class constructor
	public function __construct()	{		

	}
		
	/**
	  * Escape Tags & Slashes
	  *
	  * Handles escapping the slashes and tags
	  *
	  * @package Woo Property check voucher
	  * @since 1.0.0
	  */
	   
	 public function wpw_pcv_escape_attr($data){
	  
	 	return esc_attr(stripslashes($data));
	 }
	 
	 /**
	  * Stripslashes 
 	  * 
  	  * It will strip slashes from the content
	  *
	  * @package Woo Property check voucher
	  * @since 1.0.0
	  */
	   
	 public function wpw_pcv_escape_slashes_deep($data = array(),$flag = false){
	 	
	 	if($flag != true) {
			$data = $this->wpw_pcv_nohtml_kses($data);
		}
		$data = stripslashes_deep($data);
		return $data;
	 }

	/**
	 * Strip Html Tags 
	 * 
	 * It will sanitize text input (strip html tags, and escape characters)
	 * 
	 * @package Woo Property check voucher
	 * @since 1.0.0
	 */
	public function wpw_pcv_nohtml_kses($data = array()) {
		
		
		if ( is_array($data) ) {
			
			$data = array_map(array($this,'wpw_pcv_nohtml_kses'), $data);
			
		} elseif ( is_string( $data ) ) {
			
			$data = wp_filter_nohtml_kses($data);
		}
		
		return $data;
	}

	/**
	 * Function to return all Woo products with enable PDF voucher
	 * 
	 * @package Woo Property check voucher
	 * @since 1.0.0
	*/
	public function wpw_pcv_get_voucher_products() {

		$postargs = array( 
			'post_type' 	=> 'product',
			'post_status' 	=> 'publish',
			'order'			=> 'ASC',
		);
		
		// _woo_vou_enable
		$postargs['meta_query']	= array(
			array(
				'key'		=> '_woo_vou_enable',
				'compare'	=> 'EXISTS'
			),
			array(
				'key'		=> '_woo_vou_enable',
				'value'		=> 'yes',
				'compare'	=> '='
			),
			array(
				'relation'	=> 'OR',
				array(
					'key'		=> '_checkboxvalue',
					'value' 	=> 'yes',
					'compare'	=> '!='
				),
				array(
					'key'		=> '_checkboxvalue',
					'compare'	=> 'NOT EXISTS'
				)
			)
		);

		$postargs['posts_per_page'] = '-1';

		$result = new WP_Query( $postargs );
		$postslist = $result->posts;

		return $postslist;
	}
}