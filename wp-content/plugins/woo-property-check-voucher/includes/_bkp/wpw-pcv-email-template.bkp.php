<html>
	<head></head>
	<body>
		<div style="">
			<div class="wpcv-form-row" style="max-width:900px;margin:0 auto;border:1px solid #dedede;padding:30px;">
				<div class="preview-section-1" style="margin-bottom:40px;">
					<div class="wpcv-form-row preview-address to-address" style="display:inline-block;width:49%;">
						<ul style="list-style:none;padding:0;margin:0;">
							<li id="p-company_name" style="font-size:13px;line-height:22px;color:#333333;padding-bottom:5px;"><?php print $data['billing_name'];?></li>
							<li id="p-company_addr1" style="font-size:13px;line-height:22px;color:#333333;padding-bottom:5px;"><?php print $data['billing_address'];?></li>
							<li id="p-company_addr2" style="font-size:13px;line-height:22px;color:#333333;padding-bottom:5px;"><?php print $data['billing_postcode'].' '.$data['billing_city'];?></li>
							<li id="p-company_country" style="font-size:13px;line-height:22px;color:#333333;padding-bottom:5px;"><?php print $data['billing_country'];?></li>
							<li id="p-vat-number" style="font-size:13px;line-height:22px;color:#333333;padding-bottom:5px;"><?php print $data['billing_tax_number'];?></li>
							<li id="p-email" style="font-size:13px;line-height:22px;color:#333333;padding-bottom:5px;"><?php print $data['billing_email'];?></li>
						</ul>
					</div>
					<div class="wpcv-form-row preview-address from-address" style="display:inline-block;width:49%;">
						<ul style="list-style:none;padding:0;margin:0;">
							<li id="p-a-company_name" style="font-size:13px;line-height:22px;color:#333333;padding-bottom:5px;text-align:right;"><?php _e( 'Vera S.r.l.s','wpwpcv');?></li>
							<li id="p-a-company_addr1" style="font-size:13px;line-height:22px;color:#333333;padding-bottom:5px;text-align:right;"><?php _e( 'Piazza Toscana, 2','wpwpcv');?></li>
							<li id="p-a-company_addr2" style="font-size:13px;line-height:22px;color:#333333;padding-bottom:5px;text-align:right;"><?php _e( '20090 Pieve Emanuele - Milano','wpwpcv');?></li>
							<li id="p-a-company_country" style="font-size:13px;line-height:22px;color:#333333;padding-bottom:5px;text-align:right;"><?php _e( 'Italia','wpwpcv');?></li>
							<li id="p-a-vat-number" style="font-size:13px;line-height:22px;color:#333333;padding-bottom:5px;text-align:right;"><?php _e( 'C.F. e P.IVA 09413200966','wpwpcv');?></li>
							<li id="p-a-email" style="font-size:13px;line-height:22px;color:#333333;padding-bottom:5px;text-align:right;"><?php print $admin_email;?></li>
						</ul>	
					</div>
				</div>	
				<p class="Invoice" style="font-weight:600;color:#3b3b3b;font-size:12px;line-height:22px;padding-bottom: 20px;border-bottom:1px solid #a3abae;margin-bottom:30px;"><?php _e( 'Invoice N.:', 'wpwpcv'); ?> <span class="p-invoice-number" id="p-invoice-number" style="padding-right:10px;"> <?php print $data['invoice_number'];?></span> <?php _e( 'Date:', 'wpwpcv');?> <span class="p-invoice-date"> <?php echo date('d/m/Y', current_time('timestamp'));?></span> <?php _e( 'Invoice title:', 'wpwpcv'); ?> <span class="p-invoice-title"><?php _e( 'Occasion perfomance', 'wpwpcv'); ?></span></p> 
					<div class="wpcv-table-wrap">
						<table style="border-color:#ffffff;color:#333333;font-size:12px;line-height:22px;">
							<tr style="border-bottom:1px solid #a3abae;">
							  <th colspan="2" style="text-align:left;border-bottom:1px solid #a3abae;font-size:14px;padding:10px;"><b><?php _e( 'Description','wpwpcv');?></b></th>
							  <!-- th style="text-align:center;border-bottom:1px solid #a3abae;font-size:14px;padding:10px;"><b><?php // _e( 'Price','wpwpcv');?></b></th -->
							  <th style="text-align:right;border-bottom:1px solid #a3abae;font-size:14px;padding:10px;"><b><?php _e( 'Quantity','wpwpcv');?></b></th>
							  <th style="width: 92px;border-bottom:1px solid #a3abae;font-size:14px;padding:10px 0;text-align: right;"><b><?php _e( 'Amount','wpwpcv');?></b></th>
							  <th class="farji" style="width:30px;border-bottom:1px solid #a3abae;font-size:14px;padding:10px;"></th>
							  <th style="border-bottom:1px solid #a3abae;padding:10px;"></th>
							</tr>
							<?php 
							/*
							foreach( $data['f_ticket_amount'] as $key => $ticket) { 
								if( !empty( $data['percentage_of_tax'] ) ) {
									$ticket_per_price = ( $ticket * $data['percentage_of_tax']) / 100; 
									$ticket = $ticket - $ticket_per_price;
								}
							 * 
							 */
							?>
							<tr id="invoice_items" style="border-bottom:1px solid #a3abae;">
								<td class="inv-description" colspan="2" style="text-align:left;"><?php print $data['invoice_description']; ?></td>
						  		<td class="inv-price-" style="text-align:right;"><?php print $data['invoice_quantity']; ?></td>
						  		<td class="inv-total-price" style="text-align:right;"><?php print number_format($data['f_invoice_amt']).' '.$data['currency']; ?></td>
						  		<td class="farji" style="border:none;"></td>
								<td style="border:none;"><p class="inv-info-text" style="color:#3664c3;">(<?php _e( 'The user has 50% of the value of the Tickets entered','wpwpcv');?>)</p></td>
						 	</tr>
						 	<?php // } ?>
						 	<?php if( !empty( $data['f_tax_amount'] ) ) { ?>
						 	<tr id="inv-calculation" style="border-bottom:1px solid #a3abae;">
						 		<td class="inv-label text-right" colspan="3" style="text-align:right;border:none;border-bottom:1px solid #a3abae;border-top:1px solid #a3abae;"><?php print $data['apply_tax_name'].' '.$data['apply_tax_percentage'].'%'; ?></td>
								<td id="inv-total-amount" class="inv-total-amount" style="border:none;border-bottom:1px solid #a3abae;border-top:1px solid #a3abae; text-align:right;"><?php print number_format($data['f_tax_amount'], 2).' '.$data['currency'];?></td>
						 		<td class="farji" style="border:none;border-bottom:1px solid #a3abae;border-top:1px solid #a3abae;"></td>
						  		<td style="border:none;border-bottom:1px solid #a3abae;border-top:1px solid #a3abae;"><p class="inv-info-text" style="color:#3664c3;">(<?php _e( 'Only if the user enters a tax to be applied, this is deducted from the initial amount. Example: from 100','wpwpcv');?> &euro;)</p></td>
						 	</tr>
						 	<?php } ?>
						 	<tr id="inv-calculation" style="border-bottom:1px solid #a3abae;">
						 		<td class="inv-label text-right wpw-bold" colspan="3" style="text-align:right;border-bottom:1px solid #a3abae;"><b><?php _e( 'Total','wpwpcv');?></b></td>
								<td id="inv-cal-total-amount" class="inv-cal-total-amount wpw-bold" style="border-bottom:1px solid #a3abae;text-align:right;"><b><?php print number_format($data['f_total_invoice_amt'],2).' '.$data['currency'];?></b></td>
						 		<td class="farji" style="border-bottom:1px solid #a3abae;"></td>
						  		<th style="border-bottom:1px solid #a3abae;padding:10px;"></th>
						  	</tr>
						 	<tr id="inv-ritenuta" style="border-bottom:1px solid #a3abae;">
						 		<td class="inv-label text-right" colspan="3" style="text-align:right;border-bottom:1px solid #a3abae;"><?php _e( 'Ritenuta d\'acconto 20% out of 100.00','wpwpcv');?></td>
								<td id="inv-ritenuta-amount" class="inv-ritenuta-amount" style="border-bottom:1px solid #a3abae;text-align:right;"><?php print number_format($data['f_ritenuta_amount'],2).' '.$data['currency'];?></td>
						 		<td class="farji" style="border-bottom:1px solid #a3abae;"></td>
						  		<td style="border-bottom:1px solid #a3abae;"><p class="inv-info-text" style="color:#3664c3;">(<?php _e( 'Only If the "professional" user clicks: Yes, the performance has been performed within the Italian territory','wpwpcv');?>)</p></td>
						 	</tr>
						 	<tr id="inv-stamp" style="border-bottom:1px solid #a3abae;">
						 		<td class="inv-label text-right" colspan="3" style="text-align:right;border-bottom:1px solid #a3abae;"><?php _e( 'Stamp ( If the amount of the 	 performance exceeds','wpwpcv');?> &euro; <?php _e( '77.47 the','wpwpcv');?> &euro; <?php _e( '2.00 stamp is required','wpwpcv');?>)</td>
								<td id="inv-stamp-amount" style="border-bottom:1px solid #a3abae;text-align:right;"><?php print number_format($data['f_stamp_amount'],2).' '.$data['currency'];?></td>
						 		<td class="farji" style="border-bottom:1px solid #a3abae;"></td>
						  		<td style="border-bottom:1px solid #a3abae;"><p class="inv-info-text" style="color:#3664c3;">(<?php _e( 'Only if the amount exceeds','wpwpcv');?> &euro;<?php _e( '77.47','wpwpcv');?>)</p></td>
						 	</tr>
						 	<tr id="inv-stamp" style="border-bottom:1px solid #a3abae;">
						 		<td class="inv-label text-right wpw-bold" colspan="3" style="text-align:right;border-bottom:1px solid #a3abae;"><b><?php print strtoupper(__( 'Total invoice','wpwpcv'));?></b></td>
								<td id="inv-total-to-pay" class="inv-total-to-pay wpw-bold" style="border-bottom:1px solid #a3abae;text-align:right;"><b> <?php print number_format($data['f_amount_to_pay'],2).' '.$data['currency'];?></b></td>
						 		<td class="farji" style="border-bottom:1px solid #a3abae;"></td>
						  		<td style="border-bottom:1px solid #a3abae;"></td>
						 	</tr>
						</table>
					</div>
					<div class="wpcv-bottom" style="padding-top:50px;">
						<div class="wpcv-bottom-left user-account-info">
							<p style="font-size:14px;font-weight:600;color:black;padding-bottom:10px;margin:0;"><?php _e( 'Payment', 'wpwpcv'); ?></p>
							<p style="font-size:14px;font-weight:600;color:black;padding-bottom:10px;margin:0;"><?php _e( 'In 30 days', 'wpwpcv'); ?></p>
							<p style="font-size:14px;font-weight:600;color:black;padding-bottom:10px;margin:0;"><span id="user-bank"><?php print $data['bank_account'];?></span><?php _e( '(User entered bank)', 'wpwpcv'); ?></p>
							<p style="font-size:14px;font-weight:600;color:black;padding-bottom:10px;margin:0;"><?php _e( 'IBAN:', 'wpwpcv'); ?> <span id="bank-acc-number"><?php print $data['bank_account_number'];?></span></p>
						</div>
					</div>
				</div>
		</div>
	</body>
</html>