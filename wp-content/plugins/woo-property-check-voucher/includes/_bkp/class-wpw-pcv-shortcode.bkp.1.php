<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Plugin Shortcode Class
 *
 * Handles generic functionailties
 *
 * @package Woo Property check voucher
 * @since 1.0.0
 */


class Wpw_Pcv_Shortcode {

	public $model;

	public function __construct(){

		global $wpw_pcv_model;

		$this->model = $wpw_pcv_model;
	}

	public function wpw_pcv_products_listings( $atts, $content ) {

		extract( shortcode_atts( array(	
			'title'				=>	'',
			'title'				=>	'',
			'for_sale_product_id'	=>	'',
			'for_rent_product_id'	=>	'',
		), $atts ) );

		$products = array();
		$voucher_products = $this->model->wpw_pcv_get_voucher_products();
		if( !empty( $voucher_products ) ){
			foreach ( $voucher_products as $key => $product ) {
				$products[$product->ID] = wc_get_product($product->ID);
			}
		}

		ob_start();

		echo '<div class="wpw-pcv-products-wrap">';

			echo '<div class="wpw-pcv-tab-title">';
				echo '<a href="javascript:void(0)" class="tab-title active" data-id="#wpw_for_sale"> '.__('For Sale','wpwpcv').'</a>';
				echo '<a href="javascript:void(0)" class="tab-title" data-id="#wpw_for_rent"> '.__('For Rent','wpwpcv').'</a>';
			echo '</div>';

			echo '<div class="wpw-pcv-tab-content">';
				echo '<div class="wpw-pcv-tab active" id="wpw_for_sale">';
	
					if( !empty( $products ) ){

						echo '<ul class="wpw-pcv-products-container">';

							foreach ( $products as $key => $product ) {?>
								<li>
									<div class="i-box-mian">
										<?php 
										$attach_id = $product->get_image_id();
										$src = "";
										$link = get_permalink($key);
										if( !empty( $attach_id ) ) {
											$src = wp_get_attachment_url( $attach_id);
										}
										?>					
										<div class="image">
											<a href="<?php print $link;?>" title="<?php print $product->get_name();?>">
											<img src="<?php print $src;?>">
											</a>
										</div>
										<div class="title">
											<a href="<?php print $link;?>" title="<?php print $product->get_name();?>">
											<?php print $product->get_name();?>
											</a>
										</div>

										<div class="wpw-cart-btn-wrap">
											<?php echo do_shortcode( '[add_to_cart style="FALSE" id=' . $key . ']' ) ?>
										</div>
									</div>
								</li>
							<?php 
							}
						} ?>
						</ul>

						<div class="wpw-pcv-extra-pro">
							<ul class="wpw-pcv-products-container">
							<?php
							if( !empty($for_sale_product_id)){ ?>
							<li>
								<div class="i-box-mian">
								<?php
									$sale_product = wc_get_product($for_sale_product_id);
									$attach_id = $sale_product->get_image_id();
									$src = "";
									$link = get_permalink($for_sale_product_id);
									if( !empty( $attach_id ) ) {
										$src = wp_get_attachment_url( $attach_id);
									}
									?>
	
									<div class="image">
										<a href="<?php print $link;?>" title="<?php print $sale_product->get_name();?>">
										<img src="<?php print $src;?>">
										</a>
									</div>
									<div class="title">
										<a href="<?php print $link;?>" title="<?php print $sale_product->get_name();?>">
										<?php print $sale_product->get_name();?>
										</a>
									</div>
									<div class="wpw-cart-btn-wrap">
										<?php echo do_shortcode( '[add_to_cart style="FALSE" id=' . $for_sale_product_id . ']' ) ?>
									</div>
								</div>
							</li>
							<?php 
							}
							echo '</ul>';

					echo '</div>';
				echo '</div>';

				echo '<div class="wpw-pcv-tab" id="wpw_for_rent">';

						echo '<ul class="wpw-pcv-products-container">';

						if( !empty( $products ) ){

							foreach ( $products as $key => $product ) {?>
								<li>
									<div class="i-box-mian">
									<?php 
									$attach_id = $product->get_image_id();
									$src = "";
									$link = get_permalink($key);
									if( !empty( $attach_id ) ) {
										$src = wp_get_attachment_url( $attach_id);
									}
									?>
				
									<div class="image">
										<a href="<?php print $link;?>" title="<?php print $product->get_name();?>">
										<img src="<?php print $src;?>">
										</a>
									</div>
									<div class="title">
										<a href="<?php print $link;?>" title="<?php print $product->get_name();?>">
										<?php print $product->get_name();?>
										</a>
									</div>

									<div class="wpw-cart-btn-wrap">
										<?php echo do_shortcode( '[add_to_cart style="FALSE" id=' . $key . ']' ) ?>
									</div>
									</div>
								</li>
							<?php 
							} 
						}  ?>

						</ul>
						<div class="wpw-pcv-extra-pro">
							<div class="wpw-pcv-extra-pro-title">
								<h2><?php _e( 'Pay the rent of the property','wpwpcv');?></h2>
								<p><?php _e( 'You can pay the rent to the owner with the ticket. you can deliver the person ticket at the right time. The best, easy and safe system. ','wpwpcv');?></p>
								<br/>
							</div>

						<ul class="wpw-pcv-products-container">

						<?php
							if( !empty($for_rent_product_id)){ ?>
							<li>
								<div class="i-box-mian">
								<?php 
									$rent_product = wc_get_product($for_rent_product_id);
									$attach_id = $rent_product->get_image_id();
									$src = "";
									$link = get_permalink($for_rent_product_id);
									if( !empty( $attach_id ) ) {
										$src = wp_get_attachment_url( $attach_id);
									}
									?>

									<div class="image">
										<a href="<?php print $link;?>" title="<?php print $rent_product->get_name();?>">
										<img src="<?php print $src;?>">
										</a>
									</div>
									<div class="title">
										<a href="<?php print $link;?>" title="<?php print $rent_product->get_name();?>">
										<?php print $rent_product->get_name();?>
										</a>
									</div>
									<div class="wpw-cart-btn-wrap">
										<?php echo do_shortcode( '[add_to_cart style="FALSE" id=' . $for_rent_product_id . ']' ) ?>
									</div>
								</div>
							</li>
							<?php 
							}
						echo '</ul>';
					
					echo '</div>';

				echo '</div>';

			echo '</div>';

		echo '</div>';

		$content .= ob_get_clean();
		return $content;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	

	public function wpw_pcv_owners_sellers_form( $atts, $content ) 
	{
		// controllo che l'utente sia loggato
		if(!is_user_logged_in())
		{
			$content .= '<h3>'.__('You need to be logged in to cash in a ticket.','wpwpcv').'</h3>';
			return $content;
		}
		
		if(isset($_GET['action']) && $_GET['action'] == 'success')
		{
			$content .= '
				<div class="thank-you-section">
					<div class="sucess-right"></div>
					<p>'. __( 'The payment request has been taken care of.', 'wpwpcv') .'</p>
					<p>'. __( 'We have sent a copy of your invoice to the email address provided.', 'wpwpcv') .'</p>
					<p>'. __( 'Thank you', 'wpwpcv') .'</p>
					<p>'. __( 'Best regards', 'wpwpcv') .'<br>'. __( 'Team - iKLVB', 'wpwpcv') .'</p>
				</div>	
			';
			return $content;
		}
		
		
		
		// dati dell'utente loggato
		$current_user	= wp_get_current_user();
		$userID			= $current_user->ID;
		$user_role		= intval (get_user_meta( $userID, 'user_estate_role', true) );
		$user_role_type = ($user_role == 1 ? 'private' : 'professional');
		
		// utente privato
		if($user_role == 1)
		{
			$user_email             =   get_the_author_meta( 'user_email' , $userID );
			$billing_first_name		=   get_the_author_meta( 'billing_first_name' , $userID );
			$billing_last_name		=   get_the_author_meta( 'billing_last_name' , $userID );
			$billing_address_1		=   get_the_author_meta( 'billing_address_1' , $userID );
			$billing_city			=   get_the_author_meta( 'billing_city' , $userID );
			$billing_postcode		=   get_the_author_meta( 'billing_postcode' , $userID );
			$billing_country		=   get_the_author_meta( 'billing_country' , $userID );
			$billing_state			=   get_the_author_meta( 'billing_state' , $userID );
			$billing_fiscal_code	=   get_the_author_meta( 'billing_fiscal_code' , $userID );
		}
		
		// utente professionale
		if($user_role == 2)
		{
			$user_email             =   get_the_author_meta( 'user_email' , $userID );
			$billing_first_name		=   get_the_author_meta( 'billing_first_name' , $userID );
			$billing_last_name		=   get_the_author_meta( 'billing_last_name' , $userID );
			$billing_address_1		=   get_the_author_meta( 'billing_address_1' , $userID );
			$billing_city			=   get_the_author_meta( 'billing_city' , $userID );
			$billing_postcode		=   get_the_author_meta( 'billing_postcode' , $userID );
			$billing_country		=   get_the_author_meta( 'billing_country' , $userID );
			$billing_state			=   get_the_author_meta( 'billing_state' , $userID );
			$billing_fiscal_code	=   get_the_author_meta( 'billing_fiscal_code' , $userID );
		}
		
		
		
		
		
		
		
		
		extract( shortcode_atts( array(	
			'title'				=>	'',
		), $atts ) );
		
		// $admin_email = get_option('admin_email');
		ob_start();
		
		// valuta impostata in woocommerce
		$currency = get_woocommerce_currency_symbol();
	
		$ticket_number = '';
		
		if(isset($_GET['ticket_number']))
		{
			if(is_array($ticket_number) && isset($ticket_number[0])) 
			{
				$ticket_number = $ticket_number[0]; 
			}
			else
			{
				$ticket_number = $_GET['ticket_number'];
			}
		}
		
		?>
							
		<div id="image-loader-cash-in"><img src="<?php echo get_template_directory_uri(); ?>-child/img/loading_icon.gif"></div>	

		<form id="ticket-form" class="ticket-form" action="#" style="" novalidate="novalidate">
			
			
			<input type="hidden" name="ticket_inv_form" value="submit">
			<input type="hidden" id="user_role_type" name="user_role_type" value="<?php echo $user_role_type; ?>">
			
			<?php if($user_role_type == 'private') : ?>
			<input type="hidden" id="user_tax_name" name="user_tax_name" value="<?php __('Fiscal Code','wpwpcv'); ?>">
			<?php endif; ?>
			
			<?php if($user_role_type == 'professional') : ?>
			<input type="hidden" id="user_tax_name" name="user_tax_name" value="<?php __('VAT Number','wpwpcv'); ?>">
			<?php endif; ?>
			
			<input type="hidden" id="f_invoice_amt" name="f_invoice_amt" value="">
			<input type="hidden" id="f_tax_amount" name="f_tax_amount" value="">
			<input type="hidden" id="f_total_invoice_amt" name="f_total_invoice_amt" value="">
			<input type="hidden" id="f_ritenuta_amount" name="f_ritenuta_amount" value="">
			<input type="hidden" id="f_stamp_amount" name="f_stamp_amount" value="">
			<input type="hidden" id="f_amount_to_pay" name="f_amount_to_pay" value="">
			
			<input type="hidden" id="button_label_previous" value="<?php _e('Previous', 'wpwpcv'); ?>">
			<input type="hidden" id="button_label_next" value="<?php _e('Continue', 'wpwpcv'); ?>">
			<input type="hidden" id="button_label_finish" value="<?php _e('Confirmation', 'wpwpcv'); ?>">
			
			<input type="hidden" id="tickets_numbers" name="tickets_numbers" value="">
			<input type="hidden" id="invoice_description" name="invoice_description" value="">
			<input type="hidden" id="invoice_quantity" name="invoice_quantity" value="">
			
            <h3>0</h3>
            <section>
				
            	<div class="form-container">
	                <div class="">
	                	<h2 id="calculate-price">0,00 <?php echo $currency; ?></h2>
				    </div>
            		<div class="">
						<div class="ticket-row">
							<input type="text" value="<?php echo $ticket_number; ?>" id="ticket_number" name="ticket_number" 
								   class="ticket_number"  placeholder="<?php _e( 'Insert ticket number', 'wpwpcv'); ?>">
							<label id="ticket-not-valid" class="error"><?php _e( 'Please provide correct ticket number', 'wpwpcv'); ?></label>
							<label id="ticket-already-entered" class="error"><?php _e( 'Ticket number already entered for cash in', 'wpwpcv'); ?></label>
						</div>
						<button type="button" class="btn" id="verify-tickets"><?php _e('Verify','wpwpcv'); ?></button>
						
						<div id="dynamic-ticket"></div>
	                </div>
                </div>
            </section>


            <h3>1</h3>
            <section>
				<div class="form-section">
            		<p><?php _e( 'To be able to collect the sum, the system automatically generates your request invoice. You will receive your bill in your email. Please enter the necessary data.', 'wpwpcv'); ?></p>
            	</div>
				<div class="wpcv-form-row-container">
					
					<?php if($user_role_type == 'private') : ?>
					<h3><?php _e( 'Check your Personal data', 'wpwpcv'); ?></h3>
					<?php endif; ?>
					
					<?php if($user_role_type == 'professional') : ?>
					<h3><?php _e( 'Check your Professional data', 'wpwpcv'); ?></h3>
					<?php endif; ?>
					
					<div class="wpcv-form-row">
						<div class="wpcv-col">
							
							<?php if($user_role_type == 'private') : ?>
							<label class="form-label" for="billing_name"><?php _e( 'Your name', 'wpwpcv'); ?></label>
							<input id="billing_name" name="billing_name" placeholder="* <?php _e( 'Your name', 'wpwpcv'); ?>" class="required" 
								   type="text" value="<?php echo esc_html(trim($billing_last_name.' '.$billing_first_name)); ?>">
							<?php endif; ?>
							
							<?php if($user_role_type == 'professional') : ?>
							<label class="form-label" for="billing_name"><?php _e( 'Name of the company', 'wpwpcv'); ?></label>
							<input id="billing_name" name="billing_name" placeholder="* <?php _e( 'Company name', 'wpwpcv'); ?>" class="required" 
								   type="text" value="<?php echo esc_html($billing_company_name); ?>">
							<?php endif; ?>
							
						</div>
						<div class="wpcv-col">
							<label class="form-label" for="billing_address"><?php _e( 'Address', 'wpwpcv'); ?></label>
							<input id="billing_address" name="billing_address" placeholder="*<?php _e( 'Address', 'wpwpcv'); ?>" class="required" 
								   type="text" value="<?php echo esc_html($billing_address_1); ?>">
						</div>
					</div>
					
					<div class="wpcv-form-row">
						<div class="wpcv-col">
							<label class="form-label" for="billing_postcode"><?php _e( 'Postal code', 'wpwpcv'); ?></label>
							<input id="billing_postcode" name="billing_postcode" placeholder="* <?php _e( 'Postal code', 'wpwpcv'); ?>" class="required" 
								   type="text" value="<?php echo esc_html($billing_postcode); ?>">
						</div>
						<div class="wpcv-col">
							<label class="form-label" for="billing_city"><?php _e( 'Place / City', 'wpwpcv'); ?></label>
							<input id="billing_city" name="billing_city" placeholder="* <?php _e( 'Place / City', 'wpwpcv'); ?>" class="required" 
								   type="text" value="<?php echo esc_html($billing_city); ?>">
						</div>
					</div>

					<div class="wpcv-form-row">
						<div class="wpcv-col">
							<label class="form-label" for="billing_country"><?php _e( 'Nation', 'wpwpcv'); ?></label>
							<input id="billing_country" name="billing_country" placeholder="* <?php _e( 'Nation', 'wpwpcv'); ?>" class="required" 
								   type="text" value="<?php echo esc_html($billing_country); ?>">
						</div>
						<div class="wpcv-col">
							<label class="form-label" for="billing_email"><?php _e( 'Email', 'wpwpcv'); ?></label>
							<input id="billing_email" name="billing_email" placeholder="* <?php _e( 'Email', 'wpwpcv'); ?>" class="required email" 
								   type="text" value="<?php echo esc_html($user_email); ?>">
						</div>
					</div>

					<div class="wpcv-form-row">
						<div class="wpcv-col">
							
							<?php if($user_role_type == 'private') : ?>
							<label class="form-label" for="billing_tax_number"><?php _e( 'Your Fiscal Code', 'wpwpcv'); ?></label>
							<input id="billing_tax_number" name="billing_tax_number" placeholder="* <?php _e( 'Fiscal Code', 'wpwpcv'); ?>" class="required" 
								   type="text" value="<?php echo esc_html($billing_fiscal_code); ?>">
							<?php endif; ?>
							
							<?php if($user_role_type == 'professional') : ?>
							<label class="form-label" for="billing_tax_number"><?php _e( 'Company VAT Number', 'wpwpcv'); ?></label>
							<input id="billing_tax_number" name="billing_tax_number" placeholder="* <?php _e( 'VAT Number', 'wpwpcv'); ?>" class="required digits" 
								   type="text" value="<?php echo esc_html($billing_vat_number); ?>">
							<?php endif; ?>
							
						</div>
						<div class="wpcv-col">
						</div>
					</div>
					
				</div>
			</section>
					
			
			<h3>2</h3>
			<section>
				<div class="wpcv-form-row">					
					<div class="wpcv-col wpcv-col-1-3">
					</div>
					<div class="wpcv-col wpcv-col-1-3">
						<h3><?php _e( 'Enter your invoice number', 'wpwpcv'); ?></h3>
						
						<label class="form-label" for="invoice_number"><?php _e( 'Progressive number of your invoice', 'wpwpcv'); ?></label>
						<input id="invoice_number" name="invoice_number" placeholder="* <?php _e( 'Number of invoice', 'wpwpcv'); ?>" class="required digits" 
							   type="text">
					</div>
					<div class="wpcv-col wpcv-col-1-3">
					</div>
				</div>
					
				<div class="wpcv-form-row">
					<div class="wpcv-col wpcv-col-1-3">
					</div>
					<div class="wpcv-col wpcv-col-1-3 percentage-col">
						<h3><?php _e( 'Do you have to apply a tax to the invoice amount?', 'wpwpcv'); ?></h3>
						
						<label class="form-label" for="apply_tax_name"><?php _e( 'Enter the name of the tax', 'wpwpcv'); ?></label>
						<input id="apply_tax_name" name="apply_tax_name" placeholder="<?php _e( 'Tax name', 'wpwpcv'); ?>" class="" 
							   type="text">

						<label class="form-label" for="percentage_of_tax"><?php _e( 'Enter the percentage to be applied to the amount', 'wpwpcv'); ?></label>
						<input id="apply_tax_percentage" name="apply_tax_percentage" placeholder="<?php _e( 'Percentage of tax', 'wpwpcv'); ?>" class="number" 
							   type="text"><span class="percentage-icon">%</span>
					</div>
					<div class="wpcv-col wpcv-col-1-3">
					</div>
				</div>
			</section>	
					
			<h3>3</h3>
			<section>
				<h3><?php _e( 'Please answer these questions', 'wpwpcv'); ?></h3>
				<div class="wpcv-form-row">
					<div class="wpcv-col">
						<p><?php _e( '1. The perfomance was carried out entirely', 'wpwpcv'); ?></p>
						<label class="label-radio">
							<?php _e( 'Within the Italian territory', 'wpwpcv');?> 
							<input name="carried_out_entirely" class="required" type="radio" value="Within the Italian territory">
							<span class="radio-checkmark"></span>
						</label>
						<label class="label-radio">
							<?php _e( 'Outside the Italian territory', 'wpwpcv');?> 
							<input name="carried_out_entirely" class="required" type="radio" value="Outside the Italian territory">
							<span class="radio-checkmark"></span>
						</label>
					</div>
					<div class="wpcv-col">
						<p><?php _e( '2. The company is based Italy but none of the members is resident in Italy', 'wpwpcv'); ?></p>
						<label class="label-radio"> 
							<?php _e( 'Yes', 'wpwpcv');?>
							<input name="none_members_resident_in_italy" class="required" type="radio" value="Yes">
							<span class="radio-checkmark"></span>
						</label>
						<label class="label-radio"> 
							<?php _e( 'No', 'wpwpcv');?>
							<input name="none_members_resident_in_italy" class="required" type="radio" value="No">
							<span class="radio-checkmark"></span>
						</label>
					</div>
				</div>
				<div class="wpcv-form-row">
					<div class="wpcv-col">
						<p><?php _e( '3. The invoice amount exceeds &#8364;77.47?', 'wpwpcv'); ?></p>
						<label class="label-radio"> 
							<?php _e( 'Yes', 'wpwpcv');?>
							<input name="amount_exceeds" class="required" type="radio" value="Yes">
							<span class="radio-checkmark"></span>
						</label>
						<label class="label-radio"> 
							<?php _e( 'No', 'wpwpcv');?>
							<input name="amount_exceeds" class="required" type="radio" value="No">
							<span class="radio-checkmark"></span>
						</label>
					</div>
					<div class="wpcv-col">
						<p><?php _e( '4. Has the company exceeded &#8364;5,000.00 of receipts from iKLVB in the business year?', 'wpwpcv'); ?></p>
						<label class="label-radio"> 
							<?php _e( 'Yes', 'wpwpcv');?>
							<input name="amount_exceeds_iKLVB" class="required" type="radio" value="Yes">
							<span class="radio-checkmark"></span>
						</label>
						<label class="label-radio"> 
							<?php _e( 'No', 'wpwpcv');?>
							<input name="amount_exceeds_iKLVB" class="required" type="radio" value="No">
							<span class="radio-checkmark"></span>
						</label>
					</div>
				</div>
			</section>
					
			
			<h3>4</h3>
			<section>
				<h3><?php _e( 'Enter your data and payment method', 'wpwpcv'); ?></h3>
				<div class="wpcv-form-row">
					<div class="wpcv-col wpcv-col-1-3">
					</div>
					<div class="wpcv-col wpcv-col-1-3">
						<label class="form-label" for="payment_method"><?php _e( 'Select your payment method', 'wpwpcv'); ?></label>
						<select id="payment_method" name="payment_method" class="required">
							<option value="bank"><?php _e( 'Bank transfer', 'wpwpcv'); ?></option>
							<option value="prepaid"><?php _e( 'Prepaid card', 'wpwpcv'); ?></option>
							<option value="paypal"><?php _e( 'PayPal account', 'wpwpcv'); ?></option>
							<option value="stripe"><?php _e( 'Stripe account', 'wpwpcv'); ?></option>
						</select>
						<label class="form-label" for="payment_data">
							<?php _e( 'Enter the payment method data account (es. bank transfer number, Paypal account, etc.)', 'wpwpcv'); ?></label>
						<input id="payment_data" name="payment_data" placeholder="* <?php _e( 'Payment data', 'wpwpcv'); ?>" 
							   class="required" type="text">
					</div>
					<div class="wpcv-col wpcv-col-1-3">
					</div>
				</div>
			</section>
			
			
			<h3>5</h3>
			<section>
				<div class="form-section">
            		<p><?php _e( 'To be able to collect the sum, the system automatically generates your request invoice. You will receive your bill in your email. Please enter the necessary data.', 'wpwpcv'); ?></p>
            	</div>
				<div class="wpcv-form-row">
					<h3><?php _e( 'Please check the invoice preview and confirm to proceed', 'wpwpcv'); ?></h3>
					
					<div class="preview-section-1">
						<div class="wpcv-col preview-address to-address">
							<ul>
								<li id="p_billing_name">---</li>
								<li id="p_billing_address">---</li>
								<li id="p_billing_place">---</li>
								<li id="p_billing_tax_number">---</li>
								<li id="p_billing_email">---</li>
							</ul>
						</div>
						<div class="wpcv-col preview-address from-address">
							<ul>
								<li id="pa_company_name">Vera S.r.l.s</li>
								<li id="pa_company_addr1">Piazza Toscana, 2</li>
								<li id="pa_company_addr2">20090 Pieve Emanuele - Milano (IT)</li>
								<li id="pa_vat_number">C.F. e P.IVA 09413200966</li>
								<li id="pa_email"><?php echo WPW_PCV_NOTIFY_EMAIL; ?></li>
							</ul>	
						</div>
					</div>	
					
					<div class="form-separator"></div>
					
					<p class="invoice">
						<label><?php _e( 'Invoice N.:', 'wpwpcv'); ?></label>
						<span id="p_invoice_number" class="p-invoice-number">--</span>
						<label><?php _e( 'Date:', 'wpwpcv');?></label>
						<span class="p-invoice-date"><?php echo date('d/m/Y', current_time('timestamp'));?></span>
						<label><?php _e( 'Invoice title:', 'wpwpcv'); ?></label>
						<span class="p-invoice-title"><?php _e( 'Occasion perfomance', 'wpwpcv'); ?></span>
					</p> 
					
					<div class="form-separator"></div>
					
					<div class="wpcv-table-wrap">
						<table>
							<tr>
								<th colspan="2" style="width:45%;"><?php _e( 'Description', 'wpwpcv'); ?></th>
								<!-- th><?php // _e( 'Price', 'wpwpcv'); ?></th -->
								<th style="text-align:right;"><?php _e( 'Quantity', 'wpwpcv'); ?></th>
								<th class="farji"></th>
								<th class="text-right" style="width: 92px;"><?php _e( 'Amount', 'wpwpcv'); ?></th>
								<th style="width: 33%;"><?php _e( 'Notes', 'wpwpcv'); ?></th>
							</tr>
							<tr id="invoice_items">
								<td id="inv-description" class="inv-description" colspan="2">--</td>
						  		<!--td class="inv-price"></td -->
								<td id="inv-quantity" class="inv-qty text-right">--</td>
						  		<td class="farji"></td>
						  		<td id="inv-total-price" class="inv-total-price text-right"></td>
						  		<td><p class="inv-info-text">(<?php _e( 'The user has 50% of the value of the Tickets entered', 'wpwpcv'); ?>)</p></td>
						 	</tr>
						 	<!-- tr id="inv-calculation">
						 		<td class="inv-label text-right" colspan="3"><?php // _e( 'Total:', 'wpwpcv'); ?></td>
						 		<td class="farji"></td>
								<td id="inv-total-amount" class="inv-total-amount text-right"></td>
						  		<td></td>
						 	</tr -->
						 	<tr id="inv-tax-calculation user-tx-percent">
						 		<td id="p_apply_tax" class="inv-label text-right" colspan="3"></td>
						 		<td class="farji"></td>
								<td id="inv-tax-amount" class="inv-tax-amount text-right"></td>
						  		<td><p class="inv-info-text">(<?php _e( 'Only if the user enters a tax to be applied, this is deducted from the initial amount. Example: from 100', 'wpwpcv'); ?> &euro;)</p></td>
						 	</tr>
						 	<tr id="inv-calculation">
						 		<td class="inv-label text-right wpw-bold" colspan="3"><?php _e( 'Total', 'wpwpcv'); ?></td>
						 		<td class="farji"></td>
								<td id="inv-cal-total-amount" class="inv-cal-total-amount wpw-bold text-right"></td>
						  		<td></td>
						 	</tr>
						 	<tr id="inv-ritenuta">
						 		<td class="inv-label text-right" colspan="3"><?php _e( 'Ritenuta d\'acconto 20% out of 100.00', 'wpwpcv'); ?></td>
						 		<td class="farji"></td>
								<td id="inv-ritenuta-amount" class="inv-ritenuta-amount text-right"></td>
						  		<td><p class="inv-info-text">(<?php _e( 'Only If the "professional" user clicks: Yes, the performance has been performed within the Italian territory', 'wpwpcv'); ?>)</p></td>
						 	</tr>
						 	<tr id="inv-stamp">
						 		<td class="inv-label text-right" colspan="3"><?php _e( 'Stamp (If the amount of the performance exceeds', 'wpwpcv'); ?> &euro; <?php _e( '77,47 the ', 'wpwpcv'); ?>&euro; <?php _e( '2,00 stamp is required)', 'wpwpcv'); ?></td>
						 		<td class="farji"></td>
								<td id="inv-stamp-amount" class="text-right"></td>
						  		<td><p class="inv-info-text">(<?php _e( 'Only if the amount exceeds', 'wpwpcv'); ?> &euro; <?php _e( '77,47', 'wpwpcv'); ?>)</p></td>
						 	</tr>
						 	<tr id="inv-stamp">
						 		<td class="inv-label text-right wpw-bold" colspan="3"><?php strtoupper(_e( 'Total invoice', 'wpwpcv')); ?></td>
						 		<td class="farji"></td>
								<td id="inv-total-to-pay" class="inv-total-to-pay wpw-bold text-right"></td>
						  		<td></td>
						 	</tr>
						</table>
					</div>
					<div class="wpcv-bottom">
						<div class="wpcv-bottom-left user-account-info">
							<p><?php _e( 'Payment time:', 'wpwpcv'); ?> <span><?php _e( 'In 30 days', 'wpwpcv'); ?></span></p>							
							<p><?php _e( 'Payment method:', 'wpwpcv'); ?> <span id="p_payment_method"></span></p>
							<p><?php _e( 'Payment data:', 'wpwpcv'); ?> <span id="p_payment_data"></span></p>
						</div>
					</div>
				</div>
			</section>
			
	
        </form>
		<?php
		
		$content .= ob_get_clean();

		return $content;
	}


	/**
	 * Adding Hooks
	 * 
	 * Adding hooks for calling shortcodes.
	 * 
	 * @package Woo Property check voucher
	 * @since 1.0.0
	 */
	public function add_hooks() {

		//add shortcode to show all social login buttons
		add_shortcode( 'woo_pcv_products', array( $this, 'wpw_pcv_products_listings' ) );

		add_shortcode( 'woo_pcv_owners_sellers', array( $this, 'wpw_pcv_owners_sellers_form' ) );

	}
}