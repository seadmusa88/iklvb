<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Public Class
 *
 * Handles front side functionality 
 *
 * @package Woo Property check voucher
 * @since 1.0.0
 */

class Wpw_Pcv_Public {

	//class constructor
	public function __construct()
	{
		
	}

	public function wpw_pcv_voucher_code_guest_user_message( $message )
	{
		$message = __('You need to be logged in to your account to check ticket number.','iklvb-wpresidence');
		return $message;
	}

	public function wpw_pcv_voucher_code_invalid_message( $message ) 
	{
		$message = __('Ticket number doesn\'t exist.','iklvb-wpresidence');
		return $message;
	}

	
	
	public function wpw_pcv_add_ticket_property() 
	{
		$response = array(
			'type' 		=> 'error',
			'message' 	=> __('<strong>ERROR</strong>: Ticket number not found.','iklvb-wpresidence')
		);
		
		// controllo
		if(empty($_POST['operation'])) $_POST['operation'] == 'add_property';
		
				
		if($_POST['operation'] == 'add_property' && !empty($_POST['ticket_number']) && !empty($_POST['prop_id']) && !empty($_POST['user_id'])) 
		{
			
			// controllo i valori ricevuti dal POST
			$ticket_number	= (isset($_POST['ticket_number'])	? strval($_POST['ticket_number'])	: '');
			$prop_id		= (isset($_POST['prop_id'])			? intval($_POST['prop_id'])			: '');
			$user_id		= (isset($_POST['user_id'])			? intval($_POST['user_id'])			: '');
			$schedule_day	= (isset($_POST['schedule_day'])	? date('Y-m-d',	strtotime($_POST['schedule_day']))	: '');
			$schedule_hour	= (isset($_POST['schedule_hour'])	? date('H:i',	strtotime($_POST['schedule_hour']))	: '');
			
			
			// carico i valori dalla proprietà
			$prop_price					= get_post_meta($prop_id, 'property_price',			true);
			$prop_event_ticket_type		= get_post_meta($prop_id, 'event_ticket_type',		true);
			$prop_event_schedule_day	= get_post_meta($prop_id, 'event_schedule_day',		true);
			$prop_event_schedule_hour	= get_post_meta($prop_id, 'event_schedule_hour',	true);
			$prop_event_guest_limit		= get_post_meta($prop_id, 'event_guest_limit',		true);
			
			
			// carico / ricavo il valore delle categoria / action della property
			$prop_action_type = '';
			$prop_action_category		= get_the_terms($prop_id,'property_action_category');
            switch($prop_action_category[0]->slug)
            {
                case 'for-sale':	$prop_action_type = 'for_sale';
                    break;

                case 'in-vendita':	$prop_action_type = 'in_vendita';
                    break;

                case 'for-rent':	$prop_action_type = 'for_rent';
                    break;

                case 'in-affitto':	$prop_action_type = 'in_affitto';
                    break;


                case 'evento':
                case 'event':		$prop_action_type = 'event';
                    break;
            }

			
			
			// carico il voucher dal valore di ticket_number
			$args = array(
				'post_type'		=> 'woovouchercodes',
				'post_status'	=> 'publish',
				'meta_query'	=> array(
					array(
						'key'     => '_woo_vou_purchased_codes',
						'value'   => $ticket_number,
						'compare' => '=',
					),
				),
			);
			
			$result = new WP_Query( $args );
			$posts = $result->posts;

			
			
			if( !empty( $posts ) ) 
			{
				$post = $posts[0];

				// prex($post);
				// prex(get_post_meta($id));
				
				// carico i dati del ticket (voucher)
				$id = $post->ID;
				$vou_property_id		= get_post_meta( $id, '_wpw_pcv_assign_prop', true);
				$vou_customer_user_id	= get_post_meta( $id, '_wpw_pcv_customer_user_id', true);
				$vou_action_type		= get_post_meta( $id, '_wpw_pcv_action_type', true);
				$vou_price_range_min	= get_post_meta( $id, '_wpw_pcv_price_range_min', true);
				$vou_price_range_max	= get_post_meta( $id, '_wpw_pcv_price_range_max', true);
				$vou_event_ticket_type	= get_post_meta( $id, '_wpw_pcv_event_ticket_type', true);
				$vou_expire_date		= get_post_meta( $id, '_woo_vou_exp_date', true);

				// prex(array($prop_event_ticket_type,$vou_event_ticket_type));
				
				// init
				$error_out_of_range = false;
				$error_event_ticket_type = false;
				$error_event_guest_limit = false;
				
				// controlli preliminari
				if($prop_action_type == 'event')
				{
					$schedule_time = $prop_event_schedule_day.' '.$prop_event_schedule_hour;
					if($prop_event_ticket_type != $vou_event_ticket_type) $error_event_ticket_type = true;
					if($this->wpw_pcv_voucher_count_by_property($prop_id) >= $prop_event_guest_limit) $error_event_guest_limit = true;
				}
				else
				{
					$schedule_time = $schedule_day.' '.$schedule_hour;
					// if($prop_price <= $vou_price_range_min || $prop_price > $vou_price_range_max) $error_out_of_range = true; 
					if($prop_price > $vou_price_range_max) $error_out_of_range = true; // dal 2018-10-23 // Lorenzo M.
				}



                $array_vendita=array('for_sale','in_vendita');
                $array_affitto=array('for_rent','in_affitto');
                $sentinella_vendita=in_array($prop_action_type,$array_vendita);
                $sentinella_affitto=in_array($prop_action_type,$array_affitto);
				
				// controlli
				if(empty($prop_action_type) || empty($vou_action_type) || $sentinella_vendita!=in_array($vou_action_type,$array_vendita) ||$sentinella_affitto!=in_array($vou_action_type,$array_affitto) )
				{
					$response['message'] = __('<strong>ERROR</strong>: The type of ticket entered is not suitable for this booking.','iklvb-wpresidence');
				}
				elseif($error_out_of_range	|| $error_event_ticket_type	|| $error_event_guest_limit)	
				{
					if($error_out_of_range) $response['message'] = __('<strong>ERROR</strong>: The type of ticket entered is out of range for this property.','iklvb-wpresidence');					
					if($error_event_ticket_type) $response['message'] = __('<strong>ERROR</strong>: The type of ticket entered is not valid for this event.','iklvb-wpresidence');					
					if($error_event_guest_limit) $response['message'] = __('<strong>ERROR</strong>: There is no more availability for this event.','iklvb-wpresidence');					
				}
				elseif($user_id != $vou_customer_user_id)
				{
					$response['message'] = __('<strong>ERROR</strong>: Ticket number was not purchased by you.','iklvb-wpresidence');
				}
				elseif(date('Y-m-d H:i:s') > $vou_expire_date)
				{
					$response['message'] = __('<strong >ERROR</strong>: Ticket number has expired.','iklvb-wpresidence').' ('.$vou_expire_date.')';
				}
				elseif($schedule_time > $vou_expire_date)
				{
					// formato data e ora impostati in wordpress
					$date_format = get_option('date_format');
					$time_format = get_option('time_format');
	
					$vou_expire_date_text  = date($date_format, strtotime($ticket_expire_date)).', ';
					$vou_expire_date_text .= date($time_format, strtotime($ticket_expire_date));
					
					$response['message'] = __('<strong >ERROR</strong>: The selected date is greater than the ticket expiration date.','iklvb-wpresidence').' ('.$vou_expire_date_text.')';
					if($prop_action_type == 'event') $response['message'] = __('<strong >ERROR</strong>: The event date is greater than the ticket expiration date.','iklvb-wpresidence').' ('.$vou_expire_date_text.')';
				}
				elseif($prop_id == $vou_property_id )
				{
					$response['message'] = __('<strong>ERROR</strong>: Ticket number is already assigned to this property.','iklvb-wpresidence');
				}					
				elseif(is_numeric($vou_property_id))
				{
					$response['message'] = __('<strong>ERROR</strong>: Ticket number is already assigned to other property.','iklvb-wpresidence');
				}
				elseif( empty( $vou_property_id ) )
				{
					// assegno lo status in base al tipo di action
					$status = ($vou_action_type == 'event' ? 'confirmed' : 'assigned');
					
					// valore di next move in base al tipo di action
					$next_move = ($vou_action_type == 'event' ? '' : 'owner_first');
					
					// ottengo id dell'utente proprietario (user)
					// e controllo: se non è stato indicato l'utente allora considero l'autore del post
					// (non dovrebbe essere mai necessario)
					$property_user_id = get_post_meta($prop_id, 'property_user', true);
					if(empty($property_user_id)) $property_user_id = get_post_meta($prop_id, 'original_author',	true);
					
					
					// salvo i dati nel voucher
					update_post_meta( $id, '_wpw_pcv_assign_prop', $prop_id);
					update_post_meta( $id, '_wpw_pcv_assign_prop_user_id', $property_user_id);
					update_post_meta( $id, '_wpw_pcv_schedule_time', $schedule_time);
					update_post_meta( $id, '_wpw_pcv_status', $status);
					update_post_meta( $id, '_wpw_pcv_next_move', $next_move);
					update_post_meta( $id, '_wpw_pcv_time_update', date('Y-m-d H:i:00'));
					
					// messaggio di conferma operazione avvenuta
					$response = array(
						'type' 		=> 'success',
						'message' 	=> __('Ticket assigned successfully to this property.','iklvb-wpresidence')
					);
				}
			} 
		} 

		// wp_send_json($response);
		
		// if request through ajax
		if(isset($_POST['ajax']) && $_POST['ajax'] == true) 
		{  
			echo json_encode($response);
			exit;	
		}
		else 
		{
			return $response;
		}
	}
	
	
	

	public function wpw_pcv_ticket_validation() 
	{
		global $woo_vou_voucher;
		global $woo_vou_model;

		$response = array(
			'type' 		=> 'error',
			'message' 	=> __('<strong>ERROR</strong>: Please add valid ticket number.','iklvb-wpresidence')
		);

		if( !empty( $_POST['ticket_number'] ) ) 
		{
			$ticket_number = trim($_POST['ticket_number']);

			$args = array(
				'post_type' => 'woovouchercodes',
				'post_status' => 'publish',
				'meta_query' => array(
					array(
						'key'     => '_woo_vou_purchased_codes',
						'value'   => $ticket_number,
						'compare' => '=',
					),
					array(
						'key'     => '_wpw_pcv_assign_prop',
						'value'   => '',
						'compare' => '!=',
					),
				),
			);

			$result = new WP_Query( $args );
			$posts = $result->posts;

			if( empty( $posts ) )
			{
				$response = array(
					'type' 		=> 'error',
					'message' 	=> __('Ticket number not found.','iklvb-wpresidence')
				);
			}
			else
			{
				$post = $posts[0];
				$voucer_id					= $post->ID;
				$vou_assign_prop_user_id	= get_post_meta($post->ID, '_wpw_pcv_assign_prop_user_id', true);
				$vou_status					= get_post_meta($post->ID, '_wpw_pcv_status', true);
				$order_id					= get_post_meta($post->ID, '_woo_vou_order_id', true);
				$order						= wc_get_order( $order_id );
				
				if( !empty( $order ) ) 
				{
					// dati dell'utente loggato
					$current_user	= wp_get_current_user();
					
					// il biglietto non è assegnato alla proprietà dell'utente che sta chiedendo l'incasso
					if($vou_assign_prop_user_id != $current_user->ID)
					{
						$response = array(
							'type' 		=> 'error',
							'message' 	=> __('Ticket number not valid.','iklvb-wpresidence')
						);
					}
					
					// lo stato del biglietto non è valido per l'incasso
					elseif($vou_status !== 'performed')
					{
						$response = array(
							'type' 		=> 'error',
							'message' 	=> __('Ticket number not valid.','iklvb-wpresidence')	
						);
					}
					
					// biglietto valido per l'incasso
					else
					{
						//get order items
						$order_items 	= $order->get_items();

						$item_array	= $woo_vou_model->woo_vou_get_item_data_using_voucher_code( $order_items, $ticket_number );

						$item		= isset( $item_array['item_data'] ) ? $item_array['item_data'] : array();
						$item_id	= isset( $item_array['item_id'] ) ? $item_array['item_id'] : array();
						$price		= $woo_vou_model->woo_vou_get_product_price( $order_id, $item_id, $item ); 

						// nome del prodotto 
						// NB: potrebbe essere cambiato dopo la data di acquisto
						$product_title	= get_the_title($post->post_parent); 

						// controllo
						if(empty( $price ) ) $price = 0;					

						// prezzo di incasso // 50 % iKLVB e 50% cliente
						$price_cash_in = $price / 2;

						$response = array(
							'type'			=> 'success',
							'id'			=> $voucer_id,
							'product_title'	=> $product_title,
							'price'			=> number_format( $price, 2, '.', '' ),	
							'price_cash_in' => number_format( $price_cash_in, 2, '.', '' ),	
						);
					}
				}
			} 
		}

		// if request through ajax
		if(isset($_POST['ajax']) && $_POST['ajax'] == true) 
		{  
			echo wp_send_json($response);
			exit;	
		}
		else 
		{
			return $response;
		}
	}

    public function wpw_pcv_tickets_get_total_by_current_user()
    {
        global $woo_vou_voucher;
        global $woo_vou_model;
        $current_user	= wp_get_current_user();
        $args = array(
            'post_type' => 'woovouchercodes',
            'post_status' => 'publish',
            'meta_query' => array(
                array(
                    'key'     => '_wpw_pcv_assign_prop_user_id',
                    'value'   => $current_user->ID,
                    'compare' => '=',
                ),
                array(
                    'key'     => '_wpw_pcv_status',
                    'value'   => 'payed',
                    'compare' => '=',
                )
            ),

        );

        $result = new WP_Query( $args );
        $posts = $result->posts;

        $response=0;
        foreach ($posts as $post) {


            $voucer_id = $post->ID;

            $ticket_number=get_post_meta($post->ID, '_woo_vou_purchased_codes', true);
            $order_id = get_post_meta($post->ID, '_woo_vou_order_id', true);
            $order = wc_get_order($order_id);

            if (!empty($order)) {


                //get order items
                $order_items = $order->get_items();

                $item_array = $woo_vou_model->woo_vou_get_item_data_using_voucher_code($order_items, $ticket_number);

                $item = isset($item_array['item_data']) ? $item_array['item_data'] : array();
                $item_id = isset($item_array['item_id']) ? $item_array['item_id'] : array();
                $price = $woo_vou_model->woo_vou_get_product_price($order_id, $item_id, $item);

                // nome del prodotto
                // NB: potrebbe essere cambiato dopo la data di acquisto
                $product_title = get_the_title($post->post_parent);

                // controllo
                if (empty($price)) $price = 0;

                // prezzo di incasso // 50 % iKLVB e 50% cliente
                $price_cash_in = $price / 2;


                $response=$response+$price;

            }
        }

        return $response;

    }







    public function wpw_pcv_tickets_get_by_current_user($state)
    {
        global $woo_vou_voucher;
        global $woo_vou_model;
        $current_user	= wp_get_current_user();
            $args = array(
                'post_type' => 'woovouchercodes',
                'post_status' => 'publish',
                'meta_query' => array(
                    array(
                        'key'     => '_wpw_pcv_assign_prop_user_id',
                        'value'   => $current_user->ID,
                        'compare' => '=',
                    ),
                        array(
                            'key'     => '_wpw_pcv_status',
                            'value'   => $state,
                            'compare' => '=',
                        )
                    ),

            );

            $result = new WP_Query( $args );
            $posts = $result->posts;

            $response=array();
        foreach ($posts as $post) {


            $voucer_id = $post->ID;

            $ticket_number=get_post_meta($post->ID, '_woo_vou_purchased_codes', true);
            $order_id = get_post_meta($post->ID, '_woo_vou_order_id', true);
            $order = wc_get_order($order_id);

            if (!empty($order)) {


                //get order items
                $order_items = $order->get_items();

                $item_array = $woo_vou_model->woo_vou_get_item_data_using_voucher_code($order_items, $ticket_number);

                $item = isset($item_array['item_data']) ? $item_array['item_data'] : array();
                $item_id = isset($item_array['item_id']) ? $item_array['item_id'] : array();
                $price = $woo_vou_model->woo_vou_get_product_price($order_id, $item_id, $item);

                // nome del prodotto
                // NB: potrebbe essere cambiato dopo la data di acquisto
                $product_title = get_the_title($post->post_parent);

                // controllo
                if (empty($price)) $price = 0;

                // prezzo di incasso // 50 % iKLVB e 50% cliente
                $price_cash_in = $price / 2;

                $response_temp = array(
                    'type' => 'success',
                    'id' => $voucer_id,
                    'ticket_number'=>$ticket_number,
                    'product_title' => $product_title,
                    'price' => number_format($price, 2, '.', ''),
                    'price_cash_in' => number_format($price_cash_in, 2, '.', ''),
                );
                $response[]=$response_temp;

            }
        }

        return $response;

    }



    public function wpw_pcv_tickets_get_by_tickte_code($ticket_number)
    {
        global $woo_vou_voucher;
        global $woo_vou_model;
        $current_user	= wp_get_current_user();
        $response = array(
            'type' 		=> 'error',
            'message' 	=> __('<strong>ERROR</strong>: Please add valid ticket number.','iklvb-wpresidence')
        );
        $args = array(
            'post_type' => 'woovouchercodes',
            'post_status' => 'publish',
            'meta_query' => array(
                array(
                    'key'     => '_woo_vou_purchased_codes',
                    'value'   => $ticket_number,
                    'compare' => '='
                ),
            ),
        );

        $result = new WP_Query( $args );
        $posts = $result->posts;

        if( empty( $posts ) )
        {
            $response = array(
                'type' 		=> 'error',
                'message' 	=> __('Ticket number not found.','iklvb-wpresidence')
            );
        }
        else
        {
            $post = $posts[0];
            $voucer_id					= $post->ID;
            $vou_assign_prop_user_id	= get_post_meta($post->ID, '_wpw_pcv_assign_prop_user_id', true);
            $vou_status					= get_post_meta($post->ID, '_wpw_pcv_status', true);
            $order_id					= get_post_meta($post->ID, '_woo_vou_order_id', true);
            $order						= wc_get_order( $order_id );

            if( !empty( $order ) )
            {

                    //get order items
                    $order_items 	= $order->get_items();

                    $item_array	= $woo_vou_model->woo_vou_get_item_data_using_voucher_code( $order_items, $ticket_number );

                    $item		= isset( $item_array['item_data'] ) ? $item_array['item_data'] : array();
                    $item_id	= isset( $item_array['item_id'] ) ? $item_array['item_id'] : array();
                    $price		= $woo_vou_model->woo_vou_get_product_price( $order_id, $item_id, $item );

                    // nome del prodotto
                    // NB: potrebbe essere cambiato dopo la data di acquisto
                    $product_title	= get_the_title($post->post_parent);

                    // controllo
                    if(empty( $price ) ) $price = 0;

                    // prezzo di incasso // 50 % iKLVB e 50% cliente
                    $price_cash_in = $price / 2;

                    $response = array(
                        'type'			=> 'success',
                        'id'			=> $voucer_id,
                        'product_title'	=> $product_title,
                        'price'			=> number_format( $price, 2, '.', '' ),
                        'price_cash_in' => number_format( $price_cash_in, 2, '.', '' ),
                    );
                }
        }

        return $response;





    }


    public function wpw_pcv_tickets_get_by_current_user_customer($state,$prop_action_type)
    {
        global $woo_vou_voucher;
        global $woo_vou_model;
        $current_user	= wp_get_current_user();

        /**
         * devo distinguere se fare la query per i biglietti in vendita o in noleggio
         */

        if($prop_action_type=='for_sale' || $prop_action_type=='in_vendita')
        {
            $args = array(
                'post_type' => 'woovouchercodes',
                'post_status' => 'publish',
                'meta_query' => array(
                    array(
                        'key'     => '_wpw_pcv_customer_user_id',
                        'value'   => $current_user->ID,
                        'compare' => '=',
                    ),
                    array(
                        'key'     => '_wpw_pcv_status',
                        'value'   => $state,
                        'compare' => '=',
                    ),
                    array(
                        'key'     => '_wpw_pcv_action_type',
                        'value'   => array('for_sale','in_vendita'),
                        'compare' => 'IN',
                    )


                ),

            );
        }
        elseif ($prop_action_type=="for_rent" || $prop_action_type=="in_affitto")
        {
            $args = array(
                'post_type' => 'woovouchercodes',
                'post_status' => 'publish',
                'meta_query' => array(
                    array(
                        'key'     => '_wpw_pcv_customer_user_id',
                        'value'   => $current_user->ID,
                        'compare' => '=',
                    ),
                    array(
                        'key'     => '_wpw_pcv_status',
                        'value'   => $state,
                        'compare' => '=',
                    ),
                    array(
                        'key'     => '_wpw_pcv_action_type',
                        'value'   => array('for_rent','in_affitto'),
                        'compare' => 'IN',
                    )


                ),

            );
        }



        $result = new WP_Query( $args );
        $posts = $result->posts;

        $response=array();
        foreach ($posts as $post) {


            $voucer_id = $post->ID;

            $ticket_number=get_post_meta($post->ID, '_woo_vou_purchased_codes', true);
            $order_id = get_post_meta($post->ID, '_woo_vou_order_id', true);
            $order = wc_get_order($order_id);

            if (!empty($order)) {


                //get order items
                $order_items = $order->get_items();

                $item_array = $woo_vou_model->woo_vou_get_item_data_using_voucher_code($order_items, $ticket_number);

                $item = isset($item_array['item_data']) ? $item_array['item_data'] : array();
                $item_id = isset($item_array['item_id']) ? $item_array['item_id'] : array();
                $price = $woo_vou_model->woo_vou_get_product_price($order_id, $item_id, $item);

                // nome del prodotto
                // NB: potrebbe essere cambiato dopo la data di acquisto
                $product_title = get_the_title($post->post_parent);

                // controllo
                if (empty($price)) $price = 0;

                // prezzo di incasso // 50 % iKLVB e 50% cliente
                $price_cash_in = $price / 2;

                $response_temp = array(
                    'type' => 'success',
                    'id' => $voucer_id,
                    'ticket_number'=>$ticket_number,
                    'product_title' => $product_title,
                    'price' => number_format($price, 2, '.', ''),
                    'price_cash_in' => number_format($price_cash_in, 2, '.', ''),
                );
                $response[]=$response_temp;

            }
        }

        return $response;





    }


    public function wpw_pcv_submit_cash_in_form()
	{
		$response = array(
			'type' 		=> 'error',
			'message' 	=> __('Some error occur while submitting invoice.','iklvb-wpresidence')
		);
		
		// prex($_POST);
		
		if( !isset( $_POST['ticket_data'] ) || empty( $_POST['ticket_data'] ) ) 
		{
			wp_send_json($response);
			exit;
		}
		
		$data = array();
		parse_str($_POST['ticket_data'],$data);

		$tickets_numbers = explode(',',$data['tickets_numbers']);

		if(empty($tickets_numbers))
		{
			wp_send_json($response);
			exit;
		}

		// controllo che i biglietti non siano già stati incassati
		foreach($tickets_numbers as $v)
		{
			// rendo disponibile il numero del biglietto all'interno del metodo
			// NB: migliorare...
			$_POST['ticket_number'] = $v; 
			
			$res = $this->wpw_pcv_ticket_validation();
			
			if(isset($res['type']) && $res['type'] == 'error')
			{
				$response = array(
					'type' 		=> 'error',
					'message' 	=> __('Procedure interrupted: this ticket has already been cashed:','wpwpcv').' '.$v
				);

				wp_send_json($response);
				exit;
			}				
		}


		//
		$post_array = array (
			'title' => 'Invoice cash in '.date('Y-m-d H:i:s'),
			'post_status' => 'publish',
			'post_type' => 'wpw_pcv_invoice'
		);

		$id = wp_insert_post($post_array);

		// prex($id);
		
		if( $id )
		{
			$prefix = '_wpw_pcv_';
			$currency = get_woocommerce_currency_symbol();
			$data['currency'] = $currency;
		
			update_post_meta( $id, $prefix.'tickets_numbers',		$data['tickets_numbers']);
			update_post_meta( $id, $prefix.'user_role_type',		$data['user_role_type']);
			
			update_post_meta( $id, $prefix.'billing_name',			$data['billing_name']);
			update_post_meta( $id, $prefix.'billing_address',		$data['billing_address']);
			update_post_meta( $id, $prefix.'billing_postcode',		$data['billing_postcode'] );
			update_post_meta( $id, $prefix.'billing_city',			$data['billing_city']);
			update_post_meta( $id, $prefix.'billing_country',		$data['billing_country']);
			update_post_meta( $id, $prefix.'billing_email',			$data['billing_email']);
			update_post_meta( $id, $prefix.'billing_tax_number',	$data['billing_tax_number']);

			update_post_meta( $id, $prefix.'invoice_number',		$data['invoice_number']);
			update_post_meta( $id, $prefix.'invoice_description',	$data['invoice_description']);
			
			update_post_meta( $id, $prefix.'apply_tax_name',		$data['apply_tax_name']);
			update_post_meta( $id, $prefix.'apply_tax_percentage',	$data['apply_tax_percentage']);

			update_post_meta( $id, $prefix.'payment_method',		$data['payment_method']);
			update_post_meta( $id, $prefix.'payment_data',			$data['payment_data']);

			update_post_meta( $id, $prefix.'carried_out_entirely',				$data['carried_out_entirely']);
			update_post_meta( $id, $prefix.'none_members_resident_in_italy',	$data['none_members_resident_in_italy']);
			update_post_meta( $id, $prefix.'amount_exceeds',					$data['amount_exceeds']);
			update_post_meta( $id, $prefix.'amount_exceeds_iKLVB',				$data['amount_exceeds_iKLVB']);

			update_post_meta( $id, $prefix.'invoice_amt',			$data['f_invoice_amt'].' '.$currency);
			update_post_meta( $id, $prefix.'tax_amount',			$data['f_tax_amount'].' '.$currency);
			update_post_meta( $id, $prefix.'total_invoice_amt',		$data['f_total_invoice_amt'].' '.$currency);
			update_post_meta( $id, $prefix.'ritenuta_amount',		$data['f_ritenuta_amount'].' '.$currency);
			update_post_meta( $id, $prefix.'stamp_amount',			$data['f_stamp_amount'].' '.$currency);
			update_post_meta( $id, $prefix.'amount_to_pay',			$data['f_amount_to_pay'].' '.$currency);

			
			// cambio lo stato dei biglietti da "performed" a "checked"
			foreach($tickets_numbers as $v)
			{
				// carico il voucher dal valore di ticket_number
				$args = array(
					'post_type'		=> 'woovouchercodes',
					'post_status'	=> 'publish',
					'meta_query'	=> array(
						array(
							'key'     => '_woo_vou_purchased_codes',
							'value'   => $v,
							'compare' => '=',
						),
					),
				);

				$result = new WP_Query( $args );
				$posts = $result->posts;

				if( !empty( $posts ) ) 
				{
					$post = $posts[0];
					
					// oggi
					$now = date('Y-m-d H:i:00');
	
					// aggiorno le informazioni del biglietto
					update_post_meta($post->ID, '_wpw_pcv_status', 'checked');
					update_post_meta($post->ID, '_wpw_pcv_time_update', $now);					
				}
			}
			
			
			$email_subject = __('Invoice Cash In - iKLVB','wpwpcv');
			$email_body = $this->get_email_body( $data );

			// prex($email_body);

			$headers = array(
				'Content-Type: text/html; charset=UTF-8',
				'From:iKLVB <notification@iklvb.com>'
			);


			// destinatari del messaggio
			$send_email = array($data['billing_email'], WPW_PCV_NOTIFY_EMAIL);

			// invio email
			wp_mail( $send_email, $email_subject, $email_body, $headers);


			// risposta di successo
			$response = array(
				'type' 		=> 'success',
				'message' 	=> __('Your invoice has been submitted.','iklvb-wpresidence')
			);
		}


		wp_send_json($response);
		exit;
	}

	public function get_email_body( $data ) 
	{
		//$admin_email = get_option('admin_email');
		$admin_email = WPW_PCV_NOTIFY_EMAIL;
		
		ob_start();
		include_once WPW_PCV_INC_DIR.'/wpw-pcv-email-template.php';

		$body = ob_get_clean();

		return $body;
	}
	
	
	/**
	 * numero di voucher abbinati ad una singola proprietà
	 * es. serve per sapere se c'è ancora disponibilità per un evento
	 * 
	 * @param type $_property_id
	 */
	public function wpw_pcv_voucher_count_by_property($_property_id = '')
	{
		$args = array(
			'post_type' => 'woovouchercodes',
			'post_status' => 'publish',
			'meta_query' => array(
				array(
					'key'     => '_wpw_pcv_assign_prop',
					'value'   => $_property_id,
					'compare' => '=',
				),
				array(
					'key'     => '_wpw_pcv_status',
					'value'   => array('assigned','confirmed'),
					'compare' => 'IN',
				),
			),
		);

		$result = new WP_Query( $args );
		
		return $result->post_count;
	}
	
	
	/**
	 * controllo se per una property è già in corso un abbinamento per il cliente indicato,
	 * vengono considerati solo status 'assigned' e 'confirmed'
	 * se il ticket si trova negli altri status allora il cliente può eseguire una nuova richiesta
	 * 
	 * @param type $_property_id
	 * @param type $_customer_id
	 */
	public function wpw_pcv_voucher_assign_prop_user($_property_id = '', $_customer_id = '')
	{
		$args = array(
			'post_type' => 'woovouchercodes',
			'post_status' => 'publish',
			'meta_query' => array(
				array(
					'key'     => '_wpw_pcv_assign_prop',
					'value'   => $_property_id,
					'compare' => '=',
				),
				array(
					'key'     => '_wpw_pcv_customer_user_id',
					'value'   => $_customer_id,
					'compare' => '=',
				),
				array(
					'key'     => '_wpw_pcv_status',
					'value'   => array('assigned','confirmed'),
					'compare' => 'IN',
				),
			),
		);

		$result = new WP_Query( $args );
		
		return $result;
	}
	
	
	
	/**
	 * controllo se per una property sono presenti dei biglietti 'confirmed' per il cliente indicato
	 * e siamo nelle 24 ore prima della visita
	 * 
	 * @param type $_property_id
	 * @param type $_customer_id
	 */
	public function wpw_pcv_voucher_is_confirmed_24_prop_user($_property_id = '', $_customer_id = '')
	{
		$args = array(
			'post_type' => 'woovouchercodes',
			'post_status' => 'publish',
			'meta_query' => array(
				array(
					'key'     => '_wpw_pcv_assign_prop',
					'value'   => $_property_id,
					'compare' => '=',
				),
				array(
					'key'     => '_wpw_pcv_customer_user_id',
					'value'   => $_customer_id,
					'compare' => '=',
				),
				array(
					'key'     => '_wpw_pcv_status',
					'value'   => array('confirmed'),
					'compare' => 'IN',
				),
			),
		);

		$result = new WP_Query( $args );
		
		$flag_out = false;
		
		if($result->post_count > 0)
		{
			// oggi
			$now = date('Y-m-d H:i:00');
		
			foreach($result->post as $post)
			{
				// data incontro
				$vou_schedule_time = get_post_meta($post->ID, '_wpw_pcv_schedule_time', true);
				
				// controllo se mancano meno di 24 ore alla data dell'incontro
				if($now > date('Y-m-d H:i:00', strtotime($vou_schedule_time.' -'.WPW_PCV_TIME_BEFORE_SCHEDULE.' seconds'))) $flag_out = true;
			}
		}
		
		return $flag_out;
	}
	
	
	
	/**
	 * controllo se per una property sono presenti dei biglietti 'performed' per il cliente indicato
	 * 
	 * @param type $_property_id
	 * @param type $_customer_id
	 */
	public function wpw_pcv_voucher_is_performed_prop_user($_property_id = '', $_customer_id = '')
	{
		$args = array(
			'post_type' => 'woovouchercodes',
			'post_status' => 'publish',
			'meta_query' => array(
				array(
					'key'     => '_wpw_pcv_assign_prop',
					'value'   => $_property_id,
					'compare' => '=',
				),
				array(
					'key'     => '_wpw_pcv_customer_user_id',
					'value'   => $_customer_id,
					'compare' => '=',
				),
				array(
					'key'     => '_wpw_pcv_status',
					'value'   => array('performed'),
					'compare' => 'IN',
				),
			),
		);

		$result = new WP_Query( $args );
		
		return ($result->post_count > 0 ? true : false);
	}
	
	
	
	/**
	 * controllo se per una property sono presenti dei biglietti 'performed','checked' o 'payed' per il cliente indicato
	 * (vuol dire che la visita è avvenuta e il visitatore può commentare)
	 * 
	 * @param type $_property_id
	 * @param type $_customer_id
	 */
	public function wpw_pcv_voucher_is_visited_prop_user($_property_id = '', $_customer_id = '')
	{
		$args = array(
			'post_type' => 'woovouchercodes',
			'post_status' => 'publish',
			'meta_query' => array(
				array(
					'key'     => '_wpw_pcv_assign_prop',
					'value'   => $_property_id,
					'compare' => '=',
				),
				array(
					'key'     => '_wpw_pcv_customer_user_id',
					'value'   => $_customer_id,
					'compare' => '=',
				),
				array(
					'key'     => '_wpw_pcv_status',
					'value'   => array('performed','checked','payed'),
					'compare' => 'IN',
				),
			),
		);

		$result = new WP_Query( $args );
		
		return ($result->post_count > 0 ? true : false);
	}
	
	
	
	
	
	/**
	 * controllo se per un proprietario di immobili sono presenti dei biglietti 'performed','checked' o 'payed' collegati al cliente indicato
	 * (vuol dire che la visita è avvenuta e il visitatore può commentare)
	 * 
	 * @param type $_pro_user_id
	 * @param type $_customer_id
	 */
	public function wpw_pcv_voucher_is_visited_propuser_customer($_prop_user_id = '', $_customer_id = '')
	{
		$args = array(
			'post_type' => 'woovouchercodes',
			'post_status' => 'publish',
			'meta_query' => array(
				array(
					'key'     => '_wpw_pcv_assign_prop_user_id',
					'value'   => $_prop_user_id,
					'compare' => '=',
				),
				array(
					'key'     => '_wpw_pcv_customer_user_id',
					'value'   => $_customer_id,
					'compare' => '=',
				),
				array(
					'key'     => '_wpw_pcv_status',
					'value'   => array('performed','checked','payed'),
					'compare' => 'IN',
				),
			),
		);

		$result = new WP_Query( $args );
		
		return ($result->post_count > 0 ? true : false);
	}
	
	

	/**
	 * Adding Hooks
	 *
	 * Adding hooks for the front page.
	 *
	 * @package Woo Property check voucher
	 * @since 1.0.0
	 */
	function add_hooks()
	{
		add_filter('woo_vou_voucher_code_guest_user_message', array( $this, 'wpw_pcv_voucher_code_guest_user_message') );
		add_filter('woo_vou_voucher_code_invalid_message', array( $this, 'wpw_pcv_voucher_code_invalid_message') );
		
		add_action('wp_ajax_wpw_pcv_add_ticket_property', array( $this, 'wpw_pcv_add_ticket_property') );
		add_action('wp_ajax_nopriv_wpw_pcv_add_ticket_property', array( $this, 'wpw_pcv_add_ticket_property') );

		// valid ticket calculation
		add_action('wp_ajax_wpw_pcv_ticket_validation', array( $this, 'wpw_pcv_ticket_validation') );
		add_action('wp_ajax_nopriv_wpw_pcv_ticket_validation', array( $this, 'wpw_pcv_ticket_validation') );
		
		add_action('wp_ajax_wpw_pcv_submit_cash_in_form', array( $this, 'wpw_pcv_submit_cash_in_form') );
		add_action('wp_ajax_nopriv_wpw_pcv_submit_cash_in_form', array( $this, 'wpw_pcv_submit_cash_in_form') );
	}
}