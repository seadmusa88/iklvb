<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Scripts Class
 *
 * Handles adding scripts functionality to the admin pages
 * as well as the front pages.
 *
 * @package Woo Property check voucher
 * @since 1.0.0
 */

class Wpw_Pcv_Scripts {

	//class constructor
	function __construct()
	{
		
	}
	
	/**
	 * Enqueue Scripts on Admin Side
	 * 
	 * @package Woo Property check voucher
	 * @since 1.0.0
	 */
	public function wpw_pcv_admin_scripts(){
	
	}

	public function wpw_pcv_public_scripts(){
		global $post;

		wp_register_script('wpw-pcv-public-script', WPW_PCV_INC_URL.'/js/wpw-pcv-public-script.js', array('jquery'), date('YY-m-d-his'), false);
		wp_enqueue_script( 'wpw-pcv-public-script' );

		wp_localize_script('wpw-pcv-public-script', 'WPWPCV', array(
               
                'ajaxurl' => admin_url( 'admin-ajax.php', ( is_ssl() ? 'https' : 'http' ) ),
                'post_id' => $post->ID
            ));
		
		wp_register_script('wpw-pcv-validate-script', WPW_PCV_INC_URL.'/js/jquery.validate.min.js', array('jquery'), WPW_PCV_VERSION);
		wp_enqueue_script( 'wpw-pcv-validate-script' );

		wp_register_script('wpw-pcv-step-form-script', WPW_PCV_INC_URL.'/js/jquery.steps.min.js', array('jquery','wpw-pcv-validate-script'), WPW_PCV_VERSION);
		wp_enqueue_script( 'wpw-pcv-step-form-script' );
	}

	public function wpw_pcv_public_styles(){

		wp_register_style('wpw-pcv-public-style', WPW_PCV_INC_URL.'/css/wpw-pcv-public.css', array(), date('YY-m-d-his'));
		wp_enqueue_style( 'wpw-pcv-public-style' );

		wp_register_style('wpw-pcv-step-form-style', WPW_PCV_INC_URL.'/css/jquery.steps.css', array(), WPW_PCV_VERSION);
		wp_enqueue_style( 'wpw-pcv-step-form-style' );
	}
	
	/**
	 * Adding Hooks
	 *
	 * Adding hooks for the styles and scripts.
	 *
	 * @package Woo Property check voucher
	 * @since 1.0.0
	 */
	function add_hooks(){
		
		//add admin scripts
		// add_action('admin_enqueue_scripts', array($this, 'wpw_pcv_admin_scripts'));

		add_action('wp_enqueue_scripts', array($this, 'wpw_pcv_public_scripts'));
		add_action('wp_enqueue_scripts', array($this, 'wpw_pcv_public_styles'));
	}
}
?>