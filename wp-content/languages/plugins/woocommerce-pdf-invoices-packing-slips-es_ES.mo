��    �      �  =  �      �  $   �            �   "  ,   �  �   �  x   �  s     g   �  o   �  �   _  n     �   z     Z     ]     }      �  N   �  !   �          (      /  �   P  E   �  x   !  
   �     �     �  \   �  _     6   |     �  !   �  B   �  _   )  #   �     �  q   �  #   1  f   U  
   �  J   �          !     6     E     b  I   n     �     �     �  h   �     \      r      �      �      �      �   g   �   �   D!  F   �!     )"  	   7"     A"     S"     j"  @   {"     �"     �"     �"     �"  /   #  >   7#  h   v#     �#     �#  2   �#     -$      <$     ]$     k$     y$     �$     �$  �   �$     _%  *   r%  '   �%  Y   �%     &     '&  3   8&  :   l&  %   �&  :   �&     '     '      )'     J'     i'  B   y'     �'  B   �'     (     (     %(     3(     B(     `(  7   p(     �(  �   �(  r   �)  �   �)  -   y*     �*     �*  T   �*     +  k   1+     �+     �+     �+     �+  0   �+     �+  0   �+     ",     5,  /   C,  /   s,  (   �,     �,     �,     �,     �,     -     -      -     (-  
   5-     @-     P-     _-  N   f-     �-     �-     �-     �-     �-     �-     �-     .     .     '.     C.     X.     \.     a.     f.  )   ~.  8   �.  )   �.  =   /  
   I/     T/  �   e/  J   �/  	   90     C0  7   ]0     �0     �0     �0     �0     �0     �0     �0  	   �0     �0     1  *   1     E1     L1     U1     t1  Y   {1  Q   �1     '2     52  	   >2  !   H2  �   j2  �   3  ?   �3  ?   !4  ?   a4  �   �4     �5     �5  7   �5  '   �5  )   6     ;6  �   [6  *   �6  1   7     @7  �   D7     �7  �   �7  �   i8     9  $   "9     G9  (   `9  >   �9  ^   �9  `   ':  !   �:  <   �:     �:     �:  ;   ;     K;  	   k;     u;  C   �;     �;     �;     �;     �;     <  r   <     �<  X   �<  C  �<  4   ;>     p>     }>  �   �>  7   =?  �   u?  �   <@  �   �@  �   cA  x   �A  �   mB  u   %C    �C     �D     �D  $   �D  %   �D  i   E  *   E     �E     �E  &   �E  �   �E  O   �F  �   �F     ~G     �G     �G  �   �G  w   #H  :   �H     �H  &   �H  C   I  a   ]I  .   �I     �I  �   J  $   �J  �   �J  	   KK  u   UK     �K     �K     �K     L     0L  S   =L  #   �L  	   �L  "   �L  �   �L  (   dM     �M     �M     �M     �M     �M  �   N  �   �N  R   [O     �O  
   �O     �O     �O     �O  Z   P     aP     iP      �P     �P  3   �P  H   �P  �   AQ      �Q     �Q  H   �Q     ;R  /   JR     zR     �R     �R     �R  	   �R  �   �R     �S  9   �S  ;   T  v   BT     �T     �T  =   �T  ?   U  )   QU  J   {U     �U     �U  !   �U  #   V     2V  O   CV     �V  F   �V     �V     �V     W     W      1W     RW  <   fW     �W  �   �W  �   �X  �   Y  9   �Y     �Y     �Y  \   �Y     UZ  �   sZ     �Z      [     [     [  :   %[  	   `[  :   j[     �[     �[  8   �[  1   \  ;   A\     }\     �\     �\     �\     �\     �\     �\     �\     ]     ]     &]     6]  Z   >]     �]     �]     �]     �]     �]     �]     �]     �]     
^  ,   '^  $   T^     y^     }^     �^  +   �^  B   �^  A   �^  *   ;_  D   f_     �_  %   �_  �   �_  B   r`     �`  %   �`  ?   �`     *a  	   2a     <a     Ca     Ya     la     ~a     �a     �a     �a  +   �a     b     b  "   b     9b  Y   @b  V   �b     �b     c     c  !   #c  �   Ec  �   �c  P   �d  P   +e  P   |e    �e     �f     �f  :   �f  6   :g  >   qg     �g  �   �g  =   qh  ?   �h     �h  �   �h     �i  �   �i  �   Dj     k  $   !k     Fk  (   _k  M   �k  a   �k  n   8l  !   �l  :   �l     m     m  ;   7m  "   sm     �m  )   �m  R   �m     n     -n     /n     Jn     [n  ~   ]n  (   �n  c   o     �              K   7   �   k           #   �   Q   R   S       �   �   !   �   �   `           p   �   y   6   �   �   I   �   �      �               �   �   4   C       �   �   �   �   �   s   z       �   �   �   �   _   v   �   r   �       �   e   T                 1   �   �   B   �   �   �      �   
       �       �           Y   ~                  j   �   �   J      �   �         �       �   \      �          �   �   L       {       �   }   �   �   �       �   �      F       u       E   �   +   /   3   -   @   �   %   �      :   �             �   &   o   x       [   �   l         M       �       �   Z       �       ;      �   g           '   �   �   �       �   8   >               0   |       �       �       �   U   m   =   �   W       	   f               d   5   �           �       �   9   2   h   �   H           �   q   �   �          X   �   N       �   ^   V       �                       t          n   �   �   ?   ,   �   O   �   �   *       �           <   �   �   �   c          �       �   �         �   (         �   )           �   �   $      �   G           D   b   �   �       "       P   w   �          �   �       .   �       �       �       �      �       �          a   �   �       ]   �   i   A   �    %s email notification manually sent. (Includes %s) (includes %s) <b>Caution!</b> This setting may reveal errors (from other plugins) in other places on your site too, therefor this is not recommended to leave it enabled on live sites. <b>Completely automatic</b> scheduled emails <b>Customize</b> the <b>shipping & billing address</b> format to include additional custom fields, font sizes etc. without the need to create a custom template. <b>Disabled:</b> The PHP functions glob and filemtime are required for automatic cleanup but not enabled on your server. <b>Rich text editor</b> for the email text, including placeholders for data from the order (name, order total, etc) <b>Super versatile!</b> Can be used for any kind of reminder email (review reminders, repeat purchases) <b>Warning!</b> The settings below are meant for debugging/development only. Do not use them on a live website! <strong>Caution:</strong> enabling this will also mean that if you change your company name or address in the future, previously generated documents will also be affected. <strong>Note:</strong> invoice numbers and dates are not affected by this setting and will still be generated. <strong>Warning!</strong> Your database has an AUTO_INCREMENT step size of %s, your invoice numbers may not be sequential. Enable the 'Calculate document numbers (slow)' setting in the Status tab to use an alternate method. A4 Actually, I have a complaint... Admin email Advanced, customizable templates All available documents are listed below. Click on a document to configure it. Allow My Account invoice download Already did! Always Always use most current settings An outdated template or action hook was used to generate the PDF. Legacy mode has been activated, please try again by reloading this page. Are you sure you want to delete this document? This cannot be undone. Attach <b>up to 3 static files</b> (for example a terms & conditions document) to the WooCommerce emails of your choice. Attach to: Attach too... Attachments Automatically clean up PDF files stored in the temporary folder (used for email attachments) Automatically send new orders or packing slips to your printer, as soon as the customer orders! Automatically send payment reminders to your customers Billing Address: Calculate document numbers (slow) Check out the Premium PDF Invoice & Packing Slips templates at %s. Check out the WooCommerce Automatic Order Printing extension from our partners at Simba Hosting Check out these premium extensions! Choose a template Completely customize the invoice contents (prices, taxes, thumbnails) to your needs with a drag & drop customizer Configure it <a href="%s">here</a>. Configure the exact requirements for sending an email (time after order, order status, payment method) Create PDF Create, print & email PDF invoices & packing slips for WooCommerce orders. Customer Notes DEBUG output enabled Debug settings Delete legacy (1.X) settings Description Disable automatic creation/attachment when only free products are ordered Disable for free products Discount Display billing address Display billing address (in addition to the default shipping address) if different from shipping address Display email address Display invoice date Display invoice number Display options Display phone number Display shipping address Display shipping address (in addition to the default billing address) if different from billing address Document numbers (such as invoice numbers) are generated using AUTO_INCREMENT by default. Use this setting if your database auto increments with more than 1. Document of type '%s' for the selected order(s) could not be generated Documentation Documents Download %s (PDF) Download invoice (PDF) Download the PDF Email/print/download <b>PDF Credit Notes & Proforma invoices</b> Enable Enable automatic cleanup Enable debug output Enable font subsetting Enable invoice number column in the orders list Enable this if your currency symbol is not displaying properly Enable this option to output plugin errors if you're getting a blank page or other PDF generation issues Enter your shop name Error Error creating PDF, please contact the site owner. Ewout Fernhout Extended currency symbol support Extra field 1 Extra field 2 Extra field 3 Extra template fields Finish Font subsetting can reduce file size by only including the characters that are used in the PDF, but limits the ability to edit PDF files later. Recommended if you're using an Asian font. Fonts reinstalled! Footer: terms & conditions, policies, etc. For custom templates, contact us at %s. Fully <b>WPML Compatible</b> – emails will be automatically sent in the order language. General General settings Get WooCommerce PDF Invoices & Packing Slips Bundle Get WooCommerce PDF Invoices & Packing Slips Professional! Get WooCommerce Smart Reminder Emails Go Pro: Proforma invoices, credit notes (=refunds) & more! Happy selling! Hide this message How do you want to view the PDF? How to update your PHP version I am the wizard If you have any questions please have a look at our documentation: Image resolution Integrates seamlessly with the PDF Invoices & Packing Slips plugin Invoice Invoice Date Invoice Date: Invoice Number Invoice Number (unformatted!) Invoice Number: Invoice numbers are created by a third-party extension. Invoices & Packing Slips It looks like the temp folder (<code>%s</code>) is not writable, check the permissions for this folder! Without having write access to this folder, the plugin will not be able to email invoices. It looks like you haven't setup any email attachments yet, check the settings under <b>%sDocuments > Invoice%s</b> It would mean a lot to us if you would quickly give our plugin a 5-star rating. Help us spread the word and boost our motivation! Jumpstart the plugin by following our wizard! Legacy Document Legacy mode Legacy mode ensures compatibility with templates and filters from previous versions. Legacy settings deleted! Lets quickly setup your invoice. Please enter the name and address of your shop in the fields on the right. Letter Manual email N/A Never New to WooCommerce PDF Invoices & Packing Slips? Next Next invoice number (without prefix/suffix etc.) Nothing to delete! Number format Only for specific order statuses (define below) Only when an invoice is already created/emailed Open the PDF in a new browser tab/window Order Date: Order Number: Output to HTML PDF Invoice data PDF Invoices Packing Slip Padding Paper format Paper size Payment Method: Payment method Prefix Premium PDF Invoice bundle: Everything you need for a perfect invoicing system Previous Price Product Professional features: Quantity Ready! Reinstall fonts Remove image Remove temporary files Reset invoice number yearly Run the Setup Wizard SKU SKU: Save Save order & send email Select or upload your invoice header/logo Select some additional display options for your invoice. Select the paper format for your invoice. Select to which emails you would like to attach your invoice. Send email Send order email Send out a separate <b>notification email</b> with (or without) PDF invoices/packing slips, for example to a drop-shipper or a supplier. Send the template output as HTML to the browser instead of creating a PDF. Set image Set invoice number & date Set the header image that will display on your invoice. Settings Ship To: Shipping Shipping Address: Shipping Method: Shipping method Shop Address Shop Name Shop header/logo Skip this step Some of the export parameters are missing. Status Subtotal Successfully deleted %d files! Suffix Supercharge WooCommerce PDF Invoices & Packing Slips with the all our premium extensions: Supercharge WooCommerce PDF Invoices & Packing Slips with the following features: Support Forum Tax rate Test mode The following function was called These are used for the (optional) footer columns in the <em>Modern (Premium)</em> template, but can also be used for other elements in your custom template This extension conveniently uploads all the invoices (and other pdf documents from the professional extension) that are emailed to your customers to Dropbox. The best way to keep your invoice administration up to date! This is footer column 1 in the <i>Modern (Premium)</i> template This is footer column 2 in the <i>Modern (Premium)</i> template This is footer column 3 in the <i>Modern (Premium)</i> template This is the number that will be used for the next document. By default, numbering starts from 1 and increases for every new document. Note that if you override this and set it lower than the current/highest number, this could create duplicate numbers! Total Total ex. VAT Two extra stylish premade templates (Modern & Business) Unable to delete %d files! (deleted %d) Unable to read temporary folder contents! Upload automatically to dropbox Use <b>separate numbering systems</b> and/or format for proforma invoices and credit notes or utilize the main invoice numbering system Use alternative HTML5 parser to parse HTML Use the plugin in multilingual <b>WPML</b> setups VAT Want to use your own template? Copy all the files from <code>%s</code> to your (child) theme in <code>%s</code> to customize them Weight: When enabled, the document will always reflect the most current settings (such as footer text, document name, etc.) rather than using historical settings. With test mode enabled, any document generated will always use the latest settings, rather than using the settings as configured at the time the document was first created. WooCommerce WooCommerce Automatic Order Printing WooCommerce PDF Invoices WooCommerce PDF Invoices & Packing Slips WooCommerce PDF Invoices & Packing Slips &rsaquo; Setup Wizard WooCommerce PDF Invoices & Packing Slips requires %sWooCommerce%s to be installed & activated! WooCommerce PDF Invoices & Packing Slips requires PHP 5.3 or higher (5.6 or higher recommended). WooCommerce Smart Reminder emails Wow, you have created more than %d invoices with our plugin! Yes you deserve it! You are good to go! You do not have sufficient permissions to access this page. You haven't selected any orders Your logo click items to read more enter the number of digits here - enter "6" to display 42 as 000042 every %s days h http://www.wpovernight.com invoice invoices m note: if you have already created a custom invoice number format with a filter, the above settings will be ignored packing-slip packing-slips to use the invoice year and/or month, use [invoice_year] or [invoice_month] respectively PO-Revision-Date: 2018-12-22 12:08:28+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: es
Project-Id-Version: Plugins - WooCommerce PDF Invoices &amp; Packing Slips - Stable (latest release)
 %s correo electrónico de aviso enviado manualmente. (incluye %s) (incluye %s) <b>¡Precaución!</b> Este ajuste puede mostrar también errores (de otros plugins) en otras partes de tu web, por tanto no se recomienda dejarlo activo en sitios en producción. Correos electrónicos <b>completamente automáticos</b> <b>Personaliza</b> el formato de la <b>dirección de envío y facturación</b> para incluir más campos personalizados, tamaños de fuentes, etc., sin necesidad de crear una plantilla personalizada. <b>Desactivado:</b> Las funciones PHP glob y filemtime son obligatorias para la limpieza automática pero no están activas en tu servidor. <b>Editor visual</b> para el texto del correo electrónico, incluyendo los marcadores de posición de los datos del pedido (nombre, total del pedido, etc) <b>¡Súper versatil!</b> Se puede utilizar en cualquier recordatorio por correo electrónico (recordatorios de valoraciones, compras repetidas) <b>¡Advertencia!</b> Los siguientes ajustes solo son para depuración/desarrollo. ¡No los uses en un sitio en directo! <strong>Precaución:</strong> activar esto también implicará que si cambias el nombre o dirección de empresa en un futuro también afectará a los documentos generados previamente. <strong>Nota:</strong> Los números y fechas de factura no se ven afectados por este ajuste y se seguirán generando. <strong>¡Advertencia!</strong> Tu base de datos tiene un tamaño de paso de AUTO_INCREMENT de %s, tus números de factura podrán no ser consecutivos. Activa el ajuste «Calcular números del documento (lento)» en la pestaña «Estado» o usa un método alternativo. A4 En realidad tengo una queja… Correo electrónico de administrador Plantillas avanzadas, personalizables A continuación se muestran todos los documentos disponibles. Haz clic en un documento para configurarlo. Permitir descarga de facturas en Mi cuenta ¡Ya lo hice! Siempre Usar siempre los ajustes más actuales Se ha utilizado una plantilla o gancho de acción obsoleto para generar el PDF. Se ha activado el modo heredado, por favor, inténtalo de nuevo recargando esta página. ¿Estás seguro de que quieres borrar este documento? Esto no puede deshacerse. Adjunta <b>hasta 3 archivos estáticos</b> de tu elección (por ejemplo, un documento de términos y condiciones) a los correos electrónicos de WooCommerce. Adjuntar a: Adjuntar a… Adjuntos Archivos PDF limpiados automáticamente almacenados en la carpeta temporal (utilizados para los adjuntos por correo electrónico) Envía automáticamente los nuevos pedidos y albaranes a tu impresor, ¡en el momento en que el cliente haga el pedido! Envío automático de recordatorios de pago a tus clientes Dirección de facturación: Calcular números de documento (lento) Conoce las plantillas premium de PDF Invoice & Packing Slips en %s. Conoce la extensión WooCommerce Automatic Order Printing de nuestros colaboradores Simba Hosting ¡Echa un vistazo a estas extensiones premium! Elige una plantilla Personaliza completamente el contenido de la factura (precios, impuestos, miniaturas) según tus necesidades con un personalizador de arrastrar y soltar Configúralo <a href="%s">aquí</a>. Configura los requisitos exactos para el envío de un correo electrónico (tiempo después del pedido, estado del pedido, método de pago) Crear PDF Crea, imprime y manda por correo electrónico facturas en PDF y comprobantes de envío de los pedidos de WooCommerce. Notas del cliente Salida de DEPURACIÓN activa Ajustes de depuración Borrar ajustes heredados (1.X) Descripción Desactiva la creación/adjunto automática cuando solo se pidan productos gratuitos Desactivar para productos gratuitos Descuento Mostrar dirección de facturación Muestra la dirección de facturación (además de la dirección de envío por defecto) si es diferente de la dirección de envío Mostrar dirección de correo eletrónico Mostrar la fecha de factura Mostrar número de factura Opciones de visualización Mostrar número de teléfono Mostrar dirección de envío Muestra la dirección de envío (además de la dirección de facturación por defecto) si es diferente de la dirección de facturación Los números de documento (como los números de factura) se generan usando por defecto AUTO_INCREMENT. Utiliza este ajuste si tu base de datos incrementa automáticamente con más de 1. No se pudo generar el tipo de documento '%s' para el/los pedido(s) seleccionado(s) Documentación Documentos Descargar %s (PDF) Descargar factura (PDF) Descargando el PDF Envía por correo electrónico/imprime <b>facturas proforma y notas de crédito en PDF</b> Activar Activar la limpieza automática Activar la salida de depuración Activar ajuste de fuentes Activar la columna de número de factura en pedidos Activa esto si tu símbolo de moneda no se está mostrando correctamente Activa esta opción para mostrar los errores del plugin si estás recibiendo una página en blanco u otros errores de generación del PDF Introduce el nombre de tu tienda Error Error al crear el PDF, por favor, contacta con el propietario del sitio. Ewout Fernhout Compatibilidad con símbolo de moneda extendido Campo extra 1 Campo extra 2 Campo extra 3 Campos extra de la plantilla Finalizar El ajuste de fuentes puede reducir el tamaño de archivo simplemente incluyendo los caracteres utilizados en el PDF, pero limita la posibilidad de modificar posteriormente los archivos PDF. Recomendable si usas una fuente asiática. ¡Fuentes reinstaladas! Pie de página: términos y condiciones, políticas, etc. Para plantillas personalizadas contacta con nosotros en %s. Totalmente <b>compatible con WPML</b> – se enviarán automáticamente correos electrónicos en el idioma del pedido. General Ajustes generales Obtén el paquete de WooCommerce PDF Invoices & Packing Slips ¡Obtén WooCommerce PDF Invoices & Packing Slips Professional! Obtener WooCommerce Smart Reminder Emails Hazte pro: ¡Facturas proforma, notas sobre tarjetas (=reembolsos) y más! ¡Buenas ventas! Ocultar este mensaje ¿Cómo deseas visualizar el PDF? Cómo actualizar tu versión de PHP Soy el asistente Si tienes alguna pregunta, por favor, echa un vistazo a nuestra documentación: Resolución de la imagen Se integra de manera fluida con el plugin PDF Invoices & Packing Slips Factura Fecha de factura Fecha de factura: Número de factura Número de factura (sin formato) Número de factura: Los números de factura los crea una extensión de terceros. Facturas y albaranes Parece que la carpeta temporal (<code>%s</code>) no es modificable, ¡comprueba los permisos de esta carpeta! Sin acceso de escritura a esta carpeta el plugin no será capaz de enviar facturas por correo electrónico. Parece que aún no has configurado ningún adjunto de correo electrónico, revisa los ajustes en <b>%sDocumentos > Factura%s</b> Apreciaríamos un montón si nos dieses una valoración de 5 estrellas. ¡Ayúdanos a difundir la palabra y refuerza nuestra motivación! ¡Comienza ya con el plugin siguiendo nuestro asistente!  Documento heredado Modo heredado El modo heredado asegura la compatibilidad con plantillas y filtros de versiones anteriores. ¡Ajustes heredados borrados! Permite la configuración rápida de tu factura. Por favor, introduce el nombre y dirección de tu tienda en los campos de la derecha. Carta Correo electrónico manual N/D Nunca ¿Eres nuevo con WooCommerce PDF Invoices & Packing Slips? Siguiente Siguiente número de la factura (sin prefijo/sufijo, etc.) ¡No hay nada que borrar! Formato de numeración Solo para estados de pedido específicos (definir abajo) Solo cuando ya se haya creado/enviado una factura Abriendo el PDF en una nueva pestaña/ventana del navegador Fecha de pedido: Número de pedido: Enviar como HTML Datos de factura PDF Facturas PDF Albarán de entrega Relleno Formato del papél Tamaño del papel Método de pago: Método de pago Prefijo Paquete premium de PDF Invoice: Todo lo que necesitas para un sistema de facturas perfecto Anterior Precio Producto Características profesionales: Cantidad ¡Listo! Reinstalar fuentes Eliminar la imagen Eliminar archivos temporales Restablecer el número de factura anualmente Ejecuta el asistente de instalación SKU SKU: Guardar Guardar pedido y enviar correo electrónico Selecciona o sube una logotipo para la cabecera/logo de la factura Elige alguna opción adicional de visualización para tu factura. Elige el formato adecuado para tu factura. Elige a qué correos electrónicos te gustaría adjuntar tu factura. Enviar correo electrónico Enviar correo electrónico del pedido Envía otro <b>aviso por correo electrónico</b> con (o sin) etiquetas de factura/envío, por ejemplo a un proveedor o suministrador. Envía la plantilla como HTML al navegador en vez de crear un PDF. Añadir imagen Configurar número y fecha de factura Establece la imagen de cabecera que se mostrará en tu factura. Ajustes Enviar a: Envío Dirección de envío: Método de envío: Método de envío Dirección de la tienda Nombre de la tienda Cabecera/logotipo de la tienda Saltar este paso Faltan algunos parámetros de exportación. Estado Subtotal ¡Borrados con éxito %d archivos! Sufijo Potencia WooCommerce PDF Invoices & Packing Slips con todas nuestras extensiones premium: Potencia WooCommerce PDF Invoices & Packing Slips con las siguientes características: Foro de soporte Tasa de impuestos Modo de pruebas Se llamó a la siguiente función Estos se usan para las columnas del pie de página (opcional) en la plantilla <em>Modern (Premium)</em>, pero también se pueden emplear en tu plantilla personalizada Esta extensión sube cómodamente todas las facturas (y otros documentos pdf de la extensión profesional) que se envían por correo electrónico a tus clientes en Dropbox. ¡El mejor modo de tener al día tu administración de facturas! Esta es la columna 1 del pie de página en la plantilla <i>Modern (Premium)</i>. Esta es la columna 2 del pie de página en la plantilla <i>Modern (Premium)</i>. Esta es la columna 3 del pie de página en la plantilla <i>Modern (Premium)</i>. Este es el número que se usará en el siguiente documento. Por defecto, la numeración comienza desde 1 y se incrementa en cada nuevo documento. ¡Ten en cuenta que si omites esto y lo configuras a un número inferior que el número actual/más alto, esto creará números duplicados. Total Total sin IVA Dos plantillas con estilo adicionales (moderna y negocios) ¡No ha sido posible borrar %d archivos! (%d borrados) ¡No ha sido posible leer el contenido de la carpeta temporal! Subida automática a dropbox Utiliza <b>distintos sistemas de numeración</b> y/o formatos para facturas proforma y notas de crédito, o utiliza el sistema de numeración de facturas principal Usar el analizador alternativo en HTML5 para analizar el HTML Utiliza el plugin en instalaciones multilingues con <b>WPML</b> IVA ¿Quieres usar tu propia plantilla? Copia los archivos de la carpeta <code>%s</code> a tu "theme" (o child theme) en <code>%s</code> para personalizarlos. Peso: Cuando se activa el documento siempre reflejará los ajustes más recientes (como el texto del pié de página, nombre del documento, etc.) en vez de usar ajustes históricos. Con el modo de pruebas activo cualquier documento que se genere usará siempre los últimos ajustes, en vez de usar los ajustes como se configuraron en el momento en que se creó el documento por primera vez. WooCommerce WooCommerce Automatic Order Printing WooCommerce PDF Invoices WooCommerce PDF Invoices & Packing Slips WooCommerce PDF Invoices & Packing Slips &rsaquo; Asistente de configuración ¡WooCommerce PDF Invoices & Packing Slips requiere que %sWooCommerce%s esté instalado y activo! WooCommerce PDF Invoices & Packing Slips requiere PHP 5.3 o superior (recomendada la versión 5.6 o superior). WooCommerce Smart Reminder emails ¡Vaya, has creado más de %d facturas con nuestro plugin! ¡Sí, lo mereces! ¡Ya estás listo para empezar! No tienes suficientes permisos para acceder a esta página. No has seleccionado ningún pedido Tu logotipo haz clic en los elementos para saber más introduce aquí el número de dígitos - introduce "6" para mostrar 42 como 000042 cada %s días h http://www.wpovernight.com factura facturas m Nota: si ya has creado un formato personalizado de numeración de facturas con un filtro, los ajustes anteriores se ignorarán albarán de entrega albaranes de entrega para usar el año y/o el mes de la factura utiliza [invoice_year] o [invoice_month] respectivamente 